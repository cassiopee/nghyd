# ngHyd - Angular application for Hydraulics using JaLHyd library

:warning: **This repository has been migrated to <https://forgemia.inra.fr/cassiopee/nghyd>** :warning:

**This repository is an archive that is no longer in use since the 7<sup>th</sup> November 2023.**


See also [developers documentation](DEVELOPERS.md) (in french)

## Build and deploy

### Requirements

Requirements for developping Cassiopee can be achieved by manually install the required dependencies on your computer or by using the dedicated docker container.

#### Required dependencies

 * [jalhyd](https://gitlab.irstea.fr/cassiopee/jalhyd)
 * npm
 * python3
 * pandoc ^2 (optional, for PDF documentation only)
 * texlive, texlive-bibtex-extra, texlive-latex-extra, latexmk (optional, for PDF documentation only)

Building the HTML documentation requires MkDocs and some extensions:

```sh
sudo apt install python3-pip python3-setuptools
python3 -m pip install mkdocs python-markdown-math mkdocs-material
```

Building the PDF documentation requires pandoc and a LaTeX distribution (for ex. texlive) with a few packages:

```sh
sudo apt install pandoc texlive latexmk texlive-latex-extra texlive-bibtex-extra
```

#### Using docker container

Download and use the following docker image: https://hub.docker.com/repository/docker/geaucassiopee/ci-cd-cross-platform-webapp

More details on how to use it on vscode are available at https://gitlab.irstea.fr/cassiopee/cassiopee2-integration

### Install dependencies

#### JaLHyd
Clone JalHyd into to ngHyd, for ex. respectively in `/home/foo/nghyd` and `/home/foo/nghyd/jalhyd`.

In `jalhyd` folder, run :

```sh
npm run package
```

#### other dependencies
Then in `nghyd` folder, run :

```sh
npm ci --force --unsafe-perm
```

This installs the exact same version of dependencies as the ones specified in `package.lock.json`.
The parameter `--unsafe-perm` solves permissions issues for running e2e tests in a docker container.

### Compile and get a deployable Web app

```sh
npm run build
```


### Compile and get a deployable Web app (without PDF doc)
Use this if you don't want to install LaTeX dependencies.
```sh
npm run build-no-pdf
```


### Compile in dev (watch) mode

```sh
npm start
```


### Run end-to-end unit tests

```sh
npm run e2e
```


### Quickly run end-to-end unit tests while watch mode is running

```sh
npm run e2equick
```


### Quickly try electron wrapping when code is already compiled

```sh
npm run electron
```


### Build a desktop release for Linux (from Linux platform)

#### build Debian package

```sh
npm run release-linux
```

Find the .deb package in `/release`.

Running `dpkg -i cassiopee_*.deb` will install Cassiopée in `/opt/Cassiopee`


### Build a desktop release for Windows (from Linux platform)

#### install dependencies
 * wine >= 2.0 - see https://wiki.winehq.org/Download#binary

#### build .exe installer

```sh
npm run release-windows
```

Find the generated installer in `/release`.

Running the generated installer will install Cassiopée in `C:\Users\YourUser\AppData\local\Programs\cassiopee`


### Build a desktop release for Windows (from Windows platform)

#### install dependencies
 * python for windows https://www.python.org/downloads/windows/
    * tick "add to path" option when installing
 * mkdocs: `pip install mkdocs`
 * mkdocs-material: `pip install mkdocs-material`
 * python-markdown-math: `pip install https://github.com/mitya57/python-markdown-math/archive/master.zip`
 * pygments: `pip install pygments`

#### build .exe installer

```sh
npm run release-windows
```

Find the generated installer in `/release`.

Running the generated installer will install Cassiopée in `C:\Users\YourUser\AppData\local\Programs\cassiopee`


### Build a desktop release for MacOS (from Linux platform)

#### build package

```sh
npm run release-mac
```

Find the generated package in `/release`.

Note: the generated package will not be signed.


### Generate HTML documentation

```sh
npm run mkdocs
```


### Generate PDF documentation

```sh
npm run mkdocs2pdf
```


### Generate compodoc

```sh
npm run compodoc
```


### Flag suspicious language usage

```sh
npm run lint
```


### Generate UML diagram

The tsviz package can be used for drawing class diagram of the current code.

To install tsviz:
```sh
npm install -g tsviz
```

There's currently a bug on debian like distribution due to a wrong declaration of graphviz path in the code: https://github.com/joaompneves/tsviz/issues/5
As a workaround, you can create a link to the right path: `sudo ln -s /usr/bin/dot /usr/local/bin/dot`

To draw the diagram:
```sh
npm run viz
```

## Caveats

### Deployment

Custom Material SVG Icons will only show up when the application is deployed on the domain root (no subfolders), see [this feature request](https://github.com/angular/material2/issues/4263)

### chromedriver version in e2e tests

The chromedriver version used in e2e tests is determined by the Docker image dedicated to these tests. For further information, see Dockerfile of cassiopee2-integration project.

## Release policy

Use [semantic versioning](https://semver.org/).

**It's discouraged to execute release steps manually, skip this section and see Release Script below**

Before releasing a new stable version, a new version of JaLHyd should be tagged, see "Release Policy" in [JaLHyd's README.md](https://gitlab.irstea.fr/cassiopee/jalhyd/blob/master/README.md)

Then, one should complete the following files:
 - `CHANGELOG.md`
 - `package.json` (update "version", or use `npm version`)
 - `jalhyd_branch` (be sure that it contains "master" or is empty)

Every stable version should be tagged with both
 - the `stable` tag
 - a version tag of the form `X.Y.Z` (semver)

The `stable` tag should be set **before** the version tag, so that `git describe` returns `X.Y.Z` (latest tag). There should be **at least 1s** between the two tag commands, so that date sorting is not confused.

### Release script

**Important:** the release script assumes that you run it from the current nghyd source directory `nghyd`, and that JaLHyd source directory `jalhyd` is present under the `nghyd` directory.

Before running the script:
 * update `CHANGELOG.md` in both JaLHyd and NgHyd
 * set the content of `jalhyd_branch` to "master"

This script:
 * checks out "master" branch of JaLHyd, pulls the latest changes, installs dependencies, runs unit tests, commits changes if any
 * updates JaLHyd version in `package.json`, commits changes
 * creates the right tags for JaLHyd and pushes them
 * checks out "master" branch of NgHyd, pulls the latest changes, installs dependencies, commits changes if any
 * updates NgHyd version in `package.json`, commits changes
 * creates the right tags for NgHyd and pushes them

It **does not** check that `jalhyd_branch` is OK nor that `jalhyd/CHANGELOG.md` and `nghyd/CHANGELOG.md` are up to date, but reminds you to do it.

Once tags are pushed, Gitlab CI/CD takes care of building and deploying the new packages.

Example for a **4.10.4** version :

```sh
./scripts/deploy-new-stable-version.sh 4.10.4
```
