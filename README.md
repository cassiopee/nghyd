# ngHyd - Angular application for Hydraulics using JaLHyd library

See also [developers documentation](DEVELOPERS.md) (in french)

## Build and deploy

### Requirements

Requirements for developping Cassiopee can be achieved by manually install the required dependencies on your computer or by using the dedicated docker container.

#### Required dependencies

 * [jalhyd](https://forgemia.inra.fr/cassiopee/jalhyd)
 * npm
 * python3
 * pandoc ^2 (optional, for PDF documentation only)
 * texlive, texlive-bibtex-extra, texlive-latex-extra, latexmk (optional, for PDF documentation only)

Building the HTML documentation requires MkDocs and some extensions:

```sh
sudo apt install python3-pip python3-setuptools
python3 -m pip install mkdocs python-markdown-math mkdocs-material
```

Building the PDF documentation requires pandoc and a LaTeX distribution (for ex. texlive) with a few packages:

```sh
sudo apt install pandoc texlive latexmk texlive-latex-extra texlive-bibtex-extra
```

#### Using docker container

Use the DockerFile provided at the root of this repository. This repository also
provide a `.devcontainer` configuration for developing nghyd in vscode
(See https://code.visualstudio.com/docs/devcontainers/containers).

### Install dependencies

#### JaLHyd
Clone JalHyd into to ngHyd, for ex. respectively in `/home/foo/nghyd` and `/home/foo/nghyd/jalhyd`.

In `jalhyd` folder, run :

```sh
npm run package
```

#### other dependencies
Then in `nghyd` folder, run :

```sh
npm ci --force --unsafe-perm
```

This installs the exact same version of dependencies as the ones specified in `package.lock.json`.
The parameter `--unsafe-perm` solves permissions issues for running e2e tests in a docker container.

### Compile and get a deployable Web app

```sh
npm run build
```


### Compile and get a deployable Web app (without PDF doc)
Use this if you don't want to install LaTeX dependencies.
```sh
npm run build-no-pdf
```


### Compile in dev (watch) mode

```sh
npm start
```


### Run end-to-end unit tests

```sh
npm run e2e
```


### Quickly run end-to-end unit tests while watch mode is running

```sh
npm run e2equick
```


### Quickly try electron wrapping when code is already compiled

```sh
npm run electron
```


### Build a desktop release for Linux (from Linux platform)

#### build Debian package

```sh
npm run release-linux
```

Find the .deb package in `/release`.

Running `dpkg -i cassiopee_*.deb` will install Cassiopée in `/opt/Cassiopee`


### Build a desktop release for Windows (from Linux platform)

#### install dependencies
 * wine >= 2.0 - see https://wiki.winehq.org/Download#binary

#### build .exe installer

```sh
npm run release-windows
```

Find the generated installer in `/release`.

Running the generated installer will install Cassiopée in `C:\Users\YourUser\AppData\local\Programs\cassiopee`


### Build a desktop release for Windows (from Windows platform)

#### install dependencies
 * python for windows https://www.python.org/downloads/windows/
    * tick "add to path" option when installing
 * mkdocs: `pip install mkdocs`
 * mkdocs-material: `pip install mkdocs-material`
 * python-markdown-math: `pip install https://github.com/mitya57/python-markdown-math/archive/master.zip`
 * pygments: `pip install pygments`

#### build .exe installer

```sh
npm run release-windows
```

Find the generated installer in `/release`.

Running the generated installer will install Cassiopée in `C:\Users\YourUser\AppData\local\Programs\cassiopee`


### Build a desktop release for MacOS (from Linux platform)

#### build package

```sh
npm run release-mac
```

Find the generated package in `/release`.

Note: the generated package will not be signed.


### Generate HTML documentation

```sh
npm run mkdocs
```


### Generate PDF documentation

```sh
npm run mkdocs2pdf
```


### Generate compodoc

```sh
npm run compodoc
```


### Flag suspicious language usage

```sh
npm run lint
```


### Generate UML diagram

The tsviz package can be used for drawing class diagram of the current code.

To install tsviz:
```sh
npm install -g tsviz
```

There's currently a bug on debian like distribution due to a wrong declaration of graphviz path in the code: https://github.com/joaompneves/tsviz/issues/5
As a workaround, you can create a link to the right path: `sudo ln -s /usr/bin/dot /usr/local/bin/dot`

To draw the diagram:
```sh
npm run viz
```

### CI/CD docker image

Gitlab CI/CD uses a docker image created with the `DockerFile` located at the root
of this repository. This image needs to be pushed on the Gitlab registry in order to
be used by the CI/CD.

#### Requirements

You need to have Docker installed on your computer (See: https://docs.docker.com/get-docker/).
On Windows, you first need to install a Linux distro under WSL2 (See
https://learn.microsoft.com/en-us/windows/wsl/install).

#### Build and push container images to the Container Registry

This notice is inspired from
https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html.

You first need to create an access token (personal or project token):

Go to https://gitlab.irstea.fr/-/profile/personal_access_tokens
and create a token with a minimum scope `write_registry` and `read_registry`.

Open a terminal in the root folder of this repository on your local machine,
and type (with `<token name>` and `<token value>` the credentials
of the access token):

```
docker login registry.forgemia.inra.fr -u <token name> -p <token value>
docker build -t registry.forgemia.inra.fr/cassiopee/nghyd .
docker push registry.forgemia.inra.fr/cassiopee/nghyd
```


### Deployment

Custom Material SVG Icons will only show up when the application is deployed on the domain root (no subfolders), see [this feature request](https://github.com/angular/material2/issues/4263)

### chromedriver version in e2e tests

The chromedriver version used in e2e tests is determined by the Docker image dedicated to these tests. For further information, see Dockerfile of cassiopee2-integration project.

## Release policy

Use [semantic versioning](https://semver.org/).

**It's discouraged to execute release steps manually, skip this section and see Release Script below**

Before releasing a new stable version, a new version of JaLHyd should be tagged, see "Release Policy" in [JaLHyd's README.md](https://forgemia.inra.fr/cassiopee/jalhyd/blob/master/README.md)

Then, one should complete the following files:
 - `CHANGELOG.md`
 - `package.json` (update "version", or use `npm version`)
 - `jalhyd_branch` (be sure that it contains "master" or is empty)

Every stable version should be tagged with both
 - the `stable` tag
 - a version tag of the form `X.Y.Z` (semver)

The `stable` tag should be set **before** the version tag, so that `git describe` returns `X.Y.Z` (latest tag). There should be **at least 1s** between the two tag commands, so that date sorting is not confused.

### Release script

**Important:** the release script assumes that you run it from the current nghyd source directory `nghyd`, and that JaLHyd source directory `jalhyd` is present under the `nghyd` directory.

Before running the script:
 * update `CHANGELOG.md` in both JaLHyd and NgHyd
 * set the content of `jalhyd_branch` to "master"

This script:
 * checks out "master" branch of JaLHyd, pulls the latest changes, installs dependencies, runs unit tests, commits changes if any
 * updates JaLHyd version in `package.json`, commits changes
 * creates the right tags for JaLHyd and pushes them
 * checks out "master" branch of NgHyd, pulls the latest changes, installs dependencies, commits changes if any
 * updates NgHyd version in `package.json`, commits changes
 * creates the right tags for NgHyd and pushes them

It **does not** check that `jalhyd_branch` is OK nor that `jalhyd/CHANGELOG.md` and `nghyd/CHANGELOG.md` are up to date, but reminds you to do it.

Once tags are pushed, Gitlab CI/CD takes care of building and deploying the new packages.

Example for a **4.10.4** version :

```sh
./scripts/deploy-new-stable-version.sh 4.10.4
```
