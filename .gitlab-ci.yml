stages:
  - install
  - test
  - build
  - clean-stale-branches
  - deploy-dev
  - deploy-prod
  - releases-nightly
  - releases-version

default:
  tags: [goldenrunner]
  image: $CI_REGISTRY/cassiopee/nghyd:latest

variables:
# from Gitlab CI/CD environment variables :
# - development server :
#   DEV_HOST : deployment server hostname
#   DEV_LOGIN : user on deployment server
#   DEV_PATH : path base on deployment server
# - production server :
#   PROD_HOST : deployment server hostname
#   PROD_LOGIN : user on deployment server
#   PROD_PATH : path base on deployment server
  LC_ALL: C.UTF-8
  ANDROID_HOME: "/usr/local/android-sdk" # should already be defined in the Docker image
  ANDROID_SDK_ROOT: "/usr/local/android-sdk" # should already be defined in the Docker image
  RELEASES_PATH: "$PROD_PATH/cassiopee-releases"

before_script:
  # load private key from GitLab CI variable to deploy on Aubes server
  - eval $(ssh-agent -s)
  - ssh-add <(echo "$AUBES_B64_AUTHORIZED_SSH_PRIVATE_KEY" | base64 -d)
  # load private key from GitLab CI variable to deploy on OVH server
  - ssh-add <(echo "$OVH_B64_AUTHORIZED_SSH_PRIVATE_KEY" | base64 -d)
  # disable console prompt for host checking
  - mkdir -p ~/.ssh
  - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

install:
  stage: install
  artifacts:
    paths:
      - node_modules/
      - jalhyd/
    expire_in: 1 min
  script:
    - JALHYD_BRANCH=`cat jalhyd_branch`
    - echo "CI_COMMIT_REF_NAME - $CI_COMMIT_REF_NAME"
    - if [ "$CI_COMMIT_REF_NAME" = "master" ]; then JALHYD_BRANCH="master"; fi
    - if [ "$CI_COMMIT_REF_NAME" = "devel" ]; then JALHYD_BRANCH="devel"; fi
    - echo "Branche JalHyd - $JALHYD_BRANCH"
    - git clone https://forgemia.inra.fr/cassiopee/jalhyd.git
    - cd jalhyd
    - git checkout "$JALHYD_BRANCH" || git checkout devel
    - npm ci --force
    - npm run build
    - cd ..
    - npm ci --force --unsafe-perm

test:
  stage: test
  only:
    - tags
    - schedules
    - web
    - master
    - devel
  script:
    - npm run e2e
  timeout: 2h

.build:
  stage: build
  dependencies:
    - install
  artifacts:
    expire_in: 10 min
    paths:
      - dist/
      - build
  script:
    # -baseref option is used by npm to set the npm_config_basehref environment variable
    # used in package.json
    - npm run build-href -basehref=$BASEHREF

build-dev:
  extends: .build
  only:
    - tags
    - pushes
    - schedules
    - web
  variables:
    BASEHREF: "/cassiopee/$CI_COMMIT_REF_NAME/"

build-prod:
  extends: .build
  only:
    - tags
    - devel
  variables:
    BASEHREF: "/"

clean-stale-branches:
  stage: clean-stale-branches
  only:
    - pushes
    - tags
    - web
  script:
    - ./scripts/remove-stale-branches.sh $DEV_LOGIN@$DEV_HOST $DEV_PATH

deploy-dev:
  stage: deploy-dev
  only:
    - pushes
    - tags
    - web
  dependencies:
    - build-dev
  script:
    # Copie de la branche / du tag
    - ./scripts/deploy-version.sh $CI_COMMIT_REF_NAME $DEV_LOGIN $DEV_HOST $DEV_PATH

# déploiement de la version devel sur OVH
deploy-ovh-dev:
  stage: deploy-dev
  only:
    - devel
  dependencies:
    - build-prod
  script:
    # Copie de la branche / du tag
    - ./scripts/deploy-version.sh prod-devel $PROD_LOGIN $PROD_HOST $PROD_DEV_PATH

deploy-prod:
  stage: deploy-prod
  only:
    variables:
      - $CI_COMMIT_REF_NAME == "stable"
  dependencies:
    - build-prod
  script:
    - ./scripts/deploy-version.sh prod $PROD_LOGIN $PROD_HOST $PROD_PATH

releases-nightly:
  stage: releases-nightly
  only:
    - schedules
  except:
  # exclude master to apply releases-nightly on devel only
    - master
  dependencies:
    - install
  script:
    - ./scripts/release-version.sh nightly $PROD_LOGIN $PROD_HOST $RELEASES_PATH

releases-version:
  stage: releases-version
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /^[0-9]+\.[0-9]+\.[0-9]+$/ # version tag
  dependencies:
    - install
  script:
    - ./scripts/release-version.sh $CI_COMMIT_REF_NAME $PROD_LOGIN $PROD_HOST $RELEASES_PATH
