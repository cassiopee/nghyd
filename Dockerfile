FROM docker.io/node:18.18-bookworm-slim

LABEL maintainer="bug@cassiopee.g-eau.fr"
LABEL version="3.1"

ENV DEBIAN_FRONTEND noninteractive

# Install base packages
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        locales \
        curl \
        build-essential \
        wget \
        ca-certificates \
        gnupg \
        git \
        rsync \
        openssh-client \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Set UTF-8 locale
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Add Repositories

## Wine
RUN dpkg --add-architecture i386 \
    && wget -nc https://dl.winehq.org/wine-builds/winehq.key \
    && apt-key add winehq.key \
    && echo "deb https://dl.winehq.org/wine-builds/debian/ bookworm main" > /etc/apt/sources.list.d/winehq.list

# Install packages
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
## MkDocs
        mkdocs \
        python3-mdx-math \
        mkdocs-material-extensions \
## Pandoc & LaTeX for PDF doc
        pandoc \
        texlive \
        latexmk \
        texlive-latex-extra \
        texlive-bibtex-extra \
        texlive-lang-french \
        texlive-xetex \
## Wine
        winehq-stable \
## chromium and procps (e2e)
        chromium chromium-driver procps \
## Clean cache
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /build

ENV SHELL /bin/bash
