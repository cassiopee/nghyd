import { Injectable, Inject } from "@angular/core";

import { HttpService } from "./http.service";

import { Observable, SessionSettings } from "jalhyd";

import { StorageService, LOCAL_STORAGE } from "ngx-webstorage-service";

/**
 * Stores app preferences
 */
@Injectable()
export class ApplicationSetupService extends Observable {

    private CONFIG_FILE_PATH = "app/config.json";
    private LOCAL_STORAGE_PREFIX = "nghyd_";

    /** ultimate fallback language (read from config) */
    private _fallbackLanguage = "fr";

    // default builtin values
    public displayPrecision = 3;
    private static readonly _defaultComputePrecision = 1e-7; // tied to model
    private static readonly _defaultMaxIterations = 100; // tied to model
    public enableNotifications = true;
    public enableHotkeys = false;
    private _enableEmptyFieldsOnFormInit = true;
    private _foldedMessageLog = false;

    public set computePrecision(p: number) {
        SessionSettings.precision = p;
    }

    public get computePrecision(): number {
        return SessionSettings.precision;
    }

    public set maxIterations(m: number) {
        SessionSettings.maxIterations = m;
    }

    public get maxIterations(): number {
        return SessionSettings.maxIterations;
    }

    public get enableEmptyFieldsOnFormInit() {
        return this._enableEmptyFieldsOnFormInit;
    }

    public set enableEmptyFieldsOnFormInit(b: boolean) {
        this._enableEmptyFieldsOnFormInit = b;
    }

    public get foldedMessageLog() {
        return this._foldedMessageLog;
    }

    public set foldedMessageLog(b: boolean) {
        this._foldedMessageLog = b;
    }

    /**
     * just stores the current language preference, does not transmit it to I18nService, that is
     * not available here.
     *
     * @see ApplicationSetupComponent.currentLanguageCode() setter
     * @see I18nService.update() observer
     */
    public language = "fr";

    /** themes to group calculators, for displaying on the front page */
    public themes: any[];

    public warnBeforeTabClose: boolean;

    public constructor(
        private httpService: HttpService,
        @Inject(LOCAL_STORAGE) private storage: StorageService
    ) {
        super();
        // precedence: builtin values >> JSON config >> browser's language >> local storage
        const builtinLanguage = this.language;

        // related to @HostListener("window:beforeunload") in AppComponent
        this.warnBeforeTabClose = true;

        // by default, create empty fields for new calculators
        this.enableEmptyFieldsOnFormInit = true;

        // load JSON config
        this.readValuesFromConfig().then((data) => {
            const configLanguage = this.language;
            this._fallbackLanguage = configLanguage;

            // guess browser's language
            this.language = navigator.language.substring(0, 2).toLowerCase();
            const browserLanguage = this.language;

            // load saved preferences
            const loadedPrefKeys = this.readValuesFromLocalStorage();
            let storageLanguage: string;
            if (loadedPrefKeys.includes("language")) {
                storageLanguage = this.language;
            }

            // notify I18nService
            this.notifyObservers({
                action: "languagePreferenceChanged",
                languages: [ storageLanguage, browserLanguage, configLanguage, builtinLanguage ]
            });
        });
    }

    public get fallbackLanguage() {
        return this._fallbackLanguage;
    }

    /**
     * Save configuration values into local storage
     */
    public saveValuesIntoLocalStorage() {
        this.storage.set(this.LOCAL_STORAGE_PREFIX + "displayPrecision", this.displayPrecision);
        this.storage.set(this.LOCAL_STORAGE_PREFIX + "computePrecision", this.computePrecision);
        this.storage.set(this.LOCAL_STORAGE_PREFIX + "maxIterations", this.maxIterations);
        this.storage.set(this.LOCAL_STORAGE_PREFIX + "enableNotifications", this.enableNotifications);
        this.storage.set(this.LOCAL_STORAGE_PREFIX + "enableHotkeys", this.enableHotkeys);
        this.storage.set(this.LOCAL_STORAGE_PREFIX + "enableEmptyFieldsOnFormInit", this.enableEmptyFieldsOnFormInit);
        this.storage.set(this.LOCAL_STORAGE_PREFIX + "language", this.language);
    }

    /**
     * Restore configuration values
     */
    public async restoreDefaultValues(): Promise<any> {
        await this.readValuesFromConfig();
        // notify I18nService
        this.notifyObservers({
            action: "languagePreferenceChanged",
            languages: [ this.language ]
        });
    }

    /**
     * Read configuration values from local storage
     */
    private readValuesFromLocalStorage(): string[] {
        const loadedKeys = [];
        // get all config values (voluntarily non-generic to prevent side-effects)
        const displayPrecision = this.storage.get(this.LOCAL_STORAGE_PREFIX + "displayPrecision");
        if (displayPrecision !== undefined) {
            this.displayPrecision = displayPrecision;
            loadedKeys.push("displayPrecision");
        }
        const computePrecision = this.storage.get(this.LOCAL_STORAGE_PREFIX + "computePrecision");
        if (computePrecision !== undefined) {
            this.computePrecision = computePrecision;
            loadedKeys.push("computePrecision");
        }
        const maxIterations = this.storage.get(this.LOCAL_STORAGE_PREFIX + "maxIterations");
        if (maxIterations !== undefined) {
            this.maxIterations = maxIterations;
            loadedKeys.push("maxIterations");
        }
        const enableNotifications = this.storage.get(this.LOCAL_STORAGE_PREFIX + "enableNotifications");
        if (enableNotifications !== undefined) {
            this.enableNotifications = enableNotifications;
            loadedKeys.push("enableNotifications");
        }
        const enableHotkeys = this.storage.get(this.LOCAL_STORAGE_PREFIX + "enableHotkeys");
        if (enableHotkeys !== undefined) {
            this.enableHotkeys = enableHotkeys;
            loadedKeys.push("enableHotkeys");
        }
        const enableEmptyFieldsOnFormInit = this.storage.get(this.LOCAL_STORAGE_PREFIX + "enableEmptyFieldsOnFormInit");
        if (enableEmptyFieldsOnFormInit !== undefined) {
            this.enableEmptyFieldsOnFormInit = enableEmptyFieldsOnFormInit;
            loadedKeys.push("enableEmptyFieldsOnFormInit");
        }
        const language = this.storage.get(this.LOCAL_STORAGE_PREFIX + "language");
        if (language !== undefined) {
            this.language = language;
            loadedKeys.push("language");
        }
        return loadedKeys;
    }

    /**
     * Read configuration values from config (async)
     */
    private async readValuesFromConfig(): Promise<any> {
        const data: any = await this.httpService.httpGetRequestPromise(this.CONFIG_FILE_PATH);
        // get all config values (volontarily non-generic to prevent side-effects)
        this.displayPrecision = data.params.displayPrecision;
        this.computePrecision = data.params.computePrecision;
        this.maxIterations = data.params.maxIterations;
        this.enableNotifications = data.params.enableNotifications;
        this.enableHotkeys = data.params.enableHotkeys;
        this.enableEmptyFieldsOnFormInit = data.params.enableEmptyFieldsOnFormInit;
        this.language = data.params.language;
        // load themes for calculators list page
        this.themes = data.themes;
    }
}
