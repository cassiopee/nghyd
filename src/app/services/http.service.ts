import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class HttpService {

    constructor(private http: HttpClient) { }

    /**
     * Lance une requête GET (version standard Observable)
     * @param path ex: /mon-service/toto
     * @param headers ex: new HttpHeaders { "Authorization": "bla" }
     */
    public httpGetRequest(path: string, headers?: HttpHeaders): Observable<Object> {
        // ajout entêtes
        const opts = {};
        if (headers) {
            opts["headers"] = headers;
        }
        // lancement requête
        return this.http.get(encodeURI(path), opts);
    }

    /**
     * Lance une requête GET (version Promise)
     * @see httpGetRequest
     */
    public httpGetRequestPromise(path: string, headers?: HttpHeaders): Promise<Object> {
        const res$: Observable<Object> = this.httpGetRequest(path, headers);
        return res$.toPromise();
    }

    /**
     * Lance une requête GET et renvoie une Promise contenant un Blob (File)
     */
    public httpGetBlobRequestPromise(path: string): Promise<Blob> {
        return this.http.get<any>(encodeURI(path), { responseType: "blob" as "json" }).toPromise();
    }
}
