import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";

import { ApplicationSetupService } from "./app-setup.service";

/**
 * Displays a notifications queue as consecutive snackbars
 */
@Injectable()
export class NotificationsService {

    /** FIFO queue for notifications */
    private notifications: any[];

    private isOpen: boolean;

    public constructor(
        private snackBar: MatSnackBar,
        private setupService: ApplicationSetupService
    ) {
        this.notifications = [];
        this.isOpen = false;
    }

    /** Push a notification and display it as soon as possible */
    public notify(message: string, duration: number = 2000, action: string = "OK") {
        if (this.setupService.enableNotifications) {
            this.notifications.push({
                message: message,
                duration: duration,
                action: action
            });
            this.show();
        }
    }

    /** Show all messages in the FIFO queue one after another */
    public show() {
        if (! this.isOpen) {
            // process next notification
            if (this.notifications.length > 0) {
                const notif = this.notifications.shift();
                this.isOpen = true;
                const ref = this.snackBar.open(notif.message, notif.action, {
                    duration: notif.duration
                });
                ref.afterDismissed().subscribe(() => {
                    this.isOpen = false;
                    this.show();
                });
            }
        }
    }
}
