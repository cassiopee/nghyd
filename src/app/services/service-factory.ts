import { ApplicationSetupService } from "./app-setup.service";
import { FormulaireService } from "./formulaire.service";
import { I18nService } from "./internationalisation.service";
import { HttpService } from "./http.service";
import { NotificationsService } from "./notifications.service";
import { PrebarrageService } from "./prebarrage.service";
import { ServiceWorkerUpdateService } from "./service-worker-update.service";

/**
 * A "Singleton" the TS way, that holds pointers to all services, to be accessed
 * at any time by classes that do not support injection; fed by AppComponent at
 * construction time
 */
export const ServiceFactory: {
    applicationSetupService: ApplicationSetupService;
    formulaireService: FormulaireService;
    i18nService: I18nService;
    httpService: HttpService;
    notificationsService: NotificationsService;
    prebarrageService: PrebarrageService;
    serviceWorkerUpdateService: ServiceWorkerUpdateService;
} = {
    applicationSetupService: undefined,
    formulaireService: undefined,
    i18nService: undefined,
    httpService: undefined,
    notificationsService: undefined,
    prebarrageService: undefined,
    serviceWorkerUpdateService: undefined
};
