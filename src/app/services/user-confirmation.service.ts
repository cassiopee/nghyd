import { Injectable } from "@angular/core";

import { BidirectionalSubject } from "app/util/bidir_subject";
import { Observer } from "rxjs";

/**
 * This service enables any class (even another service) to display a confirmation dialog on GUI ans get the user answer
 */
@Injectable()
export class UserConfirmationService {

    // used to communicate with UI component in charge of displaying confirmation dialog
    // direction 0 : this
    // direction 1 : UI
    private _userConfirm: BidirectionalSubject<{}>;

    public constructor() {
        this._userConfirm = new BidirectionalSubject<{}>();

        // we choose communication canal 0, UI will use 1
        this._userConfirm.selectPostingChannel(this, 0);
    }

    /**
     * add subscription from UI
     * @param source 
     */
    public subscribe(source: any) {
        this._userConfirm.selectPostingChannel(source, 1);
    }

    /**
     * remove UI subscription
     * @param source 
     */
    public unsubscribe(source: any) {
        this._userConfirm.unselectPostingChannel(source);
    }

    /**
     * add a handler (provided bu UI) to confirmation request
     * @param source normally, UI component
     * @param obs processing function
     */
    public addHandler(source: any, obs: Observer<boolean>) {
        this._userConfirm.addHandler(source, obs)
    }

    /**
     * forward user confirmation from UI to requesting object
     * @param confirm user confirmation status
     */
    public postConfirmation(source: any, confirm: {}) {
        this._userConfirm.post(source, confirm);
    }

    /**
     * forward to UI a request from source to ask a user confirmation with a dialog
     * @param source object requesting confirmation
     * @param title confirmation dialog title
     * @param text confirmation dialog body text
     * @returns a Promise resolving to a boolean holding user confirmation status
     */
    public askUserConfirmation(title: string, text: string): Promise<{}> {
        const ret = this._userConfirm.getReceivePromise(this);
        this._userConfirm.post(this, { title: title, body: text }); // false or true, we don't care
        return ret;
    }
}
