import { EventEmitter, Injectable } from "@angular/core";
import { MermaidUtil, PbBassin, PbBassinParams, PbCloison, PreBarrage } from "jalhyd";

export enum PrebarrageServiceEvents {
    BASIN_WALL_ADDED, // a wall has been added
    BASIN_WALL_COPIED, // a basin or wall has been copied
    BASIN_WALL_REMOVED, // a basin or a wall has been removed
    MOVE_BASIN, // a basin has been moved up or down
}

/**
 * service relatif au schéma de prébarrage
 */
@Injectable()
export class PrebarrageService {
    /** predam data */
    private _model: PreBarrage;

    public readonly upstreamId = "amont";

    public readonly downstreamId = "aval";

    /** Latest clicked item: a PbCloison, a PbBassin or undefined if river "Upstream" or "Downstream" was clicked */
    private _selectedNub: PbCloison | PbBassin;

    private _changeEventEmitter = new EventEmitter();

    public get model(): PreBarrage {
        return this._model;
    }

    public set model(pb: PreBarrage) {
        this._model = pb;
    }

    public get changeEventEmitter() {
        return this._changeEventEmitter;
    }

    /**
     * return upstream (and downstream) basin object
     */
    public get upstreamBassin(): PbBassin {
        return (this._model as unknown) as PbBassin;
    }

    public get selectedNub(): PbBassin | PbCloison {
        return this._selectedNub;
    }

    /**
     * set selected nub in schema
     */
    public setSelectedNub(n: PbBassin | PbCloison): PbBassin | PbCloison {
        this._selectedNub = n;
        return n;
    }

    public deleteSelected(emptyFields: boolean) {
        this.deleteByIndex(this._selectedNub.findPositionInParent(), emptyFields);
    }

    /**
     * remove nth basin
     * @param bi basin index
     */
    public deleteBasinByIndex(bi: number, emptyFields: boolean) {
        const i = this.nthBasinIndex(bi);
        this.deleteByIndex(i, emptyFields);
    }

    private deleteByIndex(i: number, emptyFields: boolean) {
        this._model.deleteChild(i);
        // never let an unconnected basin ! (not done in model to prevent unwanted
        // automatic child addition when clearing children)
        if (this._selectedNub instanceof PbCloison) {
            // if no downstream connections remain, connect to river downstream
            if (this._selectedNub.bassinAmont?.cloisonsAval.length === 0) {
                this._model.addChild(new PbCloison(this._selectedNub.bassinAmont, undefined, undefined, emptyFields));
            }
            // if no upstream connections remain, connect to river upstream
            if (this._selectedNub.bassinAval?.cloisonsAmont.length === 0) {
                this._model.addChild(new PbCloison(undefined, this._selectedNub.bassinAval, undefined, emptyFields));
            }
        }
        this._changeEventEmitter.emit(
            {
                id: PrebarrageServiceEvents.BASIN_WALL_REMOVED,
            }
        );
    }

    /**
     * compute index of nth basin
     * @param bi nth basin
     * @returns index in child list
     */
    private nthBasinIndex(bi): number {
        let nth: number = 0;
        let res: number = 0;
        for (const kid of this._model.children) {
            if (kid instanceof PbBassin) {
                if (nth == bi) {
                    return res;
                }
                nth++;
            }
            res++;
        }
        return -1;
    }

    private copyWall(wall: PbCloison, emptyFields: boolean): PbCloison {
        const wallCopy = new PbCloison(wall.bassinAmont, wall.bassinAval, undefined, emptyFields);
        wallCopy.loadObjectRepresentation(wall.objectRepresentation());
        this._model.addChild(wallCopy);
        return wallCopy;
    }

    public copySelectedWall(emptyFields: boolean) {
        const wall = this._selectedNub as PbCloison;
        const wallCopy = new PbCloison(wall.bassinAmont, wall.bassinAval, undefined, emptyFields);
        wallCopy.loadObjectRepresentation(wall.objectRepresentation());
        this._model.addChild(wallCopy);
        this._changeEventEmitter.emit(
            {
                id: PrebarrageServiceEvents.BASIN_WALL_COPIED,
                data: wallCopy
            }
        );
    }

    public addBasin(emptyFields: boolean) {
        const newBasin = new PbBassin(new PbBassinParams(20, 99, emptyFields));
        this._model.addChild(newBasin);
        this._changeEventEmitter.emit(
            {
                id: PrebarrageServiceEvents.BASIN_WALL_ADDED,
                data: newBasin
            }
        );
    }

    public copySelectedBasin(emptyFields: boolean) {
        this.copyBasin(this._selectedNub as PbBassin, emptyFields);
    }

    /**
     * copy nth basin
     * @param bi basin index
     */
    public copyBasinByIndex(bi: number, emptyFields: boolean) {
        const i = this.nthBasinIndex(bi);
        this.copyBasin(this._model.children[i] as PbBassin, emptyFields);
    }

    private copyBasin(basin: PbBassin, emptyFields: boolean) {
        const basinCopy = new PbBassin(new PbBassinParams(20, 99, emptyFields));
        basinCopy.loadObjectRepresentation(basin.objectRepresentation());
        this._model.addChild(basinCopy);
        this._changeEventEmitter.emit(
            {
                id: PrebarrageServiceEvents.BASIN_WALL_COPIED,
                data: basinCopy
            }
        );
    }

    public get hasBasins(): boolean {
        return this._model.bassins.length > 0;
    }

    public get bassins(): PbBassin[] {
        return this._model.bassins;
    }

    public addWall(upstreamIndex: number, downstreamIndex: number, emptyFields: boolean) {
        const wall = new PbCloison(
            upstreamIndex === 0 ? undefined : this._model.bassins[upstreamIndex - 1],
            downstreamIndex === 0 ? undefined : this._model.bassins[downstreamIndex - 1],
            undefined,
            emptyFields
        );
        this._model.addChild(wall);
        this._changeEventEmitter.emit(
            {
                id: PrebarrageServiceEvents.BASIN_WALL_ADDED,
                data: wall
            }
        );
    }

    /**
     * Returns true if given basin is either connected to nothing, or only to
     * river upstream or downstream
     */
    public isStandaloneBasin(basin: PbBassin) {
        return (
            (
                basin.cloisonsAmont.length === 0
                || basin.cloisonsAmont.map(c => c.bassinAmont).every(e => e === undefined)
            ) && (
                basin.cloisonsAval.length === 0
                || basin.cloisonsAval.map(c => c.bassinAval).every(e => e === undefined)
            )
        );
    }

    /**
     * @param uid nub uid
     */
    public findBasinPosition(uid: string): number {
        return this._model.findBasinPosition(uid);
    }

    /**
     * @param uid nub uid
     */
    private moveBasinUp(uid: string) {
        this._model.moveBasin(uid, this._model.findBasinPosition(uid) - 1);
    }

    public moveSelectedBasinUp() {
        const uid = this._selectedNub.uid;
        this.moveBasinUp(uid);
        this._changeEventEmitter.emit(
            {
                id: PrebarrageServiceEvents.MOVE_BASIN,
                data: this._selectedNub
            }
        );
    }

    /**
     * @param uid nub uid
     */
    private moveBasinDown(uid: string) {
        this._model.moveBasin(uid, this._model.findBasinPosition(uid) + 1);
    }

    public moveSelectedBasinDown() {
        const uid = this._selectedNub.uid;
        this.moveBasinDown(uid);
        this._changeEventEmitter.emit(
            {
                id: PrebarrageServiceEvents.MOVE_BASIN,
                data: this._selectedNub
            }
        );
    }

    /**
     * @param uid nub uid
     */
    public isLastBasin(uid: string): boolean {
        return this._model.findBasinPosition(uid) === this._model.bassins.length - 1
    }

    public isValid(): boolean {
        return this._model.hasUpDownConnection() && !this._model.hasBasinNotConnected();
    }

    /**
     * @param uid nub uid
     */
    public findChild(uid: string): PbBassin | PbCloison {
        return this._model.findChild(uid);
    }

    /**
     * turn 'aval', 'amont' and Mermaid ids to nub real uid
     */
    public toNubUid(itemId: string): string {
        if (itemId !== undefined && itemId !== null) {
            if (this.matchMermaidIds([this.upstreamId, this.downstreamId], itemId)) {
                return this.model.uid;
            }
            if (itemId.startsWith("flowchart-")) { // Mermaid style id (ie. flowchart-id-xx)
                return itemId.split('-')[1];
            }
        }
        return itemId;
    }

    /**
     * check if a list of (Mermaid transformed) ids matches a given id
     * @param ids ids to transform the Mermaid way
     * @param itemId id to find
     */
    private matchMermaidIds(ids: string[], itemId: string): boolean {
        return ids.find(id => MermaidUtil.isMermaidEqualIds(id, itemId)) !== undefined;
    }

    /**
     * @param itemId Mermaid id to find
     * @returns 
     */
    public findFromItemId(itemId: string) {
        if (this.matchMermaidIds([this.upstreamId, this.downstreamId], itemId)) {
            return this.upstreamBassin;
        } else {
            return this.model.findChild(itemId);
        }
    }
}
