import { Injectable, NgZone } from "@angular/core";
import { SwUpdate } from '@angular/service-worker';
import { I18nService } from "./internationalisation.service";
import { NotificationsService } from "./notifications.service";
import { UserConfirmationService } from "./user-confirmation.service";
import { interval } from "rxjs";

@Injectable({providedIn: 'root'})
export class ServiceWorkerUpdateService {
    constructor(
        private swUpdate: SwUpdate,
        private notificationService: NotificationsService,
        private i18nService: I18nService,
        private userConfirmationService: UserConfirmationService,
        private ngZone: NgZone
    ) {
        if (this.swUpdate.isEnabled) {
            this.ngZone.runOutsideAngular(() =>
                interval(1000 * 60 * 60).subscribe(val => {
                    console.log('ServiceWorkerUpdateService: checking for updates...')
                    swUpdate.checkForUpdate().then(b => {
                        console.log("ServiceWorkerUpdateService: " + (b ? "new version found" : "no new version"));
                    });
                })
            );
        } else {
            console.log("ServiceWorkerUpdateService: SwUpdate is disabled");
        }

        this.swUpdate.versionUpdates.subscribe(evt => {
            console.log("ServiceWorkerUpdateService event:", evt.type);
            switch (evt.type) {
                case 'VERSION_DETECTED':
                    let ver = (evt as any).version?.appData?.version ?? "<NA>";
                    console.log("ServiceWorkerUpdateService: VERSION_DETECTED", ver);
                    this.notificationService.notify(this.i18nService.localizeText("INFO_SERVICE_WORKER_VERSION_DETECTED", { "ver": ver }), 10000);
                    break;

                case 'VERSION_READY':
                    const currVer = (evt as any).currentVersion?.appData?.version ?? "<NA>";
                    const newVer = (evt as any).latestVersion?.appData?.version ?? "<NA>";
                    console.log("ServiceWorkerUpdateService: VERSION_READY", currVer, "->", newVer);

                    this.notificationService.notify(this.i18nService.localizeText("INFO_SERVICE_WORKER_VERSION_READY", { "ver": newVer }), 10000);
                    // PLANTE si on stocke le message dans une variable !!!!
                    // const msg = i18nService.localizeText("INFO_SERVICE_WORKER_VERSION_READY", { "ver": newVer });
                    // notificationService.notify(msg, 10000);
                    // -> ReferenceError: can't access lexical declaration 'xxx' before initialization
                    // avec xxx qui varie d'une fois à l'autre !!!

                    this.userConfirmationService.askUserConfirmation("Confirmation",
                        this.i18nService.localizeText("INFO_SERVICE_WORKER_VERSION_READY", { "ver": newVer })).then(data => {
                            if (data["confirm"]) {
                                console.log("ServiceWorkerUpdateService: application update confirmed");
                                window.location.reload();
                            }
                            else {
                                console.log("ServiceWorkerUpdateService: application update canceled");
                            }
                        });
                    break;

                case 'VERSION_INSTALLATION_FAILED':
                    console.log(`Failed to install app version '${evt.version.hash}': ${evt.error}`);
                    this.notificationService.notify(this.i18nService.localizeText("ERROR_SERVICE_WORKER_INSTALL_FAILED"), 10000);
                    break;
            }
        });
        swUpdate.unrecoverable.subscribe(event => {
            console.log("SwUpdate.unrecoverable reason", event.reason, "type", event.type);
            this.notificationService.notify("SwUpdate: unrecoverable state. Reason=" + event.reason + ", type=" + event.type, 10000);
        });
    }
}
