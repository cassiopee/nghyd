import { MultiDimensionResults } from "./multidimension-results";

import { Result } from "jalhyd";

export class VerificateurResults extends MultiDimensionResults {

    /** résultats des modules Espece */
    public especeResults: Result[];

    public constructor() {
        super();
        this.reset();
    }

    public reset() {
        super.reset();
        this.especeResults = [];
    }
}
