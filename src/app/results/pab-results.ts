import { Result, ParamDefinition } from "jalhyd";

import { ServiceFactory } from "../services/service-factory";
import { MultiDimensionResults } from "./multidimension-results";
import { PlottableData } from "./plottable-data";
import { ChartType } from "./chart-type";

export class PabResults extends MultiDimensionResults implements PlottableData {

    /** résultats des modules Cloisons avant chaque bassin */
    public cloisonsResults: Result[];

    /** résultats du module ParallelStructure pour la cloison aval */
    public cloisonAvalResults: Result;

    /**
     * valeurs de la cote aval de l'ensemble de la passe, pour chaque
     * itération : si aucun paramètre ne varie, ne contient qu'un élément
     * */
    public Z2: number[];

    /** symboles des colonnes de résultat */
    protected _columns: string[];

    public chartType: ChartType = ChartType.Scatter;
    public chartX: string;
    public chartY: string;

    public constructor() {
        super();
        this.reset();
        // standard headers
        this._columns = [
            "CLOISON",
            "Z",
            "ZRAM",
            "DH",
            "Q",
            "PV",
            "YMOY",
            "ZRMB",
            "QA",
            "ENUM_StructureJetType",
            "H1",
            "H2",
            "SUBMERGENCE"
        ];
        // axes par défaut
        this.chartX = this.chartX || "CLOISON";
        this.chartY = this.chartY || "YMOY";
    }

    /** headers symbols */
    public get columns() {
        return this._columns;
    }

    /** translated headers texts */
    public get headers() {
        return this._columns.map((h) => {
            // calculator type for translation
            const sn = this.result.sourceNub;
            let ct = sn.calcType;
            if (sn.parent) {
                ct = sn.parent.calcType;
            }
            let label = ServiceFactory.formulaireService.expandVariableNameAndUnit(ct, h);
            label += this.getHelpLink(h);
            return label;
        });
    }

    public reset() {
        super.reset();
        this.cloisonsResults = [];
        this.cloisonAvalResults = undefined;
        this.Z2 = [];
    }

    /**
     * Returns true if at least one log message is present in the PAB result or any
     * of the children results
     */
    public get hasLog(): boolean {
        if (this.cloisonsResults) {
            for (const cr of this.cloisonsResults) {
                if (cr && cr.hasLog()) {
                    return true;
                }
            }
        }
        return (
            (this.result && this.result.hasLog())
            || (this.cloisonAvalResults && this.cloisonAvalResults.hasLog())
        );
    }

    // do not test result.ok else log messages will prevent partial results from being displayed
    public get hasResults(): boolean {
        return this.result !== undefined && !this.result.hasOnlyErrors;
    }

    /** retourne true si au moins un calcul a échoué (le log a un code négatif) */
    public hasError(): boolean {
        let err = false;
        // log principal
        err = (err || this.result.hasErrorMessages());
        // logs des cloisons
        for (const c of this.cloisonsResults) {
            err = (err || c.hasErrorMessages());
        }
        // log de la cloison aval
        err = (err || this.cloisonAvalResults.hasErrorMessages());

        return err;
    }

    /** retourne true si le calcul à l'itération i a échoué */
    public iterationHasError(i: number): boolean {
        let err = this.result.resultElements[i].hasErrorMessages();
        // logs des cloisons
        for (const c of this.cloisonsResults) {
            err = (err || c.resultElements[i].hasErrorMessages());
        }
        // log de la cloison aval
        err = (err || this.cloisonAvalResults.resultElements[i].hasErrorMessages());

        return err;
    }

    /** retourne true si tous les calculs ont échoué */
    public hasOnlyErrors(): boolean {
        let err = true;
        // log principal
        err = (err && this.result.hasOnlyErrors);
        // logs des cloisons
        for (const c of this.cloisonsResults) {
            err = (err && c.hasOnlyErrors);
        }
        // log de la cloison aval
        err = (err && this.cloisonAvalResults.hasOnlyErrors);

        return err;
    }

    public hasPlottableResults(): boolean {
        return this.hasResults;
    }

    /**
     * Returns the label to display, for an element of getAvailableChartAxis()
     * @param symbol parameter / result symbol (ex: "Q")
     */
    public getChartAxisLabel(symbol: string): string {
        if (symbol === "x") { // specific case for wall abscissa
            return ServiceFactory.i18nService.localizeText("INFO_LIB_ABSCISSE_CLOISON");
        } else {
            return this.headers[this.columns.indexOf(symbol)];
        }
    }

    public expandLabelFromSymbol(p: ParamDefinition): string {
        return p.symbol;
    }

    /**
     * Returns a list of plottable parameters / result elements, that can be defined
     * as X or Y chart axis
     */
    public getAvailableChartAxis(): string[] {
        const axis = [];
        axis.push("x"); // wall abscissa
        for (const c of this.columns) {
            if (c.indexOf("ENUM_") === -1) { // ENUM variables are not plottable
                axis.push(c);
            }
        }
        return axis;
    }

    public getAvailableXAxis(): string[] {
        return this.getAvailableChartAxis();
    }

    public getAvailableYAxis(): string[] {
        return this.getAvailableChartAxis();
    }

    // just to implement interface
    public getVariatingParametersSymbols(): string[] {
        return [];
    }

    /**
     * Returns the series of values for the required symbol
     * @param symbol parameter / result symbol (ex: "Q")
     */
    public getValuesSeries(symbol: string): number[] {
        const data: number[] = [];
        const l = this.cloisonsResults.length;
        // when a parameter is variating, index of the variating parameter
        // values to build the data from
        const vi = this.variableIndex;

        if (this.iterationHasError(vi)) {
            return [];
        }

        switch (symbol) {
            case "CLOISON":
                data.push(undefined);
                for (let i = 0; i <= l; i++) { // <= for one extra step (downwall)
                    data.push(i + 1);
                }
                break;

            case "DH":
            case "ZRAM":
            case "Q":
                data.push(undefined);
                for (let i = 0; i < l; i++) {
                    const er = this.cloisonsResults[i].resultElements[vi].getValue(symbol);
                    data.push(er);
                }
                const zrAval = this.cloisonAvalResults.resultElements[vi].getValue(symbol);
                data.push(zrAval);
                break;

            case "Z":
                for (let i = 0; i < l; i++) {
                    data.push(this.cloisonsResults[i].resultElements[vi].vCalc);
                }
                data.push(this.cloisonAvalResults.resultElements[vi].vCalc);
                data.push(this.Z2[vi]);
                break;

            case "H1":
            case "H2":
            case "SUBMERGENCE":
                for (let i = 0; i < l; i++) {
                    data.push(this.cloisonsResults[i].resultElements[vi].vCalc);
                }
                data.push(this.cloisonAvalResults.resultElements[vi].vCalc);
                break;

            case "PV":
            case "YMOY":
            case "ZRMB":
            case "QA":
                data.push(undefined);
                for (let i = 0; i < l; i++) {
                    const er = this.cloisonsResults[i].resultElements[vi].getValue(symbol);
                    data.push(er);
                }
                data.push(undefined);
                break;

            case "x": // wall abscissa
                data.push(undefined);
                for (let i = 0; i < l; i++) {
                    const er = this.cloisonsResults[i].resultElements[vi].getValue(symbol);
                    data.push(er);
                }
                const erXdw = this.cloisonAvalResults.resultElements[vi].getValue(symbol);
                data.push(erXdw);
                break;
        }

        return data;
    }

    public get childrenResults(): Result[] {
        return this.cloisonsResults;
    }

    public get extraResult(): Result {
        return this.cloisonAvalResults;
    }
}
