import { ServiceFactory } from "app/services/service-factory";
import { cLog, Message, MessageCode, Result, ResultElement } from "jalhyd";
import { sprintf } from "sprintf-js";
import { MultiDimensionResults } from "./multidimension-results";

/**
 * calculation log helpers
 */
export class LogHelper {
    private _childrenResults: Result[];
    private _extraResult?: Result;

    constructor(private _mdr: MultiDimensionResults) {
        this._childrenResults = this._mdr.childrenResults;
        this._extraResult = this._mdr.extraResult;
    }

    private mergeGlobalLog(result: Result, log: cLog) {
        if (result) {
            if (result.hasGlobalLog()) {
                log.addLog(result.globalLog);
            }
            // if no parameter is varying, 1st element log is considered "global"
            if (this._mdr.variatedParameters.length === 0) {
                if (result.hasResultElements() && result.resultElement.hasLog()) {
                    log.addLog(result.log);
                }
            }
        }
    }

    /**
     * Returns the number of errors, warnings, infos among children logs
     */
    private logStats(): any {
        return this._mdr.result.sourceNub.resultElementsLogStats();
    }

    private computeResultElementLogStats(index: number): any {
        let res;
        const it: Iterator<ResultElement> = this._mdr.result.nthResultElementIterator(index);
        let ire = it.next();
        while (!ire.done) {
            res = ire.value.logStats(res);
            ire = it.next();
        }
        return res;
    }

    public static messagesStatsToString(res: any): string {
        let ret = "";
        function ccat(n: number, code: MessageCode, b: string): string {
            if (n > 0) {
                const c: string = MessageCode[code];
                const m: string = ServiceFactory.i18nService.localizeText(c);
                const s = sprintf(m, n);
                if (b.length > 0) {
                    return b + ", " + s;
                }
                return s;
            }
            return b;
        }
        ret = ccat(res.error, MessageCode.WARNING_ERROR_COUNT_SHORT, ret);
        ret = ccat(res.warning, MessageCode.WARNING_WARNING_COUNT_SHORT, ret);
        ret = ccat(res.info, MessageCode.WARNING_INFO_COUNT_SHORT, ret);
        return ret;
    }

    public resultElementsStats(index: number): string {
        const res = this.computeResultElementLogStats(index);
        return LogHelper.messagesStatsToString(res);
    }

    /*
     * Retourne les logs à afficher dans le composant de log global, au dessus
     * du sélecteur d'itération : messages globaux et / ou résumé des messages
     * spécifiques à chaque ResultElement
     */
    public get globalLog(): cLog {
        const l = new cLog();
        if (this._mdr && this._mdr.variatedParameters.length > 0) {
            this.mergeGlobalLog(this._mdr.result, l);
            // un problème avec le nub en général / les cloisons / les bassins, à une étape quelconque ?
            if (
                (this._mdr.hasLog)
                && l.messages.length === 0 // existing global messages make generic message below useless
            ) {
                const logStats = this.logStats();
                let m: Message;
                if (logStats.warning > 0) {
                    m = new Message(MessageCode.WARNING_WARNINGS_ABSTRACT);
                    m.extraVar.nb = "" + logStats.warning;
                    l.add(m);
                }
                if (logStats.error > 0) {
                    let c: MessageCode = logStats.error > 1 ? MessageCode.WARNING_ERRORS_ABSTRACT_PLUR : MessageCode.WARNING_ERRORS_ABSTRACT;
                    m = new Message(c);
                    m.extraVar.nb = "" + logStats.error;
                    l.add(m);
                }
            }
        } // sinon pas de log global (aucun paramètre ne varie)
        return l;
    }

    /**
     * Retourne les logs à afficher dans le composant de log local, en dessous
     * du sélecteur d'itération : messages concernant l'itération (le ResultElement)
     * en cours
     */
    public get iterationLog(): cLog {
        const l = new cLog();
        if (this._mdr?.result) {
            if (this._mdr.variatedParameters.length > 0) {
                // A. si un paramètre varie
                const vi = this._mdr.variableIndex;
                // log du PB pour l'itération en cours
                if (
                    this._mdr.result
                    && this._mdr.result.hasResultElements()
                    && this._mdr.result.resultElements[vi]
                    && this._mdr.result.resultElements[vi]?.hasLog()
                ) {
                    l.addLog(this._mdr.result.resultElements[vi].log);
                }
                // logs des enfants pour l'itération en cours
                for (const cr of this._childrenResults) {
                    if (cr.hasResultElements() && cr.resultElements[vi]?.hasLog()) {
                        l.addLog(cr.resultElements[vi].log);
                    }
                }
                // extra results
                if (this._extraResult?.resultElements[vi].hasLog()) {
                    // l.addLog(this._pabResults.cloisonAvalResults.resultElements[vi].log);
                    l.addLog(this._extraResult.resultElements[vi].log);
                }
            } else {
                // B. si aucun paramètre ne varie
                this.mergeGlobalLog(this._mdr.result, l); // faut bien mettre le log global quelque part
                // logs des enfants
                for (const cr of this._childrenResults) {
                    if (cr.hasResultElements() && cr.resultElement?.hasLog()) {
                        l.addLog(cr.resultElement.log);
                    }
                }
                // extra result
                if (this._extraResult?.resultElement.hasLog()) {
                    l.addLog(this._extraResult.resultElement.log);
                }
            }
        }
        return l;
    }
}
