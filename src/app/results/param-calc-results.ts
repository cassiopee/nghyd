import { CalculatorResults } from "./calculator-results";
import { NgParameter } from "../formulaire/elements/ngparam";
import { Result, cLog } from "jalhyd";

/**
 * classe gérant les résultats associés à un paramètre à calculer,
 * les autres étant tous fixés ou avec un paramètre varié
 */
export abstract class CalculatedParamResults extends CalculatorResults {

    /** paramètre calculé */
    protected _calculatedParam: NgParameter;

    /** titre de la colonne du paramètre calculé */
    private _calculatedParameterHeader: string;

    /** résultat du calcul sur le paramètre calculé */
    public result: Result;

    /** custom variables order for results tables  */
    public variablesOrder: string[] = [];

    public reset() {
        this._calculatedParam = undefined;
        this._calculatedParameterHeader = undefined;
        this.result = undefined;
    }

    public get calculatedParameter(): NgParameter {
        return this._calculatedParam;
    }

    public set calculatedParameter(p: NgParameter) {
        this._calculatedParam = p;
        this.updateCalculatedParameterHeader();
    }

    public updateCalculatedParameterHeader() {
        if (this._calculatedParam !== undefined) {
            this._calculatedParameterHeader = CalculatorResults.paramLabel(this._calculatedParam, true);
        }
    }

    public get calculatedParameterHeader(): string {
        return this._calculatedParameterHeader;
    }

    public get hasResults(): boolean {
        if (this.result === undefined) {
            return false;
        }
        return true;
    }

    public get hasLog(): boolean {
        if (this.result === undefined) {
            return false;
        }
        return this.result.hasLog();
    }

    public get log(): cLog {
        return this.result && this.result.log;  // return x === undefined ? undefined : x.y
    }
}
