import { IObservable, Observable, Observer, PreBarrage, Result } from "jalhyd";

import { ServiceFactory } from "../services/service-factory";
import { MultiDimensionResults } from "./multidimension-results";
import { PbCloisonResults } from "./pb-cloison-results";

export class PrebarrageResults extends MultiDimensionResults implements IObservable {

    /** résultats des bassins, dans l'ordre */
    public bassinsResults: Result[];

    /** résultat de la cloison actuellement sélectionnée (nourrit le FixedResultsComponent) */
    public cloisonResults: PbCloisonResults;

    /** symboles des colonnes de résultat */
    protected _columns: string[];

    /** size of the longest variating parameter */
    public size: number;

    /** Stores appropriate number suffix for a given wall uid (copied from PbSchema) */
    public wallsSuffixes: { [key: string]: number } = {};

    /** Implémentation par délégation */
    private _observable: Observable;

    /** résultats enfants */
    private _childrenResults: Result[];

    public constructor() {
        super();
        this._observable = new Observable();
        this.reset();
        this.size = 1; // boulette-proufe
        // standard headers
        this._columns = [
            "BASSIN",
            "S",
            "ZF",
            "Z",
            "PV",
            "YMOY",
            "Q"
        ];
    }

    /** headers symbols */
    public get columns() {
        return this._columns;
    }

    /** translated headers texts */
    public get headers() {
        return this._columns.map((h) => {
            // calculator type for translation
            const sn = this.result.sourceNub;
            let ct = sn.calcType;
            if (sn.parent) {
                ct = sn.parent.calcType;
            }
            let label = ServiceFactory.formulaireService.expandVariableNameAndUnit(ct , h);
            label += this.getHelpLink(h);
            return label;
        });
    }

    // redéfinir le set() implique de redéfinir le get(), sinon /i
    public get variableIndex(): number {
        return this._variableIndex;
    }

    public set variableIndex(v: number) {
        this._variableIndex = v;
        // set index in pseudo-fixed Cloison results too
        this.cloisonResults.variableIndex = v;
        // refresh schema
        this.notifyObservers({
            action: "updateSchema"
        }, this);
    }

    public reset() {
        super.reset();
        this.bassinsResults = [];
        this.cloisonResults = new PbCloisonResults();
        this.cloisonResults.variableIndex = this._variableIndex;
        this.cloisonResults.size = this.size;
        this.result = undefined;
        this._childrenResults = undefined;
    }

    /**
     * Returns true if at least one log message is present in the PB result or any
     * of the children results
     */
    public get hasLog(): boolean {
        if (this.result !== undefined) {
            if (this.result.hasLog()) {
                return true;
            }
            const pb = this.result.sourceNub as PreBarrage;
            for (const pbc of pb.children) {
                if (pbc && pbc.result.hasLog()) {
                    return true;
                }
            }
        }
        return false;
    }

    public get hasResults(): boolean {
        return this.hasBasinResults || this.hasWallResults;
    }

    public get hasBasinResults(): boolean {
        // test du bassin 0 @TODO faire mieux
        return this.bassinsResults.length > 0 && this.bassinsResults[0] && ! this.bassinsResults[0].hasOnlyErrors;
    }

    public get basinResultsHaveData(): boolean {
        return (
            this.bassinsResults.length > 0
            && this.bassinsResults[0] !== undefined
            && this.bassinsResults[0].resultElements[this.variableIndex] !== undefined
            && this.bassinsResults[0].resultElements[this.variableIndex].ok
        );
    }

    public get hasWallResults(): boolean {
        return this.cloisonResults?.result?.ok;
    }

    /** retourne true si au moins un calcul a échoué (le log a un code négatif) */
    public hasError(): boolean {
        let err = false;
        // log principal
        err = (err || (this.cloisonResults && this.cloisonResults && this.cloisonResults.result.hasErrorMessages()));
        // logs des bassins
        for (const c of this.bassinsResults) {
            err = (err || c.hasErrorMessages());
        }

        return err;
    }

    /** retourne true si le calcul à l'itération i a échoué */
    public iterationHasError(i: number): boolean {
        let err = this.cloisonResults && this.cloisonResults.result && this.cloisonResults.result.resultElements[i].hasErrorMessages();
        // logs des bassins
        for (const c of this.bassinsResults) {
            err = (err || c.resultElements[i].hasErrorMessages());
        }

        return err;
    }

    /** retourne true si tous les calculs ont échoué */
    public hasOnlyErrors(): boolean {
        let err = true;
        // log principal
        err = (err && this.cloisonResults && this.cloisonResults.result && this.cloisonResults.result.hasOnlyErrors);
        // logs des bassins
        for (const c of this.bassinsResults) {
            err = (err && c.hasOnlyErrors);
        }

        return err;
    }

    /** liste des résultats enfants */
    public get childrenResults(): Result[] {
        if (this._childrenResults === undefined) {
            this._childrenResults = [];
            const sn = this.result?.sourceNub as PreBarrage;
            if (sn?.children) {
                for (const cr of sn.children) {
                    if (cr.result) {
                        this._childrenResults.push(cr.result);
                    }
                }
            }
        }
        return this._childrenResults;
    }

    // interface IObservable

    /**
     * ajoute un observateur à la liste
     */
    public addObserver(o: Observer) {
        this._observable.addObserver(o);
    }

    /**
     * supprime un observateur de la liste
     */
    public removeObserver(o: Observer) {
        this._observable.removeObserver(o);
    }

    /**
     * notifie un événement aux observateurs
     */
    public notifyObservers(data: any, sender?: any) {
        this._observable.notifyObservers(data, sender);
    }
}
