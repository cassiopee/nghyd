/**
 * A minimalistic description of a plottable data series, for charts
 */
export interface IYSeries {
    /** points to plot */
    data: { x: number, y: number }[];
    /** text for the legend */
    label: string;
    /** line color */
    color: string;
    /** points colors, might be an array */
    pointBackgroundColor?: any;
    /** points styles (shape or image), might be an array */
    pointStyle?: any;
}
