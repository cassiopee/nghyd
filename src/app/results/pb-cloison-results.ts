import { FixedResults } from "./fixed-results";

/** Fixed results, that are not really fixed (trick for PreBarrage) */
export class PbCloisonResults extends FixedResults {

    /** index de la valeur du paramètre varié à afficher dans les résultats */
    public variableIndex = 0;

    /** size of the longest variating parameter */
    public size = 1;
}
