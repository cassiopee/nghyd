import { CalculatedParamResults } from "./param-calc-results";
import { ServiceFactory } from "../services/service-factory";
import { PlottableData } from "./plottable-data";
import { ChartType } from "./chart-type";
import { longestVarParam } from "../util/util";
import { FormulaireDefinition } from "../formulaire/definition/form-definition";

import { sprintf } from "sprintf-js";

import { ResultElement, ParamFamily, capitalize, Nub, VariatedDetails, ParamDefinition, ParamDomain, ParamDomainValue } from "jalhyd";

export class VarResults extends CalculatedParamResults implements PlottableData {
    /**
     * paramètres variés
     */
    private _variatedParams: VariatedDetails[] = [];

    /**
     * titre des colonnes des résultats variés
     */
    private _variableParamHeaders: string[];

    /**
     * clés des résultats
     */
    public resultKeys: string[];

    /**
     * entête des colonnes des résultats
     */
    private _resultHeaders: string[];

    /**
     * type de graphique
     */
    protected _graphType: ChartType = ChartType.Scatter;

    /** pointer to form that instantiated this object */
    protected _form: FormulaireDefinition;

    /**
     * variated parameter or result displayed as chart's X-axis
     */
    public chartX: string;
    /**
     * variated parameter or result displayed as chart's Y-axis
     */
    public chartY: string;

    /** size of the longest variated parameter */
    public size: number;

    /** index of the longest variated parameter */
    public longest: number;

    /**
     * tableau des ordonnées du graphique des résultats variés
     */
    private _yValues: number[] = [];

    constructor(form?: FormulaireDefinition) {
        super();
        this._form = form;
        this.reset();
    }

    public reset() {
        super.reset();
        this._variableParamHeaders = [];
        this._resultHeaders = [];
        this.resultKeys = [];
        this._yValues = [];
        this.longest = 0;
    }

    public get variatedParameters(): VariatedDetails[] {
        return this._variatedParams;
    }

    public set variatedParameters(p: VariatedDetails[]) {
        this._variatedParams = p;
    }

    public get variableParamHeaders() {
        return this._variableParamHeaders;
    }

    public get yValues() {
        return this._yValues;
    }

    public get resultElements(): ResultElement[] {
        return this.result.resultElements;
    }

    public get resultHeaders() {
        return this._resultHeaders;
    }

    public get chartType(): ChartType {
        return this._graphType;
    }

    public set chartType(gt: ChartType) {
        this._graphType = gt;
        this.resetDefaultAxisIfNeeded();
    }

    public hasPlottableResults(): boolean {
        if (this.result === undefined) {
            return false;
        }
        return ! this.result.hasOnlyErrors;
    }

    public getChartAxisLabel(symbol: string): string {
        if (this.result) {
            // 1. calculated param ?
            if (this.calculatedParameter && this.calculatedParameter.symbol === symbol) {
                return this.calculatedParameterHeader;
            }
            // 2. variated param ?
            for (let i = 0; i < this.variatedParameters.length; i++) {
                if (this._variatedParams[i].param.symbol === symbol) {
                    return this.variableParamHeaders[i];
                }
            }
            // 3. Result element / child result
            return this.expandLabelFromSymbol(new ParamDefinition(undefined, symbol, ParamDomainValue.ANY));
        }
    }

    /**
     * Returns the translated name of the given symbol (usually a result or child result) with
     * its unit, but without the symbol itself
     */
    public expandLabelFromSymbol(p: ParamDefinition): string {
        let ret = "";
        // calculator type for translation
        const sn = this.result.sourceNub;
        let ct = sn.calcType;
        if (sn.parent) {
            ct = sn.parent.calcType;
        }
        // detect children results
        const match = /^([0-9]+)_(.+)$/.exec(p.symbol);
        let symbol = p.symbol;
        if (match !== null) {
            const pos = +match[1];
            // only parent translation file is loaded; look for children translations in it // ct = sn.getChildren()[pos].calcType;
            symbol = match[2];
            const cn = capitalize(ServiceFactory.i18nService.childName(sn.getChildren()[0]));
            ret += sprintf(ServiceFactory.i18nService.localizeText("INFO_STUFF_N"), cn) + (pos + 1) + " : ";
        }
        ret += ServiceFactory.formulaireService.expandVariableNameAndUnit(ct, symbol);
        return ret;
    }

    /**
     * Returns the series of values for the required variated parameter / result element
     * @param symbol parameter / result symbol (ex: "Q", "0_Q"...)
     */
    public getValuesSeries(symbol: string): number[] {
        let found = false;
        const series: number[] = [];
        // detect children results
        const isChildResult = /^([0-9]+)_(.+)$/.exec(symbol);

        // 1. variated param ?
        for (let i = 0; i < this.variatedParameters.length; i++) {
            const vp = this._variatedParams[i];
            let isTheGoodChild = false;
            // are we looking for a child variated param ?
            if (isChildResult !== null) {
                const children = this.result.sourceNub.getChildren();
                const parameterNub = vp.param.parentNub;
                if (children.includes(parameterNub)) { // current var param is a child param !
                    const pos = parameterNub.findPositionInParent();
                    isTheGoodChild = (pos === +isChildResult[1] && vp.param.symbol === isChildResult[2]);
                }
            }
            // in any case
            if (isTheGoodChild || vp.param.symbol === symbol) {
                found = true;
                const iter = vp.param.getExtendedValuesIterator(this.size);
                for (const v of iter) {
                    series.push(v);
                }
            }
        }
        // 2. Result element ?
        if (! found) {
            for (const r of this.result.resultElements) { // re:ResultElement
                for (const k in r.values) {
                    if (k === symbol) {
                        found = true;
                        series.push(r.getValue(k));
                    }
                }
            }
        }
        // 3. Child result element ?
        if (! found) {
            if (isChildResult !== null) {
                found = true;
                const sn = this.result.sourceNub;
                const pos = +isChildResult[1];
                symbol = isChildResult[2];
                const child = sn.getChildren()[pos];
                for (const r of child.result.resultElements) {
                    series.push(r.getValue(symbol));
                }
            }
        }

        return series;
    }

    /**
     * Returns a list of plottable parameters / result elements, that can be defined
     * as X chart axis
     */
    public getAvailableXAxis(): string[] {
        let res: string[] = [];
        res = res.concat(this.getVariatingParametersSymbols());
        for (const erk of this.resultKeys) {
            if (erk.indexOf("ENUM_") === -1) { // ENUM variables are not plottable
                res.push(erk);
            }
        }
        // children results
        if (this.result) {
            const sn = this.result.sourceNub;
            for (const c of sn.getChildren()) {
                if (c.result) {
                    // using latest ResultElement; results count / types are supposed to be the same on every iteration
                    for (const k of c.result.resultElement.keys) {
                        if (k.indexOf("ENUM_") === -1) { // ENUM variables are not plottable
                            res.push(c.findPositionInParent() + "_" + k);
                        }
                    }
                }
            }
        }
        return res;
    }

    /**
     * Same as X axis, plus results families if chart type is Scatter
     * (for multi-series comparison)
     */
    public getAvailableYAxis(): string[] {
        const res: string[] = this.getAvailableXAxis();
        if (this._graphType === ChartType.Scatter) {
            // add families having more than 1 variable as plottable ordinates
            const families = this.extractFamilies();
            for (const f in families) {
                if (families[f].length > 1) {
                    res.push(f);
                }
            }
        }
        return res;
    }

    /**
     * Browses all parameters and results to produce a map of families => list of
     * symbols in this family
     */
    public extractFamilies(): { [key: string]: string[] } {
        const families: { [key: string]: string[] } = {};
        // variating parameters
        for (const v of this._variatedParams) {
            // exclude pseudo-family "ANY"
            const fam = v.param.family;
            if (fam !== undefined && fam !== ParamFamily.ANY) {
                const f = ParamFamily[fam];
                if (! (f in families)) {
                    families[f] = [];
                }
                families[f].push(v.param.symbol);
            }
        }
        // results
        for (const erk of this.resultKeys) {
            const fam = this.result.sourceNub.getFamily(erk);
            // exclude pseudo-family "ANY"
            if (fam !== undefined && fam !== ParamFamily.ANY) {
                const f = ParamFamily[fam];
                if (! (f in families)) {
                    families[f] = [];
                }
                families[f].push(erk);
            }
        }
        // children results
        if (this.result) {
            const sn = this.result.sourceNub;
            for (const c of sn.getChildren()) {
                if (c.result) {
                    for (const k of c.result.resultElement.keys) {
                        const fam = this.result.sourceNub.getFamily(k);
                        // exclude pseudo-family "ANY"
                        if (fam !== undefined && fam !== ParamFamily.ANY) {
                            const f = ParamFamily[fam];
                            if (! (f in families)) {
                                families[f] = [];
                            }
                            const pos = c.findPositionInParent();
                            families[f].push(pos + "_" + k);
                        }
                    }
                }
            }
        }
        return families;
    }

    /**
     * Returns the list of variating parameters
     * (used by tooltip functions)
     */
    public getVariatingParametersSymbols(): string[] {
        if (this.result && this.result.sourceNub) {
            return this._variatedParams.map(vp => this.getVariatingParameterSymbol(vp.param, this.result.sourceNub));
        } else {
            return [];
        }
    }

    public getVariatingParameterSymbol(vp: ParamDefinition, sourceNub: Nub): string {
        // detect if variated param is a children param
        const parameterNub = vp.parentNub;
        const children = sourceNub.getChildren();
        let symb = vp.symbol;
        if (children.includes(parameterNub)) {
            symb = parameterNub.findPositionInParent() + "_" + symb;
        }
        return symb;
    }

    public update() {
        // refresh param headers
        this._variableParamHeaders = this._variatedParams.map((v) => {
            let h = this.expandLabelFromSymbol(v.param);
            h += this.getHelpLink(v.param.symbol);
            return h;
        });

        // liste la plus longue
        const lvp = longestVarParam(this._variatedParams);
        this.size = lvp.size;
        this.longest = lvp.index;

        // result keys (extra or not) - some lines might miss some results, in case of an error;
        // use those keys to ensure all columns are filled
        if (this.resultKeys.length === 0) {
            if (this.result.symbol !== undefined) {
                this.resultKeys.push(this.result.symbol);
            }
            for (const re of this.result.resultElements) { // re:ResultElement
                for (const erk in re.values) {
                    if (!this.resultKeys.includes(erk)) {
                        this.resultKeys.push(erk);
                    }
                }
            }
        }

        // set axis selectors values the first time
        let defaultY = this.chartY;
        if (this.resultKeys.length > 0) {
            defaultY = this.resultKeys[0];
        }
        this.chartY = defaultY;
        if (this.chartX === undefined || ! this.getAvailableXAxis().includes(this.chartX)) {
            this.chartX = this.getVariatingParameterSymbol(this.variatedParameters[this.longest].param, this.result.sourceNub);
        }

        // calculator type for translation
        const sn = this.result.sourceNub;
        let ct = sn.calcType;
        if (sn.parent) {
            ct = sn.parent.calcType;
        }
        // entêtes des résultats
        this._resultHeaders = [];
        for (const k of this.resultKeys) {
            let unit;
            // is k the calculated parameter ? If so, extract its unit
            try {
                const p = sn.getParameter(k);
                if (p) {
                    unit = p.unit;
                }
            } catch (e) { /* silent fail */ }
            let rh = ServiceFactory.formulaireService.expandVariableNameAndUnit(ct, k, unit);
            rh += this.getHelpLink(k);
            this._resultHeaders.push(rh);
        }
        // entêtes des résultats des enfants
        for (const c of sn.getChildren()) {
            if (c.result) {
                const cn = capitalize(ServiceFactory.i18nService.childName(c));
                // using latest ResultElement; results count / types are supposed to be the same on every iteration
                for (const k of c.result.resultElement.keys) {
                    let rh = sprintf(ServiceFactory.i18nService.localizeText("INFO_STUFF_N"), cn)
                        + (c.findPositionInParent() + 1) + " : "
                        + ServiceFactory.formulaireService.expandVariableNameAndUnit(ct, k);
                    rh += this.getHelpLink(k);
                    this._resultHeaders.push(rh);
                }
            }
        }

        this.resetDefaultAxisIfNeeded();
    }

    /**
     * When variable parameter or chart type changes, ensure the X / Y current values are still available
     */
    public resetDefaultAxisIfNeeded() {
        if (this.variatedParameters.length > 0 && ! this.getAvailableXAxis().includes(this.chartX)) {
            this.chartX = this.variatedParameters[0].param.symbol;
        }
        if (this.variatedParameters.length > 0 && ! this.getAvailableYAxis().includes(this.chartY)) {
            this.chartY = this.variatedParameters[0].param.symbol;
        }
    }
}
