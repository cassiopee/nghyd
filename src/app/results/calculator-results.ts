import { CalculatorType, Nub, capitalize, MacrorugoCompound, Pab, Structure, PbCloison, PbBassin } from "jalhyd";

import { NgParameter } from "../formulaire/elements/ngparam";
import { ServiceFactory } from "../services/service-factory";

import { sprintf } from "sprintf-js";
import { MultiDimensionResults } from "./multidimension-results";
import { PrebarrageResults } from "./prebarrage-results";

export abstract class CalculatorResults {

    /** help links by result symbol, read from calculator config */
    public helpLinks: { [key: string]: string };

    /**
     * @param p parameter to generate label for
     * @param displaySymbol if true, will prefix label with parameter symbol (ex: "ZDV")
     * @param referenceNub if given, will detect if parameter belongs to a child of this reference Nub, and
     *                     if so will add a number prefix (ex: "0_") before the label
     */
    public static paramLabel(p: NgParameter, displaySymbol: boolean, referenceNub?: Nub): string {
        let res = "";
        // detect child parameter
        let isChildParam = false;
        if (referenceNub) {
            const children = referenceNub.getChildren();
            const parameterNub = p.paramDefinition.parentNub;
            if (children.includes(parameterNub)) {
                const cn = capitalize(ServiceFactory.i18nService.childName(parameterNub));
                isChildParam = true;
                const pos = parameterNub.findPositionInParent() + 1;
                res = sprintf(ServiceFactory.i18nService.localizeText("INFO_STUFF_N"), cn) + pos + " : ";
            }
        }
        if (displaySymbol && ! isChildParam) {
            res += p.symbol;
        }
        if (p.label !== undefined && p.label !== "") {
            if (displaySymbol && ! isChildParam) {
                res += " : ";
            }
            res += p.label;
        }
        if (p.unit !== undefined && p.unit !== "") {
            res += " (" + p.unit + ")";
        }
        return res;
    }

    /**
     * Returns a label showing the boundary conditions values of all variating parameters,
     * for the given iteration
     * @param varvalues array of values: one element per variating parameter, itself an array of
     *     values, one per iteration
     * @param variatedParameters array of variating parameters, in the same order as varvalues
     * @param n index of the variating parameter(s) iteration
     */
    public static variatingModalityLabel(varValues: any[], results: MultiDimensionResults, index: number): string {
        const kv = [];
        for (let i = 0; i < varValues.length; i++) {
            const vv = varValues[i];
            const vp = results.variatedParameters[i];
            let symbol = vp.param.symbol;
            // is vp a parameter of a child Nub ?
            if (
                vp.param.parentNub
                && vp.param.parentNub !== vp.param.originNub
            ) {
                let childPrefix: string;
                // prefix the label depending on (grand)children type
                switch (vp.param.originNub.calcType) {
                    case CalculatorType.PreBarrage:
                        const pbRes = results as PrebarrageResults;
                        if (vp.param.parentNub instanceof Structure) {
                            const struct = vp.param.parentNub as Structure;
                            const wall = struct.parent as PbCloison;
                            const posS = struct.findPositionInParent() + 1;
                            childPrefix = ServiceFactory.i18nService.localizeMessage(wall.description);
                            // there might be multiple walls between the same pair of basins
                            if (wall.uid in pbRes.wallsSuffixes) {
                                childPrefix += " (" + pbRes.wallsSuffixes[wall.uid] + ")";
                            }
                            childPrefix += "_" + ServiceFactory.i18nService.localizeText("INFO_LIB_STRUCTURE_N_COURT") + posS;
                        } else if (vp.param.parentNub instanceof PbBassin) {
                            const bassin = vp.param.parentNub as PbBassin;
                            childPrefix = ServiceFactory.i18nService.localizeMessage(bassin.description);
                        }
                        break;
                    case CalculatorType.MacroRugoCompound:
                        const posMR = vp.param.parentNub.findPositionInParent() + 1;
                        childPrefix = ServiceFactory.i18nService.localizeText("INFO_LIB_RADIER_N_COURT") + posMR;
                        break;
                    case CalculatorType.Pab:
                        const posPAB = vp.param.parentNub.findPositionInParent() + 1;
                        childPrefix = ServiceFactory.i18nService.localizeText("INFO_LIB_CLOISON_N_COURT") + posPAB;
                        break;
                }
                symbol = childPrefix + "_" + symbol;
            }
            kv.push(`${symbol} = ${vv[index]}`);
        }
        return kv.join(", ");
    }

    /**
     * remet tous les résultats à zero
     */
    protected abstract reset();

    /**
     * indique si il existe des résultats à afficher
     */
    public abstract get hasResults(): boolean;

    public getHelpLink(symbol: string): string {
        // add help link if any
        if (this.helpLinks !== undefined && this.helpLinks[symbol] !== undefined) {
            const helpURL = "assets/docs/" + ServiceFactory.applicationSetupService.language
                + "/calculators/" + this.helpLinks[symbol];
            // pseudo-<mat-icon> dirty trick because <mat-icon> renderer cannot be
            // triggered when code is set through innerHTML
            return `<a href="${helpURL}" target="_blank"><span class="material-icons mat-accent">help</span></a>`;
        } else {
            return "";
        }
    }
}
