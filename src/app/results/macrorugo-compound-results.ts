import { Result, ParamDefinition } from "jalhyd";

import { ServiceFactory } from "../services/service-factory";
import { MultiDimensionResults } from "./multidimension-results";
import { PlottableData } from "./plottable-data";
import { ChartType } from "./chart-type";

export class MacrorugoCompoundResults extends MultiDimensionResults implements PlottableData {

    /** résultats des modules MacroRugo enfants */
    private _childrenResults: Result[];

    /** symboles des colonnes de résultat */
    protected _columns: string[];

    public chartType: ChartType = ChartType.Scatter;
    public chartX: string;
    public chartY: string;

    public constructor() {
        super();
        this.reset();
        // standard headers
        this._columns = [
            "RADIER_N",
            "ZF1",
            "B",
            "Y",
            "Q",
            "Vdeb",
            "Fr",
            "Vg",
            "Vmax",
            "PV",
            "ENUM_MacroRugoFlowType",
            "xCenter"
        ];
        // axes par défaut
        this.chartX = this.chartX || "xCenter";
        this.chartY = this.chartY || "Q";
    }

    /** headers symbols */
    public get columns() {
        return this._columns;
    }

    /** translated headers texts */
    public get headers() {
        return this._columns.map((h) => {
            // calculator type for translation
            const sn = this.result.sourceNub;
            let ct = sn.calcType;
            if (sn.parent) {
                ct = sn.parent.calcType;
            }
            let unit: string;
            // is h a parameter ? If so, extract its unit
            try {
                const p = sn.getParameter(h);
                if (p) {
                    unit = p.unit;
                }
            } catch (e) { /* silent fail */ }
            let label = ServiceFactory.formulaireService.expandVariableNameAndUnit(ct , h, unit);
            label += this.getHelpLink(h);
            return label;
        });
    }

    public reset() {
        super.reset();
        this._childrenResults = [];
    }

    /**
     * Returns true if at least one log message is present in the PAB result or any
     * of the children results
     */
    public get hasLog(): boolean {
        if (this.childrenResults) {
            for (const cr of this.childrenResults) {
                if (cr && cr.hasLog()) {
                    return true;
                }
            }
        }
        return (this.result && this.result.hasLog());
    }

    // do not test result.ok else log messages will prevent partial results from being displayed
    public get hasResults(): boolean {
        return this.result !== undefined && ! this.result.hasOnlyErrors;
    }

    /** retourne true si au moins un calcul a échoué (le log a un code négatif) */
    public hasError(): boolean {
        let err = false;
        // log principal
        err = (err || this.result.hasErrorMessages());
        // logs des enfants
        for (const c of this.childrenResults) {
            err = (err || c.hasErrorMessages());
        }

        return err;
    }

    /** retourne true si le calcul à l'itération i a échoué */
    public iterationHasError(i: number): boolean {
        let err = this.result.resultElements[i].hasErrorMessages();
        // logs des cloisons
        for (const c of this.childrenResults) {
            err = (err || c.resultElements[i].hasErrorMessages());
        }

        return err;
    }

    /** retourne true si tous les calculs ont échoué */
    public hasOnlyErrors(): boolean {
        let err = true;
        // log principal
        err = (err && this.result.hasOnlyErrors);
        // logs des cloisons
        for (const c of this.childrenResults) {
            err = (err && c.hasOnlyErrors);
        }

        return err;
    }
    public hasPlottableResults(): boolean {
        return this.hasResults;
    }

    /**
     * Returns the label to display, for an element of getAvailableChartAxis()
     * @param symbol parameter / result symbol (ex: "Q")
     */
    public getChartAxisLabel(symbol: string): string {
        return this.headers[this.columns.indexOf(symbol)];
    }

    public expandLabelFromSymbol(p: ParamDefinition): string {
        return p.symbol;
    }

    /**
     * Returns a list of plottable parameters / result elements, that can be defined
     * as X or Y chart axis
     */
    public getAvailableChartAxis(): string[] {
        const axis = [];
        for (const c of this.columns) {
            if (c.indexOf("ENUM_") === -1) { // ENUM variables are not plottable
                axis.push(c);
            }
        }
        return axis;
    }

    public getAvailableXAxis(): string[] {
        return this.getAvailableChartAxis();
    }

    public getAvailableYAxis(): string[] {
        return this.getAvailableChartAxis();
    }

    // just to implement interface
    public getVariatingParametersSymbols(): string[] {
        return [];
    }

    /**
     * Returns the series of values for the required symbol
     * @param symbol parameter / result symbol (ex: "Q")
     */
    public getValuesSeries(symbol: string): number[] {
        const data: number[] = [];
        const l = this.childrenResults.length;
        // when a parameter is variating, index of the variating parameter
        // values to build the data from
        const vi = this.variableIndex;

        if (this.iterationHasError(vi)) {
            return [];
        }

        for (let i = 0; i < l; i++) {
            switch (symbol) {
                case "RADIER_N":
                    data.push(i + 1);
                    break;

                case "ZF1":
                case "Y":
                case "B":
                    let v: number;
                    const nub = this.childrenResults[i].sourceNub;
                    const param = nub.getParameter(symbol);
                    try {
                        if (param.hasMultipleValues) {
                            v = param.getInferredValuesList()[vi];
                        } else {
                            v = param.singleValue;
                        }
                    } catch (e) {
                        // silent fail
                    }
                    data.push(v);
                    break;

                case "Q":
                case "Vdeb":
                case "Fr":
                case "Vmax":
                case "PV":
                case "xCenter":
                case "Vg":
                    data.push(this.childrenResults[i].resultElements[vi].getValue(symbol));
                    break;
            }
        }

        return data;
    }

    public get childrenResults(): Result[] {
        return this._childrenResults;
    }

    public set childrenResults(cr: Result[]) {
        this._childrenResults = cr;
    }
}
