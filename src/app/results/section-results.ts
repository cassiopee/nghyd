import { acSection, Result } from "jalhyd";

import { CalculatorResults } from "./calculator-results";

export class SectionResults extends CalculatorResults {
    private _result: Result;

    private _section: acSection;

    constructor() {
        super();
        this.reset();
    }

    public reset() {
        this._section = undefined;
        this._result = undefined;
    }

    public get result(): Result {
        return this._result;
    }

    public set result(r: Result) {
        this._result = r;
    }

    public get section() {
        return this._section;
    }

    public set section(s: acSection) {
        this._section = s;
    }

    public get hasResults(): boolean {
        return this._section !== undefined && this._result !== undefined;
    }
}
