import { NgParameter } from "../formulaire/elements/ngparam";
import { CalculatedParamResults } from "./param-calc-results";

export class FixedResults extends CalculatedParamResults {
    private _fixedParameters: NgParameter[];

    constructor() {
        super();
        this.reset();
    }

    public reset() {
        super.reset();
        this._fixedParameters = [];
    }

    public addFixedParameter(p: NgParameter) {
        this._fixedParameters.push(p);
    }

    public get fixedParameters(): NgParameter[] {
        return this._fixedParameters;
    }

    public get hasFixedParameters() {
        return this._fixedParameters.length > 0;
    }
}
