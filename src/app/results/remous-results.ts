import { cLog, CourbeRemousParams, Result, ResultElement, ParamDefinition, ParamDomainValue, Nub } from "jalhyd";

import { CalculatorResults } from "./calculator-results";
import { VarResults } from "./var-results";
import { ServiceFactory } from "../services/service-factory";
import { FormulaireDefinition } from "../formulaire/definition/form-definition";

export class RemousResults extends CalculatorResults {

    private _result: Result;

    /** pseudo-paramètre varié contenant la liste des abscisses pour lesquelles on a des résultats */
    private _xValues: ParamDefinition;

    /** pas de discrétisation */
    private _Dx: number;

    /** cote de fond amont */
    private _ZF1: number;

    /** cote de fond aval */
    private _ZF2: number;

    /** longueur du bief */
    private _Long: number;

    /** hauteur de berge */
    private _hautBerge: number;

    /** pente du fond */
    private _penteFond: number;

    /** hauteur normale */
    private _hautNormale: ResultElement;

    /** hauteur critique */
    private _hautCritique: ResultElement;

    /** présence de données pour la courbe fluviale */
    private _hasFlu: boolean;

    /** présence de données pour la courbe torrentielle */
    private _hasTor: boolean;

    /** présence de données pour le paramètre supplémentaire */
    private _hasExtra: boolean;

    /** résultats variables pour l'affichage d'une table de résultats numériques */
    private _varResults: VarResults;

    /** le paramètre supplémentaire est affiché dans un graphique séparé */
    private _extraChart: boolean;

    /** titre de la colonne du paramètre supplémentaire */
    private _extraParamSymbol: string;

    /** journal de calcul */
    private _log: cLog;

    /** pointer to form that instantiated this object */
    protected _form: FormulaireDefinition;

    /**
     * liste des X,Y (fluvial ou torrentiel)
     */
    private _points: any[];

    constructor(form?: FormulaireDefinition) {
        super();
        this._form = form;
        this.reset();
    }

    public reset() {
        this._result = undefined;
        this._log = new cLog();
        this._hautBerge = undefined;
        this._penteFond = undefined;
        this._hautNormale = undefined;
        this._hautCritique = undefined;
        this._extraParamSymbol = undefined;
        this._hasFlu = false;
        this._hasTor = false;
        this._hasExtra = false;
        this._extraChart = false;
    }

    public set parameters(p: CourbeRemousParams) {
        const nub = p.parent as Nub;

        // pente du fond
        this._penteFond = nub.getParameter("If").singleValue;

        // hauteur de berge
        this._hautBerge = nub.getParameter("YB").singleValue;

        // longueur du bief
        this._Long = nub.getParameter("Long").singleValue;

        // pas d'espace
        this._Dx = nub.getParameter("Dx").singleValue;

        // série de valeurs de X
        this._xValues = new ParamDefinition(
            p,
            "ABSCISSE",
            ParamDomainValue.POS_NULL
        );

        // cote de fond amont
        this._ZF1 = nub.getParameter("ZF1").singleValue;

        // cote de fond aval
        this._ZF2 = nub.getParameter("ZF2").singleValue;
    }

    public get log(): cLog {
        return this._log;
    }

    public get varResults(): VarResults {
        return this._varResults;
    }

    public get result(): Result {
        return this._result;
    }

    public set result(r: Result) {
        this._result = r;
        // abscisses pour lesquelles on a des résultats
        const abscissae = r.resultElements.map((re) => {
            return re.getValue("X");
        });
        this._xValues.setValues(abscissae);

        this.updatePoints();
    }

    private updatePoints() {
        this._points = this._result.resultElements.map((re) => {
            const X = re.getValue("X");

            // cote de fond
            const k = X / this._Long;
            const Zf = (this._ZF2 - this._ZF1) * k + this._ZF1;

            // cote de l'eau
            let Z = re.getValue("flu");
            if (Z === undefined)
                Z = re.getValue("tor");

            // tirant d'eau
            const Y = Z - Zf;

            return { x: X, y: Y, z: Z };
        });
    }

    /*
     * points pour lesquels on a des résultats
     */
    public get points(): any[] {
        return this._points;
    }

    public update() {
        this._hasFlu = false;
        this._hasTor = false;
        this._hasExtra = false;

        for (const re of this._result.resultElements) {
            if (!this._hasFlu && re.getValue("flu")) {
                this._hasFlu = true;
            }
            if (!this._hasTor && re.getValue("tor")) {
                this._hasTor = true;
            }
            if (!this._hasExtra && re.getValue(this.extraParamSymbol)) {
                this._hasExtra = true;
            }

            if (this._hasFlu && this._hasTor && this._hasExtra )
                break; // micro optimisation : pas la peine de continuer à chercher
        }

        this._log.clear();
        this._log.addLog(this._result.globalLog);

        this._varResults = new VarResults(this._form);
        this._varResults.variatedParameters = [ { param: this._xValues, values: this._xValues.paramValues } ];
        this._varResults.result = this._result;
        const keys = [];
        keys.push("ZW"); // ligne d'eau
        keys.push("Y"); // tirant d'eau
        if (this._hasFlu) {
            keys.push("flu");
        }
        if (this._hasTor) {
            keys.push("tor");
        }
        if (this._hasExtra) {
            keys.push(this.extraParamSymbol);
        }
        this._varResults.resultKeys = keys;
        this._varResults.helpLinks = this.helpLinks;
        this._varResults.update();
    }

    public get extraParamSymbol(): string {
        return this._extraParamSymbol;
    }

    public set extraParamSymbol(l: string) {
        if (l !== "") {
            this._extraParamSymbol = l;
        }
    }

    public get Dx(): number {
        return this._Dx;
    }

    public get Long(): number {
        return this._Long;
    }


    public get hautBerge() {
        return this._hautBerge;
    }

    public get penteFond() {
        return this._penteFond;
    }

    public get hautNormale() {
        return this._hautNormale;
    }

    public set hauteurNormale(v: ResultElement) {
        this._hautNormale = v;
    }

    public get hautCritique() {
        return this._hautCritique;
    }

    public set hauteurCritique(v: ResultElement) {
        this._hautCritique = v;
    }

    public get hasExtra() {
        return this._hasExtra;
    }

    public get extraChart() {
        return this._extraChart;
    }

    public set extraChart(b: boolean) {
        this._extraChart = b;
    }

    public get hasResults(): boolean {
        return this._hautBerge !== undefined ||
            this._penteFond !== undefined ||
            this._hautNormale !== undefined ||
            this._hautCritique !== undefined ||
            (this._result && this._result.ok);
    }

    /**
     * @return true si il existe au moins une données dans la série fluviale
     */
    public get hasFluData(): boolean {
        return this._hasFlu;
    }

    /**
     * @return true si il existe au moins une données dans la série torrentielle
     */
    public get hasTorData(): boolean {
        return this._hasTor;
    }
}
