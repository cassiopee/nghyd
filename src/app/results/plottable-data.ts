import { ChartType } from "./chart-type";

import { ParamDefinition } from "jalhyd";

/**
 * Une interface pour nourrir un ResultsChartComponent
 */
export interface PlottableData {

    chartType: ChartType;
    chartX: string;
    chartY: string;

    /**
     * Returns the label to display, for an element of getAvailableChartAxis()
     * (usually a variable symbol like "Q", "Z1"…)
     * @param symbol parameter / result symbol (ex: "Q")
     */
    getChartAxisLabel(symbol: string): string;

    /**
     * Returns the translated name of the given symbol (usually a result or child result)
     * if available, with its unit, but without the symbol itself
     */
    expandLabelFromSymbol(p: ParamDefinition): string;

    /**
     * Returns a list of plottable parameters / result elements, that can be defined
     * as X chart axis
     */
    getAvailableXAxis(): string[];

    /**
     * Returns a list of plottable parameters / result elements / families,
     * that can be defined as Y chart axis
     */
    getAvailableYAxis(): string[];

    /**
     * Returns the series of values for the required variated parameter / result element
     * @param symbol parameter / result symbol (ex: "Q")
     */
    getValuesSeries(symbol: string): number[];

    /**
     * Returns the list of variating parameters
     * (used by tooltip functions)
     */
    getVariatingParametersSymbols(): string[];

    /**
     * Returns true if results contain data
     */
    hasPlottableResults(): boolean;
}
