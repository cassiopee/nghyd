import { CalculatedParamResults } from "./param-calc-results";

import { Result, VariatedDetails } from "jalhyd";

export class MultiDimensionResults extends CalculatedParamResults {

    /** paramètres variés */
    public variatedParameters: VariatedDetails[] = [];

    /** index de la valeur du paramètre varié à afficher dans les résultats */
    protected _variableIndex = 0;

    public get variableIndex(): number {
        return this._variableIndex;
    }

    /** redéfini par PrebarrageResults notamment */
    public set variableIndex(v: number) {
        this._variableIndex = v;
    }

    public get childrenResults(): Result[] {
        return [];
    }

    public get extraResult(): Result {
        return undefined;
    }
}
