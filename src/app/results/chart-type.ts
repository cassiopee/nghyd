/**
 * type de graphique
 */
export enum ChartType {
    /**
     * histogramme
     */
    Histogram,

    /**
     * points indépendants, X non triés
     */
    Dots,

    /**
     * graphique XY classique, avec X triés par ordre croissant
     */
    Scatter,
}
