import { DefinedValue } from "./definedvalue";

/**
 * boolean value with initialised, changed, defined states
 */
export class DefinedBoolean extends DefinedValue<boolean> {
}
