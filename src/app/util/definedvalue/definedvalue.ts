/**
 * value management with initialised, changed and defined states
 */
export abstract class DefinedValue<T> {
    private _initialised: boolean;

    private _value: T;

    private _changed: boolean;

    constructor() {
        this._initialised = false;
        this._changed = false;
    }

    /**
     * @returns true if setter has been called at least once
     */
    public get initialised(): boolean {
        return this._initialised;
    }

    /**
     * @returns true if value is not undefined
     */
    public get defined(): boolean {
        return this._value !== undefined;
    }

    /**
     * @returns true if value has been modified by last call to setter
     */
    public get changed(): boolean {
        return this._changed;
    }

    public get value(): T {
        return this._value;
    }

    public set value(v: T) {
        this._changed = this._value !== v;
        this._initialised = true;
        this._value = v;
    }
}
