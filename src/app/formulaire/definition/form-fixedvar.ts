import { FormulaireDefinition } from "./form-definition";
import { FixedResults } from "../../results/fixed-results";
import { VarResults } from "../../results/var-results";
import { ChartType } from "../../results/chart-type";
import { NgParameter } from "../elements/ngparam";
import { FieldSet } from "../elements/fieldset";
import { FormulaireNode } from "../elements/formulaire-node";

import { SelectField } from "../elements/select/select-field";
import { Nub, IObservable, VariatedDetails } from "jalhyd";

export class FormulaireFixedVar extends FormulaireDefinition {

    protected _fixedResults: FixedResults;
    protected _varResults: VarResults;

    constructor(parent?: FormulaireNode) {
        super(parent);
        this._fixedResults = new FixedResults();
        this._varResults = new VarResults(this);
        this.updateCalcResults();
    }

    public get fixedResults() {
        return this._fixedResults;
    }

    public resetFormResults() {
        this._fixedResults.reset();
        this._varResults.reset();
        this.updateCalcResults();
    }

    protected addFixedParameters() {
        for (const p of this.getFixedParameters()) {
            this._fixedResults.addFixedParameter(p);
        }
    }

    public set chartType(t: ChartType) {
        this._varResults.chartType = t;
        this.updateCalcResults();
    }

    public get hasResults(): boolean {
        return this._fixedResults.hasResults || this._varResults.hasResults;
    }

    /**
     * Recrée l'objet retourné par public get results() et
     * évite des mises à jour par Angular dues à un une détection
     * de changement causée par le fait que get results() recréait
     * systématiquement un CalculatorResults[]
     */
    protected updateCalcResults() {
        this._calcResults = [];
        // ensure help links are propagated
        this._fixedResults.helpLinks = this.helpLinks;
        this._varResults.helpLinks = this.helpLinks;
        this._calcResults.push(this._fixedResults);
        this._calcResults.push(this._varResults);
    }

    public afterParseFieldset(fs: FieldSet) {
        // observe all Select fields @see this.update()
        fs.addPropertiesObserver(this);
    }

    protected completeParse(firstNotif: boolean = true) {
        super.completeParse(firstNotif);
        // observe all CustomSelect fields @TODO move to afterParseFieldset ?
        for (const sel of this.allSelectFields) {  // Formulaire is listening to Select value
            if (!sel.hasAssociatedNubProperty) { // only to "custom" selects
                sel.addObserver(this);
                if (firstNotif) {
                    // force 1st observation
                    (sel as SelectField).notifyValueChanged();
                }
            }
        }
    }

    protected compute() {
        this.runNubCalc(this.currentNub);
        this.reaffectResultComponents();
    }

    protected reaffectResultComponents() {
        const nub: Nub = this.currentNub;
        const computedParam: NgParameter = this.getComputedParameter();
        this.resetFormResults(); // to avoid adding fixed parameters more than once (see below)
        this.addFixedParameters();
        const varParams: VariatedDetails[] = this.getVariatedParameters();

        if (varParams.length === 0) {
            // pas de paramètre à varier
            this._fixedResults.result = nub.result;
            if (computedParam !== undefined) {
                this._fixedResults.calculatedParameter = computedParam;
            }
        } else {
            // il y a un paramètre à varier
            this._varResults.variatedParameters = varParams;
            if (computedParam !== undefined) {
                this._varResults.calculatedParameter = computedParam;
            }

            this._varResults.result = nub.result;
            this._varResults.update();
        }
        this.updateCalcResults();
    }

    /**
     * Forces all fieldsets to update all their fields
     */
    public refreshFieldsets(forceClear: boolean) {
        for (const fs of this.allFieldsets) {
            fs.updateFields(forceClear);
        }
        this.completeParse(false); // re-add observers that were destroyed by updateFields()
    }

    // interface Observer

    public update(sender: IObservable, data: any) {
        super.update(sender, data);
        // whenever a select field is touched, reset results
        if (data.action === "propertyChange") {
            this.reset();
            // reflect changes in GUI (who knows ?), for ex. show / hide dependent fields
            this.refreshFieldsets(true);
        }
    }
}
