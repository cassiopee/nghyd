import { FormulaireFixedVar } from "./form-fixedvar";

/**
 * Formulaire pour les Grilles (perte de charge)
 */
export class FormulaireGrille extends FormulaireFixedVar {

    constructor() {
        super();
        // custom variables order for results tables
        this.fixedResults.variablesOrder = [
            "QMax", "CRad", "CEau", "H", "CSomGrille", "HG", "B", "S", "SPDG", "VA", "VAPDG",
            "Beta", "Alpha", "LG", "D", "DG", "SG", "VN",
            "b", "p", "e", "a", "c", "RFB", "REEB", "OB", "O", "OEntH", "cIncl",
            "DH00", "DH05", "DH10", "DH15", "DH20", "DH25", "DH30", "DH35", "DH40", "DH45", "DH50", "DH55", "DH60"
        ];
    }

}
