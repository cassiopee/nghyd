import { Pab, Result, VariatedDetails } from "jalhyd";

import { FormulaireDefinition } from "./form-definition";
import { PabResults } from "../../results/pab-results";
import { NgParameter } from "../elements/ngparam";
import { longestVarParam } from "../../util/util";
import { PabTable } from "../elements/pab-table";

/**
 * Formulaire pour les passes à bassins, inspiré du formulaire
 * pour les structures en parallèle
 */
export class FormulairePab extends FormulaireDefinition {

    protected _pabResults: PabResults;

    constructor() {
        super();
        this._pabResults = new PabResults();
        this.updateCalcResults();
    }

    public get pabNub(): Pab {
        return this.currentNub as Pab;
    }

    protected parseConfigType(type: string, confData: any): boolean {
        switch (type) {
            case "pab_table":
                const tab: PabTable = new PabTable(this);
                tab.parseConfig(confData);
                this.kids.push(tab);
                return true;
        }

        return false;
    }

    protected compute() {
        this.runNubCalc(this.currentNub);
        // reset variable index to avoid trying to access an index > 0 when nothing varies
        const pabr = this.pabResults;
        pabr.variableIndex = 0;

        this.reaffectResultComponents();
    }

    protected reaffectResultComponents() {
        const pab: Pab = (this.currentNub as Pab);
        const computedParam: NgParameter = this.getComputedParameter();
        const varParams: VariatedDetails[] = this.getVariatedParameters();

        // résultat de calcul de la passe à bassins
        this._pabResults.calculatedParameter = computedParam;
        this._pabResults.result = pab.result;

        // résultat de chaque cloison
        const cr: Result[] = [];
        for (const c of pab.children) {
            cr.push(c.result);
        }
        this._pabResults.cloisonsResults = cr;
        // résultat de la cloison aval
        this._pabResults.cloisonAvalResults = pab.downWall.result;

        // cote aval de la passe
        this._pabResults.Z2 = [];
        if (varParams.length > 0) {
            // find longest list
            const lvp = longestVarParam(varParams);
            const longest = lvp.size;
            // get extended values lists for Z2
            if (pab.prms.Z2.hasMultipleValues) {
                const iter = pab.prms.Z2.getExtendedValuesIterator(longest);
                while (iter.hasNext) {
                    const nv = iter.next();
                    this._pabResults.Z2.push(nv.value);
                }
            } else {
                for (let i = 0; i < longest; i++) {
                    this._pabResults.Z2.push(pab.prms.Z2.v);
                }
            }
        } else {
            this._pabResults.Z2 = [pab.prms.Z2.singleValue];
        }

        if (varParams) {
            this._pabResults.variatedParameters = varParams;
        }

        this.updateCalcResults();
    }

    public get pabResults() {
        return this._pabResults;
    }

    public resetFormResults() {
        this._pabResults.reset();
        this.updateCalcResults();
    }

    protected updateCalcResults() {
        this._calcResults = [];
        if (this._pabResults) {
            // ensure help links are propagated
            this._pabResults.helpLinks = this.helpLinks;
            this._calcResults.push(this._pabResults);
        }
    }

    public get hasResults(): boolean {
        return this._pabResults.hasResults;
    }
}
