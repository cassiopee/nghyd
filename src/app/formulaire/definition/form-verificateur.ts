import { IObservable, Nub, Verificateur, Result, VariatedDetails } from "jalhyd";

import { SelectField } from "../elements/select/select-field";
import { FormulaireFixedVar } from "./form-fixedvar";
import { VerificateurResults } from "../../results/verificateur-results";
import { ServiceFactory } from "../../services/service-factory";

/**
 * Formulaire pour les Vérificateurs
 */
export class FormulaireVerificateur extends FormulaireFixedVar {

    protected _verificateurResults: VerificateurResults;

    constructor() {
        super();
        this._verificateurResults = new VerificateurResults();
        this.updateCalcResults();
    }

    public get verificateurNub(): Verificateur {
        return this.currentNub as Verificateur;
    }

    public get verificateurResults() {
        return this._verificateurResults;
    }

    protected compute() {
        this.runNubCalc(this.currentNub);
        // reset variable index to avoid trying to access an index > 0 when nothing varies
        const mrcr = this.verificateurResults;
        mrcr.variableIndex = 0;

        this.reaffectResultComponents();
    }

    protected findPassVariatedParameters(): VariatedDetails[] {
        let pvp = [];
        const passUid = this.verificateurNub.nubToVerify.uid;
        const passForm = ServiceFactory.formulaireService.getFormulaireFromNubId(passUid);
        pvp = passForm.getVariatedParameters();
        return pvp;
    }

    protected reaffectResultComponents() {
        const ver: Verificateur = (this.currentNub as Verificateur);

        // résultat de calcul de la passe à macrorugo complexe
        const vr = this.verificateurResults;
        vr.result = ver.result;

        // paramètres qui varient, pour le sélecteur d'itération
        vr.variatedParameters = this.findPassVariatedParameters();

        // résultat de chaque Espece
        const er: Result[] = [];
        for (const sp of ver.species) {
            er.push(sp.result);
        }
        vr.especeResults = er;

        this.updateCalcResults();
    }
    
    public resetFormResults() {
        this._verificateurResults.reset();
        this.updateCalcResults();
    }

    protected updateCalcResults() {
        this._calcResults = [this._verificateurResults];
    }

    public get hasResults(): boolean {
        return this._verificateurResults.hasResults;
    }

    // interface Observer

    public update(sender: IObservable, data: any) {
        // copied from FormDefinition, to avoid calling super.update() that would trigger an unwanted this.refreshFieldsets();
        if (sender instanceof Nub) {
            switch (data.action) {
                case "resultUpdated":
                    // forward Nub results update notification to FormCompute objects
                    this.reaffectResultComponents();
                    break;
            }
        }
        // copied from FormFixedVar, to avoid calling super.update()
        if (data.action === "propertyChange") {
            this.reset();
        }

        if (sender instanceof SelectField) {
            this.reset(); // reset results
            if (sender.id === "select_target_pass" && data.action === "select") {
                // update Verificateur property: Pass to check
                this._currentNub.setPropValue("nubToVerify", data.value ? data.value.value : undefined);

            } else if (sender.id === "select_species_list" && data.action === "select") {
                // update Verificateur property: Species list (string[])
                (this._currentNub as Verificateur).speciesList = data.value.map((v: any) => v.value);
            }
        }
    }

}
