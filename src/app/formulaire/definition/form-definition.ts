import {
    CalculatorType,
    SectionType,
    Nub,
    Props,
    Observer,
    Session,
    ParamDefinition,
    Result,
    VariatedDetails,
    Prop_NullParameters,
    SessionSettings
} from "jalhyd";

import { FormulaireElement } from "../elements/formulaire-element";
import { NgParameter } from "../elements/ngparam";
import { Field } from "../elements/field";
import { FormulaireNode } from "../elements/formulaire-node";
import { FieldSet } from "../elements/fieldset";
import { FieldsetContainer } from "../elements/fieldset-container";
import { DeepFieldsetIterator } from "../form-iterator/deep-fieldset-iterator";
import { TopFormulaireElementIterator } from "../form-iterator/top-element-iterator";
import { CalculatorResults } from "../../results/calculator-results";
import { ServiceFactory } from "../../services/service-factory";
import { SelectEntry } from "../elements/select/select-entry";
import { SelectField } from "../elements/select/select-field";
import { DeepSelectFieldIterator } from "../form-iterator/deep-selectfield-iterator";
import { ConfigParser } from "./config-parser";

/**
 * classe de base pour tous les formulaires
 */
export abstract class FormulaireDefinition extends FormulaireNode implements Observer {

    /** nom du module de calcul */
    private _calculatorName: string;

    /** Nub courant */
    protected _currentNub: Nub;

    /** propriétés par défaut (lues si _currentNub === undefined ) */
    protected _props = {};

    /** aide en ligne pour les résultats */
    protected _resultsHelpLinks: { [key: string]: string };

    /** set to true to definitely gray-out Calculate button  */
    protected _calculateDisabled = false;

    /** fichier de configuration */
    protected _jsonConfig: {};

    /** copy of options.resultsHelp read by FormDefinition.parseOptions() */
    public helpLinks: { [key: string]: string };

    /*
    * Propriété destinée à éviter des MAJ intempestives par Angular : avant, le getter public get results()
    * recréait à chaque appel un tableau CalculatorResults[], ce qui était détecté par Angular
    * comme un changement même si les résultats stockés dans le tableau n'étaient pas modifiés.
    *  A présent, _calcResults n'est recréé que si les resultats sont modifiés
    *  (cf. updateCalcResults()).
    */
    protected _calcResults: CalculatorResults[];

    /**
     * Flag indiquant que le composant calculette est proche de la fin du 1er affichage (1er appel à AfterViewChecked)
     */
    private _firstDisplay: boolean = true;

    constructor(parent?: FormulaireNode) {
        super(parent);
        SessionSettings.instance.addObserver(this);
    }

    // surcharge de FormulaireNode::get:uid()
    // l'UID d'un formulaire est l'UID de son Nub !
    public get uid() {
        if (this._currentNub) {
            return this._currentNub.uid;
        } else {
            throw new Error("Aucun Nub associé au formulaire !");
        }
    }

    public get calculateDisabled(): boolean {
        return this._calculateDisabled;
    }

    public get isFirstDisplay(): boolean {
        return this._firstDisplay;
    }

    public setFirstDisplayCompleted() {
        this._firstDisplay = false;
    }

    public getPropValue(key: string): any {
        if (this._currentNub === undefined)
            return this.defaultProperties[key];
        return this._currentNub.getPropValue(key);
    }

    public get calculatorType(): CalculatorType {
        return this.getPropValue("calcType")
    }

    public get nodeType(): SectionType {
        return this.getPropValue("nodeType")
    }

    public get calculatorName() {
        return this._calculatorName;
    }

    public set calculatorName(name: string) {
        this._calculatorName = name;
        this.notifyObservers({
            "action": "nameChanged",
            "name": name
        }, this);
        // convenient trick to notify param-link components
        ServiceFactory.formulaireService.propagateFormNameChange(this, name);
    }

    public get jsonConfig(): {} {
        return this._jsonConfig;
    }

    public get defaultProperties() {
        return this._props;
    }

    public parseConfigToProps(): Props {
        const res: Props = new Props();

        const jp = new ConfigParser(this._jsonConfig);
        for (const fs of jp.forAll("fieldset")) {
            const fsp = new ConfigParser(fs["fields"]);
            for (const sel of fsp.forAll("select")) {
                const p = sel["property"];
                if (p !== undefined) { // if select has associated property
                    const v = sel["default"];
                    if (v !== undefined) { // if select has a default value for associated property
                        const enumClass = Props.enumFromProperty[p];
                        res.setPropValue(p, enumClass[v]);
                    }
                }
            }
        }

        return res;
    }

    /**
     * Creates a Nub from the given properties, and associates it to the current form
     */
    public initNub(props?: Props) {
        const p = props ? props : new Props(this.defaultProperties);
        p.setPropValue(Prop_NullParameters, ServiceFactory.applicationSetupService.enableEmptyFieldsOnFormInit); // transmit "empty fields" flag
        this.currentNub = Session.getInstance().createSessionNub(p);
    }

    public get currentNub(): Nub {
        return this._currentNub;
    }

    public set currentNub(n: Nub) {
        const nubCalcType = n.getPropValue("calcType");
        if (this._props["calcType"] !== nubCalcType) {
            throw new Error(
                `Nub ${n.properties["calcType"]} incompatible avec le formulaire ${this._calculatorName} (${this._props["calcType"]})`
            );
        }
        // unsubscribe from old Nub
        if (this._currentNub) {
            this._currentNub.removeObserver(this);
        }
        // replace Nub
        this._currentNub = n;
        // subscribe to new Nub (for result updates)
        this._currentNub.addObserver(this);
    }

    protected deleteNub(sn: Nub) {
        Session.getInstance().deleteNub(sn);
    }

    /**
     * parse calculator JSON configuration for select default value
     * @param selectId select id, ie. "id" field value
     * @return select "default" field value
     */
    protected parseSelectDefaultValue(json: {}, selectId: string): string {
        const jp = new ConfigParser(json);
        for (const fs of jp.forAll("fieldset")) {
            const fsp = new ConfigParser(fs["fields"]);
            for (const sel of fsp.forAll("select")) {
                if (sel["id"] === selectId) {
                    return sel["default"];
                }
            }
        }
        return undefined;
    }

    /**
     * prepare options parsing
     */
    protected preparseOptions() {
    }

    protected parseOptions(json: {}) {
        this.preparseOptions();

        this._helpLink = json["help"];
        this._resultsHelpLinks = json["resultsHelp"];
        if (json["calculateDisabled"] !== undefined) {
            this._calculateDisabled = json["calculateDisabled"];
        }
    }

    /** called at the end of parseConfig() */
    protected completeParse(firstNotif: boolean = true) {
        this.helpLinks = this._resultsHelpLinks;
    }

    public getOption(json: {}, option: string): any {
        if (json["type"] === "options") {
            return json[option];
        }
    }

    public afterParseFieldset(fs: FieldSet) { }

    public createFieldset(parent: FormulaireNode, json: {}, data?: {}, nub?: Nub): FieldSet {
        const res: FieldSet = new FieldSet(parent);
        if (nub !== undefined) {
            res.setNub(nub, false);
        } else {
            res.setNub(this._currentNub, false);
        }
        parent.kids.push(res);
        return res;
    }

    /**
     * @return true si le SessionNub attaché (ou un de ses enfants) a l'uid donné
     * @param uid id à rechercher
     */
    public hasNubId(uid: string): boolean {
        return (this._currentNub && this._currentNub.uid === uid);
    }

    public moveFieldsetUp(fs: FieldSet) {
    }

    public moveFieldsetDown(fs: FieldSet) {
    }

    public removeFieldset(fs: FieldSet) {
    }

    protected parse_fieldset(json: {}, nub?: Nub) {
        const fs = this.createFieldset(this, json, undefined, nub);
        fs.parseConfig(json);
        this.afterParseFieldset(fs);
    }

    protected parse_template_container(json: {}, templates: any[]) {
        const fsc: FieldsetContainer = new FieldsetContainer(this);
        fsc.parseConfig(json, templates);
        this.kids.push(fsc);
    }

    /**
     * 1ère passe d'analyse de la configuration
     */
    public preparseConfig(json: {}) {
        this._jsonConfig = json;

        this.preparseOptions();

        // analyse des options globales
        // il est utile de le faire avant le reste pour les modules de calcul utilisant
        // des sections (id des selects type de section/variable à calculer)

        for (const conf_index in json) {
            const conf = json[conf_index];
            const type: string = conf["type"];

            if (type === "options") {
                this.parseOptions(conf);
                break;
            }
        }
    }

    /**
     * parse specific type from configuration file
     * @param type object name
     * @param confData object data
     * @returns false if type is unknown
     */
    protected parseConfigType(type: string, confData: any): boolean {
        return false;
    }

    /**
     * 2ème passe d'analyse de la configuration
     */
    public parseConfig(json?: {}) {
        if (json !== undefined) {
            this._jsonConfig = json;
        }

        // analyse des éléments du formulaire
        const templates: any[] = [];

        for (const conf_index in this._jsonConfig) {
            const conf = this._jsonConfig[conf_index];
            const type: string = conf["type"];

            switch (type) {
                case "fieldset":
                    this.parse_fieldset(conf);
                    break;

                case "fieldset_template":
                    templates.push(conf);
                    break;

                // options globales
                case "options":
                    break;

                case "template_container":
                    this.parse_template_container(conf, templates);
                    break;

                default:
                    if (!this.parseConfigType(type, conf)) {
                        throw new Error(`type d'objet de module de calcul ${type} non pris en charge`);
                    }
            }
        }

        this.completeParse(false);
    }

    public hasParameter(symbol: string): boolean {
        for (const p of this.allFormElements) {
            if (p instanceof NgParameter) {
                if (p.symbol === symbol) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Trouve le NgParameter correspondant au paramètre Nub parmi tous les éléments du formulaire
     * @param param paramètre critère de recherche
     */
    private getNgparamFromNubParam(param: ParamDefinition): NgParameter {
        for (const p of this.allFormElements) {
            if (p instanceof NgParameter) {
                if (p.symbol === param.symbol && p.paramDefinition.parentNub.uid === param.parentNub.uid) {
                    return p;
                }
            }
        }
    }

    public getOrCreateParam(nubParam: ParamDefinition, parent: FormulaireNode): NgParameter {
        const res = this.getNgparamFromNubParam(nubParam);
        if (res === undefined) {
            return new NgParameter(nubParam, parent);
        }
        return res;
    }

    public getFieldById(id: string): Field {
        const res = this.getFormulaireNodeById(id);
        if (res instanceof Field) {
            return res;
        }
    }

    /**
     * Forwards Nub's progress updated notification.
     * Used by CalculatorComponent to update progress bar
     */
    protected notifyNubProgressUpdated(sender: any, data: any) {
        this.notifyObservers(data, sender);
    }

    /**
     * réinitialisation du formulaire suite à un changement d'une valeur, d'une option, ... :
     * effacement des résultats, application des dépendances, ...
     */
    public reset() {
        this.resetResults([], undefined, true);
    }

    /**
     * retourne la valeur actuellement sélectionnée d'un SelectField
     * @param selectFieldId id du SelectField
     * @returns valeur courante du select sans le préfixe
     */
    public getSelectedValue(selectFieldId: string): any {
        const select: SelectField = <SelectField>this.getFieldById(selectFieldId);
        if (select.multiple) {
            const values: string[] = (select.getValue() as SelectEntry[]).map((se) => se.value);
            return values;
        } else {
            return (select.getValue() as SelectEntry).value;
        }
    }

    /**
     * retourne le label actuellement sélectionnée d'un SelectField
     * @param selectFieldId id du SelectField
     * @returns label courant du select
     */
    public getSelectedLabel(selectFieldId: string): any {
        const select: SelectField = <SelectField>this.getFieldById(selectFieldId);
        if (select.multiple) {
            const labels: string[] = (select.getValue() as SelectEntry[]).map((se) => se.label);
            return labels;
        } else {
            return (select.getValue() as SelectEntry).label;
        }
    }

    public updateLocalisation(lang: string) {
        for (const fe of this.topFormElements) {
            fe.updateLocalisation();
        }
    }

    public isDisplayed(id: string) {
        const fe = <FormulaireElement>this.getFormulaireNodeById(id);
        return fe?.isDisplayed;
    }

    /**
     * gestion d'un clic sur les radios
     */
    public onRadioClick(info: any) {
        // if mode changed, reset form results
        if (info.oldValueMode !== info.param.valueMode) {
            this.reset();
        }
    }

    /**
     * itère sur tous les FieldSet
     */
    public get allFieldsets(): IterableIterator<FieldSet> {
        return new DeepFieldsetIterator(this);
    }

    /**
     * itère sur tous les FormulaireElement de 1er niveau
    */
    private get topFormElements(): IterableIterator<FormulaireElement> {
        return new TopFormulaireElementIterator(this);
    }

    /**
     * itère sur tous les SelectField
     */
    public get allSelectFields(): IterableIterator<SelectField> {
        return new DeepSelectFieldIterator(this);
    }

    //  interface Observer

    public update(sender: any, data: any) {
        if (sender instanceof Nub) {
            switch (data.action) {
                case "resultUpdated":
                    // forward Nub results update notification to FormCompute objects
                    this.reaffectResultComponents();
                    break;
            }
        }
        else if (sender instanceof SessionSettings) {
            // reset results if a session setting (max iterations, compute precision) has been modified
            this.reset();
        }
    }

    /**
     * Resets the form results, the results panel on screen, the model
     * results, and does the same for all depending modules
     * @param symbol symbol of the parameter whose value change triggered this method
     * @param forceResetAllDependencies if true, even non-calculated non-modified parameters
     *      links will be considered as dependencies @see jalhyd#98
     */
    public resetResults(visited: string[] = [], symbol?: string, forceResetAllDependencies: boolean = false) {
        // console.debug(`FormulaireDefinition.resetResults(${visited})`);
        visited.push(this.currentNub.uid);
        // reset GUI results
        this.resetFormResults();
        // reset model results
        this.currentNub.resetResult();
        // reset the result panels of all forms depending on this one
        ServiceFactory.formulaireService.resetAllDependingFormsResults(this, visited, symbol, forceResetAllDependencies);
    }

    protected abstract compute();

    public abstract get hasResults(): boolean;

    public get results(): CalculatorResults[] {
        return this._calcResults;
    }

    /**
     * Recrée l'objet retourné par public get results() et
     * évite des mises à jour par Angular dues à un une détection
     * de changement causée par le fait que get results() recréait
     * systématiquement un CalculatorResults[]
     */
    protected abstract updateCalcResults();

    /**
     * Copies current Nub result into result components for display on page.
     * Should be called every time the Nub result changes.
     * Must be idempotent.
     * Must call updateCalcResults() at the end.
     */
    protected abstract reaffectResultComponents();

    /**
     * retourne un identifiant du paramètre dans le formulaire
     * surchargé dans le cas des ouvrages //
     */
    protected getParameterRefid(p: ParamDefinition) {
        return p.symbol;
    }

    /**
     * Lance le calcul d'un paramètre en déterminant une valeur initiale.
     * Si nécessaire déclenche un calcul en chaîne des modules en amont.
     */
    protected runNubCalc(nub: Nub): Result {
        return nub.CalcSerie();
    }

    protected getComputedParameter(): NgParameter {
        const cpd = this.currentNub.calculatedParam;
        if (cpd !== undefined) {
            return this.getOrCreateParam(cpd, this);
        }
        return undefined;
    }

    /** find variated (possibly linked) parameters from model, and get their values at the same time */
    public getVariatedParameters(): VariatedDetails[] {
        return this._currentNub.findVariatedParams();
    }

    /** find fixed (possibly linked) parameters from the given Nub */
    public getFixedParameters(): NgParameter[] {
        const fixedParams: ParamDefinition[] = this._currentNub.findFixedParams();
        let fnp: NgParameter[] = [];
        for (const fp of fixedParams) {
            fnp.push(this.getNgparamFromNubParam(fp));
        }
        fnp = fnp.filter((e) => e !== undefined);
        return fnp;
    }

    /**
     * Triggers computation of the Nub, updates form results
     */
    public doCompute() {
        // calculate module
        this.compute();
    }

    public resetFormResults() { }
}
