import { FormulaireParallelStructure } from "./form-parallel-structures";

import { IObservable, Nub, PbCloison, PbBassin, Structure } from "jalhyd";
import { FieldSet } from "../elements/fieldset";
import { FormulaireNode } from "../elements/formulaire-node";
import { FieldsetContainer } from "../elements/fieldset-container";
import { FormulairePrebarrage } from "./form-prebarrage";
import { SelectField } from "../elements/select/select-field";
import { ServiceFactory } from "app/services/service-factory";

export class FormulairePbCloison extends FormulaireParallelStructure {

    // interface Observer

    public update(sender: IObservable, data: any) {
        // do NOT call super.update() or "newFieldset" action will reset results
        if (sender instanceof FieldsetContainer) {
            switch (data.action) {
                case "newFieldset":
                    // still reset all results when a fieldset if manually added
                    if (data.resetResults !== false) {
                        this.reset();
                    }
                    this.subscribeStructureInputFields(data["fieldset"]);
                    this.subscribeStructureSelectFields(data["fieldset"]);
            }
        } else if (sender instanceof FieldSet && data.action === "propertyChange") {
            switch (sender.id) {
                case "fs_ouvrage":
                    // ensure loiDebit is set
                    //props.setPropValue("loiDebit", data.value); // ?? et si la propriété modifiée n'est pas la loi de débit ?

                    this.adjustProperties(sender, data["name"], data["value"]);
                    // replace Structure Nub
                    const newNub = this.replaceNub(sender.nub as Structure);
                    sender.setNub(newNub);
                    // treat the fieldset as new to re-subscribe to Nub properties change events
                    this.afterParseFieldset(sender);
                    this.reset();
                    break;
            }
        }

        if (sender instanceof SelectField) {
            const nub = this._currentNub as PbCloison;
            const pb = nub.parent;
            const emptyFields: boolean = ServiceFactory.applicationSetupService.enableEmptyFieldsOnFormInit;
            // empty "" data.value.value should return undefined, which is good for amont/aval
            const newBasin = pb.findChild(data.value?.value) as PbBassin;
            if (sender.id === "select_upstream_basin") {
                // update only if upstream basin changed (prevents updating on unwanted events)
                if (newBasin?.uid !== nub.bassinAmont?.uid) {
                    // remove and recreate wall (easier for pointers consistency) but preserve UID
                    const uid = nub.uid;
                    const oldDownstreamBasin = nub.bassinAval;
                    pb.deleteChild(pb.findChildPosition(nub.uid));
                    const newWall = new PbCloison(newBasin, oldDownstreamBasin, undefined, emptyFields);
                    newWall.setUid(uid);
                    // copy structures
                    for (const s of nub.structures) {
                        newWall.addChild(s);
                    }
                    pb.addChild(newWall);
                    this.currentNub = newWall;
                    this.reset();
                }
            } else if (sender.id === "select_downstream_basin") {
                // update only if downstream basin changed (prevents updating on unwanted events)
                if (newBasin?.uid !== nub.bassinAval?.uid) {
                    // remove and recreate wall (easier for pointers consistency) but preserve UID
                    const uid = nub.uid;
                    const oldUpstreamBasin = nub.bassinAmont;
                    pb.deleteChild(pb.findChildPosition(nub.uid));
                    const newWall = new PbCloison(oldUpstreamBasin, newBasin, undefined, emptyFields);
                    newWall.setUid(uid);
                    // copy structures
                    for (const s of nub.structures) {
                        newWall.addChild(s);
                    }
                    pb.addChild(newWall);
                    this.currentNub = newWall;
                    this.reset();
                }
            }
            this.notifyObservers({
                action: "updateBasin",
                value: this._currentNub.uid // node to select on the schema
            }, this);
        }
    }

    // do not reset results after adding fieldset (when switching results view
    // from one wall to another, the form is rebuilt) @TODO anyway, we should
    // reset when a fieldset is manually added from "input" (form) view
    public createFieldset(parent: FormulaireNode, json: {}, data?: {}, nub?: Nub): FieldSet {
        if (json["calcType"] === "Structure") {
            // indice après lequel insérer le nouveau FieldSet
            const after = data["after"];

            const res: FieldSet = new FieldSet(parent);
            let sn: Nub;
            if (nub) { // use existing Nub (build interface based on model)
                sn = nub;
            } else {
                sn = this.createChildNub(data["template"]);
                this.currentNub.addChild(sn, after);
            }
            res.setNub(sn, false);

            if (after !== undefined) {
                parent.kids.splice(after + 1, 0, res);
            } else {
                parent.kids.push(res);
            }

            // still reset all results when a fieldset if manually added
            if (data["resetResults"] !== false) {
                this.reset();
                (this.parent as FormulairePrebarrage).reset();
            }

            return res;
        } else {
            return super.createFieldset(parent, json, data);
        }
    }

    /**
     * réinitialisation du formulaire suite à un changement d'une valeur, d'une option, ... :
     * effacement des résultats, application des dépendances, ...
     */
    public reset() {
        // also reset parent results (that will reset all its children results)
        (this.parent as FormulairePrebarrage).reset();
    }

    // ensure all PBresults are reset
    public moveFieldsetUp(fs: FieldSet) {
        super.moveFieldsetUp(fs);
        this.reset();
    }

    // ensure all PBresults are reset
    public moveFieldsetDown(fs: FieldSet) {
        super.moveFieldsetDown(fs);
        this.reset();
    }

    // ensure all PBresults are reset
    public removeFieldset(fs: FieldSet) {
        super.removeFieldset(fs);
        this.reset();
    }

}
