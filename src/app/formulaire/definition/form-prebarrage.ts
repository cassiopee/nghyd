import { CalculatorType, PbBassin, PbCloison, IObservable, PreBarrage, VariatedDetails, ParamDefinition, IParamDefinitionIterator, ParamDefinitionIterator } from "jalhyd";

import { FormulaireFixedVar } from "./form-fixedvar";
import { PbSchema } from "../elements/pb-schema";
import { FormulaireDefinition } from "./form-definition";
import { ServiceFactory } from "../../services/service-factory";
import { FormulairePbCloison } from "./form-pb-cloison";
import { FieldsetContainer } from "../elements/fieldset-container";
import { CalculatorResults } from "../../results/calculator-results";
import { PrebarrageResults } from "../../results/prebarrage-results";
import { NgParameter } from "../elements/ngparam";
import { longestVarParam } from "../../util/util";
import { FormulaireNode } from "../elements/formulaire-node";

/**
 * Formulaire pour les PréBarrage
 */
export class FormulairePrebarrage extends FormulaireFixedVar {

    /** child form for river upstream/downstream elevations and flow */
    private riverForm: FormulaireFixedVar;

    /** child form for basins dimensions */
    private basinForm: FormulaireFixedVar;

    /** configuration for re-creating basin form every time needed */
    private basinFormConfig: string;

    /** child form for walls */
    private wallForm: FormulairePbCloison;

    /** configuration for re-creating wall form every time needed */
    private wallFormConfig: string;

    protected _selectedItem: PbBassin | PbCloison;

    protected _pbResults: PrebarrageResults;

    /** if true, show input data in the right panel, else show results */
    public showInputData = true;

    constructor() {
        super();
        this._pbResults = new PrebarrageResults();
        this.updateCalcResults();
        this._pbResults.addObserver(this);
    }

    public get selectedItem(): PbBassin | PbCloison {
        return this._selectedItem;
    }

    public get pbResults(): PrebarrageResults {
        return this._pbResults;
    }

    protected updateCalcResults() {
        this._calcResults = [];
        if (this._pbResults) {
            // ensure help links are propagated
            this._pbResults.helpLinks = this.helpLinks;
            this._calcResults.push(this._pbResults);
        }
    }

    public get hasResults(): boolean {
        return this._pbResults.hasResults;
    }

    public parseConfig(json?: {}) {
        if (json !== undefined) {
            this._jsonConfig = json;
        }
        for (const conf_index in this._jsonConfig) {
            const conf = this._jsonConfig[conf_index];
            const type: string = conf["type"];

            switch (type) {
                // options globales
                case "options":
                    break;

                case "subform":
                    this.parse_subform(conf);
                    break;

                case "pb_schema":
                    this.parse_pb_schema(conf);
                    break;

                default:
                    throw new Error(`type d'objet de module de calcul ${type} non pris en charge`);
            }
        }
        this.completeParse();
    }

    private parse_subform(json: {}) {
        switch (json["id"]) {
            case "subform_river":
                // parse it and build it once then keep it (it always has the same Nub: PreBarrage)
                this.riverForm = new FormulaireFixedVar();
                this.riverForm.defaultProperties["calcType"] = CalculatorType.PreBarrage;
                this.riverForm.currentNub = this.currentNub;
                this.riverForm.preparseConfig(json["config"]);
                this.riverForm.parseConfig(json["config"]);
                this.kids.push(this.riverForm);
                // show default form
                this.showFormElements(this.riverForm);
                break;
            case "subform_basin":
                // only store config to create it multiple times on demand
                this.basinFormConfig = json["config"];
                break;
            case "subform_wall":
                // same as above
                this.wallFormConfig = json["config"];
                break;
        }
    }

    private parse_pb_schema(json: {}) {
        const sch: PbSchema = new PbSchema(this);
        sch.parseConfig(json);
        this.kids.push(sch);
    }

    /**
     * Déclenché par CalculatorComponent lorsque le schéma de prébarrage
     * envoie un événement "nodeSelected" (on a cliqué sur un nœud)
     */
    public nodeSelected(node: PbBassin | PbCloison) {
        // console.debug(`FormulairePrebarrage.NodeSelected(${node?.uid})`);
        // did something change, or we just clicked again on the node that was already selected ?
        if (this._selectedItem !== node) {
            // store for results formatting
            this._selectedItem = node;

            // show only the relevant form
            if (node === undefined) {
                this.showFormElements(this.riverForm);

            } else if (node instanceof PbBassin) {
                this.basinForm = new FormulaireFixedVar(this);
                this.basinForm.defaultProperties["calcType"] = CalculatorType.PbBassin;
                this.basinForm.currentNub = node;
                this.basinForm.preparseConfig(this.basinFormConfig);
                this.basinForm.parseConfig(this.basinFormConfig);
                ServiceFactory.formulaireService.updateFormulaireLocalisation(this.basinForm);
                // add fieldsets for existing basins
                if (node.parent.bassins.length > 0) {
                    for (const bassin of node.parent.bassins) {
                        for (const e of this.basinForm.allFormElements) {
                            if (e instanceof FieldsetContainer) { // @TODO manage many containers one day ?
                                e.addFromTemplate(undefined, bassin, { resetResults: false });
                            }
                        }
                    }
                } else {
                    // if there was no existing basin, add a default one
                    for (const e of this.basinForm.allFormElements) {
                        if (e instanceof FieldsetContainer) {
                            e.addFromTemplate(undefined, undefined, { resetResults: false });
                            break;
                        }
                    }
                }
                this.showFormElements(this.basinForm);

            } else if (node instanceof PbCloison) {
                this.wallForm = new FormulairePbCloison(this);
                this.wallForm.defaultProperties["calcType"] = CalculatorType.PbCloison;
                this.wallForm.currentNub = node;
                this.wallForm.preparseConfig(this.wallFormConfig);
                this.wallForm.parseConfig(this.wallFormConfig);
                // subscribe to upstream/downstream basin change
                this.wallForm.addObserver(this); // @TODO why not this.form ? wallForm is just a dummy form to extract elements from…
                ServiceFactory.formulaireService.updateFormulaireLocalisation(this.wallForm);
                // add fieldsets for existing Structures
                if (node.structures.length > 0) {
                    for (const struct of node.structures) {
                        for (const e of this.wallForm.allFormElements) {
                            if (e instanceof FieldsetContainer) { // @TODO manage many containers one day ?
                                e.addFromTemplate(undefined, struct, { resetResults: false });
                            }
                        }
                    }
                } else {
                    // if there was no existing structure, add a default one
                    for (const e of this.wallForm.allFormElements) {
                        if (e instanceof FieldsetContainer) {
                            e.addFromTemplate(undefined, undefined, { resetResults: false });
                            break;
                        }
                    }
                }
                this.showFormElements(this.wallForm);
            }
            if (this._currentNub.result !== undefined) {
                this.reaffectResultComponents();
            }
        }
    }

    /**
     * Adds all elements of given Formulaire f to the current form, right
     * after the PbSchema, replacing any other element already present
     * @param f Formulaire to display
     */
    private showFormElements(f: FormulaireDefinition) {
        // clear all kids except PbSchema
        this._kids = [this.kids[0]];
        for (const e of f.kids) {
            this.kids.push(e);
        }
    }

    private refreshSchema(nodeUidToSelect?: string) {
        const pbs = this.kids[0] as PbSchema;
        pbs.refresh(nodeUidToSelect);
    }

    // interface Observer

    public update(sender: IObservable, data: any) {
        // console.debug("FormulairePrebarrage.update:", data);
        super.update(sender, data);
        if (sender instanceof FormulairePbCloison) {
            if (data.action === "updateBasin") {
                this.refreshSchema(data.value);
            }
        }
        if (sender instanceof PrebarrageResults) {
            if (data.action === "updateSchema") {
                this.refreshSchema();
            }
        }
    }

    protected compute() {
        this.runNubCalc(this.currentNub);
        this.refreshFieldsets(false); // important: before reaffectResultComponents() or it will break results components localization
        // reset variable index to avoid trying to access an index > 0 when nothing varies
        this._pbResults.variableIndex = 0;

        this.reaffectResultComponents();
        this.refreshSchema();
    }

    protected reaffectResultComponents() {
        const pb: PreBarrage = (this.currentNub as PreBarrage);
        const computedParam: NgParameter = this.getComputedParameter();

        // cacher les résultats
        this._pbResults.reset();
        this.addFixedParameters();

        // pour le sélecteur d'itérations
        const varParams: VariatedDetails[] = this.getVariatedParameters();
        if (varParams) {
            this._pbResults.variatedParameters = varParams;
            const lvp = longestVarParam(this._pbResults.variatedParameters);
            this._pbResults.size = lvp.size;
            this._pbResults.cloisonResults.size = lvp.size;
        }

        this._pbResults.result = pb.result;
        // résultats selon l'objet sélectionné sur le schéma
        if (this._selectedItem !== undefined && this._selectedItem instanceof PbCloison) {
            // afficher les résultats de cloison
            this._pbResults.cloisonResults.result = this._selectedItem.result;
            if (computedParam !== undefined) {
                this._pbResults.cloisonResults.calculatedParameter = computedParam;
            }
            // transmission des suffixes de cloisons calculés par l'algo de tri de PbSchemaComponent,
            // pour le sélecteur de conditions limites
            const pbs = this.kids[0] as PbSchema;
            this._pbResults.wallsSuffixes = pbs.wallsSuffixes;
        } else {
            // afficher les résultats des bassins
            // résultat général du Nub (amont, aval, débit)
            this._pbResults.calculatedParameter = computedParam;
            // résultat de chaque bassin
            for (const b of pb.bassins) {
                this._pbResults.bassinsResults.push(b.result);
            }
        }

        this.updateCalcResults();
    }

    /**
     * Trouve le Ngparameter correspondant au paramètre "p", parmi tous les
     * éléments du formulaire de PbCloison, qui peut contenir plusieurs
     * structures ayant des paramètres de même nom
     * @param param ParamDefinition
     */
    public getCloisonParam(param: ParamDefinition): NgParameter {
        for (const p of this.allFormElements) {
            if (p instanceof NgParameter) {
                if (p.paramDefinition === param) {
                    return p;
                }
            }
        }
    }

    protected addFixedParameters() {
        if (this._selectedItem !== undefined && this._selectedItem instanceof PbCloison) {
            for (const s of this._selectedItem.structures) {
                for (const p of s.parameterIterator) {
                    // if some parameter is variating, add id too (trick with PbResultsComponent)
                    if (p.visible) {
                        const ngp = this.getCloisonParam(p);
                        if (ngp) {
                            this._pbResults.cloisonResults.addFixedParameter(ngp);
                        }
                    }
                }
            }
        }
    }

    public resetFormResults() {
        this._pbResults.reset();
        this.updateCalcResults();
    }

    public resetResults() {
        // console.debug("FormulairePrebarrage.resetResults()");
        const hasToRedraw = (this._currentNub.result !== undefined);
        super.resetResults();
        // reset all children nubs
        for (const c of this.currentNub.getChildren()) {
            c.resetResult();
        }
        // reset all wall forms
        for (const k of this.kids) {
            if (k instanceof FormulaireDefinition) {
                k.reset();
            }
        }
        // redraw schema to remove calc results in node boxes
        if (hasToRedraw) {
            this.refreshSchema();
        }
    }

    /**
     *  Check validity of all model parameters
     *  @param withChildren check parameters of child nub as well
     *  @return array of uid nubs in error
     */
         public checkParameters(withChildren: boolean = true): string[] {
            let prmIt: IParamDefinitionIterator;
            if(withChildren) {
                prmIt = this.currentNub.parameterIterator;
            } else {
                prmIt = new ParamDefinitionIterator(this.currentNub.prms);
            }
            const uids: string[] = [];
            for (const p of prmIt) {
                if (!p.isCalculated && p.visible) {
                    try {
                        // will throw an error if no value is defined at all
                        p.paramValues.check();
                    } catch (e) {
                        let res;
                        if (p.parentNub.calcType === CalculatorType.Structure) {
                            res = p.parentNub.parent.uid;
                        } else {
                            res = p.parentNub.uid;
                        }
                        if (!uids.includes(res)) {
                            uids.push(res);
                        }
                    }
                }
            }
            return uids;
        }
}
