import { FormulaireRepeatableFieldset } from "./form-repeatable-fieldset";
import { FieldSet } from "../elements/fieldset";
import { FormulaireNode } from "../elements/formulaire-node";
import { FieldsetContainer } from "../elements/fieldset-container";

import { Nub } from "jalhyd";

/**
 * Formulaire pour "somme / produit de puissances"
 */
export class FormulaireSPP extends FormulaireRepeatableFieldset {

    public createFieldset(parent: FormulaireNode, json: {}, data?: {}, nub?: Nub): FieldSet {
        if (json["calcType"] === "YAXN") {
            // indice après lequel insérer le nouveau FieldSet
            const after = data["after"];

            const res: FieldSet = new FieldSet(parent);
            let mrn: Nub;
            if (nub) { // use existing Nub (build interface based on model)
                mrn = nub;
            } else {
                mrn = this.createChildNub(data["template"]);
                this.currentNub.addChild(mrn, after);
            }
            res.setNub(mrn, false);

            if (after !== undefined) {
                parent.kids.splice(after + 1, 0, res);
            } else {
                parent.kids.push(res);
            }

            this.resetResults();

            return res;
        } else {
            return super.createFieldset(parent, json, data);
        }
    }

    protected get fieldsetContainer(): FieldsetContainer {
        const n = this.getFormulaireNodeById("yaxn_container");
        if (n === undefined || !(n instanceof FieldsetContainer)) {
            throw new Error("l'élément 'yaxn_container' n'est pas du type FieldsetContainer");
        }
        return n as FieldsetContainer;
    }
}
