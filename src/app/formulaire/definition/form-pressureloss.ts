import { PressureLoss, Props, PressureLossType, Session, PressureLossLaw, CalculatorType, IObservable } from "jalhyd";
import { FormulaireFixedVar } from "./form-fixedvar";
import { FieldSet } from "../elements/fieldset";
import { Prop_NullParameters } from "jalhyd";

/**
 * Formulaire pour la perte de charge
 */
export class FormulairePressureLoss extends FormulaireFixedVar {
    public preparseConfig(json: {}) {
        super.preparseConfig(json);

        // get pressure loss law select default value
        const dft: string = this.parseSelectDefaultValue(json, "select_pressurelosstype");
        this.defaultProperties["pressureLossType"] = PressureLossType[dft];
    }

    public initNub(props?: Props) {
        if (props === undefined) {
            props = new Props();
        }

        // create pressure loss parent nub
        const pll = this.defaultProperties["pressureLossType"];
        props.setPropValue("calcType", this.calculatorType);
        props.setPropValue("pressureLossType", pll);
        super.initNub(props);

        // create pressure loss law child nub
        const propsLaw: Props = new Props();
        propsLaw.setPropValue(Prop_NullParameters, props.getPropValue(Prop_NullParameters));
        propsLaw.setPropValue("calcType", CalculatorType.PressureLossLaw);
        const law = Session.getInstance().createNub(propsLaw) as PressureLossLaw;
        const pl: PressureLoss = this.currentNub as PressureLoss;
        pl.setLaw(law);
    }

    public update(sender: IObservable, data: any) {
        // if (sender instanceof FieldSet && sender.id === "fs_pressureloss_law" && data.action === "propertyChange") {
        if (data.action === "propertyChange") {
            if (data.name === "pressureLossType") {
                // changement de propriété du FieldSet contenant le select de choix du type de perte de charge

                // replace underlying pressure loss law without replacing whole Nub
                const newPLL = Session.getInstance().createPressureLossLaw(data.value, undefined, this.currentNub.getPropValue(Prop_NullParameters));
                (this._currentNub as PressureLoss).setLaw(newPLL);
                // show / hide dependent fields
                this.refreshFieldsets(true);
                this.reset();
            }
            else if (data.name === "material") {
                // changement de propriété du FieldSet contenant le select de choix du type de matériau
                this.refreshFieldsets(false);
                this.reset();
            }
        }
    }
}
