import { Result, CourbeRemous, CourbeRemousParams, acSection } from "jalhyd";

import { RemousResults } from "../../results/remous-results";
import { CalculatorResults } from "../../results/calculator-results";
import { FormulaireSection } from "./form-section";

export class FormulaireCourbeRemous extends FormulaireSection {

    /** résultats de courbes de remous */
    private _remousResults: RemousResults;

    private resultYn: Result;

    private resultYc: Result;

    constructor() {
        super();
        this._remousResults = new RemousResults(this);
        this.updateCalcResults();
        this._props["varCalc"] = ""; // important
    }

    protected compute() {
        this.reaffectResultComponents();
    }

    protected reaffectResultComponents() {
        const cr: CourbeRemous = this.currentNub as CourbeRemous;
        const prmCR: CourbeRemousParams = cr.prms as CourbeRemousParams;

        this._remousResults.parameters = prmCR;

        // variable supplémentaire à calculer
        this._remousResults.extraParamSymbol = this.currentNub.getPropValue("varCalc");

        // calcul
        this._remousResults.result = cr.CalcSerie();

        const sect: acSection = cr.Sn;
        this.resultYn = sect.CalcSection("Yn"); // hauteur normale
        this.resultYc = sect.CalcSection("Yc"); // hauteur critique

        // données du graphique
        this._remousResults.hauteurNormale = this.resultYn.resultElement;
        this._remousResults.hauteurCritique = this.resultYc.resultElement;
        if (this._remousResults.extraParamSymbol) {
            this._remousResults.extraChart = !["Hs", "Hsc", "Ycor", "Ycon"].includes(this._remousResults.extraParamSymbol);
        } else {
            this._remousResults.extraChart = false;
        }
        this.updateCalcResults();
    }

    protected updateCalcResults() {
        this._calcResults = [];
        if (this._remousResults) {
            // ensure help links are propagated
            this._remousResults.helpLinks = this.helpLinks;
            this._calcResults.push(this._remousResults);
        }
    }

    public resetFormResults() {
        this._remousResults.reset();
        this.updateCalcResults();
    }

    public get hasResults(): boolean {
        return this._remousResults.hasResults;
    }
}
