import { RemousResults } from "app/results/remous-results";
import { acSection, CourbeRemous, CourbeRemousParams, MacroRugo, MacrorugoRemous, Props, Result, Session } from "jalhyd";
import { IObservable, MacrorugoRemousParams, Nub, Verificateur} from "jalhyd/build/internal_modules";
import { FormulaireFixedVar } from "./form-fixedvar";
import { SelectField } from "../elements/select/select-field";

export class FormulaireMacrorugoRemous extends FormulaireFixedVar {
    public initNub(props?: Props) {
        // scan session, find first PAM
        let pam: MacroRugo;
        for (const n of Session.getInstance().getAllNubs()) {
            if (n instanceof MacroRugo) {
                pam = n;
                break;
            }
        }
        if (pam !== undefined) {
            props.setPropValue("nubMacroRugo", pam.uid);

        }
        return super.initNub(props);
    }


    /** résultats de courbes de remous */
    private _remousResults: RemousResults;

    private resultYn: Result;

    private resultYc: Result;

    constructor() {
        super();
        this._remousResults = new RemousResults(this);
        this.updateCalcResults();
        this._props["varCalc"] = ""; // important
    }

    protected compute() {
        this.reaffectResultComponents();
    }

    protected reaffectResultComponents() {
        const mrr = this.currentNub as MacrorugoRemous;
        mrr.syncSectionMacroRugo();
        
        // variable supplémentaire à calculer
        this._remousResults.extraParamSymbol = mrr.getPropValue("varCalc");
        
        // calcul
        this._remousResults.result = mrr.CalcSerie();

        const prmMRR = mrr.prms as MacrorugoRemousParams;
        this._remousResults.parameters = prmMRR;

        // calcul
        this._remousResults.result = mrr.CalcSerie();

        const sect: acSection = mrr.Sn;
        this.resultYn = sect.CalcSection("Yn"); // hauteur normale
        this.resultYc = sect.CalcSection("Yc"); // hauteur critique

        // données du graphique
        this._remousResults.hauteurNormale = this.resultYn.resultElement;
        this._remousResults.hauteurCritique = this.resultYc.resultElement;
        if (this._remousResults.extraParamSymbol) {
            this._remousResults.extraChart = !["Hs", "Hsc", "Ycor", "Ycon"].includes(this._remousResults.extraParamSymbol);
        } else {
            this._remousResults.extraChart = false;
        }
        this.updateCalcResults();
    }

    protected updateCalcResults() {
        this._calcResults = [];
        if (this._remousResults) {
            // ensure help links are propagated
            this._remousResults.helpLinks = this.helpLinks;
            this._calcResults.push(this._remousResults);
        }
    }

    // interface Observer

    public update(sender: IObservable, data: any, props?: Props) {
        // copied from FormDefinition, to avoid calling super.update() that would trigger an unwanted this.refreshFieldsets();
        if (sender instanceof Nub) {
            switch (data.action) {
                case "resultUpdated":
                    // forward Nub results update notification to FormCompute objects
                    this.reaffectResultComponents();
                    break;
            }
        }
        // copied from FormFixedVar, to avoid calling super.update()
        if (data.action === "propertyChange") {
            this.reset();
        }

        if (sender instanceof SelectField) {
            this.reset(); // reset results
            if (sender.id === "select_target_pam" && data.action === "select") {
                this._currentNub.setPropValue("nubMacroRugo",data.value.value);
            } 
        }
    }

    public resetFormResults() {
        this._remousResults.reset();
        this.updateCalcResults();
    }

    public get hasResults(): boolean {
        return this._remousResults.hasResults;
    }

}
