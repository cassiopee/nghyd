import { FormulaireFixedVar } from "./form-fixedvar";

/**
 * Formulaire pour les PAR Calage
 */
export class FormulairePAR extends FormulaireFixedVar {

    constructor() {
        super();
        // custom variables order for results tables
        this.fixedResults.variablesOrder = [
            "Q", "Z1", "Z2", "ha", "S", "L", "a", "N", "M",
            "h", "qStar", "V", "ZD1", "ZR1", "ZD2", "ZR2", "P", "Nb", "LPI", "LPH",
            "A", "B", "C", "D", "H", "Hmin", "Hmax", "ZM"
        ];
    }

}
