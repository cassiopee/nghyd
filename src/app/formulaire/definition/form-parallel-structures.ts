import { Structure, Nub, ParallelStructure, StructureProperties, Props, Session, ParamDefinition, Prop_NullParameters, IProperties } from "jalhyd";

import { FieldsetContainer } from "../elements/fieldset-container";
import { FieldSet } from "../elements/fieldset";
import { SelectField } from "../elements/select/select-field";
import { NgParameter } from "../elements/ngparam";
import { FieldsetTemplate } from "../elements/fieldset-template";
import { FormulaireNode } from "../elements/formulaire-node";
import { FormulaireRepeatableFieldset } from "./form-repeatable-fieldset";
import { ServiceFactory } from "app/services/service-factory";

export class FormulaireParallelStructure extends FormulaireRepeatableFieldset {

    /**
     * construit un identifiant de type { uid: "abcdef", symbol: "X" }
     * avec "abcdef" l'index de l'ouvrage et "X" son paramètre
     */
    protected getParameterRefid(p: ParamDefinition): any {
        const nub = p.parentComputeNode;
        if (nub instanceof Structure) {
            return {
                uid: nub.uid,
                symbol: p.symbol
            };
        } else {
            return super.getParameterRefid(p);
        }
    }

    public createFieldset(parent: FormulaireNode, json: {}, data?: {}, nub?: Nub): FieldSet {
        if (json["calcType"] === "Structure") {
            // indice après lequel insérer le nouveau FieldSet
            const after = data["after"];

            const res: FieldSet = new FieldSet(parent);
            let sn: Nub;
            if (nub) { // use existing Nub (build interface based on model)
                sn = nub;
            } else {
                sn = this.createChildNub(data["template"]);
                this.currentNub.addChild(sn, after);
            }
            res.setNub(sn, false);

            if (after !== undefined) {
                parent.kids.splice(after + 1, 0, res);
            } else {
                parent.kids.push(res);
            }

            this.resetResults();

            return res;
        } else {
            return super.createFieldset(parent, json, data);
        }
    }

    protected createChildNub(templ: FieldsetTemplate): Nub {
        const params = {};
        params["calcType"] = templ.calcTypeFromConfig;
        params["structureType"] = templ.defaultStructTypeFromConfig;
        params["loiDebit"] = templ.defaultLoiDebitFromConfig;
        params[Prop_NullParameters] = ServiceFactory.applicationSetupService.enableEmptyFieldsOnFormInit;

        return this.createStructure(new Props(params));
    }

    /**
     * Asks JaLHyd to create a Structure Nub as a child of the current Calculator Module
     * and return it; does not store it in the Session (for Structures, not for Calculator Modules)
     * @param p properties for the new Nub
     */
    protected createStructure(p: IProperties): Structure {
        return Session.getInstance().createNub(p, this.currentNub as ParallelStructure) as Structure;
    }

    /**
     * Replaces the given Structure sn in the current calculator module,
     * with a new one built with properties "params"
     * @param sn Structure to replace
     * @param params properties to build the new Nub (calcType, loiDebit...)
     */
    protected replaceNub(sn: Structure): Nub {
        const parent = (this.currentNub as ParallelStructure);
        const newStructure = this.createStructure(sn);
        parent.replaceChildInplace(sn, newStructure);
        return newStructure;
    }

    protected completeParse(firstNotif: boolean = true) {
        super.completeParse(firstNotif);
        this.fieldsetContainer.addObserver(this);
    }

    protected get fieldsetContainer(): FieldsetContainer {
        const n = this.getFormulaireNodeById("struct_container"); // @TODO make it generic, do not force ID !
        if (n === undefined || !(n instanceof FieldsetContainer)) {
            throw new Error("l'élément 'struct_container' n'est pas du type FieldsetContainer");
        }
        return n as FieldsetContainer;
    }

    /**
     * Après une modification, détermine si les propriétés d'un Fieldset sont compatibles
     * entre elles et les ajuste au besoin
     * @param props propriétés à vérifier
     * @param name nom de la propriété qui vient de changer
     * @param val nouvelle valeur de la propriété
     */
    protected adjustProperties(props: IProperties, name: string, val: any) {
        if (name === "structureType") {
            if (! StructureProperties.isCompatibleValues(
                val, props.getPropValue("loiDebit"), this.currentNub as ParallelStructure
            )) {
                // currentNub should always be a ParallelStructure here
                const ld = StructureProperties.findCompatibleLoiDebit(
                    props.getPropValue("structureType"),
                    [],
                    this.currentNub as ParallelStructure
                );
                props.setPropValue("loiDebit", ld);
            }
        }
    }

    /**
     * abonnement en tant qu'observateur des NgParameter des FieldSet contenus dans le FieldsetContainer
     */
    protected subscribeStructureInputFields(fs: FieldSet) {
        for (const n of fs.allFormElements) {
            if (n instanceof NgParameter) {
                n.addObserver(this);
            }
        }
    }
    /**
     * abonnement en tant qu'observateur du SelectField des FieldSet contenus dans le FieldsetContainer
     */
    protected subscribeStructureSelectFields(fs: FieldSet) {
        for (const n of fs.allFormElements) {
            if (n instanceof SelectField) {
                n.addObserver(this);
            }
        }
    }

    // interface Observer

    public update(sender: any, data: any) {
        super.update(sender, data);
        if (sender instanceof FieldsetContainer) {
            switch (data.action) {
                case "newFieldset":
                    this.reset();
                    this.subscribeStructureInputFields(data["fieldset"]);
                    this.subscribeStructureSelectFields(data["fieldset"]);
            }
        } else if (sender instanceof FieldSet && data.action === "propertyChange") {
            switch (sender.id) {
                case "fs_ouvrage":
                    // ensure loiDebit is set
                    //props.setPropValue("loiDebit", data.value); // ?? et si la propriété modifiée n'est pas la loi de débit ?

                    this.adjustProperties(sender, data["name"], data["value"]);
                    // replace Structure Nub
                    const newNub = this.replaceNub(sender.nub as Structure);
                    sender.setNub(newNub);
                    // treat the fieldset as new to re-subscribe to Nub properties change events
                    this.afterParseFieldset(sender);
                    this.reset();
                    break;
            }
        }
    }
}
