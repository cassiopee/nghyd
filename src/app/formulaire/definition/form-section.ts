import { FormulaireFixedVar } from "./form-fixedvar";
import { FieldSet } from "../elements/fieldset";
import { ServiceFactory } from "../../services/service-factory";

import { IObservable, Session, SectionNub, Props, CalculatorType, Prop_NullParameters, acSection } from "jalhyd";
import { SectionType } from "jalhyd";

export class FormulaireSection extends FormulaireFixedVar {

    /**
     * determine default section type from select configuration
     */
    private parseDefaultSectionType() {
        if (this.defaultProperties["nodeType"] === undefined) // still use "nodeType" for consistency, should be named "sectionType"
        {
            const def = this.parseSelectDefaultValue(this._jsonConfig, "select_section");
            this.defaultProperties["nodeType"] = SectionType[def];
        }
    }

    protected preparseOptions() {
        this.parseDefaultSectionType();
    }

    public initNub(props?: Props) {
        super.initNub(props);

        if (this.currentNub instanceof SectionNub) {
            // add new Section as first child, from given nodeType
            const propsSection = new Props();
            propsSection.setPropValue("calcType", CalculatorType.Section);
            propsSection.setPropValue("nodeType", this.defaultProperties["nodeType"]);
            propsSection.setPropValue(Prop_NullParameters, ServiceFactory.applicationSetupService.enableEmptyFieldsOnFormInit);
            const section = Session.getInstance().createNub(propsSection);
            this.currentNub.setSection(section as acSection);
        }
        else{
            throw new Error("nub is not a section nub!");
        }
    }

    public update(sender: IObservable, data: any) {
        super.update(sender, data);
        // changement de propriété du FieldSet contenant le select de choix du type de section
        if (sender instanceof FieldSet && sender.id === "fs_section" && data.action === "propertyChange") {
            // replace underlying section without replacing whole Nub
            const newSect = Session.getInstance().createSection(data.value);
            (this._currentNub as SectionNub).setSection(newSect);
            // reflect changes in GUI
            this.refreshFieldsets(true);
            this.reset();
        }
    }

}
