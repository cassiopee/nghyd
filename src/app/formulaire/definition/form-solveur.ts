import { IObservable, ParamDefinition, Nub, Props, Solveur, SolveurParams } from "jalhyd";

import { NgParameter } from "../elements/ngparam";
import { FormulaireFixedVar } from "./form-fixedvar";
import { SelectField } from "../elements/select/select-field";
import { FieldSet } from "../elements/fieldset";
import { ServiceFactory } from "app/services/service-factory";

/**
 * Formulaire pour les Solveurs
 */
export class FormulaireSolveur extends FormulaireFixedVar {

    /** id of select configuring targetted result */
    private _targettedResultSelectId: string;

    protected parseOptions(json: {}) {
        super.parseOptions(json);
        this._targettedResultSelectId = this.getOption(json, "targettedResultSelectId");
    }

    protected completeParse(firstNotif: boolean = true) {
        super.completeParse(firstNotif);
        // even though this._targettedResultSelectId is a regular select, an extra action
        // is needed: refresh searched parameter selector
        if (this._targettedResultSelectId) {
            const sel = this.getFormulaireNodeById(this._targettedResultSelectId);
            if (sel) {
                sel.addObserver(this);
                if (firstNotif) {
                    // force 1st observation
                    (sel as SelectField).notifyValueChanged();
                }
            }
        }
    }

    public initNub(props?: Props) {
        super.initNub(props);

        // subscribe to Ytarget to be able to reset it during form construction (with empty fields option set)
        // cf. nghyd#601

        const n: Solveur = this._currentNub as Solveur;
        n.prms.Ytarget.addObserver(this);
    }

    // interface Observer

    public update(sender: IObservable, data: any) {
        // copied from FormDefinition, to avoid calling super.update() that would trigger an unwanted this.refreshFieldsets();
        if (sender instanceof Nub) {
            switch (data.action) {
                case "resultUpdated":
                    // forward Nub results update notification to FormCompute objects
                    this.reaffectResultComponents();
                    break;
            }
        }
        // copied from FormFixedVar, to avoid calling super.update()
        else if (data.action === "propertyChange") {
            this.reset();
            if (data.name === "nubToCalculate") {
                const n = (this.currentNub as Solveur).nubToCalculate;
                if (n !== undefined) {
                    const prms: SolveurParams = this.currentNub.prms as SolveurParams;
                    // update Ytarget
                    if (prms.Ytarget.singleValue === undefined && !ServiceFactory.applicationSetupService.enableEmptyFieldsOnFormInit) {
                        prms.Ytarget.singleValue = n.calculatedParam.singleValue;
                    }
                }
            }
        }
        else if (sender instanceof SelectField) {
            if (sender.id === "select_target_nub" && data.action === "select") {
                // update Solveur property: Nub to calculate
                try {
                    // if searchedParam is set to a value that won't be available anymore
                    // once nubToCalculate is updated, setPropValue throws an error, but
                    // nubToCalculate is updated anyway; here, just inhibit the error
                    this._currentNub.setPropValue("nubToCalculate", data.value.value);
                } catch (e) { }
                // refresh targetted result selector
                const trSel = this.getFormulaireNodeById(this._targettedResultSelectId) as SelectField;
                if (trSel) {
                    (trSel.parent as FieldSet).updateFields(true);
                    // trick to re-set observers
                    this.completeParse(false);
                }
                // refresh parameters selector
                this.refreshParameterEntries();
            } else if (sender.id === "select_searched_param" && data.action === "select") {
                // update Solveur property: searched Parameter
                try {
                    const p: ParamDefinition = data.value.value;
                    this._currentNub.setPropValue(
                        "searchedParameter",
                        p.nubUid + "/" + p.symbol
                    );
                } catch (e) { }
                // reflect changes in GUI
                const inputXinit = this.getFormulaireNodeById("Xinit") as NgParameter;
                inputXinit.notifyValueModified(this);
            } else if (sender.id === this._targettedResultSelectId) {
                // refresh parameters selector
                this.refreshParameterEntries();
            }
        }
    }

    /**
     * Re-populate searched parameter selector with fresh entries
     */
    private refreshParameterEntries() {
        const pSel = this.getFormulaireNodeById("select_searched_param") as SelectField;
        if (pSel) {
            pSel.updateEntries();
            // reflect changes in GUI
            const inputYtarget = this.getFormulaireNodeById("Ytarget") as NgParameter;
            inputYtarget.notifyValueModified(this);
        }
    }
}
