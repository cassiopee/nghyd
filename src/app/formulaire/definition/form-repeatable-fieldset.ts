import { FieldSet } from "../elements/fieldset";
import { FieldsetContainer } from "../elements/fieldset-container";
import { FormulaireFixedVar } from "./form-fixedvar";
import { FieldsetTemplate } from "../elements/fieldset-template";

import { Props, Session, Nub } from "jalhyd";

export abstract class FormulaireRepeatableFieldset extends FormulaireFixedVar {

    protected abstract get fieldsetContainer(): FieldsetContainer;

    public moveFieldsetUp(fs: FieldSet) {
        // déplacement du nub
        fs.nub.parent.moveChildUp(fs.nub);
        // déplacement du fieldset
        this.fieldsetContainer.moveFieldsetUp(fs);
        this.resetResults();
    }

    public moveFieldsetDown(fs: FieldSet) {
        // déplacement du nub
        fs.nub.parent.moveChildDown(fs.nub);
        // déplacement du fieldset
        this.fieldsetContainer.moveFieldsetDown(fs);
        this.resetResults();
    }

    public removeFieldset(fs: FieldSet) {
        // suppression du sous-nub dans le Nub parent
        this.currentNub.deleteChild(fs.nub.findPositionInParent());
        // suppression du fieldset
        this.fieldsetContainer.removeFieldset(fs);
        this.resetResults();
    }

    /**
     * Asks JaLHyd to create a nub as a child of the current Calculator Module
     * and return it; does not store it in the Session
     * @param p properties for the new Nub
     */
    protected createChildNub(templ: FieldsetTemplate): Nub {
        const params = {};
        params["calcType"] = templ.calcTypeFromConfig;
        return Session.getInstance().createNub(new Props(params), this.currentNub);
    }
}
