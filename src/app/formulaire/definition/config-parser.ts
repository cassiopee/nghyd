export class ConfigIterator implements IterableIterator<Object> {
    private _index = 0;

    private _entries: [string, any][];

    constructor(private _json: Object, private _key: string) {
        this._entries = Object.entries(this._json);
    }

    public next(): IteratorResult<Object> {
        let i = this._index;
        while (i < this._entries.length) {
            const ent = this._entries[i][1];
            if (ent.type === this._key) {
                this._index = i + 1;
                return {
                    done: false,
                    value: ent
                };
            }
            i++;
        }
        return {
            done: true,
            value: undefined
        };
    }

    [Symbol.iterator](): IterableIterator<Object> {
        return this;
    }
}

export class ConfigParser {
    constructor(private _json: {}) {
    }

    public forAll(key: string): ConfigIterator {
        return new ConfigIterator(this._json, key);
    }
}