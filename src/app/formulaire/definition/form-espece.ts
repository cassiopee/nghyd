import { IObservable, DivingJetSupport } from "jalhyd";

import { FormulaireFixedVar } from "./form-fixedvar";
import { NgParameter } from "../elements/ngparam";

/**
 * Formulaire pour les espèces (critères de franchissement)
 */
export class FormulaireEspece extends FormulaireFixedVar {

    protected completeParse(firstNotif: boolean = true) {
        super.completeParse(firstNotif);
        this.updateDivingJetCriteriaInputs(this.currentNub.getPropValue("divingJetSupported"));
    }

    /**
     * Enable or disable inputs related to diving jet PAB criteria
     */
    public updateDivingJetCriteriaInputs(supported: DivingJetSupport) {
        const fsPABDivingJet = this.getFormulaireNodeById("fs_param_pab_p");
        if (fsPABDivingJet !== undefined) {
            for (const e of fsPABDivingJet.allFormElements) {
                if (e instanceof NgParameter) {
                    e.disabled = (supported === DivingJetSupport.NOT_SUPPORTED);
                }
            }
        }
    }

    // interface Observer

    public update(sender: IObservable, data: any) {
        super.update(sender, data);
        if (data.action === "propertyChange") {
            switch (data.name) {
                case "divingJetSupported":
                    this.updateDivingJetCriteriaInputs(data.value);
                    this.reset();
                    break;
            }
        }
    }
}
