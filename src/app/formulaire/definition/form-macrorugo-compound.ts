import { IObservable, Nub, MacrorugoCompound, Result, MRCInclination, VariatedDetails } from "jalhyd";

import { FieldSet } from "../elements/fieldset";
import { FieldsetContainer } from "../elements/fieldset-container";
import { FormulaireNode } from "../elements/formulaire-node";
import { NgParameter } from "../elements/ngparam";
import { FormulaireRepeatableFieldset } from "./form-repeatable-fieldset";
import { MacrorugoCompoundResults } from "../../results/macrorugo-compound-results";
import { CalculatorResults } from "../../results/calculator-results";

/**
 * Formulaire pour les passes à macrorugosités complexes
 */
export class FormulaireMacrorugoCompound extends FormulaireRepeatableFieldset {

    protected _mrcResults: MacrorugoCompoundResults;

    constructor() {
        super();
        this._mrcResults = new MacrorugoCompoundResults();
        this.updateCalcResults();
        // default properties
        this._props["inclinedApron"] = MRCInclination.NOT_INCLINED;
    }

    public createFieldset(parent: FormulaireNode, json: {}, data?: {}, nub?: Nub): FieldSet {
        if (json["calcType"] === "MacroRugo") {
            // indice après lequel insérer le nouveau FieldSet
            const after = data["after"];

            const res: FieldSet = new FieldSet(parent);
            let mrn: Nub;
            if (nub) { // use existing Nub (build interface based on model)
                mrn = nub;
            } else {
                mrn = this.createChildNub(data["template"]);
                this.currentNub.addChild(mrn, after);
            }
            res.setNub(mrn, false);

            if (after !== undefined) {
                parent.kids.splice(after + 1, 0, res);
            } else {
                parent.kids.push(res);
            }

            this.resetResults();

            return res;
        } else {
            return super.createFieldset(parent, json, data);
        }
    }

    protected completeParse(firstNotif: boolean = true) {
        super.completeParse(firstNotif);
        this.fieldsetContainer.addObserver(this);
        if (firstNotif) {
            this.updateApronState(this.currentNub.getPropValue("inclinedApron"));
        }
    }

    protected get fieldsetContainer(): FieldsetContainer {
        const n = this.getFormulaireNodeById("macrorugo_container");
        if (n === undefined || !(n instanceof FieldsetContainer)) {
            throw new Error("l'élément 'macrorugo_container' n'est pas du type FieldsetContainer");
        }
        return n as FieldsetContainer;
    }

    /**
     * abonnement en tant qu'observateur des NgParameter des FieldSet contenus dans le FieldsetContainer
     */
    private subscribeMacrorugoInputFields(fs: FieldSet) {
        for (const n of fs.allFormElements) {
            if (n instanceof NgParameter) {
                n.addObserver(this);
            }
        }
    }

    /**
     * Reflect inclinedApron property state in GUI
     */
    public updateApronState(inclined: MRCInclination) {
        // show / hide dependent fields (read from model)
        this.refreshFieldsets(false);
        // show / hide children list (GUI only)
        for (const elt of this.allFormElements) {
            if (elt instanceof FieldsetContainer) {
                elt.isDisplayed = (inclined === MRCInclination.NOT_INCLINED);
            }
        }
        // when switching to multiple aprons, remove all fieldset container
        // instances and reinstanciate for every MacroRugo child
        if (inclined === MRCInclination.NOT_INCLINED) {
            for (const elt of this.allFormElements) {
                if (elt instanceof FieldsetContainer) {
                    elt.clearKids();
                    for (const c of this.currentNub.getChildren()) {
                        elt.addFromTemplate(undefined, c);
                    }
                }
            }
        }
    }

    protected compute() {
        this.runNubCalc(this.currentNub);
        // reset variable index to avoid trying to access an index > 0 when nothing varies
        const mrcr = this.mrcResults;
        mrcr.variableIndex = 0;

        this.reaffectResultComponents();
    }

    protected reaffectResultComponents() {
        const mrc: MacrorugoCompound = (this.currentNub as MacrorugoCompound);
        const computedParam: NgParameter = this.getComputedParameter();
        const varParams: VariatedDetails[] = this.getVariatedParameters();

        // résultat de calcul de la passe à macrorugo complexe
        this._mrcResults.calculatedParameter = computedParam;
        this._mrcResults.result = mrc.result;
        if (varParams) {
            this._mrcResults.variatedParameters = varParams;
        }
        // résultat de chaque enfant
        const cr: Result[] = [];
        for (const c of mrc.children) {
            cr.push(c.result);
        }
        this._mrcResults.childrenResults = cr;
        this.updateCalcResults();
    }

    public get mrcResults() {
        return this._mrcResults;
    }

    public resetFormResults() {
        this._mrcResults.reset();
        this.updateCalcResults();
    }

    protected updateCalcResults() {
        this._calcResults = [];
        if (this._mrcResults) {
            // ensure help links are propagated
            this._mrcResults.helpLinks = this.helpLinks;
            this._calcResults.push(this._mrcResults);
        }
    }

    public get hasResults(): boolean {
        return this._mrcResults.hasResults;
    }

    // interface Observer

    public update(sender: IObservable, data: any) {
        super.update(sender, data);
        if (sender instanceof FieldsetContainer) {
            switch (data.action) {
                case "newFieldset":
                    this.reset();
                    this.subscribeMacrorugoInputFields(data["fieldset"]);
            }
        } else  if (sender instanceof FieldSet && data.action === "propertyChange") {
            switch (data.name) {
                case "inclinedApron":
                    this.updateApronState(data.value);
                    this.reset();
                    break;
            }
        }
    }
}
