import { Nub, ParamDefinition, Result, SectionParametree, acSection } from "jalhyd";

import { FormulaireSection } from "./form-section";
import { SectionResults } from "../../results/section-results";
import { ParamRadioConfig, NgParameter } from "../elements/ngparam";
import { CalculatorResults } from "../../results/calculator-results";

export class FormulaireSectionParametree extends FormulaireSection {

    /**
     * résultats de section paramétrée
     */
    private _sectionResults: SectionResults;

    public constructor() {
        super();
        this._sectionResults = new SectionResults();
        this.updateCalcResults();
    }

    protected compute() {
        this.runNubCalc(this.currentNub);
        this.reaffectResultComponents();
    }

    protected reaffectResultComponents() {
        this.resetFormResults(); // to avoid adding fixed parameters more than once (see below)
        const sectNub: SectionParametree = this.currentNub as SectionParametree;
        const sect: acSection = sectNub.section;
        this._sectionResults.section = sect;

        const varParams = this.getVariatedParameters();
        if (varParams.length > 0) {
            // résultats variés avec tous les résultats complémentaires
            this._varResults.variatedParameters = varParams;
            this._varResults.result = sectNub.result;
            this._varResults.update();
        } else {
            // résultats de section (avec le graphique de section)
            this._sectionResults.result = sectNub.result;
            // résultats complémentaires des paramètres fixés
            this.addFixedParameters();
            this._fixedResults.result = sectNub.result;
        }
        this.updateCalcResults();
    }

    public resetFormResults() {
        this._fixedResults.reset();
        this._varResults.reset();
        this._sectionResults.reset();
        this.updateCalcResults();
    }

    public get hasResults(): boolean {
        return (this._fixedResults?.hasResults)
            || (this._varResults?.hasResults)
            || (this._sectionResults?.hasResults);
    }

    protected updateCalcResults() {
        this._calcResults = [];
        // ensure help links are propagated
        this._fixedResults.helpLinks = this.helpLinks;
        this._varResults.helpLinks = this.helpLinks;
        this._calcResults.push(this._fixedResults);
        this._calcResults.push(this._varResults);
        if(this._sectionResults){
            this._sectionResults.helpLinks = this.helpLinks;
            this._calcResults.push(this._sectionResults);
        }
    }
}
