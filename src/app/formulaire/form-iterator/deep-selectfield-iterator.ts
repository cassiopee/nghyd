import { AbstractFormulaireNodeIterator } from "./abstract-node-iterator";
import { FormulaireNode } from "../elements/formulaire-node";
import { SelectField } from "../elements/select/select-field";

/**
 * itérateur qui extrait récursivement les SelectField dans un tableau de FormulaireElement
 * (qui peut contenir des FieldsetContainer)
 */
export class DeepSelectFieldIterator extends AbstractFormulaireNodeIterator<SelectField> implements IterableIterator<SelectField> {
    protected isIterable(fe: FormulaireNode) {
        return fe instanceof SelectField;
    }

    // interface IterableIterator

    [Symbol.iterator](): IterableIterator<SelectField> {
        return this;
    }
}
