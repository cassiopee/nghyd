import { AbstractFormulaireNodeIterator } from "./abstract-node-iterator";
import { FieldSet } from "../elements/fieldset";
import { FormulaireNode } from "../elements/formulaire-node";

/**
 * itérateur qui extrait récursivement les FieldSet dans un tableau de FormulaireElement
 * (qui peut contenir des FieldsetContainer)
 */
export class DeepFieldsetIterator extends AbstractFormulaireNodeIterator<FieldSet> implements IterableIterator<FieldSet> {
    protected isIterable(fe: FormulaireNode) {
        return fe instanceof FieldSet;
    }

    // interface IterableIterator

    [Symbol.iterator](): IterableIterator<FieldSet> {
        return this;
    }
}
