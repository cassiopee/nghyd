import { AbstractFormulaireNodeIterator } from "./abstract-node-iterator";
import { FormulaireElement } from "../elements/formulaire-element";

/**
 * itérateur qui extrait récursivement les FormulaireElement dans un tableau de FormulaireElement
 * (qui peut contenir des FieldsetContainer)
 */
export class DeepFormulaireElementIterator
    extends AbstractFormulaireNodeIterator<FormulaireElement>
    implements IterableIterator<FormulaireElement> {

        // interface IterableIterator

    [Symbol.iterator](): IterableIterator<FormulaireElement> {
        return this;
    }
}
