import { FormulaireNode } from "../elements/formulaire-node";

/**
 * classe de construction d'itérateurs qui parcourent un arbre de FormulaireNode
 */
export class AbstractFormulaireNodeIterator<T extends FormulaireNode> {
    private _array: T[] = [];

    private _index = 0;

    constructor(n: FormulaireNode) {
        this.flatten(n.kids, this._array);
    }

    private flatten(input: FormulaireNode[], out: FormulaireNode[]) {
        for (const fe of input) {
            if (this.isIterable(fe)) {
                out.push(fe);
            }
            if (this.isDeepIterator()) {
                this.flatten(fe.kids, out);
            }
        }
    }

    protected isIterable(fe: FormulaireNode): boolean {
        return true;
    }

    protected isDeepIterator(): boolean {
        return true;
    }

    public next(): IteratorResult<T> {
        const i = this._index;
        if (this._index < this._array.length) {
            this._index = i + 1;
            return {
                done: false,
                value: this._array[i]
            };
        } else {
            return {
                done: true,
                value: undefined
            };
        }
    }
}
