import { AbstractFormulaireNodeIterator } from "./abstract-node-iterator";
import { FormulaireElement } from "../elements/formulaire-element";

/**
 * itérateur qui extrait les FormulaireElement de 1er niveau dans un tableau de FormulaireElement
 * (qui peut contenir des FieldsetContainer)
 */
export class TopFormulaireElementIterator
    extends AbstractFormulaireNodeIterator<FormulaireElement>
    implements IterableIterator<FormulaireElement> {

    protected isDeepIterator(): boolean {
        return false;
    }

    // interface IterableIterator

    [Symbol.iterator](): IterableIterator<FormulaireElement> {
        return this;
    }
}
