import { ErrorStateMatcher } from "@angular/material/core";
import { FormControl, FormGroupDirective, NgForm } from "@angular/forms";
import { Injectable } from "@angular/core";

/**
 * An error state matcher for Angular Forms that displays errors immediately,
 * instead of waiting for the focus to get out of the input at least once
 */
@Injectable()
export class ImmediateErrorStateMatcher implements ErrorStateMatcher {

    constructor() { }

    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        // const isSubmitted = form && form.submitted;
        return !!(control && control.invalid /* && (control.dirty || control.touched || isSubmitted) */ );
    }
}
