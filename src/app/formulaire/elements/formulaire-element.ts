import { FormulaireNode } from "./formulaire-node";
import { ServiceFactory } from "../../services/service-factory";
import { FormulaireDefinition } from "../definition/form-definition";
import { FormulaireService } from "../../services/formulaire.service";

/**
 * élément (enfant) du formulaire : fieldset, input, container, ...
 */
export abstract class FormulaireElement extends FormulaireNode {
    /**
     * affiché ?
     */
    private _isDisplayed: boolean;

    /**
     * étiquette affichée dans l'UI
     */
    protected _label: string;

    private formulaireService: FormulaireService;

    public static removePrefix(s: string, prefix: string): string {
        if (s.startsWith(prefix)) {
            const l = prefix.length;
            return s.substr(l, s.length - l);
        }
    }

    constructor(parent: FormulaireNode) {
        super(parent);
        this._isDisplayed = true;
        this.formulaireService = ServiceFactory.formulaireService;
    }

    get isDisplayed(): boolean {
        return this._isDisplayed;
    }

    set isDisplayed(b: boolean) {
        this._isDisplayed = b;
        for (const k of this.getKids()) {
            k.isDisplayed = b;
        }
    }

    get label(): string {
        return this._label;
    }

    public getKids(): FormulaireElement[] {
        return this.kids as FormulaireElement[];
    }

    /**
     * formulaire parent
     */
    public get parentForm(): FormulaireDefinition {
        let res = this.parent;
        if (res === undefined) {
            return undefined;
        }

        // while (!(res instanceof FormulaireDefinition))
        while (!("calculatorName" in res)) {
            // pour éviter de faire référence au type FormulaireDefinition, supprimer l'import correspondant et
            // casser les dépendances circulaires d'import
            res = res.parent;
        }
        return (res as FormulaireDefinition);
    }

    /**
     * Updates localisation for this element: first the label then all the element's children
     * @param loc calculator-specific localised messages map
     * @param key Element label key
     */
    public updateLocalisation() {
        this._label = this.formulaireService.localizeText(this._confId, this.parentForm.currentNub.calcType);
        for (const f of this.getKids()) {
            f.updateLocalisation();
        }
    }

    public toString() {
        return "id:" + this.id + (this._isDisplayed ? " displayed" : " NOT displayed") + " label:" + this.label;
    }
}
