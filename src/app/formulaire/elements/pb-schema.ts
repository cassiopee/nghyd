import { PreBarrage } from "jalhyd";

import { FormulaireElement } from "./formulaire-element";
import { FormulairePrebarrage } from "../definition/form-prebarrage";

/**
 * The interactive schema for calculator type "PreBarrage" (form element).
 *
 * This is just a gateway between the model (Prebarrage)
 * and the user interface (PbSchemaComponent)
 */
export class PbSchema extends FormulaireElement {

    /** Stores appropriate number suffix for a given wall uid (related to existingWalls above) */
    public wallsSuffixes: { [key: string]: number };

    public parseConfig(json: {}) {
        this._confId = json["id"];
    }

    /**
     * Returns the Prebarrage model associated to the parent form
     */
    public get form(): FormulairePrebarrage {
        if (this.parentForm) {
            return this.parentForm as FormulairePrebarrage;
        }
    }

    /**
     * Returns the Prebarrage model associated to the parent form
     */
    public get pb(): PreBarrage {
        if (this.parentForm) {
            return this.parentForm.currentNub as PreBarrage;
        }
    }

    /** Asks PbSchemaComponent to redraw the schema */
    public refresh(nodeUidToSelect?: string) {
        // console.debug(`PbSchema.refresh(${nodeUidToSelect})`);
        this.notifyObservers({
            action: "refresh",
            value: nodeUidToSelect
        }, this);
    }
}
