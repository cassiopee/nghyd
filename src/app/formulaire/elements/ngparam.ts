import {
    Interval, ParamDefinition, ParamDomain, ParamValueMode, INumberIterator,
    Observer, asObservable, ParamCalculability, LinkedValue
} from "jalhyd";

import { sprintf } from "sprintf-js";

import { InputField } from "./input-field";
import { ServiceFactory } from "../../services/service-factory";
import { FormulaireNode } from "./formulaire-node";
import { fv } from "../../util/util";

export enum ParamRadioConfig {
    /** pas de radio, paramètre modifiable à la main uniquement */
    FIX,

    /** boutons radio "paramètre fixé" et "paramètre à varier" */
    VAR,

    /** boutons radio "paramètre fixé", "paramètre à varier" et "paramètre à calculer" */
    CAL,

    /** boutons radio "paramètre fixé", "paramètre à varier" et "paramètre à calculer", "paramètre lié" */
    LINK
}

/**
 * classe englobante de ParamDefinition (champs supplémentaires pour l'affichage, radio boutons, ...)
 */
export class NgParameter extends InputField implements Observer {

    /** set to true to disable UI validation on this input */
    private _allowEmpty = false;

    /** user defined title (if undefined : parameter label + unit */
    private _title: string;

    /**
     * fix,var,calc,link radio buttons configuration, ie. what buttons are available,
     * NOT the one selected
     */
    private _radioConfig: ParamRadioConfig;

    public disabled: boolean;

    constructor(private _paramDef: ParamDefinition, parent: FormulaireNode) {
        super(parent);
        this._radioConfig = this.radioState;
        this.disabled = false;
    }

    /**
     * Returns a text preview of the current value(s), depending on the value mode
     * @param compact if true, will represent multiple values in a more compact way
     */
    public static preview(p: ParamDefinition, compact: boolean = false): string {
        let valuePreview: string;
        const i18n = ServiceFactory.i18nService;

        switch (p.valueMode) {
            case ParamValueMode.SINGLE:
                valuePreview = fv(p.getValue());
                break;
            case ParamValueMode.MINMAX:
                let min: any = p.min;
                let max: any = p.max;
                let step: any = p.step;
                if (min) {
                    min = fv(min);
                }
                if (max) {
                    max = fv(max);
                }
                if (step) {
                    step = fv(step);
                }
                if (compact) {
                    valuePreview = min + " … " + max;
                } else {
                    valuePreview = i18n.localizeText("INFO_PARAMFIELD_PARAMVARIER_MINMAXSTEP");
                    valuePreview = sprintf(valuePreview, min, max, step);
                }
                break;
            case ParamValueMode.LISTE:
                const vals = p.valueList || [];
                if (compact) {
                    valuePreview = fv(vals[0]) + " … " + fv(vals[vals.length - 1]);
                } else {
                    valuePreview = i18n.localizeText("INFO_PARAMFIELD_PARAMVARIER_VALUES");
                    valuePreview += " " + vals.slice(0, 5).map((v) => {
                        return fv(v);
                    }).join("; ") + "…";
                }
                break;
            case ParamValueMode.CALCUL:
                valuePreview = i18n.localizeText("INFO_PARAMFIELD_IN_CALCULATION");
                if (p.calculability === ParamCalculability.DICHO) {
                    valuePreview += " (" + i18n.localizeText("INFO_PARAMFIELD_IN_CALCULATION_INITIAL_VALUE")
                        + ": " + fv(p.initValue) + ")";
                }
                break;
            case ParamValueMode.LINK:
                if (p.isReferenceDefined()) {
                    valuePreview = NgParameter.linkedValuePreview(p.referencedValue);
                }
        }

        return valuePreview;
    }

    /**
     * Returns a text preview of the given linked value(s)
     */
    public static linkedValuePreview(ref: LinkedValue): string {
        let valuePreview: string;
        const i18n = ServiceFactory.i18nService;

        if (ref.isParameter()) {
            const targetParam = (ref.element as ParamDefinition);
            // calculated param ?
            if (targetParam.valueMode === ParamValueMode.CALCUL) {
                // was the result already computed ?
                // .result might be set but the computation might have failed (dichotomy for ex.)
                if (ref.nub.result) {
                    if (ref.hasMultipleValues()) {
                        // compact representation
                        const cVal = ref.nub.result.getCalculatedValues();
                        valuePreview = fv(cVal[0]) + " … " + fv(cVal[cVal.length - 1]);
                    } else {
                        if (ref.nub.result.resultElements.length > 0 && ref.nub.result.vCalc) {
                            valuePreview = fv(ref.nub.result.vCalc);
                        } else {
                            // computation has been run but has failed
                            valuePreview = i18n.localizeText("INFO_PARAMFIELD_CALCULATION_FAILED");
                        }
                    }
                } else {
                    valuePreview = i18n.localizeText("INFO_PARAMFIELD_IN_CALCULATION");
                }
            } else {
                // recursive call, compact mode
                valuePreview = NgParameter.preview(targetParam, true);
            }
        } else {
            // was the result already computed ?
            try {
                const remoteValues = ref.getParamValues();
                if (ref.hasMultipleValues()) {
                    // compact representation
                    const cVal = remoteValues.valueList;
                    valuePreview = fv(cVal[0]) + " … " + fv(cVal[cVal.length - 1]);
                } else {
                    // like SINGLE mode
                    valuePreview = fv(remoteValues.currentValue);
                }
            } catch (e) {
                valuePreview = i18n.localizeText("INFO_PARAMFIELD_IN_CALCULATION");
            }
        }

        return valuePreview;
    }

    public get symbol(): string {
        return this._paramDef.symbol;
    }

    public get domain(): ParamDomain {
        return this._paramDef.domain;
    }

    public get allowEmpty(): boolean {
        return this._allowEmpty;
    }

    public set confId(id: string) {
        if (this._confId !== undefined) {
            throw new Error(`NgParameter : l'identifiant de configuration est déjà défini (${this._confId})`);
        }
        this._confId = id;
    }

    public get paramDefinition() {
        return this._paramDef;
    }

    /**
     * compute radio state from value mode
     */
    public get radioState(): ParamRadioConfig {
        switch (this._paramDef.valueMode) {
            case ParamValueMode.SINGLE:
                return ParamRadioConfig.FIX;

            case ParamValueMode.LISTE:
            case ParamValueMode.MINMAX:
                return ParamRadioConfig.VAR;

            case ParamValueMode.CALCUL:
                return ParamRadioConfig.CAL;

            case ParamValueMode.LINK:
                return ParamRadioConfig.LINK;
        }
    }

    public get radioConfig(): ParamRadioConfig {
        if (this._radioConfig === undefined) {
            throw new Error("radio config not defined!");
        }
        return this._radioConfig;
    }

    public set radioConfig(rc: ParamRadioConfig) {
        if (rc === undefined) {
            throw new Error("cannot set radio config to undefined!");
        }
        this._radioConfig = rc;
    }

    get isDefined(): boolean {
        return this._paramDef.isDefined;
    }

    public get valueMode() {
        return this._paramDef.valueMode;
    }

    public get unit() {
        return this._paramDef.unit;
    }

    /**
     * Unlinks the parameter and updates its value when value mode changes
     */
    public set valueMode(m: ParamValueMode) {
        // undefined si on clique en dehors du select après l'avoir ouvert (cad sans avoir fait de sélection)
        // et au même niveau, cad à côté du bouton et non à côté du menu déroulant
        if (m !== undefined && this._paramDef.valueMode !== m) {
            this.unlinkParameter();
            this._paramDef.valueMode = m;
            let val = this._paramDef.getValue();
            if (val !== undefined) {
                val = Number(fv(val));
            }
            this.notifyObservers({
                "action": "valueModeChange",
                "value": val
            });
        }
    }

    public get isMinMaxValid(): boolean {
        return this._paramDef.isMinMaxValid;
    }

    public get minValue() {
        return this._paramDef.min;
    }

    public get maxValue() {
        return this._paramDef.max;
    }

    public get stepRefValue(): Interval {
        return this._paramDef.stepRefValue;
    }

    public get stepValue() {
        return this._paramDef.step;
    }

    public get valueList() {
        return this._paramDef.valueList;
    }

    public get inferredValuesList() {
        return this._paramDef.getInferredValuesList();
    }

    /**
     * Model validation (definition domain)
     */
    public get isValid() {
        if (this.radioState === undefined) {
            return false;
        }

        return this._paramDef.isValid;
    }

    public get title(): string {
        if (this._title !== undefined) {
            return this._title;
        }

        let t = "";
        if (this.label !== undefined) {
            t = this.label;
        }
        if (this.unit !== undefined && this.unit !== "") {
            t = t + " (" + this.unit + ")";
        }
        return t;
    }

    /**
     * set custom title
     */
    public set title(t: string) {
        this._title = t;
    }

    public get valuesIterator(): INumberIterator {
        return this._paramDef.valuesIterator;
    }

    /**
     * compute radio config from parameter calculability
     */
    private getRadioConfigFromCalculability(): ParamRadioConfig {
        switch (this.paramDefinition.calculability) {
            case ParamCalculability.FIXED:
                return ParamRadioConfig.FIX;
            case ParamCalculability.FREE:
                return ParamRadioConfig.VAR;
            case ParamCalculability.DICHO:
            case ParamCalculability.EQUATION:
                return ParamRadioConfig.CAL;
            default:
                throw new Error("parameter calculability not set!");
        }
    }

    /**
     * Trick method for overriding label; used in fake NgParameter such
     * as the one in DialogEditParamComputedComponent
     */
    public setLabel(l: string) {
        this._label = l;
    }

    /**
     * Sets this parameter as the one to be computed
     */
    public setCalculated() {
        this.paramDefinition.setCalculated();
    }

    /**
     * Asks the ParamDefinition for its singleValue (not currentValue,
     * to avoid displaying computation results); calculated linked
     * value might not be available yet
     */
    public getValue() {
        try {
            return this._paramDef.getValue();
        } catch (error) {
            return undefined;
        }
    }

    /**
     * notification envoyée après la modification de la valeur du paramètre
     */
    public notifyValueModified(sender: any) {
        let val: number;
        if (this._paramDef.valueMode === ParamValueMode.SINGLE) {
            val = this._paramDef.singleValue;
        } else {
            val = this._paramDef.v; // @WARNING coward retrocompat
        }
        this.notifyObservers(
            {
                "action": "ngparamAfterValue",
                "param": this,
                "value": val
            }, sender
        );
    }

    private notifyMinValueModified(sender: any) {
        this.notifyObservers(
            {
                "action": "ngparamAfterMinValue",
                "param": this,
                "value": this._paramDef.min
            }, sender
        );
    }

    private notifyMaxValueModified(sender: any) {
        this.notifyObservers(
            {
                "action": "ngparamAfterMaxValue",
                "param": this,
                "value": this._paramDef.max
            }, sender
        );
    }

    private notifyStepValueModified(sender: any) {
        this.notifyObservers(
            {
                "action": "ngparamAfterStepValue",
                "param": this,
                "value": this._paramDef.step
            }, sender
        );
    }

    private notifyListValueModified(sender: any) {
        this.notifyObservers(
            {
                "action": "ngparamAfterStepValue",
                "param": this,
                "value": this._paramDef.valueList
            }, sender
        );
    }

    private notifyInitValueModified(sender: any) {
        this.notifyObservers(
            {
                "action": "ngparamAfterInitValue",
                "param": this,
                "value": this._paramDef.initValue
            }, sender
        );
    }

    /**
     * fixe la valeur du paramètre.
     * une notification préalable est envoyée pour laisser l'occasion aux objets liés de préciser le contexte
     * dans lequel cette valeur existe
     * @param sender
     * @param val
     */
    public setValue(sender: any, val: number) {
        const changed = (this._paramDef.getValue() !== val);
        if (changed) {
            this._paramDef.setValue(val, sender);
            this.notifyValueModified(sender);
        }
    }

    public resetMinValue(sender: any, v: number) {
        const changed = (this._paramDef.min !== v);
        if (changed) {
            this._paramDef.min = v;
            this.notifyMinValueModified(sender);
        }
    }

    public setMinValue(sender: any, v: number) {
        const changed = (this._paramDef.min !== v);
        if (changed) {
            this._paramDef.min = v;
            this.notifyMinValueModified(sender);
        }
    }

    public resetMaxValue(sender: any, v: number) {
        const changed = (this._paramDef.max !== v);
        if (changed) {
            this._paramDef.max = v;
            this.notifyMaxValueModified(sender);
        }
    }

    public setMaxValue(sender: any, v: number) {
        const changed = (this._paramDef.max !== v);
        if (changed) {
            this._paramDef.max = v;
            this.notifyMaxValueModified(sender);
        }
    }

    public resetStepValue(sender: any, v: number) {
        const changed = (this._paramDef.step !== v);
        if (changed) {
            this._paramDef.step = v;
            this.notifyStepValueModified(sender);
        }
    }

    public setStepValue(sender: any, v: number) {
        const changed = (this._paramDef.step !== v);
        if (changed) {
            this._paramDef.step = v;
            this.notifyStepValueModified(sender);
        }
    }

    public resetValueList(sender: any, l: number[]) {
        const changed = (JSON.stringify(this._paramDef.valueList) !== JSON.stringify(l));
        if (changed) {
            this._paramDef.valueList = l;
            this.notifyListValueModified(sender);
        }
    }

    public setValueList(sender: any, l: number[]) {
        const changed = (JSON.stringify(this._paramDef.valueList) !== JSON.stringify(l));
        if (changed) {
            this._paramDef.valueList = l;
            this.notifyListValueModified(sender);
        }
    }

    /**
     * set initial value for CALC mode
     */
    public setInitValue(sender: any, v: number) {
        const changed = (this._paramDef.initValue !== v);
        if (changed) {
            this._paramDef.initValue = v;
            this.notifyInitValueModified(sender);
        }
    }

    public objectRepresentation(): any {
        return {
            prmDef: this.paramDefinition.objectRepresentation(),
            allowEmpty: this._allowEmpty,
            unit: this.unit,
            radioConfig: this._radioConfig,
            valueMode: this.paramDefinition.valueMode
        }
    }

    public loadObjectRepresentation(rep: any, propagateToCalculatedParam: boolean = true) {
        if (this._paramDef.symbol === rep.prmDef.symbol) {
            this._paramDef.loadObjectRepresentation(rep.prmDef, false);
            this._allowEmpty = rep.allowEmpty;
            this._paramDef.setUnit(rep.unit);
            this._radioConfig = rep.radioConfig;
            this.paramDefinition.setValueMode(rep.valueMode, propagateToCalculatedParam);
        }
    }

    /**
     * supprime un lien avec un paramètre
     */
    private unlinkParameter() {
        const o = asObservable(this._paramDef.referencedValue);
        if (this.valueMode === ParamValueMode.LINK) {
            this._paramDef.undefineReference();
            if (o !== undefined) {
                o.removeObserver(this);
            }
        }
    }

    /**
     * crée le lien avec un paramètre
     */
    public linkToValue(target: LinkedValue) {
        const changed: boolean =
            // changement de mode
            !this._paramDef.isReferenceDefined()
            // ou changement de référence
            || !this._paramDef.referencedValue.equals(target);

        if (changed) {
            let o = asObservable(this._paramDef.referencedValue);
            if (o !== undefined) {
                o.removeObserver(this);
            }

            // this.valueMode = ParamValueMode.LINK; // supposed to be done at model level by instruction below
            // changement de lien
            this._paramDef.defineReferenceFromLinkedValue(target);

            o = asObservable(this._paramDef.referencedValue);
            if (o !== undefined) {
                o.addObserver(this); // pour être prévenu des changements de valeur de l'object référencé
            }

            let value;
            try {
                value = this.getValue();
            } catch (e) {
                // console.log("undefined target value (pending calculation)");
            }

            this.notifyObservers({
                "action": "valueLinkChange",
                "value": value
            });
        }
    }

    public checkValue(val: number) {
        this._paramDef.checkValueAgainstDomain(val);
    }

    public checkList(l: number[]) {
        for (const e of l) {
            this.checkValue(e);
        }
    }

    public checkMin(min: number): boolean {
        return this._paramDef.checkMin(min);
    }

    public checkMax(max: number): boolean {
        return this._paramDef.checkMax(max);
    }

    public checkMinMaxStep(step: number): boolean {
        return this._paramDef.checkMinMaxStep(step);
    }

    public parseConfig(json: {}) {
        this._confId = json["id"];
        this._helpLink = json["help"];
        if (json["allowEmpty"] !== undefined && typeof json["allowEmpty"] === "boolean") {
            this._allowEmpty = json["allowEmpty"];
        }
        this._radioConfig = this.getRadioConfigFromCalculability();
        if (json["calculable"] !== undefined && json["calculable"] === false) {
            this._radioConfig = Math.min(ParamCalculability.FREE, this._radioConfig);
        }
    }

    // interface Observer

    public update(sender: any, data: any) {
        switch (data["action"]) {
            case "paramdefinitionAfterValue": // changement de valeur envoyé par l'objet référencé
                this.notifyValueModified(sender);
                break;
        }
    }
}
