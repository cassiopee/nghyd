import { FormulaireElement } from "./formulaire-element";

export abstract class Field extends FormulaireElement {

    public abstract getValue(): any;
    public abstract setValue(sender: any, val: any): void;
}
