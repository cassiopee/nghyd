import { FieldSet } from "./fieldset";
import { CalculatorType, LoiDebit, Nub, StructureType, SectionType } from "jalhyd";
import { FormulaireDefinition } from "../definition/form-definition";
import { FieldsetContainer } from "./fieldset-container";
import { SelectFieldFactory } from "./select/select-field-factory";

export class FieldsetTemplate {
    private _jsonConfig: {};

    // parsed default structure type from JSON configuration file (from select_structure field)
    private _defaultStructureType: StructureType;

    // parsed default stage discharge law from JSON configuration file (from select_loidebit field)
    private _defaultLoiDebit: LoiDebit;

    public get config() {
        return this._jsonConfig;
    }

    public get calcTypeFromConfig(): CalculatorType {
        const ct: string = this._jsonConfig["calcType"];
        return CalculatorType[ct];
    }

    public get defaultStructTypeFromConfig(): StructureType {
        return this._defaultStructureType;
    }

    public get defaultLoiDebitFromConfig(): LoiDebit {
        return this._defaultLoiDebit;
    }

    private findSelectField(id: string): any {
        for (const f of this._jsonConfig["fields"]) {
            if (f["type"] === "select" && f["id"] === id) {
                return f;
            }
        }
    }

    public parseConfig(config: {}) {
        this._jsonConfig = config;

        if (this._jsonConfig["calcType"] === "Structure") {
            // parse default stage discharge law

            const ld: string = this.findSelectField(SelectFieldFactory.SelectId_LoiDebit)["default"];
            if (LoiDebit[ld] === undefined) {
                throw new Error(`FieldsetTemplate.defaultLoiDebitFromConfig : la loi de débit ${ld} n'est pas définie`);
            }
            this._defaultLoiDebit = LoiDebit[ld];

            // parse default structure type

            const st: string = this.findSelectField(SelectFieldFactory.SelectId_StructureType)["default"];
            if (StructureType[st] === undefined) {
                throw new Error(`FieldsetTemplate.defaultStructTypeFromConfig : le type de structure ${st} n'est pas défini`);
            }
            this._defaultStructureType = StructureType[st];
        }
    }

    /**
     * crée une instance de Fieldset et l'ajoute dans un conteneur
     * @param cont conteneur
     * @param after position à laquelle on ajoute le nouveau FieldSet
     * @param nub Nub existant à injecter dans le Fieldset
     * @param extra extra key-value pairs to add to the "newFieldset" event
     */
    public instantiateTemplate(cont: FieldsetContainer, after: number, nub?: Nub, extra?: any): FieldSet {
        const parentForm = cont.parent as FormulaireDefinition;
        let info = { "template": this, "after": after };
        info = { ...info, ... extra };
        const res = parentForm.createFieldset(cont, this._jsonConfig, info, nub);
        res.parseConfig(this._jsonConfig);
        parentForm.afterParseFieldset(res);
        return res;
    }
}
