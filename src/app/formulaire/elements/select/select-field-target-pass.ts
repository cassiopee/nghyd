import { ServiceFactory } from "app/services/service-factory";
import { decodeHtml } from "app/util/util";
import { CalculatorType, FishPass, Session, Verificateur } from "jalhyd";
import { FormulaireElement } from "../formulaire-element";
import { FormulaireNode } from "../formulaire-node";
import { SelectEntry } from "./select-entry";
import { SelectField } from "./select-field";

/*
    "id": "select_target_pass",
    "type": "select_custom",
    "source": "verificateur_target",
    "messageWhenEmpty": "INFO_VERIF_CREATE_PASS_FRIST"
*/

// Vérificateur, passe cible (à vérifier)
export class SelectFieldTargetPass extends SelectField {
    constructor(parent: FormulaireNode) {
        super(parent);
        this._messageWhenEmpty = "INFO_VERIF_CREATE_PASS_FRIST";
    }

    protected populate() {
        const fs = ServiceFactory.formulaireService;
        // find all Nubs of type FishPass
        const candidateNubs: any[] = Session.getInstance().getAllNubs().filter((element) => {
            return (
                (element instanceof FishPass)
                && element.calcType !== CalculatorType.Par // ParSimulation extends Par @TODO find something better
            );
        });
        for (const cn of candidateNubs) {
            const nub = fs.getFormulaireFromId(cn.uid);
            if (nub) {
                const label = nub.calculatorName + " (" + fs.getLocalisedTitleFromCalculatorType(nub.calculatorType) + ")";
                this.addEntry(this.createOrGetEntry(this._entriesBaseId + cn.uid, cn.uid, decodeHtml(label)));
            } else {
                // silent fail, this Verificateur nub was probably loaded before all the candidate nubs are done loading
            }
        }
    }

    protected initSelectedValue() {
        const ntv = (this.nub as Verificateur).nubToVerify;
        if (ntv !== undefined) {
            this.setValueFromId(this._entriesBaseId + ntv.uid);
        }
    }

    public updateLocalisation() {
        // do not override localisation done in populate()
        // ie. avoid what is done by SelectField.updateLocalisation()
    }
}
