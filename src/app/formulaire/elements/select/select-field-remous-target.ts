import { CourbeRemous } from "jalhyd";
import { SelectField } from "./select-field";
import { SelectEntry } from "./select-entry";
import { FormulaireNode } from "../formulaire-node";

/*
    "id": "select_target",
    "type": "select",
    "property": "varCalc",
    "source": "remous_target",
    "help": {
*/

export class SelectFieldRemousTarget extends SelectField {
    constructor(parent: FormulaireNode) {
        super(parent);
        this._associatedProperty = "varCalc";
    }

    protected populate() {
        // driven by string[], not enum (easier for variable names)
        this.addEntry(this.createOrGetEntry(this._entriesBaseId + "none", ""));
        for (const at of CourbeRemous.availableTargets) {
            const e: SelectEntry = this.createOrGetEntry(this._entriesBaseId + at, at);
            this.addEntry(e);
        }
    }

    protected initSelectedValue() {
        this.findAndSetDefaultValue();
    }
}
