import { FormulaireNode } from "../formulaire-node";
import { SelectField } from "./select-field";
import { SelectFieldDeviceLoiDebit } from "./select-field-device-loi-debit";
import { SelectFieldDeviceStructureType } from "./select-field-device-structure-type";
import { SelectFieldRemousTarget } from "./select-field-remous-target";
import { SelectFieldSolverTargetedResult } from "./select-field-solver-targeted-result";
import { SelectFieldUpstreamBasin } from "./select-field-upstream-basin";
import { SelectFieldDownstreamBasin } from "./select-field-downstream-basin";
import { SelectFieldSolverTarget } from "./select-field-solveur-target";
import { SelectFieldSearchedParam } from "./select-field-searched-param";
import { SelectFieldSpeciesList } from "./select-field-species-list";
import { SelectFieldTargetPass } from "./select-field-target-pass";
import { SelectFieldNubProperty } from "./select-field-nub-prop";

export class SelectFieldFactory {
    /**
      * Id of the select linked to structure type (JSON calculator configuration).
      * Also used in fieldset template parsing.
      */
    public static readonly SelectId_StructureType: string ="select_structure";

    /**
     * Id of the select linked to stage discharge law (JSON calculator configuration).
     * Also used in fieldset template parsing.
     */
    public static readonly SelectId_LoiDebit: string ="select_loidebit";

    public static newSelectField(json: {}, parent: FormulaireNode): SelectField {
        switch (json["id"]) {
            case "select_downstream_basin":
                return new SelectFieldDownstreamBasin(parent);

            case SelectFieldFactory.SelectId_LoiDebit:
                return new SelectFieldDeviceLoiDebit(parent);

            case "select_searched_param":
                return new SelectFieldSearchedParam(parent);

            case "select_species_list":
                return new SelectFieldSpeciesList(parent);

            case SelectFieldFactory.SelectId_StructureType:
                return new SelectFieldDeviceStructureType(parent);

            case "select_target":
                return new SelectFieldRemousTarget(parent);

            case "select_target_nub":
                return new SelectFieldSolverTarget(parent);

            case "select_target_pass":
                return new SelectFieldTargetPass(parent);

            case "select_target_result":
                return new SelectFieldSolverTargetedResult(parent);

            case "select_upstream_basin":
                return new SelectFieldUpstreamBasin(parent);

            case "select_divingjetsupported":
            case "select_gridprofile":
            case "select_gridtype":
            case "select_material":
            case "select_operation":
            case "select_partype":
            case "select_passtype":
            case "select_regime":
            case "select_resolution":
            case "select_section":
            case "select_sppoperation":
            case "select_unit":
            case "select_pressurelosstype":
                return new SelectFieldNubProperty(parent);

            default:
                throw new Error(`unknown select id ${json["id"]}`);
        }
    }
}
