import { ServiceFactory } from "app/services/service-factory";
import { decodeHtml } from "app/util/util";
import { acSection, Nub, Solveur } from "jalhyd";
import { SelectEntry } from "./select-entry";
import { SelectField } from "./select-field";

/*
    "id": "select_searched_param",
    "type": "select_custom",
    "source": "solveur_searched"
*/

// Solveur, paramètre recherché (à faire varier)
export class SelectFieldSearchedParam extends SelectField {
    protected populate() {
        const fs = ServiceFactory.formulaireService;

        // find all non-calculated, non-linked parameters of all Nubs that
        // the current "target" Nub depends on (if any)
        const solv = this.parentForm.currentNub as Solveur;
        const ntc: Nub = solv.nubToCalculate;
        const searchableParams = Solveur.getDependingNubsSearchableParams(
            ntc,
            solv.targettedResult !== undefined && solv.targettedResult !== ""
        );
        for (const p of searchableParams) {
            if (p.visible) {
                const calc = fs.getFormulaireFromId(p.originNub.uid).calculatorName;
                const varName = fs.expandVariableName(p.originNub.calcType, p.symbol);
                const label = `${p.symbol} - ${varName} (${calc})`;
                this.addEntry(this.createOrGetEntry(this._entriesBaseId + p.getParentComputeNode(false).uid + "_" + p.symbol, p, decodeHtml(label)));
            }
        }
    }

    protected initSelectedValue() {
        const sp = (this.nub as Solveur).searchedParameter;
        if (sp !== undefined) {
            let parentNubUid;
            const parentNub = sp.getParentComputeNode(false);
            if (parentNub instanceof acSection) {
                parentNubUid = parentNub.uid;
            }
            else {
                parentNubUid = sp.nubUid
            }
            this.setValueFromId(this._entriesBaseId + parentNubUid + "_" + sp.symbol);
        }
    }

    public updateLocalisation() {
        // do not override localisation done in populate()
        // ie. avoid what is done by SelectField.updateLocalisation()
    }
}
