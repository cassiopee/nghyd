import { ParallelStructure, StructureType } from "jalhyd";
import { SelectField } from "./select-field";
import { SelectEntry } from "./select-entry";
import { FormulaireNode } from "../formulaire-node";

/*
    "id": "select_structure",
    "type": "select",
    "property": "structureType",
    "source": "device_structure_type"
*/

export class SelectFieldDeviceStructureType extends SelectField {
    constructor(parent: FormulaireNode) {
        super(parent);
        this._associatedProperty = "structureType";
    }

    public parseConfig(json: {}, data?: {}) {
        super.parseConfig(json, data);
        this._configDefaultValue = json["default"];
    }

    /**
    * get discharge law linked to this select's nub
    */
    private get structureType(): StructureType {
        const child = this.nub.getChildren()[this.parent.indexAsKid()];
        return child.getPropValue("structureType");
    }

    protected populate() {
        // possible values depend on CalcType
        for (const st in (this.nub as ParallelStructure).getLoisAdmissibles()) {
            const e: SelectEntry = this.createOrGetEntry(this._entriesBaseId + st, StructureType[st]);
            this.addEntry(e);
        }
    }

    protected initSelectedValue() {
        this.findAndSetDefaultValue(StructureType[this.structureType]);
    }
}
