import { ServiceFactory } from "app/services/service-factory";
import { PbCloison, PreBarrage } from "jalhyd";
import { SelectEntry } from "./select-entry";
import { SelectField } from "./select-field";

/*
    "id": "select_downstream_basin",
    "type": "select_custom",
    "source": "downstream_basin"
*/

// PbCloisons, bassin aval
export class SelectFieldDownstreamBasin extends SelectField {
    protected populate() {
        const pbWallD = this.parentForm.currentNub as PbCloison;
        const preBarrageD = pbWallD.parent as PreBarrage;
        const posUb = pbWallD.bassinAmont?.findPositionInParent();
        // all available basins, depending on current upstream basin
        for (const b of preBarrageD.bassins) {
            const pos = b.findPositionInParent();
            if (posUb === undefined || pos > posUb) {
                const e = this.createOrGetEntry(this._entriesBaseId + b.uid, b.uid);
                e.intlInfo = b.description;
                this.addEntry(e);
            }
        }
        // river downstream
        const e = this.createOrGetEntry(this._entriesBaseId + "none", undefined);
        e.intlInfo = "INFO_LIB_AVAL";
        this.addEntry(e);
    }

    protected initSelectedValue() {
        const db = (this.nub as PbCloison).bassinAval;
        this.setValueFromId(this._entriesBaseId + (db ? db.uid : "none"));
    }

    public updateLocalisation() {
        for (const e of this._entries) {
            if (e.id === this._entriesBaseId + "none") {
                e.label = ServiceFactory.i18nService.localizeText(e.intlInfo)
            }
            else {
                e.label = ServiceFactory.i18nService.localizeMessage(e.intlInfo)
            }
        }
    }
}
