/*
"type": "select_custom",
"id": "select_species_list",
"source": "verificateur_species",
"multiple": true
*/

import { ServiceFactory } from "app/services/service-factory";
import { CalculatorType, FishSpecies, Session, Verificateur } from "jalhyd";
import { sprintf } from "sprintf-js";
import { FormulaireNode } from "../formulaire-node";
import { SelectEntry } from "./select-entry";
import { SelectField } from "./select-field";

// Vérificateur, liste d'espèces (choix multiple)
export class SelectFieldSpeciesList extends SelectField {
    constructor(parent: FormulaireNode) {
        super(parent);
        this._multiple = true;
    }

    protected populate() {
        // add UIDs of all Espece type Nubs in the session
        const especeNubs = Session.getInstance().getAllNubs().filter((element) => element.calcType === CalculatorType.Espece);
        for (const en of especeNubs) {
            const form = ServiceFactory.formulaireService.getFormulaireFromNubId(en.uid);
            const e = this.createOrGetEntry(this._entriesBaseId + en.uid, en.uid);
            e.intlInfo = { "isnub": true, "code": "INFO_VERIFICATEUR_CUSTOM_SPECIES", "id": en.uid };
            this.addEntry(e);
        }
        // add all FishSpecies
        for (let j = 1; j < Object.keys(FishSpecies).length / 2; j++) { // exclude "0" (SPECIES_CUSTOM)
            const spgId = FishSpecies[j].substring(FishSpecies[j].lastIndexOf("_") + 1);
            const e = this.createOrGetEntry(this._entriesBaseId + spgId, FishSpecies[j]);
            e.intlInfo = { "isnub": false, "code": FishSpecies[j] }
            this.addEntry(e);
        }
    }

    protected initSelectedValue() {
        const sl = (this.nub as Verificateur).speciesList;
        if (sl !== undefined) {
            this.setValueFromId(sl.map((s) => {
                const spgId = s.substring(s.lastIndexOf("_") + 1);
                return this._entriesBaseId + spgId;
            }));
        }
    }

    public updateLocalisation() {
        // do not override localisation done in populate()
        // ie. avoid what is done by SelectField.updateLocalisation()

        for (const e of this._entries) {
            if (e.intlInfo.isnub) {
                const id = e.intlInfo.id;
                const form = ServiceFactory.formulaireService.getFormulaireFromNubId(id);
                e.label = sprintf(
                    ServiceFactory.i18nService.localizeText(e.intlInfo.code),
                    form ? form.calculatorName : "*** form not loaded yet ***"
                );
            }
            else {
                e.label = ServiceFactory.i18nService.localizeText("INFO_ENUM_" + e.intlInfo.code);
            }
        }
    }
}
