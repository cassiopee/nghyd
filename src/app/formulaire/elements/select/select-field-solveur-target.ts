/*
    "id": "select_target_nub",
    "type": "select_custom",
    "source": "solveur_target"
*/

import { ServiceFactory } from "app/services/service-factory";
import { decodeHtml } from "../../../util/util";
import { Session, Solveur } from "jalhyd";
import { SelectEntry } from "./select-entry";
import { SelectField } from "./select-field";

// Solveur, module cible (à calculer)
export class SelectFieldSolverTarget extends SelectField {
    protected populate() {

        const fs = ServiceFactory.formulaireService;

        // find all Nubs having at least one link to another Nub's result
        const candidateNubs: any[] =
            Session.getInstance().getDownstreamNubs().concat(
                Session.getInstance().getUpstreamNubsHavingExtraResults()
            ).filter(
                (element, index, self) => self.findIndex((e) => e.uid === element.uid) === index
            );
        for (const cn of candidateNubs) {
            const nub = fs.getFormulaireFromId(cn.uid);
            if (nub) {
                const calc = nub.calculatorName;
                let label = calc;
                // calculated param
                if (cn.calculatedParam !== undefined) {
                    const varName = fs.expandVariableName(cn.calcType, cn.calculatedParam.symbol);
                    label += ` / ${varName} (${cn.calculatedParam.symbol})`;
                }
                this.addEntry(this.createOrGetEntry(this._entriesBaseId + cn.uid, cn.uid, decodeHtml(label)));
            } else {
                // silent fail, this Solveur nub was probably loaded before all the candidate nubs are done loading
            }
        }
    }

    protected initSelectedValue() {
        const ntc = (this.nub as Solveur).nubToCalculate;
        if (ntc !== undefined) {
            this.setValueFromId(this._entriesBaseId + ntc.uid);
        }
    }

    public updateLocalisation() {
        // do not override localisation done in populate()
        // ie. avoid what is done by SelectField.updateLocalisation()
    }
}
