import { ServiceFactory } from "app/services/service-factory";
import { PbCloison, PreBarrage } from "jalhyd";
import { SelectEntry } from "./select-entry";
import { SelectField } from "./select-field";

/*
    "id": "select_upstream_basin",
    "type": "select_custom",
    "source": "upstream_basin"
*/

// PbCloisons, bassin amont
export class SelectFieldUpstreamBasin extends SelectField {
    protected populate() {
        const pbWallU = this.parentForm.currentNub as PbCloison;
        const preBarrageU = pbWallU.parent as PreBarrage;
        const posDb = pbWallU.bassinAval?.findPositionInParent();

        // river upstream
        const e = this.createOrGetEntry(this._entriesBaseId + "none", undefined);
        e.intlInfo = "INFO_LIB_AMONT";
        this.addEntry(e);

        // all available basins, depending on current downstream basin
        for (const b of preBarrageU.bassins) {
            const pos = b.findPositionInParent();
            if (posDb === undefined || pos < posDb) {
                const e = this.createOrGetEntry(this._entriesBaseId + b.uid, b.uid);
                e.intlInfo = b.description;
                this.addEntry(e);
            }
        }
    }

    protected initSelectedValue() {
        const ub = (this.nub as PbCloison).bassinAmont;
        this.setValueFromId(this._entriesBaseId + (ub ? ub.uid : "none"));
    }

    public updateLocalisation() {
        for (const e of this._entries) {
            if (e.id === this._entriesBaseId + "none") {
                e.label = ServiceFactory.i18nService.localizeText(e.intlInfo);
            } else {
                e.label = ServiceFactory.i18nService.localizeMessage(e.intlInfo);
            }
        }
    }
}
