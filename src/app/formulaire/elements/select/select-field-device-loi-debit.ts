import { LoiDebit, ParallelStructure, StructureProperties, StructureType } from "jalhyd";
import { SelectField } from "./select-field";
import { SelectEntry } from "./select-entry";
import { FormulaireNode } from "../formulaire-node";

/*
    "id": "select_loidebit",
    "type": "select",
    "property": "loiDebit",
    "source": "device_loi_debit",
    "help": {
*/

export class SelectFieldDeviceLoiDebit extends SelectField {
    constructor(parent: FormulaireNode) {
        super(parent);
        this._associatedProperty = "loiDebit";
    }

    public parseConfig(json: {}, data?: {}) {
        super.parseConfig(json, data);
        this._configDefaultValue = json["default"];
    }

    /**
     * get this select's nub
     */
    protected get nub(): ParallelStructure {
        return super.nub as ParallelStructure;
    }

    /**
     * get discharge law linked to this select's nub
     */
    private get loiDebit(): LoiDebit {
        const child = this.nub.getChildren()[this.parent.indexAsKid()];
        return child.getPropValue("loiDebit");
    }

    protected populate() {
        // possible values depend on CalcType

        // get current structure type from appropriate Nub child
        const la = this.nub.getLoisAdmissibles();
        const loiDebit: LoiDebit = this.loiDebit;
        const stCode = StructureProperties.findCompatibleStructure(loiDebit, this.nub as ParallelStructure);
        const stName = StructureType[stCode];
        if (la[stName] !== undefined) {
            for (const ld of la[stName]) {
                const e: SelectEntry = this.createOrGetEntry(this._entriesBaseId + LoiDebit[ld], ld);
                this.addEntry(e);
            }
        }
    }

    protected initSelectedValue() {
        // current discharge law from linked nub
        this.findAndSetDefaultValue(LoiDebit[this.loiDebit]);
    }
}
