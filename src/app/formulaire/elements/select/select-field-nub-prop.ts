import { Props } from "jalhyd";
import { FormulaireNode } from "../formulaire-node";
import { SelectField } from "./select-field";

/*
  par exemple :
    "id": "select_regime",
    "type": "select",
    "property": "regime",
    "default": "Fluvial"

*/

// nub property values taken from an enum
// "standard" select that normally does not require customisation
export class SelectFieldNubProperty extends SelectField {
    constructor(parent: FormulaireNode) {
        super(parent);
    }

    public parseConfig(json: {}, data?: {}) {
        super.parseConfig(json, data);
        this._associatedProperty = json["property"];
        this._configDefaultValue = json["default"];
    }

    protected populate() {
        // find enum associated to property
        const enumClass = Props.enumFromProperty[this._associatedProperty];
        if (enumClass !== undefined) {
            // add one select entry per enum entry, in the enum order
            for (let j = 0; j < Object.keys(enumClass).length / 2; j++) {
                this.addEntry(this.createOrGetEntry(this._entriesBaseId + j, j));
            }
        }
    }

    protected initSelectedValue() {
        this.findAndSetDefaultValue();
    }
}
