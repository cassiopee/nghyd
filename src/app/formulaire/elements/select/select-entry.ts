export class SelectEntry {
    /**
     * id dans le fichier de config
     */
    private _id: string;

    /**
     * valeur associée à l'entrée
     */
    private _value: any;

    /**
     * texte affiché pour cette entrée
     */
    private _label: string;

    /**
     * informations (code, symbole, ...) de traduction
     */
    private _intlInfo: any;

    constructor(id: string, val: any, lbl?: string) {
        this._id = id;
        this._value = val;
        this._label = lbl;
    }

    get id(): string {
        return this._id;
    }

    get value(): any {
        return this._value;
    }

    get label(): string {
        return this._label;
    }

    set label(l: string) {
        this._label = l;
    }

    public get intlInfo(): any {
        return this._intlInfo;
    }

    public set intlInfo(i: any) {
        this._intlInfo = i;
    }
}
