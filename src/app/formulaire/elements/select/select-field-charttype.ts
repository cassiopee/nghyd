import { ChartType } from "app/results/chart-type";
import { I18nService } from "app/services/internationalisation.service";
import { FormulaireNode } from "../formulaire-node";
import { SelectEntry } from "./select-entry";
import { SelectField } from "./select-field";

export class SelectFieldChartType extends SelectField {
    private _entryValues: ChartType[] = [ChartType.Histogram, ChartType.Dots, ChartType.Scatter];

    constructor(parent: FormulaireNode, private intlService: I18nService) {
        super(parent);
        this._confId = "chart-type";
        this._multiple = false;
    }

    public get entryValues(): ChartType[] {
        return this._entryValues;
    }

    protected populate() {
        for (const v of this._entryValues) {
            const id: string = ChartType[v];
            this.addEntry(this.createOrGetEntry(id, v));
        }
    }

    protected initSelectedValue() {
        this.findAndSetDefaultValue();
    }

    public updateLocalisation() {
        for (const e of this._entries) {
            switch (e.value) {
                case ChartType.Histogram:
                    e.label = this.intlService.localizeText("INFO_PARAMFIELD_CHART_TYPE_HISTOGRAM");
                    break;

                case ChartType.Dots:
                    e.label = this.intlService.localizeText("INFO_PARAMFIELD_CHART_TYPE_DOTS");
                    break;

                case ChartType.Scatter:
                    e.label = "XY";
                    break;

                default:
                    throw new Error("SelectFieldChartType localisation not properly defined");
            }
        }

        this._label = this.intlService.localizeText("INFO_PARAMFIELD_CHART_TYPE");
    }
}
