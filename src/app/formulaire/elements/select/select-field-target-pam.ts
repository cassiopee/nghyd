import { ServiceFactory } from "app/services/service-factory";
import { decodeHtml } from "../../../util/util";
import { CalculatorType, MacrorugoRemous, Message, MessageCode, Session } from "jalhyd";
import { SelectField } from "./select-field";
import { FormulaireNode } from "../formulaire-node";
import { SelectEntry } from "./select-entry";
import { VERSION } from "@angular/core";

// Courbe de remous dans une passe à macro-rugo.
export class SelectFieldTargetPam extends SelectField {
    constructor(parent: FormulaireNode) {
        super(parent);
        this._associatedProperty = "nubMacroRugo";
        this._messageWhenEmpty = "INFO_MACRORUGOREMOUS_CREATE_PAM_FIRST";
    }

    protected populate() {

        const fs = ServiceFactory.formulaireService;

        // find all rock-ramp fishpass forms
        const macroForms: any[] = fs.formulaires.filter((element, index, self) =>
            element.calculatorType === CalculatorType.MacroRugo
        );
        for (const cn of macroForms) {
            const calc = cn.calculatorName;
            this.addEntry(this.createOrGetEntry(this._entriesBaseId + cn.uid, cn.uid, decodeHtml(calc)));
        }
    }

    protected initSelectedValue() {
        const MrUID = this.nub.getPropValue("nubMacroRugo")
        if (MrUID !== undefined) {
            this.setValueFromId(this._entriesBaseId + MrUID);
        }
    }

    public updateLocalisation() {
        // do not override localisation done in populate()
        // ie. avoid what is done by SelectField.updateLocalisation()
    }

    public get errorMessage(): Message {
        const v = this.getValue();
        if (v instanceof SelectEntry) {
            const nubUid = v.value;
            const nub = Session.getInstance().findNubByUid(nubUid);
            if (nub.resultHasMultipleValues()) {
                return new Message(MessageCode.ERROR_MACRORUGOREMOUS_VARIATED_TARGET_PAM);
            }
            else {
                return undefined;
            }
        }
    }
}
