import { FormulaireDefinition } from "app/formulaire/definition/form-definition";
import { ServiceFactory } from "app/services/service-factory";
import { Nub, Solveur } from "jalhyd";
import { FormulaireElement } from "../formulaire-element";
import { SelectField } from "./select-field";
import { SelectEntry } from "./select-entry";
import { FormulaireNode } from "../formulaire-node";

/*
    "id": "select_target_result",
    "type": "select",
    "property": "targettedResult",
    "source": "solveur_targetted_result",
    "default": ""
*/
export class SelectFieldSolverTargetedResult extends SelectField {
    constructor(parent: FormulaireNode) {
        super(parent);
        this._associatedProperty = "targettedResult";
    }

    protected populate() {
        // @WARNING for localisation, @see hack in this.updateLocalisation()
        // 1. calculated param
        const ntc = (this.nub as Solveur).nubToCalculate;
        if (ntc?.calculatedParam !== undefined) { // some nubs have no calculatedParam, for ex. SectionParam
            this.addEntry(this.createOrGetEntry(this._entriesBaseId + "none", ""));
        }
        // 2. extra results
        if (ntc?.resultsFamilies !== undefined) {
            for (const er of Object.keys(ntc.resultsFamilies)) {
                const e: SelectEntry = this.createOrGetEntry(this._entriesBaseId + er, er);
                this.addEntry(e);
            }
        }
    }

    protected initSelectedValue() {
        this.findAndSetDefaultValue();
    }

    public updateLocalisation() {
        FormulaireElement.prototype.updateLocalisation.call(this);
        for (const e of this._entries) {
            // @WARNING clodo hack for Solveur
            // 1. calculated param
            const nub: Nub = (this.parentForm as FormulaireDefinition).currentNub;
            const ntc = (nub as Solveur).nubToCalculate;
            if (e.value !== undefined && ntc !== undefined) {
                if (e.value === "" && ntc.calculatedParam !== undefined) {
                    const varName = ServiceFactory.formulaireService.expandVariableName(ntc.calcType, ntc.calculatedParam.symbol);
                    e.label = `${varName} (${ntc.calculatedParam.symbol})`;
                } else {
                    // 2. extra results
                    const varName = ServiceFactory.formulaireService.expandVariableName(ntc.calcType, e.value);
                    e.label = `${varName} (${e.value})`;
                }
            }
        }
    }
}
