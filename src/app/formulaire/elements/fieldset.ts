import {
    CalculatorType,
    ParamDefinition,
    IProperties,
    Observer,
    Nub,
    enumValueFromString,
    ParamValueMode
} from "jalhyd";

import { FormulaireElement } from "./formulaire-element";
import { Field } from "./field";
import { SelectField } from "./select/select-field";
import { NgParameter } from "./ngparam";
import { FieldsetContainer } from "./fieldset-container";
import { SelectFieldFactory } from "./select/select-field-factory";
import { FormulaireFixedVar } from "../definition/form-fixedvar";
import { SelectEntry } from "./select/select-entry";

export class FieldSet extends FormulaireElement implements IProperties {

    /** Nub associé */
    private _nub: Nub;

    /** fichier de configuration */
    private _jsonConfig: {};

    public get nub(): Nub {
        return this._nub;
    }

    get label(): string {
        let label = this._label;
        if (this.parent instanceof FieldsetContainer) {
            label += " n°" + String(this.parent.getFieldsetPosition(this) + 1);
        }
        return label;
    }

    public setNub(sn: Nub, update: boolean = true) {
        this._nub = sn;
        if (update) {
            this.updateFields(true);
        }
    }

    private addField(f: Field) {
        if (!f) {
            throw new Error("FieldSet.addField() : argument incorrect (undefined)");
        }
        if (this.kids.indexOf(f) === -1) {
            this.kids.push(f);
        }
    }

    /**
     * backup NgParameters kids
     * @returns a list of object representation with sybol, value, modification flag
     */
    public backupParameters(): any[] {
        const res: any[] = [];
        // for (const p of this.allFormElements) {
        for (const p of this._kids) {
            if (p instanceof NgParameter) {
                res.push(p.objectRepresentation());
            }
        }
        return res;
    }

    /**
     * restore NgParameters kids from backup list
     * @param backup list of NgParameter object representation
     * @see backupParameters
     */
    public restoreParameters(backup: any[]) {
        // for (const p of this.allFormElements) {
        for (const p of this._kids) {
            if (p instanceof NgParameter) {
                for (const bp of backup) {
                    if (p.symbol === bp.prmDef.symbol) {
                        p.loadObjectRepresentation(bp, false);
                        break;
                    }
                }
            }
        }
    }

    public get hasInputs(): boolean {
        for (const f of this.kids) {
            if (f instanceof NgParameter) {
                return true;
            }
        }
        return false;
    }

    public getInput(i: number): NgParameter {
        let n = 0;
        for (const f of this.kids) {
            if (f instanceof NgParameter) {
                if (n === i) {
                    return f;
                }
                n++;
            }
        }
    }

    private getOrCreateSelect(json: {}): SelectField {
        const id: string = json["id"];
        const res = this.getFormulaireNodeById(id);
        if (res === undefined || !(res instanceof SelectField)) {
            return SelectFieldFactory.newSelectField(json, this);
        }
        return res;
    }

    private parse_select(json: {}): SelectField {
        let ok: boolean = true;
        // in case select is associated to a property, check nub has the property
        const p: string = json["property"];
        if (p !== undefined) {
            ok = this.parentForm.getPropValue(p) !== undefined;
        }
        if (ok) {
            const res: SelectField = this.getOrCreateSelect(json);
            res.parseConfig(json);
            res.afterParseConfig();
            res.addObserver(this);
            return res;
        }
        return undefined;
    }

    public addPropertiesObserver(o: Observer) {
        this.nub.addPropertiesObserver(o);
    }

    private getNubParamFromSymbol(symbol: string): ParamDefinition {
        return this._nub.getParameter(symbol);
    }

    public get jsonConfig(): {} {
        return this._jsonConfig;
    }

    /**
     * crée un input
     * @param json definition de l'input, extrait du fichier de conf du module de calcul
     * @param node_type_filter filtre sur le type de noeud (input créé si undefined ou égal)
     * @param default_radio_config config du radio fixé/à varier/à calculer
     */
    private parse_input(json: {}): NgParameter {
        const input_id: string = json["id"];
        let res: NgParameter;
        let nubParam: ParamDefinition;
        try {
            nubParam = this.getNubParamFromSymbol(input_id);
            if (nubParam.visible) {
                res = this.parentForm.getOrCreateParam(nubParam, this);
                res.parseConfig(json);
            }
        } catch (e) {
            // parameter does not exist in Nub, it's normal, simply do not show it (silent fail)
        }
        return res;
    }

    private parseFields() {
        // parse children fields from config
        const fields = this._jsonConfig["fields"];
        for (const field_index in fields) {
            let field = fields[field_index];
            // detect short input syntax
            if (typeof field === "string") {
                field = {
                    "id": field
                };
            }
            const type = field["type"] || "input"; // "input" by default
            let param: Field;

            switch (type) {
                case "input":
                    param = this.parse_input(field);
                    // potentiellement undefined car certaines définitions de FieldSet comportent des paramètres
                    // qui ne sont pas tous affichés en même temps (cf. ouvrages //)
                    if (param) {
                        this.addField(param);
                    }
                    break;

                case "select":
                    param = this.parse_select(field);
                    if (param !== undefined) {
                        this.addField(param);
                    }
                    break;
            }
        }
    }

    private clearFields() {
        for (const n of this.kids) {
            n.removeObserver(this);
        }

        this.clearKids();
    }

    /**
     * Reloads the model values and properties, and reloads localisation strings
     */
    public updateFields(forceClear: boolean) {
        if (forceClear) {
            this.clearFields();
            this.parseFields();
        }

        this.updateLocalisation();

        // for all select fields known by the form, set selected value
        // from associated property
        if (this.parentForm instanceof FormulaireFixedVar) {
            for (const sel of this.parentForm.allSelectFields) {
                if (sel.hasAssociatedNubProperty) {  // ie. if select is a standard select
                    this.setSelectValueFromProperty(sel.id);
                }
            }
        }
    }

    /**
     * Reflects a property value in the interface, through the value of a <select> field, if this select exists
     */
    private setSelectValueFromProperty(selectId: string) {
        const selectField: SelectField = this.getFormulaireNodeById(selectId) as SelectField;
        if (selectField) {
            let propVal: any = this.getPropValue(selectField.associatedProperty);
            if (propVal === undefined) {
                propVal = ""; // clodo bullet-proof loading
            }
            const selectElement = selectField.getEntryFromValue(propVal);
            try {
                selectField.setValue(selectElement);
            } catch (e) {
                // silent fail, to avoid errors being thrown in Structure Nubs when
                // trying to apply same LoiDebit on new StructureType
                // console.error(`setSelectValueFromProperty: cannot set value ${propVal} on <select> ${selectId}`);
            }
        }
    }

    /**
     * Sets Nub default properties from config file, unless properties values are already set
     * (when deserialising an existing Nub)
     * @TODO default values should be held by model, not interface !
     */
    public parseConfig(json: {}, data?: {}) {
        this._jsonConfig = json;

        this._confId = json["id"];
        this._helpLink = json["help"];

        const ct: string = json["calcType"];
        const currentCt = this.getPropValue("calcType");
        const calc_type: CalculatorType = currentCt ? currentCt : (ct ? CalculatorType[ct] : this.parentForm.calculatorType);
        this.setPropValue("calcType", calc_type);

        // parse fields once, so that SelectField elements are present
        // when setting default properties below
        this.clearFields();
        this.parseFields();

        // for all select fields known by the form, apply default value
        // to associated property, usually from associated enum
        if (this.parentForm instanceof FormulaireFixedVar) {
            for (const sel of this.parentForm.allSelectFields) {
                if (sel.hasAssociatedNubProperty) { // ie. if select is a standard select
                    const prop = sel.associatedProperty;
                    const defaultValue = sel.configDefaultValue;
                    // Sets Nub default property, unless this property is already set
                    const currentValue = this.getPropValue(prop);
                    if (defaultValue !== undefined && currentValue === undefined) {
                        this.setPropValue(prop, enumValueFromString(prop, defaultValue));
                    }
                }
            }
        }
    }

    /**
     * retourne la valeur actuellement sélectionnée d'un SelectField (qui doit être affiché)
     * @param selectFieldId id du SelectField
     * @returns valeur courante du select sans le préfixe
     */
    public getSelectedValue(selectFieldId: string): string | string[] {
        for (const p of this.kids) {
            if (p instanceof SelectField && p.isDisplayed && p.id === selectFieldId) {
                if (!p.multiple) {
                    const value: string = (p.getValue() as SelectEntry).value;
                    return FormulaireElement.removePrefix(value, selectFieldId + "_");
                } else {
                    const values: string[] = (p.getValue() as SelectEntry[]).map((se) => {
                        return FormulaireElement.removePrefix(se.value, selectFieldId + "_");
                    });
                    return values;
                }
            }
        }
    }

    // interface Observer

    update(sender: any, data: any) {
        if (data.action) {
            switch (data.action) {
                case "select":
                    const senderId: string = sender.id.replace(/\d+$/, "");
                    if (senderId === "select_section") {
                        // sections paramétrées, courbes de remous, régimes uniformes
                        // "nodeType" is a property of the section child, not of the parent
                        const oldNodeType = this.getPropValue("nodeType");
                        if (oldNodeType !== data.value.value) { // avoid infinite loops
                            // manually notify parent so that it replaces the child Nub @WARNING clodo trick
                            this.parentForm.update(this, {
                                action: "propertyChange",
                                name: "nodeType",
                                value: data.value.value
                            });
                        }
                    }
                    else if (senderId === "select_pressurelosstype") {
                        // lois de perte de charge
                        // "pressureLossType" is a property of the child law, not of the parent
                        const oldLaw = this.nub.getPropValue("pressureLossType");
                        if (oldLaw !== data.value.value) { // avoid infinite loops
                            // manually notify parent so that it replaces the child Nub @WARNING clodo trick
                            this.parentForm.update(this, {
                                action: "propertyChange",
                                name: "pressureLossType",
                                value: data.value.value
                            });
                        }
                    } else {
                        if (data.value !== undefined) {
                            if (this.parentForm instanceof FormulaireFixedVar) {
                                // for all select fields known by the form, apply received value
                                // to associated property
                                for (const sel of this.parentForm.allSelectFields) {
                                    if (senderId === sel.id) {
                                        // find select element in parent form
                                        if (sel.hasAssociatedNubProperty) { // if select is a standard select
                                            const prop = sel.associatedProperty;
                                            // for multiple select
                                            if (Array.isArray(data.value)) {
                                                this.setPropValue(prop, data.value.map((v: any) => v.value));
                                            } else {
                                                this.setPropValue(prop, data.value.value);
                                            }
                                        }
                                    }
                                }
                            }
                            this.parentForm.resetResults();
                        }
                    }
                    break; // switch (data.action)
            }
        }
    }

    // interface IProperties

    public hasProperty(key: string): boolean {
        return this._nub.hasProperty(key);
    }

    /**
     * list of properties keys
     */
    public get keys(): string[] {
        return this._nub.keys;
    }

    /**
     * get associated nub property value
     * @param key property name
     */
    public getPropValue(key: string): any {
        return this.nub.getPropValue(key);
    }

    /**
     * assign associated nub property
     * @param key nub property name
     * @param val value to assign with
     * @returns true if property value has changed
     */
    public setPropValue(key: string, val: any): boolean {
        return this.nub.setPropValue(key, val, this);
    }
}
