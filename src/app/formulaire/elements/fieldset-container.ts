import { FormulaireElement } from "./formulaire-element";
import { FieldSet } from "./fieldset";
import { FieldsetTemplate } from "./fieldset-template";
import { FormulaireNode } from "./formulaire-node";
import { Nub } from "jalhyd";

export class FieldsetContainer extends FormulaireElement {
    private _template: FieldsetTemplate;

    public title: string;

    constructor(parent: FormulaireNode) {
        super(parent);
    }

    public get template(): FieldsetTemplate {
        return this._template;
    }

    public set template(fst: FieldsetTemplate) {
        if (this._template !== undefined) {
            throw new Error("template already set!");
        }
        this._template = fst;
    }

    public addFieldset(fs: FieldSet) {
        this.fieldsets.push(fs);
    }

    public getFieldsetPosition(fs: FieldSet) {
        let i = 0;
        for (const f of this.kids) {
            if (f.uid === fs.uid ) {
                return i;
            }
            i++;
        }
    }

    public moveFieldsetUp(fs: FieldSet) {
        let i = 0;
        for (const f of this.kids) {
            if (f.uid === fs.uid && i > 0) {
                const t = this.kids[i - 1];
                this.kids[i - 1] = this.kids[i];
                this.kids[i] = t;
                return;
            }
            i++;
        }
    }

    public moveFieldsetDown(fs: FieldSet) {
        let i = 0;
        for (const f of this.kids) {
            if (f.uid === fs.uid && i < this.kids.length - 1) {
                const t = this.kids[i];
                this.kids[i] = this.kids[i + 1];
                this.kids[i + 1] = t;
                return;
            }
            i++;
        }
    }

    public removeFieldset(fs: FieldSet) {
        const i = fs.indexAsKid();
        this.kids.splice(i, 1);
    }

    /**
     * crée un FieldSet à partir d'un template
     * @param templateIndex indice du template dans la liste
     * @param after insère le nouveau FieldSet après cette position, à la fin sinon
     * @param nub attaches the given Nub to the new FieldSet
     * @param extra extra key-value pairs to add to the "newFieldset" event
     */
    public addFromTemplate(after?: number, nub?: Nub, extra?: any): FieldSet {
        const inst: FieldSet = this._template.instantiateTemplate(this, after, nub, extra);

        this.updateLocalisation();

        // notification de création d'un FieldSet
        let info = {
            "action": "newFieldset",
            "fieldset": inst
        };
        info = { ...info, ...extra };
        this.notifyObservers(info, this);

        return inst;
    }

    public get fieldsets(): FieldSet[] {
        return this.kids as FieldSet[];
    }

    public parseConfig(json: {}, data?: {}) {
        this._confId = json["id"];
        this._helpLink = json["help"];
        const templates = data as any[];

        const templateNames: string[] = json["templates"];
        for (const t of templateNames) {
            for (const d of templates) {
                if (d.id === t) {
                    const tmpl = new FieldsetTemplate();
                    tmpl.parseConfig(d);
                    this.template = tmpl;
                }
            }
        }
    }
}
