import { JalhydObject, IObservable, Observer, Observable, ParamValueMode } from "jalhyd";

import { FormulaireElement } from "./formulaire-element";
import { DeepFormulaireElementIterator } from "../form-iterator/deep-element-iterator";
import { NgParameter } from "./ngparam";

/**
 * représentation sous forme d'arbre du formulaire et de ses éléments
 */
export abstract class FormulaireNode implements IObservable {

    /** aide en ligne */
    protected _helpLink: string;

    /** identifiant dans le fichier de conf */
    protected _confId: string;

    /** id numérique unique */
    private _uid: string;

    /** parent direct */
    private _parentNode: FormulaireNode;

    /** enfants (utilisé entre autres pour FormulaireDefinition, FieldSet et FieldsetContainer) */
    protected _kids: FormulaireNode[];

    /** implémentation par délégation de IObservable */
    private _observable: Observable;

    constructor(parent: FormulaireNode) {
        this._parentNode = parent;
        this._kids = [];
        this._uid = JalhydObject.nextUID;
        this._observable = new Observable();
    }

    get id(): string {
        return this._confId;
    }

    public get parent() {
        return this._parentNode;
    }

    public get kids(): FormulaireNode[] {
        return this._kids;
    }

    public clearKids() {
        this._kids = [];
    }

    public get uid() {
        return this._uid;
    }

    public get helpLink() {
        return this._helpLink;
    }

    /**
     * cherche un FormulaireNode par son id de conf
     */
    public getFormulaireNodeById(id: string): FormulaireNode {
        if (this.id === id) {
            return this;
        }

        for (const k of this._kids) {
            const res = k.getFormulaireNodeById(id);
            if (res !== undefined) {
                return res;
            }
        }
    }

    /**
     * cherche un FormulaireNode par son id numérique unique
     */
    public getFormulaireNodeByUid(uid: string): FormulaireNode {
        if (this.uid === uid) {
            return this;
        }

        for (const k of this._kids) {
            const res = k.getFormulaireNodeByUid(uid);
            if (res !== undefined) {
                return res;
            }
        }
    }

    /**
     * retourne l'indice d'un FormulaireNode enfant dans la liste des enfants
     * @param kid FormulaireNode enfant dont on cherche l'indice
     * @return -1 si non trouvé, indice commençant à 0 sinon
     */
    public kidIndex(kid: FormulaireNode): number {
        let n = 0;
        for (const k of this._kids) {
            if (k._uid === kid._uid) {
                return n;
            }
            n++;
        }

        return -1;
    }

    /**
     * retourne l'indice de this dans la liste des enfants de son parent
     * (cad rang par rapport à ses frères)
     */
    public indexAsKid(): number {
        return this._parentNode.kidIndex(this);
    }

    public abstract parseConfig(json: {}, data?: {});

    // interface IObservable

    /**
     * ajoute un observateur à la liste
     */
    public addObserver(o: Observer) {
        this._observable.addObserver(o);
    }

    /**
     * supprime un observateur de la liste
     */
    public removeObserver(o: Observer) {
        this._observable.removeObserver(o);
    }

    /**
     * itère sur tous les FormulaireElement
     */
    public get allFormElements(): IterableIterator<FormulaireElement> {
        return new DeepFormulaireElementIterator(this);
    }

    /**
     * notifie un événement aux observateurs
     */
    notifyObservers(data: any, sender?: any) {
        this._observable.notifyObservers(data, sender);
    }
}
