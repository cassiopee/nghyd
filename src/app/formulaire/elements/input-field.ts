import { Field } from "./field";

export abstract class InputField extends Field {
    private _value: any;

    public getValue() {
        return this._value;
    }

    public setValue(sender: any, val: any) {
        this._value = val;
    }
}
