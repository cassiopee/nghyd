import { Pab } from "jalhyd";

import { FormulaireElement } from "./formulaire-element";
import { FormulairePab } from "../definition/form-pab";

/**
 * The big editable data grid for calculator type "Pab" (form element).
 *
 * This is just a gateway between the model (FormPab and its underlying Pab)
 * and the user interface (PabTableComponent)
 */
export class PabTable extends FormulaireElement {

    public parseConfig(json: {}) {
        this._confId = json["id"];
    }

    /**
     * Returns the parent FormulairePab
     */
    public get form(): FormulairePab {
        return this.parentForm as FormulairePab;
    }

    /**
     * Returns the Pab model associated to the parent form
     */
    public get pab(): Pab {
        if (this.form) {
            return this.form.pabNub;
        }
    }
}
