import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component } from "@angular/core";
import { I18nService } from "../../services/internationalisation.service";

@Component({
    selector: "dialog-confirm-empty-session",
    templateUrl: "dialog-confirm-empty-session.component.html",
})
export class DialogConfirmEmptySessionComponent {

    constructor(
        public dialogRef: MatDialogRef<DialogConfirmEmptySessionComponent>,
        private intlService: I18nService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    public get uitextYes() {
        return this.intlService.localizeText("INFO_OPTION_START_NEW");
    }

    public get uitextNo() {
        return this.intlService.localizeText("INFO_OPTION_CANCEL");
    }

    public get uitextEmptySessionTitle() {
        return this.intlService.localizeText("INFO_EMPTY_SESSION_DIALOGUE_TITRE");
    }

    public get uitextEmptySessionBody() {
        return this.intlService.localizeText("INFO_EMPTY_SESSION_DIALOGUE_TEXT");
    }

}
