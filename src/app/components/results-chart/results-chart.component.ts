import { Component, ViewChild, AfterContentInit, ChangeDetectorRef, Input, OnChanges } from "@angular/core";

import { BaseChartDirective } from "ng2-charts";

import { Observer, ParamFamily, Result } from "jalhyd";

import { ChartTypeSelectComponent } from "./chart-type.component";
import { I18nService } from "../../services/internationalisation.service";
import { PlottableData } from "../../results/plottable-data";
import { ChartType } from "../../results/chart-type";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";
import { IYSeries } from "../../results/y-series";
import { VarResults } from "../../results/var-results";
import { fv } from "../../util/util";
import { AppComponent } from "../../app.component";

import zoomPlugin from 'chartjs-plugin-zoom';
import { Chart } from "chart.js";
import { SelectFieldChartType } from "app/formulaire/elements/select/select-field-charttype";

@Component({
    selector: "results-chart",
    templateUrl: "./results-chart.component.html",
    styleUrls: [
        "./results-chart.component.scss"
    ]
})
export class ResultsChartComponent extends ResultsComponentDirective implements AfterContentInit, Observer, OnChanges {

    @ViewChild('chartcanvas')
    private chartComponent: BaseChartDirective;

    private _results: PlottableData;

    /** used to briefly destroy/rebuild the chart component, to refresh axis labels (@see bug #137) */
    public displayChart = true;

    private _zoomWasChanged = false;

    @ViewChild(ChartTypeSelectComponent, { static: true }) // true, or else adding observer will fail in ngAfterContentInit()
    private _graphTypeComponent: ChartTypeSelectComponent;

    /*
     * config du graphique
     */
    public graph_type: string;
    public graph_data: any = {};
    public graph_options: any = {
        responsive: true,
        maintainAspectRatio: true,
        animation: {
            duration: 0
        },
        plugins: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: ""
            }
        },
        elements: {
            line: {
                tension: 0
            }
        }
    };

    public constructor(
        private intlService: I18nService,
        private cd: ChangeDetectorRef
    ) {
        super();
        Chart.register(zoomPlugin);
        // enable zoom and pan (using "chartjs-plugin-zoom" package)
        const that = this;
        this.graph_options["plugins"] = {
            zoom: {
                pan: {
                    enabled: false, // conflicts with drag zoom
                },
                zoom: {
                    drag: { // conflicts with pan; set to false to enable mouse wheel zoom,
                        enabled: true,
                        borderColor: "rgba(225,225,225,0.3)",
                        borderWidth: 1,
                        backgroundColor: "rgba(0,0,0,0.25)"
                    },
                    mode: "xy",
                    // percentage of zoom on a wheel event
                    // speed: 0.1,
                    onZoomComplete: function(t: any) { return function() { t.zoomComplete(); }; }(that)
                }
            }
        };
    }

    @Input()
    public set results(r: PlottableData) {
        this._results = r;
        if (this._results && this._graphTypeComponent) {
            this._graphTypeComponent.selectedValue = r.chartType;
        }
    }

    @Input()
    public set resultData(r: Result) {
        // trick to trigger onChanges when results data changes
    }

    @Input()
    public set variableIndex(v: number) {
        // trick to trigger onChanges when variable index changes
    }

    public get availableXAxis(): string[] {
        if (this._results) {
            return this._results.getAvailableXAxis();
        }
    }

    public get availableYAxis(): string[] {
        if (this._results) {
            if (this._results.chartType !== ChartType.Scatter) {
                // do not use real Y axis (that include families), if chart cannot display multiple series
                return this._results.getAvailableXAxis();
            } else {
                return this._results.getAvailableYAxis();
            }
        }
    }

    public get chartX() {
        if (this._results) {
            return this._results.chartX;
        }
    }

    public set chartX(X) {
        if (X !== this.chartX) {
            this._results.chartX = X;
            this.drawChart();
        }
    }

    public get chartY() {
        if (this._results) {
            return this._results.chartY;
        }
    }

    public set chartY(Y) {
        if (Y !== this.chartY) {
            this._results.chartY = Y;
            this.drawChart();
        }
    }

    /**
     * Returns a human readable description of any param / result symbol
     */
    public getChartAxisLabel(symbol: string): string {
        return this._results.getChartAxisLabel(symbol);
    }

    public get uitextSelectX() {
        return this.intlService.localizeText("INFO_PARAMFIELD_CHART_SELECT_X_AXIS");
    }

    public get uitextSelectY() {
        return this.intlService.localizeText("INFO_PARAMFIELD_CHART_SELECT_Y_AXIS");
    }

    public zoomComplete() {
        this._zoomWasChanged = true;
        this.cd.detectChanges();
    }

    public get zoomWasChanged(): boolean {
        return this._zoomWasChanged;
    }

    public ngOnChanges() {
        if (this._results instanceof VarResults) {
            this._results.updateCalculatedParameterHeader();
        }

        // redessiner le graphique chaque fois qu'une entrée change
        this.drawChart();
    }

    public drawChart() {
        if (this._results && this._results.hasPlottableResults()) {
            this.forceRebuild();
            switch (this._graphTypeComponent.selectedValue) {
                case ChartType.Histogram:
                    this.graph_type = "bar";
                    this.generateBarChart();
                    break;

                case ChartType.Dots:
                    this.graph_type = "line";
                    this.generateLineChart();
                    break;

                default:
                    this.graph_type = "scatter";
                    this.generateScatterChart();
                    break;
            }
        }
    }

    public ngAfterContentInit() {
        this._graphTypeComponent.addObserver(this);
    }

    /** forces Angular to rebuild the chart @see bugs #137, #465 */
    private forceRebuild() {
        this.displayChart = false;
        const that = this;
        setTimeout(() => { // trick
            that.displayChart = true;
        }, 10);
    }

    /**
     * Calls getChartAxisLabel() and removes the symbol prefix
     * (cannot rebuild a clean label here)
     */
    private axisLabelWithoutSymbol(symbol: string) {
        // detect children results
        const match = /^([0-9]+)_(.+)$/.exec(symbol);
        // get label with symbol
        let l = this._results.getChartAxisLabel(symbol);
        // remove symbol
        if (match === null) { // child prefix also uses ":"
            const i = l.indexOf(": ");
            if (i !== -1) {
                l = l.substring(i + 2);
            }
        }
        return l;
    }

    /**
     * génère les données d'un graphique de type "bar"
     */
    private generateBarChart() {
        const labs = [];
        const dat = [];
        const xSeries = this._results.getValuesSeries(this.chartX);
        const ySeries = this._results.getValuesSeries(this.chartY);

        // both series are supposed to be the same length
        for (let i = 0; i < xSeries.length; i++) {
            labs.push((xSeries[i] !== undefined) ? xSeries[i] : "");
            dat.push((ySeries[i] !== undefined) ? ySeries[i] : "");
        }

        this.graph_options["scales"] = {
            x: {
                gridLines: {
                    offsetGridLines: true
                },
                ticks: {
                    precision: ResultsComponentDirective.CHARTS_AXIS_PRECISION,
                    callback: function (value, index, values) {
                        return fv(Number(value));
                    }
                },
                title: {
                    display: true,
                    text: this.axisLabelWithoutSymbol(this.chartX)
                }
            },
            y: {
                title: {
                    display: true,
                    text: this.axisLabelWithoutSymbol(this.chartY)
                }
            }
        };

        const that = this;
        this.graph_options["tooltip"] = {
            displayColors: false,
            callbacks: {
                title: (tooltipItems) => {
                    return this.chartY + " = " + fv(Number(tooltipItems[0].formattedValue));
                },
                label: (tooltipItem) => {
                    const lines: string[] = [];
                    const nbLines = that._results.getVariatingParametersSymbols().length;
                    for (const v of that._results.getVariatingParametersSymbols()) {
                        const series = that._results.getValuesSeries(v);
                        const line = v + " = " + fv(series[tooltipItem.dataIndex]);
                        if (v === this.chartX) {
                            if (nbLines > 1) {
                                lines.unshift("");
                            }
                            lines.unshift(line);
                        } else {
                            lines.push(line);
                        }
                    }
                    return lines;
                }
            }
        };

        this.graph_data = {
            labels: labs,
            datasets: [{
                label: "",
                data: dat,
                backgroundColor: "rgba(0, 58, 128, 0.5)", // ResultsComponentDirective.distinctColors[0] with transparency
                showLine: "true"
            }]
        };
    }

    /**
     * génère les données d'un graphique de type "line"
     */
    private generateLineChart() {
        // same as bar chart (histogram)
        this.generateBarChart();
        // override style
        this.graph_data.datasets[0].backgroundColor = "rgba(0,0,0,0)";  // fill color under the line : transparent
        this.graph_data.datasets[0].borderColor = ResultsComponentDirective.distinctColors[0];
        this.graph_data.datasets[0].showLine = false;
    }

    /**
     * génère les données d'un graphique de type "scatter"
     */
    private generateScatterChart() {
        this.graph_data = {
            datasets: []
        };
        const ySeries = this.getYSeries(this.chartY);

        // are we dealing with multiple Y series ?
        const isMultiple = (ySeries.length > 1);

        // all series are supposed to be the same length
        for (const ys of ySeries) {
            this.graph_data.datasets.push({
                label: ys.label,
                data: ys.data,
                borderColor: ys.color,
                backgroundColor: "rgba(0,0,0,0)",  // fill color under the line : transparent
                showLine: "true"
            });
        }

        this.graph_options["scales"] = {
            x: {
                type: "linear",
                position: "bottom",
                ticks: {
                    precision: ResultsComponentDirective.CHARTS_AXIS_PRECISION
                },
                title: {
                    display: true,
                    text: this.axisLabelWithoutSymbol(this.chartX)
                }
            },
            y: {
                type: "linear",
                position: "left",
                ticks: {
                    precision: ResultsComponentDirective.CHARTS_AXIS_PRECISION
                },
                title: {
                    display: true,
                    text: this.axisLabelWithoutSymbol(this.chartY)
                }
            }
        };

        if (isMultiple) {
            // add legend for multiple series
            this.graph_options.plugins.legend = {
                display: true,
                position: "bottom",
                reverse: false
            };
            // remove tooltip
            delete this.graph_options.tooltip;
        } else {
            // enhanced tooltip for single series
            const that = this;
            this.graph_options.tooltip = {
                displayColors: false,
                callbacks: {
                    title: (tooltipItems) => {
                        return this.chartY + " = " + fv(Number(tooltipItems[0].formattedValue));
                    },
                    label: (tooltipItem) => {
                        let lines: string[] = [];
                        // 1. X if different from Y
                        if (this.chartX !== this.chartY) {
                            const xseries = that._results.getValuesSeries(this.chartX);
                            const xline = this.chartX + " = " + fv(xseries[tooltipItem.dataIndex]);
                            lines.push(xline);
                        }
                        // 2. variated parameters other than X or Y
                        const varLines: string[] = [];
                        for (const v of that._results.getVariatingParametersSymbols()) {
                            if (v !== this.chartX && v !== this.chartY) {
                                const series = that._results.getValuesSeries(v);
                                const line = v + " = " + fv(series[tooltipItem.dataIndex]);
                                varLines.push(line);
                            }
                        }
                        // blank line ?
                        if (varLines.length > 0) {
                            lines.push("");
                            lines = lines.concat(varLines);
                        }
                        return lines;
                    }
                }
            };
            // remove legend
            this.graph_options.plugins.legend = {
                display: false
            };
        }
    }

    /**
     * Returns a list of plottable data series for the given symbol; unless symbol
     * is a ParamFamily, the returned list will have only one value
     */
    private getYSeries(symbol: string): IYSeries[] {
        const ret: IYSeries[] = [];
        const palette = ResultsComponentDirective.distinctColors;
        const xSeries = this._results.getValuesSeries(this.chartX);
        let symbols: string[];

        // whole family of variables => multiple series (should only happen with VarResults)
        if (ParamFamily[symbol] !== undefined && this._results instanceof VarResults) {
            symbols = this._results.extractFamilies()[symbol];
        } else {
            symbols = [ symbol ];
        }
        // loop over found symbol(s)
        let i = 0;
        for (const s of symbols) {
            const dat: Array<{ x: number, y: number }> = [];
            const ySeries = this._results.getValuesSeries(s);
            // build fixed precision x/y coordinate pairs
            for (let j = 0; j < xSeries.length; j++) {
                dat.push({
                    x: xSeries[j],
                    y: ySeries[j]
                });
            }
            // add series
            ret.push({
                data: dat,
                label: this.axisLabelWithoutSymbol(s),
                color: palette[ i % palette.length ]
            });
            i++;
        }

        return ret;
    }

    public exportAsImage(element: HTMLDivElement) {
        AppComponent.exportAsImage(element.querySelector("canvas"));
    }

    public resetZoom() {
        this.chartComponent.chart.resetZoom();
        this._zoomWasChanged = false;
    }

    public get uitextResetZoomTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_RESET_ZOOM");
    }

    public get uitextExportImageTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXPORT_IMAGE");
    }

    public get uitextEnterFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_ENTER_FS");
    }

    public get uitextExitFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXIT_FS");
    }

    // interface Observer

    update(sender: any, data: any) {
        if (sender instanceof SelectFieldChartType) {
            if (data.action === "select") {
                this._results.chartType = data.value.value;
            }
        }
        this.drawChart();
    }
}
