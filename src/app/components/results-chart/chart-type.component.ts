import { Component } from "@angular/core";
import { IObservable, Observer } from "jalhyd";
import { I18nService } from "../../services/internationalisation.service";
import { ChartType } from "../../results/chart-type";
import { SelectFieldChartType } from "app/formulaire/elements/select/select-field-charttype";
import { SelectEntry } from "app/formulaire/elements/select/select-entry";
import { decodeHtml } from "../../util/util";

@Component({
    selector: "chart-type",
    templateUrl: "../generic-select/generic-select.component.html",
    styleUrls: [
        "./chart-type.component.scss"
    ]
})
export class ChartTypeSelectComponent implements IObservable {
    private _select: SelectFieldChartType;

    constructor(private intlService: I18nService) {
        this._select = new SelectFieldChartType(undefined, this.intlService);
        this._select.afterParseConfig(); // fill entries, set default value
        this._select.updateEntries();
    }

    public get entries(): ChartType[] {
        return this._select.entryValues;
    }

    public entryLabel(ct: ChartType): string {
        const entry = this._select.getEntryFromValue(ct);
        return decodeHtml(entry.label);
    }

    public get selectedValue(): ChartType {
        const val = this._select.getValue() as SelectEntry;
        return val.value;
    }

    public set selectedValue(ct: ChartType) {
        const entry = this._select.getEntryFromValue(ct);
        this._select.setValue(entry);
    }

    public get selectId() {
        return this._select.id;
    }

    public get label(): string {
        return this._select.label;
    }

    public get messageWhenEmpty(): string {
        return this._select.messageWhenEmpty;
    }

    public get enableHelpButton(): boolean {
        return false;
    }

    public get showClearButton(): boolean {
        return false;
    }

    public get isMultiple(): boolean {
        return this._select.multiple;
    }

    // interface IObservable

    /**
     * ajoute un observateur à la liste
     */
    addObserver(o: Observer) {
        this._select.addObserver(o);
    }

    /**
     * supprime un observateur de la liste
     */
    removeObserver(o: Observer) {
        this._select.removeObserver(o);
    }

    /**
     * notifie un événement aux observateurs
     */
    notifyObservers(data: any, sender?: any) {
        this._select.notifyObservers(data, sender);
    }
}
