import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component } from "@angular/core";
import { I18nService } from "../../services/internationalisation.service";

@Component({
    selector: "dialog-log-entries-details",
    templateUrl: "dialog-log-entries-details.component.html",
})
export class DialogLogEntriesDetailsComponent {

    constructor(
        public dialogRef: MatDialogRef<DialogLogEntriesDetailsComponent>,
        private intlService: I18nService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    public get uitextLogEntriesDetailsTitle() {
        return this.intlService.localizeText("INFO_TITREJOURNAL");
    }

    public get uitextClose() {
        return this.intlService.localizeText("INFO_OPTION_CLOSE");
    }
}
