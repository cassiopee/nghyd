import { Component, Input, OnInit } from "@angular/core";

import { SelectField } from "../../formulaire/elements/select/select-field";
import { SelectEntry } from "../../formulaire/elements/select/select-entry";
import { I18nService } from "../../services/internationalisation.service";
import { ApplicationSetupService } from "../../services/app-setup.service";
import { decodeHtml } from "../../util/util";

@Component({
    selector: "select-field-line",
    templateUrl: "../generic-select/generic-select.component.html",
    styleUrls: [
        "./select-field-line.component.scss"
    ]
})
export class SelectFieldLineComponent implements OnInit {

    /** aide en ligne */
    protected helpLink: string | { [key: string]: string };

    @Input()
    private _select: SelectField;

    public constructor(
        private i18nService: I18nService,
        private appSetupService: ApplicationSetupService
    ) {}

    public get selectId() {
        return this._select.id;
    }

    public get isMultiple(): boolean {
        return this._select.multiple;
    }

    public get entries(): SelectEntry[] {
        if (! this._select) {
            return [];
        }
        return this._select.entries;
    }

    public get messageWhenEmpty(): string {
        return this._select.messageWhenEmpty;
    }

    protected entryLabel(entry: SelectEntry): string {
        return decodeHtml(entry.label);
    }

    public get selectedValue(): SelectEntry | SelectEntry[] {
        return this._select.getValue();
    }

    public get isValid(): boolean {
        return (this._select.getValue() !== undefined);
    }

    public set selectedValue(v: SelectEntry | SelectEntry[]) {
        this._select.setValue(v);
    }

    public get label() {
        if (this._select) {
            return this._select.label;
        } else {
            return "";
        }
    }

    public openHelp($event: any) {
        let link;
        if (typeof this._select.helpLink === "string") {
            link = this._select.helpLink;
        } else { // object
            if (! this.isMultiple) { // @TODO manage multiple selections ?
                const cv = this._select.getValue() as SelectEntry;
                const entryId = cv.id.substring(this._select.entriesBaseId.length);
                link = this._select.helpLink[entryId];
            }
        }
        window.open("assets/docs/" + this.appSetupService.language + "/calculators/" + link, "_blank");
        $event.preventDefault();
        $event.stopPropagation();
        return false;
    }

    public get enableHelpButton() {
        if (this._select && this._select.helpLink) {
            if (typeof this._select.helpLink === "string") {
                return true;
            } else { // object
                if (! this.isMultiple) { // @TODO manage multiple selections ?
                    const cv = this._select.getValue() as SelectEntry;
                    const entryId = cv.id.substring(this._select.entriesBaseId.length);
                    return Object.keys(this._select.helpLink).includes(entryId);
                }
            }
        }
        return false;
    }

    public get showClearButton(): boolean {
        return this.isMultiple && this.selectedValue && ! (Array.isArray(this.selectedValue) && this.selectedValue.length === 0);
    }

    public get uitextOpenHelp() {
        return this.i18nService.localizeText("INFO_CALCULATOR_OPEN_HELP");
    }

    public get uitextAndOther() {
        return this.i18nService.localizeText("INFO_SELECT_MULTIPLE_AND_OTHER");
    }

    public get uitextAndOthers() {
        return this.i18nService.localizeText("INFO_SELECT_MULTIPLE_AND_OTHERS");
    }

    // called every time we navigate to the module
    ngOnInit(): void {
        this._select.updateEntries();
    }
}
