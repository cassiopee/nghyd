import { Component, ViewChild, ChangeDetectorRef, Input, OnChanges } from "@angular/core";

import { BaseChartDirective } from "ng2-charts";

import { I18nService } from "../../services/internationalisation.service";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";
import { IYSeries } from "../../results/y-series";
import { fv } from "../../util/util";
import { AppComponent } from "../../app.component";

import { Jet, Result } from "jalhyd";


import zoomPlugin from 'chartjs-plugin-zoom';
import { Chart } from "chart.js";

@Component({
    selector: "jet-trajectory-chart",
    templateUrl: "./jet-trajectory-chart.component.html",
    styleUrls: [
        "./jet-trajectory-chart.component.scss"
    ]
})
export class JetTrajectoryChartComponent extends ResultsComponentDirective implements OnChanges {

    @ViewChild('chartcanvas')
    private chartComponent: BaseChartDirective;

    private _results: Result;

    private _zoomWasChanged = false;

    private _varValuesLists: any = {};

    /** used to briefly destroy/rebuild the chart component, to refresh axis labels (@see bug #137) */
    public displayChart = true;

    /*
     * config du graphique
     */
    public graph_data: { datasets: any[] };
    public graph_options: any = {
        responsive: true,
        maintainAspectRatio: true,
        aspectRatio: 1.5,
        animation: {
            duration: 0
        },
        plugins: {
            legend: {
                display: true,
                position: "bottom",
                reverse: false
            },
            title: {
                display: true,
                text: this.intlService.localizeText("INFO_JET_TITRE_TRAJECTOIRE")
            }
        },
        elements: {
            line: {
                tension: 0
            }
        }
    };

    public constructor(
        private intlService: I18nService,
        private cd: ChangeDetectorRef
    ) {
        super();
        Chart.register(zoomPlugin);
        // do not move following block out of constructor or scale labels won't be rendered
        this.graph_options["scales"] = {
            x: {
                type: "linear",
                position: "bottom",
                ticks: {
                    precision: ResultsComponentDirective.CHARTS_AXIS_PRECISION
                },
                title: {
                    display: true,
                    text: this.intlService.localizeText("INFO_LIB_ABSCISSE")
                }
            },
            y: {
                type: "linear",
                position: "left",
                ticks: {
                    precision: ResultsComponentDirective.CHARTS_AXIS_PRECISION
                },
                title: {
                    display: true,
                    text: this.intlService.localizeText("INFO_LIB_ALTITUDE")
                }
            }
        };
        // enable zoom and pan (using "chartjs-plugin-zoom" package)
        const that = this;
        this.graph_options["plugins"] = {
            zoom: {
                pan: {
                    enabled: false, // conflicts with drag zoom
                },
                zoom: {
                    drag: { // conflicts with pan; set to false to enable mouse wheel zoom,
                        enabled: true,
                        borderColor: "rgba(225,225,225,0.3)",
                        borderWidth: 1,
                        backgroundColor: "rgba(0,0,0,0.25)"
                    },
                    mode: "xy",
                    // percentage of zoom on a wheel event
                    // speed: 0.1,
                    onZoomComplete: function(t: any) { return function() { t.zoomComplete(); }; }(that)
                }
            }
        };
        // format numbers in tooltip
        this.graph_options.tooltip = {
            displayColors: false,
            callbacks: {
                label: (tooltipItem) => {
                    return "(" + fv(Number(tooltipItem.label)) + ", " + fv(Number(tooltipItem.formattedValue)) + ")";
                }
            }
        };
    }

    /** forces Angular to rebuild the chart @see bug #137 */
    private forceRebuild() {
        this.displayChart = false;
        const that = this;
        setTimeout(() => { // trick
            that.displayChart = true;
        }, 10);
    }

    @Input()
    public set results(r: Result) {
        this.forceRebuild(); // used for forcing redefinition of x.min/max in generateScatterChart()
        this._results = r;
    }

    // redessine le graphique dès qu'une entrée change
    public ngOnChanges() {
        if (this._results) {
            const nub = this._results.sourceNub as Jet;
            const length = nub.variatingLength();
            // extract variable values list for legend
            if (nub.resultHasMultipleValues()) {
                for (const p of nub.parameterIterator) {
                    if (p.hasMultipleValues) {
                        this._varValuesLists[p.symbol] = [];
                        if (nub.calculatedParam === p) { // calculated
                            for (let i = 0; i < length; i++) {
                                this._varValuesLists[p.symbol].push(nub.result.resultElements[i].vCalc);
                            }
                        } else { // variating
                            const iter = p.getExtendedValuesIterator(length);
                            while (iter.hasNext) {
                                const nv = iter.next();
                                this._varValuesLists[p.symbol].push(nv.value);
                            }
                        }
                    }
                }
            }
            this.generateScatterChart();
        }
    }

    public zoomComplete() {
        this._zoomWasChanged = true;
        this.cd.detectChanges();
    }

    public get zoomWasChanged(): boolean {
        return this._zoomWasChanged;
    }

    /**
     * génère les données d'un graphique de type "scatter"
     */
    private generateScatterChart() {
        const ySeries = this.getYSeries();
        const nub = (this._results.sourceNub as Jet);

        this.graph_data = {
            datasets: []
        };

        // find greatest abscissa
        let greatestAbscissa = 0;
        for (const ys of ySeries) {
            if (ys.data.length > 0) {
                greatestAbscissa = Math.max(greatestAbscissa, ys.data[ys.data.length - 1].x);
            }
        }

        // adjust chart width
        this.graph_options.scales.x.min = 0;
        this.graph_options.scales.x.max = greatestAbscissa;

        // build Y data series
        for (const ys of ySeries) {
            if (ys.data.length > 0) {
                // add 2 points for related water line
                const lastY = ys.data[ys.data.length - 1].y;
                ys.data.push({ x: greatestAbscissa, y: lastY });
                ys.data.push({ x: 0, y: lastY });
                // push series config
                this.graph_data.datasets.push({
                    label: ys.label,
                    data: ys.data,
                    borderColor: ys.color, // couleur de la ligne
                    backgroundColor: "rgba(0,0,0,0)",  // couleur de remplissage sous la courbe : transparent
                    showLine: "true"
                });
            }
        }

        // draw bottom line
        this.graph_data.datasets.push({
            label: this.intlService.localizeText("INFO_JET_FOND"),
            data: [
                { x: 0, y: nub.prms.ZF.singleValue },
                { x: greatestAbscissa, y: nub.prms.ZF.singleValue }
            ],
            tension: 0,
            spanGaps: true,
            showLine: "true",
            fill: (nub.prms.ZF.singleValue > 0), // if ZF < 0 chartjs "fills" upwards until 0
            borderColor: "#753F00",
            backgroundColor: "#753F00",
            pointRadius: 0
        });
    }

    public exportAsImage(element: HTMLDivElement) {
        AppComponent.exportAsImage(element.querySelector("canvas"));
    }

    public resetZoom() {
        this.chartComponent.chart.resetZoom();
        this._zoomWasChanged = false;
    }

    public get uitextResetZoomTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_RESET_ZOOM");
    }

    public get uitextExportImageTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXPORT_IMAGE");
    }

    public get uitextEnterFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_ENTER_FS");
    }

    public get uitextExitFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXIT_FS");
    }

    private getYSeries(): IYSeries[] {
        const ret: IYSeries[] = [];
        const palette = ResultsComponentDirective.distinctColors;
        const nub = (this._results.sourceNub as Jet);
        const trajectories = nub.generateTrajectories();

        for (let i = 0; i < trajectories.length; i++) {
            const traj = trajectories[i];
            ret.push({
                label: trajectories.length === 1
                    ? this.intlService.localizeText("INFO_JET_TITRE_TRAJECTOIRE_ET_COTE_EAU")
                    : this.getLegendForSeries(i),
                color: palette[i % palette.length],
                // map to IYSeries format
                data: traj.map((t) => {
                    return {
                        x: t[0],
                        y: t[1]
                    };
                })
            });
        }
        return ret;
    }

    /**
     * Returns a label showing the boundary conditions values for
     * the given iteration
     * @param n index of the variating parameter(s) iteration
     */
    private getLegendForSeries(n: number): string {
        return Object.keys(this._varValuesLists).map((symbol) => {
            const values = this._varValuesLists[symbol];
            const val = fv(values[n]);
            return `${symbol} = ${val}`;
        }).join(", ");
    }
}
