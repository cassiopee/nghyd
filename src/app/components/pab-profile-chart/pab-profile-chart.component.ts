import { Component, ViewChild, ChangeDetectorRef, Input, OnChanges } from "@angular/core";

import { BaseChartDirective } from "ng2-charts";

import { I18nService } from "../../services/internationalisation.service";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";
import { PabResults } from "../../results/pab-results";
import { IYSeries } from "../../results/y-series";
import { fv, longestVarParam } from "../../util/util";
import { AppComponent } from "../../app.component";

import { CloisonAval, Cloisons, LoiDebit } from "jalhyd";

import { sprintf } from "sprintf-js";
import { CalculatorResults } from 'app/results/calculator-results';

import zoomPlugin from 'chartjs-plugin-zoom';
import { Chart } from "chart.js";
import { FormulaireService } from "app/services/formulaire.service";

@Component({
    selector: "pab-profile-chart",
    templateUrl: "./pab-profile-chart.component.html",
    styleUrls: [
        "./pab-profile-chart.component.scss"
    ]
})
export class PabProfileChartComponent extends ResultsComponentDirective implements OnChanges {

    @ViewChild('chartcanvas')
    private chartComponent: BaseChartDirective;

    private _results: PabResults;

    /** size of the longest variable value */
    private size = 0;

    /** inferred extended values list for each variating parameter */
    private varValues = [];

    private _zoomWasChanged = false;

    /*
     * config du graphique
     */
    public graph_data: { datasets: any[] };
    public graph_options: any = {
        responsive: true,
        maintainAspectRatio: true,
        aspectRatio: 1.5,
        animation: {
            duration: 0
        },
        plugins: {
            legend: {
                display: true,
                position: "bottom",
                reverse: false
            },
            title: {
                display: true,
                text: this.intlService.localizeText("INFO_PAB_TITRE_PROFIL")
            }
        },
        elements: {
            line: {
                tension: 0
            }
        }
    };

    public constructor(
        private intlService: I18nService,
        private formService: FormulaireService,
        private cd: ChangeDetectorRef
    ) {
        super();
        Chart.register(zoomPlugin);
        // do not move following block out of constructor or scale labels won't be rendered
        this.graph_options["scales"] = {
            x: {
                type: "linear",
                position: "bottom",
                ticks: {
                    precision: ResultsComponentDirective.CHARTS_AXIS_PRECISION
                },
                title: {
                    display: true,
                    text: this.intlService.localizeText("INFO_LIB_DISTANCE_AMONT")
                }
            },
            y: {
                type: "linear",
                position: "left",
                ticks: {
                    precision: ResultsComponentDirective.CHARTS_AXIS_PRECISION
                },
                title: {
                    display: true,
                    text: this.intlService.localizeText("INFO_LIB_COTE")
                }
            }
        };
        // enable zoom and pan (using "chartjs-plugin-zoom" package)
        const that = this;
        this.graph_options["plugins"] = {
            zoom: {
                pan: {
                    enabled: false, // conflicts with drag zoom
                },
                zoom: {
                    drag: { // conflicts with pan; set to false to enable mouse wheel zoom,
                        enabled: true,
                        borderColor: "rgba(225,225,225,0.3)",
                        borderWidth: 1,
                        backgroundColor: "rgba(0,0,0,0.25)"
                    },
                    mode: "xy",
                    // percentage of zoom on a wheel event
                    // speed: 0.1,
                    onZoomComplete: function(t: any) { return function() { t.zoomComplete(); }; }(that)
                }
            }
        };
        // format numbers in tooltip
        this.graph_options["tooltip"] = {
            displayColors: false,
            callbacks: {
                label: (tooltipItem) => {
                    return "(" + fv(Number(tooltipItem.label)) + ", " + fv(Number(tooltipItem.formattedValue)) + ")";
                }
            }
        };
    }

    @Input()
    public set results(r: PabResults) {
        this._results = r;
    }

    // redessine le graphique dès qu'une entrée change
    public ngOnChanges() {
        // pre-extract variable parameters values
        if (this._results) {
            this.varValues = [];
            // find longest list
            const lvp = longestVarParam(this._results.variatedParameters);
            this.size = lvp.size;
            // get extended values lists for each variable parameter
            for (const v of this._results.variatedParameters) {
                const vv = [];
                const iter = v.param.getExtendedValuesIterator(this.size);
                while (iter.hasNext) {
                    const nv = iter.next();
                    vv.push(fv(nv.value));
                }
                this.varValues.push(vv);
            }
            this.generateScatterChart();
        }
    }

    public zoomComplete() {
        this._zoomWasChanged = true;
        this.cd.detectChanges();
    }

    public get zoomWasChanged(): boolean {
        return this._zoomWasChanged;
    }

    /**
     * génère les données d'un graphique de type "scatter"
     */
    private generateScatterChart() {
        const ySeries = this.getYSeries();

        this.graph_data = {
            datasets: []
        };

        // build Y data series
        for (const ys of ySeries) {
            // push series config
            this.graph_data.datasets.push({
                label: ys.label,
                data: ys.data,
                borderColor: ys.color, // couleur de la ligne
                backgroundColor: "rgba(0,0,0,0)",  // couleur de remplissage sous la courbe : transparent
                showLine: "true",
                pointStyle: ys.pointStyle
            });
        }
    }

    public exportAsImage(element: HTMLDivElement) {
        AppComponent.exportAsImage(element.querySelector("canvas"));
    }

    public resetZoom() {
        this.chartComponent.chart.resetZoom();
        this._zoomWasChanged = false;
    }

    public get uitextResetZoomTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_RESET_ZOOM");
    }

    public get uitextExportImageTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXPORT_IMAGE");
    }

    public get uitextEnterFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_ENTER_FS");
    }

    public get uitextExitFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXIT_FS");
    }

    private getXSeries(): number[] {
        const data: number[] = [];
        // X is always wall abscissa
        for (const cr of this._results.cloisonsResults) {
            const x = cr.resultElement.getValue("x"); // any resultElement will do
            data.push(x);
        }
        const xdw = this._results.cloisonAvalResults.resultElement.getValue("x");
        data.push(xdw);
        return data;
    }

    private getYSeries(): IYSeries[] {
        const ret: IYSeries[] = [];
        const xs = this.getXSeries(); // abscissae
        const pabLength = xs[xs.length - 1] - xs[0];
        const pabLength5Pct = (pabLength * 5) / 100;
        const dw = (this._results.cloisonAvalResults.sourceNub as CloisonAval);
        const ZRAMdw = dw.prms.ZRAM.singleValue;

        // 1. radier (cotes amont et mi-bassin)
        const dataF: { x: number, y: number }[] = [];
        const pointStyles: string[] = [];
        // one data series for one device repeated throughout the basins
        const ddSeries: { x: number, y: number }[][] = [];
        // extend upstream
        dataF.push({
            x: xs[0] - pabLength5Pct,
            y: this._results.cloisonsResults[0].resultElement.getValue("ZRAM")
        });
        // regular walls
        for (let i = 0; i < this._results.cloisonsResults.length; i++) {
            const c = (this._results.result.sourceNub.getChildren()[i] as Cloisons);
            const cr = this._results.cloisonsResults[i];
            const ZRAM = cr.resultElement.getValue("ZRAM"); // any ResultElement will do
            const ZRMB = cr.resultElement.getValue("ZRMB"); // any ResultElement will do
            const halfLB = c.prms.LB.singleValue / 2;
            // ZRAM
            dataF.push({
                x: xs[i],
                y: ZRAM
            });
            // ZDV of each device…
            for (let sti = 0; sti < c.structures.length; sti ++) {
                const st = c.structures[sti];
                // init device series if it does not exist yet
                if (ddSeries[sti] === undefined) {
                    ddSeries[sti] = [];
                }
                // orifices have no relevant ZDV
                if (st.getPropValue("loiDebit") !== LoiDebit.OrificeSubmerged) {
                    // 2 points, to draw a segment
                    ddSeries[sti].push({
                        x: xs[i],
                        y: ZRAM
                    });
                    ddSeries[sti].push({
                        x: xs[i],
                        y: st.prms.ZDV.v
                    });
                    // 1 null point, to disjoin segments
                    ddSeries[sti].push({
                        x: xs[i],
                        y: null
                    });
                }
            }
            // ZRMB
            dataF.push({
                x: xs[i] + halfLB,
                y: ZRMB
            });
        }
        // downwall
        dataF.push({
            x: xs[ xs.length - 1 ],
            y: ZRAMdw
        });
        // ZDV of each device…
        for (let sti = 0; sti < dw.structures.length; sti ++) {
            const st = dw.structures[sti];
            // init device series if it does not exist yet
            if (ddSeries[sti] === undefined) {
                ddSeries[sti] = [];
            }
            // orifices have no relevant ZDV; lift gate will be drawn later for each series
            if (! [ LoiDebit.OrificeSubmerged, LoiDebit.VanLevLarinier, LoiDebit.VanLevVillemonte ]
                .includes(st.getPropValue("loiDebit"))
            ) {
                // 2 points, to draw a segment
                ddSeries[sti].push({
                    x: xs[ xs.length - 1 ],
                    y: ZRAMdw
                });
                ddSeries[sti].push({
                    x: xs[ xs.length - 1 ],
                    y: st.prms.ZDV.v
                });
                // 1 null point, to disjoin segments
                ddSeries[sti].push({
                    x: xs[ xs.length - 1 ],
                    y: null
                });
            }
        }
        // extend downstream
        dataF.push({
            x: xs[xs.length - 1] + pabLength5Pct,
            y: ZRAMdw
        });
        // add bottom series
        ret.push({
            data: dataF,
            label: this.intlService.localizeText("INFO_LIB_RADIER"),
            color: "#808080"
        });
        // add devices series with a different point style for each
        const psPalette = ResultsComponentDirective.distinctPointStyles;
        for (let ddi = 0; ddi < ddSeries.length; ddi++) {
            const ds = ddSeries[ddi];
            // series might have no eligible device, thus no point at all
            if (ds.length > 0) {
                ret.push({
                    data: ds,
                    label: sprintf(this.intlService.localizeText("INFO_LIB_PAB_CHART_SEUILS"), ddi + 1),
                    color: "#808080", // same as bottom line
                    pointStyle: psPalette[ddi]
                });
            }
        }

        // 2. séries
        const nbSeries = this._results.cloisonsResults[0].resultElements.length;
        const palette = ResultsComponentDirective.distinctColors;

        seriesLoop:
        for (let n = 0; n < nbSeries; n++) {
            // --------- build nth series ---------
            const dataN: { x: number, y: number }[] = [];
            let i = 0; // abscissa index

            // if this iteration has errors, do not try to draw it
            if (this._results.iterationHasError(n)) {
                continue seriesLoop;
            }

            // extend upstream
            dataN.push({
                x: xs[0] - pabLength5Pct,
                y: this._results.cloisonsResults[0].resultElements[n].vCalc
            });

            // walls
            for (const x of xs) {
                let Z1: number;
                let nextZ1: number;
                let isLastAbscissa = false;
                if (i < xs.length - 2) {
                    // regular walls
                    Z1 = this._results.cloisonsResults[i].resultElements[n].vCalc;
                    nextZ1 = this._results.cloisonsResults[i + 1].resultElements[n].vCalc;
                } else if (i === xs.length - 2) {
                    // last regular wall
                    Z1 = this._results.cloisonsResults[i].resultElements[n].vCalc;
                    nextZ1 = this._results.cloisonAvalResults.resultElements[n].vCalc;
                } else {
                    // downwall
                    Z1 = this._results.cloisonAvalResults.resultElements[n].vCalc;
                    nextZ1 = this._results.Z2[n];
                    isLastAbscissa = true;
                }

                // 2 points for each abscissa
                dataN.push({
                    x: x,
                    y: Z1
                });
                dataN.push({
                    x: x,
                    y: nextZ1
                });

                // draw lift gate if any
                if (isLastAbscissa) {
                    for (const st of dw.structures) {
                        if ([LoiDebit.VanLevLarinier, LoiDebit.VanLevVillemonte].includes(st.getPropValue("loiDebit"))) {
                            // skip a point to disjoin line
                            dataN.push({
                                x: x,
                                y: null
                            });
                            // draw gate between bottom and ZDV
                            dataN.push({
                                x: x,
                                y: ZRAMdw
                            });
                            dataN.push({
                                x: x,
                                y: this._results.cloisonAvalResults.resultElements[n].getValue("ZDV")
                            });
                            // skip a point to disjoin line
                            dataN.push({
                                x: x,
                                y: null
                            });
                            // back to last water line point
                            dataN.push({
                                x: x,
                                y: nextZ1
                            });
                        }
                    }
                }

                i++;
            }

            // extend downstream
            dataN.push({
                x: xs[xs.length - 1] + pabLength5Pct,
                y: this._results.Z2[n]
            });

            ret.push({
                data: dataN,
                label: (
                    this._results.variatedParameters.length > 0 ?
                    this.getLegendForSeries(n) :
                        this.formService.localizeText("ZW", this.formService.currentForm.calculatorType) // ligne d'eau
                ),
                color: palette[ n % palette.length ]
            });
        }

        return ret;
    }

    /**
     * Returns a label showing the boundary conditions values for
     * the given iteration
     * @param n index of the variating parameter(s) iteration
     */
    private getLegendForSeries(n: number): string {
        /* let i = 0;
        return this.varValues.map((vv) => {
            const vp = this._results.variatedParameters[i];
            i++;
            let value = "0";
            value = vv[n];
            return `${vp.symbol} = ${value}`;
        }).join(", "); */
        return CalculatorResults.variatingModalityLabel(this.varValues, this._results, n);
    }
}
