import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component, OnInit } from "@angular/core";

import { I18nService } from "../../services/internationalisation.service";

import { PbBassin } from "jalhyd";

@Component({
    selector: "dialog-new-pb-cloison",
    templateUrl: "dialog-new-pb-cloison.component.html",
    styleUrls: ["dialog-new-pb-cloison.component.scss"]
})
export class DialogNewPbCloisonComponent implements OnInit {

    /** the selected upstream basin */
    public upstreamIndex: number;

    /** the selected downstream basin */
    public downstreamIndex: number;

    /** list of connectable basins, plus river upstream / downstream */
    protected availableBasins: PbBassin[];

    constructor(
        public dialogRef: MatDialogRef<DialogNewPbCloisonComponent>,
        private i18nService: I18nService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.availableBasins = data.basins;
        this.upstreamIndex = 0;
        this.downstreamIndex = 0;
    }

    public get availableUpstreamIndexes(): number[] {
        // add river upstream as "0"
        const ab: number[] = [ 0 ];
        for (let i = 0; i < this.availableBasins.length; i++) {
            ab.push(i + 1);
        }
        return ab;
    }

    public get availableDownstreamIndexes(): number[] {
        const ab: number[] = [ ];
        for (let i = 0; i < this.availableBasins.length; i++) {
            ab.push(i + 1);
        }
        // add river downstream as "0"
        ab.push(0);
        return ab;
    }

    /**
     * Returns true if a basin is selectable in the possible upstream basins list,
     * considering which downstream basin is currently selected
     * @param index index of basin
     * @param downstream if true, inverts the test
     */
    public basinIsSelectable(index: number, downstream: boolean = false): boolean {
        if (downstream) {
            return (this.upstreamIndex === 0 || index > this.upstreamIndex);
        } else {
            return (this.downstreamIndex === 0 || index < this.downstreamIndex);
        }
    }

    public basinDescription(i: number, fallback: string): string {
        if (i === 0) {
            return fallback;
        } else {
            return this.i18nService.localizeText("INFO_PB_BASSIN_N") + i;
        }
    }

    // @TODO redundant with lists filtering, useless
    public get enableValidate(): boolean {
        return (
            this.upstreamIndex !== this.downstreamIndex
            || this.upstreamIndex === 0 // river upstream to river downstream direct connection is allowed
        );
    }

    public onValidate(close = true) {
        if (close) {
            this.dialogRef.close({
                up: this.upstreamIndex,
                down: this.downstreamIndex,
            });
        }
        return true;
    }

    public ngOnInit() {
        // this.initVariableValues();
    }

    public get uitextSelectBasins(): string {
        return this.i18nService.localizeText("INFO_PB_NEW_WALL_SELECT_BASINS");
    }

    public get uiTextUpstreambasin(): string {
        return this.i18nService.localizeText("INFO_PB_NEW_WALL_UP_BASIN");
    }

    public get uiTextDownstreambasin(): string {
        return this.i18nService.localizeText("INFO_PB_NEW_WALL_DOWN_BASIN");
    }

    public get uitextRiverUpstream(): string {
        return this.i18nService.localizeText("INFO_LIB_AMONT");
    }

    public get uitextRiverDownstream(): string {
        return this.i18nService.localizeText("INFO_LIB_AVAL");
    }

    public get uitextValidate() {
      return this.i18nService.localizeText("INFO_OPTION_VALIDATE");
    }

    public get uitextCancel() {
      return this.i18nService.localizeText("INFO_OPTION_CANCEL");
    }
}
