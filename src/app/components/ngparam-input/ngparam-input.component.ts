// cf. https://blog.thoughtram.io/angular/2016/07/27/custom-form-controls-in-angular-2.html

import { Component, ChangeDetectorRef, OnDestroy, Input, ElementRef } from "@angular/core";

import { Message, MessageCode, Observer } from "jalhyd";

import { I18nService } from "../../services/internationalisation.service";
import { NgParameter } from "../../formulaire/elements/ngparam";
import { GenericInputComponentDirective } from "../generic-input/generic-input.component";
import { ApplicationSetupService } from "../../services/app-setup.service";

@Component({
    selector: "ngparam-input",
    templateUrl: "../generic-input/generic-input.component.html",
    styleUrls: [
        "./ngparam-input.component.scss"
    ]
})
export class NgParamInputComponent extends GenericInputComponentDirective implements Observer, OnDestroy {

    @Input()
    public captureTabEvents: boolean;

    /**
     * true : edit parameter initial value (in CALC mode) instead of single value
     */
    public editInitValue: boolean;

    /**
     * paramètre géré
     */
    private get _paramDef(): NgParameter {
        return this.model;
    }

    /**
     * valeur intermédiaire nécessitée par le fait que toutes les valeurs numériques ne sont pas légales
     * pour NgParameter (l'affecter peut provoquer une exception) et qui permet de faire fonctionner la validation du modèle
     */
    private _tmp: number;

    constructor(
        intlService: I18nService,
        appSetupService: ApplicationSetupService,
        cdRef: ChangeDetectorRef,
        private element: ElementRef
    ) {
        super(cdRef, intlService, appSetupService);
        this.captureTabEvents = true;
        this.editInitValue = false;
    }

    /**
     * appelé avant le changement de modèle
     */
    protected beforeSetModel() {
        if (this._paramDef) {
            this._paramDef.removeObserver(this);
        }
    }

    /**
     * appelé après le changement de modèle
     */
    protected afterSetModel() {
        if (this._paramDef) {
            if (this.editInitValue) {
                this._tmp = this._paramDef.paramDefinition.initValue;
            }
            else {
                this._tmp = this._paramDef.getValue();
            }
            this._paramDef.addObserver(this);
        }
    }

    protected getModelValue(): any {
        return this._tmp;
    }

    protected setModelValue(sender: any, v: any) {
        this._tmp = v;
        try {
            if (this.editInitValue) {
                this._paramDef.setInitValue(sender, v);
            } else {
                this._paramDef.setValue(sender, v);
            }
        } catch (e) {
            // géré par validateModelValue()
        }
    }

    protected validateModelValue(v: any): { isValid: boolean, message: string } {
        let msg: string;
        let valid = false;

        if (! this._paramDef) {
            msg = "internal error, model undefined";
        } else {
            try {
                if (!this._paramDef.allowEmpty && v === undefined) {
                    throw new Message(MessageCode.ERROR_PARAMDEF_VALUE_UNDEFINED);
                }
                this._paramDef.checkValue(v);
                valid = true;
            } catch (e) {
                if (e instanceof Message) {
                    msg = this.intlService.localizeMessage(e);
                } else {
                    msg = "invalid value";
                }
            }
        }

        return { isValid: valid, message: msg };
    }

    private undefineModel() {
        if (this.getModelValue() !== undefined) {
            this.setModelValue(this, undefined);
        }
    }

    protected setModelValid(b: boolean) {
        if (!b) {
            this.undefineModel();
        }
        super.setModelValid(b);
    }

    protected setUIValid(b: boolean) {
        if (!b) {
            this.undefineModel();
        }
        super.setUIValid(b);
    }

    public update(sender: any, data: any): void {
        switch (data["action"]) {
            case "ngparamAfterValue":
            case "ngparamAfterInitValue":
                // on ne fait rien au cas où la modif vient de l'interface (on ne remet pas à jour _uiValue ce qui permet
                // de garder par ex le '.' si on supprime le '2' de '1.2')
                if (sender !== this) {
                    this._tmp = data["value"];
                    this.updateAndValidateUI();
                }
                break;

            // changement de valueMode du paramètre ou de valeur à laquelle il est lié
            case "valueModeChange":
            case "valueLinkChange":
                if (this._tmp !== data["value"]) {
                    this._tmp = data["value"];
                    this.updateAndValidateUI();
                }
                break;
        }
    }

    /**
     * Renvoie l'événement au composant du dessus
     */
    public onTabPressed(event, shift: boolean) {
        this.tabPressed.emit({ originalEvent: event, shift: shift });
        // stop event propagation ?
        if (this.captureTabEvents) {
            return false;
        } // else let it bubble !
    }

    public ngOnDestroy() {
        // résoudre le conflit en supprimant le code ajouté cad ne conserver que removeObserver()
        this._paramDef.removeObserver(this);
    }
}
