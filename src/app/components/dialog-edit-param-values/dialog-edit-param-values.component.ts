import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators, NgForm } from "@angular/forms";

import { I18nService } from "../../services/internationalisation.service";
import { NgParameter } from "../../formulaire/elements/ngparam";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";

import { sprintf } from "sprintf-js";

import { ParamValueMode, ExtensionStrategy } from "jalhyd";

import { fv } from "../../util/util";
import { ServiceFactory } from "app/services/service-factory";

@Component({
    selector: "dialog-edit-param-values",
    templateUrl: "dialog-edit-param-values.component.html",
    styleUrls: ["dialog-edit-param-values.component.scss"]
})
export class DialogEditParamValuesComponent implements OnInit {

    /** the related parameter to change the "variable" value of */
    public param: NgParameter;

    /** available value modes (min / max, list) */
    public valueModes: { value: ParamValueMode; label: string; }[];

    /** available decimal separators */
    public decimalSeparators: { label: string; value: string; }[];

    /** available extension strategies */
    public extensionStrategies: { value: ExtensionStrategy; label: string; }[];

    /** current decimal separator */
    public decimalSeparator: string;

    /** form for values list */
    public valuesListForm: FormGroup;

    /** when true, shows the values chart instead of the edit form */
    public viewChart = false;
    // chart config
    public chartData = {};
    public chartOptions;

    /** form for min/max/step values */
    @ViewChild("minMaxForm", { static: false })
    public minMaxForm: NgForm;

    constructor(
        public dialogRef: MatDialogRef<DialogEditParamValuesComponent>,
        private intlService: I18nService,
        private fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.param = data.param;
        // an explicit ReactiveForm is required for file input component
        const initialValue = (this.param.valueMode === ParamValueMode.LISTE ? this.valuesList : "");
        this.valuesListForm = this.fb.group({
            file: [""],
            valuesList: [ initialValue,
                [
                    Validators.required
                    // Validators.pattern(new RegExp(this.valuesListPattern)) // behaves weirdly
                ]
            ]
        });

        // available options for select controls
        this.valueModes = [
            {
                value: ParamValueMode.MINMAX,
                label: this.intlService.localizeText("INFO_PARAMMODE_MINMAX")
            },
            {
                value: ParamValueMode.LISTE,
                label: this.intlService.localizeText("INFO_PARAMMODE_LIST")
            }
        ];
        this.decimalSeparators = [
            {
                label: this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_SEPARATEUR_POINT"),
                value: "."
            },
            {
                label: this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_SEPARATEUR_VIRGULE"),
                value: ","
            }
        ];
        this.decimalSeparator = this.decimalSeparators[0].value;
        this.extensionStrategies = [
            {
                value: ExtensionStrategy.REPEAT_LAST,
                label: this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_EXT_STRATEGY_REPEAT_LAST")
            },
            {
                value: ExtensionStrategy.RECYCLE,
                label: this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_EXT_STRATEGY_RECYCLE")
            }
        ];

        // chart configuration
        this.chartOptions = {
            responsive: true,
            maintainAspectRatio: true,
            animation: {
                duration: 0
            },
            scales: {
                x: {
                    type: "linear",
                    position: "bottom",
                    ticks: {
                        precision: ResultsComponentDirective.CHARTS_AXIS_PRECISION
                    }
                },
                y: {
                    type: "linear",
                    position: "left",
                    ticks: {
                        precision: ResultsComponentDirective.CHARTS_AXIS_PRECISION
                    }
                }
            },
            elements: {
                line: {
                    tension: 0
                }
            },
            plugins: {
                legend: {
                    display: false
                },
                tooltip: {
                    callbacks: {
                        label: function (tooltipItem) {
                            return fv(Number(tooltipItem.formattedValue));
                        }
                    }
                }
            }
        };
    }

    // proxy to model values

    public get minValue() {
        return this.param.minValue;
    }

    public set minValue(v) {
        this.param.setMinValue(this, v);
    }

    public get maxValue() {
        return this.param.maxValue;
    }

    public set maxValue(v) {
        this.param.setMaxValue(this, v);
    }

    public get stepValue() {
        return this.param.stepValue;
    }

    public set stepValue(v) {
        this.param.setStepValue(this, v);
    }

    /**
     * regular expression pattern for values list validation (depends on decimal separator)
     */
    public get valuesListPattern() {
        // standard pattern for decimal separator "." : ^-?([0-9]*\.)?([0-9]+[Ee]-?)?[0-9]+$
        const escapedDecimalSeparator = (this.decimalSeparator === "." ? "\\." : this.decimalSeparator);
        const numberSubPattern = `-?([0-9]*${escapedDecimalSeparator})?([0-9]+[Ee]-?)?[0-9]+`;
        const re = `^${numberSubPattern}(${this.separatorPattern}${numberSubPattern})*$`;
        return re;
    }

    /**
     * accepted separator: everything but [numbers, E, +, -, decimal separator], any length
     */
    public get separatorPattern() {
        return "[^0-9-+Ee" + this.decimalSeparator  + "]+";
    }

    public get selectedValueMode() {
        return this.param.valueMode;
    }

    public set selectedValueMode(v) {
        this.param.valueMode = v;
    }

    public get selectedExtensionStrategy() {
        return this.param.paramDefinition.extensionStrategy;
    }

    public set selectedExtensionStrategy(es) {
        this.param.paramDefinition.extensionStrategy = es;
    }

    public get isMinMax() {
        return this.param.valueMode === ParamValueMode.MINMAX;
    }

    public get isListe() {
        return this.param.valueMode === ParamValueMode.LISTE;
    }

    public get numberOfValues(): number {
        if (this.isMinMax) {
            try {
                // .getInferredValuesList().length is too slow when nb values > 1000
                if (this.param.paramDefinition.step === 0) {
                    return 0;
                } else {
                    return Math.max(
                        0,
                        Math.ceil((this.param.paramDefinition.max - this.param.paramDefinition.min) / this.param.paramDefinition.step) + 1
                    );
                }
            } catch (e) {
                // min > max or something, silent fail
                return 0;
            }
        } else {
            // values are not set before closing modal
            return this.buildNumbersListFromString(this.valuesListForm.controls.valuesList.value).length;
        }
    }

    /**
     * renders model's numbers list as text values list (semicolon separated)
     */
    public get valuesList() {
        return (this.param.valueList || []).join(";");
    }

    /**
     * injects text values list into model's numbers list
     */
    public set valuesList(list: string) {
        const vals = this.buildNumbersListFromString(list);
        this.param.setValueList(this, vals);
    }

    public toggleViewChart() {
        // validate values before switching views ?
        if (!this.viewChart && this.isFormValid) {
                // toggle
            this.viewChart = true;
            }
        else
            this.viewChart = false;

        // refresh chart when displaying it only
        if (this.viewChart) {
            this.drawChart();
        }
    }

    /** returns true if form is valid */
    public get isFormValid(): boolean {
        var ret: boolean = false;
        switch (this.param.valueMode) {
            case ParamValueMode.LISTE:
                const status = this.validateValuesListString(this.valuesListForm.controls.valuesList.value);
                ret = status.ok;
                if (ret) {
                    this.valuesListForm.controls.valuesList.setErrors(null);
                    this.valuesList = this.valuesListForm.controls.valuesList.value;
                }
                else
                    this.valuesListForm.controls.valuesList.setErrors({ "model": status.message });
                break;

            case ParamValueMode.MINMAX:
                // return true when form is not initialised to avoid ExpressionChangedAfterItHasBeenCheckedError
                ret = this.minMaxForm === undefined ? true : this.minMaxForm.valid;
                break;
        }
        return ret;
    }

    public onCancel() {
        this.dialogRef.close({
            cancelled: true
        });
    }

    public onValidate() {
        if (this.isFormValid) {
            switch (this.param.valueMode) {
                case ParamValueMode.LISTE:
                    this.data.param.setValueList(this, this.param.valueList);
                    break;

                case ParamValueMode.MINMAX:
                    this.data.param.setMinValue(this, this.param.minValue);
                    this.data.param.setMaxValue(this, this.param.maxValue);
                    this.data.param.setStepValue(this, this.param.stepValue);
                    break;
            }
            this.dialogRef.close({
                cancelled: false
            });
        }
    }

    /**
     * Converts the string given by the form input to a numbers list ready
     * to be set as Parameter values
     */
    private buildNumbersListFromString(list: string): number[] {
        const vals = [];
        const separatorRE = new RegExp(this.separatorPattern);
        const parts = list.trim().split(separatorRE);
        parts.forEach((e) => {
            if (e.length > 0) {
                // ensure decimal separator is "." for Number()
                if (this.decimalSeparator !== ".") {
                    const re = new RegExp(this.decimalSeparator, "g");
                    e = e.replace(re, ".");
                }
                vals.push(Number(e));
            }
        });
        return vals;
    }

    /**
     * Returns { ok: true } if every element of list is a valid Number, { ok: false, message: "reason" } otherwise
     * @param list a string containing a list of numbers separated by this.separatorPattern
     */
    private validateValuesListString(list: string) {
        let message: string;
        // 1. validate against general pattern
        let ok = new RegExp(this.valuesListPattern).test(list);

        if (ok) {
            // 2. validate each value
            const separatorRE = new RegExp(this.separatorPattern);
            const parts = list.trim().split(separatorRE);
            for (let i = 0; i < parts.length && ok; i++) {
                let e = parts[i];
                if (e.length > 0) { // should always be true as separator might be several characters long
                    // ensure decimal separator is "." for Number()
                    if (this.decimalSeparator !== ".") {
                        const re = new RegExp(this.decimalSeparator, "g");
                        e = e.replace(re, ".");
                    }
                    // 2.1 check it is a valid Number
                    const n = (Number(e));
                    // 2.2 validate against model
                    let modelIsHappy = true;
                    try {
                        this.param.checkValue(n);
                    } catch (e) {
                        modelIsHappy = false;
                        message = sprintf(this.intlService.localizeText("ERROR_INVALID_AT_POSITION"), i + 1)
                            + " " + this.intlService.localizeMessage(e);
                    }
                    // synthesis
                    ok = (
                        ok
                        && !isNaN(n)
                        && isFinite(n)
                        && modelIsHappy
                    );
                }
            }
        } else {
            message = this.uitextMustBeListOfNumbers;
        }

        return { ok, message };
    }

    public onFileSelected(event: any) {
        if (event.target.files && event.target.files.length) {
            const fr = new FileReader();
            fr.onload = () => {
                this.valuesListForm.controls.valuesList.setErrors(null);
                // this.valuesList = String(fr.result);
                this.valuesListForm.controls.valuesList.setValue(String(fr.result));
            };
            fr.onerror = () => {
                fr.abort();
                throw new Error("Erreur de lecture du fichier");
            };
            fr.readAsText(event.target.files[0]);
        }
    }

    public onValueModeChange(event) {
        DialogEditParamValuesComponent.initVariableValues(this.param);
        this.initListForm();
    }

    /**
     * initialise si besoin le paramètre en mode varié avec des valeurs par défaut calculées
     */
    public static initVariableValues(param: NgParameter) {
        const canFill: boolean = !ServiceFactory.applicationSetupService.enableEmptyFieldsOnFormInit;
        // init min / max / step
        if (param.valueMode === ParamValueMode.MINMAX) {
            const pVal = param.getValue();
            if (param.minValue === undefined && canFill) {
                param.resetMinValue(undefined, pVal !== undefined ? param.getValue() / 2 : undefined);
            }
            if (param.maxValue === undefined && canFill) {
                param.resetMaxValue(undefined, pVal !== undefined ? param.getValue() * 2 : undefined);
            }
            let step = param.stepValue;
            if (step === undefined && canFill) {
                step = pVal !== undefined ? ((param.maxValue - param.minValue) / 20) : undefined;
                param.resetStepValue(undefined, step);
            }
        }
        // init values list
        else if (param.valueMode === ParamValueMode.LISTE) {
            if (param.valueList === undefined && canFill) {
                if (param.isDefined) {
                    param.resetValueList(this, [param.getValue()]);
                } else {
                    param.resetValueList(this, []);
                }
            }
        }
    }

    /**
     * initialise values list
     */
    private initListForm() {
        if (this.isListe) {
            // set form control initial value
            this.valuesListForm.controls.valuesList.setValue(this.valuesList);
        }
    }

    /**
     * (re)Génère le graphique d'évolution des valeurs
     */
    private drawChart() {
        const data = [];
        let i = 0;
        for (const v of this.param.valuesIterator) {
            data.push({
                x: i,
                y: v
            });
            i++;
        }
        this.chartData = {
            datasets: [{
                label: "",
                data: data,
                borderColor: "#808080", // couleur de la ligne
                backgroundColor: "rgba(0,0,0,0)",  // couleur de remplissage sous la courbe : transparent
                showLine: "true"
            }]
        };
    }

    public get uiTextModeSelection(): string {
        return this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_MODE");
    }

    public get uitextValeurMini(): string {
        return this.intlService.localizeText("INFO_PARAMFIELD_VALEURMINI");
    }

    public get uitextValeurMaxi(): string {
        return this.intlService.localizeText("INFO_PARAMFIELD_VALEURMAXI");
    }

    public get uitextPasVariation(): string {
        return this.intlService.localizeText("INFO_PARAMFIELD_PASVARIATION");
    }

    public get uitextCancel(): string {
        return this.intlService.localizeText("INFO_OPTION_CANCEL");
    }

    public get uitextValidate(): string {
        return this.intlService.localizeText("INFO_OPTION_VALIDATE");
    }

    public get uitextEditParamVariableValues(): string {
        return this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_TITLE");
    }

    public get uitextListeValeurs(): string {
        return this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_VALUES_FORMAT");
    }

    public get uitextMustBeANumber(): string {
        return this.intlService.localizeText("ERROR_PARAM_MUST_BE_A_NUMBER");
    }

    public get uitextMustBeListOfNumbers(): string {
        return sprintf(this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_VALUES_FORMAT_ERROR"), this.separatorPattern);
    }

    public get uitextDecimalSeparator(): string {
        return this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_SEPARATEUR_DECIMAL");
    }

    public get uitextImportFile(): string {
        return this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_IMPORT_FICHIER");
    }

    public get uitextExtensionStrategy(): string {
        return this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_EXT_STRATEGY");
    }

    public get uitextMustBePositive(): string {
      return this.intlService.localizeText("ERROR_PARAM_MUST_BE_POSITIVE");
    }

    public get uitextShowValuesChart(): string {
        if (this.viewChart) {
            return this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_EDIT_VALUES");
        } else {
            return this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER_SHOW_CHART");
        }
    }

    public ngOnInit() {
        this.initListForm();
    }
}
