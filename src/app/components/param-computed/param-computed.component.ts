import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { NgParameter } from "../../formulaire/elements/ngparam";
import { ParamCalculability } from "jalhyd";
import { DialogEditParamComputedComponent } from "../dialog-edit-param-computed/dialog-edit-param-computed.component";
import { I18nService } from "../../services/internationalisation.service";

@Component({
    selector: "param-computed",
    templateUrl: "./param-computed.component.html",
    styleUrls: [
        "./param-computed.component.scss"
    ]
})
export class ParamComputedComponent {

    @Input()
    public param: NgParameter;

    @Input()
    public title: string;

    /**
     * initial value before opening dialog
     */
    private initValueBackup;

    /**
     * id de l'input, utilisé notamment pour les tests
     */
    public get inputId() {
        let id = "calc_" + this.param.symbol;
        // if inside a child Nub, prefix with child position to disambiguate
        const nub = this.param.paramDefinition.parentNub;
        if (nub && nub.parent && nub.intlType) {
            id = nub.findPositionInParent() + "_" + id;
        }
        return id;
    }

    constructor(
        private editInitialValueDialog: MatDialog,
        private i18nService: I18nService
    ) { }

    public get isDicho() {
        return this.param.paramDefinition.calculability === ParamCalculability.DICHO;
    }

    public get infoText() {
        return NgParameter.preview(this.param.paramDefinition);
    }

    public openDialog() {
        this.initValueBackup = this.param.paramDefinition.initValue;
        // modification de la valeur initiale, sans avoir à remettre le mode de paramètre sur "fixé"
        const dialogRef = this.editInitialValueDialog.open(
            DialogEditParamComputedComponent,
            {
                data: {
                    param: this.param
                },
                autoFocus: false,
                disableClose: true
            }
        );
        dialogRef.afterClosed().subscribe(result => {
            if (result.cancel) {
                // use setInitValue() and not setValue() to prevent value mode from going back to SINGLE
                this.param.paramDefinition.setInitValue(this.initValueBackup);
            }
        });
    }

    public get uitextEditInitialValue() {
        return this.i18nService.localizeText("INFO_DIALOG_COMPUTED_VALUE_TITLE");
    }
}
