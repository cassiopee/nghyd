import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component } from "@angular/core";

import { config } from "jalhyd";
import { decode } from "he";

import { I18nService } from "../../services/internationalisation.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ServiceFactory } from "../../services/service-factory";

import { sprintf } from "sprintf-js";

@Component({
    selector: "dialog-load-session",
    templateUrl: "dialog-load-session.component.html",
    styleUrls: ["dialog-load-session.component.scss"]
})
export class DialogLoadSessionComponent {

    public calculators: any[] = [];

    public fileFormatVersion: string;

    public libFormatVersion: string;

    public file: any;

    public loadSessionForm: FormGroup;

    public dependenciesProblems: any[] = [];

    public emptyCurrentSession = false;

    /** flag showing that a file loading attempt has failed */
    private loadingError = false;

    /** flag showing that a file loading attempt has succeeded */
    private loadingComplete = false;

    constructor(
        public dialogRef: MatDialogRef<DialogLoadSessionComponent>,
        private intlService: I18nService,
        private fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.loadSessionForm = this.fb.group({
            file: [null, Validators.required]
        });
        this.libFormatVersion = config.serialisation.fileFormatVersion;
    }

    public selectAll() {
        for (const c of this.calculators) {
            c.selected = true;
        }
        // re-run dependency checking
        this.checkLinkedParamsAndModelsDependencies();
    }

    public selectNone() {
        for (const c of this.calculators) {
            c.selected = false;
        }
        // re-run dependency checking
        this.checkLinkedParamsAndModelsDependencies();
    }

    /**
     * Checks the dependencies between Nubs :
     *  - linked params depend on their targets
     *  - PabCloisons depend on their models
     */
    public checkLinkedParamsAndModelsDependencies() {
        this.dependenciesProblems = [];
        // for all checked Nubs
        this.calculators.forEach((c) => {
            if (c.selected) {
                // do all required nubs are checked ?
                c.requires.forEach((r) => {
                    if (!this.isCalculatorOrParentSelected(r)) {
                        const realUid = this.getUidOrParentUid(r);
                        const depTitle = this.getTitleFromUid(realUid);
                        this.dependenciesProblems.push({
                            requiring: c.title,
                            required: depTitle,
                            requiredUid: realUid,
                            message: c.title + " " + this.intlService.localizeText("INFO_REQUIRES") + " " + depTitle
                        });
                    }
                });
            }
        });
    }

    public fixDependencies() {
        for (const dp of this.dependenciesProblems) {
            this.selectRequiredModule(dp.requiredUid);
        }
    }

    private isCalculatorOrParentSelected(uid: string): boolean {
        let isSelected = false;
        this.calculators.forEach((c) => {
            if (c.uid === uid || c.children.includes(uid)) {
                isSelected = c.selected;
            }
        });
        return isSelected;
    }

    private getUidOrParentUid(uid: string): string {
        let realUid: string;
        this.calculators.forEach((c) => {
            if (c.uid === uid || c.children.includes(uid)) {
                realUid = c.uid;
            }
        });
        return realUid;
    }

    private getTitleFromUid(uid: string): string {
        let title: string;
        this.calculators.forEach((c) => {
            if (c.uid === uid) {
                title = c.title;
            }
        });
        return title;
    }

    private selectRequiredModule(uid: string) {
        this.calculators.forEach((c) => {
            if (c.uid === uid) {
                c.selected = true;
            }
        });
        // re-run dependency checking
        this.checkLinkedParamsAndModelsDependencies();
    }

    public async onFileSelected(event: any) {
      if (event.target.files && event.target.files.length) {
        this.file = event.target.files[0];
        // reinit file infos
        this.calculators = [];
        this.fileFormatVersion = "";
        // reinit flags
        this.loadingError = false;
        this.loadingComplete = false;

        const formService = ServiceFactory.formulaireService;
        try {
          const calcInfos: any = await formService.calculatorInfosFromSessionFile(this.file);
          this.fileFormatVersion = calcInfos.formatVersion;
          this.calculators = calcInfos.nubs;
          for (const n of this.calculators) {
            n.selected = true;
            // if no title was given, generate a default one
            if (! n.title) {
              n.title = decode(formService.getLocalisedShortTitleFromCalculatorType(n.type));
            }
          }
          this.loadingComplete = true;
        } catch (err) {
          console.error(err);
          this.loadingError = true;
        }
      }
    }

    public loadSession() {
        this.dialogRef.close({
            calculators: this.calculators,
            file: this.file,
            emptySession: this.emptyCurrentSession
        });
    }

    public get atLeastOneCheckboxSelected() {
        let ok = false;
        for (const c of this.calculators) {
            ok = (ok || c.selected);
        }
        return ok;
    }

    /**
     * @returns true if any problem occurred during file loading
     */
    public get fileProblem(): boolean {
        return (
            this.loadingError
            ||
            this.fileFormatVersionProblem()
            ||
            this.fileIsEmpty()
        );
    }

    private fileFormatVersionProblem(): boolean {
        return this.fileFormatVersion && (this.fileFormatVersion !== this.libFormatVersion);
    }

    private fileIsEmpty(): boolean {
        return this.loadingComplete && this.calculators.length === 0;
    }

    public get uitextLoad() {
        return this.intlService.localizeText("INFO_OPTION_LOAD");
    }

    public get uitextCancel() {
        return this.intlService.localizeText("INFO_OPTION_CANCEL");
    }

    public get uitextAll() {
        return this.intlService.localizeText("INFO_OPTION_ALL");
    }

    public get uitextNone() {
        return this.intlService.localizeText("INFO_OPTION_NONE");
    }

    public get uitextLoadSessionFilename() {
        return this.intlService.localizeText("INFO_DIALOG_LOAD_SESSION_FILENAME");
    }

    public get uitextLoadSessionTitle() {
        return this.intlService.localizeText("INFO_DIALOG_LOAD_SESSION_TITLE");
    }

    public get uitextFixMissingDependencies() {
        return this.intlService.localizeText("INFO_DIALOG_FIX_MISSING_DEPENDENCIES");
    }

    public get uitextEmptyCurrentSession() {
        return this.intlService.localizeText("INFO_DIALOG_EMPTY_CURRENT_SESSION");
    }

    public get uitextFileProblem() {
        if (this.loadingError) {
            return this.intlService.localizeText("INFO_DIALOG_ERROR_LOADING_FILE");
        }
        if (this.fileFormatVersionProblem()) {
            return sprintf(
                this.intlService.localizeText("INFO_DIALOG_FORMAT_VERSIONS_MISMATCH"),
                this.fileFormatVersion,
                this.libFormatVersion
            );
        }
        if (this.fileIsEmpty()) {
            return this.intlService.localizeText("INFO_DIALOG_FILE_IS_EMPTY");
        }
    }
}
