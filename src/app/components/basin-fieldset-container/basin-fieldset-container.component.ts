import { Component } from "@angular/core";

import { I18nService } from "../../services/internationalisation.service";
import { ApplicationSetupService } from "../../services/app-setup.service";
import { FieldsetContainerComponent } from "../fieldset-container/fieldset-container.component";
import { PrebarrageService } from "app/services/prebarrage.service";
import { ServiceFactory } from "app/services/service-factory";
import { FieldSet } from "app/formulaire/elements/fieldset";

@Component({
    selector: "basin-fieldset-container",
    templateUrl: "../fieldset-container/fieldset-container.component.html",
    styleUrls: [
        "../fieldset-container/fieldset-container.component.scss"
    ]
})
export class BasinFieldsetContainerComponent extends FieldsetContainerComponent {

    constructor(i18nService: I18nService, appSetupService: ApplicationSetupService, private predamService: PrebarrageService) {
        super(i18nService, appSetupService);
    }

    protected onFieldsetListChange() {
        // disable "add" button (and "how many children" select)
        setTimeout(() => // setTimeout to avoid ExpressionChangedAfterItHasBeenCheckedError
            this._fieldsetComponents.forEach(fs => {
                fs.showAddChildren = false;
                fs.showMoveArrows = false;
            }));
    }

    protected addSubNub(after: FieldSet, clone?: boolean): void {
        const fsIndex = this._container.kidIndex(after);
        this.predamService.copyBasinByIndex(fsIndex, ServiceFactory.applicationSetupService.enableEmptyFieldsOnFormInit);
    }

    public onRemoveFieldset(fs: FieldSet) {
        const fsIndex = this._container.kidIndex(fs);
        super.onRemoveFieldset(fs);
        this.predamService.deleteBasinByIndex(fsIndex, ServiceFactory.applicationSetupService.enableEmptyFieldsOnFormInit);
    }
}
