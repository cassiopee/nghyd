import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component } from "@angular/core";

import { I18nService } from "../../services/internationalisation.service";

import { sprintf } from "sprintf-js";

@Component({
    selector: "dialog-edit-pab",
    templateUrl: "dialog-edit-pab.component.html",
    styleUrls: ["dialog-edit-pab.component.scss"]
})
export class DialogEditPabComponent {

    /** variables eligible to modification */
    public availableVariables: any[];

    /** symbols of the variables eligible to modification */
    public availableVariableSymbols: string[];

    /** action to apply to the variable */
    public varAction: string;

    /** value to set to all occurrences of the variable */
    public valueToSet: number;

    /** delta to apply (add) to all occurrences of the variable */
    public delta: number;

    /** true if selected devices are contained in one unique column */
    public vertical: boolean;

    /** variable to modify on every selected object */
    private _variable: string;

    /** number of selected walls and devices */
    private selectedItemsAbstract: { walls: number, wallsDevices: number, devices: number };

    constructor(
        public dialogRef: MatDialogRef<DialogEditPabComponent>,
        private intlService: I18nService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.selectedItemsAbstract = this.data.selectedItemsAbstract;
        this.vertical = this.data.vertical;
        this.availableVariables = this.data.availableVariables;
        // console.log("AV VAR", this.availableVariables);
        this.availableVariableSymbols = this.availableVariables.map(av => av.value);
        this.variable = this.availableVariableSymbols[0];
        // default action
        this.varAction = "set-value";
        // example values for validation / avoiding using a placeholder
        this.valueToSet = this.availableVariables[0].first;
        this.delta = 0;
    }

    public get variable(): string {
        return this._variable;
    }

    // find available variable details from key
    private findVariableDetails(variable: string): any {
        let details: any;
        for (const av of this.availableVariables) {
            if (av.value === variable) {
                details = av;
            }
        }
        return details;
    }

    // preloads valueToSet depending on variable chosen
    public set variable(v: string) {
        this._variable = v;
        const av = this.findVariableDetails(v);
        this.valueToSet = av.first;
    }

    /** closes dialog and tells parent to apply modifications */
    public applyValues() {
        this.dialogRef.close({
            action: this.varAction,
            variable: this.variable,
            value: Number(this.valueToSet),
            delta: Number(this.delta),
            variableDetails: this.findVariableDetails(this.variable)
        });
    }

    public buttonDisabled(value: any, delta: any) {
        return (
            (this.varAction === "set-value" && !value.valid)
            || (this.varAction === "delta" && !delta.valid)
        );
    }

    /** returns a short text summing up what is selected */
    public get selectionAbstract() {
        let abstract = "";
        if (this.selectedItemsAbstract.walls > 0) {
            // devices outside of complete walls ?
            if (this.selectedItemsAbstract.devices > 0) {
                abstract = sprintf(
                    this.intlService.localizeText("INFO_DIALOG_EDIT_PAB_N_WALLS_P_DEVICES"),
                    this.selectedItemsAbstract.walls,
                    this.selectedItemsAbstract.devices,
                    this.selectedItemsAbstract.wallsDevices + this.selectedItemsAbstract.devices
                );
            } else {
                // only complete walls
                abstract = sprintf(
                    this.intlService.localizeText("INFO_DIALOG_EDIT_PAB_N_WALLS"),
                    this.selectedItemsAbstract.walls,
                    this.selectedItemsAbstract.wallsDevices
                );
            }
        } else {
            // no complete wall, devices only
            abstract = sprintf(
                this.intlService.localizeText("INFO_DIALOG_EDIT_PAB_N_DEVICES"),
                this.selectedItemsAbstract.devices
            );
        }
        return abstract;
    }

    public variableLabel(v: string) {
        const av = this.findVariableDetails(v);
        return av.label;
    }

    public get interpolationEnabled() {
        const varDetails = this.findVariableDetails(this.variable);
        return (
            this.vertical
            && (this.selectedItemsAbstract.devices + this.selectedItemsAbstract.wallsDevices) > 1
            && (varDetails.occurrences > 1)
            && (["ZRAM", "ZRMB", "ZDV"].includes(this.variable))
        );
    }

    public get interpolationBounds() {
        let bounds = "";
        if (this.interpolationEnabled) {
            const varDetails = this.findVariableDetails(this.variable);
            if (varDetails) {
                bounds = sprintf(
                    this.intlService.localizeText("INFO_DIALOG_EDIT_PAB_INTERPOLATION_BOUNDS"),
                    varDetails.first,
                    varDetails.last
                );
            }
        }
        return bounds;
    }

    public get uitextEditPabTitle() {
        return this.intlService.localizeText("INFO_DIALOG_EDIT_PAB_TITLE");
    }

    public get uitextVariable() {
        return this.intlService.localizeText("INFO_DIALOG_EDIT_PAB_OPTION_VARIABLE");
    }

    public get uitextSetValue() {
        return this.intlService.localizeText("INFO_DIALOG_EDIT_PAB_OPTION_SET_VALUE");
    }

    public get uitextDelta() {
        return this.intlService.localizeText("INFO_DIALOG_EDIT_PAB_OPTION_DELTA");
    }

    public get uitextInterpolate() {
        return this.intlService.localizeText("INFO_DIALOG_EDIT_PAB_OPTION_INTERPOLATE");
    }

    public get uitextSetValueInput() {
        return this.intlService.localizeText("INFO_DIALOG_EDIT_PAB_SET_VALUE_INPUT");
    }

    public get uitextDeltaInput() {
        return this.intlService.localizeText("INFO_DIALOG_EDIT_PAB_DELTA_INPUT");
    }

    public get uitextMustBeANumber(): string {
        return this.intlService.localizeText("ERROR_PARAM_MUST_BE_A_NUMBER");
    }

    public get uitextValidate() {
        return this.intlService.localizeText("INFO_OPTION_VALIDATE");
    }

    public get uitextCancel() {
        return this.intlService.localizeText("INFO_OPTION_CANCEL");
    }

}
