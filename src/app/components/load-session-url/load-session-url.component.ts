import { Location } from "@angular/common";
import { Component, forwardRef, Inject } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute } from "@angular/router";
import { AppComponent } from "app/app.component";
import { FormulaireService } from "app/services/formulaire.service";
import { HttpService } from "app/services/http.service";
import { DialogConfirmLoadSessionURLComponent } from "../dialog-confirm-load-session-url/dialog-confirm-load-session-url.component";
import { DialogShowMessageComponent } from "../dialog-show-message/dialog-show-message.component";

// load a session file by its URL (either local or remote)

@Component({
    selector: "load-session-url",
    template: ""
})
export class LoadSessionURLComponent {

    constructor(
        @Inject(forwardRef(() => AppComponent)) private appComponent: AppComponent,
        private dialog: MatDialog,
        private route: ActivatedRoute,
        private httpService: HttpService,
        private location: Location,
        private formulaireService: FormulaireService
    ) {
    }

    ngOnInit() {
        // check open calculators
        if (this.formulaireService.formulaires.length > 0) {
            this.confirmLoadSession().then(emptySession => {
                if (emptySession === undefined) {
                    // cancel has been clicked, go to previous route
                    this.location.back();
                }
                else {
                    if (emptySession) {
                        this.appComponent.doEmptySession();
                    }
                    this.loadSession();
                }
            });
        }
        else {
            this.loadSession();
        }
    }

    private loadSession() {
        // get "path" argument from URL
        const path = this.route.snapshot.params.path;

        if (path.startsWith("http")) {
            // general URL path

            // example URLs:
            // http://localhost:4200/#/loadsession/https%3A%2F%2Fhydraulique.g-eau.fr%2Fcassiopee%2Fdevel%2Fapp%2Fexamples%2Fpab-complete-chain.json
            // http://localhost/dist/#/loadsession/https%3A%2F%2Fhydraulique.g-eau.fr%2Fcassiopee%2Fdevel%2Fapp%2Fexamples%2Fpab-complete-chain.json
            // turned by loadRemoteSession() to
            // http://localhost/gofetch.php?url=https%3A%2F%2Fhydraulique.g-eau.fr%2Fcassiopee%2Fdevel%2Fapp%2F%2Fexamples%2Fpab-complete-chain.json
            this.loadRemoteSession(path);
        }
        else {
            // local path

            // input URL example : http://localhost:4200/#/loadsession/app%2Fexamples%2Fpab-complete-chain.json
            // extracted path : app/examples/pab-complete-chain.json

            this.loadLocalSession(path);
        }
    }

    private async loadRemoteSession(path: string) {
        try {
            const url = "assets/scripts/gofetch.php?url=" + encodeURIComponent(path);
            this.httpService.httpGetRequestPromise(url).then(resp => {
                const s = JSON.stringify(resp);
                this.appComponent.loadSessionFile(s);
            });
        } catch (e) {
            // display error dialog
            await this.openErrorDialog(path);
            // go to previous route
            this.location.back();
        }
    }

    /**
     * load a locally stored session file
     * @param path local path in the form eg. app/examples/pab-complete-chain.json
     */
    private async loadLocalSession(path: string) {
        try {
            const d = await this.httpService.httpGetBlobRequestPromise(path);
            const f: any = new Blob([d], { type: "application/json" });
            this.appComponent.loadSessionFile(f);
        } catch (e) {
            // display error dialog
            await this.openErrorDialog(path);
            // go to previous route
            this.location.back();
        }
    }

    private async openErrorDialog(path: string) {
        const dialogRef = this.dialog.open(
            DialogShowMessageComponent,
            {
                data: {
                    title: "Error!",
                    message: "Session " + path + " does not exist."
                },
                disableClose: true
            }
        );

        // wait for dialog to be closed
        await dialogRef.afterClosed().toPromise();
    }

    private confirmLoadSession(): Promise<boolean> {
        const dialogRef = this.dialog.open(
            DialogConfirmLoadSessionURLComponent,
            {
                data: {
                },
                disableClose: true
            }
        );

        return dialogRef.afterClosed().toPromise().then(result => {
            return result.emptySession;
        });
    }
}
