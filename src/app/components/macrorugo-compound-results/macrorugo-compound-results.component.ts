import { Component, Input } from "@angular/core";

import { Result, cLog, Message, MessageCode, MessageSeverity, MRCInclination } from "jalhyd";

import { fv } from "../../../app/util/util";

import { CalculatorResults } from "../../results/calculator-results";
import { NgParameter } from "../../formulaire/elements/ngparam";
import { ApplicationSetupService } from "../../services/app-setup.service";
import { I18nService } from "../../services/internationalisation.service";
import { MacrorugoCompoundResults } from "../../results/macrorugo-compound-results";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";
import { LogHelper } from "app/results/log-helper";

@Component({
    selector: "macrorugo-compound-results",
    templateUrl: "./macrorugo-compound-results.component.html",
    styleUrls: [
        "./macrorugo-compound-results.component.scss"
    ]
})
export class MacrorugoCompoundResultsComponent extends ResultsComponentDirective {

    /** résultats non mis en forme */
    private _mrcResults: MacrorugoCompoundResults;

    private _logHelper: LogHelper;

    constructor(
        private appSetupService: ApplicationSetupService,
        private i18nService: I18nService,
    ) {
        super();
    }

    @Input()
    public set results(rs: CalculatorResults[]) {
        this._mrcResults = undefined;
        this._logHelper = undefined;
        if (rs.length > 0 && rs[0] instanceof MacrorugoCompoundResults) {
            this._mrcResults = rs[0] as MacrorugoCompoundResults;
            this._logHelper = new LogHelper(this._mrcResults);
        }
    }

    public get globalLog(): cLog {
        return this._logHelper.globalLog;
    }

    public get iterationLog(): cLog {
        return this._logHelper.iterationLog;
    }

    public get mrcResults() {
        return this._mrcResults;
    }

    public formattedValue(p: NgParameter): string {
        const nDigits = this.appSetupService.displayPrecision;
        return p.getValue().toFixed(nDigits);
    }

    public get hasResults(): boolean {
        return this._mrcResults && this._mrcResults.hasResults;
    }

    public get hasDisplayableResults(): boolean {
        let ret = this._mrcResults && this._mrcResults.hasResults;
        if (
            this._mrcResults
            && this._mrcResults.variatedParameters
            && this._mrcResults.variatedParameters.length > 0
        ) {
            ret = ret
            && this._mrcResults.variableIndex !== undefined
            && this._mrcResults.result.resultElements[this._mrcResults.variableIndex] !== undefined
            && this._mrcResults.result.resultElements[this._mrcResults.variableIndex].ok;
        }
        return ret;
    }

    public get uitextGeneralLogTitle(): string {
        return this.i18nService.localizeText("INFO_TITREJOURNAL_GLOBAL");
    }

    public get uitextLateralInclination(): string {
        return this.i18nService.localizeText("INFO_MACRORUGOCOMPOUND_LINCL");
    }

    public get isInclined(): boolean {
        return (
            this.mrcResults
            && this.mrcResults.result
            && this.mrcResults.result.sourceNub
            && this.mrcResults.result.sourceNub.getPropValue("inclinedApron") === MRCInclination.INCLINED
        );
    }

    public get lateralInclination(): string {
        const lincl = this.mrcResults.result.resultElements[this._mrcResults.variableIndex].values.LIncl;
        return fv(lincl);
    }

}
