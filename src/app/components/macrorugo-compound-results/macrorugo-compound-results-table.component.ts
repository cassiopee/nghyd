import { Component, ViewChild, ElementRef, Input, OnChanges } from "@angular/core";

import { MacroRugo } from "jalhyd";

import { ApplicationSetupService } from "../../services/app-setup.service";
import { I18nService } from "../../services/internationalisation.service";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";
import { MacrorugoCompoundResults } from "../../results/macrorugo-compound-results";
import { AppComponent } from "../../app.component";

@Component({
    selector: "macrorugo-compound-results-table",
    templateUrl: "./macrorugo-compound-results-table.component.html",
    styleUrls: [
        "./macrorugo-compound-results-table.component.scss"
    ]
})
export class MacrorugoCompoundResultsTableComponent extends ResultsComponentDirective implements OnChanges {

    /** résultats non mis en forme */
    @Input()
    public results: MacrorugoCompoundResults;

    /** index de l'élément de résultat à afficher (modifié par le sélecteur de conditions limites) */
    @Input()
    public variableIndex = 0;

    /** entêtes des colonnes */
    private _headers: string[];

    /** résultats mis en forme */
    private _dataSet: any[];

    @ViewChild("tableContainer")
    table: ElementRef;

    constructor(
        protected appSetupService: ApplicationSetupService,
        protected intlService: I18nService
    ) {
        super();
    }

    public ngOnChanges() {
        // rebuild dataset every time results or variableIndex change
        this._dataSet = [];
        if (
            this.results
            && this.results.childrenResults
            && this.results.childrenResults.length > 0
            && ! this.results.hasOnlyErrors()
        ) {
            const pr = this.results;
            const nDigits = this.appSetupService.displayPrecision;
            // when a parameter is variating, index of the variating parameter
            // values to build the data from

            // refresh headers here if language changed
            this._headers = pr.headers;

            // lines 1 - n-1 (aprons)
            for (let i = 0; i < pr.childrenResults.length; i++) {
                // protect loop contents with if(vCalc) ? Will hide erroneous apron results..
                const res = pr.childrenResults[i].resultElements[this.variableIndex].values;
                const nub = (pr.childrenResults[i].sourceNub as MacroRugo);
                // does ZF1 or B vary ?
                let zf1: number;
                try {
                    if (nub.prms.ZF1.hasMultipleValues) {
                        zf1 = nub.prms.ZF1.getInferredValuesList()[this.variableIndex];
                    } else {
                        zf1 = nub.prms.ZF1.singleValue;
                    }
                } catch (e) {
                    // silent fail
                }
                let b: number;
                try {
                    if (nub.prms.B.hasMultipleValues) {
                        b = nub.prms.B.getInferredValuesList()[this.variableIndex];
                    } else {
                        b = nub.prms.B.singleValue;
                    }
                } catch (e) {
                    // silent fail
                }
                let Y: number;
                try {
                    if (nub.prms.Y.hasMultipleValues) {
                        Y = nub.prms.Y.getInferredValuesList()[this.variableIndex];
                    } else {
                        Y = nub.prms.Y.singleValue;
                    }
                } catch (e) {
                    // silent fail
                }

                this._dataSet.push([
                    i + 1, // n° radier
                    (zf1 !== undefined ? zf1.toFixed(nDigits) : "-"),
                    (b !== undefined ? b.toFixed(nDigits) : "-"),
                    (Y !== undefined ? Y.toFixed(nDigits) : "-"),
                    (res.Q !== undefined ? res.Q.toFixed(nDigits) : "-"),
                    res.Vdeb.toFixed(nDigits),
                    res.Fr?.toFixed(nDigits),
                    res.Vg?.toFixed(nDigits),
                    res.Vmax?.toFixed(nDigits),
                    res.PV.toFixed(nDigits),
                    this.intlService.localizeText("INFO_ENUM_MACRORUGOFLOWTYPE_" + res.ENUM_MacroRugoFlowType),
                    res.xCenter.toFixed(nDigits)
                ]);
            }

            // line n (total flow)
            this._dataSet.push([
                this.intlService.localizeText("INFO_LIB_TOTAL"),
                "", "", "",
                pr.result.resultElements[this.variableIndex].vCalc.toFixed(nDigits),
                "", "", "", "", "", ""
            ]);
        }
    }

    public get headers() {
        return this._headers;
    }

    /**
     * Returns a combination of parameters and results for mat-table
     */
    public get dataSet() {
        return this._dataSet;
    }

    public exportAsSpreadsheet() {
        AppComponent.exportAsSpreadsheet(this.table.nativeElement);
    }

    public get uitextExportAsSpreadsheet() {
        return this.intlService.localizeText("INFO_RESULTS_EXPORT_AS_SPREADSHEET");
    }

    public get uitextEnterFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_ENTER_FS");
    }

    public get uitextExitFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXIT_FS");
    }
}
