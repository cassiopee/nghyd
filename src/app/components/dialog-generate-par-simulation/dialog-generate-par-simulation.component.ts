import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component } from "@angular/core";

import { I18nService } from "../../services/internationalisation.service";
import { MultiDimensionResults } from "../../results/multidimension-results";
import { fv, longestVarParam } from "../../util/util";

@Component({
    selector: "dialog-generate-par-simulation",
    templateUrl: "dialog-generate-par-simulation.component.html",
    styleUrls: ["dialog-generate-par-simulation.component.scss"]
})
export class DialogGeneratePARSimulationComponent {

    public selectedValue: number;

    /** résultats de la ParCalage */
    private _results: MultiDimensionResults;

    /** size of the longest variable value */
    private size = 0;

    /** inferred extended values list for each variating parameter */
    private varValues = [];

    constructor(
        public dialogRef: MatDialogRef<DialogGeneratePARSimulationComponent>,
        private intlService: I18nService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this._results = data.results;
        this.selectedValue = 0;

        if (this._results) {
            // pre-extract variable parameters values
            this.varValues = [];
            // find longest list
            const lvp = longestVarParam(this._results.variatedParameters);
            this.size = lvp.size;
            // get extended values lists for each variable parameter
            for (const v of this._results.variatedParameters) {
                const vv = [];
                const iter = v.param.getExtendedValuesIterator(this.size);
                while (iter.hasNext) {
                    const nv = iter.next();
                    vv.push(fv(nv.value));
                }
                this.varValues.push(vv);
            }
        }
    }

    public generatePARSimulation() {
        this.dialogRef.close({
            generate: true,
            selected: this.selectedValue,
            size: this.size
        });
    }

    public get uitextDescription() {
        return this.intlService.localizeText("INFO_DIALOG_PARSIM_DESC");
    }

    public get uitextGeneratePARSimulation() {
        return this.intlService.localizeText("INFO_CALCULATOR_RESULTS_GENERATE_PAR_SIMULATION");
    }

    public get uitextGenerate() {
        return this.intlService.localizeText("INFO_OPTION_GENERATE");
    }

    public get uitextCancel() {
        return this.intlService.localizeText("INFO_OPTION_CANCEL");
    }

    public get entries(): number[] {
        const ret: number[] = [];
        for (let i = 0; i < this.size; i++) {
            ret.push(i);
        }
        return ret;
    }

    protected entryLabel(index: number): string {
        const kv = [];
        for (let i = 0; i < this.varValues.length; i++) {
            const vv = this.varValues[i];
            const vp = this._results.variatedParameters[i];
            let symbol = vp.param.symbol;
            // is vp a parameter of a child Nub ?
            if (
                vp.param.parentNub
                && vp.param.parentNub !== vp.param.originNub
            ) {
                const pos = vp.param.parentNub.findPositionInParent() + 1;
                symbol = this.intlService.localizeText("INFO_LIB_RADIER_N_COURT") + pos + "_" + symbol;
            }
            kv.push(`${symbol} = ${vv[index]}`);
        }
        return kv.join(", ");
    }

    public get label() {
        return this.intlService.localizeText("INFO_PARAMFIELD_BOUNDARY_CONDITIONS");
    }

}
