import { Component, Input, AfterViewInit, Output, EventEmitter } from "@angular/core";
import { NgParameter } from "../../formulaire/elements/ngparam";
import { DialogEditParamValuesComponent } from "../dialog-edit-param-values/dialog-edit-param-values.component";
import { MatDialog } from "@angular/material/dialog";
import { ParamValueMode, Observer, Structure } from "jalhyd";
import { I18nService } from "../../services/internationalisation.service";

@Component({
    selector: "param-values",
    templateUrl: "./param-values.component.html",
    styleUrls: [
        "./param-values.component.scss"
    ]
})
export class ParamValuesComponent implements AfterViewInit, Observer {

    @Input()
    public param: NgParameter;

    @Input()
    public title: string;

    /**
     * événement signalant un changement de valeur du modèle
     */
    @Output()
    protected change = new EventEmitter<any>();

    constructor(
        private editValuesDialog: MatDialog,
        private i18nService: I18nService
    ) { }

    public get isMinMax() {
        return this.param.valueMode === ParamValueMode.MINMAX;
    }

    public get isListe() {
        return this.param.valueMode === ParamValueMode.LISTE;
    }

    public get infoText() {
        return NgParameter.preview(this.param.paramDefinition);
    }

    public get inputId() {
        let id = "var_" + this.param.symbol;
        // if inside a child Nub, prefix with child position to disambiguate
        const nub = this.param.paramDefinition.parentNub;
        if (nub && nub.parent && nub.intlType) {
            id = nub.findPositionInParent() + "_" + id;
        }
        return id;
    }

    public openDialog() {
        // initialisation du paramètre en mode varié
        // (fait avant d'ouvrir le dialogue pour éviter le message ExpressionChangedAfterItHasBeenCheckedError lié au paramètre)
        DialogEditParamValuesComponent.initVariableValues(this.param);
        // modification des valeurs variables
        const dlgRef = this.editValuesDialog.open(
            DialogEditParamValuesComponent,
            {
                disableClose: true,
                data: {
                    param: this.param
                },
                autoFocus: false,
                panelClass: "dialogMinWidth320px"
            }
        );
        dlgRef.afterClosed().toPromise().then(result => {
            if (result.cancelled) {
                this.change.emit({
                    action: "cancelvar"
                });
            } else {
                this.change.emit({
                    action: "okvar"
                });
            }
        });
    }

    public ngAfterViewInit() {
        // subscribe to parameter values change (through dialog actions)
        this.param.addObserver(this);
    }

    /**
     * événement de changement de la valeur du modèle
     */
    private emitModelChanged() {
        this.change.emit({
            action: "model",
            value: this.param.getValue(),
            symbol: this.param.symbol
        });
    }

    public update(sender: any, data: any): void {
        switch (data.action) {
            case "ngparamAfterMinValue":
            case "ngparamAfterMaxValue":
            case "ngparamAfterStepValue":
            case "ngparamAfterListValue":
                // tell the form to clear the results
                this.emitModelChanged();
                break;
        }
    }

    public get uitextEditValues(): string {
        return this.i18nService.localizeText("INFO_PARAMFIELD_PARAMVARIER_EDIT_VALUES");
    }

}
