import { Component, Input, Inject, forwardRef } from "@angular/core";

import { I18nService } from "../../services/internationalisation.service";
import { AppComponent } from "../../app.component";

@Component({
    selector: "quicknav",
    templateUrl: "./quicknav.component.html",
    styleUrls: [
        "./quicknav.component.scss"
    ]
})
export class QuicknavComponent {

    public static prefix = "qn_";

    @Input()
    public items: string[];

    @Input()
    public currentItem: string;

    @Input()
    public align: string;

    public constructor(
        @Inject(forwardRef(() => AppComponent)) private appComponent: AppComponent,
        private i18nService: I18nService
    ) {
        this.items = [];
        this.currentItem = "";
        this.align = "center";
    }

    public get id() {
        return QuicknavComponent.prefix + this.currentItem;
    }

    public get hasItems() {
        return this.items.length > 0;
    }

    public get alignStyle() {
        return {
            "text-align": this.align
        };
    }

    public label(item: string = "") {
        return this.i18nService.localizeText("INFO_QUICKNAV_" + item.toUpperCase());
    }

    public isCurrent(item: string) {
        return item === this.currentItem;
    }

    public scrollTo(item: string) {
        this.appComponent.scrollToQuicknav(item);
    }
}
