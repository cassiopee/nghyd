import { Component } from "@angular/core";

import { FixedVarResultsComponent } from "../fixedvar-results/fixedvar-results.component";
import { FixedResults } from "../../results/fixed-results";
import { VarResults } from "../../results/var-results";

@Component({
    selector: "jet-results",
    templateUrl: "./jet-results.component.html",
    styleUrls: [
        "./jet-results.component.scss"
    ]
})
export class JetResultsComponent extends FixedVarResultsComponent {

    public get hasResults(): boolean {
        return (
            (this._fixedResults?.hasResults)
            ||
            (this._varResults?.hasResults)
        );
    }

    public get hasValidResults(): boolean {
        return this.hasResults && (
            this._fixedResults?.result?.sourceNub?.result?.ok
            ||
            this._varResults?.result?.sourceNub?.result?.ok
        );
    }

    public get trajectoryResults(): FixedResults | VarResults {
        // draw chart whether params are variating or not,
        // hence different Results object for each case
        if (this._varResults && this._varResults.hasResults) {
            return this._varResults;
        } else {
            return this._fixedResults;
        }
    }
}
