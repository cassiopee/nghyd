import { Component, OnInit, isDevMode } from "@angular/core";

import { ParamDomainValue, Observer, ParamDomain } from "jalhyd";

import { ApplicationSetupService } from "../../services/app-setup.service";
import { I18nService } from "../../services/internationalisation.service";
import { NgBaseParam } from "../base-param-input/base-param-input.component";
import { ErrorStateMatcher } from "@angular/material/core";
import { MatSnackBar } from "@angular/material/snack-bar";

import { MatomoTracker } from "@ngx-matomo/tracker";


@Component({
    selector: "setup",
    templateUrl: "./app-setup.component.html",
    styleUrls: ["./app-setup.component.scss"]
})
export class ApplicationSetupComponent implements Observer, OnInit {

    /** précision d'affichage */
    public displayPrec: NgBaseParam;

    /** précision de calcul */
    public computePrec: NgBaseParam;

    /** nombre d'iterations max Newton */
    public newtonMaxIter: NgBaseParam;

    public matcher: ErrorStateMatcher;

    constructor(
        private appSetupService: ApplicationSetupService,
        private intlService: I18nService,
        private snackBar: MatSnackBar,
        private matomoTracker: MatomoTracker
    ) {
        this.appSetupService.addObserver(this);
        if (! isDevMode()) {
            this.matomoTracker.trackPageView("setup");
        }
    }

    public get availableLanguages() {
        return this.intlService.languages;
    }

    public get currentLanguageCode() {
        if (this.intlService.currentLanguage) {
            return this.intlService.currentLanguage;
        }
    }

    public set currentLanguageCode(lc: string) {
        this.intlService.setLanguage(lc);
        // keep language in sync in app-wide parameters service
        this.appSetupService.language = this.intlService.currentLanguage;
    }

    /** notifications à l'écran (snackbar) */
    public get enableNotifications(): boolean {
        return this.appSetupService.enableNotifications;
    }

    public set enableNotifications(v: boolean) {
        this.appSetupService.enableNotifications = v;
    }

    /** hotkeys (keyboard shortcuts) */
    public get enableHotkeys(): boolean {
        return this.appSetupService.enableHotkeys;
    }

    public set enableHotkeys(v: boolean) {
        this.appSetupService.enableHotkeys = v;
    }

    /** empty all fields when creating a new form */
    public get enableEmptyFieldsOnFormInit(): boolean {
        return this.appSetupService.enableEmptyFieldsOnFormInit;
    }

    public set enableEmptyFieldsOnFormInit(v: boolean) {
        this.appSetupService.enableEmptyFieldsOnFormInit = v;
    }

    /** fold result messages log by default */
    public get foldedMessageLog(): boolean {
        return this.appSetupService.foldedMessageLog;
    }

    public set foldedMessageLog(v: boolean) {
        this.appSetupService.foldedMessageLog = v;
    }

    public get uitextTitle(): string {
        return this.intlService.localizeText("INFO_SETUP_TITLE");
    }

    public get uitextLanguage(): string {
        return this.intlService.localizeText("INFO_SETUP_LANGUAGE");
    }

    public get uitextDisplayAccuracy(): string {
        return this.intlService.localizeText("INFO_SETUP_PRECISION_AFFICHAGE");
    }

    public get uitextComputeAccuracy(): string {
        return this.intlService.localizeText("INFO_SETUP_PRECISION_CALCUL");
    }

    public get uitextNewtonMaxIteration(): string {
        return this.intlService.localizeText("INFO_SETUP_NEWTON_MAX_ITER");
    }

    public get uitextEnableNotifications(): string {
        return this.intlService.localizeText("INFO_SETUP_ENABLE_NOTIFICATIONS");
    }

    public get uitextEnableHotkeys(): string {
        return this.intlService.localizeText("INFO_SETUP_ENABLE_HOTKEYS");
    }

    public get uitextEnableEmptyFieldsOnFormInit(): string {
        return this.intlService.localizeText("INFO_SETUP_ENABLE_EMPTY_FIELDS");
    }

    public get uitextEnableFoldedMessageLog(): string {
        return this.intlService.localizeText("INFO_SETUP_FOLDED_RESULT_MESSAGES_LOG");
    }    

    public get uitextMustBeANumber(): string {
        return this.intlService.localizeText("ERROR_PARAM_MUST_BE_A_NUMBER");
    }

    public get uitextStorePreferences(): string {
        return this.intlService.localizeText("INFO_SETUP_STORE_PREFERENCES");
    }

    public get uitextRestoreDefaultValues(): string {
        return this.intlService.localizeText("INFO_SETUP_RESTORE_DEFAULT_VALUES");
    }

    public storePreferences() {
        this.appSetupService.saveValuesIntoLocalStorage();
        this.snackBar.open(this.intlService.localizeText("INFO_SNACKBAR_SETTINGS_SAVED"), "OK", {
            duration: 2500
        });
    }

    public async restoreDefaultValues() {
        const text = this.intlService.localizeText("INFO_SNACKBAR_DEFAULT_SETTINGS_RESTORED");
        await this.appSetupService.restoreDefaultValues();
        this.snackBar.open(text, "OK", {
            duration: 2500
        });
    }

    private init() {
        // modèle du composant BaseParamInputComponent de précision d'affichage
        this.displayPrec = new NgBaseParam("dp",
            new ParamDomain(ParamDomainValue.INTERVAL, 0, 10),
            this.appSetupService.displayPrecision
        );
        this.displayPrec.addObserver(this);

        // modèle du composant BaseParamInputComponent de précision de calcul
        this.computePrec = new NgBaseParam("cp",
            new ParamDomain(ParamDomainValue.INTERVAL, 1e-15, 1),
            this.appSetupService.computePrecision
        );
        this.computePrec.addObserver(this);

        // modèle du composant BaseParamInputComponent du max d'itérations pour Newton
        this.newtonMaxIter = new NgBaseParam("nmi",
            new ParamDomain(ParamDomainValue.INTERVAL, 1, 10000),
            this.appSetupService.maxIterations
        );
        this.newtonMaxIter.addObserver(this);

        // notifications
        this.enableNotifications = this.appSetupService.enableNotifications;
    }

    ngOnInit() {
        this.init();
    }

    // interface Observer
    public update(sender: any, data: any): void {
        if (sender instanceof NgBaseParam) {
            // someone typed a new value in the app-setup form
            const p: NgBaseParam = sender;
            switch (p.symbol) {
                case "dp":
                    this.appSetupService.displayPrecision = +data;
                    break;

                case "cp":
                    this.appSetupService.computePrecision = +data;
                    break;

                case "nmi":
                    this.appSetupService.maxIterations = +data;
                    break;
            }
        }
        if (sender instanceof ApplicationSetupService) {
            // if app starts on /setup page, wait for config file to be (re)loaded by app-setup service
            this.init();
        }
    }
}
