import { Component, Input, Output, EventEmitter, OnChanges, OnDestroy } from "@angular/core";

import { NgParameter } from "../../formulaire/elements/ngparam";
import { LinkedValue, ParamValueMode, Observer, acSection, ParamDefinition, ChildNub, Cloisons, Pab } from "jalhyd";
import { FormulaireService } from "../../services/formulaire.service";
import { I18nService } from "../../services/internationalisation.service";
import { FormulaireDefinition } from "../../formulaire/definition/form-definition";
import { sprintf } from "sprintf-js";

@Component({
    selector: "param-link",
    templateUrl: "./param-link.component.html",
    styleUrls: [
        "./param-link.component.scss"
    ]
})
export class ParamLinkComponent implements OnChanges, Observer, OnDestroy {
    // paramètre géré (qui sera lié à une valeur, cad qui importe cette valeur)
    @Input()
    public param: NgParameter;

    @Input()
    public title: string;

    /**
     * événement signalant un changement de valeur du modèle
     * @TODO l'utiliser aussi pour le changement de validité à
     * la place de this.valid, comme dans GenericInputComponentDirective
     */
    @Output()
    protected change = new EventEmitter<any>();

    /** indice actuel du paramètre sélectionné dans la liste */
    private _currentIndex = -1;

    /** liste des paramètres liables */
    private _linkableParams: LinkedValue[];

    public get selectId() {
        let id = "linked_" + this.param.symbol;
        // if inside a child Nub, prefix with child position to disambiguate
        const nub = this.param.paramDefinition.parentNub;
        if (nub && nub.parent && nub.intlType) {
            id = nub.findPositionInParent() + "_" + id;
        }
        return id;
    }

    public get isVariable(): boolean {
        return this.param.paramDefinition.hasMultipleValues;
    }

    /**
     * Returns true if current target requires a calculation (direct or not),
     * whether the result is already available or not
     */
    public get isCalculated(): boolean {
        const refValue = this.param.paramDefinition.referencedValue;
        return refValue && refValue.isCalculated();
    }

    /**
     * Returns true if current target requires a calculation (direct or not),
     * but result is not yet available
     */
    public get isAwaitingCalculation(): boolean {
        if (this.isCalculated) {
            const refValue = this.param.paramDefinition.referencedValue;
            try {
                refValue.getParamValues();
                return false;
            } catch (e) {
                return true;
            }
        }
        return false;
    }

    constructor(
        private intlService: I18nService,
        private formService: FormulaireService
    ) {
        this.formService.addObserver(this);
    }

    public get linkableParams() {
        return this._linkableParams;
    }

    public get label() {
        return this.intlService.localizeText("INFO_PARAMFIELD_PARAMLIE_LABEL");
    }

    public set currentLinkedParam(p: LinkedValue) {
        for (let i = 0; i < this._linkableParams.length; i++) {
            const li = this._linkableParams[i];
            if (li.equals(p)) {
                this.linkTo(i);
                break;
            }
        }
    }

    public get currentLinkedParam(): LinkedValue {
        if (this._linkableParams !== undefined) {
            if (this._currentIndex !== -1 && this._currentIndex < this._linkableParams.length) {
                return this._linkableParams[this._currentIndex];
            }
        }
    }

    /**
     * valeur courante affichée dans le select des paramètres liables
     */
    public get currentLinkedParamLabel(): string {
        const clp = this.currentLinkedParam;
        if (clp) {
            return this.selectItemLabel(clp);
        }
    }

    public get tooltipText(): string {
        return this.currentLinkedParamLabel
            + "\n"
            + NgParameter.preview(this.param.paramDefinition);
    }

    public get isValid(): boolean {
        return this._currentIndex !== -1 && this.param.isValid;
    }

    public get uitextVariated() {
        return this.intlService.localizeText("INFO_PARAMFIELD_VARIATED");
    }

    public get uitextCalculated() {
        return this.intlService.localizeText("INFO_PARAMFIELD_CALCULATED");
    }

    public get uitextAwaitingCalculation() {
        return this.intlService.localizeText("INFO_PARAMFIELD_AWAITING_CALCULATION");
    }

    /**
     * Label d'une entrée du select des paramètres liés
     */
    private selectItemLabel(i: LinkedValue): string {
        let s = i.symbol; // nom associé au paramètre/à la valeur
        const c = i.meta["formTitle"]; // nom du module de calcul

        // calculator type for translation
        let ct = i.nub.calcType;
        if (i.nub.parent) {
            ct = i.nub.parent.calcType;
        }
        // expand variable name from symbol
        s = this.formService.expandVariableName(ct, s);

        // value(s) preview
        let preview: string;
        if (i.isResult() || i.isExtraResult()) {
            preview = NgParameter.linkedValuePreview(i);
        } else {
            preview = NgParameter.preview(i.element as ParamDefinition, true);
        }

        // 1. Paramètre / résultat d'un Nub enfant au sein d'un Nub parent
        if (
            (i.nub instanceof ChildNub)
            || (
                (i.nub instanceof Cloisons)
                && i.nub.parent !== undefined
                && (i.nub.parent instanceof Pab)
            )
        ) {
            let pos: number;
            pos = i.nub.findPositionInParent();
            return `${preview} - ` + sprintf(
                this.intlService.localizeText("INFO_LINKED_VALUE_CHILD"),
                s, c,
                this.intlService.childName(i.nub).toLowerCase()
                , (pos + 1)
            );
        } else
            // 2. Paramètre / résultat d'une section dans un Nub de type SectionNub
            if (i.nub instanceof acSection) {
                if (i.isResult()) {
                    // résultat de section
                    return `${preview} - ` + sprintf(
                        this.intlService.localizeText("INFO_LINKED_VALUE_SECTION_RESULT"),
                        s, c
                    );
                } else {
                    // paramètre de section
                    return `${preview} - ` + sprintf(
                        this.intlService.localizeText("INFO_LINKED_VALUE_SECTION"),
                        s, c
                    );
                }
            } else
                // 3. Résultat
                if (i.isResult()) {
                    return `${preview} - ` + sprintf(
                        this.intlService.localizeText("INFO_LINKED_VALUE_RESULT"),
                        s, c
                    );
                } else
                    // 4. Résultat complémentaire
                    if (i.isExtraResult()) {
                        if (i.meta["result"]) {
                            // @TODO not used ?
                            return `${preview} - ` + sprintf(
                                this.intlService.localizeText("INFO_LINKED_VALUE_EXTRA_RESULT_OF"),
                                s, c, i.meta["result"]
                            );
                        } else {
                            return `${preview} - ` + sprintf(
                                this.intlService.localizeText("INFO_LINKED_VALUE_EXTRA_RESULT"),
                                s, c
                            );
                        }
                    } else {
                        // 5. Paramètre (cas général)
                        return `${preview} - ${s} (${c})`;
                    }
    }

    /**
     * lie le paramètre géré à un des paramètres liables de la liste; appelé
     * systématiquement lorsqu'on construit le formulaire, même si le
     * paramètre est déjà lié
     * @param index indice dans la liste
     */
    private linkTo(index: number) {
        if (this._currentIndex !== index) {
            this._currentIndex = index;
            const lp = this._linkableParams[index];
            this.param.linkToValue(lp);
            // reset form results when a new target is selected
            this.emitModelChanged();
        }
    }

    /**
     * événement de changement de la valeur du modèle
     */
    private emitModelChanged() {
        let value;
        try {
            value = this.currentLinkedParam.getValue();
        } catch (e) {
            // console.log("undefined target value (pending calculation)");
        }
        this.change.emit({
            action: "model",
            value: value,
            symbol: this.param.symbol
        });
    }

    public updateParamList() {
        let noMoreTarget = false;
        // liste des paramètres liables
        if (this.param.valueMode === ParamValueMode.LINK) {
            this._linkableParams = this.formService.getLinkableValues(this.param);
            noMoreTarget = this._linkableParams.length === 0;
        } else {
            this._linkableParams = [];
        }

        // initialisation de l'indice courant
        if (this._linkableParams.length > 0) {
            if (this._currentIndex === -1) {
                // le paramètre est il déjà lié à une valeur ? si oui laquelle ?
                if (this.param.valueMode === ParamValueMode.LINK && this._linkableParams !== undefined) {
                    let i = 0;
                    for (const e of this._linkableParams) {
                        const refValue = this.param.paramDefinition.referencedValue;
                        if (refValue && e.equals(refValue)) {
                            this._currentIndex = i;
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                this.linkTo(Math.max(this._currentIndex, 0)); // might still be -1
            }
        } else {
            this._currentIndex = -1;
            // back to SINGLE mode by default if no more target module available
            if (noMoreTarget) {
                this.param.valueMode = ParamValueMode.SINGLE;
            }
        }
    }

    public ngOnChanges() {
        if (this.param !== undefined) {
            this.param.removeObserver(this);
        }
        this.param.addObserver(this);

        this.updateParamList();
    }

    public ngOnDestroy() {
        this.param.removeObserver(this);
    }

    // interface Observer

    public update(sender: any, data: any) {
        if (sender instanceof FormulaireService) {
            switch (data.action) {
                // scan newly created form for available linkable params
                case "createForm":
                    this.updateParamList();
                    break;

                case "formNameChanged":
                    const f = data.form as FormulaireDefinition;
                    for (const lp of this._linkableParams) {
                        if (
                            lp.nub.uid === f.currentNub.uid
                            || ( // repeatable Nub inside parent Nub
                                lp.nub.parent
                                && lp.nub.parent.uid === f.currentNub.uid
                            )
                        ) {
                            // update <select> option
                            lp.meta["formTitle"] = data.value;
                        }
                    }
                    break;
            }
        }
    }
}
