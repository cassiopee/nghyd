import {
    Component, OnInit, DoCheck, OnDestroy, ViewChild, ViewChildren,
    QueryList, AfterViewChecked, ElementRef, Inject, forwardRef, isDevMode
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import {
    Observer,
    Session,
    Cloisons,
    Pab,
    ParamValueMode,
    CalculatorType,
    Bief,
    SectionParametree,
    acSection,
    ParamDefinition,
    round,
    Nub,
    RegimeUniforme,
    Par,
    ParSimulationParams,
    ParSimulation,
    PreBarrage,
    PbCloison,
    Espece,
    VariatedDetails,
    ParallelStructure
} from "jalhyd";

import { generateValuesCombination, getUnformattedIthResult, getUnformattedIthValue } from "../../util/util";

import { AppComponent } from "../../app.component";
import { FormulaireService } from "../../services/formulaire.service";
import { ApplicationSetupService } from "../../services/app-setup.service";
import { I18nService } from "../../services/internationalisation.service";
import { FieldSet } from "../../formulaire/elements/fieldset";
import { FormulaireDefinition } from "../../formulaire/definition/form-definition";
import { Subscription } from "rxjs";
import { FieldSetComponent } from "../field-set/field-set.component";
import { FormulaireElement } from "../../formulaire/elements/formulaire-element";
import { FieldsetContainer } from "../../formulaire/elements/fieldset-container";
import { FieldsetContainerComponent } from "../fieldset-container/fieldset-container.component";
import { PabTableComponent } from "../pab-table/pab-table.component";
import { MatDialog } from "@angular/material/dialog";
import { DialogConfirmCloseCalcComponent } from "../dialog-confirm-close-calc/dialog-confirm-close-calc.component";
import { DialogGeneratePABComponent } from "../dialog-generate-pab/dialog-generate-pab.component";
import { DialogGeneratePARSimulationComponent } from "../dialog-generate-par-simulation/dialog-generate-par-simulation.component";
import { DialogLoadPredefinedEspeceComponent } from "../dialog-load-predefined-espece/dialog-load-predefined-espece.component";
import { PabTable } from "../../formulaire/elements/pab-table";
import { MultiDimensionResults } from "../../results/multidimension-results";
import { FormulaireFixedVar } from "../../formulaire/definition/form-fixedvar";
import { PbSchema } from "../../formulaire/elements/pb-schema";
import { PbSchemaComponent } from "../pb-schema/pb-schema.component";
import { FormulairePrebarrage } from "../../formulaire/definition/form-prebarrage";

import { HotkeysService, Hotkey } from "angular2-hotkeys";

import { MatomoTracker } from "@ngx-matomo/tracker";

import { sprintf } from "sprintf-js";

import * as XLSX from "xlsx";
import { ServiceFactory } from "app/services/service-factory";
import { DefinedBoolean } from "../../util/definedvalue/definedboolean";
import { FormulaireCourbeRemous } from "app/formulaire/definition/form-courbe-remous";
import { RemousResults } from "app/results/remous-results";

@Component({
    selector: "hydrocalc",
    templateUrl: "./calculator.component.html",
    styleUrls: ["./calculator.component.scss"]
})
export class GenericCalculatorComponent implements OnInit, DoCheck, AfterViewChecked, OnDestroy, Observer {
    /**
     * liste des FieldSetComponent
     */
    @ViewChildren(FieldSetComponent)
    private _fieldsetComponents: QueryList<FieldSetComponent>;

    /**
     * liste des FieldsetContainerComponent
     */
    @ViewChildren(FieldsetContainerComponent)
    private _fieldsetContainerComponents: QueryList<FieldsetContainerComponent>;

    /**
     * PabTableComponent if any
     */
    @ViewChild(PabTableComponent)
    private _pabTableComponent: PabTableComponent;

    /**
     * PbSchemaComponent if any
     */
    @ViewChild(PbSchemaComponent)
    private _pbSchemaComponent: PbSchemaComponent;

    /**
     * formulaire affiché
     */
    private _formulaire: FormulaireDefinition;

    private _subscription: Subscription; // pour souscrire aux changements d'URL envoyés par le routeur

    /**
     * true si on a cliqué sur le bouton de lancement de calcul
     */
    private _computeClicked: boolean;

    /**
     * flag de validité gloable de la saisie
     * NB : la validité du bouton "calculer" dépend de la validité de la saisie dans l'UI et de celle indiquée par le formulaire.
     * La validité de l'UI comprend la forme (pas de chaîne alpha dans les champs numériques, etc..).
     * La validité formulaire comprend le domaine de définition des valeurs saisies.
     */
    private _isUIValid: DefinedBoolean;

    /**
     * flag disabled du bouton "calculer"
     */
    public isCalculateDisabled = true;

    /** Allows trigerring afterFirstViewChecked() just after calculator is loaded */
    private firstViewChecked = false;

    public get ID() {
        if (this._formulaire) {
            return this._formulaire.uid;
        } else {
            return "calculator_1";
        }
    }

    /** For PreBarrage only: if true, show input data in the right panel, else show results */
    public get showPBInputData(): boolean {
        if (this.isPB) {
            return (this._formulaire as FormulairePrebarrage).showInputData;
        } else {
            return false; // whatever, should never happen
        }
    }

    public set showPBInputData(v: boolean) {
        if (this.isPB) {
            (this._formulaire as FormulairePrebarrage).showInputData = v;
        } // else do nothing, should never happen
    }

    constructor(
        @Inject(forwardRef(() => AppComponent)) private appComponent: AppComponent,
        private route: ActivatedRoute,
        private router: Router,
        private confirmCloseCalcDialog: MatDialog,
        private generatePABDialog: MatDialog,
        private generatePARSimulationDialog: MatDialog,
        private loadPredefinedEspeceDialog: MatDialog,
        private _elementRef: ElementRef,
        private hotkeysService: HotkeysService,
        private appSetupService: ApplicationSetupService,
        private intlService: I18nService,
        private formulaireService: FormulaireService,
        private matomoTracker: MatomoTracker
    ) {
        this._isUIValid = new DefinedBoolean();
        // hotkeys listeners
        this.hotkeysService.add(new Hotkey("alt+w", AppComponent.onHotkey(this.closeCalculator, this)));
        this.hotkeysService.add(new Hotkey("alt+d", AppComponent.onHotkey(this.cloneCalculator, this)));
        this.hotkeysService.add(new Hotkey("alt+enter", AppComponent.onHotkey(this.doCompute, this)));
        this.hotkeysService.add(new Hotkey("alt+1", AppComponent.onHotkey(() => {
            this.appComponent.scrollToQuicknav("input");
        }, this)));
        this.hotkeysService.add(new Hotkey("alt+2", AppComponent.onHotkey(() => {
            this.appComponent.scrollToQuicknav("results");
        }, this)));
        this.hotkeysService.add(new Hotkey("alt+3", AppComponent.onHotkey(() => {
            this.appComponent.scrollToQuicknav("charts");
        }, this)));
    }

    public get formulaire(): FormulaireDefinition {
        return this._formulaire;
    }

    public get formElements(): FormulaireElement[] {
        if (this._formulaire === undefined) {
            return [];
        }
        return this._formulaire.kids as FormulaireElement[];
    }

    public get calculateDisabledPermanently(): boolean {
        return this._formulaire.calculateDisabled;
    }

    /**
     * détermine si un FormulaireElement est du type FieldSet
     */
    /** détermine si un FormulaireElement est du type FieldSet */
    public isFieldset(fe: any): boolean {
        return fe instanceof FieldSet;
    }

    /** détermine si un FormulaireElement est du type FieldsetContainer et générique */
    public isFieldsetContainer(fe: any): boolean {
        if (fe instanceof FieldsetContainer) {
            switch (fe.template.calcTypeFromConfig) {
                case CalculatorType.Structure:
                case CalculatorType.PbBassin:
                    return false;

                default:
                    return true;
            }
        }
        return false;
    }

    /** détermine si un FormulaireElement est du type FieldsetContainer contenant des structures */
    public isStructureFieldsetContainer(fe: any): boolean {
        if (fe instanceof FieldsetContainer) {
            return fe.template.calcTypeFromConfig === CalculatorType.Structure;
        }
        return false;
    }

    /** détermine si un FormulaireElement est du type FieldsetContainer contenant des bassins */
    public isBasinFieldsetContainer(fe: any): boolean {
        if( fe instanceof FieldsetContainer){
            return fe.template.calcTypeFromConfig === CalculatorType.PbBassin;
        }
        return false;
    }

    public getFieldsetContainerClass(fe: any): string {
        if (this.isStructureFieldsetContainer(fe)) {
            return "structure-fieldsetcontainer";
        }
        if (this.isBasinFieldsetContainer(fe)) {
            return "basin-fieldsetcontainer";
        }
        return "fieldsetcontainer";
    }

    /** détermine si un FormulaireElement est du type PabTable */
    public isPabTable(fe: any): boolean {
        return fe instanceof PabTable;
    }

    /** détermine si un FormulaireElement est du type PbSchema */
    public isPbSchema(fe: any): boolean {
        return fe instanceof PbSchema;
    }

    public get hasForm() {
        return this._formulaire !== undefined;
    }

    public get hasResults(): boolean {
        if (this.hasForm) {
            return this._formulaire.hasResults;
        }
        return false;
    }

    public get uitextTitre() {
        if (this.hasForm) {
            return this.formulaireService.getLocalisedTitleFromCalculatorType(this._formulaire.calculatorType);
        }
    }

    public get uitextCalculer() {
        return this.intlService.localizeText("INFO_CALCULATOR_CALCULER");
    }

    public get uitextCalculatorName() {
        return this.intlService.localizeText("INFO_CALCULATOR_CALC_NAME");
    }

    public get uitextResultsTitle() {
        let title = this.intlService.localizeText("INFO_CALCULATOR_RESULTS_TITLE");
        if (this.isPB) {
            const selectedItem = (this._formulaire as FormulairePrebarrage).selectedItem;
            if (selectedItem instanceof PbCloison) {
                // suffix with wall name
                title = sprintf(
                    this.intlService.localizeText("INFO_CALCULATOR_RESULTS_TITLE_PB_WALL"),
                    this.intlService.localizeMessage(selectedItem.description)
                );
            } else {
                title = this.intlService.localizeText("INFO_CALCULATOR_RESULTS_TITLE_PB_BASINS");
            }
        }
        return title;
    }

    public get uitextGeneratePAB() {
        return this.intlService.localizeText("INFO_CALCULATOR_RESULTS_GENERATE_PAB");
    }

    public get uitextGenerateSPAmont() {
        return this.intlService.localizeText("INFO_CALCULATOR_RESULTS_GENERATE_SP_AMONT");
    }

    public get uitextGenerateSPAval() {
        return this.intlService.localizeText("INFO_CALCULATOR_RESULTS_GENERATE_SP_AVAL");
    }

    public get uitextGenerateRuSp() {
        return this.intlService.localizeText("INFO_CALCULATOR_RESULTS_GENERATE_RU_SP");
    }

    public get uitextGeneratePARSimulation() {
        return this.intlService.localizeText("INFO_CALCULATOR_RESULTS_GENERATE_PAR_SIMULATION");
    }

    public get uitextExportAllPbResults() {
        return this.intlService.localizeText("INFO_CALCULATOR_RESULTS_EXPORT_ALL_PB_RESULTS");
    }

    public get uitextLoadPredefinedEspece(): string {
        return this.intlService.localizeText("INFO_CALCULATOR_LOAD_PREDEFINED_ESPECE");
    }

    public get uitextOpenHelp() {
        return this.intlService.localizeText("INFO_CALCULATOR_OPEN_HELP");
    }

    public get uitextCloneCalculator() {
        return this.intlService.localizeText("INFO_CALCULATOR_CLONE");
    }

    public get uitextSaveCalculator() {
        return this.intlService.localizeText("INFO_CALCULATOR_SAVE");
    }

    public get uitextCloseCalculator() {
        return this.intlService.localizeText("INFO_CALCULATOR_CLOSE");
    }

    public get uitextUsedBy() {
        return this.intlService.localizeText("INFO_CALCULATOR_USED_BY");
    }

    public get uitextInputData() {
        return this.intlService.localizeText("INFO_QUICKNAV_INPUT");
    }

    public get uitextResults() {
        return this.intlService.localizeText("INFO_QUICKNAV_RESULTS");
    }

    public get quicknavItems() {
        const elts = ["input", "results"];
        if (this.isWide && this.hasResults) {
            elts.push("charts");
        }
        return elts;
    }

    /**
     * Triggered at calculator instanciation
     */
    ngOnInit() {
        this.formulaireService.addObserver(this);
        this.subscribeRouter();
    }

    /**
     * Setting flag at check time prevents ExpressionChangedAfterItHasBeenCheckedError
     * when loading calculator (probably because of afterFirstViewChecked() changing
     * the UI validity state)
     */
    ngDoCheck() {
        this.isCalculateDisabled = !this._isUIValid.value;
    }

    ngOnDestroy() {
        this.unsubscribeRouter();
        this.formulaireService.removeObserver(this);
        this.intlService.removeObserver(this);
    }

    private subscribeRouter() {
        // récupération des paramètres passés avec la route (URL)
        this._subscription = this.route.params.subscribe(params => {
            const uid: string = params["uid"];
            this.formulaireService.setCurrentForm(uid);
        });
    }

    private unsubscribeRouter() {
        if (this._subscription) {
            this._subscription.unsubscribe();
        }
    }

    /*
     * gestion des événements clic sur les radios
     */
    public onRadioClick(info: any) {
        if (info.param.valueMode === ParamValueMode.LINK) {
            this.updateLinkedParameters(); // only when switching to LINK mode
        }
        this._formulaire.onRadioClick(info);
    }

    public ngAfterViewChecked() {
        if (!this.firstViewChecked) {
            this.firstViewChecked = true;
            this.afterFirstViewChecked();
        }
    }

    /**
     * Runs once, after the first execution of ngAfterViewChecked(). Used
     * to fix #349 (calculator button grayed out after loading a session)
     *
     * IMPORTANT: this component is reused until user leaves the /calculator/:id route,
     * so this method will run only when switching from another route to
     * the /calculator/:id route
     */
    protected afterFirstViewChecked() {
        this.updateUIValidity();
        this.formulaire.setFirstDisplayCompleted();
    }

    public onCloseForm() {
        this.formulaireService.requestCloseForm(this._formulaire.uid);
    }

    /**
     * met à jour les paramètres liés
     */
    private updateLinkedParameters() {
        this._fieldsetComponents.forEach(fsc => fsc.updateLinkedParameters());
        this._fieldsetContainerComponents.forEach(fscc => fscc.updateLinkedParameters());
    }

    public doCompute() {
        if (!isDevMode()) {
            this.matomoTracker.trackEvent("userAction", "triggerCalculation", CalculatorType[this._formulaire.currentNub.calcType]);
        }
        this._formulaire.resetResults([]);
        this.appComponent.showProgressBar = true;
        this._computeClicked = true;
        this.showPBInputData = false;
        // calculate module
        setTimeout(() => {
            this._formulaire.doCompute();
            this.appComponent.showProgressBar = false;
        }, 100); // 100ms is important, a lower value may make the progress bar never appear :/
    }

    public onCalcResultsViewChecked() {
        if (this._computeClicked) {
            this._computeClicked = false;
            this.scrollToResults();
        }
    }

    /** trackBy:index simulator @see nghyd#364 */
    public tbIndex(index: number, item: any) {
        return index;
    }

    /** open calculator identified by given UID */
    public toCalc(id: string) {
        this.appComponent.toCalc(id);
    }

    /** list of calculators having a link pointing to the current calculator */
    public get calculatorsUsingThisOne(): any {
        const sources = Session.getInstance().getDependingNubs(this._formulaire.currentNub.uid, undefined, true, true);
        return sources.map((s) => {
            const form = this.formulaireService.getFormulaireFromNubId(s.uid);
            return {
                label: (form !== undefined) ? form.calculatorName : "unknown Nub",
                uid: s.uid
            };
        });
    }

    /**
     * Moves the view to the start of the "Results" section
     */
    private scrollToResults() {
        // try to scroll to quicknav "results" first
        try {
            this.appComponent.scrollToQuicknav("results");
        } catch (e) {
            const element = document.getElementById("fake-results-anchor");
            if (element) {
                element.scrollIntoView();
            }
        }
    }

    // interface Observer

    update(sender: any, data: any): void {
        if (sender instanceof FormulaireService) {
            switch (data["action"]) {
                case "currentFormChanged":
                    const uid: string = data["formId"];
                    this._formulaire = (this.formulaireService.getFormulaireFromId(uid));
                    // reload localisation in all cases (it does not eat bread)
                    this.formulaireService.updateFormulaireLocalisation(this._formulaire);
                    break;
            }
        }
    }

    /**
     * calcul de la validité globale de la vue
     */
    private updateUIValidity() {
        let res = false;
        if (!this._formulaire.calculateDisabled) {
            // all fieldsets must be valid
            res = true;
            if (this._fieldsetComponents?.length > 0) {
                res = res && this._fieldsetComponents.reduce(
                    // callback
                    (
                        // accumulator (valeur précédente du résultat)
                        acc,
                        // currentValue (élément courant dans le tableau)
                        fieldset,
                        // currentIndex (indice courant dans le tableau)
                        currIndex,
                        // array (tableau parcouru)
                        array
                    ) => {
                        return acc && fieldset.isValid;
                    }
                    // valeur initiale
                    , true);
            }
            // all fieldset containers must be valid
            if (this._fieldsetContainerComponents?.length > 0) {
                res = res && this._fieldsetContainerComponents.reduce<boolean>(
                    // callback
                    (
                        // accumulator (valeur précédente du résultat)
                        acc,
                        // currentValue (élément courant dans le tableau)
                        fieldsetContainer,
                        // currentIndex (indice courant dans le tableau)
                        currIndex,
                        // array (tableau parcouru)
                        array
                    ): boolean => {
                        return acc && fieldsetContainer.isValid;
                    }
                    // valeur initiale
                    , true);
            }
            // special components must be valid
            if (this._pabTableComponent !== undefined) {
                res = res && this._pabTableComponent.isValid;
            }
            if (this._pbSchemaComponent !== undefined) {
                res = res && this._pbSchemaComponent.isValid;
            }
            switch (this._formulaire.currentNub.calcType) {
                case CalculatorType.PreBarrage:
                    const form: FormulairePrebarrage = this._formulaire as FormulairePrebarrage;
                    res = res && form.checkParameters().length === 0;
                    break;

                case CalculatorType.CloisonAval:
                case CalculatorType.Cloisons:
                case CalculatorType.PbCloison:
                case CalculatorType.Dever:
                    // in case of parallel structures, check that child structures count > 0
                    const pstr = this.formulaire.currentNub as ParallelStructure;
                    res = res && pstr.getChildren().length > 0;
                    break;
            }

            res = res && this._formulaire.currentNub.isComputable();
        }

        this._isUIValid.value = res;

        // update prébarrage schema validity
        this._pbSchemaComponent?.updateItemsValidity();
    }

    public getElementStyleDisplay(id: string) {
        const isDisplayed: boolean = this._formulaire.isDisplayed(id);
        return isDisplayed ? "block" : "none";
    }

    /**
     * réception d'un événement de validité d'un FormElement
     */
    public onElementValid() {
        this.updateUIValidity();
    }

    /**
     * réception d'un événement de changement de valeur d'un input
     */
    public onInputChange(event: any) {
        // console.debug(`CalculatorComponent.onInputChange(${event?.symbol})`);
        this._formulaire.resetResults([], (event ? event.symbol : undefined));
    }

    /**
     * Passe d'un champ de saisie à l'autre lors de l'appui sur
     * TAB, sans passer par les autres éléments d'interface
     */
    public onTabPressed(event: any) {
        // event source input id
        const srcId = event.originalEvent.target.id;

        // find all available inputs
        const qs = "ngparam-input input.form-control";
        let inputs: Node[] = Array.from(this._elementRef.nativeElement.querySelectorAll(qs));

        // find calculated symbol if any, to exclude it from inputs list
        let calcSymbol = "";
        if (this._formulaire.currentNub.calculatedParam) {
            calcSymbol = this._formulaire.currentNub.calculatedParam.symbol;
        }
        inputs = inputs.filter((i: any) => {
            return i.id !== calcSymbol;
        });

        // find position among inputs list
        const currentIndex = inputs.findIndex((i: any) => {
            return i.id === srcId;
        });

        // focus previous / next input
        let newIndex = currentIndex;
        if (event.shift) {
            newIndex = (newIndex === 0) ? (inputs.length - 1) : (newIndex - 1);
        } else {
            newIndex = (newIndex + 1) % inputs.length;
        }
        const elt = (inputs[newIndex] as HTMLInputElement);
        elt.focus();
        elt.select();
    }

    /** réception d'un événement de clic sur un nœud du schéma de PréBarrage */
    public onPBNodeSelected(event: any) {
        // console.debug(`GenericCalculatorComponent.onPBNodeSelected(${event?.node?.uid})`);
        // show proper form (actually subform elements) or proper results,
        // depending on what was clicked
        (this._formulaire as FormulairePrebarrage).nodeSelected(event.node);
    }

    /** True if PreBarrage Nub has no results */
    public get pbShowResultsDisabled(): boolean {
        const res = ((this._formulaire as FormulairePrebarrage).currentNub as PreBarrage).result;
        return res === undefined;
    }

    public openHelp() {
        window.open("assets/docs/" + this.appSetupService.language + "/calculators/" + this._formulaire.helpLink, "_blank");
    }

    /**
     * flag d'affichage du bouton d'aide
     */
    public get enableHelpButton() {
        return this._formulaire && this._formulaire.helpLink;
    }

    /** return true if current Nub type is ct */
    public is(ct: CalculatorType): boolean {
        return (
            this._formulaire
            && this._formulaire.currentNub
            && this._formulaire.currentNub.calcType === ct
        );
    }

    // for "one wide column" layout
    public get isWide() {
        return (this.isPAB || this.isMRC || (this.isPB && this.showPBInputData));
    }

    // true if current Nub is Solveur
    public get isSolveur() {
        return this.is(CalculatorType.Solveur);
    }

    // true if current Nub is PAB
    public get isPAB() {
        return this.is(CalculatorType.Pab);
    }

    // true if current Nub is MacroRugoCompound
    public get isMRC() {
        return this.is(CalculatorType.MacroRugoCompound);
    }

    // true if current Nub is PreBarrage
    public get isPB() {
        return this.is(CalculatorType.PreBarrage);
    }

    // true if current Nub is Jet
    public get isJet() {
        return this.is(CalculatorType.Jet);
    }

    // for "generate PAB" button
    public get isPABCloisons() {
        return this.is(CalculatorType.Cloisons);
    }

    // true if current Nub is Bief
    public get isBief() {
        return this.is(CalculatorType.Bief);
    }

    // true if current Nub is RegimeUniforme
    public get isRegimeUniforme() {
        return this.is(CalculatorType.RegimeUniforme);
    }

    // true if CourbeRemous results are present
    public get hasCourbeRemousResults() {
        return this.is(CalculatorType.CourbeRemous) && this.hasResults;
    }

    // true if current Nub is PAR
    public get isPAR() {
        return this.is(CalculatorType.Par);
    }

    // true if current Nub is Verificateur
    public get isVerificateur() {
        return this.is(CalculatorType.Verificateur);
    }

    // true if current Nub is Espece
    public get isEspece() {
        return this.is(CalculatorType.Espece);
    }

    // true if current Nub is PAR
    public get isSP() {
        return this.is(CalculatorType.SectionParametree);
    }

    // true if current Nub is PAR
    public get isRemous() {
        return this.is(CalculatorType.CourbeRemous);
    }

    /**
     * Returns true if no parameter is varying; ignores parameters having
     * one of the given {except} symbols, if any
     */
    private allParamsAreFixed(except: string[] = []) {
        let ret = true;
        for (const p of this._formulaire.currentNub.parameterIterator) {
            if (!except.includes(p.symbol)) {
                if (p.valueMode === ParamValueMode.LINK) {
                    ret = ret && (!p.hasMultipleValues);
                } else {
                    // avoid calling hasMultipleValues here, because changing parameter mode in GUI
                    // switches valueMode before setting min/max/step or valuesList, and iterator
                    // checker fails to count values that do not exist yet
                    ret = ret && (![ParamValueMode.LISTE, ParamValueMode.MINMAX].includes(p.valueMode));
                }
            }
        }
        return ret;
    }

    // PAB generation is enabled only if :
    // - no parameter is varying
    // - Cloisons is calculated first
    public get generatePABEnabled(): boolean {
        return this.allParamsAreFixed() && this.hasResults;
    }

    public get uitextGeneratePabTitle() {
        if (!this.hasResults) {
            return this.intlService.localizeText("INFO_CALCULATE_FIRST");
        }
        if (!this.allParamsAreFixed()) {
            return this.intlService.localizeText("INFO_PARAMETRES_FIXES");
        }
        return "";
    }

    /**
     * Génère une passe à bassins à partir du modèle de cloisons en cours
     */
    public generatePAB() {
        // création du dialogue de génération d'une passe à bassin
        const cloisons = (this._formulaire.currentNub as Cloisons);
        const chute = cloisons.prms.DH.V;
        const debit = cloisons.prms.Q.V;
        const zAmont = cloisons.prms.Z1.V;
        const nbBassins = ServiceFactory.applicationSetupService.enableEmptyFieldsOnFormInit ? undefined : 6;
        const dialogRef = this.generatePABDialog.open(
            DialogGeneratePABComponent,
            {
                data: {
                    debit: debit,
                    coteAmont: zAmont,
                    nbBassins: nbBassins
                },
                disableClose: false
            }
        );
        dialogRef.afterClosed().subscribe(async result => {
            if (result) {
                if (result.generate) {
                    const f: FormulaireDefinition = await this.formulaireService.createFormulaire(CalculatorType.Pab);
                    const pab = (f.currentNub as Pab);
                    const params = pab.prms;
                    // calculate downstream elevation
                    const chute = cloisons.prms.DH.V;
                    const coteAval = result.coteAmont - (chute * result.nbBassins);
                    // paramètres hydrauliques
                    params.Q.singleValue = result.debit;
                    params.Z1.singleValue = result.coteAmont;
                    params.Z2.singleValue = coteAval;
                    // création des bassins
                    pab.deleteChild(0);
                    pab.addCloisonsFromModel(this._formulaire.currentNub as Cloisons, result.nbBassins);
                    // go to new PAB
                    this.router.navigate(["/calculator", f.uid]);
                }
            }
        });
    }

    public get generateSPAmontEnabled(): boolean {
        const bief = (this._formulaire.currentNub as Bief);
        if (bief.prms.Z1 === bief.calculatedParam || bief.calculatedParam === bief.childrenPrms[0].get("Q")) {
            return this.hasResults && !bief.result.hasErrorMessages();
        } else {
            // check that linked values are available, if any
            return (
                (bief.prms.Z1.valueMode !== ParamValueMode.LINK || bief.prms.Z1.referencedValue.isDefined())
                && (bief.prms.ZF1.valueMode !== ParamValueMode.LINK || bief.prms.ZF1.referencedValue.isDefined())
            );
        }
    }

    public get generateSPAvalEnabled(): boolean {
        const bief = (this._formulaire.currentNub as Bief);
        if (bief.prms.Z2 === bief.calculatedParam || bief.calculatedParam === bief.childrenPrms[0].get("Q")) {
            return this.hasResults && !bief.result.hasErrorMessages();
        } else {
            // check that linked values are available, if any
            return (
                (bief.prms.Z2.valueMode !== ParamValueMode.LINK || bief.prms.Z2.referencedValue.isDefined())
                && (bief.prms.ZF2.valueMode !== ParamValueMode.LINK || bief.prms.ZF2.referencedValue.isDefined())
            );
        }
    }

    public get uitextGenerateSPAmontTitle(): string {
        if (!this.generateSPAmontEnabled) {
            return this.intlService.localizeText("INFO_CALCULATE_FIRST");
        } else {
            return "";
        }
    }

    public get uitextGenerateSPAvalTitle(): string {
        if (!this.generateSPAvalEnabled) {
            return this.intlService.localizeText("INFO_CALCULATE_FIRST");
        } else {
            return "";
        }
    }

    /**
     * Génère une SectionParametree à partir des cotes amont du module Bief en cours
     */
    public generateSPAmont() {
        const bief = (this._formulaire.currentNub as Bief);
        this.generateBiefSP(
            this.generateBiefYValuesForSP(bief.prms.Z1, bief.prms.ZF1),
            this.generateBiefIfValuesForSP()
        );
    }

    /**
     * Génère une SectionParametree à partir des cotes aval du module Bief en cours
     */
    public generateSPAval() {
        const bief = (this._formulaire.currentNub as Bief);
        this.generateBiefSP(
            this.generateBiefYValuesForSP(bief.prms.Z2, bief.prms.ZF2),
            this.generateBiefIfValuesForSP()
        );
    }

    /**
     * Génère une liste de valeurs de tirant d'eau pour un couple Z1/ZF1 ou Z2/ZF2 donné
     * @param Z cote de l'eau (Z1 ou Z2)
     * @param ZF cote de fond (ZF1 ou ZF2)
     */
    private generateBiefYValuesForSP(Z: ParamDefinition, ZF: ParamDefinition): number | number[] {
        const bief = (this._formulaire.currentNub as Bief);
        return generateValuesCombination(
            bief,
            [Z, ZF],
            (nub: Nub, values: { [key: string]: number }): number => {
                return round(values[Z.symbol] - values[ZF.symbol], 3);
            }
        );
    }

    /**
     * Génère une liste de valeurs de pente en fonction de ZF1, ZF2 et Long
     */
    private generateBiefIfValuesForSP(): number | number[] {
        const bief = (this._formulaire.currentNub as Bief);
        return generateValuesCombination(
            bief,
            [bief.prms.ZF1, bief.prms.ZF2, bief.prms.Long],
            (nub: Nub, values: { [key: string]: number }): number => {
                return round((values["ZF1"] - values["ZF2"]) / values["Long"], 5);
            }
        );
    }

    /**
     * Génère une SectionParametree à partir du module Bief en cours
     * @param Ys tirant(s) d'eau
     * @param Ifs pente(s)
     */
    private async generateBiefSP(Ys: number | number[], Ifs: number | number[]) {
        const bief = (this._formulaire.currentNub as Bief);
        const serialisedSection = bief.section.serialise();
        const sectionCopy = Session.getInstance().unserialiseSingleNub(serialisedSection, false).nub;
        const secParam = new SectionParametree(sectionCopy as acSection);
        Session.getInstance().registerNub(secParam);
        // link all section params
        for (const p of secParam.section.parameterIterator) {
            if (
                // "If" is hidden in Bief, for ex.
                bief.section.getParameter(p.symbol).visible
                // do not link Y and If
                && !["If", "Y"].includes(p.symbol)
            ) {
                const bP = bief.section.getParameter(p.symbol);
                if (bP.valueMode === ParamValueMode.LINK) {
                    p.defineReference(bP.referencedValue.nub, p.symbol);
                } else {
                    p.defineReference(bief.section, p.symbol);
                }
            }
        }

        const f: FormulaireDefinition = await this.formulaireService.createFormulaire(CalculatorType.SectionParametree, secParam);
        const sp = (f.currentNub as SectionParametree);
        sp.section.prms.Y.setValues(Ys);
        sp.section.prms.If.setValues(Ifs);
        // calculate
        f.doCompute();
        // go to new SP
        this.router.navigate(["/calculator", f.uid]);
    }

    public get generateRuSpEnabled(): boolean {
        return this.hasResults && !this._formulaire.currentNub.result.hasErrorMessages();
    }

    public get uitextGenerateRuSpTitle(): string {
        if (!this.generateRuSpEnabled) {
            return this.intlService.localizeText("INFO_CALCULATE_FIRST");
        } else {
            return "";
        }
    }

    /**
     * Génère une SectionParametree à partir du module RegimeUniforme en cours
     */
    public async generateRuSp() {
        const f = await this.formulaireService.generateParametricSectionForm();
        // calculate
        f.doCompute();
        // go to new SP
        this.router.navigate(["/calculator", f.uid]);
    }

    /**
     * @returns liste des abscisses du graphe de courbes de remous
     */
    public get courbeRemousPoints(): any[] {
        if (this.hasCourbeRemousResults) {
            const crForm = this._formulaire as FormulaireCourbeRemous;
            for (const r of crForm.results) {
                if (r instanceof RemousResults) {
                    return r.points;
                }
            }
        }
    }

    public get generatePARSimulationEnabled(): boolean {
        const parCalage = (this._formulaire.currentNub as Par);
        return (
            this.hasResults
            && !parCalage.result.hasErrorMessages()
            && parCalage.prms.Z1.isDefined
            && parCalage.prms.Z2.isDefined
        );
    }

    public get uitextGenerateParSimulationTitle(): string {
        const parCalage = (this._formulaire.currentNub as Par);
        if (!this.hasResults || parCalage.result.hasErrorMessages()) {
            return this.intlService.localizeText("INFO_CALCULATE_FIRST");
        }
        if (
            !parCalage.prms.Z1.isDefined
            || !parCalage.prms.Z2.isDefined
        ) {
            return this.intlService.localizeText("INFO_Z1_Z2_MUST_BE_DEFINED");
        }
        return "";
    }

    /**
     * Génère une simulation de passe à ralentisseurs à partir du calage en cours; si
     * au moins un paramètre varie, propose de choisir parmi les combinaisons de valeurs
     */
    public generatePARSimulation() {
        const parCalage = (this._formulaire.currentNub as Par);
        const pcal = parCalage.prms;
        let pres: { [key: string]: number } = parCalage.result.values;

        const varParams: VariatedDetails[] = this._formulaire.getVariatedParameters();
        if (varParams.length > 0) {
            // open popup to choose combination of varying parameters
            const mdParResults = new MultiDimensionResults();
            mdParResults.variatedParameters = varParams;
            const dialogRef = this.generatePARSimulationDialog.open(
                DialogGeneratePARSimulationComponent,
                {
                    data: {
                        results: mdParResults
                    },
                    disableClose: false
                }
            );
            dialogRef.afterClosed().subscribe(result => {
                if (result && result.generate) {
                    const i = result.selected;
                    const s = result.size; // longest variating series, ie. number of iterations
                    pres = parCalage.result.resultElements[i].values;
                    // generate set of fixed values from chosen iteration i
                    this.doGenerateParSimWithValues({
                        Q: pcal.Q.isCalculated ? pres.Q : (pcal.Q.hasMultipleValues ? pcal.Q.getInferredValuesList(s)[i] : pcal.Q.V),
                        Z1: pcal.Z1.hasMultipleValues ? pcal.Z1.getInferredValuesList(s)[i] : pcal.Z1.V,
                        Z2: pcal.Z2.hasMultipleValues ? pcal.Z2.getInferredValuesList(s)[i] : pcal.Z2.V,
                        S: pcal.S.hasMultipleValues ? pcal.S.getInferredValuesList(s)[i] : pcal.S.V,
                        P: pres.P,
                        L: pcal.L.isCalculated ? pres.L : (pcal.L.hasMultipleValues ? pcal.L.getInferredValuesList(s)[i] : pcal.L.V),
                        N: pcal.N.hasMultipleValues ? pcal.N.getInferredValuesList(s)[i] : pcal.N.V,
                        M: pcal.M.hasMultipleValues ? pcal.M.getInferredValuesList(s)[i] : pcal.M.V,
                        Nb: pres.Nb,
                        ZR1: pres.ZR1,
                        ZD1: pres.ZD1,
                        ZR2: pres.ZR2,
                        ZD2: pres.ZD2,
                        a: pcal.a.hasMultipleValues ? pcal.a.getInferredValuesList(s)[i] : pcal.a.V,
                    });
                }
            });
        } else {
            // no parameter is varyng, generate directly
            this.doGenerateParSimWithValues({
                Q: pcal.Q.V,
                Z1: pcal.Z1.V,
                Z2: pcal.Z2.V,
                S: pcal.S.V,
                P: pcal.P.V,
                L: pcal.L.V,
                N: pcal.N.V,
                M: pcal.M.V,
                Nb: pres.Nb,
                ZR1: pres.ZR1,
                ZD1: pres.ZD1,
                ZR2: pres.ZR2,
                ZD2: pres.ZD2,
                a: pcal.a.V
            });
        }
    }

    public get exportAllPbResultsEnabled(): boolean {
        const pb = (this._formulaire as FormulairePrebarrage).currentNub as PreBarrage;
        return (pb.result !== undefined && !pb.result.hasOnlyErrors);
    }

    /**
     * Download an XLSX file containing all the PreBarrage data
     * and results in a single table
     */
    public exportAllPbResults() {
        const pb = (this._formulaire as FormulairePrebarrage).currentNub as PreBarrage;
        const res = pb.result;
        const data: any[][] = [];
        let line: any[];

        // 1. trouver ce qui varie
        const varP = pb.findVariatedParams();
        for (const vd of varP) {
            line = [
                this.intlService.prefix(vd.param.parentNub, vd.param.symbol, true),
                this.intlService.prefix(
                    vd.param.parentNub,
                    this.formulaireService.expandVariableNameAndUnit(pb.calcType, vd.param.symbol, vd.param.unit)
                )
            ];
            for (let i = 0; i < res.resultElements.length; i++) {
                line.push(getUnformattedIthValue(vd.param, i));
            }
            data.push(line);
        }

        // 2. rivière
        line = [
            "Q",
            this.formulaireService.expandVariableNameAndUnit(pb.calcType, "Q", pb.prms.Q.unit)
        ];
        for (let i = 0; i < res.resultElements.length; i++) {
            line.push(getUnformattedIthValue(pb.prms.Q, i));
        }
        data.push(line);
        line = [
            "Z1",
            this.formulaireService.expandVariableNameAndUnit(pb.calcType, "Z1", pb.prms.Z1.unit)
        ];
        for (let i = 0; i < res.resultElements.length; i++) {
            line.push(getUnformattedIthValue(pb.prms.Z1, i));
        }
        data.push(line);
        line = [
            "Z2",
            this.formulaireService.expandVariableNameAndUnit(pb.calcType, "Z2", pb.prms.Z2.unit)
        ];
        for (let i = 0; i < res.resultElements.length; i++) {
            line.push(getUnformattedIthValue(pb.prms.Z2, i));
        }
        data.push(line);

        // 3. enfants dans l'ordre
        for (const c of pb.children) {
            // paramètres visibles dans l'ordre
            for (const p of c.prms) {
                if (p.visible) {
                    line = [
                        this.intlService.prefix(c, p.symbol, true),
                        this.intlService.prefix(c, this.formulaireService.expandVariableNameAndUnit(pb.calcType, p.symbol, p.unit))
                    ];
                    for (let i = 0; i < res.resultElements.length; i++) {
                        line.push(getUnformattedIthValue(p, i));
                    }
                    data.push(line);
                }
            }

            // pour les cloisons, paramètres des enfants
            if (c instanceof PbCloison) {
                for (const s of c.structures) {
                    // paramètres visibles dans l'ordre
                    for (const sp of s.prms) {
                        if (sp.visible) {
                            line = [
                                this.intlService.prefix(s, sp.symbol, true),
                                this.intlService.prefix(
                                    s, this.formulaireService.expandVariableNameAndUnit(pb.calcType, sp.symbol, sp.unit)
                                )
                            ];
                            for (let i = 0; i < res.resultElements.length; i++) {
                                line.push(getUnformattedIthValue(sp, i));
                            }
                            data.push(line);
                        }
                    }

                    // autres résultats dans l'ordre, sauf vCalc (lecture depuis resultElements[0], ça ou autre chose…)
                    for (const k of Object.keys(s.result.resultElements[0].values)) {
                        line = [
                            this.intlService.prefix(s, k, true),
                            this.intlService.prefix(s, this.formulaireService.expandVariableNameAndUnit(pb.calcType, k)),
                        ];
                        for (let i = 0; i < res.resultElements.length; i++) {
                            const iRes = getUnformattedIthResult(s.result, k, i);
                            if (k.indexOf("ENUM_") === 0 && iRes !== undefined) {
                                line.push(this.intlService.localizeText(`INFO_${k.toUpperCase()}_${Number(iRes)}`));
                            } else {
                                line.push(iRes);
                            }
                        }
                        data.push(line);
                    }
                }
            }

            // autres résultats dans l'ordre, sauf vCalc (lecture depuis resultElements[0], ça ou autre chose…)
            for (const k of Object.keys(c.result.resultElements[0].values)) {
                line = [
                    this.intlService.prefix(c, k, true),
                    this.intlService.prefix(c, this.formulaireService.expandVariableNameAndUnit(pb.calcType, k)),
                ];
                for (let i = 0; i < res.resultElements.length; i++) {
                    const iRes = getUnformattedIthResult(c.result, k, i);
                    if (k.indexOf("ENUM_") === 0 && iRes !== undefined) {
                        line.push(this.intlService.localizeText(`INFO_${k.toUpperCase()}_${Number(iRes)}`));
                    } else {
                        line.push(iRes);
                    }
                }
                data.push(line);
            }
        }

        const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data);
        AppComponent.downloadSpreadsheet(ws);
    }

    /**
     * Creates a new Formulaire with a ParSimulation Nub, using given
     * values as parameters
     */
    protected async doGenerateParSimWithValues(v: any) {
        const parCalage = (this._formulaire.currentNub as Par);
        const psim = new ParSimulationParams(
            round(v.Q, 3), round(v.Z1, 3), round(v.Z2, 3),
            round(v.S, 3), round(v.P, 3), round(v.Nb, 3),
            round(v.ZR1, 3), round(v.ZD1, 3), round(v.ZR2, 3),
            round(v.ZD2, 3), round(v.L, 3), round(v.a, 3),
            round(v.N, 3), round(v.M, 3)
        );
        const parSimulation = new ParSimulation(psim);
        parSimulation.parType = parCalage.parType;
        Session.getInstance().registerNub(parSimulation);

        const f: FormulaireDefinition = await this.formulaireService.createFormulaire(CalculatorType.ParSimulation, parSimulation);
        // calculate
        f.doCompute();
        // go to new ParSimulation
        this.router.navigate(["/calculator", f.uid]);
    }

    /**
     * Opens a modal that allows to choose a predefined Espece
     * and load its values in current Espece module
     */
    public loadPredefinedEspece() {
        const dialogRef = this.loadPredefinedEspeceDialog.open(
            DialogLoadPredefinedEspeceComponent,
            {
                data: {},
                disableClose: false
            }
        );
        dialogRef.afterClosed().subscribe(result => {
            if (result && result.load) {
                const form = this._formulaire as FormulaireFixedVar;
                const nub = (form.currentNub as Espece);
                nub.loadPredefinedSpecies(result.selected);
                form.refreshFieldsets(false);
            }
        });
    }

    public saveCalculator() {
        this.formulaireService.saveForm(this._formulaire);
    }

    public closeCalculator() {
        const dialogRef = this.confirmCloseCalcDialog.open(
            DialogConfirmCloseCalcComponent,
            {
                data: {
                    uid: this._formulaire.currentNub.uid
                },
                disableClose: true
            }
        );
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.onCloseForm();
            }
        });
    }

    /**
     * Duplicates the current calculator form
     */
    public async cloneCalculator() {
        const serialisedNub: string = this._formulaire.currentNub.serialise({ title: this._formulaire.calculatorName });
        const nubPointer = Session.getInstance().unserialiseSingleNub(serialisedNub);
        const f = await this.formulaireService.createFormulaire(nubPointer.nub.calcType, nubPointer.nub, nubPointer.meta.title);
        this.router.navigate(["/calculator", f.uid]);
    }
}
