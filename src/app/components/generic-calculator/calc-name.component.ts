import { Component, Input } from "@angular/core";
import { GenericInputComponentDirective } from "../generic-input/generic-input.component";
import { FormulaireDefinition } from "../../formulaire/definition/form-definition";
import { I18nService } from "../../services/internationalisation.service";
import { ApplicationSetupService } from "../../services/app-setup.service";

@Component({
    selector: "calc-name",
    templateUrl: "./calc-name.component.html",
    styleUrls: [
        "./calc-name.component.scss"
    ]
})
export class CalculatorNameComponent extends GenericInputComponentDirective {

    constructor(
        intlService: I18nService,
        appSetupService: ApplicationSetupService
    ) {
        super(null, intlService, appSetupService);
    }

    @Input()
    public set model(v: any) {
        super.model = v;
    }

    /**
     * formulaire géré
     */
    private get _form(): FormulaireDefinition {
        return this._model as FormulaireDefinition;
    }

    /**
     * retourne la valeur du modèle
     */
    protected getModelValue(): any {
        if (this._form) {
            return this._form.calculatorName;
        }
    }

    /**
     * affecte la valeur du modèle
     */
    protected setModelValue(sender: any, v: any) {
        this._form.calculatorName = v;
        this.updateAndValidateUI();
    }

    protected validateModelValue(v: any): { isValid: boolean, message: string } {
        // no model validation for a simple string
        return { isValid: true, message: undefined };
    }

    protected validateUIValue(ui: string): { isValid: boolean, message: string } {
        let valid = false;
        let msg: string;

        if (ui === undefined || ui.length < 1) {
            msg = "Veuillez entrer un nom";
        } else {
            valid = true;
        }

        return { isValid: valid, message: msg };
    }

    public updateModelFromUI() {
        if (this.validateUI()) {
            this.setAndValidateModel(this, this.uiValue); // do NOT cast UI value to Number
        }
    }
}
