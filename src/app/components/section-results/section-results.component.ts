import { Component, ElementRef, Input } from "@angular/core";

import { SectionResults } from "../../results/section-results";
import { CalculatorResults } from "../../results/calculator-results";
import { I18nService } from "../../services/internationalisation.service";
import { AppComponent } from "../../app.component";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";

@Component({
    selector: "section-results",
    templateUrl: "./section-results.component.html",
    styleUrls: [
        "./section-results.component.scss"
    ]
})
export class SectionResultsComponent extends ResultsComponentDirective {

    /** taille (pixels) du canvas (c'est un carré) */
    public _size: number;

    /** hardcoded bullet-proof default canvas size **/
    private previousContainerSize: number;

    private defaultSize = 400;

    private minimalSize = 300;

    /**  résultats non mis en forme */
    private _results: SectionResults;

    constructor(
        private intlService: I18nService,
        private element: ElementRef,
    ) {
        super();
        this.previousContainerSize = this.defaultSize;
        this._size = this.previousContainerSize;
    }

    @Input()
    public set results(rs: CalculatorResults[]) {
        this._results = undefined;
        if (rs) {
            for (const r of rs) {
                if (r instanceof SectionResults) {
                    this._results = r;
                }
            }
        }
    }

    public get hasResults(): boolean {
        return this._results && this._results.hasResults;
    }

    public get section() {
        return this._results.section;
    }

    public get result() {
        return this._results.result;
    }

    public get size() {
        return this._size;
    }

    public exportAsImage(element: HTMLDivElement) {
        AppComponent.exportAsImage(element.querySelector("canvas"));
    }

    /** redraw canvas on fullscreen state change (scale drawing) */
    public fullscreenChange(isFullscreen: boolean) {
        this._size = this.getContainerSize(! isFullscreen, isFullscreen);
        this.previousContainerSize = this._size;
    }

    private getContainerSize(useDefaultSize: boolean = false, useMinOfWidthAndHeight: boolean = false): number {
        const container = this.element.nativeElement.querySelector(".section-results-container");
        let size: number;
        if (container) {
            if (useMinOfWidthAndHeight) {
                // when going to fullscreen mode, ensure the drawing
                // is not larger or higher than the screen
                size = Math.min(container.offsetWidth, container.offsetHeight);
            } else {
                size = container.offsetWidth;
            }
        } else {
            size = this.previousContainerSize;
        }
        // when going back from fullscreen mode, container size tends to be
        // too high for whatever reason; use defaultSize on this purpose
        if (useDefaultSize) {
            size = this.defaultSize;
        }
        // lower boundary
        size = Math.max(size, this.minimalSize);
        // remove 10% in "fullscreen" mode
        if (size > this.minimalSize) {
            size = Math.max(size * 0.9, this.minimalSize);
        }

        return size;
    }

    public get uitextExportImageTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXPORT_IMAGE");
    }

    public get uitextEnterFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_ENTER_FS");
    }

    public get uitextExitFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXIT_FS");
    }
}
