import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from "@angular/material/legacy-dialog";
import { Inject, Component } from "@angular/core";

@Component({
    selector: "dialog-show-message",
    templateUrl: "dialog-show-message.component.html",
    styleUrls: ["dialog-show-message.component.scss"]
})
export class DialogShowMessageComponent {

    private title: string;

    private message: string;

    constructor(
        public dialogRef: MatDialogRef<DialogShowMessageComponent>,
        @Inject(MAT_DIALOG_DATA) data: any
    ) {
        this.title = data.title;
        this.message = data.message;
    }

    public get uitextTitle() {
        return this.title;
    }

    public get uitextMessage() {
        return this.message;
    }

    public get uitextClose() {
        return "Close";
    }
}
