import { Component, Input } from "@angular/core";

import { FixedResultsComponent } from "../fixedvar-results/fixed-results.component";
import { NgParameter } from "../../formulaire/elements/ngparam";
import { getIthValue } from "../../util/util";
import { PbCloisonResults } from "../../results/pb-cloison-results";

import { Result, ResultElement } from "jalhyd";

@Component({
    selector: "pb-cloison-results",
    templateUrl: "../fixedvar-results/fixed-results.component.html",
    styleUrls: [
        "../fixedvar-results/fixed-results.component.scss"
    ]
})
export class PbCloisonResultsComponent extends FixedResultsComponent {

    @Input()
    public set results(r: PbCloisonResults) {
        this._fixedResults = r;
    }

    public get results(): PbCloisonResults {
        return this._fixedResults as PbCloisonResults;
    }

    /** Retourne la valeur du paramètre fixe… qui n'en est pas forcément un ici ! */
    protected getFixedParamValue(fp: NgParameter): string {
        let val: string;
        if (fp.paramDefinition.hasMultipleValues) {
            val = getIthValue(fp.paramDefinition, this.results.variableIndex, this.results.size);
        } else {
            val = this.formattedValue(fp.getValue());
        }
        return val;
    }

    /** Retourne l'élément de résultat en cours, en fonction de l'index variable */
    protected getResultElement(r: Result): ResultElement {
        return r.resultElements[this.results.variableIndex];
    }
}
