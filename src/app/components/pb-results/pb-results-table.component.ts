import { Component, ViewChild, ElementRef, Input, OnChanges } from "@angular/core";

import { PreBarrage, PbBassin } from "jalhyd";

import { I18nService } from "../../services/internationalisation.service";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";
import { AppComponent } from "../../app.component";
import { fv, getIthValue } from "../../util/util";
import { PrebarrageResults } from "../../results/prebarrage-results";

@Component({
    selector: "pb-results-table",
    templateUrl: "./pb-results-table.component.html",
    styleUrls: [
        "./pb-results-table.component.scss"
    ]
})
export class PbResultsTableComponent extends ResultsComponentDirective implements OnChanges {

    /** résultats non mis en forme */
    private _pbResults: PrebarrageResults;

    /** entêtes des colonnes */
    private _headers: string[];

    /** résultats mis en forme */
    private _dataSet: any[];

    /** index de l'élément de résultat à afficher (modifié par le sélecteur de conditions limites) */
    @Input()
    public variableIndex = 0;

    @ViewChild("tableContainer")
    table: ElementRef;

    constructor(
        protected intlService: I18nService
    ) {
        super();
    }

    @Input()
    public set results(r: PrebarrageResults) {
        this._pbResults = r;
    }

    public ngOnChanges() {
        this._dataSet = [];
        if (
            this._pbResults
            && this._pbResults.bassinsResults
            && this._pbResults.bassinsResults.length > 0
            && !this._pbResults.hasOnlyErrors()
        ) {
            const pr = this._pbResults;
            const pb = pr.result.sourceNub as PreBarrage;

            // @TODO results.size ? To extend values lists…

            // refresh headers here if language changed
            this._headers = pr.headers;

            // upstream line
            if (pr.result.resultElements[this.variableIndex]?.vCalc) { // le calcul peut avoir échoué
                this._dataSet.push([
                    this.intlService.localizeText("INFO_LIB_AMONT"),
                    "", "",
                    getIthValue(pb.prms.Z1, this.variableIndex, this._pbResults.size),
                    "", "",
                    getIthValue(pb.prms.Q, this.variableIndex, this._pbResults.size),
                ]);
            }

            // basins 1 - n
            for (let i = 0; i < pr.bassinsResults.length; i++) {
                if (
                    pr.bassinsResults[i].resultElements[this.variableIndex] !== undefined
                    && Object.keys(pr.bassinsResults[i].resultElements[this.variableIndex].values).length > 0 // no vCalc in this case
                ) {
                    const rb = pr.bassinsResults[i].resultElements[this.variableIndex].values;
                    const basin = pr.bassinsResults[i].sourceNub as PbBassin;
                    this._dataSet.push([
                        i + 1, // n° cloison
                        fv(basin.prms.S.V),
                        fv(basin.prms.ZF.V),
                        fv(rb.Z),
                        fv(rb.PV),
                        fv(rb.YMOY),
                        fv(rb.Q)
                    ]);
                }
            }

            // downstream line
            if (pr.result.resultElements[this.variableIndex]?.vCalc) { // le calcul peut avoir échoué
                this._dataSet.push([
                    this.intlService.localizeText("INFO_LIB_AVAL"),
                    "", "",
                    getIthValue(pb.prms.Z2, this.variableIndex, this._pbResults.size),
                    "", "",
                    getIthValue(pb.prms.Q, this.variableIndex, this._pbResults.size),
                ]);
            }
        }
    }

    public get headers() {
        return this._headers;
    }

    /**
     * Returns a combination of parameters and results for mat-table
     */
    public get dataSet() {
        return this._dataSet;
    }

    public exportAsSpreadsheet() {
        AppComponent.exportAsSpreadsheet(this.table.nativeElement);
    }

    public get uitextExportAsSpreadsheet() {
        return this.intlService.localizeText("INFO_RESULTS_EXPORT_AS_SPREADSHEET");
    }

    public get uitextEnterFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_ENTER_FS");
    }

    public get uitextExitFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXIT_FS");
    }
}
