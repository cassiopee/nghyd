import { Component, Input } from "@angular/core";

import { CalculatorResults } from "../../results/calculator-results";
import { PrebarrageResults } from "../../results/prebarrage-results";
import { I18nService } from "../../services/internationalisation.service";

import { cLog, Message, MessageCode, MessageSeverity, PreBarrage, Result } from "jalhyd";
import { ServiceFactory } from "app/services/service-factory";
import { LogHelper } from "app/results/log-helper";

@Component({
    selector: "pb-results",
    templateUrl: "./pb-results.component.html",
    styleUrls: [
        "./pb-results.component.scss"
    ]
})
export class PbResultsComponent {

    /** résultats des bassins, non mis en forme */
    private _pbResults: PrebarrageResults;

    private _logHelper: LogHelper;

    constructor(
        private i18nService: I18nService,
    ) { }

    @Input()
    public set results(rs: CalculatorResults[]) {
        this._pbResults = undefined;
        this._logHelper = undefined;
        for (const r of rs) {
            if (r instanceof PrebarrageResults) {
                this._pbResults = r as PrebarrageResults;
                // log helper
                this._logHelper = new LogHelper(this._pbResults);
            }
        }
    }

    public get pbResults() {
        return this._pbResults;
    }

    // true if any result is present
    public get hasResults(): boolean {
        return this._pbResults && this._pbResults.hasResults;
    }

    // true if basin results are present
    public get hasBasinResults(): boolean {
        return this._pbResults && this._pbResults.hasBasinResults;
    }

    // true if basin results at current iteration have data
    public get basinResultsHaveData(): boolean {
        return this._pbResults && this._pbResults.result && ! this._pbResults.result.hasOnlyErrors && this._pbResults.basinResultsHaveData;
    }

    // true if wall results are present
    public get hasWallResults(): boolean {
        return this._pbResults && this._pbResults.hasWallResults;
    }

    public get globalLog(): cLog {
        return this._logHelper.globalLog;
    }

    public get iterationLog(): cLog {
        return this._logHelper.iterationLog;
    }

    public get uitextGeneralLogTitle(): string {
        return this.i18nService.localizeText("INFO_TITREJOURNAL_GLOBAL");
    }

}
