// cf. https://blog.thoughtram.io/angular/2016/07/27/custom-form-controls-in-angular-2.html

import { Component, ChangeDetectorRef } from "@angular/core";

import { Message, ParamDefinition, ParamDomain, ParamDomainValue, Observable } from "jalhyd";

import { I18nService } from "../../services/internationalisation.service";
import { GenericInputComponentDirective } from "../generic-input/generic-input.component";
import { ServiceFactory } from "../../services/service-factory";
import { NgParameter } from "../../formulaire/elements/ngparam";
import { ApplicationSetupService } from "../../services/app-setup.service";

export class NgBaseParam extends Observable {
    private _param: ParamDefinition;

    constructor(symb: string, d: ParamDomain | ParamDomainValue, val: number, unit?: string) {
        super();
        this._param = new ParamDefinition(null, symb, d, unit, val, undefined, undefined, false);
    }

    public get param() {
        return this._param;
    }

    public get symbol() {
        return this._param.symbol;
    }

    public get isDefined() {
        return this._param.isDefined;
    }

    public getValue() {
        return this._param.getValue();
    }

    public checkValue(val: number) {
        return this._param.checkValueAgainstDomain(val);
    }

    public checkMin(min: number): boolean {
        return this._param.checkMin(min);
    }

    public checkMax(max: number): boolean {
        return this._param.checkMax(max);
    }

    public checkMinMaxStep(step: number): boolean {
        return this._param.checkMinMaxStep(step);
    }

    public setValue(val: number) {
        this._param.setValue(val);
        this.notifyObservers(val);
    }

    public get value() {
        return this.getValue();
    }

    public set value(val: number) {
        this.setValue(val);
    }

    public validateModelValue(v: any): { isValid: boolean, message: string } {
        let msg: string;
        let valid = false;

        if (v === null || v === "") {
            // NULL values are always invalid
            msg = ServiceFactory.i18nService.localizeText("ERROR_PARAM_NULL");
        } else {
            try {
                this._param.checkValueAgainstDomain(v);
                valid = true;
            } catch (e) {
                if (e instanceof Message) {
                    // ici au début le service de localisation n'a pas encore chargé ses messages…
                    msg = ServiceFactory.i18nService.localizeMessage(e);
                } else {
                    msg = "invalid value";
                }
            }
        }

        return { isValid: valid, message: msg };
    }
}

@Component({
    selector: "base-param-input",
    templateUrl: "../generic-input/generic-input.component.html",
})
export class BaseParamInputComponent extends GenericInputComponentDirective {
    constructor(
        intlService: I18nService,
        appSetupService: ApplicationSetupService,
        cdRef: ChangeDetectorRef
    ) {
        super(cdRef, intlService, appSetupService);
    }

    /**
     * paramètre géré
     */
    private get _paramDef(): NgParameter {
        return this._model as NgParameter;
    }

    /**
     * valeur intermédiaire nécessitée par le fait que toutes les valeurs numériques ne sont pas légales
     * pour NgParameter (l'affecter peut provoquer une exception) et qui permet de faire fonctionner la validation du modèle
     */
    private _tmp: number;

    protected afterSetModel() {
        this._tmp = this._paramDef.getValue();
    }

    protected getModelValue(): any {
        return this._tmp;
    }

    protected setModelValue(sender: any, v: any) {
        this._tmp = v;
        try {
            this._paramDef.setValue(null, v);
        } catch (e) {
            // géré par validateModelValue()
        }
    }

    protected validateModelValue(v: any): { isValid: boolean, message: string } {
        if (this._model instanceof NgBaseParam) {
            return this._model.validateModelValue(v);
        }
    }
}
