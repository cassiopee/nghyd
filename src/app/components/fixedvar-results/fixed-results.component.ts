import { Component, ViewChild, ElementRef, Input } from "@angular/core";

import { FixedResults } from "../../results/fixed-results";
import { I18nService } from "../../services/internationalisation.service";
import { FormulaireService } from "../../services/formulaire.service";
import { ResultsComponentDirective } from "./results.component";
import { AppComponent } from "../../app.component";
import { NgParameter } from "../../formulaire/elements/ngparam";

import { capitalize, Result, ResultElement } from "jalhyd";

import { sprintf } from "sprintf-js";
import { PressureLoss } from "jalhyd";

@Component({
    selector: "fixed-results",
    templateUrl: "./fixed-results.component.html",
    styleUrls: [
        "./fixed-results.component.scss"
    ]
})
export class FixedResultsComponent extends ResultsComponentDirective {

    /** résultats non mis en forme */
    protected _fixedResults: FixedResults;

    @ViewChild("tableContainer")
    table: ElementRef;

    public tableColumns = [ "parametre", "valeur" ];

    constructor(
        private intlService: I18nService,
        private formService: FormulaireService
    ) {
        super();
    }

    @Input()
    public set results(r: FixedResults) {
        this._fixedResults = r;
    }

    public get hasParameterResult(): boolean {
        return this._fixedResults && this._fixedResults.hasResults;
    }

    public get uitextParamFixes() {
        return this.intlService.localizeText("INFO_CALCULATOR_PARAMFIXES");
    }

    public get uitextValeurs() {
        return this.intlService.localizeText("INFO_CALCULATOR_VALEURS");
    }

    public get uitextExportAsSpreadsheet() {
        return this.intlService.localizeText("INFO_RESULTS_EXPORT_AS_SPREADSHEET");
    }

    public get fixedParams() {
        return this._fixedResults && this._fixedResults.fixedParameters;
    }

    public get hasFixedParameters() {
        return this._fixedResults && this._fixedResults.fixedParameters.length > 0;
    }

    /**
     * Returns a set of parameters and results for mat-table
     */
    public get dataSet() {
        if (
            this._fixedResults !== undefined
            && Array.isArray(this._fixedResults.variablesOrder)
            && this._fixedResults.variablesOrder.length > 0
        ) {
            return this.buildCustomOrderedDataset();
        } else {
            return this.buildDefaultOrderedDataset();
        }
    }

    /**
     * When this.variablesOrder is defined and not empty, builds an ordered set of
     * data according to the given symbols order; variables whose symbol is not
     * mentioned in this.variablesOrder are not displayed !
     */
    protected buildCustomOrderedDataset() {
        const data = [];
        let resultFound: boolean;
        for (const symbol of this._fixedResults.variablesOrder) {

            // is it a fixed parameter ?
            for (const fp of this.fixedParams) {
                if (fp.symbol === symbol) {
                    let label = this.formattedLabel(fp);
                    const nub = fp.paramDefinition.parentNub;
                    // add child type and position before label
                    if (nub && nub.parent && nub.intlType) {
                        const pos = nub.findPositionInParent();
                        // label = this.intlService.localizeText("INFO_OUVRAGE") + " n°" + (pos + 1) + ": " + label;
                        const cn = capitalize(this.intlService.childName(nub));
                        label = sprintf(this.intlService.localizeText("INFO_STUFF_N"), cn)
                            + (pos + 1) + ": " + label;
                    }
                    label += this._fixedResults.getHelpLink(symbol);
                    data.push({
                        label: label,
                        value: this.getFixedParamValue(fp),
                        isCalcResult: false // for CSS
                    });
                }
            }
        }

        for (const symbol of this._fixedResults.variablesOrder) {
            resultFound = false;
            // 1) is it a result or child result ?
            const res = this._fixedResults.result;
            if (
                res
                && res.resultElements.length > 0
                && this.getResultElement(res)
                && this.getResultElement(res).count() > 0
            ) {
                const sn = this._fixedResults.result.sourceNub;
                let found = false;

                // 2.1 all results
                for (const k of this.getResultElement(res).keys) {
                    if (k === symbol) {
                        found = true;
                        const er: number = this.getResultElement(res).getValue(k);
                        // calculator type for translation
                        let ct = sn.calcType;
                        if (sn.parent) {
                            ct = sn.parent.calcType;
                        }
                        let unit: string;
                        // is k the calculated parameter ? If so, extract its unit
                        try {
                            const p = res.sourceNub.getParameter(k);
                            if (p) {
                                unit = p.unit;
                            }
                        } catch (e) { /* silent fail */ }
                        let label = this.formService.expandVariableNameAndUnit(ct, k, unit);
                        label += this._fixedResults.getHelpLink(symbol);
                        data.push({
                            label: label,
                            value: this.intlService.formatResult(k, er),
                            isCalcResult: true // for CSS
                        });
                    }
                }

                resultFound = found;
                // 2.2. children results
                if (! found) {
                    for (const c of sn.getChildren()) {
                        if (c.result) {
                            for (const k of this.getResultElement(c.result).keys) {
                                if (k === symbol) {
                                    resultFound = true;
                                    const er: number = this.getResultElement(c.result).getValue(k);
                                    // calculator type for translation
                                    let ct = sn.calcType;
                                    if (sn.parent) {
                                        ct = sn.parent.calcType;
                                    }
                                    const cn = capitalize(this.intlService.childName(c));
                                    let label = sprintf(this.intlService.localizeText("INFO_STUFF_N"), cn)
                                        + (c.findPositionInParent() + 1) + " : "
                                        + this.formService.expandVariableNameAndUnit(ct, k);
                                    label += this._fixedResults.getHelpLink(symbol);
                                    data.push({
                                        label: label,
                                        value: this.intlService.formatResult(k, er),
                                        isCalcResult: true // for CSS
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
        return data;
    }

    /**
     * When this.variablesOrder is empty or undefined, builds an ordered set of
     * data with fixed parameters first, then parent results, then children results
     */
    protected buildDefaultOrderedDataset() {
        const data = [];
        // 1. fixed parameters
        for (const fp of this.fixedParams) {
            let label = this.formattedLabel(fp);
            const nub = fp.paramDefinition.parentNub;
            // add child type and position before label
            if (nub && nub.parent && nub.intlType) {
                const pos = nub.findPositionInParent();
                const cn = capitalize(this.intlService.childName(nub));
                label = sprintf(this.intlService.localizeText("INFO_STUFF_N"), cn)
                    + (pos + 1) + ": " + label;
            }
            label += this._fixedResults.getHelpLink(fp.symbol);
            data.push({
                label: label,
                value: this.getFixedParamValue(fp),
                isCalcResult: false // for CSS
            });
        }
        // 2. calculation results
        const res = this._fixedResults.result;
        if (
            res
            && res.resultElements.length > 0
            && this.getResultElement(res)
            && this.getResultElement(res).count() > 0
        ) {
            const sn = this._fixedResults.result.sourceNub;

            // 2.1 all results
            for (const k of this.getResultElement(res).keys) {
                const er: number = this.getResultElement(res).getValue(k);
                // calculator type for translation
                let ct = sn.calcType;
                if (sn.parent) {
                    ct = sn.parent.calcType;
                }
                let unit: string;
                // is k the calculated parameter ? If so, extract its unit
                try {
                    const p = res.sourceNub.getParameter(k);
                    if (p) {
                        unit = p.unit;
                    }
                } catch (e) { /* silent fail */ }
                let label = this.formService.expandVariableNameAndUnit(ct, k, unit);
                label += this._fixedResults.getHelpLink(k);
                data.push({
                    label: label,
                    value: this.intlService.formatResult(k, er),
                    isCalcResult: true // for CSS
                });
            }

            // 2.2. children results
            for (const c of sn.getChildren()) {
                if (c.result) {
                    for (const k of this.getResultElement(c.result).keys) {
                        const er: number = this.getResultElement(c.result).getValue(k);
                        // calculator type for translation
                        let ct = sn.calcType;
                        if (sn.parent) {
                            ct = sn.parent.calcType;
                        }

                        // do not display child rank in case of pressure loss law
                        const displayChildRank = !(sn instanceof PressureLoss);
                        let label: string;
                        if (displayChildRank) {
                            const cn = capitalize(this.intlService.childName(c));
                            label = sprintf(this.intlService.localizeText("INFO_STUFF_N"), cn) + (c.findPositionInParent() + 1);
                        }
                        else {
                            label = capitalize(this.intlService.childName(c));
                        }

                        label += " : ";
                        label += this.formService.expandVariableNameAndUnit(ct, k);
                        label += this._fixedResults.getHelpLink(k);
                        data.push({
                            label: label,
                            value: this.intlService.formatResult(k, er),
                            isCalcResult: true // for CSS
                        });
                    }
                }
            }
        }
        return data;
    }

    /**
     * Retourne la valeur du paramètre fixe; redéfini
     * dans PbCloisonResultsComponent notamment
     */
    protected getFixedParamValue(fp: NgParameter): string {
        return this.formattedValue(fp);
    }

    /**
     * Retourne l'élément de résultat en cours pour le résultat donné;
     * redéfini dans PbCloisonResultsComponent notamment
     */
    protected getResultElement(r: Result): ResultElement {
        return r.resultElement;
    }

    public exportAsSpreadsheet() {
        AppComponent.exportAsSpreadsheet(this.table.nativeElement, true);
    }

    /** trackBy:index simulator @see nghyd#364 */
    public tbIndex(index: number, item: any) {
        return index;
    }
}
