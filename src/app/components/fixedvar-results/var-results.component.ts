import { Component, ViewChild, ElementRef, Input, OnInit } from "@angular/core";

import { MatDialog } from "@angular/material/dialog";

import { VarResults } from "../../results/var-results";
import { ResultElement, Message, MessageSeverity, Observer } from "jalhyd";
import { I18nService } from "../../services/internationalisation.service";
import { ResultsComponentDirective } from "./results.component";
import { DialogLogEntriesDetailsComponent } from "../dialog-log-entries-details/dialog-log-entries-details.component";
import { AppComponent } from "../../app.component";
import { longestVarParam } from "../../../app/util/util";

@Component({
    selector: "var-results",
    templateUrl: "./var-results.component.html",
    styleUrls: [
        "./var-results.component.scss"
    ]
})
export class VarResultsComponent extends ResultsComponentDirective implements Observer, OnInit {

    /** size of the longest variated parameter */
    public size: number;

    /** résultats non mis en forme */
    protected _varResults: VarResults;

    /** résultats mis en forme */
    protected _results: any[];

    /** entêtes des colonnes (param à varier, résultats) */
    protected _headers: string[];

    /** messages de log issus des résultats variés */
    protected _messages: Message[];

    @ViewChild("tableContainer")
    table: ElementRef;

    constructor(
        protected intlService: I18nService,
        protected logEntriesDetailsDialog: MatDialog
    ) {
        super();
        this.intlService.addObserver(this);
    }

    /** Refreshes results and builds the dataset */
    @Input()
    public set results(r: VarResults) {
        this._varResults = r;
        this.updateResults();
    }

    private updateResults() {
        this._results = [];
        this._headers = [];
        this._messages = [];

        if (this._varResults) {
            const sn = this._varResults.result.sourceNub;
            // A. gather messages
            for (const re of this._varResults.resultElements) {
                this._messages = this._messages.concat(re.log.messages); // es6 concat;
            }

            // B. build headers
            if (this._messages.length > 0) { // has log messages
                this._headers.push("logMessagesColumn");
            }
            this._headers = this._headers.concat(this._varResults.variableParamHeaders);
            this._headers = this._headers.concat(this._varResults.resultHeaders);

            // C. pre-extract variable parameters values
            const varValues = [];
            // find longest list
            const lvp = longestVarParam(this._varResults.variatedParameters);
            this.size = lvp.size;
            // get extended values lists for each variable parameter
            for (const v of this._varResults.variatedParameters) {
                const vv = [];
                const iter = v.param.getExtendedValuesIterator(this.size);
                while (iter.hasNext) {
                    const nv = iter.next();
                    vv.push(this.formattedValue(nv.value));
                }
                varValues.push(vv);
            }

            // D. build dataset
            for (let i = 0; i < this._varResults.resultElements.length; i++) {
                const re: ResultElement = this._varResults.resultElements[i];
                if (re) {
                    // build ordered list of : variable params values; result; extra results
                    const list = [];

                    // log messages for this computation step
                    if (this._messages.length > 0 && re.log.messages.length > 0) {
                        // find highest log level to display
                        let highest = 100;
                        for (const lm of re.log.messages) {
                            highest = Math.min(highest, lm.getSeverity());
                        }
                        list.push({
                            messages: re.log.messages,
                            isInfo: (highest === MessageSeverity.INFO),
                            isWarning: (highest === MessageSeverity.WARNING),
                            isError: (highest === MessageSeverity.ERROR)
                        });
                    } else {
                        list.push({ messages: [] }); // empty log element to preserve row length
                    }

                    // 1. variable params values for this computation step
                    for (const vv of varValues) {
                        list.push(vv[i]);
                    }

                    // 2 all results
                    for (const k of this._varResults.resultKeys) {
                        list.push(this.intlService.formatResult(k, re.getValue(k)));
                    }

                    // 3 children results
                    for (const c of sn.getChildren()) {
                        if (c.result) {
                            for (const k of c.result.resultElements[i].keys) {
                                const er: number = c.result.resultElements[i].getValue(k);
                                list.push(this.intlService.formatResult(k, er));
                            }
                        }
                    }

                    this._results.push(list);
                }
            }
        }
    }

    public get hasMessages() {
        return this._messages.length > 0;
    }

    public get hasResults(): boolean {
        return this._varResults && this._varResults.hasResults;
    }

    public get headers() {
        return this._headers;
    }

    public get headersWithoutLogColumn() {
        if (this.hasMessages) {
            return this._headers.slice(1);
        } else {
            return this._headers;
        }
    }

    /**
     * Returns a combination of parameters and results for mat-table
     */
    public get dataSet() {
        return this._results;
    }

    public exportAsSpreadsheet() {
        AppComponent.exportAsSpreadsheet(this.table.nativeElement);
    }

    public get uitextExportAsSpreadsheet() {
        return this.intlService.localizeText("INFO_RESULTS_EXPORT_AS_SPREADSHEET");
    }

    public get uitextEnterFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_ENTER_FS");
    }

    public get uitextExitFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXIT_FS");
    }

    /** Shows a modal displaying the log messages details for a calcutation step */
    public openLogDetails(messages: Message[]) {
        if (this.isFullscreen) {
            this.exitFullscreen();
        }
        this.logEntriesDetailsDialog.open(
            DialogLogEntriesDetailsComponent,
            {
                data: {
                    messages: messages
                },
                autoFocus: false
            }
        );
    }

    ngOnInit(): void {
        this._varResults.updateCalculatedParameterHeader();
    }

    // Observer interface

    update(sender: any, data: any): void {
        if (sender instanceof I18nService) {
            this._varResults.update();
        }
    }
}
