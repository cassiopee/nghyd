import { Component, Input } from "@angular/core";

import { FixedResults } from "../../results/fixed-results";
import { VarResults } from "../../results/var-results";
import { CalculatorResults } from "../../results/calculator-results";
import { Result, cLog } from "jalhyd";
import { ResultsComponentDirective } from "./results.component";

@Component({
    selector: "fixedvar-results",
    templateUrl: "./fixedvar-results.component.html",
    styleUrls: [
        "./fixedvar-results.component.scss"
    ]
})
export class FixedVarResultsComponent extends ResultsComponentDirective {
    /**
     * résultats non mis en forme
     */
    protected _fixedResults: FixedResults;
    protected _varResults: VarResults;

    @Input()
    public set results(rs: CalculatorResults[]) {
        this._fixedResults = undefined;
        this._varResults = undefined;
        if (rs !== undefined) {
            for (const r of rs) {
                if (r instanceof FixedResults) {
                    this._fixedResults = r;
                } else if (r instanceof VarResults) {
                    this._varResults = r;
                }
            }
        }
    }

    private mergeLog(result: Result, log: cLog) {
        if (result && result.hasLog()) {
            if (result.hasGlobalLog()) {
                log.addLog(result.globalLog);
            } else {
                log.addLog(result.log);
            }
        }
    }

    public get mergedGlobalLogs(): cLog {
        const res = new cLog();
        if (this._fixedResults) {
            this.mergeLog(this._fixedResults.result, res);
        }
        if (this._varResults) {
            this.mergeLog(this._varResults.result, res);
        }
        return res;
    }

    /**
     * affichage de la table des résultats fixés
     */
    public get showFixedResults(): boolean {
        return this._fixedResults && this._fixedResults.hasResults;
    }

    /**
     * affichage de la table des résultats variés
     */
    public get showVarResults(): boolean {
        return this._varResults && this._varResults.hasResults;
    }

    /**
     * affichage du graphique des résultats variés
     */
    public get showVarResultsChart(): boolean {
        return this._varResults && this._varResults.hasPlottableResults();
    }

    public getFixedResultClass(i: number) {
        // tslint:disable-next-line:no-bitwise
        return "result_id_" + String(i & 1);
    }

    public get fixedParams() {
        return this._fixedResults.fixedParameters;
    }

    public get fixedResults() {
        return this._fixedResults;
    }

    public get varResults() {
        return this._varResults;
    }

    public get hasResults(): boolean {
        return this._fixedResults?.hasResults;
    }
}
