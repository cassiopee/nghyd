import screenfull from "screenfull";

import { NgParameter } from "../../formulaire/elements/ngparam";
import { ServiceFactory } from "../../services/service-factory";
import { fv } from "../../util/util";
import { CalculatorResults } from "../../results/calculator-results";

import { Directive, HostListener } from "@angular/core";

/**
 * Base class for results components, including common features
 */
@Directive()
export class ResultsComponentDirective {

    /** max number of decimals for auto-adjusting charts axis graduations */
    public static CHARTS_AXIS_PRECISION = 10;

    /** tracks the fullscreen state */
    public get isFullscreen() {
        if (screenfull.isEnabled) {
            return screenfull.isFullscreen;
        }
    }

    public async setFullscreen(element): Promise<void> {
        if (screenfull.isEnabled) {
            await screenfull.request(element);
            this.fullscreenChange(true);
        }
    }

    public async exitFullscreen(): Promise<void> {
        if (screenfull.isEnabled) {
            await screenfull.exit();
            // this.fullscreenChange(false); // handled by fullScreenHostListener below
        }
    }

    @HostListener("document:fullscreenchange", []) fullScreenHostListener() {
        if (! this.isFullscreen) {
            // exiting fullscreen mode
            this.fullscreenChange(false);
        }
    }

    /** called when fullscreen state changes */
    public fullscreenChange(isFullscreen: boolean) { }

    /**
     * 14 distinct colors @see https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors
     */
    public static get distinctColors(): string[] {
        return [
            "#003A80", // Irstea "marine", replacing the suggested "#4363d8" (blue)
            "#f58231", // orange
            "#3cb44b", // green
            "#e6194B", // red
            "#911eb4", // purple
            "#ffe119", // yellow
            "#f032e6", // magenta
            "#9A6324", // brown
            "#000075", // navy
            "#808000", // olive
            "#42d4f4", // cyan
            "#a9a9a9", // grey
            "#bfef45", // lime
            "#469990", // teal
        ];
    }

    /** the 10 different point styles available in Chart.js, ordered in a way that seems nice to me */
    public static get distinctPointStyles(): string[] {
        return [
            "circle",
            "rect",
            "triangle",
            "cross",
            "star",
            "rectRot",
            "crossRot",
            "dash",
            "rectRounded",
            "line"
        ];
    }

    public formattedLabel(p: NgParameter): string {
        return CalculatorResults.paramLabel(p, false);
    }

    /**
     * Formats (rounds) the given number (or the value of the given parameter) with the
     * number of decimals specified in app preferences; if given number is too low and
     * more decimals would be needed, keep as much significative digits as the number of
     * decimals specified in app preferences, except potential trailing zeroes
     *
     * ex. with 3 decimals:
     * 1             => 1.000
     * 65431         => 65431.000
     * 0.002         => 0.002
     * 0.00004521684 => 0.0000452
     * 0.000001      => 0.000001
     */
    protected formattedValue(p: NgParameter | number): string {
        let originalValue: number;
        if (p instanceof NgParameter) {
            originalValue = p.getValue();
        } else if (typeof p === "number") {
            originalValue = p;
        }
        const nDigits = ServiceFactory.applicationSetupService.displayPrecision;
        const minRenderableNumber = Number("1E-" + nDigits);
        // if required precision is too low, avoid rendering only zeroes
        if (originalValue < minRenderableNumber) {
            return String(Number(originalValue.toPrecision(nDigits))); // double casting avoids trailing zeroes
        } else {
            return fv(originalValue);
        }
    }
}
