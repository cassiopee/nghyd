import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormulaireDefinition } from 'app/formulaire/definition/form-definition';
import { FormulaireService } from 'app/services/formulaire.service';
import { I18nService } from 'app/services/internationalisation.service';
import { fv } from 'app/util/util';
import { formattedValue } from 'jalhyd';

/**
 * liste déroulante de boutons de génération de formulaire "section paramétrée"
 */

@Component({
  selector: 'select-section-details',
  templateUrl: './select-section-details.component.html',
  styleUrls: ['./select-section-details.component.scss']
})
export class SelectSectionDetailsComponent {
  private _points: any[];

  /**
   * points auxquels on peut créer un formulaire "section paramétrée"
   */
  @Input()
  public set points(ps: any[]) {
    this._points = ps;
  }

  public get points(): any[] {
    return this._points;
  }

  constructor(
    private formulaireService: FormulaireService,
    private router: Router,
    private intlService: I18nService
  ) { }

  public pointLabel(p: any): string {
    const abs = this.intlService.localizeText("INFO_REMOUSRESULTS_ABSCISSE");
    const tirant = this.intlService.localizeText("INFO_REMOUSRESULTS_TIRANT");
    return abs + " : " + formattedValue(p.x, 1) + " - " + tirant + " : " + fv(p.y);
  }

  /**
   * Génère une SectionParametree à partir du module en cours
   */
  public async generateCrSp(p: any) {
    const f: FormulaireDefinition = await this.formulaireService.generateParametricSectionForm(p.y);

    // calculate form
    f.doCompute();

    // go to new form
    this.router.navigate(["/calculator", f.uid]);
  }

  public get uitextPlaceholder() {
    return this.intlService.localizeText("INFO_REMOUSRESULTS_PARAM_SECTION_PLACEHOLDER");
  }
}
