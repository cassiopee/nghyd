import { Component, Input, OnChanges } from "@angular/core";

import { I18nService } from "../../services/internationalisation.service";
import { fv, longestVarParam } from "../../util/util";
import { MultiDimensionResults } from "../../results/multidimension-results";
import { VariatedDetails } from "jalhyd";
import { CalculatorResults } from "../../results/calculator-results";
import { LogHelper } from "app/results/log-helper";

@Component({
    selector: "variable-results-selector",
    templateUrl: "./variable-results-selector.component.html",
    styleUrls: [
        "./variable-results-selector.component.scss"
    ]
})
export class VariableResultsSelectorComponent implements OnChanges {

    /** résultats non mis en forme */
    @Input()
    private results: MultiDimensionResults;

    /** détails des paramètres qui varient dans le Nub associé au formulaire */
    @Input()
    private variatedParameters: VariatedDetails[];

    /** stats du log synthétique */
    private _logHelper: LogHelper;

    /** valeur en cours */
    private _selectedValue = 0;

    /** size of the longest variable value */
    private size = 0;

    /** inferred extended values list for each variating parameter */
    private varValues = [];

    /* @Output()
    protected indexChange = new EventEmitter(); */

    constructor(
        protected intlService: I18nService,
    ) { }

    public ngOnChanges() {
        // rebuild variable parameters values everytime somthing changes
        if (this.variatedParameters) {
            this.varValues = [];
            // find longest list
            const lvp = longestVarParam(this.variatedParameters);
            this.size = lvp.size;
            // get extended values lists for each variable parameter
            for (const v of this.variatedParameters) {
                const vv = [];
                const iter = v.param.getExtendedValuesIterator(this.size);
                while (iter.hasNext) {
                    const nv = iter.next();
                    vv.push(fv(nv.value));
                }
                this.varValues.push(vv);
            }
        }
        // get current variatedIndex even if component was rebuilt
        if (this.results) {
            this._selectedValue = this.results.variableIndex;
            this._logHelper = new LogHelper(this.results);
        }
    }

    public get hasVariableResults(): boolean {
        return (
            this.results
            && this.results.hasResults
            && this.results.variatedParameters.length > 0
        );
    }

    public get entries(): number[] {
        const ret: number[] = [];
        for (let i = 0; i < this.size; i++) {
            ret.push(i);
        }
        return ret;
    }

    /**
     * generate entry text for boundary conditions select
     * @param index select option index
     */
    protected entryLabel(index: number): string {
        let res = CalculatorResults.variatingModalityLabel(this.varValues, this.results, index);
        const restat = this._logHelper.resultElementsStats(index);
        if (restat.length > 0) {
            res += " - " + restat;
        }
        return res;
    }

    public get selectedValue(): number {
        return this._selectedValue;
    }

    public set selectedValue(v: number) {
        this.results.variableIndex = v;
        // this.indexChange.emit();
    }

    public get label() {
        return this.intlService.localizeText("INFO_PARAMFIELD_BOUNDARY_CONDITIONS");
    }
}
