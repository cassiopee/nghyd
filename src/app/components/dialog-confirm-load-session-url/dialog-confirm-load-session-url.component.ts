import { Component, Inject } from "@angular/core";
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from "@angular/material/legacy-dialog";

@Component({
    selector: "dialog-confirm-load-session-url",
    templateUrl: "dialog-confirm-load-session-url.component.html",
})
export class DialogConfirmLoadSessionURLComponent {

    public emptyCurrentSession: boolean = false;

    constructor(
        public dialogRef: MatDialogRef<DialogConfirmLoadSessionURLComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
    }

    public loadSession() {
        this.dialogRef.close({
            emptySession: this.emptyCurrentSession
        });
    }

    public get uitextTitle() {
        return "Please confirm loading";
    }

    public get uitextEmptyCurrentSession() {
        return "Empty current session";
    }

    public get uitextCancel() {
        return "Cancel";
    }

    public get uitextLoad() {
        return "Load";
    }
}
