import { Component, Input } from "@angular/core";

import { cLog, Message } from "jalhyd";

import { CalculatorResults } from "../../results/calculator-results";
import { I18nService } from "../../services/internationalisation.service";
import { VerificateurResults } from "../../results/verificateur-results";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";

@Component({
    selector: "verificateur-results",
    templateUrl: "./verificateur-results.component.html",
    styleUrls: [
        "./verificateur-results.component.scss"
    ]
})
export class VerificateurResultsComponent extends ResultsComponentDirective {

    /** résultats non mis en forme */
    private _verificateurResults: VerificateurResults;

    constructor(
        private i18nService: I18nService,
    ) {
        super();
    }

    @Input()
    public set results(rs: CalculatorResults[]) {
        this._verificateurResults = undefined;
        if (rs.length > 0 && rs[0] instanceof VerificateurResults) {
            this._verificateurResults = rs[0] as VerificateurResults;
        }
    }

    /*
     * Retourne les messages à afficher dans le composant de log global, au dessus
     * du sélecteur d'itération : messages globaux du Verificateur
     */
    public get globalLog(): cLog {
        const l = new cLog();
        if (this._verificateurResults && this._verificateurResults.result && this._verificateurResults.result.hasGlobalLog()) {
            l.addLog(this._verificateurResults.result.globalLog);
        }
        return l;
    }

    /**
     * Retourne les messages à afficher dans le composant de log "du bas" : logs de l'itération
     * en cours (messages non-globaux du Vérificateur et eds Espèce), que le résultat varie ou non
     */
    public get iterationLog(): Array<{ message: Message, subLog: cLog }> {
        const l: Array<{ message: Message, subLog: cLog }> = [];
        if (this._verificateurResults) {
            // = 0 lorsque rien ne varie
            const vi = this._verificateurResults.variableIndex;
            if (
                this._verificateurResults.result
                && this._verificateurResults.result.hasResultElements()
                && this._verificateurResults.result.resultElements[vi]
                && this._verificateurResults.result.resultElements[vi].hasLog()
            ) {
                // every log message of Verificateur is supposed to always be a status message for the nth Espece
                const verLog: cLog = this._verificateurResults.result.resultElements[vi].log;
                for (const vm of verLog.messages) {
                    l.push({
                        message: vm,
                        subLog: new cLog()
                    });
                }
            }
            // logs des Espece pour l'itération en cours; l.length devrait toujours être égal à especeResults.length,
            // sauf s'il y a une erreur dans la passe calculée
            if (l.length === this._verificateurResults.especeResults.length) {
                for (let i = 0; i < this._verificateurResults.especeResults.length; i++) {
                    const er = this._verificateurResults.especeResults[i];
                    if (
                        er
                        && er.hasResultElements()
                        && er.resultElements[vi]
                        && er.resultElements[vi].hasLog())
                    // tslint:disable-next-line:one-line
                    {
                        l[i].subLog.addLog(er.resultElements[vi].log);
                    }
                }
            } // else silent fail
        }
        return l;
    }

    public get verificateurResults() {
        return this._verificateurResults;
    }

    public get hasResults(): boolean {
        return this._verificateurResults && this._verificateurResults.hasResults;
    }

    public get uitextGeneralLogTitle(): string {
        return this.i18nService.localizeText("INFO_TITREJOURNAL_GLOBAL");
    }

}
