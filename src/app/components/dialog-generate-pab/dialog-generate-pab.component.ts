import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component } from "@angular/core";

import { ApplicationSetupService } from "../../services/app-setup.service";
import { I18nService } from "../../services/internationalisation.service";

import { sprintf } from "sprintf-js";
import { round } from "jalhyd";

@Component({
    selector: "dialog-generate-pab",
    templateUrl: "dialog-generate-pab.component.html",
    styleUrls: ["dialog-generate-pab.component.scss"]
})
export class DialogGeneratePABComponent {

    public debit = 1.5;

    public coteAmont = 102;

    public nbBassins = 6;

    constructor(
        public dialogRef: MatDialogRef<DialogGeneratePABComponent>,
        private intlService: I18nService,
        private appSetupService: ApplicationSetupService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        const nDigits = 3;  // nghyd#543
        this.coteAmont = data.coteAmont ? round(data.coteAmont, nDigits) : undefined;
        this.debit = data.debit ? round(data.debit, nDigits) : undefined;
        this.nbBassins = data.nbBassins;
    }

    public generatePAB() {
        // create PAB
        this.dialogRef.close({
            generate: true,
            debit: +this.debit,
            coteAmont: +this.coteAmont,
            nbBassins: +this.nbBassins
        });
    }

    public get uitextDebit() {
        return this.intlService.localizeText("INFO_DIALOG_PAB_Q");
    }

    public get uitextCoteAmont() {
        return this.intlService.localizeText("INFO_DIALOG_PAB_Z1");
    }

    public get uitextCoteAval() {
        return this.intlService.localizeText("INFO_DIALOG_PAB_Z2");
    }

    public get uitextNBBassins() {
        return this.intlService.localizeText("INFO_DIALOG_PAB_NB");
    }

    public get uitextMustBeANumber() {
        return this.intlService.localizeText("ERROR_PARAM_MUST_BE_A_NUMBER");
    }

    public get uitextMustBePositive() {
        return this.intlService.localizeText("ERROR_PARAM_MUST_BE_POSITIVE");
    }

    public get uitextMustBeAtLeastTwo() {
        return sprintf(this.intlService.localizeText("ERROR_PARAM_MUST_BE_AT_LEAST"), 2);
    }

    public get uitextGeneratePAB() {
        return this.intlService.localizeText("INFO_CALCULATOR_RESULTS_GENERATE_PAB");
    }

    public get uitextGenerate() {
        return this.intlService.localizeText("INFO_OPTION_GENERATE");
    }

    public get uitextCancel() {
        return this.intlService.localizeText("INFO_OPTION_CANCEL");
    }
}
