import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component } from "@angular/core";
import { I18nService } from "../../services/internationalisation.service";

@Component({
    selector: "dialog-confirm",
    templateUrl: "dialog-confirm.component.html",
})
export class DialogConfirmComponent {

    private _title: string;
    private _text: string;

    constructor(
        public dialogRef: MatDialogRef<DialogConfirmComponent>,
        private intlService: I18nService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this._title = data.title;
        this._text = data.text;
    }

    public get uitextYes() {
        return this.intlService.localizeText("INFO_OPTION_YES");
    }

    public get uitextNo() {
        return this.intlService.localizeText("INFO_OPTION_NO");
    }

    public get uitextTitle() {
        return this._title;
    }

    public get uitextBody() {
        return this._text;
    }
}
