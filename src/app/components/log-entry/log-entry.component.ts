import { Component, Input, OnChanges } from "@angular/core";

import { Message, MessageSeverity, Result, Nub } from "jalhyd";

import { I18nService } from "../../services/internationalisation.service";
import { ApplicationSetupService } from "../../services/app-setup.service";

@Component({
    selector: "log-entry",
    templateUrl: "./log-entry.component.html",
    styleUrls: [
        "./log-entry.component.scss"
    ]

})
export class LogEntryComponent implements OnChanges {
    @Input()
    private _message: Message;

    /**
     * message mis en forme
     */
    private _text: string;

    constructor(
        private intlService: I18nService,
        private appSetupService: ApplicationSetupService
    ) { }

    /**
     * appelé quand les @Input changent
     */
    ngOnChanges() {
        this.updateText();
    }

    private updateText() {
        const nDigits = this.appSetupService.displayPrecision;
        this._text = this.intlService.localizeMessage(this._message, nDigits);
    }

    public get text(): string {
        return this._text;
    }

    public get levelInfo(): boolean {
        return this._message.getSeverity() === MessageSeverity.INFO;
    }

    public get levelWarning(): boolean {
        return this._message.getSeverity() === MessageSeverity.WARNING;
    }

    public get levelError(): boolean {
        return this._message.getSeverity() === MessageSeverity.ERROR;
    }
}
