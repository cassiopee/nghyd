import { Component, ViewChild, Input, OnChanges, AfterViewInit, ElementRef } from "@angular/core";
import { I18nService } from "app/services/internationalisation.service";

import {
    acSection, cSnTrapez, ParamsSectionTrapez, cSnRectang, ParamsSectionRectang, cSnCirc,
    ParamsSectionCirc, cSnPuiss, ParamsSectionPuiss, Result
} from "jalhyd";

import { ResultsComponentDirective } from "../fixedvar-results/results.component";

@Component({
    selector: "section-canvas",
    templateUrl: "./section-canvas.component.html",
    styleUrls: [
        "./section-canvas.component.scss"
    ]
})
export class SectionCanvasComponent extends ResultsComponentDirective implements AfterViewInit, OnChanges {

    private static labelColors: { [key: string]: any; } = {
        "Hs": { r: 255, g: 0, b: 0 },
        "Hsc": { r: 0, g: 0, b: 255 },
        "Yn": { r: 16, g: 128, b: 16 },
        "Ycor": { r: 128, g: 128, b: 128 },
        "Yc": { r: 255, g: 128, b: 0 },
        "Ycon": { r: 255, g: 0, b: 255 },
        "Y": { r: 50, g: 50, b: 50 }
    };

    /*
     * notes :
     * - l'origine des coordonnées en m est par ex pour la section rectangulaire le coin en bas (radier) à gauche de la section
     * - canal : partie physique, sans les niveaux d'eau
     * - section : canal + niveaux d'eau
     * - rendu : section + texte + pointillés en haut
     * - W : dimension horizontale, H : dimension verticale
     * _ _m : valeur en mètres, _pix : valeur en pixels
     */

    /** largeur gauche/droite du texte (pixels) */
    private readonly _textWidth = 110;

    /** marge basse (pixels) */
    private readonly _bottomMargin = 5;

    /** marge haute pour les pointillés (pixels) */
    private readonly _topDashMargin = 30;

    /** largeur des repères des niveaux sur le bord de la section (pixels) */
    private readonly _levelMark = 5;

    /** facteurs d'échelle (coordonnées en m <-> coordonnées en pixels) */
    private _scaleX: number;
    private _scaleY: number;

    /** origine (pixels) */
    private _origX: number;
    private _origY: number;

    private _section: acSection;

    private _result: Result;

    /**
     * paramètre de taille du canvas en entrée (pixels)
     */
    private _size: number;

    /**
     * taille horizontale de la section (m)
     */
    private _Wsect_m: number;

    /**
     * taille verticale de la section (m)
     */
    private _Hsect_m: number;

    /**
     * taille horizontale de la section (pixels)
     */
    private _Wsect_pix: number;

    /**
     * taille verticale de la section (pixels)
     */
    private _Hsect_pix: number;

    // tirants
    private _levels: Object[] = [];

    constructor(private intlService: I18nService) {
        super();
    }

    public get width(): number {
        return this._size;
    }

    public get height(): number {
        return this._size;
    }

    private _context2d: CanvasRenderingContext2D;

    @ViewChild("canvas", { static: true })
    private _calcCanvas: ElementRef;

    @Input()
    public set section(s: acSection) {
        this._section = s;
    }

    @Input()
    public set result(r: Result) {
        this._result = r;
    }

    @Input()
    public set size(s: number) {
        this._size = s;
    }

    // respect du rapport abscisses/ordonnées
    private _useRealAspectRatio: boolean = false;

    // redessine le canvas chaque fois qu'une entrée change
    public ngOnChanges() {
        this.computeSectionWidth();
        this.computeSectionHeight();
        this.computeTransform();
        setTimeout(() => {
            this.draw();
        }, 10);
    }

    public ngAfterViewInit() { // wait for the view to init before using the element
        this._context2d = this._calcCanvas.nativeElement.getContext("2d");
    }

    /**
     * calcul des tailles horizontale et verticale de la zone de rendu (pixels)
     * @returns true si le rendu touche le haut et le bas du canvas
     */
    private computeRenderSize(): boolean {
        let res: boolean;
        if (this._useRealAspectRatio) {
            // largeur en pixels pour la représentation de la section
            const a = this.width - 2 * (this._textWidth + this._levelMark);

            // calcul des rapports largeur/hauteur pour le canvas et la section
            const rlim = a / this.height;
            const rsect = this._Wsect_m / this._Hsect_m;

            if (rsect <= rlim) {
                this._Hsect_pix = this.height - this._bottomMargin - this._topDashMargin;
                this._Wsect_pix = this._Hsect_pix * rsect;
                res = true;
            }
            else {
                this._Wsect_pix = a;
                this._Hsect_pix = this._Wsect_pix / rsect;
                res = false;
            }

        } else {
            this._Wsect_pix = this.width - 2 * (this._textWidth + this._levelMark)
            this._Hsect_pix = this.height - this._bottomMargin - this._topDashMargin;
            res = true;
        }

        return res;
    }

    /**
     * calcul du coef pour passer de m -> pixels
     * et de l'origine du rendu
     */
    private computeTransform() {
        const b = this.computeRenderSize();

        // échelle m -> pix
        this._scaleX = this._Wsect_pix / this._Wsect_m;
        this._scaleY = this._Hsect_pix / this._Hsect_m;
        // origine
        this._origX = (this._size - this._Wsect_pix) / 2;
        this._origY = b ? this._bottomMargin : (this._size - this._Hsect_pix) / 2;
    }

    /**
     * convertit une abscisse en m en pixels
     */
    private Xm2pix(x: number) {
        return this._origX + x * this._scaleX;
    }

    /**
     * convertit une ordonnée en m en pixels
     */
    private Ym2pix(y: number) {
        return this._size - this._origY - y * this._scaleY;
    }

    public get useRealAspectRatio(): boolean {
        return this._useRealAspectRatio;
    }

    public setUseRealAspectRatio(b: boolean) {
        this._useRealAspectRatio = b;
        this.computeTransform();
        this.draw();
    }

    private setLevels() {
        this._levels = [];

        // traduction des symboles des variables calculées
        const re = this._result.resultElement;
        if (re !== undefined) {
            for (const k in re.values) {
                if (k !== re.vCalcSymbol) {
                    //const lbl = k.toUpperCase();
                    const er = re.getValue(k);
                    // this._resultElement.addExtraResult(lbl, er);

                    if (this.isSectionLevel(k)) {
                        this.addLevel(er, k + " = " + this.formattedValue(er), SectionCanvasComponent.labelColors[k]);
                    }
                }
            }
        }

        // ajout du tirant d'eau saisi
        const valY = this._result.sourceNub.getParameter("Y").singleValue;
        this.addLevel(valY, "Y = " + this.formattedValue(valY), SectionCanvasComponent.labelColors["Y"]);

        this.sortLevels();
    }

    private addLevel(val: number, label: string, rgb: {}) {
        this._levels.push({ val, label, rgb });
    }

    /**
     * trie les tirants par niveau d'eau croissant
     */
    private sortLevels() {
        this._levels.sort((a, b) => {
            if (a["val"] < b["val"]) {
                return -1;
            }
            if (a["val"] > b["val"]) {
                return 1;
            }
            return 0;
        });
    }

    // max des niveaux à représenter
    private getMaxLevel(): number {
        // max de la cote de berge et des niveaux (qui sont triés)
        return Math.max(this._section.prms.YB.v, this._levels[this._levels.length - 1]["val"]);
    }

    private isSectionLevel(s: string) {
        for (const k in SectionCanvasComponent.labelColors) {
            if (k === s) {
                return true;
            }
        }
        return false;
    }

    /**
     * calcul des tailles horizontale et verticale de la section (m)
     */
    private computeSectionWidth() {
        if (this._section instanceof cSnTrapez) {
            this.computeSectionWidthTrapez();
        }
        else if (this._section instanceof cSnRectang) {
            this.computeSectionWidthRect();
        }
        else if (this._section instanceof cSnCirc) {
            this.computeSectionWidthCirc();
        }
        else if (this._section instanceof cSnPuiss) {
            this.computeSectionWidthPara();
        }
        else
            throw new Error("SectionCanvasComponent.computeSectionWidth() : type de section non pris en charge");
    }

    public draw() {
        if (this._context2d && this._section) {
            this.clear();
            setTimeout(() => { // à cause du changement de taille du canvas dans updateCanvasSize()
                this.drawFrame();
                this.drawSection();
                this.drawLevels();
            }, 10);
        }
    }

    /**
     * dessin des pointillés au dessus de la berge
     * xmin,xmax : position gauche/droite
     * yb : cote de berge
     * maxHeight : valeur maxi des cotes
     */
    private drawTopDashLines(xmin: number, xmax: number, yb: number) {
        this.setLineWidth(2);
        this.setLineDash([5]);
        this.setStrokeColor(128, 128, 128);
        const x1 = this.Xm2pix(xmin);
        const ybp = this.Ym2pix(yb);
        this.drawLine(x1, ybp, x1, 0);
        const x2 = this.Xm2pix(xmax);
        this.drawLine(x2, ybp, x2, 0);
    }

    private computeSectionHeight() {
        this.setLevels();

        // hauteur totale de la section
        this._Hsect_m = this.getMaxLevel();
    }

    private computeSectionWidthTrapez() {
        const sect: cSnTrapez = <cSnTrapez>this._section;
        const prms: ParamsSectionTrapez = <ParamsSectionTrapez>sect.prms;

        // largeur de la partie pentue
        const lp: number = prms.Fruit.v * prms.YB.v;

        // largeur totale de la section
        this._Wsect_m = lp * 2 + prms.LargeurFond.v;
    }

    private drawSectionTrapez() {
        const sect: cSnTrapez = <cSnTrapez>this._section;
        const prms: ParamsSectionTrapez = <ParamsSectionTrapez>sect.prms;

        // cote de berge
        const yb: number = prms.YB.v;

        // largeur de la partie pentue
        const lp: number = prms.Fruit.v * prms.YB.v;

        // dessin de la section

        this.setStrokeColor(0, 0, 0);
        this.setLineWidth(5);
        this.drawSectionLine(0, yb, lp, 0);
        this.drawSectionLine(lp, 0, lp + prms.LargeurFond.v, 0);
        this.drawSectionLine(lp + prms.LargeurFond.v, 0, this._Wsect_m, yb);

        // pointillés du haut

        this.drawTopDashLines(0, this._Wsect_m, yb);
    }

    private computeSectionWidthRect() {
        const sect: cSnRectang = <cSnRectang>this._section;
        const prms: ParamsSectionRectang = <ParamsSectionRectang>sect.prms;

        // largeur totale de la section
        this._Wsect_m = prms.LargeurBerge.v;
    }

    private drawSectionRect() {
        const sect: cSnRectang = <cSnRectang>this._section;
        const prms: ParamsSectionRectang = <ParamsSectionRectang>sect.prms;

        // cote de berge
        const yb: number = prms.YB.v;


        // dessin de la section

        this.setStrokeColor(0, 0, 0);
        this.setLineWidth(5);
        this.drawSectionLine(0, yb, 0, 0);
        this.drawSectionLine(0, 0, this._Wsect_m, 0);
        this.drawSectionLine(this._Wsect_m, 0, this._Wsect_m, yb);

        // pointillés du haut

        this.drawTopDashLines(0, this._Wsect_m, yb);
    }

    private computeSectionWidthCirc() {
        const sect: cSnCirc = <cSnCirc>this._section;
        const prms: ParamsSectionCirc = <ParamsSectionCirc>sect.prms;

        // cote de berge
        const yb: number = prms.YB.v;

        // diamètre, rayon
        const D: number = prms.D.v;
        const r: number = D / 2;

        // largeur au miroir
        const B: Result = sect.CalcSection("B", yb);
        if (!B.ok) {
            throw B;
        }

        // largeur totale de la section
        this._Wsect_m = yb < r ? B.vCalc : D;
    }

    private drawSectionCirc() {
        const sect: cSnCirc = <cSnCirc>this._section;
        const prms: ParamsSectionCirc = <ParamsSectionCirc>sect.prms;

        // cote de berge
        const yb: number = prms.YB.v;

        // diamètre, rayon
        const D: number = prms.D.v;
        const r: number = D / 2;

        try {
            // largeur au miroir
            const B: Result = sect.CalcSection("B", yb);
            if (!B.ok) {
                throw B;
            }

            // dessin de la section

            this.setStrokeColor(0, 0, 0);
            this.setLineWidth(5);

            const wx: number = this._Wsect_m / 2;
            const alpha: Result = sect.CalcSection("Alpha", yb);
            if (!alpha.ok) {
                throw alpha;
            }

            const s: number = Math.PI / 2 - alpha.vCalc;
            const e: number = Math.PI / 2 + alpha.vCalc;
            this.drawSectionEllipse(wx, r, r, r, 0, s, e);

            // pointillés du haut

            const w: number = yb > r ? (D - B.vCalc) / 2 : 0;
            this.drawTopDashLines(w, this._Wsect_m - w, yb);
        } catch (e) {
            const res: Result = e as Result;
            this.drawText("error : " + res.log.toString(), 0, 0);
        }
    }

    private computeSectionWidthPara() {
        const sect: cSnPuiss = <cSnPuiss>this._section;
        const prms: ParamsSectionPuiss = <ParamsSectionPuiss>sect.prms;

        // largeur au miroir
        const B: number = prms.LargeurBerge.v;

        // largeur totale de la section
        this._Wsect_m = B;
    }

    private drawSectionPara() {
        const sect: cSnPuiss = <cSnPuiss>this._section;
        const prms: ParamsSectionPuiss = <ParamsSectionPuiss>sect.prms;

        // cote de berge
        const yb: number = prms.YB.v;

        // largeur au miroir
        const B: number = prms.LargeurBerge.v;

        // contour de la section

        this.setStrokeColor(0, 0, 0);
        this.setLineWidth(5);
        const k: number = prms.k.v;
        const lambda: number = B / Math.pow(yb, k);
        const inv_k: number = 1 / k;

        const n = 20;
        this._context2d.beginPath();
        for (let x: number = -B / 2; x <= B / 2; x += B / n) {
            const y: number = Math.pow(Math.abs(x) * 2 / lambda, inv_k);
            this._context2d.lineTo(this.Xm2pix(x + B / 2), this.Ym2pix(y));
        }
        this._context2d.stroke();

        // pointillés du haut
        this.drawTopDashLines(0, this._Wsect_m, yb);
    }

    private drawSectionEllipse(x: number, y: number, rX: number, rY: number, rot: number, start: number, end: number) {
        this.drawEllipse(this.Xm2pix(x), this.Ym2pix(y), rX * this._scaleX, rY * this._scaleY, rot, start, end);
    }

    /**
     * dessine un texte
     * @param s texte à dessiner
     * @param x position horizontale (m)
     * @param y position verticale (m)
     * @param align alignement "left" ou "right" cf. https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/textAlign
     */
    private drawText(s: string, x: number, y: number, align?: string) {
        // décalage du texte par rapport au bord gauche/droit de la section
        const shiftX = align === undefined ? 0 : (align === "left" ? this._levelMark : -this._levelMark);
        this.fillText(s, this.Xm2pix(x) + shiftX, this.Ym2pix(y), align);
    }

    private drawSectionLine(x1: number, y1: number, x2: number, y2: number) {
        this.drawLine(this.Xm2pix(x1), this.Ym2pix(y1), this.Xm2pix(x2), this.Ym2pix(y2));
    }

    /**
     * dessin de la section
     * @returns largeur de la section (en m)
     */
    private drawSection() {
        if (this._section instanceof cSnTrapez) {
            this.drawSectionTrapez();
        }
        else if (this._section instanceof cSnRectang) {
            this.drawSectionRect();
        }
        else if (this._section instanceof cSnCirc) {
            this.drawSectionCirc();
        }
        else if (this._section instanceof cSnPuiss) {
            this.drawSectionPara();
        }
        else
            throw new Error("SectionCanvasComponent.drawSection() : type de section non pris en charge");
    }

    /**
     * dessin des niveaux en gérant le chevauchement
     * @param levels liste des niveaux à tracer
     * @param textHeight hauteur minimal entre le texte des niveaux (m)
     * @param left true pour tracer le texte à gauche du niveau, false à droite
     */
    private drawLevelsWithoutOverlap(levels: any, textHeight: number, left: boolean) {
        for (let i = levels.length - 1; i >= 0; i--) {
            const l = levels[i];

            // chevauchement avec le précédent ?
            if (i < levels.length - 1) {
                let yprec = levels[i + 1].y;

                const ycurr = l.y;
                if (yprec - ycurr < textHeight) {
                    l.y = yprec - textHeight;
                }
            }

            // tracé du tirant courant
            const col = l["rgb"];
            this.setStrokeColor(col["r"], col["g"], col["b"]);
            this.drawSectionLine(0, l.val, this._Wsect_m, l.val);
            this.setFillColor(col["r"], col["g"], col["b"]);

            if (left) {
                this.drawText(l["label"], 0, l.y, "right");
            } else {
                this.drawText(l["label"], this._Wsect_m, l.y, "left");
            }
        }
    }

    private drawLevels() {
        this.resetLineDash();
        this.setLineWidth(1);
        this.setFont("12px sans-serif");

        // hauteur des caractères
        const tm: TextMetrics = this._context2d.measureText("Ag");
        const charHeightPix = tm.actualBoundingBoxAscent + tm.actualBoundingBoxDescent;
        const charHeightMeter = charHeightPix / this._scaleY;

        // sépare les niveaux de gauche/droite
        const leftLevels = [];
        const rightLevels = [];
        let left = true;
        for (const l of this._levels) {
            const y = l["val"];
            Object.assign(l, { "y": y }); // y = ordonnée de tracé
            if (left) {
                leftLevels.push(l);
            } else {
                rightLevels.push(l);
            }
            left = !left;
        }

        // dessin des textes
        this.drawLevelsWithoutOverlap(leftLevels, charHeightMeter, true);
        this.drawLevelsWithoutOverlap(rightLevels, charHeightMeter, false);
    }

    // contour du canvas
    private drawFrame() {
        this.resetLineDash();
        this.setStrokeColor(128, 128, 128);
        this.drawRect(0, 0, this._size, this._size);
    }

    public clear() {
        if (this._context2d) {
            this._context2d.clearRect(0, 0, this._size, this._size);
        }
    }

    public setStrokeColor(r: number, g: number, b: number) {
        const col: string = "rgb(" + r + "," + g + "," + b + ")";
        this._context2d.strokeStyle = col;
    }

    public setFillColor(r: number, g: number, b: number) {
        const col: string = "rgb(" + r + "," + g + "," + b + ")";
        this._context2d.fillStyle = col;
    }

    private setFont(f: string) {
        this._context2d.font = f;
    }

    /**
     * dessine un texte sur le canvas
     * @param s texte à dessiner
     * @param x position horizontale (pixels)
     * @param y position verticale (pixels)
     * @param align alignement "left" ou "right" cf. https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/textAlign
     */
    public fillText(s: string, x: number, y: number, align?: any) {
        if (align) {
            this._context2d.textAlign = align;
        }
        this._context2d.fillText(s, x, y);
    }

    public setLineWidth(w: number) {
        this._context2d.lineWidth = w;
    }

    public setLineDash(d: number[]) {
        this._context2d.setLineDash(d);
    }

    public resetLineDash() {
        this._context2d.setLineDash([]);
    }

    public drawRect(x1: number, y1: number, w: number, h: number) {
        this._context2d.strokeRect(x1, y1, w, h);
    }

    public drawLine(x1: number, y1: number, x2: number, y2: number) {
        this._context2d.beginPath();
        this._context2d.moveTo(x1, y1);
        this._context2d.lineTo(x2, y2);
        this._context2d.stroke();
    }

    /**
     *
     * @param x The x axis of the coordinate for the ellipse's center.
     * @param y The y axis of the coordinate for the ellipse's center.
     * @param radiusX The ellipse's major-axis radius.
     * @param radiusY The ellipse's minor-axis radius.
     * @param rotation The rotation for this ellipse, expressed in radians
     * @param startAngle The starting point, measured from the x axis, from which it will be drawn, expressed in radians
     * @param endAngle The end ellipse's angle to which it will be drawn, expressed in radians
     */
    public drawEllipse(x: number, y: number, radiusX: number, radiusY: number, rotation: number, startAngle: number, endAngle: number) {
        this._context2d.beginPath();
        this._context2d.ellipse(x, y, radiusX, radiusY, rotation, startAngle, endAngle);
        this._context2d.stroke();
    }

    public get uitextUseRealRatio(): string {
        return this.intlService.localizeText("INFO_SECTIONPARAMETREE_REAL_RATIO");
    }
}
