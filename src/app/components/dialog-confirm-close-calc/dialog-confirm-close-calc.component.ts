import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component } from "@angular/core";
import { FormulaireService } from "../../services/formulaire.service";
import { I18nService } from "../../services/internationalisation.service";
import { Session } from "jalhyd";

@Component({
    selector: "dialog-confirm-close-calc",
    templateUrl: "dialog-confirm-close-calc.component.html",
    styleUrls: [
        "dialog-confirm-close-calc.component.scss"
    ]
})
export class DialogConfirmCloseCalcComponent {

    private _dependingNubs: any[] = [];

    constructor(
        public dialogRef: MatDialogRef<DialogConfirmCloseCalcComponent>,
        private intlService: I18nService,
        private formService: FormulaireService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this._dependingNubs = Session.getInstance().getDependingNubs(data.uid, undefined, true, true).map((n) => {
            return this.formService.getFormulaireFromNubId(n.uid).calculatorName;
        });
    }

    public get uitextYes() {
        return this.intlService.localizeText("INFO_OPTION_CLOSE");
    }

    public get uitextNo() {
        return this.intlService.localizeText("INFO_OPTION_CANCEL");
    }

    public get uitextCloseCalcTitle() {
        return this.intlService.localizeText("INFO_CLOSE_DIALOGUE_TITRE");
    }

    public get uitextCloseCalcBody() {
        return this.intlService.localizeText("INFO_CLOSE_DIALOGUE_TEXT");
    }

    public get uitextDependingModules() {
        return this.intlService.localizeText("INFO_CLOSE_DIALOGUE_DEPENDING_MODULES");
    }

    public get dependingNubs() {
        return this._dependingNubs;
    }

}
