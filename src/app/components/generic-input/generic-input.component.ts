import { Input, Output, EventEmitter, ChangeDetectorRef, OnChanges, ViewChild, Directive } from "@angular/core";
import { NgModel } from "@angular/forms";
import { isNumeric } from "jalhyd";
import { FormulaireDefinition } from "../../formulaire/definition/form-definition";
import { NgParameter } from "../../formulaire/elements/ngparam";
import { I18nService } from "../../services/internationalisation.service";
import { ApplicationSetupService } from "../../services/app-setup.service";
import { DefinedBoolean } from "../../util/definedvalue/definedboolean";

/**
 * classe de gestion générique d'un champ de saisie avec titre, validation et message d'erreur
 * définitions :
 * - modèle : entité mémoire gérée, indépendamment de la façon dont elle est affichée.
 *   A noter que si cette entité est une classe, on peut ne présenter à l'interface qu'un membre de cette classe,
 *   cad que get model() et getModelValue() ne renverront pas la même chose.
 *   Par ex : get model()-> instance_de_la_classe_Toto, getModelValue() -> Toto.unMembreNumerique
 * - valeur gérée : entité elle même si c'est un type simple (number, string, ...) ou une partie d'un classe
 * - UI : interface utilisateur, présentation de la valeur gérée
 */
@Directive()
export abstract class GenericInputComponentDirective implements OnChanges {

    /** entité mémoire gérée */
    protected _model: NgParameter | FormulaireDefinition; // CalcName utilise un FormDefinition ici !?

    /** flag d'affichage du message d'erreur */
    public showError = true;

    /** id de l'input, utilisé notamment pour les tests */
    public get inputId() {
        let id = "error-in-inputId";
        if (this._model) {
            // unique input id based on parameter symbol
            if (this._model instanceof NgParameter) {
                const param = this._model as NgParameter;
                id = param.symbol;
                // if inside a child Nub, prefix with child position to disambiguate
                const nub = param.paramDefinition.parentNub;
                if (nub && nub.parent && nub.intlType) {
                    id = nub.findPositionInParent() + "_" + id;
                }
            }
        }
        return id;
    }

    /**
     * chaîne affichée dans l'input quand aucune valeur n'est saisie
     */
    @Input()
    public title: string;

    /**
     * événement signalant un changement : valeur du modèle, validité, ...
     */
    @Output()
    protected change = new EventEmitter<any>();

    /**
     * événement signalant un appui sur TAB ou SHIFT+TAB
     */
    @Output()
    protected tabPressed = new EventEmitter<any>();

    /**
     * valeur saisie.
     * Cette variable n'est modifiée que lorsqu'on affecte le modèle ou que l'utilisateur fait une saisie
     */
    private _uiValue: string;

    /**
     * flag de validité de la saisie dans l'UI
     * par ex : est ce bien une valeur numérique ? n'y a-t-il que des minuscules ? etc...
     */
    private _isValidUI = false;

    /**
     * flag de validité de la valeur du modèle
     * par ex : la valeur saisie fait elle bien partie d'un domaine de définition donné ? date inférieure à une limite ? etc...
     */
    private _isValidModel = false;

    /**
     * flag de validité globale
     */
    private _isValid: DefinedBoolean;

    /**
     * message d'erreur UI
     */
    private _errorMessageUI: string;

    /**
     * message d'erreur modèle
     */
    private _errorMessageModel: string;

    @ViewChild("inputControl", { static: true })
    inputField: NgModel;

    constructor(
        private cdRef: ChangeDetectorRef,
        protected intlService: I18nService,
        protected appSetupService: ApplicationSetupService
    ) {
        this._isValid = new DefinedBoolean();
    }

    public get isDisabled(): boolean {
        if (this._model instanceof NgParameter) {
            return this._model.disabled;
        } else {
            return false;
        }
    }

    /**
     * modification et émission d'un événement de changement de la validité
     */
    private setAndEmitValid() {
        this._isValid.value = this._isValidUI && this._isValidModel;
        // if (this._isValid.changed) 
        // {
        this.change.emit({ "action": "valid", "value": this._isValid.value });
        // }
    }

    /**
     * détection des changements dans l'UI par le ChangeDetector du framework
     */
    protected detectChanges() {
        if (this.cdRef) {
            // if (!this.cdRef['destroyed']) // pour éviter l'erreur "Attempt to use a destroyed view: detectChanges"
            //     this.cdRef.detectChanges();
            this.cdRef.markForCheck();
        }
    }

    /**
     * calcul de la validité globale du composant (UI+modèle)
     */
    public get isValid() {
        return this._isValid.value;
    }

    protected setUIValid(b: boolean) {
        this._isValidUI = b;
        this.setAndEmitValid();
    }

    protected validateUI() {
        const { isValid, message } = this.validateUIValue(this._uiValue);
        this._errorMessageUI = message;
        this.detectChanges();
        this.setUIValid(isValid);
        return isValid;
    }

    protected setModelValid(b: boolean) {
        this._isValidModel = b;
        this.setAndEmitValid();

        // répercussion des erreurs sur le Form angular, pour faire apparaître/disparaître les mat-error
        // setTimeout(() => {  // en cas de pb, décommenter le timeout
        if (b) {
            this.inputField.control.setErrors(null);
        } else {
            this.inputField.control.setErrors({ "incorrect": true });
        }
        //     this.inputField.control.markAsTouched();
        // }, 100);
    }

    private validateModel(): boolean {
        let isValid: boolean;
        // specific case for "allowEmpty" params
        if (this.model instanceof NgParameter && this.uiValue === "" && this.model.allowEmpty) {
            isValid = true;
        } else {
            // general case
            const resp = this.validateModelValue(this.getModelValue());
            isValid = resp.isValid;
            this._errorMessageModel = resp.message;
        }
        this.detectChanges();
        this.setModelValid(isValid);

        return isValid;
    }

    public validate() {
        this.validateUI();
        this.validateModel();
    }

    /**
     * getter du message d'erreur affiché.
     * L'erreur de forme (UI) est prioritaire
     */
    public get errorMessage() {
        if (this._errorMessageUI !== undefined) {
            return this._errorMessageUI;
        }
        return this._errorMessageModel;
    }

    public get model(): any {
        return this._model;
    }

    public get isRequired(): boolean {
        return ! this.model.allowEmpty;
    }

    /**
     * événement de changement de la valeur du modèle
     */
    private emitModelChanged() {
        this.change.emit({
            action: "model",
            value: this.getModelValue(),
            symbol: (this._model instanceof NgParameter ? this._model.symbol : undefined)
        });
    }

    protected setAndValidateModel(sender: any, v: any): boolean {
        this.setModelValue(sender, v);
        this.emitModelChanged();

        return this.validateModel();
    }

    public set model(v: any) {
        this.beforeSetModel();
        this._model = v;
        this.afterSetModel();
        this.updateAll();
        this.detectChanges();
    }

    /**
     * MAJ et validation de l'UI
     */
    protected updateAndValidateUI() {
        if (this.getModelValue() !== undefined) {
            this._uiValue = String(this.getModelValue());
        } else {
            this._uiValue = "";
        }
        this.validateUI();
    }

    public get uiValue() {
        return this._uiValue;
    }

    /*
     * fonction appelée lorsque l'utilisateur fait une saisie
     * @param ui valeur dans le contrôle
     */
    public set uiValue(ui: any) {
        this._uiValue = ui;
        this.updateModelFromUI();
    }

    /**
     * met à jour le modèle d'après la saisie
     */
    public updateModelFromUI() {
        const valid = this.validateUI();
        if (valid || this._uiValue === "") {
            let val: number; // = undefined
            if (valid && this._uiValue !== "") {
                val = +this._uiValue; // cast UI value to Number
            }
            this.setAndValidateModel(this, val);
        }
    }

    /**
     * Renvoie l'événement au composant du dessus
     */
    public onTabPressed(event, shift: boolean) {
        this.tabPressed.emit({ originalEvent: event, shift: shift });
        return false; // stops event propagation
    }

    private updateAll() {
        this.updateAndValidateUI();
        this.validateModel();
    }

    /**
     * appelé quand les @Input changent
     */
    public ngOnChanges() {
        this.updateAll();
    }

    /**
     * appelé avant le changement de modèle
     */
    protected beforeSetModel() { }

    /**
     * appelé après le changement de modèle
     */
    protected afterSetModel() { }

    /**
     * retourne la valeur du modèle
     */
    protected abstract getModelValue(): any;

    /**
     * affecte la valeur du modèle
     */
    protected abstract setModelValue(sender: any, v: any);

    /**
     * valide une valeur de modèle : est ce une valeur acceptable ? (par ex, nombre dans un intervalle, valeur dans une liste, ...)
     * @param v valeur à valider
     * @returns isValid : true si la valeur est valide, false sinon
     * @returns message : message d'erreur
     */
    protected abstract validateModelValue(v: any): { isValid: boolean, message: string };

    /**
     * valide une valeur saisie dans l'UI (forme de la saisie : est ce bien une date, un nombre, ...)
     * @param ui saisie à valider
     * @returns isValid : true si la valeur est valide, false sinon
     * @returns message : message d'erreur
     */
    protected validateUIValue(ui: string): { isValid: boolean, message: string } {
        let valid = false;
        let msg: string;

        // specific case for "allowEmpty" params
        if (this.model instanceof NgParameter && ui === "" && this.model.allowEmpty) {
            valid = true;
        } else {
            // general case
            if (! isNumeric(ui)) {
                msg = this.intlService.localizeText("ERROR_PARAM_MUST_BE_A_NUMBER");
            } else {
                valid = true;
            }
        }

        return { isValid: valid, message: msg };
    }

    public openHelp($event: any) {
        window.open("assets/docs/" + this.appSetupService.language + "/calculators/" + this._model.helpLink, "_blank");
        $event.preventDefault();
        $event.stopPropagation();
        return false;
    }

    public get enableHelpButton() {
        return this._model && this._model.helpLink;
    }

    public get uitextOpenHelp() {
        return this.intlService.localizeText("INFO_CALCULATOR_OPEN_HELP");
    }
}
