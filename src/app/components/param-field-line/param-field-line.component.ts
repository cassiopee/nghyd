import { Component, ViewChild, Input, Output, EventEmitter, OnChanges } from "@angular/core";

import { I18nService } from "../../services/internationalisation.service";
import { NgParameter, ParamRadioConfig } from "../../formulaire/elements/ngparam";
import { NgParamInputComponent } from "../ngparam-input/ngparam-input.component";
import { ServiceFactory } from "../../services/service-factory";
import { ParamValueMode, ParallelStructure, ParamCalculability, Pab, Session } from "jalhyd";
import { FormulaireService } from "../../services/formulaire.service";
import { ParamLinkComponent } from "../param-link/param-link.component";
import { ParamValuesComponent } from "../param-values/param-values.component";

/**
 * Sélecteur de mode pour chaque paramètre: fixé, varier, calculer, lié
 */
@Component({
    selector: "param-field-line",
    templateUrl: "./param-field-line.component.html",
    styleUrls: [
        "./param-field-line.component.scss"
    ]
})
export class ParamFieldLineComponent implements OnChanges {

    constructor() {
        this.intlService = ServiceFactory.i18nService;
        this._formService = ServiceFactory.formulaireService;
        this.valid = new EventEmitter();
        this.inputChange = new EventEmitter();
        this.captureTabEvents = true;
    }

    public get uitextParamFixe() {
        return this.intlService.localizeText("INFO_PARAMFIELD_PARAMFIXE");
    }

    public get uitextParamVarier() {
        return this.intlService.localizeText("INFO_PARAMFIELD_PARAMVARIER");
    }

    public get uitextParamCalculer() {
        return this.intlService.localizeText("INFO_PARAMFIELD_PARAMCALCULER");
    }

    public get uitextParamLie() {
        return this.intlService.localizeText("INFO_PARAMFIELD_PARAMLIE");
    }

    /** user defined parameter placeholder (fixed mode) */
    private _fixedPlaceHolder: string;

    public get fixedPlaceHolder(): string {
        if (this._fixedPlaceHolder !== undefined) {
            return this._fixedPlaceHolder;
        }
        return this.param.title;
    }

    @Input()
    public set fixedPlaceHolder(ph: string) {
        this._fixedPlaceHolder = ph;
    }

    // états booléens des boutons

    public get isRadioFixChecked(): boolean {
        return this.param.radioState === ParamRadioConfig.FIX;
    }
    public get isRadioVarChecked(): boolean {
        return this.param.radioState === ParamRadioConfig.VAR;
    }
    public get isRadioCalChecked(): boolean {
        return this.param.radioState === ParamRadioConfig.CAL;
    }
    public get isRadioLinkChecked(): boolean {
        return this.param.radioState === ParamRadioConfig.LINK;
    }

    /**
     * validité des saisies du composant
     */
    public get isValid(): boolean {
        switch (this.param.radioState) {
            case ParamRadioConfig.FIX:
                return this._isInputValid;

            case ParamRadioConfig.VAR:
                return this._isRangeValid;

            case ParamRadioConfig.LINK:
                // at first this._paramLinkComponent is undefined until view is refreshed
                return this._paramLinkComponent ? this._paramLinkComponent.isValid : true;

            default:
                return true;
        }
    }

    public get formHasResults(): boolean {
        return ServiceFactory.formulaireService.currentFormHasResults;
    }

    @Input()
    public param: NgParameter;

    @Input()
    public captureTabEvents: boolean;

    @ViewChild(NgParamInputComponent, { static: true })
    private _ngParamInputComponent: NgParamInputComponent;

    @ViewChild(ParamValuesComponent)
    private _paramValuesComponent: ParamValuesComponent;

    @ViewChild(ParamLinkComponent)
    private _paramLinkComponent: ParamLinkComponent;

    @Output()
    private valid: EventEmitter<void>;

    @Output()
    private inputChange: EventEmitter<void>;

    /** événement signalant un appui sur TAB ou SHIFT+TAB */
    @Output()
    protected tabPressed = new EventEmitter<any>();

    /** true si la valeur saisie est valide */
    private _isInputValid = false;

    /** true si le min-max/liste est valide */
    private _isRangeValid = true;

    private intlService: I18nService;

    private _formService: FormulaireService;

    /**
     * sauvegarde de certaines propriétés du paramètre avant clic sur le radio "varier", utilisé pour
     * restaurer le paramètre en cas de cancel du dialogue de modification des valeurs en mode variable
     */
    private paramBackup: any;

    /*
     * gestion des événements clic sur les radios :
     * envoi d'un message au composant parent
     * cf. https://angular.io/guide/component-interaction#parent-listens-for-child-event
     */
    @Output()
    private radio = new EventEmitter<any>();

    /**
     * calcule la présence du radio "paramètre fixé"
     */
    public hasRadioFix(): boolean {
        switch (this.param.radioConfig) {
            case ParamRadioConfig.FIX:
                return this.hasRadioLink(); // gné ?

            default:
                return true;
        }
    }

    /**
     * calcule la présence du radio "paramètre à varier"
     */
    public hasRadioVar(): boolean {
        switch (this.param.radioConfig) {
            case ParamRadioConfig.VAR:
            case ParamRadioConfig.CAL:
                return true;

            default:
                return false;
        }
    }

    /**
    * calcule la présence du radio "paramètre à calculer"
    */
    public hasRadioCal(): boolean {
        switch (this.param.radioConfig) {
            case ParamRadioConfig.CAL:
                return true;

            default:
                return false;
        }
    }

    /**
    * calcule la présence du radio "paramètre lié" (importé d'un autre module de calcul)
    */
    public hasRadioLink(): boolean {
        if (this._formService.formulaires.length > 0 && !this.isParameterLinkTarget()) {
            // au moins 2 modules de calcul ouverts
            if (this._formService.formulaires.length > 1) {
                return this._formService.getLinkableValues(this.param).length > 0;
            }

            // ou un seul module de calcul "ouvrages parallèles" avec au moins 2 ouvrages
            if (this._formService.formulaires[0].currentNub instanceof ParallelStructure) {
                const ps: ParallelStructure = this._formService.formulaires[0].currentNub;
                if (ps.structures.length > 1) {
                    return this._formService.getLinkableValues(this.param).length > 0;
                }
            }

            // ou un seul module de calcul "PAB" avec au mois 2 ouvrages
            if (this._formService.formulaires[0].currentNub instanceof Pab) {
                const pab: Pab = this._formService.formulaires[0].currentNub;
                if (pab.children.length > 1) {
                    return this._formService.getLinkableValues(this.param).length > 0;
                }
            }

        }
        return false;
    }

    /**
     * Returns true if the current parameter is in CALC mode but no SINGLE
     * parameter is available to take its place
     */
    public canExitCalcMode() {
        let ret = true;
        if (this.param.paramDefinition.isCalculated) {
            const nub = this.param.paramDefinition.parentNub;
            try {
                const p = nub.findFirstCalculableParameter(this.param.paramDefinition);
                ret = (p !== undefined);
            }
            catch (e) {
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Returns true if setting this parameter to Calc mode leads to a links loop
     */
    public get isRadioCalDisabled(): boolean {
        // find all Nubs that have to be calculated for this one to run
        const nub = this.param.paramDefinition.originNub;
        const requiredNubs = nub.getRequiredNubsDeep();
        // if one of those Nubs depends on the parameter we're about to set
        // to Calc mode, then we shouldn't do so
        for (const r of requiredNubs) {
            if (r.dependsOnParameter(this.param.paramDefinition)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @returns true if parameter is a link target
     */
    private isParameterLinkTarget(): boolean {
        return Session.getInstance().isParameterLinkTarget(this.param.paramDefinition);
    }

    /**
     * compute link radio button "disabled" status
     */
    public get isRadioLinkDisabled(): boolean {
        return !this.canExitCalcMode() || this.isParameterLinkTarget();
    }

    public get radioCalTitle(): string {
        if (this.isRadioCalDisabled) {
            return this.intlService.localizeText("INFO_PARAMFIELD_CANNOT_CALC_LINKS_LOOP");
        }
        return "";
    }

    public onRadioClick(option: string) {
        const oldValueMode = this.param.valueMode;
        switch (option) {
            case "fix":
                this.param.valueMode = ParamValueMode.SINGLE;
                break;

            case "var":
                this.paramBackup = this.param.objectRepresentation();

                // prevent setting LISTE mode back to MINMAX if someone clicks "variable" again
                // after setting variable mode to LISTE
                if (oldValueMode !== ParamValueMode.MINMAX && oldValueMode !== ParamValueMode.LISTE) {
                    this.param.valueMode = ParamValueMode.MINMAX; // min/max par défaut
                }

                // let framework create ParamValuesComponent instance and open dialog
                setTimeout(() => {
                    if (this._paramValuesComponent) {
                        // re-open modal when clicking the "var" mode button again (PoLS)
                        this._paramValuesComponent.openDialog();
                    }
                }, 100);
                break;

            case "cal":
                this.param.setCalculated(); // sets mode to CALCUL and more
                break;

            case "link":
                this.param.valueMode = ParamValueMode.LINK;
                break;
        }
        this.radio.emit({
            "param": this.param,
            "oldValueMode": oldValueMode
        });
        // MAJ validité
        this.emitValidity();
    }

    /**
     * Renvoie l'événement au composant du dessus
     */
    public onTabPressed(event) {
        this.tabPressed.emit(event);
    }

    /**
     * émission d'un événement de validité
     */
    private emitValidity() {
        this.valid.emit();
    }

    /**
     * réception d'un événement de NgParamInputComponent
     */
    public onInputChange(event: any) {
        switch (event.action) {
            case "valid":
                this._isInputValid = event.value;
                this.emitValidity();
                break;

            case "model":
                this.inputChange.emit(event);
                break;

            case "cancelvar": // cancel button clicked in DialogEditParamValuesComponent
                this.param.loadObjectRepresentation(this.paramBackup);
                break;

            case "okvar":
                this.emitValidity();
                break;
        }
    }

    public ngOnChanges() {
        this._ngParamInputComponent.model = this.param;
        this._ngParamInputComponent.showError = this.isRadioFixChecked;
    }

    /**
     * relit la valeur dans l'interface et met à jour le NgParameter
     */
    public updateParameterFromUI() {
        this._ngParamInputComponent.updateModelFromUI();
    }

    /**
     * met à jour les paramètres liés
     */
    public updateLinkedParameter() {
        if (this._paramLinkComponent !== undefined) {
            this._paramLinkComponent.updateParamList();
        }
    }
}
