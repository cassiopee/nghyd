import { Component, OnInit, isDevMode } from "@angular/core";
import { Router } from "@angular/router";

import { I18nService } from "../../services/internationalisation.service";

import { Session } from "jalhyd";

import { KatexOptions } from "ngx-markdown/src/katex-options";

import { MatomoTracker } from "@ngx-matomo/tracker";

@Component({
    selector: "session-properties",
    templateUrl: "./session-properties.component.html",
    styleUrls: ["./session-properties.component.scss"]
})
export class SessionPropertiesComponent implements OnInit {

    public selectedTab = 0;

    public options: KatexOptions;

    constructor(
        private intlService: I18nService,
        private router: Router,
        private matomoTracker: MatomoTracker
    ) {
        this.options = {
            displayMode: false, // true centers equations
            throwOnError: false,
            errorColor: "#cc0000"
        };
        if (!isDevMode()) {
            this.matomoTracker.trackPageView("notes");
        }
    }

    public get content(): string {
        return Session.getInstance().documentation;
    }

    public set content(c: string) {
        Session.getInstance().documentation = c;
    }

    public get uitextTitle(): string {
        return this.intlService.localizeText("INFO_SESSION_PROPERTIES_TITLE");
    }

    public get uitextPreview(): string {
        return this.intlService.localizeText("INFO_SESSION_PROPERTIES_PREVIEW");
    }

    public get uitextEdit(): string {
        return this.intlService.localizeText("INFO_SESSION_PROPERTIES_EDIT");
    }

    public get uitextUsesMdFormat(): string {
        return this.intlService.localizeText("INFO_SESSION_PROPERTIES_USES_MD_FORMAT");
    }

    public get uitextHelp(): string {
        return this.intlService.localizeText("INFO_SESSION_PROPERTIES_HELP");
    }

    public ngOnInit() {
        // if app is started on this page but session is empty, redirect to home page
        if (!this.hasModules) {
            this.router.navigate(["/list"]);
        }
        // if notes content is empty, switch to editor
        if (this.content === undefined || this.content === "") {
            this.selectedTab = 1;
        }
    }

    public get hasModules(): boolean {
        return Session.getInstance().getNumberOfNubs() > 0;
    }

}
