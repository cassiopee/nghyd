import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component } from "@angular/core";

import { I18nService } from "../../services/internationalisation.service";

import { FishSpecies } from "jalhyd";

@Component({
    selector: "dialog-load-predefined-espece",
    templateUrl: "dialog-load-predefined-espece.component.html",
    styleUrls: ["dialog-load-predefined-espece.component.scss"]
})
export class DialogLoadPredefinedEspeceComponent {

    public selectedValue: number;

    constructor(
        public dialogRef: MatDialogRef<DialogLoadPredefinedEspeceComponent>,
        private intlService: I18nService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.selectedValue = 1;
    }

    public loadPredefinedEspece() {
        this.dialogRef.close({
            load: true,
            selected: this.selectedValue
        });
    }

    public get uitextLoadPredefinedEspece() {
        return this.intlService.localizeText("INFO_CALCULATOR_LOAD_PREDEFINED_ESPECE");
    }

    public get uitextLoad() {
        return this.intlService.localizeText("INFO_OPTION_LOAD");
    }

    public get uitextCancel() {
        return this.intlService.localizeText("INFO_OPTION_CANCEL");
    }

    public get entries(): number[] {
        const ret: number[] = [];
        for (let j = 1; j < Object.keys(FishSpecies).length / 2; j++) { // exclude "0" (SPECIES_CUSTOM)
            // const spgId = FishSpecies[j].substring(FishSpecies[j].lastIndexOf("_") + 1);
            ret.push(j);
        }
        return ret;
    }

    protected entryLabel(index: number): string {
        return this.intlService.localizeText("INFO_ENUM_" + FishSpecies[index]);
    }

    public get label() {
        return this.intlService.localizeText("INFO_ESPECE_TITRE_COURT");
    }

}
