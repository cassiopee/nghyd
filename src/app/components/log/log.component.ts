import { Component, Input } from "@angular/core";
import { LogHelper } from "app/results/log-helper";
import { ApplicationSetupService } from "app/services/app-setup.service";

import { cLog } from "jalhyd";

import { I18nService } from "../../services/internationalisation.service";

@Component({
    selector: "log",
    templateUrl: "./log.component.html",
    styleUrls: [
        "./log.component.scss"
    ]
})
export class LogComponent {
    /**
     * journal
     */
    private _log: cLog;

    // title to display above the log
    @Input()
    public logTitle: string;

    // expanded/folded panel state
    public expandedStatus: boolean;

    constructor(
        private intlService: I18nService,
        private appSetupService: ApplicationSetupService
    ) {
        this.expandedStatus = !this.appSetupService.foldedMessageLog;
    }

    public get uitextTitreJournal() {
        let res;
        if (this.logTitle) {
            res = this.logTitle;
        } else {
            res = this.intlService.localizeText("INFO_TITREJOURNAL");
        }

        const stats = cLog.messagesStats(this._log.messages);
        return res + " - " + LogHelper.messagesStatsToString(stats);;
    }

    public get hasEntries(): boolean {
        return this._log?.messages?.length > 0;
    }

    @Input()
    public set log(log: cLog) {
        this._log = log;
    }
}
