import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component, ViewChild, OnInit } from "@angular/core";
import { I18nService } from "../../services/internationalisation.service";
import { NgParameter } from "../../formulaire/elements/ngparam";
import { NgParamInputComponent } from "../ngparam-input/ngparam-input.component";

import { ParamDefinition } from "jalhyd";

@Component({
    selector: "dialog-edit-param-computed",
    templateUrl: "dialog-edit-param-computed.component.html",
    styleUrls: ["dialog-edit-param-computed.component.scss"]
})
export class DialogEditParamComputedComponent implements OnInit {

    /** the related parameter to change the "fixed" value of */
    public param: NgParameter;

    @ViewChild(NgParamInputComponent, { static: true })
    private _ngParamInputComponent: NgParamInputComponent;

    constructor(
        public dialogRef: MatDialogRef<DialogEditParamComputedComponent>,
        private intlService: I18nService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        // copy given parameter in a "fake" Ngparameter
        this.param = data.param;
    }

    public get uitextCancel(): string {
        return this.intlService.localizeText("INFO_OPTION_CANCEL");
    }

    public get uitextValidate(): string {
        return this.intlService.localizeText("INFO_OPTION_VALIDATE");
    }

    public get uitextEditParamComputedInitialValue() {
        return this.intlService.localizeText("INFO_DIALOG_COMPUTED_VALUE_TITLE");
    }

    public ngOnInit() {
        this._ngParamInputComponent.editInitValue = true;
        this._ngParamInputComponent.model = this.param;
    }

    public onCancel() {
        this.dialogRef.close({
            cancel: true
        });
    }

    public onValidate() {
        this.dialogRef.close({
            cancel: false
        });
    }
}
