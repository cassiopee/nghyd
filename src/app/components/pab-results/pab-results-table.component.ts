import { Component, ViewChild, ElementRef, Input, OnChanges } from "@angular/core";

import { CloisonAval, Result, capitalize } from "jalhyd";

import { PabResults } from "../../results/pab-results";
import { I18nService } from "../../services/internationalisation.service";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";
import { AppComponent } from "../../app.component";
import { fv } from "../../util/util";

@Component({
    selector: "pab-results-table",
    templateUrl: "./pab-results-table.component.html",
    styleUrls: [
        "./pab-results-table.component.scss"
    ]
})
export class PabResultsTableComponent extends ResultsComponentDirective implements OnChanges {

    /** résultats non mis en forme */
    @Input()
    public results: PabResults;

    /** index de l'élément de résultat à afficher (modifié par le sélecteur de conditions limites) */
    @Input()
    public variableIndex = 0;

    /** entêtes des colonnes */
    private _headers: string[];

    /** résultats mis en forme */
    private _dataSet: any[];

    @ViewChild("tableContainer")
    table: ElementRef;

    constructor(
        protected intlService: I18nService
    ) {
        super();
    }

    public ngOnChanges() {
        this._dataSet = [];
        if (
            this.results
            && this.results.cloisonsResults
            && this.results.cloisonsResults.length > 0
            && !this.results.hasOnlyErrors()
        ) {
            const pr = this.results;
            // when a parameter is variating, index of the variating parameter
            // values to build the data from
            const vi = pr.variableIndex;

            // refresh headers here if language changed
            this._headers = pr.headers;

            // line 1
            if (pr.cloisonsResults[0].resultElements[vi].vCalc) { // parfois le calcul des cloisons échoue
                this._dataSet.push([
                    this.intlService.localizeText("INFO_LIB_AMONT"),
                    pr.cloisonsResults[0].resultElements[vi] ? fv(pr.cloisonsResults[0].resultElements[vi].vCalc) : "",
                    "", "", "", "", "", "", ""
                ]);
            }

            // lines 2 - n-1
            for (let i = 0; i < pr.cloisonsResults.length; i++) {
                if (
                    pr.cloisonsResults[i].resultElements[vi].vCalc
                ) {
                    const r2n = pr.cloisonsResults[i].resultElements[vi].values;
                    let Z1: number;
                    if (i < pr.cloisonsResults.length - 1) {
                        Z1 = pr.cloisonsResults[i + 1].resultElements[vi].vCalc;
                    } else {
                        Z1 = pr.cloisonAvalResults.resultElements[vi].vCalc;
                    }

                    this._dataSet.push([
                        i + 1, // n° cloison
                        fv(Z1), // Z
                        fv(r2n.ZRAM),
                        fv(r2n.DH),
                        fv(r2n.Q),
                        fv(r2n.PV),
                        fv(r2n.YMOY),
                        fv(r2n.ZRMB),
                        fv(r2n.QA),
                        this.getJetTypes(pr.cloisonsResults[i], vi),
                        this.getStructuresParam(pr.cloisonsResults[i], vi,"H1"),
                        this.getStructuresParam(pr.cloisonsResults[i], vi,"H2"),
                        this.getStructuresParam(pr.cloisonsResults[i], vi,"SUBMERGENCE"),
                    ]);
                }
            }

            // downstream line
            const cloisonAval = (pr.cloisonAvalResults.sourceNub as CloisonAval);
            if (pr.cloisonAvalResults.resultElements[vi].vCalc) {
                const rln = pr.cloisonAvalResults.resultElements[vi].values;
                this._dataSet.push([
                    this.intlService.localizeText("INFO_LIB_AVAL"),
                    (pr.Z2[vi] !== undefined ? fv(pr.Z2[vi]) : ""),
                    fv(cloisonAval.prms.ZRAM.singleValue),
                    fv(rln.DH),
                    fv(rln.Q),
                    "", "", "", "",
                    this.getJetTypes(pr.cloisonAvalResults, vi),
                    this.getStructuresParam(pr.cloisonAvalResults, vi, "H1"),
                    this.getStructuresParam(pr.cloisonAvalResults, vi, "H2"),
                    this.getStructuresParam(pr.cloisonAvalResults, vi, "SUBMERGENCE"),
                ]);
                // extra lift gate ?
                if (cloisonAval && cloisonAval.hasVanneLevante()) {
                    const vanneZDV = cloisonAval.result.resultElements[vi].getValue("ZDV");
                    if (vanneZDV) {
                        this._dataSet.push([
                            this.intlService.localizeText("INFO_LIB_COTE_VANNE_LEVANTE"),
                            fv(vanneZDV),
                            "", "", "", "", "", "", ""
                        ]);
                    }
                }
            }
        }
    }

    private getJetTypes(re: Result, vi: number): string {
        return this.getStructuresParam(re, vi, "ENUM_StructureJetType", "INFO_ENUM_STRUCTUREJETTYPE_");
    }

    private getStructuresParam(re: Result, vi: number, propName: string, localisationCode?: string): string {
        const devices = re.sourceNub.getChildren();
        // parameter for each device
        const params: string[] = devices.map((device) => {
            const val = device.result.resultElements[vi].getValue(propName);
            let sVal;
            if (localisationCode !== undefined) {
                sVal = this.intlService.localizeText(localisationCode + val);
            }
            else {
                sVal = fv(val);
            }
            if (devices.length > 1) {
                // evil HTML injection in table cell (simpler)
                sVal = this.intlService.localizeText("INFO_LIB_FS_OUVRAGE") + " n°"
                    + (device.findPositionInParent() + 1) + ": " + sVal;
            }
            return sVal;
        });
        return `<div class="inner-cell-line">` + params.join(`, </div><div class="inner-cell-line">`) + `</div>`;
    }

    public get headers() {
        return this._headers;
    }

    /**
     * Returns a combination of parameters and results for mat-table
     */
    public get dataSet() {
        return this._dataSet;
    }

    public exportAsSpreadsheet() {
        AppComponent.exportAsSpreadsheet(this.table.nativeElement);
    }

    public get uitextExportAsSpreadsheet() {
        return this.intlService.localizeText("INFO_RESULTS_EXPORT_AS_SPREADSHEET");
    }

    public get uitextEnterFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_ENTER_FS");
    }

    public get uitextExitFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXIT_FS");
    }
}
