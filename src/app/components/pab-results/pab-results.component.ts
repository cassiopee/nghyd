import { Component, Input } from "@angular/core";
import { LogHelper } from "app/results/log-helper";

import { Result, cLog, Message, MessageCode, MessageSeverity } from "jalhyd";

import { CalculatorResults } from "../../results/calculator-results";
import { PabResults } from "../../results/pab-results";
import { I18nService } from "../../services/internationalisation.service";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";

@Component({
    selector: "pab-results",
    templateUrl: "./pab-results.component.html",
    styleUrls: [
        "./pab-results.component.scss"
    ]
})
export class PabResultsComponent extends ResultsComponentDirective {

    /** résultats non mis en forme */
    private _pabResults: PabResults;

    private _logHelper: LogHelper;

    constructor(
        private i18nService: I18nService,
    ) {
        super();
    }

    @Input()
    public set results(rs: CalculatorResults[]) {
        this._pabResults = undefined;
        if (rs.length > 0 && rs[0] instanceof PabResults) {
            this._pabResults = rs[0] as PabResults;

            // log helper
            this._logHelper = new LogHelper(this._pabResults);
        }
    }

    public get globalLog(): cLog {
        return this._logHelper.globalLog;
    }

    public get iterationLog(): cLog {
        return this._logHelper.iterationLog;
    }

    public get pabResults() {
        return this._pabResults;
    }

    public get hasResults(): boolean {
        return this._pabResults && this._pabResults.hasResults;
    }

    public get hasDisplayableResults(): boolean {
        let ret = this._pabResults && this._pabResults.hasResults;
        if (
            this._pabResults
            && this._pabResults.variatedParameters
            && this._pabResults.variatedParameters.length > 0
        ) {
            ret = ret
            && this._pabResults.variableIndex !== undefined
            && this._pabResults.result.resultElements[this._pabResults.variableIndex] !== undefined
            && this._pabResults.result.resultElements[this._pabResults.variableIndex].ok;
        }
        return ret;
    }

    public get uitextGeneralLogTitle(): string {
        return this.i18nService.localizeText("INFO_TITREJOURNAL_GLOBAL");
    }

}
