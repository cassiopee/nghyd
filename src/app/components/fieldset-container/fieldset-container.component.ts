import { Component, Input, Output, EventEmitter, QueryList, ViewChildren, DoCheck, AfterViewInit } from "@angular/core";

import { FieldsetContainer } from "../../formulaire/elements/fieldset-container";
import { FieldSetComponent } from "../field-set/field-set.component";
import { FieldSet } from "../../formulaire/elements/fieldset";
import { FormulaireDefinition } from "../../formulaire/definition/form-definition";
import { I18nService } from "../../services/internationalisation.service";
import { ApplicationSetupService } from "../../services/app-setup.service";
import { DefinedBoolean } from "../../util/definedvalue/definedboolean";
import { ParamValueMode } from "jalhyd";

@Component({
    selector: "fieldset-container",
    templateUrl: "./fieldset-container.component.html",
    styleUrls: [
        "./fieldset-container.component.scss"
    ]
})
export class FieldsetContainerComponent implements DoCheck, AfterViewInit {

    public get title(): string {
        if (this._container) {
            return this._container.label;
        }
    }

    public get fieldsets() {
        return this._container.fieldsets;
    }

    public get isValid() {
        return this._isValid.value;
    }
    @Input()
    protected _container: FieldsetContainer;

    /**
     * liste des composants FieldSet enfants
     */
    @ViewChildren(FieldSetComponent)
    protected _fieldsetComponents: QueryList<FieldSetComponent>;

    /**
     * flag de validité des FieldSet enfants
     */
    private _isValid: DefinedBoolean;

    /**
     * événément de changement d'état d'un radio
     */
    @Output()
    private radio = new EventEmitter<any>();

    /**
     * événément de changement de validité
     */
    @Output()
    private validChange = new EventEmitter();

    /**
     * événément de changement de valeur d'un input
     */
    @Output()
    private inputChange = new EventEmitter();

    /** événement signalant un appui sur TAB ou SHIFT+TAB */
    @Output()
    protected tabPressed = new EventEmitter<any>();

    /**
     * nombre d'appels à DoCheck
     */
    private _DoCheckCount: number = 0;

    public constructor(
        private i18nService: I18nService,
        private appSetupService: ApplicationSetupService
    ) {
        this._isValid = new DefinedBoolean();
    }

    /**
     * Ajoute un nouveau sous-nub (Structure, PabCloisons, YAXN… selon le cas)
     * dans un nouveau fieldset
     */
    protected addSubNub(after: FieldSet, clone: boolean = false) {
        const newFs = this._container.addFromTemplate(after.indexAsKid());
        if (clone) {
            const prms = after.backupParameters();
            // replace in-place to change properties (overkill)
            // @WTF why only those two ?
            newFs.setPropValue("structureType", after.getPropValue("structureType"));
            newFs.setPropValue("loiDebit", after.getPropValue("loiDebit"));

            // au cas où un des paramètres du fieldset source est en mode calcul,
            // on met le paramètre copié en mode fixé (nghyd#567)
            for (const o of prms) {
                if (o.prmDef !== undefined) {
                    if (o.valueMode === ParamValueMode.CALCUL) {
                        o.valueMode = ParamValueMode.SINGLE;
                    }
                }
            }

            newFs.restoreParameters(prms);
        }
    }

    protected onFieldsetListChange() {
    }

    public ngAfterViewInit() {
        this.onFieldsetListChange();
        this._fieldsetComponents.changes.subscribe(_ => this.onFieldsetListChange());
    }

    /*
     * gestion des événements clic sur les radios :
     * réception d'un message du composant enfant (field-set)
     * cf. https://angular.io/guide/component-interaction#parent-listens-for-child-event
     */
    public onRadioClick(info: any) {
        // on renvoie l'info au parent
        this.radio.emit(info);
    }

    public ngDoCheck() {
        this._DoCheckCount++;
        // à priori, DoCheck n'est plus utile après quelques cycles de détection de changement
        // puisque la validité du fieldset container est déterminée par les saisies dans les inputs
        if (this._DoCheckCount < 3) {
            this.updateValidity(true);
        }
    }

    /**
    * calcul de la validité de tous les FieldSet de la vue
    */
    private computeValidity(): boolean {
        let res = false;

        if (this._fieldsetComponents?.length > 0) {
            res = this._fieldsetComponents.reduce(
                // callback
                (
                    // accumulator (valeur précédente du résultat)
                    acc,
                    // currentValue (élément courant dans le tableau)
                    fieldset,
                    // currentIndex (indice courant dans le tableau)
                    currIndex,
                    // array (tableau parcouru)
                    array
                ) => {
                    return acc && fieldset.isValid;
                }
                // valeur initiale
                , this._fieldsetComponents.length > 0);
        } else {
            // empty / hidden container ? everything OK.
            res = true;
        }

        return res;
    }

    private updateValidity(forceEmit: boolean = false) {
        // global validity
        this._isValid.value = this.computeValidity();
        if (forceEmit || this._isValid.changed) {
            this.validChange.emit();
        }
    }

    /**
     * réception d'un événement de validité de FieldSet
     */
    public onFieldsetValid() {
        this.updateValidity();
    }

    /**
    * réception d'un événement de changement de valeur d'un input
    */
    public onInputChange($event) {
        this.inputChange.emit($event);
    }

    /**
     * met à jour les paramètres liés
     */
    public updateLinkedParameters() {
        this._fieldsetComponents.forEach(fsc => fsc.updateLinkedParameters());
    }

    /**
    * réception d'un événement de demande d'ajout d'un FieldSet
    */
    public onAddFieldset(evt: { fs: FieldSet, clone: boolean }) {
        this.addSubNub(evt.fs, evt.clone);
    }

    /**
     * clic sur le bouton "ajouter une structure"
     */
    public onAddStructureClick() {
        this._container.addFromTemplate(0);
        this.validChange.emit();
    }

    /**
    * réception d'un événement de demande de suppression d'un FieldSet
    */
    public onRemoveFieldset(fs: FieldSet) {
        const form = this._container.parent as FormulaireDefinition;
        form.removeFieldset(fs);
    }

    /**
    * réception d'un événement de demande de remontée d'un FieldSet
    */
    public onMoveFieldsetUp(fs: FieldSet) {
        const form = this._container.parent as FormulaireDefinition;
        form.moveFieldsetUp(fs);
    }

    /**
    * réception d'un événement de demande de descente d'un FieldSet
    */
    public onMoveFieldsetDown(fs: FieldSet) {
        const form = this._container.parent as FormulaireDefinition;
        form.moveFieldsetDown(fs);
    }

    /**
     * Renvoie l'événement au composant du dessus
     */
    public onTabPressed(event) {
        this.tabPressed.emit(event);
    }

    public openHelp() {
        window.open("assets/docs/" + this.appSetupService.language + "/calculators/" + this._container.helpLink, "_blank");
    }

    public get enableHelpButton() {
        return this._container && this._container.helpLink;
    }

    public get enableAddStructureButton() {
        return this._container.getKids().length === 0;
    }

    public get uitextOpenHelp() {
        return this.i18nService.localizeText("INFO_CALCULATOR_OPEN_HELP");
    }

    public get uitextAddStructure(): string {
        return this.i18nService.localizeText("INFO_FIELDSET_ADD");
    }
}
