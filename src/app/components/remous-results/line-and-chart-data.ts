/**
 * données pour une ligne dans le graphique
 */
export class LineData {
    /**
     * abscisses
     */
    private _tx: number[];

    /**
     * orodonnées
     */
    private _ty: any[] = [];

    /**
     * graphique auquel appartient la ligne
     */
    private _parentChart: ChartData;

    /**
     * données fournies à ChartJS
     */
    private _data: any = {};

    /**
     * profondeur à laquelle est dessinée la ligne
     * les profondeurs les plus petites sont dessinées derrière les profondeurs les plus grandes
     */
    public z: number;

    constructor(gr: ChartData) {
        this._parentChart = gr;
        this._tx = gr.tx;
        for (let i = this._tx.length - 1; i >= 0; i--) {
            this._ty.push(null);
        }
    }

    public getYat(x: number) {
        const i = this._tx.indexOf(x);
        if (Array.isArray(this._ty[i])) {
            return this._ty[i][0];
        } else {
            return this._ty[i];
        }
    }

    public setPoint(x: number, y: number, reverse: boolean = false) {
        const i = this._tx.indexOf(x);
        // already a value ?
        if (this._ty[i] !== null) {
            // not already an Array ?
            if (! Array.isArray(this._ty[i])) {
                this._ty[i] = [ this._ty[i] ];
            }
            if (reverse) {
                this._ty[i].unshift(y);
            } else {
                this._ty[i].push(y);
            }
        } else {
            this._ty[i] = y;
        }
    }

    public mapPoint(x: number, y: number) {
        this.setPoint(x, this._parentChart.mapY(x, y));
    }

    public get ty() {
        return this._ty;
    }

    public get tx() {
        return this._tx;
    }

    public hasYs(): boolean {
        for (const y of this._ty) {
            if (y !== null) {
                return true;
            }
        }
        return false;
    }

    public get data() {
        const scatterData = [];
        for (let i = 0; i < this._data.data.length; i++) {
            const d = this._data.data[i];
            const x = this._tx[i];
            if (Array.isArray(d)) {
                for (const dd of d) {
                    scatterData.push({
                        x: x,
                        y: dd
                    });
                }
            } else {
                scatterData.push({
                    x: x,
                    y: d
                });
            }
        }
        const dataCopy = JSON.parse(JSON.stringify(this._data));
        dataCopy.data = scatterData;
        return dataCopy;
        // return this._data;
    }

    public set data(d: {}) {
        this._data = d;
        this._data["data"] = this._ty;
    }
}

export class ChartData {
    /**
     * tableau des (labels) des abscisses
     */
    private _tx: number[];

    private _lines: LineData[] = [];

    /**
     * pente du fond
    */
    private _penteFond: number;

    /**
     * longueur du bief
     */
    private _longBief: number;

    constructor(tX: number[], pente: number, bief: number) {
        this._tx = tX;
        this._penteFond = pente;
        this._longBief = bief;
    }

    /**
     * crée une ligne dans le graphique
     * @param z profondeur de la lign
     */
    public newLine(z: number): LineData {
        const res = new LineData(this);
        res.z = z;
        this._lines.push(res);
        return res;
    }

    public get tx(): number[] {
        return this._tx;
    }

    /**
   * transforme une cote en tenant compte du fond
   * @param x abscisse où se trouve la cote à transformer
   * @param y cote à transformer
   */
    public mapY(x: number, y: number) {
        let d: number;
        if (this._penteFond >= 0) {
            d = this._penteFond * (this._longBief - x);
        } else {
            d = -this._penteFond * x;
        }
        return y + d;
    }

    /**
     * dessine une ligne droite
     * @param y0 y en x=0
     * @param ymax y en x=xmax
     * @param color couleur de la ligne
     * @param lbl légende de la ligne
     * @param fillColor couleur de remplissage sous la ligne
     */
    public drawLine(y0: number, ymax: number, prof: number, color: string, lbl: string, fillColor?: string) {
        const l = this.newLine(prof);
        l.mapPoint(0, y0);
        l.mapPoint(this._longBief, ymax);

        // l.data = { label: lbl, data: l, fill: fillColor !== undefined, tension: 0,
        // borderColor: color, backgroundColor: fillColor, pointRadius: 0 };
        l.data = {
            label: lbl, fill: fillColor !== undefined, tension: 0, spanGaps: true,
            borderColor: color, backgroundColor: fillColor, pointRadius: 0
        };
    }

    /**
     * Dessigne une ligne droite entre y0 (abscisse 0) et ymax (abscisse max),
     * sans passer par les méthodes mapPoint() et mapY() utilisées dans drawLine
     * @param fillArea true pour remplir la zone sous la ligne
     * @param fillColor couleur de remplissage de la zone sous la ligne
     */
    public drawSimpleLine(y0: number, ymax: number, prof: number, color: string, lbl: string, fillArea: boolean = false, fillColor?: string) {
        const l = this.newLine(prof);
        l.setPoint(0, y0);
        l.setPoint(this._longBief, ymax);
        l.data = {
            label: lbl, fill: fillArea, tension: 0, spanGaps: true,
            borderColor: color, backgroundColor: fillColor, pointRadius: 0, showLine: "true"
        };
    }

    public get data() {
        const ds = [];
        this._lines.sort((a, b) => {
            if (a.z > b.z) {
                return -1;
            }
            if (a.z < b.z) {
                return 1;
            }
            return 0;
        });

        for (const l of this._lines) {
            ds.push(l.data);
        }

        return {
            labels: this._tx,
            datasets: ds
        };
    }
}
