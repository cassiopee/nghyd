import { ChangeDetectorRef, Component, Input, OnChanges, SimpleChanges, ViewChild } from "@angular/core";

import { INumberIterator, CourbeRemousParams, CourbeRemous, ParamDefinition, ParamDomainValue, cLog } from "jalhyd";

import { I18nService } from "../../services/internationalisation.service";
import { RemousResults } from "../../results/remous-results";
import { CalculatorResults } from "../../results/calculator-results";
import { FormulaireService } from "../../services/formulaire.service";
import { ResultsComponentDirective } from "../fixedvar-results/results.component";
import { AppComponent } from "../../app.component";
import { LineData, ChartData } from "./line-and-chart-data";
import { fv } from "../../util/util";
import { VarResults } from "../../results/var-results";

import { BaseChartDirective } from "ng2-charts";
import zoomPlugin from 'chartjs-plugin-zoom';
import { Chart } from "chart.js";

@Component({
    selector: "remous-results",
    templateUrl: "./remous-results.component.html",
    styleUrls: [
        "./remous-results.component.scss"
    ]
})
export class RemousResultsComponent extends ResultsComponentDirective implements OnChanges {

    private _remousResults: RemousResults;

    @ViewChild('chartcanvas1')
    private chartComponent1: BaseChartDirective;

    @ViewChild('chartcanvas2')
    private chartComponent2: BaseChartDirective;

    private _zoom1WasChanged = false;

    private _zoom2WasChanged = false;

    /*
    * config du graphique principal
    */
    public graph1_type = "scatter";
    public graph1_data = {};
    public graph1_options = {};

    /*
    * config du graphique secondaire (paramètre supplémentaire sauf Hs, Hsc, Ycor, Ycon)
    */
    public graph2_type = "line";
    public graph2_data = {};
    public graph2_options = {};

    /**
     * entêtes de la table de résultats numériques
     */
    private _tableHeaders: string[];

    constructor(
        private intlService: I18nService,
        private formService: FormulaireService,
        private cd: ChangeDetectorRef
    ) {
        super();
        Chart.register(zoomPlugin);
    }

    public get remousResults() {
        return this._remousResults;
    }

    public get uitextLigneFluviale() {
        // calculator type for translation
        const sn = this._remousResults.result.sourceNub;
        let ct = sn.calcType;
        if (sn.parent) {
            ct = sn.parent.calcType;
        }
        return this.formService.expandVariableNameAndUnit(ct, "FLU");
    }

    public get uitextLigneTorrentielle() {
        // calculator type for translation
        const sn = this._remousResults.result.sourceNub;
        let ct = sn.calcType;
        if (sn.parent) {
            ct = sn.parent.calcType;
        }
        return this.formService.expandVariableNameAndUnit(ct, "TOR");
    }

    public get uitextAbscisse() {
        return this.intlService.localizeText("INFO_REMOUSRESULTS_ABSCISSE");
    }

    public get uitextFond() {
        return this.intlService.localizeText("INFO_REMOUSRESULTS_FOND");
    }

    public get uitextBerge() {
        return this.intlService.localizeText("INFO_REMOUSRESULTS_BERGE");
    }

    public get uitextTirantNormal() {
        return this.intlService.localizeText("INFO_REMOUSRESULTS_TIRANTNORMAL");
    }

    public get uitextTirantCritique() {
        return this.intlService.localizeText("INFO_REMOUSRESULTS_TIRANTCRITIQUE");
    }

    public get uitextExportImageTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXPORT_IMAGE");
    }

    public get uitextEnterFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_ENTER_FS");
    }

    public get uitextExitFSTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_EXIT_FS");
    }

    public get extraChart(): boolean {
        return this._remousResults === undefined ? false : this._remousResults.extraChart;
    }

    private get extraParamLabel(): string {
        if (! this._remousResults) {
            return undefined;
        } else {
            // calculator type for translation
            const sn = this._remousResults.result.sourceNub;
            let ct = sn.calcType;
            if (sn.parent) {
                ct = sn.parent.calcType;
            }
            return this.formService.expandVariableNameAndUnit(ct, this._remousResults.extraParamSymbol);
        }
    }

    public headers(): string[] {
        return this._tableHeaders;
    }

    public get hasResults(): boolean {
        return this._remousResults?.hasResults;
    }

    public get hasData(): boolean {
        return this._remousResults && this._remousResults.result && this._remousResults.result.ok;
    }

    @Input()
    public set results(rs: CalculatorResults[]) {
        this._remousResults = undefined;
        if (rs !== undefined) {
            for (const r of rs) {
                if (r instanceof RemousResults) {
                    this._remousResults = r;
                    break;
                }
            }
        }
    }

    public ngOnChanges(s: SimpleChanges) {
        // console.log("_________> RRC onChanges", s);
        if (this._remousResults && this._remousResults.result) {
            this.generateChart();
        }
    }

    public get log(): cLog {
        if (this._remousResults !== undefined) {
            return this._remousResults.log;
        }
    }

    public get varResults(): VarResults {
        if (this._remousResults !== undefined) {
            return this._remousResults.varResults;
        }
    }

    /**
     * Retourne un peudo-paramètre variable représentant toutes les abscisses possibles
     */
    private get abscissaePseudoParameter(): ParamDefinition {
        const param = new ParamDefinition(undefined, "", ParamDomainValue.POS_NULL);
        param.setValues(
            0,
            this._remousResults.Long,
            this._remousResults.Dx
        );
        return param;
    }

    /**
     * Retourne un itérateur sur toutes les absisses possibles
     * (même celles n'ayant pas de résultat)
     */
    private get abscisseIterator(): INumberIterator {
        const param = this.abscissaePseudoParameter;
        return param.paramValues.initValuesIterator(false, undefined, true); // add last step !
    }

    private connectRessaut(lineFlu: LineData, lineTor: LineData) {
        if (lineFlu !== undefined && lineTor !== undefined) {
            const p = this.abscissaePseudoParameter;
            const xs = p.getInferredValuesList();
            // 1e passe : trouver combien d'abscisses en commun
            let aec = 0;
            for (const x of xs) {
                if (
                    (lineTor.getYat(x) !== null)
                    && (lineFlu.getYat(x) !== null)
                ) {
                    aec++;
                }
            }
            // 2e passe : prolonger la torrentielle
            outerloop1:
            for (let i = 0; i < xs.length; i++) {
                const x = xs[i];
                if (lineTor.getYat(x) === null) {
                    if (aec === 1 && i > 0) {
                        // une seule abscisse en commun, cas particulier: connexion verticale
                        lineTor.setPoint(xs[i - 1], lineFlu.getYat(xs[i - 1]));
                    } else {
                        lineTor.setPoint(x, lineFlu.getYat(x));
                    }
                    break outerloop1;
                }
            }
            // 3e passe : prolonger la fluviale
            outerloop2:
            for (let i = xs.length - 1; i >= 0; i--) {
                const x = xs[i];
                if (lineFlu.getYat(x) === null) {
                    if (aec === 1 && i < (xs.length - 1)) {
                        // une seule abscisse en commun, cas particulier: connexion verticale
                        lineFlu.setPoint(xs[i + 1], lineTor.getYat(xs[i + 1]), true);
                    } else {
                        lineFlu.setPoint(x, lineTor.getYat(x));
                    }
                    break outerloop2;
                }
            }
        }
    }

    private generateChart() {
        // http://www.chartjs.org/docs/latest/charts/line.html
        // le dernier dataset de la liste datasets est dessiné en 1er
        this._remousResults.update();

        const nub = this._remousResults.result.sourceNub as CourbeRemous;
        const params = nub.prms as CourbeRemousParams;
        const ZF1 = params.ZF1.singleValue;
        const ZF2 = params.ZF2.singleValue;
        const hauteurBerge = nub.section.prms.YB.singleValue;
        const penteFond: number = this._remousResults.penteFond;

        // abscisses

        let labs: number[] = [];
        let xmax: number;
        if (this._remousResults.result.ok) {
            xmax = -1e8;
            const itX2 = this.abscisseIterator;
            while (itX2.hasNext) {
                const x = itX2.next().value;
                labs.push(x);
                xmax = Math.max(x, xmax);
            }
        } else {
            labs = [0, 1];
            xmax = 1;
        }

        // init graphiques

        const gr1 = new ChartData(labs, penteFond, xmax);
        let gr2: ChartData;
        if (this._remousResults.extraChart) {
            gr2 = new ChartData(labs, 0, xmax);
        }

        // ligne de fond
        gr1.drawSimpleLine(ZF1, ZF2, 3, "#753F00", this.uitextFond, true, "#753F00");

        // ligne de berge
        if (hauteurBerge) {
            gr1.drawSimpleLine(ZF1 + hauteurBerge, ZF2 + hauteurBerge, 4, "#C58F50", this.uitextBerge, false, "#C58F50");
        }

        // hauteur normale
        if (this._remousResults.hautNormale?.ok) {
            const Yn = this._remousResults.hautNormale.vCalc;
            gr1.drawSimpleLine(Yn + ZF1, Yn + ZF2,
                5, "#A4C537", this.uitextTirantNormal, false, "#A4C537"
            );
        }

        // hauteur critique
        if (this._remousResults.hautCritique?.ok) {
            const Yc = this._remousResults.hautCritique.vCalc;
            gr1.drawSimpleLine(Yc + ZF1, Yc + ZF2,
                6, "#FF0000", this.uitextTirantCritique, false, "#FF0000"
            );
        }

        // lignes d'eau torrentielle et fluviale
        let lineFlu: LineData;
        if (this._remousResults.hasFluData) {
            lineFlu = gr1.newLine(0);
        }
        let lineTor: LineData;
        if (this._remousResults.hasTorData) {
            lineTor = gr1.newLine(1);
        }
        let lineExtra: LineData;
        if (this._remousResults.hasExtra) {
            if (this._remousResults.extraChart) {
                lineExtra = gr2.newLine(2);
            } else {
                lineExtra = gr1.newLine(2);
            }
        }

        // abscisses pour lesquelles on a un résultat
        const rrr = this._remousResults.result;
        if (rrr.resultElements.length > 0) {
            const firstResultAbscissa = rrr.resultElements[0].values.X;
            const lastResultAbscissa = rrr.resultElements[rrr.resultElements.length - 1].values.X;
            let resultAbscissaIndex = 0;
            const itX = this.abscisseIterator;

            // parcours des abscisses une par une
            while (itX.hasNext) {
                const x = itX.next().value;
                // skip abscissa that have no result
                if (x >= firstResultAbscissa && x <= lastResultAbscissa) {
                    // result element for the given abscissa
                    const re = rrr.resultElements[resultAbscissaIndex];
                    resultAbscissaIndex++;

                    const yExtra = re.getValue(this._remousResults.extraParamSymbol);
                    if (yExtra !== undefined) {
                        lineExtra.setPoint(x, yExtra);
                    }

                    const yFlu = re.getValue("flu");
                    if (yFlu !== undefined) {
                        lineFlu.setPoint(x, yFlu);
                    }

                    const yTor = re.getValue("tor");
                    if (yTor !== undefined) {
                        lineTor.setPoint(x, yTor);
                    }
                }
            }
        }

        const extraLineColor = "#0093BD";
        if (this._remousResults.hasExtra) {
            if (this._remousResults.extraChart) {
                lineExtra.data = {
                    label: this.extraParamLabel,
                    tension: 0, spanGaps: true, borderColor: extraLineColor
                };
            } else {
                lineExtra.data = {
                    label: this.extraParamLabel,
                    tension: 0, fill: false, spanGaps: true, borderColor: "#C17AF0", pointBackgroundColor: "#C17AF0", pointRadius: 4, backgroundColor: "#C17AF0", showLine: "true"
                };
            }
        }

        // raccordement ligne fluviale -> torrentielle pour dessiner le ressaut
        this.connectRessaut(lineFlu, lineTor);

        // couleur de la zone sous la ligne
        const lineAreaColor = "rgba(209,208,212,0.5)";

        // ajout des données au graphique
        if (lineTor !== undefined) {
            lineTor.data = {
                label: this.uitextLigneTorrentielle,
                tension: 0,
                borderColor: "#77A3CD",
                pointBackgroundColor: "#77A3CD",
                pointRadius: 4,
                backgroundColor: lineAreaColor,
                showLine: "true"
            };
        }
        if (lineFlu !== undefined) {
            lineFlu.data = {
                label: this.uitextLigneFluviale,
                tension: 0,
                borderColor: "#0093BD",
                pointBackgroundColor: "#0093BD",
                pointRadius: 4,
                backgroundColor: lineAreaColor,
                showLine: "true"
            };
        }

        this.graph1_data = gr1.data;

        const that = this;
        this.graph1_options = {
            responsive: true,
            maintainAspectRatio: true,
            animation: {
                duration: 0
            },
            scales: {
                x: {
                    gridLines: {
                        display: true,
                        color: "rgba(255,99,132,0.2)",
                        offsetGridLines: true
                    },
                    ticks: {
                        precision: ResultsComponentDirective.CHARTS_AXIS_PRECISION,
                        callback: function (value, index, values) {
                            return fv(Number(value));
                        }
                    },
                    title: {
                        display: true,
                        text: this.uitextAbscisse
                    }
                },
                y: {
                    // stacked: true,
                    gridLines: {
                        display: true,
                        color: "rgba(255,99,132,0.2)"
                    }
                }
            },
            plugins: {
                legend: {
                    display: true,
                    position: "bottom"
                },
                tooltip: {
                    callbacks: {
                        label: function (tooltipItem) {
                            return fv(tooltipItem.dataset.data[tooltipItem.dataIndex].y);
                        }
                    }
                },
                zoom: {
                    pan: {
                        enabled: false,
                    },
                    zoom: {
                        drag: {
                            enabled: true,
                            borderColor: "rgba(225,225,225,0.3)",
                            borderWidth: 1,
                            backgroundColor: "rgba(0,0,0,0.25)"
                        },
                        mode: 'xy',
                        onZoomComplete: function (t: any) { return function () { t.zoom1Complete(); }; }(that)
                    }
                }
            },
            elements: {
                line: {
                    fill: true
                }
            }
        };

        if (this._remousResults.extraChart) {
            this.graph2_data = gr2.data;

            this.graph2_options = {
                responsive: true,
                maintainAspectRatio: true,
                animation: {
                    duration: 0
                },
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: this.uitextAbscisse
                        },
                        ticks: {
                            precision: ResultsComponentDirective.CHARTS_AXIS_PRECISION,
                            callback: (value, index, values) => {
                                // pour une raison mystérieuse, values ne contient pas les x fournis mais des valeurs générées,
                                // et donc value est inutilisable
                                return fv(gr2.data.datasets[0].data[index].x);
                            }
                        },
                    }
                },
                plugins: {
                    legend: {
                        display: true,
                        position: "bottom"
                    },
                    tooltip: {
                        callbacks: {
                            label: function (tooltipItem) {
                                return fv(tooltipItem.dataset.data[tooltipItem.dataIndex].y);
                            },
                            title: () => {  // pour empêcher le tooltip de contenir l'abscisse du point
                                return undefined; // pq a t-on besoin de ce callback dans ce graphe et pas dans l'autre ? mystère aussi...
                            }
                        }
                    },
                    zoom: {
                        pan: {
                            enabled: false,
                        },
                        zoom: {
                            drag: {
                                enabled: true,
                                borderColor: "rgba(225,225,225,0.3)",
                                borderWidth: 1,
                                backgroundColor: "rgba(0,0,0,0.25)"
                            },
                            mode: 'xy',
                            onZoomComplete: function (t: any) { return function () { t.zoom2Complete(); }; }(that)
                        }
                    }
                },
                elements: {
                    line: {
                        fill: true,
                        backgroundColor: lineAreaColor
                    },
                    point: {
                        pointBorderColor: extraLineColor,
                        pointBackgroundColor: lineAreaColor,
                        radius: 4,
                        borderWidth: 1
                    }
                }
            };
        }
    }

    public exportAsImage(element: HTMLDivElement) {
        AppComponent.exportAsImage(element.querySelector("canvas"));
    }

    public resetZoom1() {
        this.chartComponent1.chart.resetZoom();
        this._zoom1WasChanged = false;
    }

    public resetZoom2() {
        this.chartComponent2.chart.resetZoom();
        this._zoom2WasChanged = false;
    }

    public zoom1Complete() {
        this._zoom1WasChanged = true;
        console.log("detect changes");
        this.cd.detectChanges();
    }

    public zoom2Complete() {
        this._zoom2WasChanged = true;
        this.cd.detectChanges();
    }

    public get zoom1WasChanged(): boolean {
        return this._zoom1WasChanged;
    }

    public get zoom2WasChanged(): boolean {
        return this._zoom2WasChanged;
    }

    public get uitextResetZoomTitle() {
        return this.intlService.localizeText("INFO_CHART_BUTTON_TITLE_RESET_ZOOM");
    }
}
