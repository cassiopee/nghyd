import { Component, Output, EventEmitter, AfterViewChecked, Inject, forwardRef, Input } from "@angular/core";

import { FormulaireDefinition } from "../../formulaire/definition/form-definition";
import { CalculatorResults } from "../../results/calculator-results";
import { GenericCalculatorComponent } from "../generic-calculator/calculator.component";

@Component({
    selector: "calc-results",
    templateUrl: "./calculator-results.component.html",
})
export class CalculatorResultsComponent implements AfterViewChecked {

    private _formulaire: FormulaireDefinition;

    /** notify CalculatorComponent that it may scroll down to results panel */
    @Output()
    private afterViewChecked = new EventEmitter();

    public constructor(
        @Inject(forwardRef(() => GenericCalculatorComponent)) private calculatorComponent: GenericCalculatorComponent
    ) { }

    @Input()
    public set formulaire(f: FormulaireDefinition) {
        this._formulaire = f;
    }

    public get formResultsArray(): CalculatorResults[] {
        let r: CalculatorResults[] = [];
        if (this._formulaire !== undefined) {
            r = this._formulaire.results;
        }
        return r;
    }

    public ngAfterViewChecked() {
        this.afterViewChecked.emit();
    }

    public get isSP() {
        return this.calculatorComponent.isSP;
    }

    public get isRemous() {
        return this.calculatorComponent.isRemous;
    }

    public get isPAB() {
        return this.calculatorComponent.isPAB;
    }

    public get isPB() {
        return this.calculatorComponent.isPB;
    }

    public get isMRC() {
        return this.calculatorComponent.isMRC;
    }

    public get isJet() {
        return this.calculatorComponent.isJet;
    }

    public get isVerificateur() {
        return this.calculatorComponent.isVerificateur;
    }

}
