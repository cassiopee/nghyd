import { Component, Input } from "@angular/core";

import { cLog, Message } from "jalhyd";

import { I18nService } from "../../services/internationalisation.service";

@Component({
    selector: "log-drawer",
    templateUrl: "./log-drawer.component.html",
    styleUrls: [
        "./log-drawer.component.scss"
    ]
})
/** le log à tiroirs ! */
export class LogDrawerComponent {

    /** A list of log messages accompanied by a sub-log (multiple messages) */
    private _log: Array<{ message: Message, subLog: cLog }>;

    private entriesStates: boolean[] = [];

    // title to display above the log
    @Input()
    public logTitle: string;

    constructor(
        private intlService: I18nService,
    ) {
        this._log = [];
    }

    @Input()
    public set log(log: Array<{ message: Message, subLog: cLog }>) {
        this._log = log;
    }

    public get log(): Array<{ message: Message, subLog: cLog }> {
        return this._log;
    }

    public get uitextTitreJournal(): string {
        if (this.logTitle) {
            return this.logTitle;
        } else {
            return this.intlService.localizeText("INFO_TITREJOURNAL");
        }
    }

    public get hasEntries(): boolean {
        return this._log !== undefined && this._log.length !== 0;
    }

    public get uitextShowDetails(): string {
        return this.intlService.localizeText("INFO_LOG_SHOW_DETAILS");
    }

    public get uitextHideDetails(): string {
        return this.intlService.localizeText("INFO_LOG_HIDE_DETAILS");
    }

    public entryIsOpen(i: number): boolean {
        if (this.entriesStates[i] !== undefined) {
            return this.entriesStates[i];
        }
        return false;
    }

    public setEntryOpen(i: number, open: boolean) {
        this.entriesStates[i] = open;
    }

    /** trackBy:index simulator @see nghyd#364 */
    public tbIndex(index: number, item: any) {
        return index;
    }
}
