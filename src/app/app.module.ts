import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

import { MatBadgeModule } from "@angular/material/badge";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatCardModule } from "@angular/material/card";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { ErrorStateMatcher } from "@angular/material/core";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatMenuModule } from "@angular/material/menu";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatRadioModule } from "@angular/material/radio";
import { MatSelectModule } from "@angular/material/select";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatTableModule } from "@angular/material/table";
import { MatTabsModule } from "@angular/material/tabs";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatExpansionModule } from '@angular/material/expansion';
import { MaterialFileInputModule } from "ngx-material-file-input";

import { DragDropModule } from "@angular/cdk/drag-drop";

import { TableModule } from "primeng/table";
import { MarkdownModule, MarkedOptions } from "ngx-markdown";

import { FlexLayoutModule } from "@angular/flex-layout";
import {
    CustomBreakPointsProvider,
    FlexGtXxsShowHideDirective,
    FlexXxsShowHideDirective,
    FlexLtXsShowHideDirective
} from "./directives/flex-xxs.directive";

import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms"; // <-- NgModel lives here
import { NgChartsModule } from "ng2-charts";
import { RouterModule, Routes } from "@angular/router";
import { HotkeyModule } from "angular2-hotkeys";
import { NgxMatomoTrackerModule } from "@ngx-matomo/tracker";

import { FormulaireService } from "./services/formulaire.service";
import { I18nService } from "./services/internationalisation.service";
import { HttpService } from "./services/http.service";
import { ApplicationSetupService } from "./services/app-setup.service";
import { NotificationsService } from "./services/notifications.service";

import { AppComponent } from "./app.component";
import { NgParamInputComponent } from "./components/ngparam-input/ngparam-input.component";
import { FieldSetComponent } from "./components/field-set/field-set.component";
import { FieldsetContainerComponent } from "./components/fieldset-container/fieldset-container.component";
import { ParamComputedComponent } from "./components/param-computed/param-computed.component";
import { ParamFieldLineComponent } from "./components/param-field-line/param-field-line.component";
import { ParamValuesComponent } from "./components/param-values/param-values.component";
import { SelectFieldLineComponent } from "./components/select-field-line/select-field-line.component";
import { CalculatorResultsComponent } from "./components/calculator-results/calculator-results.component";
import { FixedVarResultsComponent } from "./components/fixedvar-results/fixedvar-results.component";
import { SectionResultsComponent } from "./components/section-results/section-results.component";
import { GenericCalculatorComponent } from "./components/generic-calculator/calculator.component";
import { CalculatorNameComponent } from "./components/generic-calculator/calc-name.component";
import { SectionCanvasComponent } from "./components/section-canvas/section-canvas.component";
import { RemousResultsComponent } from "./components/remous-results/remous-results.component";
import { ResultsChartComponent } from "./components/results-chart/results-chart.component";
import { PabResultsComponent } from "./components/pab-results/pab-results.component";
import { PabResultsTableComponent } from "./components/pab-results/pab-results-table.component";
import { PbResultsComponent } from "./components/pb-results/pb-results.component";
import { PbResultsTableComponent } from "./components/pb-results/pb-results-table.component";
import { ChartTypeSelectComponent } from "./components/results-chart/chart-type.component";
import { CalculatorListComponent } from "./components/calculator-list/calculator-list.component";
import { ApplicationSetupComponent } from "./components/app-setup/app-setup.component";
import { BaseParamInputComponent } from "./components/base-param-input/base-param-input.component";
import { FixedResultsComponent } from "./components/fixedvar-results/fixed-results.component";
import { VarResultsComponent } from "./components/fixedvar-results/var-results.component";
import { LogComponent } from "./components/log/log.component";
import { LogDrawerComponent } from "./components/log-drawer/log-drawer.component";
import { LogEntryComponent } from "./components/log-entry/log-entry.component";
import { ParamLinkComponent } from "./components/param-link/param-link.component";
import { PabProfileChartComponent } from "./components/pab-profile-chart/pab-profile-chart.component";
import { PabTableComponent } from "./components/pab-table/pab-table.component";
import { PbSchemaComponent } from "./components/pb-schema/pb-schema.component";
import { PbCloisonResultsComponent } from "./components/pb-results/pb-cloison-results.component";
import { VariableResultsSelectorComponent } from "./components/variable-results-selector/variable-results-selector.component";
import { QuicknavComponent } from "./components/quicknav/quicknav.component";
import { ModulesDiagramComponent } from "./components/modules-diagram/modules-diagram.component";
import { MacrorugoCompoundResultsTableComponent } from "./components/macrorugo-compound-results/macrorugo-compound-results-table.component";
import { MacrorugoCompoundResultsComponent } from "./components/macrorugo-compound-results/macrorugo-compound-results.component";
import { JetResultsComponent } from "./components/jet-results/jet-results.component";
import { JetTrajectoryChartComponent } from "./components/jet-trajectory-chart/jet-trajectory-chart.component";
import { SessionPropertiesComponent } from "./components/session-properties/session-properties.component";
import { VerificateurResultsComponent } from "./components/verificateur-results/verificateur-results.component";

import { DialogConfirmComponent } from "./components/dialog-confirm/dialog-confirm.component";
import { DialogConfirmEmptySessionComponent } from "./components/dialog-confirm-empty-session/dialog-confirm-empty-session.component";
import { DialogConfirmCloseCalcComponent } from "./components/dialog-confirm-close-calc/dialog-confirm-close-calc.component";
import { DialogEditPabComponent } from "./components/dialog-edit-pab/dialog-edit-pab.component";
import { DialogEditParamComputedComponent } from "./components/dialog-edit-param-computed/dialog-edit-param-computed.component";
import { DialogEditParamValuesComponent } from "./components/dialog-edit-param-values/dialog-edit-param-values.component";
import { DialogGeneratePABComponent } from "./components/dialog-generate-pab/dialog-generate-pab.component";
import { DialogGeneratePARSimulationComponent } from "./components/dialog-generate-par-simulation/dialog-generate-par-simulation.component";
import { DialogLoadSessionComponent } from "./components/dialog-load-session/dialog-load-session.component";
import { DialogLogEntriesDetailsComponent } from "./components/dialog-log-entries-details/dialog-log-entries-details.component";
import { DialogSaveSessionComponent } from "./components/dialog-save-session/dialog-save-session.component";
import { DialogNewPbCloisonComponent } from "./components/dialog-new-pb-cloison/dialog-new-pb-cloison.component";
import { DialogLoadPredefinedEspeceComponent } from "./components/dialog-load-predefined-espece/dialog-load-predefined-espece.component";

import { JalhydAsyncModelValidationDirective } from "./directives/jalhyd-async-model-validation.directive";
import {
    JalhydModelValidationDirective,
    JalhydModelValidationMinDirective,
    JalhydModelValidationMaxDirective,
    JalhydModelValidationStepDirective
} from "./directives/jalhyd-model-validation.directive";
import { ImmediateErrorStateMatcher } from "./formulaire/immediate-error-state-matcher";
import { LoadSessionURLComponent } from "./components/load-session-url/load-session-url.component";
import { DialogShowMessageComponent } from "./components/dialog-show-message/dialog-show-message.component";
import { DialogConfirmLoadSessionURLComponent } from "./components/dialog-confirm-load-session-url/dialog-confirm-load-session-url.component";
import { BasinFieldsetContainerComponent } from "./components/basin-fieldset-container/basin-fieldset-container.component";
import { PrebarrageService } from "./services/prebarrage.service";
import { SelectSectionDetailsComponent } from "./components/select-section-details/select-section-details.component";
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ServiceWorkerUpdateService } from "./services/service-worker-update.service";
import { UserConfirmationService } from "./services/user-confirmation.service";

const appRoutes: Routes = [
    { path: "list/search", component: CalculatorListComponent },
    { path: "list", component: CalculatorListComponent },
    { path: "calculator/:uid", component: GenericCalculatorComponent },
    { path: "setup", component: ApplicationSetupComponent },
    { path: "diagram", component: ModulesDiagramComponent },
    { path: "properties", component: SessionPropertiesComponent },
    { path: "loadsession/:path", component: LoadSessionURLComponent },
    { path: "**", redirectTo: "list", pathMatch: "full" }
];

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        BrowserModule,
        NgChartsModule,
        DragDropModule,
        FlexLayoutModule,
        HotkeyModule.forRoot(),
        HttpClientModule,
        MarkdownModule.forRoot({
            markedOptions: {
                provide: MarkedOptions,
                useValue: {
                    gfm: true,
                    breaks: true
                }
            }
        }),
        MatBadgeModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatDialogModule,
        MaterialFileInputModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatProgressBarModule,
        MatRadioModule,
        MatSelectModule,
        MatSidenavModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatExpansionModule,
        NgxMatomoTrackerModule.forRoot({
            // Matomo open-source Web analytics
            siteId: 1,
            trackerUrl: "https://stasi.g-eau.fr/"
        }),
        RouterModule.forRoot(appRoutes, {
            useHash: true,
            enableTracing: false,
            relativeLinkResolution: 'legacy'
        }),
        TableModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
          enabled: environment.production,
          // Register the ServiceWorker as soon as the application is stable
          // or after 30 seconds (whichever comes first).
          registrationStrategy: 'registerWhenStable:30000'
        })
    ],
    declarations: [
        AppComponent,
        ApplicationSetupComponent,
        BaseParamInputComponent,
        CalculatorListComponent,
        CalculatorNameComponent,
        CalculatorResultsComponent,
        DialogConfirmCloseCalcComponent,
        DialogConfirmEmptySessionComponent,
        DialogConfirmComponent,
        DialogEditPabComponent,
        DialogEditParamComputedComponent,
        DialogEditParamValuesComponent,
        DialogGeneratePABComponent,
        DialogGeneratePARSimulationComponent,
        DialogLoadSessionComponent,
        DialogLogEntriesDetailsComponent,
        DialogSaveSessionComponent,
        DialogNewPbCloisonComponent,
        DialogLoadPredefinedEspeceComponent,
        DialogShowMessageComponent,
        DialogConfirmLoadSessionURLComponent,
        FieldSetComponent,
        FieldsetContainerComponent,
        BasinFieldsetContainerComponent,
        FixedResultsComponent,
        FixedVarResultsComponent,
        FlexGtXxsShowHideDirective,
        FlexLtXsShowHideDirective,
        FlexXxsShowHideDirective,
        GenericCalculatorComponent,
        ChartTypeSelectComponent,
        JalhydAsyncModelValidationDirective,
        JalhydModelValidationDirective,
        JalhydModelValidationMinDirective,
        JalhydModelValidationMaxDirective,
        JalhydModelValidationStepDirective,
        JetResultsComponent,
        JetTrajectoryChartComponent,
        LoadSessionURLComponent,
        LogComponent,
        LogDrawerComponent,
        LogEntryComponent,
        ModulesDiagramComponent,
        NgParamInputComponent,
        PabProfileChartComponent,
        PabResultsComponent,
        PabResultsTableComponent,
        PabTableComponent,
        PbResultsComponent,
        PbCloisonResultsComponent,
        PbResultsTableComponent,
        PbSchemaComponent,
        VariableResultsSelectorComponent,
        MacrorugoCompoundResultsComponent,
        MacrorugoCompoundResultsTableComponent,
        ParamComputedComponent,
        ParamFieldLineComponent,
        ParamLinkComponent,
        ParamValuesComponent,
        QuicknavComponent,
        RemousResultsComponent,
        ResultsChartComponent,
        SectionCanvasComponent,
        SectionResultsComponent,
        SelectFieldLineComponent,
        SessionPropertiesComponent,
        VarResultsComponent,
        VerificateurResultsComponent,
        SelectSectionDetailsComponent
    ],
    providers: [ // services
        ApplicationSetupService,
        CustomBreakPointsProvider,
        FormulaireService,
        HttpService,
        I18nService,
        NotificationsService,
        PrebarrageService,
        {
            provide: ErrorStateMatcher,
            useClass: ImmediateErrorStateMatcher
        },
        ServiceWorkerUpdateService,
        UserConfirmationService
    ],
    schemas: [NO_ERRORS_SCHEMA],
    bootstrap: [AppComponent]
})

export class AppModule { }
