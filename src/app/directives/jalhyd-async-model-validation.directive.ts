import { Validator, AbstractControl, ValidationErrors, NG_ASYNC_VALIDATORS } from "@angular/forms";
import { Directive, Input, forwardRef } from "@angular/core";
import { NgBaseParam } from "../components/base-param-input/base-param-input.component";
import { Observable } from "rxjs";

/**
 * Asynchronous validator for Ngparam, relying on JaLHyd ParamDefinition model
 */
@Directive({
    selector: "[appAsyncJalhydModelValidation]",
    providers: [ {
        provide: NG_ASYNC_VALIDATORS,
        useExisting: forwardRef(() => JalhydAsyncModelValidationDirective),
        multi: true
    } ]
})
export class JalhydAsyncModelValidationDirective implements Validator {
    @Input("appAsyncJalhydModelValidation") ngBaseParam: NgBaseParam;

    validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
        let errorPromiseReturn = new Promise(res => {
            res(null);
        }); // no error, everything OK
        const result = this.ngBaseParam.validateModelValue(control.value);
        if (! result.isValid) {
            errorPromiseReturn = new Promise<ValidationErrors>(res => {
                res({
                    "jalhydModel": {
                        value: control.value,
                        message: result.message
                    }
                });
            });
        }
        return errorPromiseReturn;
    }
}
