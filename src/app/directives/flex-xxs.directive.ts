import { Directive, Input } from "@angular/core";
import { BREAKPOINT, ShowHideDirective } from "@angular/flex-layout";

const XXS_BREAKPOINTS = [
    {
        alias: "xxs",
        mediaQuery: "screen and (max-width: 479px)",
        overlapping: false
    },
    {
        alias: "xs", // redéfinition
        mediaQuery: "screen and (min-width: 480px) screen and (max-width: 599px)",
        overlapping: false
    },
    {
        alias: "gt-xxs",
        mediaQuery: "screen and (min-width: 480px)",
        overlapping: false
    },
    {
        alias: "lt-xs",
        mediaQuery: "screen and (max-width: 479px)",
        overlapping: false
    }
];

export const CustomBreakPointsProvider = {
    provide: BREAKPOINT,
    useValue: XXS_BREAKPOINTS,
    multi: true
};

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: `[fxHide.xxs]`
})
export class FlexXxsShowHideDirective extends ShowHideDirective {
    protected inputs = [ "fxHide.xxs" ];
    @Input() "fxHide.xxs": string;
}

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: `[fxHide.gt-xxs]`
})
export class FlexGtXxsShowHideDirective extends ShowHideDirective {
    protected inputs = [ "fxHide.gt-xxs" ];
    @Input() "fxHide.gt-xxs": string;
}

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: `[fxHide.lt-xs]`
})
export class FlexLtXsShowHideDirective extends ShowHideDirective {
    protected inputs = [ "fxHide.lt-xs" ];
    @Input() "fxHide.lt-xs": string;
}
