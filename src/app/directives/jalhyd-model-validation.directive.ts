import { NG_VALIDATORS, Validator, AbstractControl, ValidatorFn } from "@angular/forms";
import { Directive, Input } from "@angular/core";
import { NgBaseParam } from "../components/base-param-input/base-param-input.component";
import { NgParameter } from "../formulaire/elements/ngparam";
import { sprintf } from "sprintf-js";
import { ServiceFactory } from "../services/service-factory";

/**
 * Synchronous validator for Ngparam, relying on JaLHyd ParamDefinition model
 */
@Directive({
    selector: "[appJalhydModelValidation]",
    providers: [ {
        provide: NG_VALIDATORS,
        useExisting: JalhydModelValidationDirective,
        multi: true
    } ]
})
export class JalhydModelValidationDirective implements Validator {
    @Input("appJalhydModelValidation") ngBaseParam: NgBaseParam;

    validate(control: AbstractControl): { [key: string]: any } | null {
        const mv = jalhydModelValidator(this.ngBaseParam)(control);
        return mv;
    }
}

/**
 * Synchronous validator for Ngparam, relying on JaLHyd ParamDefinition checkMin()
 */
@Directive({
    selector: "[appJalhydModelValidationMin]",
    providers: [ {
        provide: NG_VALIDATORS,
        useExisting: JalhydModelValidationMinDirective,
        multi: true
    } ]
})
export class JalhydModelValidationMinDirective implements Validator {
    @Input("appJalhydModelValidationMin") ngParam: NgParameter;

    validate(control: AbstractControl): { [key: string]: any } | null {
        const mv = jalhydModelValidatorMin(this.ngParam)(control);
        return mv;
    }
}

/**
 * Synchronous validator for Ngparam, relying on JaLHyd ParamDefinition checkMax()
 */
@Directive({
    selector: "[appJalhydModelValidationMax]",
    providers: [ {
        provide: NG_VALIDATORS,
        useExisting: JalhydModelValidationMaxDirective,
        multi: true
    } ]
})
export class JalhydModelValidationMaxDirective implements Validator {
    @Input("appJalhydModelValidationMax") ngParam: NgParameter;

    validate(control: AbstractControl): { [key: string]: any } | null {
        const mv = jalhydModelValidatorMax(this.ngParam)(control);
        return mv;
    }
}

/**
 * Synchronous validator for Ngparam, relying on JaLHyd ParamDefinition checkMinMaxStep()
 */
@Directive({
    selector: "[appJalhydModelValidationStep]",
    providers: [ {
        provide: NG_VALIDATORS,
        useExisting: JalhydModelValidationStepDirective,
        multi: true
    } ]
})
export class JalhydModelValidationStepDirective implements Validator {
    @Input("appJalhydModelValidationStep") ngParam: NgParameter;

    validate(control: AbstractControl): { [key: string]: any } | null {
        const mv = jalhydModelValidatorStep(this.ngParam)(control);
        return mv;
    }
}

export function jalhydModelValidator(ngBaseParam: NgBaseParam): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        let errorReturn = null; // no error, everything OK
        const result = ngBaseParam.validateModelValue(control.value);
        if (result && ! result.isValid) {
            errorReturn = {
                "jalhydModel": {
                    value: control.value,
                    message: result.message
                }
            };
        }
        return errorReturn;
    };
}

export function jalhydModelValidatorMin(ngParam: NgParameter): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        let errorReturn = null; // no error, everything OK
        const result = ngParam.checkMin(control.value);
        if (! result) {
            errorReturn = {
                "jalhydModelMin": {
                    value: control.value,
                    message: sprintf(
                        ServiceFactory.i18nService.localizeText("ERROR_MINMAXSTEP_MIN"),
                        ngParam.domain.minValue,
                        ngParam.maxValue
                    )
                }
            };
        }
        return errorReturn;
    };
}

export function jalhydModelValidatorMax(ngParam: NgParameter): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        let errorReturn = null; // no error, everything OK
        const result = ngParam.checkMax(control.value);
        if (! result) {
            errorReturn = {
                "jalhydModelMax": {
                    value: control.value,
                    message: sprintf(
                        ServiceFactory.i18nService.localizeText("ERROR_MINMAXSTEP_MAX"),
                        ngParam.minValue,
                        ngParam.domain.maxValue
                    )
                }
            };
        }
        return errorReturn;
    };
}

export function jalhydModelValidatorStep(ngParam: NgParameter): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        let errorReturn = null; // no error, everything OK
        // @WARNING remplacement du contrôle complet de min/max/step par une
        // simple vérification que le pas est positif
        // const result = ngParam.checkMinMaxStep(control.value);
        const result = (control.value > 0);
        if (! result) {
            errorReturn = {
                "jalhydModelStep": {
                    value: control.value,
                    message: sprintf(
                        // ServiceFactory.i18nService.localizeText("ERROR_MINMAXSTEP_STEP"),
                        ServiceFactory.i18nService.localizeText("ERROR_PARAM_MUST_BE_POSITIVE"),
                        ngParam.stepRefValue.toString()
                    )
                }
            };
        }
        return errorReturn;
    };
}
