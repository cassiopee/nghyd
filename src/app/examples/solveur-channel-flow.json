{
    "header": {
        "source": "jalhyd",
        "format_version": "1.3",
        "created": "2020-03-10T10:41:12.161Z"
    },
    "settings": {
        "precision": 1e-7,
        "maxIterations": 100,
        "displayPrecision": 3
    },
    "documentation": "\nCette session correspond au premier cas d'étude présent dans *la méthodologie de calcul du débit du droit d'eau fondé en titre, AFB, Irstea, septembre 2017*.\n\nElle consiste à calculer le débit d'un moulin à partir de la cote de fonctionnement normale (fixée à 394,66m) et des différents organes hydrauliques constituant la prise d'eau du moulin (une vanne motrice, un canal d'amenée et une vanne à l'entrée du canal d'amenée).\n\nLe calcul s'effectue par itérations successives des opérations suivantes :\n\n- Choix d'un débit probable \n- Calcul de la cote de l'eau amont de la vanne motrice\n- Calcul de la cote de l'eau à l'amont du canal d'amenée\n- Calcul de la cote de l'eau à l'amont de la vanne d'entrée\n- Vérification de l'égalité de la cote obtenue avec la cote normale de fonctionnement\n\nLes différents paramètres utiles au calcul suivant sont liés à ceux du précédent. Il suffit de demander le calcul de la cote de l'eau à l'amont de la vanne d'entrée du canal pour que les calculs précédents s'effectuent automatiquement.\n\nCes itérations peuvent être faites manuellement par exemple en faisant varier le débit de la vanne motrice et d'observer pour quel débit la cote de l'eau à l'amont de la vanne d'entrée croise la cote normale de fonctionnement.\n\nCette session propose d'utiliser le \"solveur multi-modules\" qui se charge d'effectuer une dichotomie afin de trouver le débit permettant d'obtenir la cote normale de fonctionnement au niveau de la cote de l'eau de la vanne d'entrée du canal d'amenée.\n",
    "session": [
        {
            "uid": "MHQxN3",
            "props": {
                "calcType": "ParallelStructure"
            },
            "meta": {
                "title": "Vanne motrice"
            },
            "children": [
                {
                    "uid": "cW1pZD",
                    "props": {
                        "calcType": "Structure",
                        "structureType": "SeuilRectangulaire",
                        "loiDebit": "WeirFree"
                    },
                    "children": [],
                    "parameters": [
                        {
                            "symbol": "ZDV",
                            "mode": "SINGLE",
                            "value": 393.99
                        },
                        {
                            "symbol": "L",
                            "mode": "SINGLE",
                            "value": 1.84
                        },
                        {
                            "symbol": "CdWR",
                            "mode": "SINGLE",
                            "value": 0.4
                        }
                    ]
                }
            ],
            "parameters": [
                {
                    "symbol": "Q",
                    "mode": "SINGLE",
                    "value": 1.143420537740305
                },
                {
                    "symbol": "Z1",
                    "mode": "CALCUL",
                    "value": 102
                },
                {
                    "symbol": "Z2",
                    "mode": "SINGLE",
                    "value": 393.99
                }
            ]
        },
        {
            "uid": "Mnk0Mj",
            "props": {
                "calcType": "Bief",
                "regime": "Fluvial"
            },
            "meta": {
                "title": "Canal d'amenée"
            },
            "children": [
                {
                    "uid": "ZWFod3",
                    "props": {
                        "regime": "Fluvial",
                        "calcType": "Section",
                        "nodeType": "SectionRectangle"
                    },
                    "children": [],
                    "parameters": [
                        {
                            "symbol": "Ks",
                            "mode": "SINGLE",
                            "value": 40
                        },
                        {
                            "symbol": "Q",
                            "mode": "LINK",
                            "targetNub": "MHQxN3",
                            "targetParam": "Q"
                        },
                        {
                            "symbol": "YB",
                            "mode": "SINGLE",
                            "value": 1
                        },
                        {
                            "symbol": "Y",
                            "mode": "SINGLE",
                            "value": 0.8
                        },
                        {
                            "symbol": "LargeurBerge",
                            "mode": "SINGLE",
                            "value": 2.3
                        }
                    ]
                }
            ],
            "parameters": [
                {
                    "symbol": "Long",
                    "mode": "SINGLE",
                    "value": 30
                },
                {
                    "symbol": "Dx",
                    "mode": "SINGLE",
                    "value": 1
                },
                {
                    "symbol": "Z1",
                    "mode": "CALCUL"
                },
                {
                    "symbol": "Z2",
                    "mode": "LINK",
                    "targetNub": "MHQxN3",
                    "targetParam": "Z1"
                },
                {
                    "symbol": "ZF1",
                    "mode": "SINGLE",
                    "value": 393.99
                },
                {
                    "symbol": "ZF2",
                    "mode": "SINGLE",
                    "value": 394.1
                }
            ]
        },
        {
            "uid": "bmlreT",
            "props": {
                "calcType": "ParallelStructure"
            },
            "meta": {
                "title": "Vanne à l'entrée du canal"
            },
            "children": [
                {
                    "uid": "cnN3Y2",
                    "props": {
                        "calcType": "Structure",
                        "structureType": "SeuilRectangulaire",
                        "loiDebit": "WeirSubmerged"
                    },
                    "children": [],
                    "parameters": [
                        {
                            "symbol": "ZDV",
                            "mode": "SINGLE",
                            "value": 393.99
                        },
                        {
                            "symbol": "L",
                            "mode": "SINGLE",
                            "value": 2.8
                        },
                        {
                            "symbol": "CdWR",
                            "mode": "SINGLE",
                            "value": 0.4
                        }
                    ]
                }
            ],
            "parameters": [
                {
                    "symbol": "Q",
                    "mode": "LINK",
                    "targetNub": "MHQxN3",
                    "targetParam": "Q"
                },
                {
                    "symbol": "Z1",
                    "mode": "CALCUL",
                    "value": 102
                },
                {
                    "symbol": "Z2",
                    "mode": "LINK",
                    "targetNub": "Mnk0Mj",
                    "targetParam": "Z1"
                }
            ]
        },
        {
            "uid": "ZDM5OT",
            "props": {
                "calcType": "Solveur",
                "nubToCalculate": "bmlreT",
                "searchedParameter": "MHQxN3/Q",
                "targettedResult": ""
            },
            "meta": {
                "title": "Calcul du débit à partir de la cote de fonctionnement"
            },
            "children": [],
            "parameters": [
                {
                    "symbol": "Xinit",
                    "mode": "SINGLE",
                    "value": 0.5
                },
                {
                    "symbol": "Ytarget",
                    "mode": "SINGLE",
                    "value": 394.66
                }
            ]
        }
    ]
}