<?php 

function do_log($msg) {
 // echo($msg."\n");
}

do_log("start");

// http://localhost/gofetch.php?url=http%3A%2F%2Flocalhost%3A4200%2Fapp%2F%2Fexamples%2Fperr.json
// http://localhost/gofetch.php?url=https%3A%2F%2Fhydraulique.g-eau.fr%2Fcassiopee%2Fdevel%2Fapp%2F%2Fexamples%2Fperr.json

// fonction str_ends_with si PHP < 8
if ( ! function_exists( 'str_ends_with' ) ) {
    function str_ends_with( $haystack, $needle ) {
        if ( '' === $haystack && '' !== $needle ) {
            return false;
        }
        $len = strlen( $needle );
        return 0 === substr_compare( $haystack, $needle, -$len, $len );
    }
}

do_log("get url");
$url = $_GET['url'];
do_log("url=" . $url);

$url=urldecode($url);
do_log("decode url=" . $url);

if( str_ends_with( strtolower( $url ), '.json' ) )  {

do_log("curl init");
// Initialise une session CURL.
$ch = curl_init();

do_log("setopt 1");
// Récupère le contenu de la page
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

do_log("setopt 2");
// Configure l'URL
curl_setopt($ch, CURLOPT_URL, $url);

// Désactive la vérification du certificat si l'URL utilise HTTPS
if (strpos($url,'https')===0) {
  do_log("setopt 3");
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
}

do_log("curl exec");
// Exécute la requête 
$result = curl_exec($ch);

// Affiche le résultat
echo $result;
}
?>
