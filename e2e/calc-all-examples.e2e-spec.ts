import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { SideNav } from "./sidenav.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { newSession } from "./util.po";

/**
 * Calculate all modules of all examples
 */
describe("ngHyd − example sessions −", () => {

    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let prefPage: PreferencesPage;
    let sideNav: SideNav;

    beforeAll(() => {
        // increase timeout to avoid "Error: Timeout - Async callback was not invoked within timeout specified by jasmine.DEFAULT_TIMEOUT_INTERVAL" message
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10 * 60 * 1000; // 10 min

        calcPage = new CalculatorPage();
        navBar = new Navbar();
        prefPage = new PreferencesPage();
        sideNav = new SideNav();
    });

    it("calcul de tous les modules de tous les exemples −", async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);

        // for each calculator
        let lastExampleFound = true;
        let i = 0;
        while (lastExampleFound) {
            if (i == 0) {
                // start page
                await navBar.clickNewCalculatorButton();
                await browser.pause(200);
            }
            else {
                // empty session
                await newSession(navBar, sideNav);
            }

            const examples = await $$("#examples-list .load-example");
            await browser.pause(200);
            if (examples.length > i) {
                // click example #i
                await examples[i].click();
                await browser.pause(200);

                const nbModules = await navBar.getCalculatorEntriesCount();
                await browser.pause(200);
                for (let j = 0; j < nbModules; j++) {
                    // select module
                    await navBar.openNthCalculator(j);
                    await browser.pause(300);
                    await calcPage.closeSnackBars(5, 500); // opening a calculator can trigger a snackbar with "results invalidated for..."
                    await browser.pause(500);
                    // calculate module
                    const calcBtn = await calcPage.getCalculateButton();
                    await calcBtn.click();
                    await browser.pause(300);
                    // check results
                    const hasResults = await calcPage.hasValidResults();
                    expect(hasResults).toBe(true, `example ${i + 1}, module ${j + 1} (starting at 1)`);
                }
            } else {
                // no more examples
                lastExampleFound = false;
            }
            i++;
        }
    });
});
