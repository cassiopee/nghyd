import { changeSelectValue } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

export class PreferencesPage {
    navigateTo() {
        return browser.url("/#/setup");
    }

    getHeader1() {
        return $("h1");
    }

    getLanguageSelect() {
        return $(`mat-select[data-testid="language-select"]`);
    }

    getEmptyFieldsCheckbox() {
        return $(`mat-checkbox#cb_emptyFields`);
    }

    getInputLabels() {
        return $$("label.mat-form-field-label");
    }

    getNumericFormFields() {
        return $$(`mat-form-field[data-testclass="numeric-input"]`);
    }

    getInputForField(ff) {
        return ff.$("input");
    }

    /**
     * find input with given "name" attribute
     */
    getInputFromName(name: string) {
        const cssSelector: string = `input[name="${name}"]`;
        return $(cssSelector);
    }

    getErrorsForField(ff) {
        return ff.$("mat-error");
    }

    async changeLanguage(index: number) {
        const select = await this.getLanguageSelect();
        await changeSelectValue(select, index);
    }

    async enableEvilEmptyFields() {
        const cb = await this.getEmptyFieldsCheckbox();
        const underlyingCB = await cb.$(`input.mat-checkbox-input`);
        if (!await underlyingCB.isSelected()) {
            await cb.click();
        }
    }

    async disableEvilEmptyFields() {
        const cb = await this.getEmptyFieldsCheckbox();
        const underlyingCB = await cb.$(`input.mat-checkbox-input`);
        if (await underlyingCB.isSelected()) {
            await cb.click();
        }
    }

    /**
     * enable/disable option "empty fields on module creation"
     * @param b true to enable "empty fields on module creation" option, false to disable it (fill fields with default values)
     */
    async setEmptyFields(b: boolean) {
        await this.navigateTo();
        await browser.pause(1000); // 1000 is necessary to avoid "element click intercepted" warning

        if (b) {
            await this.enableEvilEmptyFields();
        }
        else {
            await this.disableEvilEmptyFields();
        }
        await browser.pause(200);
    }

    async setIterationCount(n: number) {
        const input = await this.getInputFromName("nmi");
        await input.setValue(n.toString());
    }

    async setComputePrecision(p: number) {
        const input = await this.getInputFromName("cp");
        await input.setValue(p.toString());
    }
}
