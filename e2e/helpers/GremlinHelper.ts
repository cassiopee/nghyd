// inspired from https://medium.com/ranorex-webtestit/unleash-the-horde-with-ranorex-webtestit-how-to-automate-monkey-testing-6580a9d9e0a1

import { readFileSync } from "fs";

export function getFpsResults(results) {
    return [
        ...results.log.filter(findFpsMessages),
        ...results.info.filter(findFpsMessages),
        ...results.warn.filter(findFpsMessages),
        ...results.error.filter(findFpsMessages)
    ].map((r) => r.pop());
}

function findFpsMessages(log: any[]) {
    return log.find ? log.find((l: string) => {
        return l.toString().startsWith("fps");
    }) : null;
}

export function readGremlinsScript(): string {
    return readFileSync(__dirname + "/gremlins.min.js", "utf-8");
}

export function unleashGremlins(callback: (args?: any) => void) {

    // OPTIONS
    // const seed = Math.floor(Math.random() * 1000000);
    const seed = 12345; // doesn't work :/
    const maxExecutionTime = 30 * 1000;
    const delay = 30;
    const maxErrors = 10;

    // logs holder
    const logs = {
        log: [],
        info: [],
        warn: [],
        error: []
    };

    if (!(window as any).gremlins) {
        callback(logs);
    }

    // stop conditions
    const stop = () => {
        horde.stop();
        callback(logs);
    };
    window.onbeforeunload = stop;
    // maximum execution time
    setTimeout(stop, maxExecutionTime);

    // init horde
    const Gremlins = (window as any).gremlins;
    const horde = Gremlins.createHorde();

    logs.info.push("Using seed: " + seed);
    // configure horde
    horde
        .seed(seed) // doesn't work :/

        // enhanced clicker
        .gremlin(Gremlins.species.clicker()
            .clickTypes(["click"])
            // option "touche pas à ça p'tit con !" : évite de sortir du logiciel en cliquant
            // sur un lien target="_blank" (ou un élément enfant d'un tel lien) ou une icône
            // d'aide, ou de rester coincé en cliquant sur un bouton "choisir un fichier"
            .canClick((element: HTMLElement) => {
                // 1. lien <a> ?
                let aElt: HTMLElement;
                if (element.tagName === "A") {
                    aElt = element;
                } else if (element.parentElement && element.parentElement.tagName === "A") {
                    aElt = element.parentElement;
                }
                if (aElt) {
                    if (aElt.getAttribute("target") === "_blank") {
                        return false;
                    }
                } else {
                    // 2. <mat-icon> d'aide ?
                    if (
                        element.tagName === "MAT-ICON"
                        && ["help-calc", "help-fieldset", "help-fieldset-container", "help-input", "help-select"].includes(element.id)
                    ) {
                        return false;
                    } else {
                        // 3. file input ?
                        let isFileInput = false;
                        let curElem = element;
                        while (!isFileInput && curElem) {
                            isFileInput = (
                                curElem.tagName === "MAT-FORM-FIELD"
                                && curElem.classList.contains("file-input-field")
                            );
                            curElem = curElem.parentElement;
                        }
                        if (isFileInput) {
                            // logs.info.push(">> Oh le file input très vilain !");
                            return false;
                        }
                    }
                }
                return true;
            })
            // detect modals and close them asap
            .positionSelector(() => {
                const modals = document.getElementsByTagName("mat-dialog-container");
                if (modals.length > 0) {
                    const modal = modals.item(0);
                    // 10% chance to click on of the modal's active buttons (usually "cancel", at least)
                    if (Math.floor(Math.random() * 10) === 5) {
                        const activeButtons = modal.querySelectorAll("button:not([disabled])");
                        if (activeButtons.length > 0) {
                            const element = activeButtons.item(Math.floor(Math.random() * activeButtons.length));
                            const pos = getElementCenter(element);
                            return pos;
                        }
                    } else {
                        // 90% of the time, click inside the modal randomly
                        return getRandomPositionInsideElement(modal, true);
                    }
                }
                // default random position
                return [
                    Math.floor(Math.random() * document.documentElement.clientWidth - 1),
                    Math.floor(Math.random() * document.documentElement.clientHeight - 1)
                ];
            })
        )

        .gremlin(Gremlins.species.typer())

        .gremlin(Gremlins.species.scroller()) // too many scrolls make clicks fail

        // enhanced form filler for Angular-Material
        // @WARNING disabled because provokes "stale reference" errors !
        .gremlin(Gremlins.species.formFiller()
            .elementMapTypes({
                // tslint:disable-next-line:quotemark
                'input[type="text"]': function fillTextElement(element: HTMLInputElement) {
                    // only send numbers in form fields
                    const num = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"][Math.floor(Math.random() * 10)];
                    element.value += num;
                    element.dispatchEvent(new Event("input")); // so that the model is updated @TODO check that it works
                    // @TODO wait before returning, needs async/await
                    return num;
                }
            })
            .canFillElement((element: HTMLElement) => {
                /* if (element.id.substr(0, 5) !== "calc_") {
                    logs.info.push("(FF) >> " + element.tagName + " / " + element.id);
                    return true;
                } */
                return false; // makes the gremlin do nothing
            }) // debug
        )
        .gremlin(Gremlins.species.toucher()) // OK
        // custom gremlin : click side menu, then an entry of the menu
        .gremlin(function () {
            clickElementCenter("#open-menu");
            const menuLinks = document.querySelectorAll("mat-sidenav .links-container > a");
            const idx = Math.floor(Math.random() * menuLinks.length);
            const link = menuLinks.item(idx);
            // exclude links that would make us leave the app
            if (!["side-nav-help", "side-nav-bug-report"].includes(link.id)) {
                doClickElementCenter(link);
                logs.log.push([
                    "gremlin",
                    "customClicker",
                    "clickMenuItem",
                    "on",
                    link.id
                ]);
            }
        })
        // custom gremlin : click new module button
        .gremlin(function () {
            clickElementCenter("#new-calculator");
        })
        // custom gremlin : click calculate button
        .gremlin(function () {
            clickElementCenter("#trigger-calculate");
        })

        .mogwai(Gremlins.mogwais.alert())
        .mogwai(Gremlins.mogwais.fps())
        .mogwai(Gremlins.mogwais.gizmo()
            .maxErrors(maxErrors) // default: 10
        )

        .strategy(Gremlins.strategies.distribution()
            .delay(delay) // default: 10
            // .nb(100) // default: 100
            .distribution([0.62, 0.05, 0.05, 0.1, 0.02, 0.03, 0.03, 0.1])
            // .distribution([ 0.72, 0.05, 0.05, 0.02, 0.03, 0.03, 0.1 ]) // same as above, without formFiller
            // .distribution([ 0.5, 0, 0, 0, 0, 0.5, 0, 0 ]) // only "open menu" and "click"
        )

        .logger({
            log: (...args: any) => { logs.log.push(args); },
            info: (...args: any) => { logs.info.push(args); },
            warn: (...args: any) => { logs.warn.push(args); },
            error: (...args: any) => { logs.error.push(args); }
        })
        .after(() => {
            // clearInterval(stopRefreshes);
            callback(logs);
        })
        // go !
        .unleash({}, () => {
            callback(logs);
        });

    // inner helper function for custom clicker gremlins
    function clickElementCenter(selector: string) {
        const targetElement = document.querySelector(selector);
        if (doClickElementCenter(targetElement)) {
            logs.log.push([
                "gremlin",
                "customClicker",
                "click",
                "on",
                selector
            ]);
        }
    }

    function doClickElementCenter(targetElement: Element): boolean {
        if (targetElement) {
            const evt = document.createEvent("MouseEvents");
            const [posX, posY] = getElementCenter(targetElement);
            evt.initMouseEvent("click", true, true, window, 0, 0, 0, posX, posY, false, false, false, false, 0, null);
            targetElement.dispatchEvent(evt);
            return true;
        }
        return false;
    }

    function getElementCenter(element: Element) {
        const rect = element.getBoundingClientRect();
        const posX = rect.left + window.scrollX + (rect.width / 2);
        const posY = rect.top + window.scrollY + (rect.height / 2);
        return [posX, posY];
    }

    function getRandomPositionInsideElement(element: Element, modal: boolean = false) {
        const rect = element.getBoundingClientRect();
        let minX = rect.left;
        if (!modal) {
            minX += window.scrollX;
        }
        let minY = rect.top;
        if (!modal) {
            minY += window.scrollY;
        }
        const posX = minX + Math.floor(Math.random() * rect.width);
        const posY = minY + Math.floor(Math.random() * rect.height);
        return [posX, posY];
    }
}
