import { CalculatorPage } from "./calculator.po";
import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

describe("check the select default value - ", () => {
    let prefPage: PreferencesPage;
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);

        await prefPage.changeLanguage(1); // fr
        await browser.pause(200);
    });

    it("in the 'backwater curve' calculator", async () => {
        // open backwater curve calculator
        await openCalculator(4, navBar, listPage);

        // in the calculator configuration file, the default resolution method is 'Trapezes'.
        // let's check this...
        const sel = await calcPage.getSelectById("select_resolution");
        const val = await calcPage.getSelectValueText(sel);
        expect(val).toBe("Intégration par trapèzes");
    });

    it("in the 'up/downstream elevations of a reach' calculator", async () => {
        // open "up/downstream elevations of a reach" calculator
        await openCalculator(21, navBar, listPage);

        // in the calculator configuration file, the default section method is 'Rectangulaire'.
        // let's check this...
        const sel = await calcPage.getSelectById("select_section");
        const val = await calcPage.getSelectValueText(sel);
        expect(val).toBe("Rectangulaire");
    });
});
