import { CalculatorPage } from "./calculator.po";
import { ListPage } from "./list.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { newSession, openApplication, openCalculator } from "./util.po";
import { Navbar } from "./navbar.po";
import { AppPage } from "./app.po";
import { SideNav } from "./sidenav.po";

/**
 * Check that a cancel button is present in min/max/list edition dialog
 * for variable parameters
 */
describe("ngHyd - check cancel button for variable parameters - ", () => {
    let startPage: AppPage;
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let sideNav: SideNav;

    beforeAll(() => {
        startPage = new AppPage();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        sideNav = new SideNav();
    });

    it("when min/max/list values dialog opens, a cancel button should be present", async () => {
        await openApplication(startPage);
        await newSession(navBar, sideNav);

        // open PAB chute
        await openCalculator(12, navBar, listPage);

        // click "var" radio on Z1 parameter
        const z1btn = await calcPage.getInputRadioButtonFromId("Z1", "var");
        await z1btn.click();
        await browser.pause(300);

        // cancel button presence
        const cancelbtn = await $("#btn-cancel");
        expect((await cancelbtn.isExisting()) && (await cancelbtn.isDisplayed())).toEqual(true);
    });
});
