import { CalculatorPage } from "./calculator.po";
import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po"
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

/**
 * check that fields are empty on creation
 */
describe("ngHyd − check that predam fields are empty", () => {
    let listPage: ListPage;
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
    });

    beforeEach(async () => {
        // enable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.enableEvilEmptyFields();
        await browser.pause(200);
    });

    it("on creation", async () => {
        // open predam calculator
        await openCalculator(30, navBar, listPage);

        // check upstream inputs
        await calcPage.checkEmptyInput("Q");
        // Z1 is calculated
        await calcPage.checkEmptyInput("Z2");

        // check basin 1 inputs
        let node = await $("g.node.basin");
        await node.click();
        await browser.pause(200);
        await calcPage.checkEmptyInput("0_S");
        await calcPage.checkEmptyInput("0_ZF");

        // check walls inputs
        const walls = await $$("g.node.wall");
        expect(walls.length).toEqual(2);
        for (const w of walls) {
            await w.click();
            await browser.pause(200);
            await calcPage.checkEmptyInput("0_ZDV");
            await calcPage.checkEmptyInput("0_L");
        };

        // check downstream basin inputs
        node = await $("g[id^='flowchart-aval-']"); // Mermaid generated id
        await node.click();
        await browser.pause(200);
        await calcPage.checkEmptyInput("Q");
        // Z1 is calculated
        await calcPage.checkEmptyInput("Z2");
    });

    it("when a basin is added", async () => {
        // open predam calculator
        await openCalculator(30, navBar, listPage);

        // add basin
        const addBasinBtn = await $("#add-basin");
        await addBasinBtn.click();
        await browser.pause(200);

        // check "surface" input is empty
        let inp = await calcPage.getInputById("3_S");
        let txt = await inp.getValue();
        expect(txt).toEqual("");

        // check "cote de fond" input is empty
        inp = await calcPage.getInputById("3_ZF");
        txt = await inp.getValue();
        expect(txt).toEqual("");
    });

    it("when a wall is added", async () => {
        // open predam calculator
        await openCalculator(30, navBar, listPage);

        // add wall
        const addWallBtn = await $("#add-wall");
        await addWallBtn.click();

        // connect basins
        const connectBasinsBtn = await $("#validate-connect-basins");
        await connectBasinsBtn.click();

        // check ZDV input is empty
        let inp = await calcPage.getInputById("0_ZDV");
        let txt = await inp.getValue();
        expect(txt).toEqual("");

        // check L input is empty
        inp = await calcPage.getInputById("0_L");
        txt = await inp.getValue();
        expect(txt).toEqual("");

        // check CdWSL input is empty (in this case, the structure happens to be a Larinier weir
        // which discharge coefficient must be empty)
        inp = await calcPage.getInputById("0_CdWSL");
        txt = await inp.getValue();
        expect(txt).toEqual("");
    });
});
