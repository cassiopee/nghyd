import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { CalculatorPage } from "./calculator.po";
import { PreferencesPage } from "./preferences.po";
import { AppPage } from "./app.po";
import { loadSession, openCalculator } from "./util.po";
import { SideNav } from "./sidenav.po";
import { browser, $, $$, expect } from '@wdio/globals'

describe("Parallel structures - ", () => {
        let startPage: AppPage;
        let listPage: ListPage;
        let navBar: Navbar;
        let calcPage: CalculatorPage;
        let prefPage: PreferencesPage;
        let sideNav: SideNav;

        beforeAll(() => {
                startPage = new AppPage();
                listPage = new ListPage();
                navBar = new Navbar();
                calcPage = new CalculatorPage();
                prefPage = new PreferencesPage();
                sideNav = new SideNav();
        });

        beforeEach(async () => {
                // disable evil option "empty fields on module creation"
                await prefPage.navigateTo();
                await prefPage.disableEvilEmptyFields();
                await browser.pause(200);
        });

        it("check calculated parameter remains the same when copying a structure", async () => {
                // open "fish ladder: cross walls" calculator
                await openCalculator(10, navBar, listPage);

                // check L in first structure calc toggle is not checked
                const L1 = await calcPage.getInputById("0_L");

                // const h11 = calcPage.getInputById("0_h1");

                // set L to calculated in first structure
                await calcPage.setParamMode(L1, "cal");
                await browser.pause(500);

                // check L calc toggle is  checked
                expect(await calcPage.inputIsInCalculatedMode(L1)).toBe(true);

                // copy 1st structure
                const copyStruct = await calcPage.getCopyStructureButton();

                await browser.execute(e => { e.click() }, copyStruct); // await copyStruct.click() fails with "element not interactable" error
                await browser.pause(200);

                await calcPage.closeSnackBar();
                await browser.pause(200);

                // check L in first structure is still in "calc" state
                expect(await calcPage.inputIsInCalculatedMode(L1)).toBe(true);

                // // check L in second structure is still in "fix" state
                const L2 = await calcPage.getInputById("1_L");
                expect(await calcPage.inputIsInFixedMode(L2)).toBe(true);
        });

        it("check submergence error", async () => {
                await startPage.navigateTo();
                await browser.pause(200);

                await loadSession(navBar, sideNav, "./session/session-erreur-ennoiement-614.json");
                await browser.pause(1000);

                // first calculator

                await navBar.clickCalculatorTab(0);
                await browser.pause(200);

                // run calculation
                let btnCalc = await calcPage.getCalculateButton();
                await btnCalc.click();
                await browser.pause(500);

                // check error message in log
                expect(await calcPage.nbLogEntries()).toBe(2);

                expect(await calcPage.nthLogEntryIsError(0)).toBe(true);
                expect(await calcPage.nthLogEntryIsWarning(1)).toBe(true);

                // second calculator

                await navBar.clickCalculatorTab(1);
                await browser.pause(200);

                // run calculation
                btnCalc = await calcPage.getCalculateButton();
                await btnCalc.click();
                await browser.pause(500);

                // check error message in log
                expect(await calcPage.nbLogEntries()).toBe(2);

                expect(await calcPage.nthLogEntryIsError(0)).toBe(true);
                expect(await calcPage.nthLogEntryIsWarning(1)).toBe(true);
        });
});
