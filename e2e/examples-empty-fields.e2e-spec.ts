import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po"
import { SideNav } from "./sidenav.po";
import { changeSelectValue, newSession } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * check that fields are empty on creation
 */
describe("ngHyd - Check that examples fields are not empty with 'empty fields on calculator creation' is enabled - ", () => {
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;
    let sideNav: SideNav;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
        sideNav = new SideNav();
    });

    beforeEach(async () => {
        // enable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.enableEvilEmptyFields();
        await browser.pause(200);
    });

    it("when a standard fish ladder calculator is created", async () => {
        // start page
        await navBar.clickNewCalculatorButton();
        await browser.pause(200);

        // open 1st example
        const examples = await $$("#examples-list .load-example");
        await examples[0].click();
        await browser.pause(50);

        // select wall module
        await navBar.openNthCalculator(4);
        await browser.pause(50);

        // check fields are not empty
        const inputIds = ["Z1", "LB", "PB", "0_L", "0_CdWSL"];
        const emptys = [false, false, false, false, false];
        await calcPage.checkEmptyOrFilledFields(inputIds, emptys);
    });

    it("calculated parameter initial value when discharge law is modified", async () => {
        await newSession(navBar, sideNav);

        // open 1st example
        const examples = await $$("#examples-list .load-example");
        await examples[0].click();
        await browser.pause(50);

        // select wall module
        await navBar.openNthCalculator(4);
        await browser.pause(50);

        // modify 1st structure discharge law
        const dischargeSelect = await calcPage.getSelectById("select_loidebit");
        await changeSelectValue(dischargeSelect, 1);
        await browser.pause(200);

        // open initial dialog
        const initDlgButton = await $(".param-computed-more");
        await initDlgButton.click();
        await browser.pause(200);

        // check input value is not null
        const underlyingInput = await $("ngparam-input input.form-control[id='0_h1']");
        const txt = await underlyingInput.getValue();
        expect(txt === "").toEqual(false);
    });
});

describe("ngHyd - Check that examples work with 'empty fields on calculator creation' enabled - ", () => {
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;
    let sideNav: SideNav;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
        sideNav = new SideNav();
    });

    beforeEach(async () => {
        // enable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.enableEvilEmptyFields();
        await browser.pause(200);
    });

    it("when calculation is run on a generated fish ladder calculator", async () => {
        await newSession(navBar, sideNav);

        // open 1st example
        const examples = await $$("#examples-list .load-example");
        await examples[0].click();
        await browser.pause(500);

        // select wall module
        await navBar.openNthCalculator(4);
        await browser.pause(200);

        // run calculation
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(500);

        // click "generate PAB" button
        const genButton = await calcPage.getGeneratePabButton();
        await genButton.click();
        await browser.pause(2000);

        // write "6" in basin count input
        const nbBassins = await calcPage.getInputById("generatePabNbBassins");
        await nbBassins.setValue("6");
        await browser.pause(50);

        // click "Generate PAB"
        await $("dialog-generate-pab button#do-generate").click();
        await browser.pause(1000);

        // calculate PAB
        const calcButtonPAB = await calcPage.getCalculateButton();
        await calcButtonPAB.click();
        await browser.pause(200);

        // check that result is not empty
        const hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(true);
    });
});
