import { AppPage } from "./app.po";
import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { SideNav } from "./sidenav.po";
import { PreferencesPage } from "./preferences.po";
import { changeSelectValue, expectNumber, loadSession, openCalculator } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

const fs = require("fs");
const path = require("path");
const os = require("os");

let startPage: AppPage;
let listPage: ListPage;
let calcPage: CalculatorPage;
let navBar: Navbar;
let sidenav: SideNav;
let prefPage: PreferencesPage;

function findDownloadedFile(filename: string): string {
    const downloadDirs = ["Téléchargements", "Downloads", "/tmp", "."];
    for (const d of downloadDirs) {
        const download_prefix = (d.charAt(0) === "/" || (d.length === 1 && d.charAt(0) === ".")) ? d : path.resolve(os.homedir(), d);
        const fp = path.resolve(download_prefix, filename);
        if (fs.existsSync(fp)) {
            return fp;
        }
    }
}

function deleteDownloadedFile(filename: string) {
    // see: https://stackoverflow.com/questions/21935696/protractor-e2e-test-case-for-downloading-pdf-file
    const fp = findDownloadedFile(filename);
    if (fp !== undefined) {
        // Make sure the browser doesn't have to rename the download.
        fs.unlinkSync(fp);
    }
}

async function saveSession(): Promise<string> {
    const sessionFile = "session-e2e-tests.json";
    deleteDownloadedFile(sessionFile);

    await calcPage.clickSaveCalcButton();
    await browser.pause(300);

    const inpName = await $("dialog-save-session input.mat-input-element");
    await inpName.setValue(sessionFile);

    await calcPage.getSaveSessionButton().click();
    await browser.pause(500);

    return findDownloadedFile(sessionFile);
}

/**
 * Save and load (serialise and unserialise) calculators to/from JSON files
 */
describe("ngHyd − save and load sessions", () => {
    beforeAll(() => {
        startPage = new AppPage();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        sidenav = new SideNav();
        prefPage = new PreferencesPage();
    });

    beforeEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 45 * 60 * 1000; // 45 min
    });

    it("when loading session-6-calc.test.json file from home page, 6 calculators should be loaded", async () => {
        await startPage.navigateTo();

        await loadSession(navBar, sidenav, "./session/session-6-calc.test.json");
        await browser.pause(1000);

        expect(await navBar.getAllCalculatorTabs().length).toBe(6);
    });

    it("when loading session-optional-params.test.json file from home page, the calculator should be loaded", async () => {
        await startPage.navigateTo();

        await loadSession(navBar, sidenav, "./session/session-optional-params.test.json");
        await browser.pause(200);

        expect(await navBar.getAllCalculatorTabs().length).toBe(1);
    });

    it("when saving a calculator, the current parameter values should be found in the file", async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);

        await openCalculator(2, navBar, listPage); // Section paramétrée

        await changeSelectValue(await calcPage.getSelectById("select_section"), 2); // mode "trapezoidal"

        const inpKs = await calcPage.getInputById("Ks");
        await inpKs.setValue("42");

        const filename = await saveSession();
        await browser.pause(500);
        const fileContent = fs.readFileSync(filename, { encoding: "utf8" });
        await browser.pause(200);

        expect(fileContent).toContain(`"nodeType":"SectionTrapeze"`);
        expect(fileContent).toContain(`{"symbol":"Ks","mode":"SINGLE","value":42}`);

        deleteDownloadedFile(filename);
    });

    it("select value must be recovered when loading a session file", async () => {
        // start page
        await startPage.navigateTo();
        await browser.pause(200);

        const calcTypes = await listPage.getAvailableCalcTypes();

        const excludedCalculators = [
            34, // vérificateur (nécessite d'ouvrir plusieurs calculettes)
            35 // pressureloss : le select de loi de perte de charge détermine la présence d'un 2ème select (Lechapt -> matériau) (non géré pour l'instant)
        ];

        for (let i = 0; i < calcTypes.length; i++) {
            const ct = calcTypes[i];
            if (!excludedCalculators.includes(ct)) {
                if (i == 0) {
                    // enable evil option "empty fields on module creation"
                    await prefPage.navigateTo();
                    await prefPage.disableEvilEmptyFields();
                    await browser.pause(200);

                    // // start page
                    // await navBar.clickNewCalculatorButton();
                    // await browser.pause(200);
                }
                else {
                    // empty session
                    await navBar.clickMenuButton();
                    await browser.pause(500);
                    await sidenav.clickNewSessionButton();
                    await browser.pause(200);
                }

                // open calculator
                await openCalculator(ct, navBar, listPage);

                // detect selects
                const nsel = await calcPage.getCalculatorSelectCount();
                for (let s = 0; s < nsel; s++) {
                    const sel = await calcPage.getAllCalculatorSelects()[s];
                    const selId = await sel.getAttribute("id");

                    const options = await calcPage.getMatselectOptionsText(sel);
                    const optionCount = options.length;

                    if (optionCount > 1) { // we need at least 2 options to be able to modify select
                        // index of current selected option
                        const optTxt = await calcPage.getMatselectCurrentOptionText(sel);
                        const ind = options.indexOf(optTxt);

                        // select next select option (optionally looping)
                        const nextInd = (ind + 1) % optionCount;
                        await changeSelectValue(sel, nextInd);
                        await browser.pause(200);

                        // save session
                        const filename = await saveSession();
                        await browser.pause(500);

                        // load session
                        await loadSession(navBar, sidenav, filename);
                        await browser.pause(500);
                        // the displayed calculator is now the loaded one

                        // check the calculator has been loaded
                        expectNumber(`calc ${ct} select ${selId} : num calcs`, await navBar.getCalculatorEntriesCount(), 2);

                        // check the select in the loaded session points to the same option
                        const sel2 = await calcPage.getSelectById(selId);

                        // check the same option is in the select
                        const optTxt2 = await calcPage.getMatselectCurrentOptionText(sel2);
                        const ind2 = options.indexOf(optTxt2);
                        expectNumber(`calc ${ct} select ${selId} : opt '${optTxt2}' index`, ind2, nextInd);

                        // close last calculator (the loaded one)
                        await navBar.middleClickCalculatorTab(1);
                        await browser.pause(200);

                        // check last calculator has been closed
                        expectNumber(`calc ${ct} select ${selId} : num calcs(2)`, await navBar.getCalculatorEntriesCount(), 1);

                        deleteDownloadedFile(filename);

                        // get selects once again since page has been replaced (session load)
                        const selects = await calcPage.getAllCalculatorSelects();

                        // restore select previous option, other selects presence may depend on it (eg. pressure loss calculator)
                        await changeSelectValue(selects[s], ind);
                        await browser.pause(200);
                    }
                }
            }
        }
    });
});
