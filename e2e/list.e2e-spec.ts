import { ListPage } from "./list.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * Show calculators list (home page)
 */
describe("ngHyd − list page", () => {
    let page: ListPage;

    beforeAll(() => {
        page = new ListPage();
    });

    it("when list is open, user should see the list of available compute nodes", async () => {
        await page.navigateTo();
        expect(await page.getThemesCardsLength()).toBeGreaterThan(4);
        expect(await page.getCalculatorsMenuLength()).toBeGreaterThan(8);
    });

    it("when list is open, link to doc should be well-formed (2-letter language code)", async () => {
        await page.navigateTo();
        const docLink = await $("a#header-doc");
        const href = await docLink.getAttribute("href");
        const re = new RegExp("assets/docs/[a-z]{2}/index.html");
        expect(re.test(href)).toBe(true);
    });
});
