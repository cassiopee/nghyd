import { AppPage } from "./app.po";
import { Navbar } from "./navbar.po";
import { ListPage } from "./list.po";
import { SideNav } from "./sidenav.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { newSession, openApplication, openCalculator } from "./util.po";

describe("check calculator notes", () => {
    let startPage: AppPage;
    let navBar: Navbar;
    let listPage: ListPage;
    let sideNav: SideNav;

    beforeAll(() => {
        startPage = new AppPage();
        navBar = new Navbar();
        listPage = new ListPage();
        sideNav = new SideNav();
    });

    it(" - notes should display properly when opened from a calculator", async () => {
        await openApplication(startPage);
        await newSession(navBar, sideNav);

        // open PAB: chute calculator
        await openCalculator(12, navBar, listPage);

        // open notes
        await navBar.clickMenuButton();
        await browser.pause(500);
        await sideNav.clickNotesButton();
        await browser.pause(200);

        // input some text
        const ta = await $("textarea");
        await ta.clearValue();
        await ta.setValue("azerty123");
        await browser.pause(200);

        // reopen calculator
        await navBar.openNthCalculator(0);
        await browser.pause(200);

        // reopen notes
        await navBar.clickMenuButton();
        await browser.pause(200);
        await sideNav.clickNotesButton();
        await browser.pause(200);

        // check text
        const md = await $("markdown p");
        await browser.pause(200);
        expect(await md.getText()).toEqual("azerty123");
    });

    it(" - notes should display properly when opened from modules diagram", async () => {
        await openApplication(startPage);
        await newSession(navBar, sideNav);

        // open PAB: chute calculator
        await openCalculator(12, navBar, listPage);

        // open notes
        await navBar.clickMenuButton();
        await browser.pause(500);
        await sideNav.clickNotesButton();
        await browser.pause(200);

        // input some text
        const ta = await $("textarea");
        await ta.clearValue();
        await ta.setValue("azerty123");
        await browser.pause(200);

        // open modules diagram
        await navBar.clickMenuButton();
        await browser.pause(500);
        await sideNav.clickDiagramButton();
        await browser.pause(200);

        // open notes
        const notesLink = await $("#show-notes a");
        notesLink.click();
        await browser.pause(200);

        // check text
        const md = await $("markdown p");
        await browser.pause(200);
        expect(await md.getText()).toEqual("azerty123");
    });
});
