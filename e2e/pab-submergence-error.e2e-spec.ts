import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { AppPage } from "./app.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * Clone calculators
 */
describe("Passe à Bassins - ", () => {
    let startPage: AppPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        startPage = new AppPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        prefPage = new PreferencesPage();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.setEmptyFields(false);
    });

    it("check submergence error occurs", async () => {
        await startPage.navigateTo();
        await browser.pause(200);

        // open PAB example
        const examples = await $$("#examples-list .load-example");
        await examples[0].click();
        await browser.pause(500);

        // select PAB module
        await navBar.openNthCalculator(5);
        await browser.pause(300);

        // modify downstream water elevation
        const inpZ2 = await calcPage.getInputById("Z2");
        await inpZ2.setValue("27");
        await browser.pause(100);

        // calculate PAB
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(300);

        // check that result is empty
        const hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(false);

        // check presence of log messages
        expect(await calcPage.nbLogEntries()).toBe(3);
        expect(await calcPage.nthLogEntryIsError(0)).toBe(true);
        expect(await calcPage.nthLogEntryIsError(1)).toBe(true);
        expect(await calcPage.nthLogEntryIsWarning(2)).toBe(true);
    });
});
