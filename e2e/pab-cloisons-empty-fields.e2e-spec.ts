import { ListPage } from "./list.po";
import { PreferencesPage } from "./preferences.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

/**
 * enable evil option "empty fields on module creation"
 */
async function enableEmptyFieldsOption(prefPage: PreferencesPage) {
    await prefPage.navigateTo();
    await browser.pause(200);
    await prefPage.enableEvilEmptyFields();
    await browser.pause(200);
}

async function fillInput(calcPage: CalculatorPage, symbol: string) {
    const inp = await calcPage.getInputById(symbol);
    await inp.clearValue();
    await inp.setValue("1");
}

/**
 * Check that when "empty fields on calculator creation" is enabled
 * fields are empty in the "generate fish ladder" dialog of the
 * cross walls calculator
 */
describe("ngHyd - check the cross walls calculator has empty fields - ", () => {
    let prefPage: PreferencesPage;
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
    });

    beforeEach(async () => {
        await enableEmptyFieldsOption(prefPage);
    });

    it("in the 'generate fish ladder' dialog", async () => {
        // open cross walls calculator
        await openCalculator(10, navBar, listPage);

        // fill inputs
        await fillInput(calcPage, "Z1");
        await fillInput(calcPage, "LB");
        await fillInput(calcPage, "BB");
        await fillInput(calcPage, "PB");
        await fillInput(calcPage, "DH");
        await fillInput(calcPage, "0_h1");
        await fillInput(calcPage, "0_L");
        await fillInput(calcPage, "0_CdWSL");

        // calculate
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(200);

        // click "generate PAB" button
        const genButton = await calcPage.getGeneratePabButton();
        await genButton.click();
        await browser.pause(2000);

        await calcPage.checkEmptyOrFilledFields(["generatePabNbBassins"], [true]);
    });
});

/**
 * Check that when "empty fields on calculator creation" is enabled
 * fields are not empty after calculation
 */
describe("ngHyd - check the cross walls calculator has no empty field - ", () => {
    let prefPage: PreferencesPage;
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
    });

    beforeEach(async () => {
        await enableEmptyFieldsOption(prefPage);
    });

    it("after calculation", async () => {
        // open cross walls calculator
        await openCalculator(10, navBar, listPage);

        // fill inputs
        await fillInput(calcPage, "Z1");
        await fillInput(calcPage, "LB");
        await fillInput(calcPage, "BB");
        await fillInput(calcPage, "PB");
        await fillInput(calcPage, "DH");
        await fillInput(calcPage, "0_h1");
        await fillInput(calcPage, "0_L");
        await fillInput(calcPage, "0_CdWSL");

        // calculate
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(200);

        await calcPage.checkEmptyOrFilledFields(["Z1", "LB", "BB", "PB", "DH", "0_h1", "0_L", "0_CdWSL"], [false, false, false, false, false, false, false, false]);
    });
});
