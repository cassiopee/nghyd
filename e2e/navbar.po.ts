import { browser, $, $$, expect } from '@wdio/globals'
import { Key } from 'webdriverio'
import { scrollToElement } from './util.po';

export class Navbar {
    getAllCalculatorTabs() {
        return $$("#tabs-container button.calculator-button");
    }

    /**
     * Robust method that returns number of calculator entries,
     * whether they are buttons or dropdown select options
     */
    async getCalculatorEntriesCount() {
        // if dropDown calculators select is visible
        const dropDown = await $("mat-select#selectCalculator");
        if ((await dropDown.isExisting()) && (await dropDown.isDisplayed())) {
            await dropDown.click();
            browser.pause(100);
            const options = await $$(".cdk-overlay-container mat-option");
            await browser.keys(Key.Escape); // close dropdown
            return options.length;
        } else {
            return (await this.getAllCalculatorTabs()).length;
        }
    }

    /**
     * Robust method that opens the nth calculator, whether the
     * calculator selector is a list of buttons or a dropdown select
     */
    async openNthCalculator(n: number) {
        // if dropDown calculators select is visible
        const dropDown = await $("mat-select#selectCalculator");
        if ((await dropDown.isExisting()) && (await dropDown.isDisplayed())) {
            await dropDown.click();

            // 1st option is not necessarly "mat-option-0"...
            const options = await $$(".cdk-overlay-container mat-option");
            const option = options[n];

            await option.click();
        } else {
            const tabs = await this.getAllCalculatorTabs();
            await tabs[n].click();
        }
    }

    getCalculatorTabForUid(uid: string) {
        return $("#tabs-container button.calculator-button.calculator-uid-" + uid);
    }

    getNewCalculatorButton() {
        return $("#new-calculator");
    }

    getMenuButton() {
        return $("#open-menu");
    }

    async clickCalculatorTab(n: number) {
        const tabs = await this.getAllCalculatorTabs();
        await browser.execute(e => { e.click() }, tabs[n]); // await tabs[n].click() fails with "element not interactable" error
    }

    async clickCalculatorTabForUid(uid: string) {
        const tab = this.getCalculatorTabForUid(uid);
        await tab.click();
    }

    async clickRandomCalculatorTab(n: number) {
        const tabs = await this.getAllCalculatorTabs();
        const l = tabs.length;
        const r = Math.min((Math.floor(Math.random() * l)), (l - 1));

        await browser.execute(e => { e.click() }, tabs[r]); // await tabs[r].click() fails with "element not interactable" error
    }

    /**
     * close nth calculator by clicking with middle mouse button
     * @param confirmCloseDialog true to confirm opening dialog and indeed close calculator
     */
    async middleClickCalculatorTab(n: number, confirmCloseDialog: boolean = true) {
        const calcTabs = await this.getAllCalculatorTabs();
        await calcTabs[n].click({ button: 'middle' });
        if (confirmCloseDialog) {
            const btns = await $$("dialog-confirm-close-calc .mat-dialog-actions button");
            await btns[1].click();
        }
    }

    async clickNewCalculatorButton() {
        const ncb = this.getNewCalculatorButton();
        await ncb.click();
    }

    async clickMenuButton() {
        const ncb = await this.getMenuButton();
        await ncb.click();
    }
}
