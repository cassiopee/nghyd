import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { testedCalcTypes } from "./tested_calctypes";
import { openCalculator, scrollPageToTop } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { newSession } from "./util.po";
import { SideNav } from "./sidenav.po";

/**
 * For all calculators, try to calculate every parameter: check that only one parameter
 * is set to CAL mode, trigger the calculation, check that result is not empty
 */
describe("ngHyd − calculate all parameters of all calculators", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let prefPage: PreferencesPage;
    let sideNav: SideNav;

    beforeAll(async () => {
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        prefPage = new PreferencesPage();
        navBar = new Navbar();
        sideNav = new SideNav();

        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);

        // increase timeout to avoid "Error: Timeout - Async callback was not invoked within timeout specified by jasmine.DEFAULT_TIMEOUT_INTERVAL" message
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10 * 60 * 1000; // 10 min
    });

    // get calculators list (IDs) @TODO read it from config, but can't import jalhyd here :/
    const calcTypes = testedCalcTypes;

    // for each calculator
    for (const ct of calcTypes) {
        if ([22, 31, 32, 33, 34].includes(ct)) {
            // omit 22 - Solveur
            // omit 31 - PbCloison
            // omit 32 - PbBassin
            // omit 33 - Espece
            // omit 34 - Verificateur
            // (not calculated here because they are not independent)
            continue;
        }
        describe(" − calculate all parameters of calculator type [" + ct + "]", () => {
            it("", async () => {
                await newSession(navBar, sideNav);
                await openCalculator(ct, navBar, listPage);

                // get all parameters IDs
                const inputs = await calcPage.getParamInputsHavingCalcMode();

                if (inputs.length > 0) {
                    // for each param
                    for (let i = 0; i < inputs.length; i++) {
                        // for PreBarrage, switch back to "input" after calculating, so
                        // that the calculable parameters are shown
                        if (ct === 30 && i > 0) {
                            // prevents "Element is not clickable at point"
                            await scrollPageToTop();
                            const inputLink = await $("#pb-data-results-selector .drs-item a");
                            await inputLink.click();
                        }
                        // grab input again because calculating the module just refreshed all the fieldsets
                        const input = (await calcPage.getParamInputsHavingCalcMode())[i];
                        // click "calc" mode button for this parameter
                        await calcPage.setParamMode(input, "cal");
                        // check that only 1 button is in "calc" state
                        const nbParamsCalc = await calcPage.getCheckedCalcModeButtons().length;
                        expect(nbParamsCalc).toBe(1);
                        // check that "compute" button is active
                        const calcButton = await calcPage.checkCalcButtonEnabled(true);
                        // click "compute" button
                        await calcButton.click();
                        await browser.pause(500);
                        // check that result is not empty
                        const hasResults = await calcPage.hasResults();
                        expect(hasResults).toBe(true);
                    }
                } else {
                    // module has no calculable params, just click the "compute" button
                    // check that "compute" button is active
                    const calcButton = await calcPage.checkCalcButtonEnabled(true);
                    // click "compute" button
                    await calcButton.click();
                    await browser.pause(500);
                    // check that result is not empty
                    const hasResults = await calcPage.hasResults();
                    expect(hasResults).toBe(true);
                }
            });
        });
    }
});
