import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

describe("documentation − ", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let prefPage: PreferencesPage;
    let navBar: Navbar;

    beforeAll(() => {
        calcPage = new CalculatorPage();
        prefPage = new PreferencesPage();
        navBar = new Navbar();
        listPage = new ListPage();

        // browser.manage().window().setPosition(2000, 30);
    });

    async function checkMathjaxInHelp(lang: number) {
        // change language setup
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.changeLanguage(lang);
        await browser.pause(200);

        // open "fish ladder: fall" calculator
        await openCalculator(12, navBar, listPage);

        // click help
        await calcPage.getCalculatorHelpButton().click();
        await browser.pause(200);

        const handles = await browser.getWindowHandles();

        // switch to help tab
        await browser.switchToWindow(handles[1])
        await browser.pause(200);
        // check Mathjax element is present
        const cont = await $("mjx-container");
        expect(await cont.isExisting()).toBe(true);

        // close help tab
        await browser.execute("window.close()");

        // switch back to calculator (required to avoid failure of next language test)
        await browser.switchToWindow(handles[0]);
        await browser.pause(200);
    };

    it("check Mathjax formula are displayed in calculator French help", async () => {
        await checkMathjaxInHelp(1); // fr
    });

    it("check Mathjax formula are displayed in calculator English help", async () => {
        await checkMathjaxInHelp(0); // en
    });
});
