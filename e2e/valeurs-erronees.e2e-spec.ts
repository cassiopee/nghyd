import { CalculatorPage } from "./calculator.po";
import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

describe("ngHyd - check invalid values are removed - ", () => {
    let listPage: ListPage;
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    it("when switching to another calculator", async () => {
        // open PAB dimensions calculator
        await openCalculator(5, navBar, listPage);

        // modify W input with invalid value
        let inputW = await calcPage.getInputById("W");
        await inputW.setValue("-1");

        // open another calculator
        await openCalculator(12, navBar, listPage);

        // back to first calculator
        await navBar.openNthCalculator(0);
        await browser.pause(200);

        // check invalid value is removed
        inputW = await calcPage.getInputById("W"); // re-request element to avoid "Request encountered a stale element - terminating request" warning (due tab change)
        const w = await inputW.getValue();
        expect(w).toEqual("");

        // check that "compute" button is disabled
        await calcPage.checkCalcButtonEnabled(false);
    });
});
