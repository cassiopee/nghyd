import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { CalculatorPage } from "./calculator.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * Use navbar buttons to navigate from one open calculator to another
 */
describe("ngHyd − create calculators and navigate among them", () => {
    let listPage: ListPage;
    let calculatorPage: CalculatorPage;
    let navbar: Navbar;

    beforeEach(() => {
        listPage = new ListPage();
        calculatorPage = new CalculatorPage();
        navbar = new Navbar();
    });

    it("when many calculators are open, navigating among them should not fail (all active labels should be visible)", async () => {
        await listPage.navigateTo();
        // open 8 calculators
        for (let i = 0; i < 6; i++) {
            if (i > 0) {
                await navbar.clickNewCalculatorButton();
                await browser.pause(200);
            }
            await listPage.clickRandomCalculatorMenuEntry();
            await browser.pause(200);
        }
        // navigate among them
        for (let i = 0; i < 10; i++) {
            await navbar.clickRandomCalculatorTab(i);
            await browser.pause(200);
            // test all form labels
            const labels = await calculatorPage.getInputLabels();
            for (const l of labels) {
                const label = await l.getText();
                expect(label.length).toBeGreaterThan(0);
            };
        }
        // expect no error ?
    });
});
