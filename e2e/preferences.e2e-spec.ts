import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * Open app preferences, check the default values, the validators, the language change
 */
describe("ngHyd − preferences page", () => {
    let page: PreferencesPage;

    beforeAll(() => {
        page = new PreferencesPage();
    });

    beforeEach(async () => {
        await page.navigateTo();
        browser.pause(200);
    });

    it("when preferences are open, user should see a heading title", async () => {
        const text = await page.getHeader1().getText();
        expect(text.length).toBeGreaterThan(0);
    });

    it("when preferences are open, no label should be empty", async () => {
        const labels = await page.getInputLabels();
        for (const l of labels) {
            const label = await l.getText();
            expect(label.length).toBeGreaterThan(0);
        };
    });

    it("when preferences are open, no input field should be empty", async () => {
        const numericFields = await page.getNumericFormFields();
        for (const nf of numericFields) {
            const input = await page.getInputForField(nf);
            const val = await input.getValue();
            expect(val).toBeTruthy();
        };
    });

    it("when erroneous values are input, errors should appear", async () => {
        const numericFields = await page.getNumericFormFields();
        for (const nf of numericFields) {
            const inp = await page.getInputForField(nf);
            // add a letter after the numerical value
            await inp.addValue("d");
            let errorField = await page.getErrorsForField(nf);
            expect(await errorField.isExisting()).toBe(true);
            // empty input
            await inp.clearValue();
            errorField = await page.getErrorsForField(nf);
            expect(await errorField.isExisting()).toBe(true);
            // send bad value
            await inp.setValue("50000");
            errorField = await page.getErrorsForField(nf);
            expect(await errorField.isExisting()).toBe(true);
        };
    });

    it("when correct values are input, errors should disappear", async () => {
        const numericFields = await page.getNumericFormFields();
        for (const nf of numericFields) {
            // send correct value
            const inp = await page.getInputForField(nf);
            await inp.setValue("1");
            const errorField = await page.getErrorsForField(nf);
            expect(await errorField.isExisting()).toBe(false);
        };
    });

    it("when language is changed, language should change", async () => {
        await page.changeLanguage(0);
        await browser.pause(200);
        const val1 = await page.getHeader1().getText();
        await page.changeLanguage(1);
        await browser.pause(200);
        const val2 = await page.getHeader1().getText();
        expect(val1).not.toEqual(val2);
    });
});
