import { AppPage } from "./app.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { SideNav } from "./sidenav.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { loadSession, newSession } from "./util.po";

/**
 * Load a session containing 3 calculators, having linked parameters
 * from one to another, triggers computation from the top-most, triggers
 * results reset from the bottom-most.
 * Does it once with parameters linked to parameters, once with parameters
 * linked to results.
 */
describe("ngHyd − compute then reset chained results − ", () => {
    let startPage: AppPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let sidenav: SideNav;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        startPage = new AppPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        sidenav = new SideNav();
        prefPage = new PreferencesPage();
    });

    it("when loading session-cascade-params.json, computation should not be chained, but results reset should be", async () => {
        // load session file
        await startPage.navigateTo();
        await loadSession(navBar, sidenav, "./session/session-cascade-params.json");
        expect(await navBar.getAllCalculatorTabs().length).toBe(3);

        // 1. get down-most module
        await navBar.clickCalculatorTabForUid("Y2l2Y3");

        // click "compute" button
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(200);

        // down-most module should have results
        let hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(true);
        // up-most should not
        await navBar.clickCalculatorTabForUid("ZTFxeW");
        hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(false);
        await navBar.clickCalculatorTabForUid("Z3EwY2");
        // middle one should
        hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(true);

        // 2. get up-most module
        await navBar.clickCalculatorTabForUid("ZTFxeW");

        // modify an input that is not linked
        await calcPage.getInputById("Ks").clearValue();
        await calcPage.getInputById("Ks").setValue("42");

        // other 2 modules should still have their results
        for (let i = 1; i < 3; i++) {
            await navBar.clickCalculatorTab(i);
            hasResults = await calcPage.hasResults();
            expect(hasResults).toBe(true);
        }

        // 3. get up-most module again
        await navBar.clickCalculatorTabForUid("ZTFxeW");

        // modify input that is linked
        await calcPage.getInputById("LargeurBerge").clearValue();
        await calcPage.getInputById("LargeurBerge").setValue("2.6");

        // check all 3 modules for absence of results
        for (let i = 0; i < 3; i++) {
            await navBar.clickCalculatorTab(i);
            hasResults = await calcPage.hasResults();
            expect(hasResults).toBe(false);
        }
    });

    it("when loading session-cascade-results.json, computation and results reset should be chained", async () => {
        // load session file
        await startPage.navigateTo();
        await loadSession(navBar, sidenav, "./session/session-cascade-results.json");
        expect(await navBar.getAllCalculatorTabs().length).toBe(3);

        // 1. get down-most module (PAB Dimensions)
        await navBar.clickCalculatorTabForUid("eHh5YX");

        // click "compute" button
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(200);

        // check all 3 modules for results
        for (let i = 0; i < 3; i++) {
            await navBar.clickCalculatorTab(i);
            const hasResults = await calcPage.hasResults();
            expect(hasResults).toBe(true);
        }

        // 2. get up-most module (Macro-rugo)
        await navBar.clickCalculatorTabForUid("MGg5OH");

        // modify any input (for ex. "Ks")
        await calcPage.getInputById("Ks").clearValue();
        await calcPage.getInputById("Ks").setValue("42");

        // check all 3 modules for absence of results
        for (let i = 0; i < 3; i++) {
            await navBar.clickCalculatorTab(i);
            const hasResults = await calcPage.hasResults();
            expect(hasResults).toBe(false);
        }
    });

    it("when loading session-results-invalidation.json, results reset should not be triggered on calculation", async () => {
        await newSession(navBar, sidenav);

        // disable evil option "empty fields on module creation"
        await prefPage.setEmptyFields(false);

        // start page
        await navBar.clickNewCalculatorButton();
        await browser.pause(200);

        // load session file
        await loadSession(navBar, sidenav, "./session/session-results-invalidation.json");
        expect(await navBar.getAllCalculatorTabs().length).toBe(2);

        // get down-most module (Ouvrages)
        await navBar.clickCalculatorTabForUid("amd2OG");

        // click "compute" button
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(200);
        const hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(true);

        // 2. get up-most module (Ouvrages 1)
        await navBar.clickCalculatorTabForUid("aTgwMm");

        // modify an input that is not linked
        await calcPage.getInputById("Z2").clearValue();
        await calcPage.getInputById("Z2").setValue("101.8");

        // the down-most module should still have its results
        await navBar.clickCalculatorTabForUid("amd2OG");
        const hasResults2 = await calcPage.hasResults();
        expect(hasResults2).toBe(true);

        // calculate the upmost module
        await navBar.clickCalculatorTabForUid("aTgwMm");
        const calcButton2 = await calcPage.getCalculateButton();
        await calcButton2.click();
        await browser.pause(200);

        // the down-most module should still have its results
        await navBar.clickCalculatorTabForUid("amd2OG");
        const hasResults3 = await calcPage.hasResults();
        expect(hasResults3).toBe(true);

        // modify an input that is linked
        await navBar.clickCalculatorTabForUid("aTgwMm");
        const inpZDV = await calcPage.getInputById("0_ZDV");
        await inpZDV.clearValue();
        await inpZDV.setValue("101");

        // the down-most module should not have its results anymore
        await navBar.clickCalculatorTabForUid("amd2OG");
        const hasResults4 = await calcPage.hasResults();
        expect(hasResults4).toBe(false);
    });
});
