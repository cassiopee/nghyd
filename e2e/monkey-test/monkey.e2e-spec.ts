// import { readGremlinsScript, getFpsResults, unleashGremlins } from "../helpers/GremlinHelper";

// import { PreferencesPage } from "../preferences.po";
// import { Navbar } from "../navbar.po";
// import { browser, $, $$, expect } from '@wdio/globals'

// let prefPage: PreferencesPage;
// let navBar: Navbar;

// describe("Monkey test with Gremlins.js −", async () => {

//     beforeEach(() => {
//         prefPage = new PreferencesPage();
//         navBar = new Navbar();
//     });

//     // keep greater than setTimeout() in GremlinHelper.ts (twice as much sounds good)
//     const specificLongTimeout = 120 * 1000;

//     it("bennes-y tout là-bas d'dans !", async () => {
//         // disable evil option "empty fields on module creation"
//         await prefPage.navigateTo();
//         await prefPage.disableEvilEmptyFields();
//         await browser.pause(200);
//         // go to list page
//         await navBar.clickNewCalculatorButton();

//         // unleash the gremlins !
//         await browser.executeScript(readGremlinsScript());
//         const logs: any = await browser.executeAsyncScript(unleashGremlins);

//         /* console.log("----------- ERRORS -----------");
//         console.log(logs.error);
//         console.log("----------- WARNINGS -----------");
//         console.log(logs.warn);
//         console.log("----------- INFO -----------");
//         console.log(logs.info);
//         console.log("----------- FPS -----------");
//         console.log(getFpsResults(logs));
//         console.log("----------- ACTIONS -----------");
//         console.log(logs.log);
    
//         browser.manage().logs().get("browser").then(function(browserLog) {
//           console.log("log: " + require("util").inspect(browserLog));
//         }); */

//         expect(logs.error.length).toBe(0, logs.error);

//     }, specificLongTimeout);
// });
