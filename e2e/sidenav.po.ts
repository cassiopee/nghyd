import * as path from "path";
import { browser, $, $$, expect } from '@wdio/globals'
import { CalculatorPage } from "./calculator.po";
import { scrollToElement } from "./util.po";

export class SideNav {

    getLoadSessionButton() {
        return $("#side-nav-load-session");
    }

    getNewSessionButton() {
        return $("#side-nav-empty-session");
    }

    getDiagramButton() {
        return $("#side-nav-diagram");
    }

    getSetupButton() {
        return $("#side-nav-setup");
    }

    getFileInput() {
        return $(`dialog-load-session input[type="file"]`);
    }

    getFileLoadButton() {
        return $(`dialog-load-session button[type="submit"]`);
    }

    getConfirmNewSessionButton() {
        return $(`button#confirm-new-session`);
    }

    getNotesButton() {
        return $("#side-nav-session-props");
    }

    async clickLoadSessionButton() {
        const ncb = await this.getLoadSessionButton();
        await browser.pause(500);
        await ncb.click();
    }

    async clickDiagramButton() {
        const ncb = this.getDiagramButton();
        await browser.pause(500);
        await ncb.click();
    }

    async clickNewSessionButton() {
        const ncb = await this.getNewSessionButton();
        await ncb.click();
        await browser.pause(200);
        const cb = await this.getConfirmNewSessionButton();
        await cb.click();
    }

    async loadSessionFile(file: string, click: boolean = true) {
        const absolutePath = path.resolve(__dirname, file);
        const input = await this.getFileInput();
        await input.addValue(absolutePath);
        if (click) {
            const btn = await this.getFileLoadButton();
            await btn.click();
        }
    }

    async clickNotesButton() {
        const nb = this.getNotesButton();
        await browser.pause(200);
        await nb.click();
    }
}
