import { CalculatorPage } from "./calculator.po";
import { ListPage } from "./list.po";

/**
 * Create a random calculator
 */
describe("ngHyd − calculator page", () => {
    let page: CalculatorPage;
    let listPage: ListPage;

    beforeAll(() => {
        page = new CalculatorPage();
        listPage = new ListPage();
    });

    it("when a calculator name is clicked on list page, a calculator should be opened", async () => {
        await listPage.navigateTo();
        await listPage.clickRandomCalculatorMenuEntry();
        const text = await page.getHeader1().getText();
        expect(text.length).toBeGreaterThan(0);
    });

    it("when a calculator is open, no active label should be empty", async () => {
        const labels = await page.getInputLabels();
        for (const l of labels) {
            const label = await l.getText();
            expect(label.length).toBeGreaterThan(0);
        }
    });
});
