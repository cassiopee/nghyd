import { PreferencesPage } from "./preferences.po"
import { Navbar } from "./navbar.po";
import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

describe("ngHyd − check that results are not duplicated", () => {
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let listPage: ListPage;
    let calcPage: CalculatorPage;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    it("in 'baffle fishway: setup' calculator", async () => {
        // open baffle fishway setup calculator
        await openCalculator(28, navBar, listPage);

        // run calculation
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(200);

        // check result count
        const fixRows = await calcPage.getAllFixedResultsRows();
        const nbRows = fixRows.length;
        expect(nbRows).toBe(24); // boundaries are included
    });
});
