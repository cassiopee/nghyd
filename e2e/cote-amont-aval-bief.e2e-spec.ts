import { PreferencesPage } from "./preferences.po"
import { Navbar } from "./navbar.po";
import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { newSession, openCalculator } from "./util.po";
import { SideNav } from "./sidenav.po";

describe("ngHyd − up/downstream elevations of a reach", () => {
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let sideNav: SideNav;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
        sideNav = new SideNav();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    it("check hydraulic details availability - flow calculation", async () => {
        // open "up/downstream elevations of a reach" calculator
        await openCalculator(21, navBar, listPage);

        // set upstream flow in calculate mode
        const flowCalcBtn = await $(
            "//field-set[4]/mat-card-content/param-field-line[1]/div/div[2]/mat-button-toggle-group/mat-button-toggle[3]"
        );
        await flowCalcBtn.click();

        // check details buttons are disabled
        let upDetailsBtn = await $("#generate-sp-aval");
        expect(await upDetailsBtn.isEnabled()).toBe(false);
        const downDetailsBtn = await $("#generate-sp-amont");
        expect(await downDetailsBtn.isEnabled()).toBe(false);

        // set value to upstream water elevation so that flow calculation leads to no error
        const upWEinput = await calcPage.getInputById("Z1");
        await upWEinput.clearValue();
        await upWEinput.setValue("100.664");

        // run calculation
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(300);

        // check details buttons are enabled
        expect(await upDetailsBtn.isEnabled()).toBe(true);
        expect(await downDetailsBtn.isEnabled()).toBe(true);

        // click upstream hydraulic details button
        await upDetailsBtn.click();
        await browser.pause(500);

        // a second calculator should be created
        let calcs = await navBar.getAllCalculatorTabs();
        expect(calcs.length).toEqual(2);

        // click downstream hydraulic details button
        await navBar.openNthCalculator(0);
        upDetailsBtn = await $("#generate-sp-aval"); // re-request element to avoid "Request encountered a stale element - terminating request" warning (due to tab change)
        await upDetailsBtn.click();
        await browser.pause(500);

        // a third calculator should be created
        calcs = await navBar.getAllCalculatorTabs();
        expect(calcs.length).toEqual(3);
    });

    it("check hydraulic details availability - upstream water elevation calculation", async () => {
        await newSession(navBar, sideNav);

        // open "up/downstream elevations of a reach" calculator
        await openCalculator(21, navBar, listPage);

        // check details buttons status
        let upDetailsBtn = await $("#generate-sp-aval");
        expect(await upDetailsBtn.isEnabled()).toBe(true);
        const downDetailsBtn = await $("#generate-sp-amont");
        expect(await downDetailsBtn.isEnabled()).toBe(false);

        // run calculation
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(300);

        // check details buttons are enabled
        expect(await upDetailsBtn.isEnabled()).toBe(true);
        expect(await downDetailsBtn.isEnabled()).toBe(true);

        // click upstream hydraulic details button
        await upDetailsBtn.click();
        await browser.pause(500);

        // a second calculator should be created
        let calcs = await navBar.getAllCalculatorTabs();
        expect(calcs.length).toEqual(2);

        // click downstream hydraulic details button
        await navBar.openNthCalculator(0);
        upDetailsBtn = await $("#generate-sp-aval"); // re-request element to avoid "Request encountered a stale element - terminating request" warning (due to tab change)
        await upDetailsBtn.click();
        await browser.pause(500);

        // a third calculator should be created
        calcs = await navBar.getAllCalculatorTabs();
        expect(calcs.length).toEqual(3);
    });
});
