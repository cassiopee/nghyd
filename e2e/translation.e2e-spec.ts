import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { CalculatorPage } from "./calculator.po";
import { PreferencesPage } from "./preferences.po";
import { SideNav } from "./sidenav.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

describe("Check translation", () => {
    let listPage: ListPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        listPage = new ListPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
        prefPage = new PreferencesPage();
    });

    beforeEach(async () => {
        prefPage.setEmptyFields(false);
    });

    it("variables in results", async () => {
        // *** results in french ***

        await prefPage.navigateTo();
        await prefPage.changeLanguage(1); // fr

        // open "fish ladder: fall" calculator
        await openCalculator(12, navBar, listPage);

        // set Z2 to variated mode
        const inpZ2 = await calcPage.getInputById("Z2");
        await calcPage.setParamMode(inpZ2, "var");

        // run calculation
        await calcPage.getCalculateButton().click();
        await browser.pause(500);

        // "variable for X axis" select label
        let selXaxis = await calcPage.getSelectById("selectX");
        expect(await calcPage.getMatselectCurrentOptionText(selXaxis)).toEqual("Cote aval");

        // "variable for Y axis" select label
        let selYaxis = await calcPage.getSelectById("selectY");
        expect(await calcPage.getMatselectCurrentOptionText(selYaxis)).toEqual("DH : Chute (m)");

        // fixed results variables
        let frr = await calcPage.getAllFixedResultsRows();
        let row0 = frr[0];
        let lbl1 = await row0.$("td").getText();
        expect(lbl1).toEqual("Cote amont (m)");

        // variated results headers
        let vrh = await calcPage.getAllVariatedResultsTableHeaders();
        let lbl2 = await vrh[0].getText();
        expect(lbl2).toEqual("Cote aval");
        let lbl3 = await vrh[1].getText();
        expect(lbl3).toEqual("Chute (m)");

        // *** results in english ***

        // setup -> english
        await prefPage.navigateTo();
        await prefPage.changeLanguage(0); // en
        await browser.pause(200);

        // back to calculator
        await navBar.clickCalculatorTab(0);
        await browser.pause(200);

        // "variable for X axis" select label
        selXaxis = await calcPage.getSelectById("selectX");   // re-request element to avoid "Request encountered a stale element - terminating request" warning (due tab change)
        expect(await calcPage.getMatselectCurrentOptionText(selXaxis)).toEqual("Downstream elevation");

        // "variable for Y axis" select label
        selYaxis = await calcPage.getSelectById("selectY");   // avoid "Request encountered a stale element - terminating request" warning (due tab change)
        expect(await calcPage.getMatselectCurrentOptionText(selYaxis)).toEqual("DH : Fall (m)");

        // fixed results variables
        frr = await calcPage.getAllFixedResultsRows();  // re-request element to avoid "Request encountered a stale element - terminating request" warning (due to pref modifcation -> page update)
        row0 = frr[0];
        lbl1 = await row0.$("td").getText();
        expect(lbl1).toEqual("Upstream elevation (m)");

        // variated results headers
        vrh = await calcPage.getAllVariatedResultsTableHeaders(); // re-request element to avoid "Request encountered a stale element - terminating request" warning (due to pref modifcation -> page update)
        lbl2 = await vrh[0].getText();
        expect(lbl2).toEqual("Downstream elevation");
        lbl3 = await vrh[1].getText();
        expect(lbl3).toEqual("Fall (m)");
    });
});
