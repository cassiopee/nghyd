import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { CalculatorPage } from "./calculator.po";
import { PreferencesPage } from "./preferences.po";
import { changeSelectValue, openCalculator } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

describe("Check fields are empty in 'pressure loss' calculator when created with 'empty fields' option -", () => {
    let listPage: ListPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        listPage = new ListPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
        prefPage = new PreferencesPage();
    });

    beforeEach(async () => {
        // enable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.enableEvilEmptyFields();
        await browser.pause(200);
    });

    it("with Lechapt-Calmon pressure loss law", async () => {
        // open "pressure loss" calculator
        await openCalculator(35, navBar, listPage);

        // select Lechapt-Calmon pressure loss law
        const materialSelect = await calcPage.getSelectById("select_pressurelosstype");
        await changeSelectValue(materialSelect, 0);
        await browser.pause(200);

        expect(await calcPage.checkEmptyOrFilledFields(["Q", "D", "Lg", "Kloc"], [true, true, true, true]));
    });
});
