import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { changeSelectValue, openCalculator, scrollPageToTop } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * Clone calculators
 */
describe("ngHyd − clone a calculator", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let prefPage: PreferencesPage;

    beforeAll(async () => {
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        prefPage = new PreferencesPage();
        navBar = new Navbar();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    it("when cloning a calculator, the clone should have the same values for all parameters", async () => {
        // 1. create target modules for linked parameter
        await openCalculator(3, navBar, listPage);

        const debitRU = await calcPage.getInputById("calc_Q"); // "Débit" is calculated by default
        await calcPage.setParamMode(debitRU, "fix");
        await browser.pause(500);

        await openCalculator(4, navBar, listPage);

        // 2. create source module to clone
        await openCalculator(2, navBar, listPage);

        // 3. change and store source parameter values
        const sourceValues = {
            k: 0.6,
            Ks: 42
        };
        const selSection = await calcPage.getSelectById("select_section");
        await changeSelectValue(selSection, 3); // mode "parabolique"
        await calcPage.getInputById("k").clearValue();
        await calcPage.getInputById("k").setValue(sourceValues["k"]);
        await calcPage.getInputById("Ks").clearValue();
        await calcPage.getInputById("Ks").setValue(sourceValues["Ks"]);
        // link "Débit" to "Courbe de remous"
        const debitSP = await calcPage.getInputById("Q");
        await calcPage.setParamMode(debitSP, "link");
        await browser.pause(500);
        const selQ = await calcPage.getSelectById("linked_Q");
        await changeSelectValue(selQ, 1); // "Courbe de remous"
        await browser.pause(500);

        // otherwise clickCloneCalcButton() fails with "Element is not clickable at point"
        await scrollPageToTop();
        await calcPage.clickCloneCalcButton();
        await browser.pause(500);

        // 4. check existence of the cloned module
        expect(await navBar.getAllCalculatorTabs().length).toBe(4);
        await navBar.clickCalculatorTab(3); // n°3 should be the latest
        await browser.pause(500);

        // 5. compare values
        for (const k in sourceValues) {
            const v = sourceValues[k];
            const inp = await calcPage.getInputById(k);
            const displayedVal = await inp.getValue();
            expect(displayedVal).toBe("" + v);
        };

        // @TODO check linked value (see above)
    });
});
