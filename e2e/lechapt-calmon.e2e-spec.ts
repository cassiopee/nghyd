import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { PreferencesPage } from "./preferences.po";
import { Navbar } from "./navbar.po";
import { changeSelectValue, openCalculator } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * Check that created/cloned structures have empty fields when
 * "empty fields on calculator creation" is enabled
 */
describe("Lechapt&Calmon - ", () => {
    let prefPage: PreferencesPage;
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    async function setup() {
        // open Lechapt-Calmon (pressure loss) calculator
        await openCalculator(35, navBar, listPage);
    }

    it("when material is modified, results should change", async () => {
        await setup();

        // select last material type
        const materialSelect = await calcPage.getSelectById("select_material");
        await changeSelectValue(materialSelect, 8);
        await browser.pause(200);

        // run calculation
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(200);

        // store total pressure loss result
        const rows = await calcPage.getAllFixedResultsRows();
        const res1 = rows[4];
        const pl1 = await res1.$$("td")[1].getText();

        // select first material type
        await changeSelectValue(materialSelect, 0);
        await browser.pause(200);

        // run calculation
        await calcButton.click();
        await browser.pause(200);

        // compare total pressure loss result with first calculaiotn
        const rows2 = await calcPage.getAllFixedResultsRows();
        const res2 = rows2[4];
        const pl2 = await res2.$$("td")[1].getText();

        expect(pl1).not.toEqual(pl2);
    });
});
