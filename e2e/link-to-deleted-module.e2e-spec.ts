import { CalculatorPage } from "./calculator.po";
import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { newSession, openCalculator } from "./util.po";
import { SideNav } from "./sidenav.po";

describe("linked parameter - ", () => {
    let calcPage: CalculatorPage;
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let listPage: ListPage;
    let sideNav: SideNav;

    beforeAll(() => {
        calcPage = new CalculatorPage();
        prefPage = new PreferencesPage();
        navBar = new Navbar();
        listPage = new ListPage();
        sideNav = new SideNav();
    });

    beforeEach(async () => {
        // browser.manage().window().setPosition(2000, 30);

        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);

        await navBar.clickNewCalculatorButton();
    });

    it("delete linked module", async () => {
        // open "channel flow with hydraulic structures" example
        const examples = await $$("#examples-list .load-example");
        await examples[1].click();
        await browser.pause(500);

        // select "up/downstream elevations of a reach" tab
        await navBar.clickCalculatorTab(1);
        await browser.pause(500);

        // close "solver" calculator
        await navBar.middleClickCalculatorTab(3);

        // reselect "up/downstream elevations of a reach" tab
        await navBar.clickCalculatorTab(1);
        await browser.pause(500);

        // close second "parallel structures" calculator
        await navBar.middleClickCalculatorTab(2);

        // close first "parallel structures" calculator
        await navBar.middleClickCalculatorTab(0);

        // check Z2 input is in "fixed" state in remaining calculator
        const inpZ2 = await calcPage.getInputById("Z2");
        expect(await calcPage.inputIsInFixedMode(inpZ2)).toBe(true);
    });

    it("delete linked module and duplicate remaining one", async () => {
        await newSession(navBar, sideNav);

        // open "fish ladder: fall" calculator
        await openCalculator(12, navBar, listPage);

        // clone calculator
        await calcPage.clickCloneCalcButton();
        await browser.pause(200);

        // set DH in link mode
        let inpDH = await calcPage.getInputById("DH");
        await calcPage.setParamMode(inpDH, "link");
        await browser.pause(200);

        // close 1st calculator
        await navBar.middleClickCalculatorTab(0);
        await browser.pause(200);

        // check DH input is in "fixed" state in remaining calculator (not the aim of this test)
        inpDH = await calcPage.getInputById("DH");
        expect(await calcPage.inputIsInFixedMode(inpDH)).toBe(true);

        // set DH to calculated mode
        await calcPage.setParamMode(inpDH, "cal");
        await browser.pause(100);

        // clone calculator
        await calcPage.clickCloneCalcButton();
        await browser.pause(200);

        // select 1st tab
        await navBar.clickCalculatorTab(0);
        await browser.pause(500);

        // check DH input is in "calc" mode
        inpDH = await calcPage.getInputById("DH");
        expect(await calcPage.inputIsInCalculatedMode(inpDH)).toBe(true);
    });
});
