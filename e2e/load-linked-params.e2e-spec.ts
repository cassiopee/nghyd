import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { SideNav } from "./sidenav.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { loadSession } from "./util.po";

/**
 * Load a session containing 5 calculators, having multiple linked parameters
 * from one to another
 * @TODO les valeurs des Select sont comparées au français, pas très générique :/
 */
describe("ngHyd − load session with multiple linked parameters − ", () => {
    let calcPage: CalculatorPage;
    let prefPage: PreferencesPage;
    let navbar: Navbar;
    let sidenav: SideNav;

    beforeAll(() => {
        calcPage = new CalculatorPage();
        prefPage = new PreferencesPage();
        navbar = new Navbar();
        sidenav = new SideNav();
    });

    beforeEach(async () => {
        // force language to prevent issues due to default browser language
        await prefPage.navigateTo();
        await prefPage.changeLanguage(1); // fr
        await browser.pause(200);
        // disable evil option "empty fields on module creation"
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
        // start page
        await navbar.clickNewCalculatorButton();
    });

    it("when loading session-liens-spaghetti.json, all links should point to the right target", async () => {
        await loadSession(navbar, sidenav, "./session/session-liens-spaghetti.json");

        expect(await navbar.getAllCalculatorTabs().length).toBe(5);

        // 1. check Section paramétrée
        await navbar.clickCalculatorTab(0);
        await browser.pause(500);

        // check target params values
        const sp_lb = await calcPage.getSelectById("linked_LargeurBerge");
        const sp_lbv = await calcPage.getSelectValueText(sp_lb);
        expect(sp_lbv).toContain("Largeur du déversoir (Ouvrages, ouvrage 3)");

        // 2. check Passe à macro-rugosités
        await navbar.clickCalculatorTab(1);
        await browser.pause(500);

        // check target params values
        const mr_zf1 = await calcPage.getSelectById("linked_ZF1");
        const mr_zf1v = await calcPage.getSelectValueText(mr_zf1);
        expect(mr_zf1v).toContain("Cote de l'eau amont (Ouvrages)");

        const mr_q = await calcPage.getSelectById("linked_Q");
        const mr_qv = await calcPage.getSelectValueText(mr_q);
        expect(mr_qv).toContain("Débit (Sec. param., section)");

        // 3. check Lois d'ouvrages
        await navbar.clickCalculatorTab(2);
        await browser.pause(500);

        // check target params values
        // const lo_z2 = calcPage.getSelectById("linked_Z2");
        // const lo_z2v = await calcPage.getSelectValueText(lo_z2);
        // expect(lo_z2v).toContain("Cote de fond aval (Macro-rugo.)");

        /**
         * depuis qu'on interdit les liens vers des paramètres cachés/liés eux mêmes,
         * l'appli les remplace par un paramètre en mode fixé (valeur undefined) lors du chargement d'une session
         * jalhyd#289/adf6009
         * nghyd#551
         */
        const inpZ2 = await calcPage.getInputById("Z2");
        expect(await inpZ2.getValue()).toEqual("");

        // const lo_l = calcPage.getSelectById("1_linked_L");
        // const lo_lv = await calcPage.getSelectValueText(lo_l);
        // expect(lo_lv).toContain("Largeur au miroir (Sec. param.)");
        const inpL = await calcPage.getInputById("1_L");
        expect(await inpL.getValue()).toEqual("");

        const lo_w = await calcPage.getSelectById("2_linked_W");
        const lo_wv = await calcPage.getSelectValueText(lo_w);
        expect(lo_wv).toContain("Ouverture de vanne (Ouvrages, ouvrage 2)");

        // 5. check Déver. dénoyés
        await navbar.clickCalculatorTab(4);
        await browser.pause(500);

        // check target params values
        // const lo_br = calcPage.getSelectById("linked_BR");
        // const lo_brv = await calcPage.getSelectValueText(lo_br);
        // expect(lo_brv).toContain("Largeur au miroir (Sec. param.)");
        const inpBR = await calcPage.getInputById("BR");
        expect(await inpBR.getValue()).toEqual("");
    });
});
