import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { testedCalcTypes } from "./tested_calctypes";
import { newSession, openCalculator, scrollPageToTop } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { SideNav } from "./sidenav.po";

/**
 * Clone calculators
 */
describe("ngHyd − clone all calculators with all possible <select> values", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let prefPage: PreferencesPage;
    let sideNav: SideNav;

    beforeAll(async () => {
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        prefPage = new PreferencesPage();
        sideNav = new SideNav();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
        await navBar.clickNewCalculatorButton();
    });

    describe("", () => {

        const calcTypes = testedCalcTypes;

        // for each calculator
        for (const ct of calcTypes) {
            if (ct === 22) {
                // omit 22 - Solveur is not tested here because it is not independent
                continue;
            }
            describe(" − clone all variations of calculator type [" + ct + "]", () => {
                it("", async () => {
                    await newSession(navBar, sideNav);

                    // click calculator button (instanciate)
                    await openCalculator(ct, navBar, listPage);

                    // get all select IDs outside Structures
                    // get select IDs inside Structures
                    // @TODO set configuration to every combination of <select> options

                    // modify all <input> values and store them
                    await calcPage.modifyAllInputValues();
                    const sourceValues = await calcPage.storeAllInputValues();

                    // clone calculator
                    await scrollPageToTop();
                    await calcPage.clickCloneCalcButton();
                    await browser.pause(300);

                    // check existence of the cloned module
                    expect(await navBar.getAllCalculatorTabs().length).toBe(2);

                    // @TODO check <select> values

                    // read all <input> values and compare them to stored ones
                    const cloneValues = await calcPage.storeAllInputValues();
                    for (const k in cloneValues) {
                        expect(cloneValues[k]).toBeCloseTo(sourceValues[k]);
                    }
                });
            });
        }
    });
});
