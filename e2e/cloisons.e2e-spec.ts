import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { changeSelectValue, openCalculator } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * Cloisons - différents tests qui n'ont pas tant de rapport que ça avec les cloisons :)
 */
describe("ngHyd − cloisons", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        prefPage = new PreferencesPage();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    it("when all parent Nub parameters are linked, Structure parameter modes should be alterable without problem", async () => {
        // 1. create target module for linked parameters
        await openCalculator(10, navBar, listPage);

        // 2. create module to test
        await calcPage.clickCloneCalcButton();
        await browser.pause(300);

        // 3. link every parameter except Structure ones
        await calcPage.setParamMode(await calcPage.getInputById("calc_Q"), "link");
        await calcPage.setParamMode(await calcPage.getInputById("Z1"), "link");
        await calcPage.setParamMode(await calcPage.getInputById("LB"), "link");
        await calcPage.setParamMode(await calcPage.getInputById("BB"), "link");
        await calcPage.setParamMode(await calcPage.getInputById("PB"), "link");
        await calcPage.setParamMode(await calcPage.getInputById("DH"), "link");
        await browser.pause(300);

        // 4. change LoiDebit
        await changeSelectValue(await calcPage.getSelectById("select_loidebit"), 1);
        await browser.pause(300);

        // 5. check number of inputs in CALC mode
        expect(await calcPage.getCheckedCalcModeButtons().length).toBe(1);

        // 6. try calculating the module
        const btn = await calcPage.getCalculateButton()
        await btn.click();
        await browser.pause(200);
        expect(await calcPage.hasResults()).toBe(true);
    });
});
