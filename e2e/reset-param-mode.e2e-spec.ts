import { ListPage } from "./list.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { newSession, openApplication, openCalculator } from "./util.po";
import { Navbar } from "./navbar.po";
import { SideNav } from "./sidenav.po";
import { CalculatorPage } from "./calculator.po";
import { AppPage } from "./app.po";

/**
 * Parameter mode should be set to its previous mode (fixed/var/...) when cancel is pressed
 * in variable mode edition dialog
 */
describe("ngHyd - check parameter mode is set to its previous value - ", () => {
    let startPage: AppPage;
    let listPage: ListPage;
    let navBar: Navbar;
    let sideNav: SideNav;
    let calcPage: CalculatorPage;

    beforeAll(() => {
        startPage = new AppPage();
        listPage = new ListPage();
        navBar = new Navbar();
        sideNav = new SideNav();
        calcPage = new CalculatorPage();
    });

    it("when min/max/list values dialog is cancelled on 'fish ladder: fall' calculator", async () => {
        await openApplication(startPage);
        await newSession(navBar, sideNav);

        // open 'PAB fall' calculator
        await openCalculator(12, navBar, listPage);

        // click "calc" radio on Z1 parameter
        const z1calcbtn = await calcPage.getInputRadioButtonFromId("Z1", "cal");
        await z1calcbtn.click();
        await browser.pause(200);

        // click "var" radio on Z1 parameter
        const z1varbtn = await calcPage.getInputRadioButtonFromId("Z1", "var");
        await z1varbtn.click();
        await browser.pause(200);

        // click cancel button
        const cancelbtn = await $("#btn-cancel");
        await cancelbtn.click();
        await browser.pause(500);

        // check Z1 var toggle is not checked
        expect(await calcPage.isRadioButtonChecked(z1varbtn)).toEqual(false);

        // check Z1 calc toggle is checked
        expect(await calcPage.isRadioButtonChecked(z1calcbtn)).toEqual(true);
    });

    it("when min/max/list values dialog is cancelled on 'fish ladder' calculator", async () => {
        await openApplication(startPage);
        await newSession(navBar, sideNav);

        // open PAB calculator
        await openCalculator(15, navBar, listPage);

        // "fixed" radio on Q parameter
        const qfixbtn = await calcPage.getInputRadioButtonFromId("Q", "fix");

        // "var" radio on Z1 parameter
        const z1varbtn = await calcPage.getInputRadioButtonFromId("Z1", "var");

        // "calc" radio on Z1 parameter
        const z1calcbtn = await calcPage.getInputRadioButtonFromId("Z1", "cal");

        // click "var" radio on Z1 parameter
        await z1varbtn.click();
        await browser.pause(200);

        // click cancel button
        const cancelbtn = await $("#btn-cancel");
        await cancelbtn.click();
        await browser.pause(500);

        // check Q fix toggle is checked
        expect(await calcPage.isRadioButtonChecked(qfixbtn)).toEqual(true);

        // check Z1 calc toggle is checked
        expect(await calcPage.isRadioButtonChecked(z1calcbtn)).toEqual(true);
    });
});
