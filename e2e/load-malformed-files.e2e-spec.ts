import { Navbar } from "./navbar.po";
import { SideNav } from "./sidenav.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { loadSession } from "./util.po";

/**
 * Load 4 malformed session files :
 *  - one having an format version too old
 *  - one having an empty list of modules
 *  - one having missing mandatory data (ex: parameter symbol)
 *  - one having malformed JSON syntax
 * @WARNING error messages are tested in french
 */
describe("ngHyd − load malformed session files − ", () => {
    let navbar: Navbar;
    let sidenav: SideNav;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        navbar = new Navbar();
        sidenav = new SideNav();
        prefPage = new PreferencesPage();
    });

    beforeEach(async () => {
        // force language to prevent issues due to default browser language
        await prefPage.navigateTo();
        await prefPage.changeLanguage(1); // fr
        await browser.pause(200);
        await navbar.clickNewCalculatorButton();
    });

    it("when loading session-bad-syntax.json, displayed error should be relevant", async () => {
        await loadSession(navbar, sidenav, "./session/session-bad-syntax.json");

        const err = await $(".file-problem .mat-list-item-content");
        expect(await err.getText()).toContain("La syntaxe du fichier semble incorrecte");
    });

    it("when loading session-missing-info.json, displayed error should be relevant", async () => {
        await loadSession(navbar, sidenav, "./session/session-missing-info.json");

        const err = await $(".file-problem .mat-list-item-content");
        expect(await err.getText()).toContain("La syntaxe du fichier semble incorrecte");
    });

    it("when loading session-empty-modules-list.json, displayed error should be relevant", async () => {
        await loadSession(navbar, sidenav, "./session/session-empty-modules-list.json");

        const err = await $(".file-problem .mat-list-item-content");
        expect(await err.getText()).toContain("Le fichier ne contient aucun module");
    });

    it("when loading session-format-too-old.json, displayed error should be relevant", async () => {
        await loadSession(navbar, sidenav, "./session/session-format-too-old.json", false);

        const err = await $(".file-problem .mat-list-item-content");
        expect(await err.getText()).toContain("Mauvaise version du format de fichier");
    });
});
