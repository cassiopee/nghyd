import { Navbar } from "./navbar.po";
import { ListPage } from "./list.po";
import { SideNav } from "./sidenav.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

/**
 * convert Mermaid formatted id ("flowchat-id-xx") to id
 * COPIED from Jalhyd since import is not possible.
 */
function removeMermaidIdFormat(id: string): string {
    if (id.startsWith("flowchart-")) {
        const i1 = id.indexOf("-");
        const i2 = id.lastIndexOf("-");
        id = id.substring(i1 + 1, i2);
    }
    return id;
}

describe("modules diagram", () => {
    let navBar: Navbar;
    let listPage: ListPage;
    let sideNav: SideNav;
    let prefPage: PreferencesPage;

    beforeAll(async () => {
        listPage = new ListPage();
        navBar = new Navbar();
        sideNav = new SideNav();
        prefPage = new PreferencesPage();
    });

    beforeEach(async () => {
        await prefPage.navigateTo();
    });

    it("- check click on a module opens matching calculator", async () => {
        //browser.manage().window().setPosition(2000, 30);

        // open predam calculator
        await openCalculator(30, navBar, listPage);

        // open modules diagram
        await navBar.clickMenuButton();
        await browser.pause(500);
        await sideNav.clickDiagramButton();
        await browser.pause(200);

        // click first module
        const nodes = await $$("g.node.default");
        const node0 = nodes[0];
        const n0id = removeMermaidIdFormat(await node0.getAttribute("id"));
        await node0.click();
        await browser.pause(200);

        // check calculator is open
        const url = await browser.getUrl();
        expect(url).toContain("/#/calculator/" + n0id);
    });
});
