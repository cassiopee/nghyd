import { browser } from '@wdio/globals'

export class AppPage {
    navigateTo() {
        return browser.url("/");
    }
}
