import { browser, $, $$, expect } from '@wdio/globals'
import { Navbar } from './navbar.po';
import { SideNav } from './sidenav.po';
import { ListPage } from './list.po';
import { AppPage } from './app.po';

/**
 * scroll page to make element visible
 */
export async function scrollToElement(elem) {
    await browser.execute(e => { e.scrollIntoView({ block: 'center' }) }, elem);
    await browser.pause(200);
}

/**
 * scroll page to top
 */
export async function scrollPageToTop() {
    await browser.execute("window.scrollTo(0, 0);");
}

/**
 * execute expect() on numbers and displays a failure message
 * @param msg message in caase of failure
 * @param val value to check
 * @param expected expected value
 */
export function expectNumber(msg: string, val: number, expected: number) {
    if (val !== expected) {
        console.log(msg, "got", val, "expected", expected);
    }
    expect(val).toEqual(expected);
}

/**
 * execute expect() on a boolean and displays a failure message
 * @param msg message in caase of failure
 * @param val value to check
 * @param expected expected value
 */
export function expectBoolean(msg: string, val: boolean, expected: boolean) {
    if (val !== expected) {
        console.log(msg, "got", val, "expected", expected);
    }
    expect(val).toEqual(expected);
}

export async function changeSelectValue(elt, index: number) {
    await scrollToElement(elt);
    await browser.pause(100);
    await elt.click();
    await browser.pause(100);
    const optionId = ".cdk-overlay-container mat-option:nth-of-type(" + (index + 1) + ")";
    const option = await $(optionId);
    await option.click();
    await browser.pause(200);
}

// Remove all calculators from current session.
// Often used since WebDriverIO migration since it()s in a single e2e-spec file
// reuse the same browser window, hence calculators from previous it()s remain
// and may disturb subsequent tests.
// Avoids splitting it()s into multiple e2e-spec files...
export async function newSession(navBar: Navbar, sideNav: SideNav) {
    const tabs = await navBar.getAllCalculatorTabs();
    if (tabs.length > 0) {
        await navBar.clickMenuButton();
        await browser.pause(500);
        await sideNav.clickNewSessionButton();
        await browser.pause(200);
        // après ça, le bouton "#new-calculator" est absent
    }
}

/**
 * open application if necessary (ie. if no URL has been open)
 */
export async function openApplication(startPage: AppPage) {
    const url = await browser.getUrl();
    if (url.indexOf("localhost") === -1) {
        await startPage.navigateTo();
        await browser.pause(500);
    }
}

export async function openCalculator(id: number, navBar: Navbar, listPage: ListPage) {
    const ncButton = await navBar.getNewCalculatorButton();
    await browser.pause(100); // avoid "webdriver: Request encountered a stale element - terminating request" warning
    if (await ncButton.isExisting()) {
        await scrollToElement(ncButton);
        if (await ncButton.isDisplayed()) {
            await ncButton.click()
            await browser.pause(300);
        }
    }

    await listPage.clickMenuEntryForCalcType(id);
    await browser.pause(500);
}

/**
 * load a session from JSON file
 */
export async function loadSession(navbar: Navbar, sideNav: SideNav, path: string, click: boolean = true) {
    await navbar.clickMenuButton();
    await browser.pause(500);

    await sideNav.clickLoadSessionButton();
    await browser.pause(500);

    await sideNav.loadSessionFile(path, click);
    await browser.pause(500);
}

/**
 * @returns true if an element exists and is displayed
 */
export async function isElementDisplayed(element) {
    return await element.isExisting() && await element.isDisplayed();
}
