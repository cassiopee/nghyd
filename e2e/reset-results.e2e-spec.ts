import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { CalculatorPage } from "./calculator.po";
import { PreferencesPage } from "./preferences.po";
import { SideNav } from "./sidenav.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

describe("Check results are reset after application settings modification - ", () => {
    let listPage: ListPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;
    let prefPage: PreferencesPage;
    let sideNav: SideNav;

    beforeAll(() => {
        listPage = new ListPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
        prefPage = new PreferencesPage();
        sideNav = new SideNav();
    });

    beforeEach(async () => {
        // browser.manage().window().setPosition(2000, 30);

        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    async function runTestWithParameter(param: string, val1: number, val2: number) {
        // set starting compute precision
        let input = await prefPage.getInputFromName(param);
        await input.clearValue();
        await input.setValue(val1.toString());

        // open "fish ladder: fall" calculator
        await openCalculator(12, navBar, listPage);

        // click "compute" button
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(500);

        // results should be here
        let hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(true);

        // reopen settings
        await navBar.clickMenuButton();
        await browser.pause(500);
        const setupBtn = await sideNav.getSetupButton();
        await browser.pause(200);
        await setupBtn.click();
        await browser.pause(200);

        // modify compute precision
        input = await prefPage.getInputFromName(param);
        await input.setValue(val2.toString());
        await browser.pause(500);

        // back to calculator
        await navBar.openNthCalculator(0);

        // results should not be here
        hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(false);
    }

    it("compute precision", async () => {
        await runTestWithParameter("cp", 0.001, 0.0001);
    });

    it("max iterations", async () => {
        await runTestWithParameter("nmi", 10, 100);
    });
});
