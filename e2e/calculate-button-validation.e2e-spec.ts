import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

describe("Calculate button - ", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
    });

    beforeEach(async () => {
        await prefPage.navigateTo();
        // disable evil option "empty fields on module creation"
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    it("check button status only depends on calculator (no link between calculators)", async () => {
        // open PAB: chute calculator
        await openCalculator(12, navBar, listPage);

        // open PAB: dimensions
        await openCalculator(5, navBar, listPage);

        // fill width field with invalid data
        const inputW = await calcPage.getInputById("W");
        await inputW.clearValue();
        await browser.pause(20);
        await inputW.setValue("-1");
        await browser.pause(200);

        // check that "compute" button is inactive
        await calcPage.checkCalcButtonEnabled(false);

        // back to PAB: chute
        await navBar.clickCalculatorTab(0);
        await browser.pause(200);

        // check that "compute" button is active
        await calcPage.checkCalcButtonEnabled(true);

        // back to PAB: dimensions
        await navBar.clickCalculatorTab(1);
        await browser.pause(200);

        // check that "compute" button is inactive
        await calcPage.checkCalcButtonEnabled(false);
    });

    describe("check button status in prébarrages - ", () => {
        it("invalid data in Q input", async () => {
            // open prébarrages calculator
            await openCalculator(30, navBar, listPage);

            // Q input
            const inputQ = await $("#Q");
            await inputQ.clearValue();
            await browser.pause(200);
            await inputQ.setValue("-1");
            await browser.pause(200);

            await calcPage.checkCalcButtonEnabled(false);

            // upstream item
            // look for g element with id starting by "flowchart-amont-"
            // Mermaid changed the way ids are generated, it preprends "flowchart-" and appends a number to the id given in the graph description
            const upstream = await $("g[id^='flowchart-amont-']");
            // should be displayed in error
            expect(await upstream.getAttribute('class')).toContain("node-highlighted-error"); // upstream item is now selected by default (was 'node-error')
        });

        it("add basin, invalid data in Q input", async () => {
            // open prébarrages calculator
            await openCalculator(30, navBar, listPage);

            // "add basin" button
            const addBasinBtn = await $("#add-basin");
            await addBasinBtn.click();
            await browser.pause(200);

            // upstream item
            const upstream = await $("g[id^='flowchart-amont-']"); // Mermaid generated id
            await upstream.click();
            await browser.pause(200);

            // invalid data in Q input
            const inputQ = await $("#Q");
            await inputQ.clearValue();
            await browser.pause(200);
            await inputQ.setValue("-1");
            await browser.pause(200);

            // calculate button disabled ?
            await calcPage.checkCalcButtonEnabled(false);

            // upstream item displayed in error ?
            expect(await upstream.getAttribute('class')).toContain("node-highlighted-error"); // upstream item is now selected by default (was 'node-error')

            // valid data in Q input
            await inputQ.clearValue();
            await browser.pause(200);
            await inputQ.setValue("1");
            await browser.pause(200);

            // calculate button still disabled ? (the basin is not connected to anything)
            await calcPage.checkCalcButtonEnabled(false);

            // upstream item displayed not in error ?
            expect(await upstream.getAttribute('class')).not.toContain("node-error");
        });
    });

    async function checkCalculateButtonValidity(calcType: number) {
        // open calculator
        await openCalculator(calcType, navBar, listPage);

        // for each input, set empty and check calculate button is not active

        const inputs = await calcPage.getParamInputs();
        for (const inp of inputs) {
            // set input to fixed mode
            await calcPage.setParamMode(inp, "fix");
            await browser.pause(100);

            // clear input
            await calcPage.clearInput(inp);
            await browser.pause(10);

            // check calculate button is disabled
            await calcPage.checkCalcButtonEnabled(false);
            await browser.pause(100);

            // refill input
            await browser.keys("1");
            await browser.pause(100);
        }
    }

    it("check status for various calculators", async () => {
        // "parallel structures" calculator
        await checkCalculateButtonValidity(8);

        // "fish ladder : cross walls" calculator
        await checkCalculateButtonValidity(10);
    });
});
