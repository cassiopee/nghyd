import { CalculatorPage } from "./calculator.po";
import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po"
import { browser, $, $$, expect } from '@wdio/globals'
import { changeSelectValue, loadSession, openCalculator, scrollToElement } from "./util.po";
import { AppPage } from "./app.po";
import { SideNav } from "./sidenav.po";

describe("predams - ", () => {
    let listPage: ListPage;
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;
    let startPage: AppPage;
    let sideNav: SideNav;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
        startPage = new AppPage();
        sideNav = new SideNav();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.disableEvilEmptyFields();
        await browser.pause(100);
    });

    it("check that low iteration count leads to log messages", async () => {
        // set low iteration count
        await prefPage.setIterationCount(5);

        // open predam calculator
        await openCalculator(30, navBar, listPage);

        // run calculation
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(200);

        // check log messages presence
        expect(await calcPage.hasLog()).toBe(true);
    });

    it("check log messages in predams example", async () => {
        await startPage.navigateTo();
        await browser.pause(200);

        // open predams example
        const examples = await $$("#examples-list .load-example");
        await examples[6].click();
        await browser.pause(500);

        // click "Amont-B2" 
        const nodes = await $$("g.nodes > g");
        const node = nodes[9];
        await scrollToElement(node);
        await browser.pause(100);
        await node.click();
        await browser.pause(200);

        // change discharge law to Larinier
        const sel = await calcPage.getSelectById("select_loidebit");
        await changeSelectValue(sel, 3);
        await browser.pause(100);

        // calculate module
        const calcBtn = await calcPage.getCalculateButton();
        await calcBtn.click();
        await browser.pause(300);

        // check log messages presence
        expect(await calcPage.hasLog()).toBe(true);
    });

    it("check submergence error does not trigger too soon", async () => {
        await startPage.navigateTo();
        await browser.pause(200);

        await loadSession(navBar, sideNav, "./session/session-submergence-error-Larinier.json");

        // calculate module
        const calcBtn = await calcPage.getCalculateButton();
        await calcBtn.click();
        await browser.pause(300);

        // check log messages absence
        expect(await calcPage.hasLog()).toBe(false);
    });

    it("check submergence error and results if dichotomy did not converge", async () => {
        await startPage.navigateTo();
        await browser.pause(200);

        // open predams example
        const examples = await $$("#examples-list .load-example");
        await examples[6].click();
        await browser.pause(500);

        // modify iteration count,precision
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.setIterationCount(10);
        await prefPage.setComputePrecision(1e-15);

        // back to module
        await navBar.openNthCalculator(0);
        await browser.pause(500);

        // calculate module
        const calcBtn = await calcPage.getCalculateButton();
        await calcBtn.click();
        await browser.pause(300);

        // check log messages presence
        expect(await calcPage.hasLog()).toBe(true);
        expect(await calcPage.nbLogEntries()).toBe(12);

        // check that results are not empty (dichotomy did not convergence but results should be displayed anyway)
        const hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(true);
    });

    it("check results (dichotomy did not converge + submergence error)", async () => {
        await startPage.navigateTo();
        await browser.pause(200);

        // open predams example
        const examples = await $$("#examples-list .load-example");
        await examples[6].click();
        await browser.pause(500);

        // modify iteration count,precision (should trigger submergence+dichotomy non convergence errors)
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.setIterationCount(20);
        await prefPage.setComputePrecision(1e-15);

        // back to module
        await navBar.openNthCalculator(0);
        await browser.pause(500);

        // click "Amont-B2" 
        const nodes = await $$("g.nodes > g");
        const node = nodes[9];
        await scrollToElement(node);
        await browser.pause(100);
        await node.click();
        await browser.pause(200);

        // change discharge law to Larinier
        const sel = await calcPage.getSelectById("select_loidebit");
        await changeSelectValue(sel, 3);
        await browser.pause(100);

        // calculate module
        const calcBtn = await calcPage.getCalculateButton();
        await calcBtn.click();
        await browser.pause(300);

        // check that results are empty
        const hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(false);

        // check log messages
        expect(await calcPage.hasLog()).toBe(true);
        expect(await calcPage.nbLogEntries()).toBe(4);
        expect(await calcPage.nthLogEntryIsWarning(0)).toBe(true);
        expect(await calcPage.nthLogEntryIsError(1)).toBe(true);
        expect(await calcPage.nthLogEntryIsWarning(2)).toBe(true);
        expect(await calcPage.nthLogEntryIsError(3)).toBe(true);
    });
});
