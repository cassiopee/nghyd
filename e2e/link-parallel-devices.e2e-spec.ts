import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { PreferencesPage } from "./preferences.po";
import { Navbar } from "./navbar.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { newSession, openCalculator } from "./util.po";
import { SideNav } from "./sidenav.po";

describe("ngHyd − parallel structures with multiple linked parameters − ", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let sideNav: SideNav;

    beforeAll(() => {
        calcPage = new CalculatorPage();
        listPage = new ListPage();
        prefPage = new PreferencesPage();
        navBar = new Navbar();
        sideNav = new SideNav();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    it("when creating Parallel Structures, devices should be linkable to one another", async () => {
        await openCalculator(8, navBar, listPage);

        const addBtn = await calcPage.getAddStructureButton();
        await addBtn.click();
        await browser.pause(200);
        const nb1 = await calcPage.getAllLinkButtons().length;
        expect(nb1).toBe(8); // link buttons on children but not on parent
    });

    it("when creating Cloisons, devices should be linkable to one another", async () => {
        await newSession(navBar, sideNav);
        await openCalculator(10, navBar, listPage);

        const addBtn = await calcPage.getAddStructureButton();
        await addBtn.click();
        await browser.pause(200);
        const nb2 = await calcPage.getAllLinkButtons().length;
        expect(nb2).toBe(6); // link buttons on children but not on parent
    });

    it("when creating Dever, devices should be linkable to one another", async () => {
        await newSession(navBar, sideNav);
        await openCalculator(9, navBar, listPage);

        const addBtn = await calcPage.getAddStructureButton();
        await addBtn.click();
        await browser.pause(200);
        const nb3 = await calcPage.getAllLinkButtons().length;
        expect(nb3).toBe(6); // link buttons on children but not on parent
    });
});
