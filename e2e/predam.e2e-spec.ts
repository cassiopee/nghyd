import { browser, $, $$, expect } from '@wdio/globals'
import { PreferencesPage } from './preferences.po';
import { Navbar } from './navbar.po';
import { CalculatorPage } from './calculator.po';
import { changeSelectValue, scrollToElement } from './util.po';

describe("predams - ", () => {
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.setEmptyFields(false);
        await navBar.clickNewCalculatorButton();
        await browser.pause(200);
    });

    it("bug nghyd#620", async () => {
        // open predams example
        const examples = await $$("#examples-list .load-example");
        await examples[6].click();
        await browser.pause(200);

        // select "Amont-B1" basin
        const nodes = await $$("g.nodes > g");
        await nodes[8].click();
        await browser.pause(200);

        // modify 1st structure discharge law
        const sel = await calcPage.getSelectById("select_loidebit");
        await changeSelectValue(sel, 3); // Larinier
        await browser.pause(200);

        // check 2nd structure ZDV
        const inpZDV2 = await calcPage.getInputById("1_ZDV");
        expect(await inpZDV2.getValue()).toEqual("96.25")

        // run computation
        const calcBtn = await calcPage.getCalculateButton();
        await calcBtn.click();
        await browser.pause(300);

        // back to inputs tab
        const resSelectors = await $$("#pb-data-results-selector div div");
        scrollToElement(resSelectors[0]);
        await browser.pause(200);
        await resSelectors[0].click();
        await browser.pause(200);

        // check 2nd structure ZDV didn't change
        expect(await inpZDV2.getValue()).toEqual("96.25")
    });
});
