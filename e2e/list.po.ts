import { browser, $, $$, expect } from '@wdio/globals'

export class ListPage {
    navigateTo() {
        return browser.url("/#/list");
    }

    getThemesCards() {
        return $$("mat-card.compute-nodes-theme");
    }

    async getThemesCardsLength() {
        return await this.getThemesCards().length;
    }

    getCalculatorsMenuEntries() {
        return $$("mat-card.compute-nodes-theme button.theme-calculator");
    }

    async getCalculatorsMenuLength() {
        return await this.getCalculatorsMenuEntries().length;
    }

    async getAvailableCalcTypes() {
        const ids = [];
        const menuEntries = await this.getCalculatorsMenuEntries();
        for (const elt of menuEntries) {
            const eltid = await elt.getAttribute("id");
            const ct = eltid.replace("create-calc-", "");
            const nct = Number(ct);
            // remove duplicates
            if (!ids.includes(nct)) {
                ids.push(nct);
            }
        };
        return ids;
    }

    async clickRandomCalculatorMenuEntry() {
        const menuEntries = await this.getCalculatorsMenuEntries();
        const l = menuEntries.length;
        const r = Math.min((Math.floor(Math.random() * l)), (l - 1));
        return await menuEntries[r].click();
    }

    async clickMenuEntryForCalcType(type: number) {
        const but = await $("#create-calc-" + type);
        return await but.click();
    }

    async getCalcMenuTextForCalcType(type: number): Promise<string> {
        const but = await $("#create-calc-" + type);
        return but.getText();
    }
}
