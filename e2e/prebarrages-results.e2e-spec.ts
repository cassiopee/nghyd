import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { loadSession, openCalculator, scrollToElement } from "./util.po";
import { SideNav } from "./sidenav.po";

/**
 * Clone calculators
 */
describe("Prébarrages results - ", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let prefPage: PreferencesPage;
    let sidenav: SideNav;

    beforeAll(() => {
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        prefPage = new PreferencesPage();
        sidenav = new SideNav();
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000 * 1000;
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    it("every module shoud have results", async () => {
        // create prébarrage calculator
        await openCalculator(30, navBar, listPage);

        // run calculation
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(200);

        const nodes = await $$("g.nodes > g");
        const nb = nodes.length;
        for (let n = 0; n < nb; n++) {
            const node = nodes[n];
            await scrollToElement(node);
            await browser.pause(100);
            await node.click();
            await browser.pause(200);

            // check that result is not empty
            const hasResults = await calcPage.hasResults();
            expect(hasResults).toBe(true);
        }
    });

    it("an error in calculation must lead to empty results for all walls/basins", async () => {
        // load session file
        await loadSession(navBar, sidenav, "./session/session-prebarrage-calculation-error.json");

        // click "compute" button
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(300);

        // check no result is available
        const nodes = await $$("g.nodes > g");
        const nb = nodes.length;
        for (let n = 0; n < nb; n++) {
            const node = nodes[n];
            await scrollToElement(node);
            await browser.pause(100);
            await node.click();
            await browser.pause(200);

            // check that result is empty
            const hasResults = await calcPage.hasResults();
            expect(hasResults).toBe(false);
        }
    });
});
