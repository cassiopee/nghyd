// cannot import JaLHyd here :/
// @WARNING keep in sync if CalculatorType enum order changes in JaLHyd
export const testedCalcTypes = [
    0,
    // omit 1 - LechaptCalmon
    2, 3, 4, 5, 6,
    // omit 7 - Structure
    8, 9, 10, 11, 12, 13,
    // omit 14 -Section
    15,
    // omit 16 - CloisonAval
    17, 18, 19, 20, 21, 22, 23, 24, 25,
    // omit 26 - YAXN
    27, 28, 29, 30,
    // omit 31 - PbCloison and 32 - PbBassin
    33, 34, 35
];
