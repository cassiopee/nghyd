import { AppPage } from "./app.po";

/**
 * Start app
 */
describe("ngHyd − start page", () => {
    let page: AppPage;

    beforeAll(() => {
        page = new AppPage();
    });

    it("when app starts, user should be redirected to /list page", async () => {
        await page.navigateTo();
        const url = await browser.getUrl();
        expect(url).toContain("/list");
    });
});
