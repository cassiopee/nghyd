import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { CalculatorPage } from "./calculator.po";
import { changeSelectValue, openCalculator } from "./util.po";
import { browser } from "@wdio/globals";

describe("linked parameter in calculator with section - ", () => {
    let listPage: ListPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        listPage = new ListPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
        prefPage = new PreferencesPage();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    it("modify section type", async () => {
        // browser.manage().window().setPosition(2000, 30);

        // open first "parametric section" calculator
        await openCalculator(2, navBar, listPage);

        // open second "parametric section" calculator
        await openCalculator(2, navBar, listPage);

        // set Q parameter to linked mode
        let inputQ = await calcPage.getInputById("Q");
        await calcPage.setParamMode(inputQ, "link");

        // change section type
        await changeSelectValue(await calcPage.getSelectById("select_section"), 3); // mode "parabolique"

        // check Q is still in linked mode
        inputQ = await calcPage.getInputById("Q"); // re-request input to avoid "Request encountered a stale element - terminating request" warning
        expect(await calcPage.inputIsInLinkedMode(inputQ)).toBe(true);
    });
});
