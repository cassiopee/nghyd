import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { CalculatorPage } from "./calculator.po";
import { PreferencesPage } from "./preferences.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

describe("Check fields are empty in 'parametric section' calculator when created with 'empty fields' option", () => {
    let listPage: ListPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        listPage = new ListPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
        prefPage = new PreferencesPage();
    });

    beforeEach(async () => {
        // enable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.enableEvilEmptyFields();
        await browser.pause(200);
    });

    it("", async () => {
        // open "parametric section" calculator
        await openCalculator(2, navBar, listPage);

        expect(await calcPage.checkEmptyOrFilledFields(["LargeurBerge", "Ks", "If", "YB", "Q", "Y"], [true, true, true, true, true, true]));
    });
});
