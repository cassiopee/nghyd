import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { PreferencesPage } from "./preferences.po";
import { Navbar } from "./navbar.po";
import { changeSelectValue, openApplication, openCalculator } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { AppPage } from "./app.po";

/**
 * Check that created/cloned structures have empty fields when
 * "empty fields on calculator creation" is enabled
 */
describe("ngHyd - check that created/cloned structures have empty fields - ", () => {
    let prefPage: PreferencesPage;
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let startPage: AppPage;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        startPage = new AppPage();
    });

    beforeEach(async () => {
        // enable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.enableEvilEmptyFields();
        await browser.pause(200);
    });

    async function setup() {
        // start page
        await openApplication(startPage);

        // open structures calculator
        await openCalculator(8, navBar, listPage);
    }

    it("when a structure calculator is created", async () => {
        await setup();

        // check 1st structure empty fields
        const inputIds = ["Q", "Z1", "Z2", "0_ZDV", "0_L", "0_W", "0_CdGR"];
        const emptys = [true, true, true, true, true, true, false];
        await calcPage.checkEmptyOrFilledFields(inputIds, emptys);

        // change 1st structure type to rectangular weir
        const structSelect = await calcPage.getSelectById("select_structure");
        await changeSelectValue(structSelect, 1);
        await browser.pause(200);

        // check 1st structure empty fields
        const inputIds2 = ["Q", "Z1", "Z2", "0_ZDV", "0_L", "0_CdWR"];
        const emptys2 = [true, true, true, true, true, false];
        await calcPage.checkEmptyOrFilledFields(inputIds2, emptys2);
    });

    it("when a structure is added", async () => {
        await setup();

        // add structure
        const addStruct = await calcPage.getAddStructureButton();
        await addStruct.click();
        await browser.pause(200);

        // check 2nd structure empty fields
        const inputIds = ["1_ZDV", "1_L", "1_W", "1_CdGR"];
        const emptys = [true, true, true, false];
        await calcPage.checkEmptyOrFilledFields(inputIds, emptys);
    });

    it("when a structure is copied", async () => {
        await setup();

        // copy structure
        const addStruct = await calcPage.getCopyStructureButton();
        await addStruct.click();
        await browser.pause(200);

        // check 2nd structure empty fields
        const inputIds = ["1_ZDV", "1_L", "1_W", "1_CdGR"];
        const emptys = [true, true, true, false];
        await calcPage.checkEmptyOrFilledFields(inputIds, emptys);
    });

    it("when a modified structure is copied (type)", async () => {
        await setup();

        // change 1st structure type to rectangular weir
        const structSelect = await calcPage.getSelectById("select_structure");
        await changeSelectValue(structSelect, 1);
        await browser.pause(200);

        // copy structure
        const addStruct = await calcPage.getCopyStructureButton();
        await addStruct.click();
        await browser.pause(200);

        // change 2nd structure type to rectangular gate
        const selects = await $$("mat-select#select_structure");
        const structSelect2 = selects[1];
        await changeSelectValue(structSelect2, 5);
        await browser.pause(200);

        // check empty fields
        const inputIds = ["1_ZDV", "1_L", "1_W", "1_CdGR"];
        const emptys = [true, true, true, false];
        await calcPage.checkEmptyOrFilledFields(inputIds, emptys);
    });

    it("when a modified structure is copied (input)", async () => {
        await setup();

        // fill 
        const inp = await calcPage.getInputById("0_ZDV");
        await inp.clearValue();
        await inp.setValue("1");

        // copy structure
        const addStruct = await calcPage.getCopyStructureButton();
        await addStruct.click();
        await browser.pause(200);

        // check empty fields
        const inputIds = ["1_ZDV", "1_L", "1_W", "1_CdGR"];
        const emptys = [false, true, true, false];
        await calcPage.checkEmptyOrFilledFields(inputIds, emptys);
    });

    it("except for discharge coefficient when a Larinier weir is used", async () => {
        await setup();

        // change 1st structure type to rectangular weir
        const structSelect = await calcPage.getSelectById("select_structure");
        await changeSelectValue(structSelect, 1);
        await browser.pause(200);

        // change discharge law to Larinier
        const dischargeSelect = await calcPage.getSelectById("select_loidebit");
        await changeSelectValue(dischargeSelect, 3);
        await browser.pause(200);

        // check empty fields
        const inputIds = ["0_ZDV", "0_L", "0_CdWSL"];
        const emptys = [true, true, true];
        await calcPage.checkEmptyOrFilledFields(inputIds, emptys);
    });

    it("when a structure is modified (input) and then a structure is added", async () => {
        await setup();

        // fill 
        const inp = await calcPage.getInputById("0_ZDV");
        await inp.clearValue();
        await inp.setValue("1");

        // copy structure
        const addStruct = await calcPage.getAddStructureButton();
        await addStruct.click();
        await browser.pause(200);

        // check empty fields
        const inputIds = ["1_ZDV", "1_L", "1_W", "1_CdGR"];
        const emptys = [true, true, true, false];
        await calcPage.checkEmptyOrFilledFields(inputIds, emptys);
    });
});
