import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { AppPage } from "./app.po";
import { SideNav } from "./sidenav.po";
import { PreferencesPage } from "./preferences.po";
import { changeSelectValue, loadSession, newSession, openCalculator, scrollPageToTop } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * Clone calculators
 */
describe("ngHyd − Passe à Bassins", () => {
    let startPage: AppPage;
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let sideNav: SideNav;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        startPage = new AppPage();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        sideNav = new SideNav();
        prefPage = new PreferencesPage();
    });

    describe("create PAB - ", () => {
        beforeEach(async () => {
            // disable evil option "empty fields on module creation"
            await prefPage.navigateTo();
            await prefPage.disableEvilEmptyFields();
            await browser.pause(200);
        });

        it("when PAB is created from scratch", async () => {
            // create PAB
            await openCalculator(15, navBar, listPage);

            // check that pab-table is present
            const innerFieldsets = await $$(".pab-data-table");
            expect(await innerFieldsets.length).toBe(1);

            // calculate PAB
            const calcButton = await calcPage.getCalculateButton();
            await calcButton.click();
            await browser.pause(200);

            // check that result is not empty
            const hasResults = await calcPage.hasResults();
            expect(hasResults).toBe(true);

            // check absence of logs
            expect(await calcPage.nbLogEntries()).toBe(0);
        });

        it("complete example of all PAB modules", async () => {
            // PAB - chute
            await openCalculator(12, navBar, listPage);

            const c_Z1 = await calcPage.getInputById("Z1");
            await c_Z1.setValue("78.27");
            const c_Z2 = await calcPage.getInputById("Z2");
            await c_Z2.setValue("74.86");

            // PAB - nombre
            await openCalculator(13, navBar, listPage);

            const n_DHT = await calcPage.getInputById("DHT");
            await calcPage.setParamMode(n_DHT, "link");
            const n_DH = await calcPage.getInputById("DH");
            await calcPage.setParamMode(n_DH, "cal");
            const n_N = await calcPage.getInputById("N");
            await n_N.clearValue();
            await n_N.setValue("15");

            // PAB - dimensions
            await openCalculator(5, navBar, listPage);

            const d_V = await calcPage.getInputById("V");
            await calcPage.setParamMode(d_V, "cal");
            const d_Y = await calcPage.getInputById("Y");
            await d_Y.setValue("1.5");
            const d_L = await calcPage.getInputById("L");
            await d_L.setValue("3.100");
            const d_W = await calcPage.getInputById("W");
            await d_W.setValue("2.5");

            // PAB - puissance dissipée (volume)
            await openCalculator(6, navBar, listPage);

            const p_DH = await calcPage.getInputById("DH");
            await calcPage.setParamMode(p_DH, "link");
            const p_Q = await calcPage.getInputById("Q");
            await calcPage.setParamMode(p_Q, "cal");
            const p_V = await calcPage.getInputById("V");
            await calcPage.setParamMode(p_V, "link");
            const p_PV = await calcPage.getInputById("PV");
            await p_PV.clearValue();
            await p_PV.setValue("150");

            // PAB - cloisons
            await openCalculator(10, navBar, listPage);

            const cl_LB = await calcPage.getInputById("LB");
            await calcPage.setParamMode(cl_LB, "link");
            const cl_BB = await calcPage.getInputById("BB");
            await calcPage.setParamMode(cl_BB, "link");
            const cl_DH = await calcPage.getInputById("DH");
            await calcPage.setParamMode(cl_DH, "link");
            const cl_Z1 = await calcPage.getInputById("Z1");
            await calcPage.setParamMode(cl_Z1, "link");
            const cl_PB = await calcPage.getInputById("PB");
            await cl_PB.setValue("1.5");

            // calculate Cloisons
            const calcButtonCl = await calcPage.getCalculateButton();
            await calcButtonCl.click();
            await browser.pause(200);

            // make sure "Generate PAB" button is visible (it might be hidden behind navbar)
            await scrollPageToTop();
            // generate PAB
            const genButton = await calcPage.getGeneratePabButton();
            // await genButton.isExisting();
            // await genButton.isDisplayed();
            await genButton.click();
            await browser.pause(1000);
            const nbBassins = await calcPage.getInputById("generatePabNbBassins");
            await nbBassins.setValue("9");

            // click "Generate"
            // await $("dialog-generate-pab button#do-generate").click();
            const doGen = await $("dialog-generate-pab button#do-generate");
            await doGen.click();
            await browser.pause(1000);

            // calculate PAB
            const calcButtonPAB = await calcPage.getCalculateButton();
            await calcButtonPAB.click();
            await browser.pause(500);

            // check that result is not empty
            const hasResults = await calcPage.hasResults();
            expect(hasResults).toBe(true);
        });

    });

    describe("generate PAB - ", () => {

        beforeEach(async () => {
            // disable evil option "empty fields on module creation"
            await prefPage.navigateTo();
            await prefPage.disableEvilEmptyFields();
            await browser.pause(200);
        });

        it("from a Cloisons among many", async () => {
            await newSession(navBar, sideNav);

            // create many Cloisons
            await openCalculator(10, navBar, listPage);
            await openCalculator(10, navBar, listPage);
            await openCalculator(10, navBar, listPage);

            // choose one of them and change its parameters
            await navBar.clickCalculatorTab(1);
            const Z1 = await calcPage.getInputById("Z1");
            await Z1.setValue("114");
            const LB = await calcPage.getInputById("LB");
            await LB.setValue("11.5");
            const DH = await calcPage.getInputById("DH");
            await DH.setValue("0.72");

            // calculate Cloisons
            const calcButtonCl = await calcPage.getCalculateButton();
            await calcButtonCl.click();
            await browser.pause(200);

            // make sure "Generate PAB" button is visible (it might be hidden behind navbar)
            await scrollPageToTop();
            // create PAB from it, changing modal parameters
            const genButton = await calcPage.getGeneratePabButton();
            await genButton.click();
            await browser.pause(1000);

            const debit = await calcPage.getInputById("generatePabDebit");
            expect(await debit.getValue()).toBe("1.128");
            await debit.clearValue();
            // send "1.6" in 3 movements, because "." triggers an error and Angular can't cope with the subsequent keys
            await debit.setValue("1");
            await debit.addValue(".");
            await debit.addValue("6");
            const coteAmont = await calcPage.getInputById("generatePabCoteAmont");
            expect(await coteAmont.getValue()).toBe("114");
            await coteAmont.setValue("115");
            const nbBassins = await calcPage.getInputById("generatePabNbBassins");
            expect(await nbBassins.getValue()).toBe("6");
            await nbBassins.setValue("5");

            // click "Generate"
            const btnGenerate = await $("dialog-generate-pab button#do-generate");
            await btnGenerate.click();
            await browser.pause(1000);

            // check parameters values
            const P_Q = await calcPage.getInputById("Q");
            expect(await P_Q.getValue()).toBe("1.6");
            const P_Z2 = await calcPage.getInputById("Z2");
            expect(await P_Z2.getValue()).toBe("111.4");

            // check number of basins
            const innerFieldsets = await $$("td.basin_number");
            expect(innerFieldsets.length).toBe(5);

            // calculate PAB
            const calcButton = await calcPage.getCalculateButton();
            await calcButton.click();
            await browser.pause(200);

            // check that result is not empty
            const hasResults = await calcPage.hasResults();
            expect(hasResults).toBe(true);
        });
    });

    describe("load session files - ", () => {
        it("complete example of all PAB modules", async () => {
            await startPage.navigateTo();
            // load
            await loadSession(navBar, sideNav, "./session/session-pab-complete.json");

            // check existence of the loaded modules
            expect(await navBar.getAllCalculatorTabs().length).toBe(6);

            // check parameters values
            await navBar.clickCalculatorTab(5);
            await browser.pause(700);
            const P_Q = await calcPage.getInputById("Q");
            expect(await P_Q.getValue()).toBe("0.275");
            const P_Z2 = await calcPage.getInputById("Z2");
            expect(await P_Z2.getValue()).toBe("74.865");
            // check number of basins
            const innerFieldsets = await $$("td.basin_number");
            expect(await innerFieldsets.length).toBe(15);

            // calculate PAB
            const calcButton = await calcPage.getCalculateButton();
            await calcButton.click();
            await browser.pause(200);

            // check that result is not empty
            const hasResults = await calcPage.hasResults();
            expect(hasResults).toBe(true);

            // check absence of logs
            expect(await calcPage.nbLogEntries()).toBe(0);
        });
    });

    describe("load regulated variated PAB with calc errors - ", () => {
        it("should display logs", async () => {
            await startPage.navigateTo();
            await browser.pause(500);
            // load
            await loadSession(navBar, sideNav, "./session/session-pab-regulee-variee.json");
            // check existence of the loaded module
            expect(await navBar.getAllCalculatorTabs().length).toBe(1);
            await navBar.clickCalculatorTab(0);

            // calculate
            const calcButton = await calcPage.getCalculateButton();
            await calcButton.click();
            await browser.pause(200);

            // check that result is not empty
            const hasResults = await calcPage.hasResults();
            expect(hasResults).toBe(true);

            // check presence of logs
            await browser.pause(300);
            expect(await calcPage.nbLogEntries()).toBe(2);

            // change iteration
            const pve = await calcPage.getSelectById("pab-variating-element");
            await changeSelectValue(pve, 3);
            await browser.pause(300);
            // check absence of logs
            expect(await calcPage.nbLogEntries()).toBe(2);
        });
    });
});
