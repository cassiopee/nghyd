import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { SideNav } from "./sidenav.po";
import { changeSelectValue, loadSession, openCalculator } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * Remous
 */
describe("ngHyd − remous", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let prefPage: PreferencesPage;
    let sidenav: SideNav;

    beforeAll(() => {
        listPage = new ListPage();
        prefPage = new PreferencesPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        sidenav = new SideNav();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    it("when leaving calculated remous page then coming back to it, logs should not be duplicated", async () => {

        // 1. create new Remous
        await openCalculator(4, navBar, listPage);

        // 2. calculate it, there should be 6 messages in the log
        const calcBtn = await calcPage.getCalculateButton()
        await calcBtn.click();
        await browser.pause(200);

        expect(await calcPage.nbLogEntries()).toBe(6);
        await browser.pause(300);

        // 3. go to home page (simplest example)
        await navBar.clickNewCalculatorButton();
        await browser.pause(300);

        // 4. go back to Remous
        await navBar.clickCalculatorTab(0);
        await browser.pause(300);

        // 5. there should still be 6 messages in the log
        expect(await calcPage.nbLogEntries()).toBe(6);
    });

    it("Calculation with large bed width should run successfully", async () => {
        // 1. create new Remous
        await openCalculator(4, navBar, listPage);

        // 2. Set to trapezoidal section with bank slope of 2m/m and 20 meter width bed
        await changeSelectValue(await calcPage.getSelectById("select_section"), 2);
        await browser.pause(300);
        await calcPage.getInputById("LargeurFond").clearValue();
        await browser.pause(300);
        await calcPage.getInputById("LargeurFond").setValue("20");
        await calcPage.getInputById("Fruit").clearValue();
        await browser.pause(300);
        await calcPage.getInputById("Fruit").setValue("2");

        // 3. Calculate, the calculation should succeed
        const calcBtn = await calcPage.getCalculateButton()
        await calcBtn.click();
        await browser.pause(200);

        const hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(true);
    });

    it("Check that calculation with dx >= reach length fails", async () => {
        // load backwater curve session file with Dx>=Long
        await loadSession(navBar, sidenav, "./session/session-remous-dx-bief.json");

        // Calculate, the calculation should fail
        const calcBtn = await calcPage.getCalculateButton()
        await calcBtn.click();
        await browser.pause(200);

        // check error message in log
        expect(await calcPage.nbLogEntries()).toBe(1);

        // check there is no result
        expect(await calcPage.presentAndVisible("var-results")).toBe(false);
    });
});
