import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { SideNav } from "./sidenav.po";
import { PreferencesPage } from "./preferences.po";
import { changeSelectValue, loadSession, newSession, openCalculator } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * Uses an example configuration to calculate :
 *  - with a parameter linked to a single parameter
 *  - with a parameter linked to a single parameter, plus local variated parameter
 *  - with a parameter linked to a variated parameter
 *  - with a parameter linked to a single result
 *  - with a parameter linked to a single result, plus local variated parameter
 *  - with a parameter linked to a variated result
 *  - with a parameter linked to a single extra result
 *  - with a parameter linked to a single extra result, plus local variated parameter
 *  - with a parameter linked to a variated extra result
 *
 *  => plus all those combinations in indeirect link mode (linked parameter target
 *     is also a linked parameter)
 */
describe("ngHyd − calculate with linked parameters", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let sideNav: SideNav;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        prefPage = new PreferencesPage();
        sideNav = new SideNav();
    });

    beforeEach(async () => {
        await newSession(navBar, sideNav);

        // disable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await browser.pause(200);
        await prefPage.disableEvilEmptyFields();
        await browser.pause(100);
    });

    async function computeAndCheckPresenceOfResults() {
        // check that "compute" button is active
        const calcButton = await calcPage.checkCalcButtonEnabled(true);
        // click "compute" button
        await calcButton.click();
        await browser.pause(200);
        // check that result is not empty
        const hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(true);
    }

    it(" − direct links : parameter linked to a single parameter", async () => {
        // create a Régime uniforme
        await openCalculator(3, navBar, listPage);

        // create a PAB : dimensions
        await openCalculator(5, navBar, listPage);

        // link Y to Y (R uniforme)
        const Y = await calcPage.getInputById("Y");
        await calcPage.setParamMode(Y, "link");
        const sel = await calcPage.getLinkedValueSelect(Y);
        await changeSelectValue(sel, 0);

        await computeAndCheckPresenceOfResults();
    });

    it(" − direct links : parameter linked to a single parameter, plus local variated parameter", async () => {
        // create a Régime uniforme
        await openCalculator(3, navBar, listPage);

        // create a PAB : dimensions
        await openCalculator(5, navBar, listPage);

        // link Y to Y (R uniforme)
        const Y = await calcPage.getInputById("Y");
        await calcPage.setParamMode(Y, "link");
        const sel = await calcPage.getLinkedValueSelect(Y);
        await changeSelectValue(sel, 0);
        // vary W
        const W = await calcPage.getInputById("W");
        await calcPage.setParamMode(W, "var");

        await computeAndCheckPresenceOfResults();
    });

    it(" − direct links : parameter linked to a variated parameter", async () => {
        // create a Régime uniforme
        await openCalculator(3, navBar, listPage);

        // vary Y
        const Y1 = await calcPage.getInputById("Y");
        await calcPage.setParamMode(Y1, "var");

        // create a PAB : dimensions
        await openCalculator(5, navBar, listPage);

        // link Y to Y (R uniforme)
        const Y2 = await calcPage.getInputById("Y");
        await calcPage.setParamMode(Y2, "link");
        const sel = await calcPage.getLinkedValueSelect(Y2);
        await changeSelectValue(sel, 0);

        await computeAndCheckPresenceOfResults();
    });

    it(" − direct links : parameter linked to a single result", async () => {
        // create a Régime uniforme
        await openCalculator(3, navBar, listPage);

        // calculate Y
        const Y1 = await calcPage.getInputById("Y");
        await calcPage.setParamMode(Y1, "cal");

        // create a PAB : dimensions
        await openCalculator(5, navBar, listPage);

        // link Y to Y (R uniforme)
        const Y2 = await calcPage.getInputById("Y");
        await calcPage.setParamMode(Y2, "link");
        const sel = await calcPage.getLinkedValueSelect(Y2);
        await changeSelectValue(sel, 0);

        await computeAndCheckPresenceOfResults();
    });

    it(" − direct links : parameter linked to a single result, plus local variated parameter", async () => {
        // create a Régime uniforme
        await openCalculator(3, navBar, listPage);

        // calculate Y
        const Y1 = await calcPage.getInputById("Y");
        await calcPage.setParamMode(Y1, "cal");

        // create a PAB : dimensions
        await openCalculator(5, navBar, listPage);

        // link Y to Y (R uniforme)
        const Y2 = await calcPage.getInputById("Y");
        await calcPage.setParamMode(Y2, "link");
        const sel = await calcPage.getLinkedValueSelect(Y2);
        await changeSelectValue(sel, 0);
        // vary W
        const W = await calcPage.getInputById("W");
        await calcPage.setParamMode(W, "var");

        await computeAndCheckPresenceOfResults();
    });

    it(" − direct links : parameter linked to a variated result", async () => {
        // create a Régime uniforme
        await openCalculator(3, navBar, listPage);

        // vary Q
        const Q = await calcPage.getInputById("Q");
        await calcPage.setParamMode(Q, "var");
        // calculate Y
        const Y1 = await calcPage.getInputById("Y");
        await calcPage.setParamMode(Y1, "cal");

        // create a PAB : dimensions
        await openCalculator(5, navBar, listPage);

        // link Y to Y (R uniforme)
        const Y2 = await calcPage.getInputById("Y");
        await calcPage.setParamMode(Y2, "link");
        const sel = await calcPage.getLinkedValueSelect(Y2);
        await changeSelectValue(sel, 0);

        await computeAndCheckPresenceOfResults();
    });

    it(" − direct links : parameter linked to a single extra result", async () => {
        // create a Régime uniforme
        await openCalculator(3, navBar, listPage);

        // create a Jet
        await openCalculator(18, navBar, listPage);

        // link V0 to V (Régime uniforme)
        const V0 = await calcPage.getInputById("V0");
        await calcPage.setParamMode(V0, "link");

        await computeAndCheckPresenceOfResults();
    });

    it(" − direct links : parameter linked to a single extra result, plus local variated parameter", async () => {
        // create a Régime uniforme
        await openCalculator(3, navBar, listPage);

        // create a Jet
        await openCalculator(18, navBar, listPage);

        // link V0 to V (Régime uniforme)
        const V0 = await calcPage.getInputById("V0");
        await calcPage.setParamMode(V0, "link");
        // vary ZJ
        const ZJ = await calcPage.getInputById("ZJ");
        await calcPage.setParamMode(ZJ, "var");

        await computeAndCheckPresenceOfResults();
    });

    it(" − direct links : parameter linked to a variated extra result", async () => {
        // create a Régime uniforme
        await openCalculator(3, navBar, listPage);

        // vary LargeurBerge
        const LargeurBerge = await calcPage.getInputById("LargeurBerge");
        await calcPage.setParamMode(LargeurBerge, "var");

        // create a Jet
        await openCalculator(18, navBar, listPage);

        // link V0 to V (Régime uniforme)
        const V0 = await calcPage.getInputById("V0");
        await calcPage.setParamMode(V0, "link");

        await computeAndCheckPresenceOfResults();
    });

    it(" − multiple variated parameters, including a linked one (#253)", async () => {
        // load session file
        await navBar.clickNewCalculatorButton();
        await browser.pause(200);

        await loadSession(navBar, sideNav, "./session/session-multivar-link.json");

        expect(await navBar.getAllCalculatorTabs().length).toBe(2);

        // calculate
        await navBar.clickCalculatorTab(0);
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(200);

        // check that result is not empty
        const hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(true);

        // check that there are 6 parameters columns in the variated results table :
        // Q, Z2, Z1, + 4 for device #1
        expect(await calcPage.getAllVariatedResultsTableHeaders().length).toBe(7);

        // check that number of rows in the variated results table equals number of variated values
        const varRows = await calcPage.getAllVariatedResultsRows();
        expect(varRows.length).toBe(191); // boundaries are included

        // check that all parameters have values for all iterations (Z2 might miss some if repeat strategy is not applied)
        const lastRow = varRows[varRows.length - 1];
        const tds = await lastRow.$$("td");
        tds.forEach((td) => {
            expect(td.getText()).not.toBe("");
        });
    });

    it(" − bug nghyd#329 : unexpected ERR in results table", async () => {
        await navBar.clickNewCalculatorButton();
        await browser.pause(200);

        // load session
        await loadSession(navBar, sideNav, "./session/session-pab-chain-nghyd-329.json");
        expect(await navBar.getAllCalculatorTabs().length).toBe(3);

        // calculate PAB-Dimensions
        await navBar.clickCalculatorTab(2);
        await browser.pause(200);
        // click "compute" button
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(300);
        // check that result is not empty
        expect(await calcPage.hasResults()).toBe(true);

        // check that ERR is not present in Volume line of results table
        let frt = await calcPage.getFixedResultsTable();
        let volumeRow = await calcPage.getNthRow(frt, 3);
        let volumeCol = await calcPage.getNthColumn(volumeRow, 2);
        let volume = await volumeCol.getText();
        expect(Number(volume)).toBeCloseTo(44.565, 3);

        // click PAB-Nombre tab
        await navBar.clickCalculatorTab(0);
        await browser.pause(200);

        // go back to PAB-Dimensions
        await navBar.clickCalculatorTab(2);
        await browser.pause(200);

        // check that ERR is not present in Volume line of results table
        frt = await calcPage.getFixedResultsTable();
        volumeRow = await calcPage.getNthRow(frt, 3);
        volumeCol = await calcPage.getNthColumn(volumeRow, 2);
        volume = await volumeCol.getText();
        expect(Number(volume)).toBeCloseTo(44.565, 3);
    });

    it(" − a link target parameter should not be able to link to another parameter", async () => {
        // create 1st PAB-Chute
        await openCalculator(12, navBar, listPage);

        //  upstream water level should not have link mode (only one calculator)
        let Z1_1 = await calcPage.getInputById("Z1");
        expect(await calcPage.inputHasLinkModeButton(Z1_1)).toBe(false);

        // create 2nd PAB-Chute
        await openCalculator(12, navBar, listPage);

        // back to 1st calculator
        await navBar.clickCalculatorTab(0);
        await browser.pause(200);

        //  upstream water level should have link mode (now there are 2 calculators)
        Z1_1 = await calcPage.getInputById("Z1"); // re-request input to avoid "Request encountered a stale element - terminating request" warning (due to tab change)
        expect(await calcPage.inputHasLinkModeButton(Z1_1)).toBe(true);

        // back to 2nd calculator
        await navBar.clickCalculatorTab(1);
        await browser.pause(200);

        // link upstream water level in 2nd calculator to upstream water level in 1st one
        const Z1_2 = await calcPage.getInputById("Z1");
        await calcPage.setParamMode(Z1_2, "link");

        // back to 1st calculator
        await navBar.clickCalculatorTab(0);
        await browser.pause(200);

        //  upstream water level should not have link mode (already a link target)
        Z1_1 = await calcPage.getInputById("Z1"); // re-request input to avoid "Request encountered a stale element - terminating request" warning (due to tab change)
        expect(await calcPage.inputHasLinkModeButton(Z1_1)).toBe(false);
    });
});
