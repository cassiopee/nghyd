import { ListPage } from "./list.po";
import { Navbar } from "./navbar.po";
import { CalculatorPage } from "./calculator.po";
import { PreferencesPage } from "./preferences.po";
import { changeSelectValue, openCalculator } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

describe("Pressure loss - ", () => {
    let listPage: ListPage;
    let navBar: Navbar;
    let calcPage: CalculatorPage;
    let prefPage: PreferencesPage;

    beforeAll(async () => {
        listPage = new ListPage();
        navBar = new Navbar();
        calcPage = new CalculatorPage();
        prefPage = new PreferencesPage();
    });

    beforeEach(async () => {
        // enable evil option "empty fields on module creation"
        await prefPage.navigateTo();
        await prefPage.disableEvilEmptyFields();
        await browser.pause(200);
    });

    describe("modify pressure loss law displays the appropriate fields - ", () => {
        it("Lechapt&Calmon", async () => {
            // open "pressure loss" calculator
            await openCalculator(35, navBar, listPage);

            // select Lechapt-Calmon pressure loss law
            const materialSelect = await calcPage.getSelectById("select_pressurelosstype");
            await changeSelectValue(materialSelect, 0);
            await browser.pause(200);

            // check inputs presence
            expect(await calcPage.isNgParamPresent("Q")).toBe(true);
            expect(await calcPage.isNgParamPresent("D")).toBe(true);
            expect(await calcPage.isNgParamPresent("J")).toBe(true); // en calcul (donc id=calc_J mais ça marche quand même !)
            expect(await calcPage.isNgParamPresent("Lg")).toBe(true);
            expect(await calcPage.isNgParamPresent("Kloc")).toBe(true);

            // check material select
            expect(await calcPage.isMatSelectPresent("select_material")).toBe(true);
        });

        it("Strickler", async () => {
            // open "pressure loss" calculator
            await openCalculator(35, navBar, listPage);

            // select Strickler pressure loss law
            const materialSelect = await calcPage.getSelectById("select_pressurelosstype");
            await changeSelectValue(materialSelect, 1);
            await browser.pause(200);

            // check inputs presence

            expect(await calcPage.isNgParamPresent("0_Ks")).toBe(true);
            expect(await calcPage.isNgParamPresent("Q")).toBe(true);
            expect(await calcPage.isNgParamPresent("D")).toBe(true);
            expect(await calcPage.isNgParamPresent("J")).toBe(true);
            expect(await calcPage.isNgParamPresent("Lg")).toBe(true);
            expect(await calcPage.isNgParamPresent("Kloc")).toBe(true);
        });
    });
});
