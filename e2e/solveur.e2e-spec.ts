import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { SideNav } from "./sidenav.po";
import { PreferencesPage } from "./preferences.po";
import { changeSelectValue, loadSession, newSession, openCalculator, scrollPageToTop } from "./util.po";
import { browser, $, $$, expect } from '@wdio/globals'

/**
 * Clone calculators
 */
describe("Solveur - ", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let sideNav: SideNav;
    let prefPage: PreferencesPage;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        sideNav = new SideNav();
    });

    beforeEach(async () => {
        // disable evil option "empty fields on module creation"
        await prefPage.setEmptyFields(false);
        // force language to prevent issues due to default browser language
        await prefPage.changeLanguage(1); // fr
        await browser.pause(200);
        await navBar.clickNewCalculatorButton();
    });

    it("load > calculate", async () => {
        await loadSession(navBar, sideNav, "./session/session-solveur-chutes.json");

        expect(await navBar.getAllCalculatorTabs().length).toBe(4);
        await navBar.clickCalculatorTab(3); // n°3 should be the latest

        // check input values
        const inpXinit = await calcPage.getInputById("Xinit");
        expect(await inpXinit.getValue()).toBe("0.5");
        const inpYtarget = await calcPage.getInputById("Ytarget");
        expect(await inpYtarget.getValue()).toBe("252");
        // check Nub to calculate
        const ntc = await calcPage.getSelectById("select_target_nub");
        const ntcV = await calcPage.getSelectValueText(ntc);
        expect(ntcV).toContain("PAB : puissance / Puissance dissipée (PV)");
        // check targetted result
        let ntt = await calcPage.getSelectById("select_target_result");
        const nttV = await calcPage.getSelectValueText(ntt);
        expect(nttV).toContain("Puissance dissipée (PV)");
        // check searched Parameter
        const sp = await calcPage.getSelectById("select_searched_param");
        const spV = await calcPage.getSelectValueText(sp);
        expect(spV).toContain("Z2 - Cote aval (PAB : chute)");

        // check that "compute" button is active
        const calcButton = await calcPage.checkCalcButtonEnabled(true);
        // click "compute" button
        await calcButton.click();
        await browser.pause(200);

        // check that result is not empty
        const hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(true);

        // change targetted Nub, check that targetted result changes too
        await changeSelectValue(ntc, 0);
        ntt = await calcPage.getSelectById("select_target_result"); // re-request element to avoid "Request encountered a stale element - terminating request" warning (probably due to select change)
        const nttV2 = await calcPage.getSelectValueText(ntt);
        expect(nttV2).not.toContain("Puissance dissipée (PV)");
    });

    it("create > feed > calculate > clone > calculate clone", async () => {
        await newSession(navBar, sideNav);

        // 1. create empty Solveur
        await openCalculator(22, navBar, listPage);

        // 2. create PAB:Chute, PAB:Nombre and PAB:Puissance linked to one another
        await openCalculator(12, navBar, listPage); // PAB:Chute
        await openCalculator(13, navBar, listPage); // PAB:Nombre

        // link DHT to PAB:Chute.DH
        const dht = await calcPage.getInputById("DHT");
        await calcPage.setParamMode(dht, "link");
        // Calculate DH
        const dh_nombre = await calcPage.getInputById("DH");
        await calcPage.setParamMode(dh_nombre, "cal");

        await openCalculator(6, navBar, listPage); // PAB:Puissance

        // link DH to PAB:Nombre.DH
        const dh_puiss = await calcPage.getInputById("DH");
        await calcPage.setParamMode(dh_puiss, "link");

        // Go back to Solveur
        await navBar.clickCalculatorTab(0);

        await changeSelectValue(await calcPage.getSelectById("select_target_nub"), 1); // "Puissance / PV"
        await browser.pause(500);
        await changeSelectValue(await calcPage.getSelectById("select_searched_param"), 2); // "Chute / Z2"
        await browser.pause(500);
        await calcPage.getInputById("Ytarget").setValue("318");

        // check that "compute" button is active
        const calcButton = await calcPage.checkCalcButtonEnabled(true);
        await browser.pause(200);

        // click "compute" button
        await calcButton.click();
        await browser.pause(200);

        // check that result is not empty
        const hasResults = await calcPage.hasResults();
        expect(hasResults).toBe(true);

        await scrollPageToTop(); // otherwise clickCloneCalcButton() fails with "Element is not clickable at point"

        await calcPage.clickCloneCalcButton();
        await browser.pause(500);

        // 4. check existence of the cloned module
        expect(await navBar.getAllCalculatorTabs().length).toBe(5);
        await navBar.clickCalculatorTab(4); // n°4 should be the latest

        // check that result is empty
        const hasResultsClone1 = await calcPage.hasResults();
        expect(hasResultsClone1).toBe(false);

        // check that "compute" button is active
        const calcButtonClone = await calcPage.checkCalcButtonEnabled(true);
        await browser.pause(200);

        // click "compute" button
        await calcButtonClone.click();
        await browser.pause(200);

        // check that result is not empty
        const hasResultsClone2 = await calcPage.hasResults();
        expect(hasResultsClone2).toBe(true);
    });

    it("channel flow example > solver > change searched parameter > run calculation", async () => {
        await newSession(navBar, sideNav);

        // open "channel flow with hydraulic structures" example
        const examples = await $$("#examples-list .load-example");
        await examples[1].click();
        await browser.pause(500);

        // select solver tab
        await navBar.clickCalculatorTab(3);
        await browser.pause(500);

        // modify searched parameter
        const sel = await calcPage.getSelectById("select_searched_param");
        await changeSelectValue(sel, 11);
        await browser.pause(300);
        const selText = await calcPage.getSelectValueText(sel);

        // run calculation
        const calcButton = await calcPage.getCalculateButton();
        await calcButton.click();
        await browser.pause(500);

        // check "search parameter" value has not changed
        expect(await calcPage.getSelectValueText(sel)).toEqual(selText);
    });

    it("check solver searched parameter is set to 'bottom slope'", async () => {
        await newSession(navBar, sideNav);

        // open "canal critical slope" example
        const examples = await $$("#examples-list .load-example");
        await examples[3].click();
        await browser.pause(500);

        // select solver tab
        await navBar.clickCalculatorTab(2);
        await browser.pause(500);

        // check selected searched parameter text
        const sel = await calcPage.getSelectById("select_searched_param");
        const selText = await calcPage.getSelectValueText(sel);
        expect(selText).toEqual("If - Pente du fond (Sec. param.)");
    });
});

/**
 * solver with empty fields option
 */
describe("Solveur - nghyd#601 with empty fields option", () => {
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let sideNav: SideNav;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        navBar = new Navbar();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        sideNav = new SideNav();
    });


    async function openSolver() {
        await openCalculator(22, navBar, listPage);
    }

    beforeEach(async () => {
        // enable evil option "empty fields on module creation"
        await prefPage.setEmptyFields(true);
    });

    it("check solver with empty fields option does not fill inputs - solver alone", async () => {
        await newSession(navBar, sideNav);

        // open new solver calculator
        await openSolver();

        // check inputs are empty
        await calcPage.checkEmptyInput("Ytarget");
        await browser.pause(200);

        await calcPage.checkEmptyInput("Xinit");
        await browser.pause(200);
    });

    it("check solver with empty fields option does not fill target parameter input", async () => {
        // open "parallel structures" calculator
        await openCalculator(8, navBar, listPage);

        // open second "parallel structures" calculator
        await openCalculator(8, navBar, listPage);

        // link Q to first calculator's
        const inpQ = await calcPage.getInputById("Q");
        await calcPage.setParamMode(inpQ, "link");
        await browser.pause(200);

        // open new solver calculator
        await openSolver();

        // check target parameter input is empty
        await calcPage.checkEmptyInput("Ytarget");
        await browser.pause(100);

        // check initial value input is not empty
        await calcPage.checkEmptyInput("Xinit");
        await browser.pause(100);
    });

    it("check removing and recreating solver with empty fields option does not fill target parameter input", async () => {
        await newSession(navBar, sideNav);

        // open "channel flow with hydraulic structures" example
        const examples = await $$("#examples-list .load-example");
        await examples[1].click();
        await browser.pause(500);

        // close existing "solver" calculator
        await navBar.middleClickCalculatorTab(3);
        await browser.pause(500);

        // open new solver calculator
        await openSolver();

        // check target parameter input is empty
        await calcPage.checkEmptyInput("Ytarget");
        await browser.pause(200);

        // check initial value input is not empty
        await calcPage.checkEmptyInput("Xinit", false);
        await browser.pause(200);
    });
});

/**
 * solver without empty fields option
 */
describe("Solveur - nghyd#601 without empty fields option", () => {
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let sideNav: SideNav;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        navBar = new Navbar();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        sideNav = new SideNav();
    });

    async function openSolver() {
        await openCalculator(22, navBar, listPage);
    }

    beforeEach(async () => {
        // await browser.manage().window().setPosition(2000, 30);

        // disable evil option "empty fields on module creation"
        await prefPage.setEmptyFields(false);
    });

    it("check solver without empty fields option does not fill inputs - solver alone", async () => {
        await newSession(navBar, sideNav);

        // open new solver calculator
        await openSolver();

        // check inputs are empty
        await calcPage.checkEmptyInput("Ytarget");
        await browser.pause(200);

        await calcPage.checkEmptyInput("Xinit");
        await browser.pause(200);
    });

    it("check solver without empty fields option fills inputs", async () => {
        // open "parallel structures" calculator
        await openCalculator(8, navBar, listPage);

        // open second "parallel structures" calculator
        await openCalculator(8, navBar, listPage);

        // link Q to first calculator's
        const inpQ = await calcPage.getInputById("Q");
        await calcPage.setParamMode(inpQ, "link");
        await browser.pause(200);

        // open new solver calculator
        await openSolver();

        // check target parameter input is not empty
        await calcPage.checkEmptyInput("Ytarget", false);
        await browser.pause(200);

        // check initial value input is not empty
        await calcPage.checkEmptyInput("Xinit", false);
        await browser.pause(200);
    });

    it("check removing and recreating solver without empty fields option fills inputs", async () => {
        await newSession(navBar, sideNav);

        // open "channel flow with hydraulic structures" example
        const examples = await $$("#examples-list .load-example");
        await examples[1].click();
        await browser.pause(500);

        // close existing "solver" calculator
        await navBar.middleClickCalculatorTab(3);
        await browser.pause(500);

        // open new solver calculator
        await openSolver();

        // check target parameter input is not empty
        await calcPage.checkEmptyInput("Ytarget", false);
        await browser.pause(200);

        // check initial value input is not empty
        await calcPage.checkEmptyInput("Xinit", false);
        await browser.pause(200);
    });

    it(" with empty fields option, check selecting a target module does not fill inputs", async () => {
        // enable evil option "empty fields on module creation"
        await prefPage.setEmptyFields(true);
        await browser.pause(200);

        await navBar.clickNewCalculatorButton();
        await browser.pause(200);

        // open "channel flow with hydraulic structures" example
        const examples = await $$("#examples-list .load-example");
        await examples[1].click();
        await browser.pause(500);

        // close existing "solver" calculator
        await navBar.middleClickCalculatorTab(3);
        await browser.pause(500);

        // open new solver calculator
        await openSolver();
        await browser.pause(500);

        // select other target module
        const ntc = await calcPage.getSelectById("select_target_nub");
        await changeSelectValue(ntc, 1);

        // check target value input is empty
        await calcPage.checkEmptyInput("Ytarget", true);
        await browser.pause(200);
    });
});

describe("Solveur - nghyd#606 - a single module cannot be used with solver", () => {
    let prefPage: PreferencesPage;
    let navBar: Navbar;
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let sideNav: SideNav;

    beforeAll(() => {
        prefPage = new PreferencesPage();
        navBar = new Navbar();
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        sideNav = new SideNav();
    });

    async function openSolver() {
        await openCalculator(22, navBar, listPage);
    }

    beforeEach(async () => {
        // await browser.manage().window().setPosition(2000, 30);

        // disable evil option "empty fields on module creation"
        await prefPage.setEmptyFields(false);
    });

    it("", async () => {
        await newSession(navBar, sideNav);

        // open uniform flow calculator
        await openCalculator(3, navBar, listPage);

        // open a solver module
        await openSolver();

        // set speed as target parameter
        const trs = await calcPage.getSelectById("select_target_result");
        await changeSelectValue(trs, 1);
        await browser.pause(200);

        // check searched parameter has options
        const sps = await calcPage.getSelectById("select_searched_param");
        expect(await calcPage.isSelectEmpty(sps)).toBe(false);
    });
});
