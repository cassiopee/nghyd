import { ListPage } from "./list.po";
import { PreferencesPage } from "./preferences.po";
import { Navbar } from "./navbar.po";
import { browser, $, $$, expect } from '@wdio/globals'
import { isElementDisplayed, openCalculator, scrollPageToTop } from "./util.po";
import { CalculatorPage } from "./calculator.po";
import { newSession } from "./util.po";
import { SideNav } from "./sidenav.po";

//TODO à réactiver après fusion de nghyd#609 jalhyd#325 dans devel
xdescribe("MacroRugoRemous - ", () => {
        let prefPage: PreferencesPage;
        let listPage: ListPage;
        let navBar: Navbar;
        let calcPage: CalculatorPage;
        let sideNav: SideNav;

        beforeAll(() => {
                prefPage = new PreferencesPage();
                listPage = new ListPage();
                navBar = new Navbar();
                calcPage = new CalculatorPage();
                sideNav = new SideNav();
        });

        beforeEach(async () => {
                // disable evil option "empty fields on module creation"
                await prefPage.navigateTo();
                await browser.pause(200);
                await prefPage.disableEvilEmptyFields();
                await browser.pause(200);
        });

        it("check select empty message is present when no PAM is in the session", async () => {
                // MacroRugoRemous calculator
                await openCalculator(37, navBar, listPage);

                // const emptyMsg = element(by.css("div .message-when-empty"));
                const emptyMsg = await $("div .message-when-empty");
                expect(await isElementDisplayed(emptyMsg)).toBe(true);
        });

        it("check select empty message is not present when a PAM exists in the session", async () => {
                // PAM calculator
                await openCalculator(11, navBar, listPage);

                await navBar.clickNewCalculatorButton();
                await browser.pause(200);

                // MacroRugoRemous calculator
                await openCalculator(37, navBar, listPage);

                const emptyMsg = $("div .message-when-empty");
                expect(await isElementDisplayed(emptyMsg)).toBe(false);
        });

        it("check select error message when target PAM has no variated parameter", async () => {
                // PAM calculator
                await openCalculator(11, navBar, listPage);

                // MacroRugoRemous calculator
                await openCalculator(37, navBar, listPage);

                const errorMsg = await $("div .select-error-message");
                expect(await isElementDisplayed(errorMsg)).toBe(false);
        });

        it("check select error message when target PAM has a variated parameter", async () => {
                await newSession(navBar, sideNav);

                // open PAM calculator
                await openCalculator(11, navBar, listPage);

                // set ZF1 to variated mode
                const zf1 = await calcPage.getInputById("ZF1");
                await calcPage.setParamMode(zf1, "var");
                await browser.pause(200);

                // MacroRugoRemous calculator
                await openCalculator(37, navBar, listPage);

                const errorMsg = await $("div .select-error-message");
                expect(await isElementDisplayed(errorMsg)).toBe(true);
        });
        it("Generate from PAM", async () => {

                // Open PAM with default values
                await openCalculator(11, navBar, listPage);

                // calculate PAM
                const calcButtonCl = await calcPage.getCalculateButton();
                await calcButtonCl.click();
                await browser.pause(200);

                // make sure "Generate backwater curve" button is visible (it might be hidden behind navbar)
                await scrollPageToTop();
                // generate Macrorugo backwater curve
                const genButton = await calcPage.getButton("generate-backwater-curve");
                await genButton.click();
                await browser.pause(1000);

                // calculate PAB
                const calcButtonMRCR = await calcPage.getCalculateButton();
                await calcButtonMRCR.click();
                await browser.pause(500);

                // check that result is not empty
                const hasResults = await calcPage.hasResults();
                expect(hasResults).toBe(true);
        });
});
