import { ListPage } from "./list.po";
import { CalculatorPage } from "./calculator.po";
import { Navbar } from "./navbar.po";
import { PreferencesPage } from "./preferences.po";
import { SideNav } from "./sidenav.po";
import { testedCalcTypes } from "./tested_calctypes";
import { browser, $, $$, expect } from '@wdio/globals'
import { openCalculator } from "./util.po";

/**
 * For all calculators, try to calculate every parameter: check that only one parameter
 * is set to CAL mode, trigger the calculation, check that result is not empty
 */
describe("ngHyd − check translation of all calculators", () => {
    let listPage: ListPage;
    let calcPage: CalculatorPage;
    let navBar: Navbar;
    let prefPage: PreferencesPage;
    let sideNav: SideNav;

    beforeAll(() => {
        listPage = new ListPage();
        calcPage = new CalculatorPage();
        navBar = new Navbar();
        prefPage = new PreferencesPage();
        sideNav = new SideNav();
    });

    describe("", () => {
        // get calculators list (IDs) @TODO read it from config, but can't import jalhyd here :/
        const calcTypes = testedCalcTypes;

        // options of "Language" selector on preferences page
        const langs = ["English", "Français"];

        // for each language
        for (let i = 0; i < langs.length; i++) {
            describe("language " + langs[i] + " − ", () => {

                beforeAll(async () => {
                    await prefPage.navigateTo();
                    await prefPage.changeLanguage(i);
                    await browser.pause(200);
                    await navBar.clickNewCalculatorButton();
                });

                beforeEach(function () {
                    jasmine.addMatchers({
                        toContain: function () {
                            return {
                                compare: function (actual, expected) {
                                    const result = {
                                        pass: actual.includes(expected),
                                        message: "error" // short message to avoid whole page source being dumped on expect() failure
                                    };
                                    return result;
                                }
                            };
                        }
                    });
                });

                // for each calculator
                for (const ct of calcTypes) {
                    it(" − check translations of calculator type [" + ct + "]", async () => {
                        // click calculator button (instanciate)
                        await openCalculator(ct, navBar, listPage);

                        // just click the "compute" button with default values
                        // check that "compute" button is active
                        const calcButton = await calcPage.getCalculateButton();
                        const disabledState = await calcButton.getAttribute("disabled");
                        if (!disabledState) {
                            // click "compute" button
                            await calcButton.click();
                            await browser.pause(200);
                            // check that result is not empty
                            const hasResults = await calcPage.hasResults();
                            expect(hasResults).toBe(true);
                        }

                        // check absence of "*** message not found" in whole DOM
                        const source = await browser.execute("return document.body.innerHTML");
                        expect(source).not.toContain("*** message not found", "missing translations found");

                        // empty session
                        await navBar.clickMenuButton();
                        await browser.pause(200);
                        await sideNav.clickNewSessionButton();
                        browser.pause(200);
                    });
                }
            });
        }
    });
});
