# Méthode par intégration de trapèzes

La forme intégrale de l'équation différentielle ordinaire du premier ordre s'écrit&nbsp;:

$$\int_{t_{i}}^{t_{i+1}}\frac{dy}{dt} = \int_{t_{i}}^{t_{i+1}}f(y,t)$$

La méthode des trapèzes donne&nbsp;:

$$y_{i+1} \simeq y_i + \frac{y'_i + y'_{i+1}}{2} \Delta t$$
