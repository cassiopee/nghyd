# Principe de fonctionnement d'un module de calcul

Les modules de calcul de Cassiopée permettent chacun de calculer un paramètre au choix parmi celles intervenant dans une ou plusieurs équations.

## Ouvrir un nouveau module de calcul

![Bandeau supérieur de l'application avec le menu, la liste des modules ouverts et le bouton pour ajouter un nouveau module](principe_fonctionnement_bandeau_superieur.png)

La liste des modules est disponible au lancement de l'application. Après avoir ouvert un nouveau module de calcul, cette liste est disponible via le bouton "+" situé dans le bandeau supérieur ou par le menu "☰" puis le lien "Nouveau module de calcul" situé dans le menu.

La liste des modules ouverts apparaît dans le bandeau supérieur et permet de naviguer entre les modules ouverts.

## Comment s'opèrent les choix pour effectuer un calcul ou une série de calcul&nbsp;?

Le module se présente sous la forme d'une série de paramètres intervenant dans la résolution de l'équation du module de calcul.

![Paramètres du module de calcul de la chute d'une passe à bassin](principe_fonctionnement_grandeurs.png)

Pour chacun d'entre eux, l'utilisateur peut au choix&nbsp;:

- Fixer le paramètre (Bouton "FIXÉ");
- Faire varier le paramètre afin d'effectuer une série de calcul (Bouton "VARIER")
- Choisir le paramètre qui sera calculé (Bouton "CALCULER");

L'interface est conçue pour qu'un paramètre et un seul soit choisi pour le calcul. Les paramètres qui ne peuvent être calculés ne sont pas pourvus du bouton "CALCULER".

## Comment faire varier un paramètre pour effectuer une série de calculs

Une série de calculs peut être lancée entre une valeur min et une valeur max pour un pas donné&nbsp;:

![Définition des valeurs min, max et du pas pour un paramètre à varier](principe_fonctionnement_varie_pas.png)

Ou pour une liste de valeurs définies&nbsp;:

![Définition d'une liste de valeurs pour un paramètre à varier](principe_fonctionnement_varie_liste.png)

L'import d'une liste de valeur s'effectue soit par saisie ou copier/coller dans le champ "Liste de valeurs" ou par un fichier texte. Le séparateur décimal est configurable. Tout caractère en dehors des caractères numériques, la lettre "E" et le séparateur décimal sera considéré comme séparateur entre les valeurs. De fait, le séparateur pourra être la virgule, le point-virgule, l'espace, la tabulation, le retour charriot...

Le titre de la fenêtre contient le nombre d'occurrences correspondant. Un clic sur le logo en forme de graphique à droite du titre de la fenêtre permet d'afficher un graphique des variations du paramètre.

Pour le cas où plusieurs paramètres varient et qu'ils n'ont pas le même nombre d'occurrences, il faut définir une stratégie d'extension des listes les plus courtes pour les adaptés à la liste du paramètre ayant le plus d'occurrences. Deux stratégies sont disponibles&nbsp;: répéter la dernière valeur ou réutiliser les valeurs de la liste depuis la première occurrence.

## Comment lancer un calcul ou une série de calcul

Presser la touche [Entrée] ou cliquer sur le bouton "Calculer" situé au bas de l'écran.

## Les résultats du calcul

Pour des paramètres fixés, le panneau de résultat affiche les paramètres fixés et le paramètre calculé ainsi que d'éventuels résultats complémentaires.

![Résultat d'un calcul pour des grandeurs fixées](principe_fonctionnement_resultat_fix.png)

Pour un ou plusieurs paramètres qui varient, le panneau de résultat affiche&nbsp;:

- un graphique d'évolution sur lequel on peut choisir le paramètre à utiliser en abscisse et en ordonnée&nbsp;;
- un tableau reprenant les paramètres fixés&nbsp;;
- un tableau reprenant les paramètres qui varient et le paramètre calculé ainsi que les valeurs des éventuels résultats complémentaires.

![Résultat d'une série de calculs pour un paramètre qui varie](principe_fonctionnement_resultat_var.png)

Les tableaux et graphiques sont pourvues de différentes fonctionnalités&nbsp;:

- un bouton de téléchargement pour récupérer le contenu du tableau au format XLS&nbsp;
- un bouton de téléchargement pour récupérer le graphique affiché au format PNG&nbsp;
- un bouton pour afficher le tableau ou le graphique en plein écran.

Les graphiques sont pourvus d'un zoom accessible en effectuant une sélection à la souris sur le graphique. Le bouton avec la flèche faisant un demi-tour à gauche permet de réinitialiser le zoom à sa valeur d'origine affichant toutes les valeurs disponibles.
