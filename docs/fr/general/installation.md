# Installation

Cassiopée peut être installé pour une utilisation sans connexion internet sous la forme d'un programme exécutable ou d'une Web App progressive (PWA).

## Installation de l'application web progressive Cassiopée

Une progressive web app (PWA, application web progressive en français) est une application web qui consiste en des pages ou des sites web, et qui peuvent apparaître à l'utilisateur de la même manière que les applications natives ou les applications mobiles.

Les PWA sont disponibles pour toutes les platesformes (Windows, Linux, MacOS, Android, et iOS) à condition d'avoir un browser compatible :

| Plateforme | Chrome/Chromium | Edge | Firefox       | Safari |
|------------|-----------------|------|---------------|--------|
| Windows    | Oui             | Oui  | via extension |        |
| Linux      | Oui             |      | via extension |        |
| MacOS      | Oui             |      | via extension |        |
| Android    | Oui             |      | Oui           |        |
| iOS        |                 |      |               | Oui    |

L'installation se fait directement à partir du navigateur internet à l'adresse <https://cassiopee.g-eau.net> en cliquant sur un bouton situé à droite de la barre d'adresse. L'aspect du bouton peut varier en fonction du navigateur utilisé :

* Installer une PWA avec Chrome/Chromium : <https://support.google.com/chrome/answer/9658361?hl=fr>
* Installer une PWA avec Edge : <https://learn.microsoft.com/fr-fr/microsoft-edge/progressive-web-apps-chromium/ux>
* Installer une PWA (tout browser et plateforme - en anglais) : <https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Installing>

Après l'installation, Cassiopée peut être lancé à partir de l'icône de l'application présente sur le bureau.

Les mises à jour sont automatiquement détectées et installées (l'utilisateur est invité à redémarrer Cassiopée après le téléchargement de la mise à jour).

N.B.: même sans passer par la procédure d'installation de la PWA, Cassiopée est disponible dans le navigateur sans connection internet à condition d'avoir été chargé précédemment.

## Installation de l'application Desktop (obsolète)

Cassiopée est disponible sous la forme d'un programme exécutable pour les plateformes Windows, Linux et MacOS. Les programmes d'installation sont téléchargeables à l'adresse suivante : <https://cassiopee.g-eau.fr/cassiopee-releases/>

L'installation sous la forme d'une application web progressive (cf. ci-dessus) remplacera définitivement ce mode d'installation dans une prochaine version de Cassiopée.
