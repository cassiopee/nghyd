# Paramètres de l'application

Accessible depuis le menu latéral gauche, les paramètres de l'application modifiables par l'utilisateur sont les suivants&nbsp;:

- Nombre de décimales affichées&nbsp;: Nombre de décimales affichées pour les résultats des calculs. Pour les nombres proches de zéro affichés en notation scientifique, cette option règle le nombres de chiffres significatifs affichés&nbsp;;
- Précision de calcul&nbsp;: Précision utilisée pour la convergence du solveur numérique ([Méthode de Brent](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Brent) ou [méthode de Newton](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Newton))&nbsp;;
- Solveur: nombre d'itérations maximum&nbsp;;
- Activer les notifications à l'écran&nbsp;: autorise l'affichage de notifications lors de certaines opérations (avertissement au chargement d'une session, invalidation de calcul...)&nbsp;;
- Activer les raccourcis clavier: autorise l'utilisation des raccourcis clavier (Voir [liste des raccourcis disponibles](raccourcis_clavier.md))&nbsp;;
- Créer des nouveaux modules avec des champs vides (aucune valeur par défaut)&nbsp;: si décoché les paramètres des modules sont pré-remplis avec des valeurs par défaut&nbsp;;
- Langue&nbsp;: définit la langue des interfaces du logiciel en français ou en anglais.

Le bouton d'enregistrement en haut de la fenêtre permet d'enregistrer les préférences de l'utilisateur dans son navigateur pour les utilisations ultérieures. Le bouton "Reset" permet de restaurer les réglages par défaut de l'application.
