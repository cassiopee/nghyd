# Liste des raccourcis clavier

Pour utiliser les raccourcis clavier, il faut activer l'option dans [les paramètres de l'application](parametres_application.md).

- Alt + S&nbsp;: Enregistre la session courante
- Alt + O&nbsp;: Ouvre une nouvelle session
- Alt + Q&nbsp;: Vide la session courante
- Alt + N&nbsp;: Ajoute un module de calcul à la session courante
- Alt + ↵&nbsp;: Lance le calcul du module en cours
- Alt + D&nbsp;: Duplique le module en cours
- Alt + W&nbsp;: Ferme le module en cours
- Alt + G&nbsp;: Montre le diagramme des modules
- Alt + 1&nbsp;: Positionne la page sur la section "Données" du module en cours
- Alt + 2&nbsp;: Positionne la page sur la section "Résultats" du module en cours
- Alt + 3&nbsp;: Positionne la page sur la section "Graphique" du module en cours
