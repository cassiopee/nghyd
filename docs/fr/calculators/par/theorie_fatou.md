# Passe à ralentisseurs "Fatou"

## Caractéristiques géométriques

![Caractéristiques d'une passe à ralentisseurs Fatou](theorie_fatou_schema.png)

*Extrait de Larinier, 2002[^1]*

## Lois hydrauliques issues des abaques

Les expériences effectuées par Larinier, 2002[^1] ont permis d'établir des abaques permettant de relier le débit adimensionnel \(Q^*\)&nbsp;:

$$ Q^* = \dfrac{Q}{\sqrt{g}L^{2,5}} $$

 à la charge amont \(ha\) et le niveau d'eau moyen dans la passe \(h\) :

![Abaques d'une passe à ralentisseurs Fatou pour une pente de 10%](baffle_fishway_Fatou_slope_10_.svg)

*Abaques d'une passe à ralentisseurs Fatou pour une pente de 10% (Extrait de Larinier, 2002[^1])*

![Abaques d'une passe à ralentisseurs Fatou pour une pente de 15%](baffle_fishway_Fatou_slope_15_.svg)

*Abaques d'une passe à ralentisseurs Fatou pour une pente de 15% (Extrait de Larinier, 2002[^1])*

![Abaques d'une passe à ralentisseurs Fatou pour une pente de 20%](baffle_fishway_Fatou_slope_20_.svg)

*Abaques d'une passe à ralentisseurs Fatou pour une pente de 20% (Extrait de Larinier, 2002[^1])*

Pour effectuer les calculs pour toutes les pentes entre 8% et 22%, les coefficients de polynômes des abaques ci-dessus sont eux-mêmes ajustés sous le forme de polynômes dépendant de la pente \(S\).

On a donc&nbsp;:

$$ ha/L = a_2(S) Q^{*2} + a_1(S) Q^* + a_0(S) $$

$$a_2(S) = - 783.592S^2 + 269.991S - 25.2637$$

$$a_1(S) = 302.623S^2 - 106.203S + 13.2957$$

$$a_0(S) = 15.8096S^2 - 5.19282S + 0.465827$$

Et&nbsp;:

$$ h/L = b_2(S) Q^{*2} + b_1(S) Q^* + b_0 $$

$$b_2(S) = - 73.4829S^2 + 54.6733S - 14.0622$$

$$b_1(S) = 42.4113S^2 - 24.4941S + 8.84146$$

$$b_0(S) = - 3.56494S^2 + 0.450262S + 0.0407576$$

## Calcul de \(ha\), \(h\) et \(Q\)

On peut ensuite utilise ces coefficients pour calculer \(ha\), \(h\) et \(Q^*\)&nbsp;:

$$ ha = L \left( a_2 (Q^*)^2 + a_1 Q^* + a_0 \right)$$

$$ h = L \left( b_2 (Q^*)^2 + b_1 Q^* + b_0 \right)$$

En utilisant la fonction inverse positive en fonction de \(ha/L\), on obtient:

$$ Q^* = \dfrac{-a_1 + \sqrt{a_1^2 - 4 a_2 (a_0 - h_a/L)}}{2 a_2}$$

Et on a enfin&nbsp;:

$$ Q = Q^* \sqrt{g} L^{2,5} $$

Les limites de calcul de \(Q^*\), \(ha/L\) et \(h/L\) sont fixées à partir des extrémités des courbes des abaques.

## Vitesse débitante

La vitesse débitante \(V\) va correspondre à la vitesse moyenne d'écoulement compte tenu de la section d'écoulement \(A_w\) au droit du ralentisseur :

$$ V = \dfrac{Q}{A_w} $$

pour les passes à ralentisseurs Fatou en utilisation les notations du schéma ci-dessus, on aura :

$$ A_w = B \times h $$

Ce qui donne avec les proportions standards :

$$ A_w = 0.6hL $$

## Cote de radier amont \(Z_{r1}\)

$$ Z_{r1} = Z_{d1} + \frac{0.3 S - 0.2}{\sqrt{1 + S^2}} $$

## Cote d'arase minimale des murs latéraux \(Z_m\)

$$ Z_m = Z_{r1} + \frac{4 L}{3 \sqrt{1 + S^2}} $$

[^1]: Larinier, M. 2002. “BAFFLE FISHWAYS.” Bulletin Français de La Pêche et de La Pisciculture, no. 364: 83–101. doi:[10.1051/kmae/2002109](https://doi.org/10.1051/kmae/2002109).
