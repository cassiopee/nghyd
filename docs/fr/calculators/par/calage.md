# Calage d'une passe à ralentisseurs

Ce module permet de dimensionner une passe à ralentisseur. Les types de passes à ralentisseurs supportés sont:

- [passes à ralentisseurs plans](theorie_plans.md)&nbsp;;
- [passes à ralentisseurs "Fatou"](theorie_fatou.md)&nbsp;;
- [passes à ralentisseurs à fonds suractifs](theorie_suractif.md)&nbsp;;
- [passes à ralentisseurs mixtes](theorie_mixte.md).

Voir [l'ensemble des formules utilisées pour les passes à ralentisseurs](formules.md).

## Calage hydraulique de la passe

L'outil permet de calculer l'une des valeurs suivantes&nbsp;:

- le débit passant dans la passe (m<sup>3</sup>/s)&nbsp;;
- la charge en amont de la passe (m)&nbsp;;
- la largeur de la passe (m) pour les types ralentisseurs plans et Fatou&nbsp;.

Compte tenu des paramètres obligatoires suivants&nbsp;:

- le type de passe (Plans, Fatou, fonds suractifs ou mixtes)&nbsp;;
- [la pente (m/m)](../hsl/pente.md).

Le paramètre "Espacement entre les ralentisseurs (m)" est facultatif. S'il n'est pas renseigné, sa valeur standard est alors calculée. S'il est fourni, si sa valeur s'écarte de plus de 10% de la valeur standard, une erreur est générée.

## Calage altimétrique de la passe

Les paramètres de calage altimétriques (cote de l'eau amont et cote de l'eau aval) sont facultatifs et permettent de calculer:

- la longueur longitudinale et au fil de l'eau de la passe
- le nombre de ralentisseurs
- les cotes de radier et de déversement à l'amont et à l'aval de la passe
- les cotes d'arase des murs latéraux à l'amont de la passe.

## Génération d'un module de simulation de passe à ralentisseurs

Les résultats d'une passe calée en altimétrie peuvent servir à générer un module de [simulation de passe à ralentisseurs](simulation.md) à l'aide du bouton ad hoc.