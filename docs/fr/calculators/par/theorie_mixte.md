# Passe à ralentisseurs mixte ou à chevrons

![Caractéristiques d'une passe à ralentisseurs mixte ou à chevrons](theorie_mixte_schema.png)

*Extrait de Larinier, 2002[^1]*

## Lois hydrauliques issues des abaques

Les expériences effectuées par Larinier, 2002[^1] ont permis d'établir des abaques permettant de relier le débit adimensionnel \(q^*\)&nbsp;:

$$ q^* = \dfrac{Q/L}{\sqrt{2g}a^{1,5}} $$

 à la charge amont \(ha\) et le niveau d'eau moyen dans la passe \(h\) :

![Abaques d'une passe à ralentisseurs mixte pour une pente de 10%](baffle_fishway_mixte_slope_10_.svg)

*Abaques d'une passe à ralentisseurs mixte pour une pente de 10% (Extrait de Larinier, 2002[^1])*

![Abaques d'une passe à ralentisseurs mixte pour une pente de 16%](baffle_fishway_mixte_slope_16_.svg)

*Abaques d'une passe à ralentisseurs mixte pour une pente de 15% (Extrait de Larinier, 2002[^1])*

Pour effectuer les calculs pour toutes les pentes entre 8% et 22%, les coefficients de polynômes des abaques ci-dessus sont eux-mêmes ajustés sous le forme de polynômes dépendant de la pente \(S\).

On a donc&nbsp;:

$$ ha/a = a_2(S) q^{*2} + a_1(S) q^* + a_0(S) $$

$$a_2(S) = 0.188324S - 0.0427461$$

$$a_1(S) =  - 2.47998S + 1.25363$$

$$a_0(S) = 5.02138S + 0.709434$$

Et&nbsp;:

$$ h/a = b_2(S) q^{*2} + b_1(S) q^* + b_0 $$

$$b_2(S) = - 0.0733832S - 0.00839864$$

$$b_1(S) = 0.176261S + 0.661656$$

$$b_0(S) =  - 4.97686S + 1.30546$$

## Calcul de \(ha\), \(h\) et \(Q\)

On peut ensuite utilise ces coefficients pour calculer \(ha\), \(h\) et \(q^*\)&nbsp;:

$$ ha = a \left( a_2 (q^*)^2 + a_1 q^* + a_0 \right)$$

$$ h = a \left( b_2 (q^*)^2 + b_1 q^* + b_0 \right)$$

En utilisant la fonction inverse positive en fonction de \(ha/L\), on obtient:

$$ q^* = \dfrac{-a_1 + \sqrt{a_1^2 - 4 a_2 (a_0 - h_a/a)}}{2 a_2}$$

Et on a enfin&nbsp;:

$$ Q = L q^* \sqrt{g} a^{1,5} $$

Les limites de calcul de \(q^*\), \(ha/a\) et \(h/a\) sont fixées à partir des extrémités des courbes des abaques.

## Vitesse débitante

La vitesse débitante \(V\) va correspondre à la vitesse moyenne d'écoulement compte tenu de la section d'écoulement \(A_w\) au droit du ralentisseur :

$$ V = \dfrac{Q}{A_w} $$

pour les passes à ralentisseurs mixte en utilisation les notations du schéma ci-dessus, on aura :

$$ A_w = h \times L$$

## Cote de radier amont \(Z_{r1}\)

$$ Z_{r1} = Z_{d1} + \frac{3 a S - a}{\sqrt{1 + S^2}} $$

[^1]: Larinier, M. 2002. “BAFFLE FISHWAYS.” Bulletin Français de La Pêche et de La Pisciculture, no. 364: 83–101. doi:[10.1051/kmae/2002109](https://doi.org/10.1051/kmae/2002109).

