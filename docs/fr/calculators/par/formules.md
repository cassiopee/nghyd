# Formules de calcul des passes à ralentisseurs

Pour le calcul de :

- la charge amont \(ha\)&nbsp;;
- la hauteur d'eau dans la passe \(h\)&nbsp;;
- du débit \(Q\)&nbsp;;
- de la vitesse débitante \(V\)&nbsp;;
- la cote de radier amont \(Z_{r1}\)&nbsp;;
- la cote d'arase minimale des murs latéraux \(Z_m\)

Se référer aux formules propres à chaque type de passe à ralentisseurs:

- [passes à ralentisseurs plans](theorie_plans.md)&nbsp;;
- [passes à ralentisseurs "Fatou"](theorie_fatou.md)&nbsp;;
- [passes à ralentisseurs à fonds suractifs](theorie_suractif.md)&nbsp;;
- [passes à ralentisseurs mixtes](theorie_mixte.md).

## Cote de l'eau à l'amont de la passe \(Z_1\)

$$Z_{1} = Z_{d1} + h_a$$

Avec \(Z_{d1}\) la cote de déversement du premier ralentisseur amont, \(h_a\) la charge amont.

## Longueur de la passe

La longueur de la passe le long d'une ligne d'eau parallèle à la pente de la passe \(L_w\) est égale à&nbsp;

$$L_w = (Z_1 - Z_2)\dfrac{\sqrt{1 + S^2}}{S}$$

avec \(Z_1\) et \(Z_2\) les cotes de l'eau à l'amont et l'aval de la passe, \(S\) la pente.

La longueur de la passe le long de la pente \(L_S\) doit être un multiple de la longueur entre les ralentisseurs \(P\) arrondi à l'entier supérieur&nbsp;:

$$L_S = \lceil (L_w - \epsilon) / P \rceil \times P $$

Avec \(\epsilon\) = 1 mm pour laisser une marge avant le rajout d'un ralentisseur supplémentaire.

La projection horizontale de la longueur de la passe \(L_h\) est alors égale à&nbsp;:

$$L_h = \dfrac{L_S}{\sqrt{1 + S^2}} $$

## Nombre de ralentisseurs \(N_b\)

Pour les types plans et Fatou&nbsp;:

$$N_b = L_S / P + 1$$

Pour les types à fonds suractifs et mixtes&nbsp;:

$$N_b = L_S / P$$

## Cotes de radier à l'aval \(Z_{r2}\) et de déversement à l'aval \(Z_{d2}\)

$$Z_{r2} = Z_{r1} - \dfrac{L_S \times S}{\sqrt{1 + S^2}}$$

$$Z_{d2} = Z_{r2} + Z_{d1} - Z_{r1}$$
