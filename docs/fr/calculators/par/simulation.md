# Simulation d'une passe à ralentisseurs

Ce module permet de calculer différentes conditions hydrauliques sur une passe à ralentisseur dont la géométrie est connue. Cette géométrie provient de mesures topographiques ou du [résultat d'un calage de passe à ralentisseurs](calage.md).

Les types de passes à ralentisseurs supportés sont:

- [passes à ralentisseurs plans](theorie_plans.md)&nbsp;;
- [passes à ralentisseurs "Fatou"](theorie_fatou.md)&nbsp;;
- [passes à ralentisseurs à fonds suractifs](theorie_suractif.md)&nbsp;;
- [passes à ralentisseurs mixtes](theorie_mixte.md).

Voir [l'ensemble des formules utilisées pour les passes à ralentisseurs](formules.md).

L'outil permet de calculer l'une des valeurs suivantes&nbsp;:

- le débit passant dans la passe (m<sup>3</sup>/s)&nbsp;;
- la cote de l'eau en amont de la passe (m)&nbsp;;
- la cote de déversement en amont de la passe (m).

Compte tenu des paramètres obligatoires suivants&nbsp;:

- le type de passe (Plans, Fatou, fonds suractifs ou mixtes)&nbsp;;
- [la pente (m/m)](../hsl/pente.md)&nbsp;;
- la largeur de la passe (m)&nbsp;;
- la cote de déversement ou la cote de radier à l'amont (m)&nbsp;;
- la cote de déversement ou la cote de radier à l'aval (m)&nbsp;.
