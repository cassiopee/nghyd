# Calcul du débit d'une passe à macro-rugosité

Le calcul du débit d'une passe à macro-rugosité correspond à l'implémentation de l'algorithme et des équations présentent dans Cassan et al. (2016)[^1].

## Principe général du calcul

![Organigramme de la méthode de calcul](cassan2016_flow_chart_design_method.png)

*Extrait de Cassan et al., 2016[^1]*

Il existe trois cas :

- le cas submergé quand \(h \ge 1.1 \times k\)
- le cas émergent quand \(h \le k\)
- le cas quasi-émergent pour \(k < h < 1.1 \times k\)

Dans le cas quasi-émergent, le calcul du débit correspond à une transition entre les formules cas émergent et cas submergé :

$$Q = a \times Q_{submerge} + (1 - a) \times Q_{emergent}$$

avec \(a = \dfrac{h / k - 1}{1.1 - 1}\)

## Cas submergé

Le calcul se fait en intégrant le profil de vitesse dans et au-dessus des macrorugosités.
Les vitesses calculées sont les moyennes temporelles et spatiales par plan parallèle au fond.

Dans les macrorugosités, les vitesses sont obtenues par la double moyenne des équations de Navier-Stokes en régime uniforme avec un modèle de longueur de mélange pour la turbulence.

Au dessus des macrorugosités, l'analyse classique de couche limite turbulente est maintenue.
Le profil de vitesse est continu au sommet des macrorugosités et ce dernier dépend des conditions aux limites fixées par l'hydraulique&nbsp;:

- vitesse au fond (sans turbulence) en m/s&nbsp;:

$$u_0 = \sqrt{2 g S D (1 - \sigma C)/(C_d C)}$$

- contrainte de cisaillement totale au sommet des rugosités en m/s&nbsp;:

$$u_* = \sqrt{gS(h-k)}$$

La vitesse moyenne du lit est donnée par intégration des débits entre et au-dessus des blocs&nbsp;:

$$\bar{u} = \frac{Q_{inf} + Q_{sup}}{h}$$

avec respectivement \(Q_{inf}\) et \(Q_{sup}\) les débits unitaires pour la partie dans la canopée et la partie au dessus de la canopée.

### Calcul du débit unitaire *Q<sub>inf</sub>* dans la canopée

Le débit dans la canopée est obtenu par intégration du profil de vitesse (Eq. 9, Cassan et al., 2016)&nbsp;:

$$Q_{inf} = \int_{0}^1 u(\tilde{z}) d \tilde{z}$$

avec

$$u(\tilde{z}) = u_0 \sqrt{\beta \left( \frac{h}{k} -1 \right) \frac{\sinh(\beta \tilde{z})}{\cosh(\beta)} + 1}$$

avec \(\beta^2\) le ratio entre les forces trainée et de turbulence (Cassan et al., 2016, Eq. 8)&nbsp;:

$$\beta = \sqrt{(k / \alpha_t)(C_d C k / D)/(1 - \sigma C)}$$

avec :

$$C_d = C_{x} f_{h_*}(h_*)$$

et \(\alpha_t\) obtenu à partir de la résolution de l'équation suivante&nbsp;:

$$\alpha_t u(1) - l_0 u_* = 0$$

avec

$$l_0 = \min \left( s, 0.15 k \right)$$

avec

$$s = D \left( \frac{1}{\sqrt{C}} - 1 \right)$$

### Calcul du débit unitaire *Q<sub>sup</sub>* au dessus de la canopée

$$Q_{sup} = \int_k^h u(z) dz$$

avec (Eq. 12, Cassan et al., 2016)

$$u(z) = \frac{u_*}{\kappa} \ln \left( \frac{z - d}{z_0} \right)$$

avec (Eq. 14, Cassan et al., 2016)

$$z_0 = (k - d) \exp \left( {\frac{-\kappa u_k}{u_*}} \right)$$

et (Eq. 13, Cassan et al., 2016)

$$ d =  k - \frac{\alpha_t u_k}{\kappa u_*}$$

ce qui donne

$$Q_{sup} = \frac{u_*}{\kappa} \left( (h - d) \left( \ln \left( \frac{h-d}{z_0} \right) - 1\right)  - \left( (k - d) \left( \ln \left( \frac{k-d}{z_0} \right) - 1 \right) \right) \right)$$

## Cas émergent

Le calcul du débit se fait par itérations successives qui consistent à trouver la valeur de débit permettant d'obtenir l'égalité entre la vitesse débitante \(V\) et la vitesse moyenne du lit donnée par l'équilibre des forces de frottements (fond + traînée) avec la gravité&nbsp;:

$$u_0 = \sqrt{\frac{2 g S D (1 - \sigma C)}{C_d f_F(F) C (1 + N)}}$$

avec

$$N = \frac{\alpha C_f}{C_d f_F(F) C h_*}$$

avec

$$\alpha = 1 - (a_y / a_x \times C)$$

## Formules utilisées

### Vitesse débitante *V*

$$V = \frac{Q}{B \times h}$$

### Vitesse moyenne entre les blocs *V<sub>g</sub>*

Eq. 1 Cassan et al (2016)[^1] et Eq. 1 Cassan et al (2014)[^2]:

$$V_g = \frac{V}{1 - \sqrt{(a_x/a_y)C}}$$

### Coefficient de trainée d'un bloc *C<sub>d0</sub>*

\(C_{d0}\) est le coefficient de trainée théorique d'un bloc de hauteur infinie pour un Froude \(F << 1\) (Cassan et al, 2014[^2]).

| Forme du bloc | Cylindre | Forme "face arrondie" | Parallélépipède à base carré | Forme "face plate" |
|:--------------|:--------:|:---------------------:|:----------------------------:|:------------------:|
| | ![Cylindre](bloc_cylindre.png) | ![Forme "face arrondie"](bloc_face_arrondie.png) | ![Parallélépipède à base carré](bloc_base_carree.png) | ![Forme "face plate"](bloc_face_plate.png) |
| Valeur de \(C_{d0}\) | 1.0 | 1.2-1.3 | 2.0 | 2.2 |

Lors de l'établissement des formules statistiques du guide technique de 2006 (Larinier et al. 2006[^4]), la définition des formes de blocs à tester a été établie dans la perspective de l'utilisation de blocs de carrière à faces ni complètement rondes, ni complètement carrées.
La forme dite à « face arrondie » n'était ainsi pas complètement cylindrique, mais présentait une face aval trapézoïdale (vue en plan).
De même, la forme dite à « face plane » ne présentait pas une section carrée, mais également une face aval trapézoïdale.
Ces différences de forme entre la « face arrondie » et un véritable cylindre d’une part, et la « face plate » et un véritable parallélépipède à base carrée d’autre part, se traduisent par de légères différences entre celles-ci sur les coefficients de forme \(C_{d0}\).

### Coefficient de forme de bloc *σ*

Cassan et al. (2014)[^2], et Cassan et al. (2016)[^1] définit \(\sigma\) comme le ratio entre l'aire du bloc vu du dessus et \(D^2\).
On a donc \(\sigma = \pi / 4\) pour un bloc circulaire et \(\sigma = 1\) pour un bloc carré.

### Rapport entre la vitesse moyenne à l'aval d'un bloc et la vitesse max *r*

Les valeurs de \(r\) dépendent de la forme des blocs (Cassan et al., 2014[^2] et Tran et al. 2016 [^3])&nbsp;:

- rond : \(r=1.1\)
- face arrondie : \(r=1.2\)
- carré : \(r=1.5\)
- face plate : \(r=1.6\)

Cassiopée propose une formule de calcul en fonction de \(C{d0}\)&nbsp;:

$$ r = 0.4 C_{d0} + 0.7 $$

### Froude *F*

$$F = \frac{V_g}{\sqrt{gh}}$$

### Fonction de correction du coefficient de trainée liée au Froude *f<sub>F</sub>(F,r)*

Si \(F < 1\) (Eq. 19, Cassan et al., 2014[^2])&nbsp;:

$$f_F(F) = \min \left( \frac{r}{1- \frac{F_{g}^{2}}{4}}, \frac{1}{F^{\frac{2}{3}}} \right)^2$$

sinon \(f_F(F) = 1\) car un écoulement torrentiel à l'amont des blocs est théoriquement impossible à cause du ressaut hydraulic provoqué par le bloc aval.

### Vitesse maximale *u<sub>max</sub>*

D'après l'équation 19 de Cassan et al., 2014[^2] :


$$ u_{max} = V_g \sqrt{f_F(F)} $$


### Fonction de correction du coefficient de trainée lié à la profondeur relative *f<sub>h\*</sub>(h<sub>\*</sub>)*

L'équation utilisée dans Cassiopée diffère légèrement de l'équation 20 de Cassan et al. 2014[^2] et l'équation 6 de Cassan et al. 2016[^1].
Cette formule est un ajustement sur les mesures expérimentales sur les blocs circulaires utilisées dans de Cassan et al. 2016[^1]&nbsp;:

$$ f_{h_*}(h_*) = (1 + 1 / h_*^{2}) $$

### Coefficient de friction du lit *C<inf>f</inf>*

Si \(k_s < 10^{-6} \mathrm{m}\) alors on utilise la formule de Blasius

$$C_f = 0.3164 / 4 * Re^{-0.25}$$

avec

$$Re = u_0 \times h / \nu$$

Sinon (Eq. 3, Cassan et al., 2016 d'après Rice et al., 1998[^5])

$$C_f = \frac{2}{(5.1 \mathrm{log} (h/k_s)+6)^2}$$


## Notations

- \(\alpha\)&nbsp;: ratio de l'aire concernée par la friction du lits sur \(a_x \times a_y\)
- \(\alpha_t\)&nbsp;: échelle de longueur de la turbulence dans la couche des blocs(m)
- \(\beta\)&nbsp;: ratio entre la contrainte due à la trainée et la contrainte due aux turbulences
- \(\kappa\)&nbsp;: constante de Von Karman = 0.41
- \(\sigma\)&nbsp;: ratio entre l'aire du block dans le plan X,y et \(D^2\)
- \(a_x\)&nbsp;:  largeur d'une cellule (perpendiculaire à l'écoulement) (m)
- \(a_y\)&nbsp;:  longueur d'une cellule (parallèle à l'écoulement) (m)
- \(B\)&nbsp;: largeur de la passe (m)
- \(C\)&nbsp;: concentration de blocs
- \(C_d\)&nbsp;: coefficient de trainée d'un bloc dans les conditions d'écoulement actuel
- \(C_{d0}\)&nbsp;: coefficient de trainée d'un bloc considérant un bloc infiniment haut avec \(F \ll 1\)
- \(C_f)\)&nbsp;: coefficient de friction du lit
- \(d\)&nbsp;: déplacement dans le plan zéro du profil logarithmique (m)
- \(D\)&nbsp;: largeur du bloc face à l'écoulement (m)
- \(F\)&nbsp;: nombre de Froude basé sur \(h\) et \(V_g\)
- \(g\)&nbsp;: accélération de la gravité = 9.81 m.s<sup>-2</sup>
- \(h\)&nbsp;: profondeur moyenne (m)
- \(h_*\)&nbsp;: profondeur adimensionnelle (\(h / D\))
- \(k\)&nbsp;: hauteur utile des blocs (m)
- \(k_s\)&nbsp;: hauteur de la rugosité (m)
- \(l_0\)&nbsp;: échelle de longueur de la turbulence au sommet des blocs (m)
- \(N\)&nbsp;: ratio entre la friction du lit et la force de trainée
- \(Q\)&nbsp;: débit (m<sup>3</sup>/s)
- \(S\)&nbsp;: pente de la passe (m/m)
- \(u_0\)&nbsp;: vitesse moyenne dans le lit (m/s)
- \(u_*\)&nbsp;: vitesse de cisaillement (m/s)
- \(V\)&nbsp;: vitesse débitante (m/s)
- \(V_g\)&nbsp;: vitesse entre les blocs (m/s)
- \(s\)&nbsp;: distance minimale entre les blocs (m)
- \(z\)&nbsp;: position verticale (m)
- \(z_0\)&nbsp;: rugosité hydraulique (m)
- \(\tilde{z}\)&nbsp;: position verticale adimensionnelle \(\tilde{z} = z / k\)

[^1]: Cassan L, Laurens P. 2016. Design of emergent and submerged rock-ramp fish passes. Knowl. Manag. Aquat. Ecosyst., 417, 45. https://doi.org/10.1051/kmae/2016032

[^2]: Cassan, L., Tien, T.D., Courret, D., Laurens, P., Dartus, D., 2014. Hydraulic Resistance of Emergent Macroroughness at Large Froude Numbers: Design of Nature-Like Fishpasses. Journal of Hydraulic Engineering 140, 04014043. https://doi.org/10.1061/(ASCE)HY.1943-7900.0000910

[^3]: Tran, T.D., Chorda, J., Laurens, P., Cassan, L., 2016. Modelling nature-like fishway flow around unsubmerged obstacles using a 2D shallow water model. Environmental Fluid Mechanics 16, 413–428. https://doi.org/10.1007/s10652-015-9430-3

[^4]: Larinier, Michel, Courret, D., Gomes, P., 2006. Guide technique pour la conception des passes à poissons “naturelles,” Rapport GHAPPE RA. Compagnie Nationale du Rhône / Agence de l’Eau Adour Garonne. http://dx.doi.org/10.13140/RG.2.1.1834.8562

[^5]: Rice C. E., Kadavy K. C., et Robinson K. M., 1998. Roughness of Loose Rock Riprap on Steep Slopes. Journal of Hydraulic Engineering 124, 179‑85. https://doi.org/10.1061/(ASCE)0733-9429(1998)124:2(179)
