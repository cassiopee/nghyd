# Courbe de remous d'une passe à macro-rugosité

Ce module permet de simuler la courbe de remous d'une [passe à macro-rugosité dite "simple"](macrorugo.md) afin de déterminer le niveau d'ennoiement aval de la passe.

## Principe de fonctionnement

Les paramètres de ce module sont :

- Le choix parmi les modules présents dans la session de travail du module de
[passe à macro-rugosité dite "simple"](macrorugo.md) qui sera utilisé pour
effectuer le calcul&nbsp;
- La cote de l'eau à l'aval de la passe&nbsp;
- Le pas d'espace utilisé pour la discrétisation du calcul de la courbe.

N.B.&nbsp;: Comme le calcul de la courbe de remous ne peut se faire que pour un
jeu de paramètres, le module de [passe à macro-rugosité dite "simple"](macrorugo.md)
ne peut pas contenir de [paramètres "variés"](../../general/principe_fonctionnement.md).

Le calcul théorique effectué dans la passe à macro-rugosité correspond à un
calcul de ligne d'eau en régime uniforme où la pente de l'eau est égale à la pente
du fond de la passe. On utilise ici le module de passe à macro-rugosité pour
calculer la pente de l'eau dans le cas non-uniforme.
Le calcul de la courbe de remous fluviale s'effectue alors depuis la cote de
l'eau imposée à l'aval à l'aide de la méthode d'[intégration de trapèzes](../../theorie/methodes_numeriques/integration_trapezes.md).

Ce module se base [le module de calcul de courbe de remous d'une section
paramétrée](../hsl/courbe_remous.md) pour le calcul et l'affichage des résultats.
