# Passe à macro-rugosités

Le module de calcul passe à macro-rugosités permet de calculer les caractéristiques d'une passe à macro-rugosités constituée de blocs uniformément répartis avec des espacements transversaux \(ax\) et longitudinaux \(ay\) égaux.

![Schéma d'une disposition régulière des enrochements et notations](pam_schema_enrochement_regulier.png)

*Extrait de Larinier et al., 2006[^1]*

[^1]: Larinier, Michel, Courret, D., Gomes, P., 2006. Guide technique pour la conception des passes à poissons “naturelles,” Rapport GHAPPE RA. Compagnie Nationale du Rhône / Agence de l’Eau Adour Garonne. http://dx.doi.org/10.13140/RG.2.1.1834.8562


L'outil permet de calculer l'une des valeurs suivantes&nbsp;:

- La largeur de la passe (m)&nbsp;;
- La pente de la passe (m)&nbsp;;
- Le débit (m<sup>3</sup>/s)&nbsp;;
- La profondeur \(h\) (m)&nbsp;;
- La concentration des blocs \(C\).

Il nécessite d'entrer les valeurs suivantes&nbsp;:

- La cote de fond amont (m)&nbsp;;
- La longueur de la passe (m)&nbsp;;
- La rugosité de fond (m)&nbsp;;
- La largeur des blocs \(D\) face à l’écoulement (m)&nbsp;;
- La hauteur utile des blocs \(k\) (m)&nbsp;;
- Le coefficient de trainée d'un bloc (1 pour rond, 2 pour carré).

# Rugosité de fond

![](schema_rugosite_fond.png)
