# Concentration de blocs

Ce module permet de calculer la concentration de blocs uniformément répartis dans une [passe à macro-rugosités](./macrorugo.md).

L'outil permet de calculer l'une des valeurs suivantes&nbsp;:

- La largeur de la passe (m)&nbsp;;
- Le nombre de blocs&nbsp;;
- Le diamètre des blocs (m)&nbsp;;
- La concentration des blocs \(C\).

## Formule

![Schéma d'une disposition régulière des enrochements et notations](pam_schema_enrochement_regulier.png)

*Extrait de Larinier et al., 2006[^1]*

[^1]: Larinier, Michel, Courret, D., Gomes, P., 2006. Guide technique pour la conception des passes à poissons “naturelles,” Rapport GHAPPE RA. Compagnie Nationale du Rhône / Agence de l’Eau Adour Garonne. http://dx.doi.org/10.13140/RG.2.1.1834.8562


L'espacement entre les blocs se calcule ensuite avec la formule suivante&nbsp;:

$$ax = ay = \frac{D}{\sqrt{C}}$$

Avec :

- \(ax\) et \(ay\) respectivement les distances longitudinales et latérales entre les centres des blocs (m)&nbsp;;
- \(D\) la largeur des blocs face à l’écoulement (m)&nbsp;;
- \(C\) la concentration des blocs (-).

Le nombre de blocs \(N\)  pour une passe de largeur \(B\) s'obtient alors avec l'équation&nbsp;:

$$ N = B / ax $$

## Harmonisation

Lors du calcul du nombre de blocs, si celui-ci n'est pas entier, des nombres de blocs harmonisés à la hausse et à la baisse sont proposés accompagnés des valeurs de concentration correspondantes.

La tolérance est de l'ordre du centimètre.
