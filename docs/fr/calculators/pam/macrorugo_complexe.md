# Passe à macro-rugosités complexe

Ce module de calcul permet de calculer le débit transitant par une passe à macro-rugosité dite "complexe" car possédant un radier incliné ou de multiples radiers.

## Caractéristiques générales

Les paramètres à rentrer sont les mêmes que pour [la passe à macro-rugosité dite "simple"](macrorugo.md). Concernant le radier de la passe deux choix sont offerts&nbsp;:

- Les radiers multiples&nbsp;: il est possible de créer, dupliquer, supprimer, changer l'ordre d'autant de radiers que nécessaire. Pour chaque radier, les paramètres à rentrer sont&nbsp;: la largeur du radier et la cote du radier à l'amont de la passe.
- Le radier incliné&nbsp;: en plus de la largeur du radier, il faut entrer les cotes droite et gauche de radier à l'amont de la passe.

Les données calculées sont les mêmes que pour [la passe à macro-rugosité dite "simple"](macrorugo.md). Les résultats affichent les différentes données pour chaque radier et le graphique permet de visualiser ces données pour chaque radier (profil transversal). Dans le cas où au moins un des paramètres de calcul varie, les résultats sont disponibles individuellement via une liste déroulante.

## Cas du radier incliné

Le calcul d'une passe à radier incliné consiste à discrétiser la passe en plusieurs radiers horizontaux. La largeur des radiers créés est fixée à la distance séparant deux blocs avec un ajustement du radier le plus haut pour obtenir la largeur totale de la passe. Il est possible d'éditer les radiers créés en sélectionnant "Radiers multiples" après avoir effectué un calcul avec un radier incliné.
