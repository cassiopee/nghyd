# Passe à rugosités de fond

Les rampes à rugosités de fond sont généralement constituées de blocs de
dimensions relativement uniformes disposés les uns contre les autres et formant
un coursier de pente plus ou moins importante.

![Schéma coupe longitudinale d'une passe à rugosités de fond](rugofond_schema.png)

## Avertissement sur la franchissabilité de l'ouvrage

En l'absence de bassins et d'éléments protubérant dans l'écoulement, ces rampes
ne présentent pas de zones de repos pour les poissons en montaison. Les poissons
doivent ainsi les franchir d'une traite en utilisant généralement leur vitesse
de nage maximale. La franchissabilité d'une rampe à rugosités de fond dépend des
vitesses de l'écoulement, des hauteurs d'eau au dessus de la crête des
enrochements, de la "qualité" de la lame d'eau (absence de ressauts et de
décollement rendant la nage du poisson difficile, sinon impossible), mais aussi
de la longueur de la rampe que les poissons doivent parcourir avant d'être
fatigués. C'est pourquoi les rampes à rugosités de fond ne sont pas considérées
comme des dispositifs de franchissement à proprement parler (Larinier et al.
2006). Ce type de dispositif peut être utile (1) pour rendre un seuil
directement franchissable par les espèces aux meilleures capacités de nage
(salmonidés, voire aloses et lamproies) ou (2) pour former des prébarrages
associant des rampes courtes avec des chutes réduites et des bassins
intermédiaires.

## Méthode de calcul

### <a name = "apron_type"></a>Radier incliné

Le radier incliné est calculé en discrétisant la largeur de passe en plusieurs
radiers horizontaux dont le nombre est déterminé par le paramètre "Nombre de
tranches d'écoulement".

### Relation entre la charge amont sur la crête et le débit

La relation entre la charge amont et le débit est représenté par une [formule de
déversoir dénoyé](../structures/seuil_denoye.md).

### Relation entre la hauteur d'eau, la vitesse débitante et le débit en régime uniforme

A partir du débit fourni ou calculé en entrée de passe, la hauteur d'eau et la
vitesse débitante dans la passe sont calculées avec [la formule de
Manning-Strickler en régime uniforme](../hsl/regime_uniforme.md) pour une section
rectangulaire équivalente à celle de la passe.

<a name="coef_a"></a>Le coefficient de Strickler est évalué à partir du
\(D_{65}\) des enrochements:

$$ K_s = \dfrac{a}{{D_{65}}^{1/6}} $$

Avec \(a\) le **coefficient correcteur du Strickler** fonction du mode de mise en place des enrochements et
de leur niveau de jointement (Larinier et al., 2006):

 - enrochements déversés, non jointoyés&nbsp;: 21
 - enrochements disposés de manière compacte sans jointoiement&nbsp;: 15.5
 - jointoiement à 30%&nbsp;: 16.7
 - jointoiement à 50%&nbsp;: 18

## Références

Larinier, M., J. Chorda, et O. Ferlin. 1995. « Le franchissement des seuils en
enrochements par les poissons migrateurs : étude expérimentale ». GHAAPPE
95/05-HYDRE 161. irstea. [https://hal.inrae.fr/hal-02575575](https://hal.inrae.fr/hal-02575575).

Larinier, Michel, Dominique Courret, et Peggy Gomes. 2006. « Guide technique
pour la conception des passes à poissons “naturelles” ». Rapport GHAPPE RA.
Compagnie Nationale du Rhône / Agence de l'Eau Adour Garonne.
[https://dx.doi.org/10.13140/RG.2.1.1834.8562](https://dx.doi.org/10.13140/RG.2.1.1834.8562).
