# Solveur multimodule

Le solveur multimodule permet de calculer la valeur d'un paramètre d'entrée d'un module dépendant d'une chaine de calcul constituée de plusieurs modules pour laquelle on cherche à atteindre une valeur cible pour un paramètre calculé en sortie.

Le schéma ci-dessous reprend l'exemple d'une chaîne de calcul comprenant deux modules. Le module `x` prend entrée le paramètre `Input x` et calcule le paramètre `Output x`. Le module `y` prend entrée le paramètre `Input y` et calcule le paramètre `Output y`. Le paramètre `Input y` est lié au paramètre `Output x`. Le problème à résoudre par le solveur est d'obtenir la valeur de l'`Input x` permettant d'obtenir une valeur cible en `Output y`.

<div class="mermaid">
    graph LR
    Ix([Input x])
    x[Module x]
    Ox([Output x / Input y])
    y[Module y]
    Oy([Output y])
    Ix --> x
    x --> Ox
    Ox --> y
    y --> Oy
    Oy -..->|Input x pour une valeur cible Output y ?| Ix
</div>

Pour résoudre ce problème, il faut définir les caractéristiques du paramètre cible qui comprennent&nbsp;:

- Le module et son paramètre calculé (Ici le module `y` calculant `Output y`)&nbsp;;
- Le résultat ciblé qui peut être le paramètre calculé par le module où un de ces résultats complémentaire (Ici `Output y`)&nbsp;;
- La valeur cible désirée.

Il faut aussi définir les caractéristiques du paramètre recherché&nbsp;:

- Le paramètre recherché est à choisir parmi la liste des paramètres en entrée des modules de la session (Ici le paramètre `Input x` du module `x`)&nbsp;;
- Une valeur initiale de ce paramètre pour démarrer le calcul.

La valeur initiale choisie doit être assez proche de la solution car il n'y a aucune garantie que la fonction issue de l'enchaînement des calculs soit continue et monotone.

Des exemples d'utilisation du solveur multimodule sont présents dans les sessions exemples "Débit d'un chenal avec ouvrages" et "Pente critique d'un canal". Les notes de ces sessions décrivent l'enchaînement des modules et l'utilisation du solveur.
