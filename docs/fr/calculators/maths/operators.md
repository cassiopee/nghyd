# Opérateurs et fonctions trigonométriques

Les opérateurs et fonctions mathématiques basiques pourvus par Cassiopée permettent avant tout de faciliter le lien entre les résultats d'un module de calcul et l'entrée dans autre dans le cas d'un enchaînement de calcul entre plusieurs modules. L'exemple fourni "Longueur de jet d'un déversoir" montre des utilisations du module "Fonction affine".

## Fonction affine

Le module fonction affine résout l'équation de droite&nbsp;:

$$ y = a x + b $$

Trois paramètres doivent être saisis et le module calcule le paramètre manquant.

## Somme et produit de puissances

Ce module permet d'écrire une équation sommant des puissances sous la forme \(a x ^ n\) avec \(a\), \(x\), et \(n\) des réels.

Dans le cas d'une somme l'équation résolue par le module s'écrit&nbsp;:

$$ y = \sum_{i=1}^{k} a_i x_i ^ {n_i}$$

Dans le cas d'un produit l'équation résolue par le module s'écrit&nbsp;:

$$ y = \prod_{i=1}^{k} a_i x_i ^ {n_i}$$

Tous les paramètres doivent être saisis à l'exception du dernier qui est la valeur calculée dans l'équation.

## Fonction trigonométrique

Ce module permet de calculer la valeur d'une fonction trigonométrique ou son inverse.

L'équation résolue par ce module s'écrit&nbsp;:

$$ y = f(x) $$

Avec \(f\), une fonction trigonométrique et \(x\) un angle en degré ou radian.

Le paramètre "Opération" permet de choisir l'opérateur parmi les fonctions disponibles sont&nbsp;: cos, sin, tan, cosh, sinh et tanh. Le paramètre "Unité" permet de choisir entre degré et radian.
