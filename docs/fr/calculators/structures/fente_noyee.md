# Formule de la fente noyée

![Schéma de la fente noyée](fente_noyee_schema.png)

*Extrait de Larinier, M., Travade, F., Porcher, J.-P., Gosset, C., 1992. Passes à poissons : expertise et conception des ouvrages de franchissement. CSP. (page 94)*

Larinier (1992) propose l'équation suivante&nbsp;:

$$Q = C_d b H_1\sqrt{2g \Delta H}$$

Avec&nbsp;:

* *b* la largeur de la fente en m&nbsp;
* *H<sub>1</sub>* la charge sur la fente m&nbsp;
* *C<sub>d</sub>* le coefficient de débit.

# Coefficient de débit C<sub>d</sub> pour la formule de la fente noyée (passe à bassins à fente verticale)

Le coefficient de débit **C<sub>d</sub>** est un paramètre important pour le dimensionnement des passes à fente(s) verticale(s). Ce terme est intégré dans la formule de débitance d’un déversoir de type fente noyée, qui relie le débit transité avec la chute entre bassins, la charge amont sur la fente et sa largeur.

**Pour les passes à simple fente verticale**, des études réalisées sur modèle-réduit et par simulations numériques ont permis de déterminer des valeurs moyennes de coefficients de débit selon différentes configurations, pour une géométrie type de fente et de déflecteurs définie à partir des passes à fentes verticales construites en France (Figure 1). Une géométrie différente de fente et de déflecteur peut induire des écarts par rapport aux valeurs moyennes des coefficients de débit selon les configurations indiquées ci-dessous.

![Figure 1](fente_noyee_fig1.png)

*Figure 1 : géométrie type de fente et de déflecteurs à partir de laquelle les valeurs des coefficients de débit ont été déterminés (Ballu, 2017)*

* **Dans des conditions de radier lisse (sans rugosité de fond) et en l’absence de seuil dans la fente**, Wang *et al.* (2010) et Ballu *et al.* (2017) ont montré que la valeur du **C<sub>d</sub>** n’est pas significativement influencée par le débit et dépend principalement de la pente **S** (%) et du rapport entre la largeur des bassins **B** et la largeur de la fente **b** (**B/b**).

* **Dans des conditions de radier lisse (sans rugosité de fond) avec la présence d’un seuil dans la fente**, selon 3 configurations de hauteurs de seuils **h<sub>s</sub>** rapportées à la largeur de la fente **b** (**h<sub>s</sub>/b** = 0.5, 1 et 1.5), Ballu *et al.* (2015) et Ballu (2017) ont montré que la valeur du **C<sub>d</sub>** n’est pas significativement influencée par le débit, et qu’elle est principalement influencée par la pente **S**, le rapport de forme **B/b** et la hauteur des seuils **h<sub>s</sub>**.

* **Dans des conditions de radier avec rugosités de fond préfabriquées et en l’absence de seuil dans la fente**, avec des rugosités régulièrement réparties de hauteur **h<sub>r</sub>/b** = 2/3 et de diamètre **Ø<sub>r</sub>/b** = 1/2, selon 2 configurations de densités **d<sub>r</sub>** de 10% et 15%, Ballu *et al.* (2017) et Ballu (2017) ont montré que la valeur du **C<sub>d</sub>** n’est pas significativement influencée par le débit, et qu’elle est principalement influencée par la pente **S**, le rapport de forme **B/b** et la présence de la rugosité de fond **d<sub>r</sub>**.

* En fonction de ces différentes configurations, les valeurs moyennes du coefficient de débit s’étalent d’environ 0.62 jusqu’à près de 0.88 (Figure 2).

![Figure 2](fente_noyee_fig2.png)

*Figure 2 : valeurs moyennes et incertitudes (intervalles de confiance à 95%, k=2) des coefficients de débits selon la pente S et le rapport de forme B/b pour différentes configurations&nbsp;: (A) radier lisse sans seuil dans la fente, (B) rugosités de fond d<sub>r</sub> = 10% sans seuil dans la fente, (C) rugosités de fond d<sub>r</sub> = 15% sans seuil dans la fente, (D) seuil dans la fente h<sub>s</sub>/b = 0.5 et radier lisse, (E) seuil dans la fente h<sub>s</sub>/b = 1 et radier lisse, (F) seuil dans la fente h<sub>s</sub>/b = 1.5 et radier lisse*

**Pour les passes à doubles fentes verticales**, des valeurs de **C<sub>d</sub>** comprises entre 0.75 et 0.80 sont couramment retenues pour les dispositifs construits récemment avec des pentes longitudinales entre 4% et 5.5%. Des études sont en cours afin de préciser l’influence de différentes configurations sur les coefficients de débits et leurs valeurs correspondantes.

**Bibliographie :**

Ballu A., Pineau G., Calluaud D., David L. (2015). Experimental study of the influence of sills on vertical slot fishway flow. *36<sup>th</sup> IAHR World Congress*, 7p.

Ballu A. (2017). Étude numérique et expérimentale de l’écoulement turbulent au sein des passes à poissons à fentes verticales. Analyse de l’écoulement tridimensionnel et instationnaire. *Thèse de l’Université de Poitiers*, 223p.

Ballu A., Calluaud D., Pineau G., david L. (2017). Experimental study of the influence of macro‑roughnesses on vertical slot fishway flows. *La Houille Blanche*, 2: 9-14.

Wang R.W., David L., Larinier M. (2010). Contribution of experimental ﬂuid mechanics to the design of vertical slot ﬁsh passes. *Knowledge and Management of Aquatic Ecosystems*, 396(2).
