# Formules de Cunge 1980

Cette loi de débit est inspirée des équations décrites par Cunge dans son livre [^1], ou avec plus de détails dans un article de Mahmood et Yevjevich [^2]. Cette loi est une compilation des lois classiques prenant en considération les différentes conditions d’écoulement : noyé, dénoyé, à surface libre et en charge tout comme les équations [CEM88(D) : Déversoir / Orifice (pelle importante)](cem_88_d.md) et [CEM88(V) : Déversoir / Vanne de fond (pelle faible)](cem_88_v.md). Cependant contrairement à ces équations, celle-ci n'asssure aucune continuité entre les conditions d’écoulement à surface libre et en charge. Cela peut entraîner des problèmes de calcul au voisinage de cette transition.

Cette loi est adaptée pour un seuil rectangulaire à crête épaisse, éventuellement associée à une vanne. Le coefficient de débit par défaut \(C_d = 1\) correspond aux coefficients de débits suivant pour les équations classiques:

- \(C_d = 0.385\) pour [le seuil dénoyé](seuil_denoye.md).
- \(C_d = 1\) pour [le seuil noyé](seuil_noye.md).
- \(C_d = 1\) pour [la vanne noyée](vanne_noyee.md).
- \(C_c = 0.611\) pour [la vanne dénoyée](vanne_denoyee.md) avec \(C_d\) calculé à partir de \(C_c\) (Voir ci-dessous).

Une modification du coefficient de débit modifiera proportionnellement les coefficients ci-dessus.

## Détection du régime noyé / dénoyé

Le régime d'écoulement est dénoyé lorsque la hauteur d'eau à l'aval est inférieure à la hauteur critique :

$$ (Z_2 - Z_{dv}) < \dfrac{2}{3} (Z_1 - Z_{dv})$$

avec \(Z_1\) la cote de l'eau à l'amont, \(Z_2\) la cote de l'eau à l'aval, et \(Z_{dv}\) la cote de radier de l'ouvrage.

## Détection de l'écoulement en charge ou à surface libre

On considère ici que la hauteur d'eau au niveau de la vanne quand celle-ci est ouverte est égale à&nbsp;:

- la hauteur critique dans le cas d'un régime dénoyé&nbsp;:
- la hauteur d'eau aval dans le cas d'un régime noyé.

L'écoulement se met en charge à partir du moment où la pelle de la vanne touche la surface de l'eau à cet endroit.

En régime dénoyé, l'écoulement est donc en charge quand&nbsp;:

$$ W \leq \dfrac{2}{3} (Z_1 - Z_{dv})$$

En régime noyé, la condition devient&nbsp;:

$$ W \leq Z_2$$

## Equations de débit

L'équation de la vanne dénoyée utilise un coefficient de contraction \(C_c\) fixe avec &nbsp;:

\(C_d = \frac{C_c}{\sqrt{1 + C_c W / h_{am}}}\)

Pour tous les autres régimes d'écoulement les équations utilisées sont les suivantes telles qu'elles peuvent être indépendamment utilsées&nbsp;:

|        | Surface libre (seuil rectangulaire) | En charge (orifice rectangulaire) |
|--------|---------------|-----------|
| Dénoyé | [Seuil dénoyé](seuil_denoye.md) | [Vanne dénoyée](vanne_denoyee.md) |
| Noyé   | [Seuil noyé](seuil_noye.md) | [Vanne noyée](vanne_noyee.md) |

[^1]: Cunge, Holly, Verwey, 1980, "Practical aspects of computational river hydraulics", Pitman, p. 169 pour les seuils et p. 266 pour les vannes.

[^2]: Mahmood K., Yevjevich V., 1975, "Unsteady flow in open channels, Volume 1 and 2", Water resources publications, Fort Collins, USA, 923 p.
