# Formule de Kindsvater-Carter et Villemonte


Le module de calcul permet d'effectuer des calculs hydrauliques pour plusieurs ouvrages en parallèle.

## Formule de Kindsvater-Carter (1957)

![Formule de Kindsvater-Carter&nbsp;: Schéma déversoir](kivi_schema_seuil.png)

*Vue en perspective d'un déversoir rectangulaire avec contraction latérale (Extrait de CETMEF, 2005[^CETMEF2005])*

[^CETMEF2005]: CETMEF. Notice sur les déversoirs : synthèse des lois d’écoulement au droit des seuils et déversoirs. Compiègne: Centre d’Études Techniques Maritimes Et Fluviales, 2005. <http://www.side.developpement-durable.gouv.fr/EXPLOITATION/DEFAULT/doc/IFD/IFD_REFDOC_0513410/notice-sur-les-deversoirs-synthese-des-lois-d-ecoulement-au-droit-des-seuils-et-deversoirs>

La formule de Kindsvater-Carter correspond à la formule classique du déversoir&nbsp;:

$$Q = \mu L \sqrt{2g}h_1^{1.5}$$

Avec&nbsp;:

- \(\mu\) le coefficient de débit \(\mu = \alpha + \beta h_1/p\)
- \(L\) la largeur du déversoir
- \(h_1\) la hauteur d'eau au dessus de la crête du déversoir
- \(p\) la pelle ou hauteur de la crête du déversoir

Les coefficient \(\alpha\) et \(\beta\) dépendent du rapport entre la largeur du déversoir (\(L\)) et la largeur du bassin (\(B\)). Leurs valeurs sont données par les abaques ci dessous (extrait de Larinier, M., Porcher, J.-P., 1986. Programmes de calcul sur HP86 : hydraulique et passes à poissons)&nbsp;:

![Formule de Kindsvater-Carter&nbsp;: Abaques](kivi_abaques_alpha_beta.png)

## Écoulement noyé&nbsp;: formule de Villemonte (1947)

![Formule de Villemonte&nbsp;: schéma seuil noyé](kivi_villemonte_schema_seuil_noye.png)

*Extrait de CETMEF, 2005[^CETMEF2005]*

Pour une cote de l'eau aval supérieure à la cote de la crête du déversoir, l'écoulement est noyé et un coefficient de noyage s'applique sur le coefficient de débit.

Villemonte propose la formule suivante&nbsp;:

$$K = \frac{Q_{noyé}}{Q_{dénoyé}} = \left [ 1- \left ( \frac{h2}{h1} \right)^n \right]^{0.385}$$

Avec&nbsp;:
- \(h_1\)la hauteur d'eau amont au dessus de la crête du déversoir
- \(h_2\)la hauteur d'eau aval au dessus de la crête du déversoir
- \(n\)l'exposant dans les relations d'écoulement dénoyé (rectangulaire=1.5, triangulaire=2.5, parabolique=2)

Voir aussi : [Villemonte 1947](villemonte_1947.md)
