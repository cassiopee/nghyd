# Formule du déversoir noyé (Rajaratnam, 1969)

![Schéma du seuil noyé](seuil_noye_schema.png)

En régime noyé, le débit dépend de la hauteur d'eau amont \(h_{amont}\) et de la hauteur aval \(h_{aval}\) (Rajaratnam et al., 1969)&nbsp;:

$$Q = Cd \sqrt{2g} Lh_{aval} \sqrt{h_{amont}-h_{aval}}$$

Avec&nbsp;:

* *L* la largeur du seuil en m&nbsp;
* *h<sub>amont</sub>* la charge sur le seuil à l'amont en m&nbsp;
* *h<sub>aval</sub>* la charge sur le seuil à l'aval en m&nbsp;
* *C<sub>d</sub>* le coefficient de débit (égal à 0.9 par défaut).

Cette formule n'est pas conseillée pour un ennoiement inférieur à 80%.

*Rajaratnam, N., Muralidhar, D., 1969. Flow below deeply submerged rectangular weirs. Journal of Hydraulic Research 7, 355–374.*
