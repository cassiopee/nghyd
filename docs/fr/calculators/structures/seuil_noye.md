# Formule du déversoir noyé (Rajaratnam, 1969)

![Schéma du seuil noyé](seuil_noye_schema.png)

En régime noyé, le débit dépend de la hauteur d'eau amont \(h_1\) et de la hauteur aval \(h_2\) (Rajaratnam et al., 1969)&nbsp;:

$$Q = Cd \sqrt{2g} Lh_2 \sqrt{h_1-h_2}$$

Avec&nbsp;:

* \(L\) la largeur du seuil en m&nbsp;
* \(h_1\) la charge sur le seuil à l'amont en m
* \(h_2\) la charge sur le seuil à l'aval en m&nbsp;
* \(C_d\) le coefficient de débit (égal à 0.9 par défaut).

Cette formule n'est pas conseillée pour un ennoiement inférieur à 80%.

*Rajaratnam, N., Muralidhar, D., 1969. Flow below deeply submerged rectangular weirs. Journal of Hydraulic Research 7, 355–374.*
