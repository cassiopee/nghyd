# Lois de déversoirs dénoyés

Ce module de calcul est similaire à celui des [Lois d'ouvrages](lois_ouvrages.md) à la différence près qu'il ne simule que des écoulements dénoyés et affine le calcul en utilisant la vitesse d'approche dans le bief amont.

Il permet de calculer la relation entre le niveau de l'eau à l'amont d'un déversoir et le débit. Il est
utilisé le plus souvent pour évaluer la relation cote amont-débit au niveau d'un seuil ou d'un ouvrage
évacuateur d'un aménagement. L'ouvrage peut comporter plusieurs niveaux de déversement distincts et des
déversoirs rectangulaires (à cote de déversement horizontale), triangulaires, semi-triangulaires ou encore
triangulaires tronqués.

L'utilisation classique consiste à entrer les niveaux extrêmes (mini/maxi) de la cote de l'eau amont ainsi que le pas de calcul
pour lesquels on désire effectuer les estimations de débit.

Les caractéristiques du bief amont (la largeur du bief amont ainsi et la cote du lit) permettent d'évaluer la vitesse d'approche ainsi que l'énergie cinétique amont, exprimée en mètre, et de calculer le débit total à partir de la charge totale compte tenu d'une vitesse d'approche non négligeable.

$$ V = \frac{Q}{L \times (Z_1 - Z_{lit})} $$

avec \(V\), la vitesse d'approche, \(Q\) le débit, \(Z_1\) la cote de l'eau à l'amont des ouvrages, \(Z{lit}\) la cote du lit à l'amont.

$$ E_c = \frac{V^2}{2g} $$

avec \(E_c\) l'énergie cinétique amont en mètres, et \(g\) l'accélération de la pesanteur (9.81 m.s<sup>-2</sup>).

La charge \(H\) utilisée pour le calcul du débit vaut alors :

$$ H = Z_1 + E_c $$

La difficulté du calcul réside dans le fait que le débit à calculer intervient dans le calcul de la charge. Ce problème est résolu avec l'algorithme du point fixe où le calcul du débit est répété plusieurs fois en mettant la charge à jour à chaque itération jusqu'à ce que le calcul converge vers la valeur finale du débit.

Le coefficient de correction lié à la vitesse d'approche \(Cv\) est ensuite calculé en faisant le rapport entre le débit obtenu avec la charge \(H\) et le calcul du débit avec la cote amont \(Z_1\).
