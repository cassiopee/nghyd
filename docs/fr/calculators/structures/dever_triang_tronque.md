# Formule du déversoir triangulaire tronqué

TT1 caractérisé par&nbsp;:

* \(C_d\)&nbsp;: coefficient de débit
* \(Z_d\)&nbsp;: cote de déversement basse du triangle
* \(Z_t\)&nbsp;: cote de déversement haute du triangle
* \(B/2\)&nbsp;: demi-ouverture du triangle

## Formule

### pour \(Z_{am} \leq Z_t\)

$$Q = C_d \frac{B}{2 (Z_t - Z_d)} \left ( Z_{am} - Z_d \right )^{2.5}$$

### pour \(Z_{am} > Z_t\)

$$Q = C_d \frac{B}{2 (Z_t - Z_d)} \left ( \left ( Z_{am} - Z_d \right )^{2.5} - \left ( Z_{am} - Z_t \right )^{2.5} \right )$$

Déversoir en mince paroi&nbsp;: \(C_d\) = 1.37

Déversoir épais sans contraction (arrondi \(r > 0.1 * h1\))&nbsp;: \(C_d\) = 1.27

Déversoir à profil triangulaire&nbsp;: (1/2 amont, 1/2 ou 1/5 aval)&nbsp;: \(C_d\) = 1.68 et 1.56
