# CEM88(D) : Déversoir / Orifice (pelle importante)

![Schéma CEM 88 D](cem_88_d_schema.jpg)

## Déversoir - régime dénoyé
(\(h_1 < W et h_2 \leq \frac{2}{3} h_1\))

\(Q = \mu_F L \sqrt{2g} h_1^{3/2}\)

Formulation classique du déversoir dénoyé (\(\mu_F \simeq 0.4\)).

## Déversoir - régime noyé
(\(h_1 < W\) et \(h_2 \geq \frac{2}{3} h_1\))

\(Q = \mu_S L \sqrt{2g} (h_1-h_2)^{1/2} h_2\)

Formulation classique du déversoir noyé.

Le passage noyé-dénoyé s’effectue pour \(h_2 = \frac{2}{3} h_1\), on a alors :

\(\mu_S = \frac{3 \sqrt{3}}{2} \mu_F\) pour \(\mu_F = 0.4 \Rightarrow \mu_S = 1.04\)

On peut calculer un coefficient de débit dénoyé équivalent :

\(\mu_{F} = \frac{Q}{L \sqrt{2g} h_{1}^{3/2}}\)

qui permet de juger du degré d’ennoiement du seuil en le comparant au coefficient dénoyé \(\mu_F\) introduit. En effet, le coefficient directeur de l’ouvrage introduit est celui du déversoir dénoyé (\(\mu_F\) proche de \(0.4\)).

## Orifice - régime dénoyé
(\(h_1 \geq W\) et \(h_2 \leq \frac{2}{3} h_1\))

On prend une formulation du type :

\(Q = \mu L \sqrt{2g} \left( h_1^{3/2} - (h_1 - W)^{3/2} \right)\)

Cette modélisation s’applique bien aux orifices rectangulaires de grande largeur.

La continuité vers le fonctionnement à surface libre est assuré quand :

\(\frac{h1}{W} = 1\), on a alors \(\mu = \mu_F\).

## Orifice - régime noyé

Il existe deux formulations suivant que l’on est partiellement noyé ou totalement noyé.

### Régime partiellement noyé
(\(h_1 \geq W\) et \(\frac{2}{3} h_1 < h_2 < \frac{2}{3} h_1 + \frac{W}{3}\))

\(Q = \mu_F L \sqrt{2g} \left[ \frac{3\sqrt{3}}{2} \left( \left( h_1 - h_2 \right)^{1/2} h_2 \right) - \left(h_1 - W \right)^{3/2} \right]\)

### Régime totalement noyé
(\(h_1 \geq W\) et \(\frac{2}{3} h_1 + \frac{W}{3} < h_2\))

\(Q = \mu' L \sqrt{2g} (h_1-h_2)^{1/2} \left[ h_2 - (h_2 - W) \right]
\Rightarrow Q = \mu' L \sqrt{2g} (h_1-h_2)^{1/2} W\)

Formulation classique des orifices noyés, avec \(\mu' = \mu_S\).

Le fonctionnement déversoir orifice est représenté par les équations ci-dessus et la figure 19. Quel que soit le type d’écoulement en charge, on calcule un coefficient de débit dénoyé équivalent correspondant à la formulation classique de l’orifice dénoyé :

\(C_F = \frac{Q}{L \sqrt{2g} W (h_1 - 0.5 W)^{1/2}}\)

![Graphique CEM 88 D](cem_88_d_graphique.jpg)

 * (12) : Déversoir - dénoyé
 * (15) : Orifice - partiellement noyé
 * (13) : Déversoir - régime noyé
 * (16) : Orifice - totalement noyé
 * (14) : Orifice - dénoyé

Figure 19. Déversoir - Orifice
