# Lois d'ouvrages

## Description du module de calcul

Ce module de calcul permet de simuler le fonctionnement hydraulique de vannes et de seuils placés en parallèle. Toutes les lois de débits présentent dans Cassiopée sont regroupées dans ce module qui permet notamment de comparer facilement les lois de débit entre elles.

Ce module permet de calculer n'importe quel paramètre manquant parmi&nbsp;:

- Les conditions aux limites (cote de l'eau à l'amont et à l'aval des ouvrages);
- Le débit traversant les ouvrages;
- Les paramètres des ouvrages (cote de crête, largeur, coefficient de débit...).

Le module calcule le paramètre demandé et affiche pour chaque ouvrage présent&nbsp;:

- Le débit transitant dans l'ouvrage;
- Le type d'écoulement&nbsp;: en charge (écoulement pincé sous une vanne), ou à surface libre;
- Le régime&nbsp;: noyé, partiellement noyé ou dénoyé;
- Le type de jet pour les écoulements à surface libre&nbsp;: de surface ou plongeant.

## Type de jet

Pour la définition du type de jet (plongeant ou de surface), voir&nbsp;: Larinier, M., 1992. Passes à bassins successifs, prébarrages et rivières artificielles. Bulletin Français de la Pêche et de la Pisciculture 45–72. <https://doi.org/10.1051/kmae:1992005>.

![Schéma du type de jet](type_de_jet.png)

*Extrait de Larinier, M., 1992. Passes à bassins successifs, prébarrages et rivières artificielles. Bulletin Français de la Pêche et de la Pisciculture 45–72. <https://doi.org/10.1051/kmae:1992005>*

La définition retenue dans Cassiopée est la suivante&nbsp;:

- si \(DH \geq 0.5 H1\) alors le jet est plongeant;
- si \(DH < 0.5 H1\) alors le jet est de surface.

Avec \(H1\), la charge à l'amont du seuil et \(DH\) la perte de charge sur le seuil.
