# Formule de l'orifice noyé

![Schéma orifice noyé](orifice_noye_schema.png)

*Extrait de Larinier, M., Travade, F., Porcher, J.-P., Gosset, C., 1992. Passes à poissons : expertise et conception des ouvrages de franchissement. CSP. (page 94)*

L'équation correspond à peu de chose près à celle du module de calcul de la vanne rectangulaire noyée à la différence près que la surface de l'orifice est donnée directement plutôt que par le rapport de la largeur avec la hauteur&nbsp;:

$$Q = \mu S \sqrt{2g \Delta H}$$

Avec&nbsp;:

* *Q* le débit en m<sup>3</sup>/s&nbsp;;
* *μ* le coefficient de débit (égal à 0.7 par défaut);
* *S* la surface de l'orifice en m<sup>2</sup>&nbsp;;
* *ΔH* La perte de charge *H<sub>1</sub> - H<sub>2</sub>* en m&nbsp;(noté "Chute" dans Cassiopée).
