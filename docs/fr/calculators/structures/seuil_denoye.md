
# Formule du déversoir dénoyé

La formule est dérivée de la formule originale de Poleni (1717).

En régime dénoyé, le débit ne dépend que de la hauteur d'eau amont \(h_{amont}\)&nbsp;:

$$Q = Cd \sqrt{2g} L h_{amont}^{3/2}$$

Avec&nbsp;:

* *Q* le débit en m<sup>3</sup>/s&nbsp;
* *C<sub>d</sub>* le coefficient de débit
* \(g\) l'accélération de la pesanteur terrestre égale à 9.81 m/s<sup>2</sup>
* *L* la largeur du déversoir en m&nbsp;
* *h<sub>amont</sub>* la hauteur d'eau à l'amont au dessus de la crète du déversoir en m&nbsp;

Une valeur du coefficient de débit \(C_d = 0.4\)) est généralement une bonne approximation pour un seuil rectangulaire. Pour des formes de déversoir plus complexes (trapézoïdale, circulaire…) ou pour prendre en compte des caractéristiques du profil longitudinal (seuil à crête mince, à crête épaisse), on pourra se reporter à la notice sur les déversoirs du CETMEF (CETMEF, 2005).

*[CETMEF (2005). Notice sur les déversoirs : synthèse des lois d’écoulement au droit des
seuils et déversoirs. Compiègne : Centre d’Études Techniques Maritimes Et Fluviales.
89 p.](http://www.side.developpement-durable.gouv.fr/EXPLOITATION/DEFAULT/doc/IFD/IFD_REFDOC_0513410/notice-sur-les-deversoirs-synthese-des-lois-d-ecoulement-au-droit-des-seuils-et-deversoirs)*
