# Formule de l'orifice dénoyé

![Schéma orifice dénoyé](orifice_denoye_schema.png)

*Extrait de CARLIER, M. (1972). Hydraulique générale et appliquée. OCLC : 421635236. Paris : Eyrolles*

La formule générale pour un orifice ou un ajutage dénoyé est la suivante (CARLIER, 1972)&nbsp;:

$$Q = C_d S \sqrt{2g H}$$

Avec&nbsp;:

* \(Q\) le débit en m<sup>3</sup>/s&nbsp;;
* \(C_d\) le coefficient de débit;
* \(S\) la surface de l'orifice en m<sup>2</sup>&nbsp;;
* \(g\) l'accélération de la pesanteur terrestre égale à 9.81 m/s<sup>2</sup>
* \(H\) La hauteur d'eau mesurée entre la surface de l'eau et le centre de l'orifice en mètres.

La surface \(S\) à prendre en compte est la section la plus réduite de l’orifice ou de l’ajutage
(Figure 5.12c).
Le coefficient de débit \(C_d\) varie en fonction du type d’orifice ou d’ajutage. La Figure 5.12
présente les formes et les coefficients de débit les plus courants (Source CARLIER, 1972).
