# Villemonte 1947

L'équation "Villemonte (1947)[^Villemonte1947]" utilise l'équation du [seuil dénoyé](seuil_denoye.md) auquel s'applique le coefficient de noyage proposé par Villemonte (cf. explications ci-dessous). Ce coefficient de noyage est aussi utilisé pour les formules de seuil triangulaire et seuil triangulaire tronqué.

![Formule de Villemonte&nbsp;: schéma seuil noyé](kivi_villemonte_schema_seuil_noye.png)

*Extrait de CETMEF, 2005[^CETMEF2005]*

[^CETMEF2005]: CETMEF. Notice sur les déversoirs : synthèse des lois d’écoulement au droit des seuils et déversoirs. Compiègne: Centre d’Études Techniques Maritimes Et Fluviales, 2005. <http://www.side.developpement-durable.gouv.fr/EXPLOITATION/DEFAULT/doc/IFD/IFD_REFDOC_0513410/notice-sur-les-deversoirs-synthese-des-lois-d-ecoulement-au-droit-des-seuils-et-deversoirs>

Pour une cote de l'eau aval supérieure à la cote de la crête du déversoir, l'écoulement est noyé et un coefficient de noyage s'applique sur le coefficient de débit.

Villemonte propose la formule suivante&nbsp;:

$$K = \frac{Q_{noyé}}{Q_{dénoyé}} = \left [ 1- \left ( \frac{h2}{h1} \right)^n \right]^{0.385}$$

Avec&nbsp;:

 - \(h_1\) la hauteur d'eau amont au dessus de la crête du déversoir
 - \(h_2\) la hauteur d'eau aval au dessus de la crête du déversoir
 - \(n\) l'exposant dans les relations d'écoulement dénoyé (rectangulaire=1.5, triangulaire=2.5, parabolique=2)

[^Villemonte1947]: Villemonte, J.R., 1947. Submerged weir discharge studies. Engineering news record 866, 54–57.
