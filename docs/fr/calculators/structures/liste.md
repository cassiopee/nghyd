# Liste des équations d'ouvrages

| Typologie d'ouvrages                                        | Géométrie de l'ouvrage | Type d'ouvrage                           | Régimes d'écoulement | Nom de l'équation              | Coef. Débit par défaut  | Modules                                                           |
|-------------------------------------------------------------|------------------------|------------------------------------------|----------------------|--------------------------------|-------------------------|-------------------------------------------------------------------|
| Seuil déversant à surface libre                             | Rectangulaire          | Seuil mince (échancrure)                 | Dénoyé               | [Poleni](./seuil_denoye.md)    | 0,4                     | Lois d'ouvrages, lois déversoirs dénoyés                          |
|                                                             |                        | Seuil mince (fente)                      | Noyé                 | [Rajaratnam](./seuil_noye.md)  | 0,9                     | Lois d'ouvrages                                                   |
|                                                             |                        | Seuil mince (fente)                      | Noyé                 | [Fente Larinier](./fente_noyee.md) | à définir par projeteur | Lois d'ouvrages, cloisons, cloison aval                           |
|                                                             |                        | Seuil mince (échancrure)                 | Noyé / dénoyé        | [Kindsvater-Carter & Villemonte](./kivi.md) | ⍺ = 0.4 ; ß = 0.001     | Lois d'ouvrages                                                   |
|                                                             |                        | Seuil mince (échancrure)                 | Noyé / dénoyé        | [Villemonte](./villemonte_1947.md) | 0,4                     | Lois d'ouvrages, cloisons, cloison aval                           |
|                                                             |                        | Seuil mince régulé (échancrure)          | Noyé / dénoyé        | [Villemonte](./villemonte_1947.md)    | 0,4                     | Cloison aval                                                      |
|                                                             |                        | Seuil mince régulé (fente)               | Noyé                 | [Fente Larinier](./fente_noyee.md)    | à définir par projeteur | Cloison aval                                                      |
|                                                             | Triangulaire           | Seuil mince (échancrure)                 | Noyé / dénoyé        | [Villemonte](./dever_triang.md) | 1,36                    | Lois d'ouvrages, lois déversoirs dénoyés, cloisons, cloisons aval |
|                                                             |                        | Seuil épais (échancrure)                 | Noyé / dénoyé        | [Bos](./dever_triang.md)       | 1,36                    | Lois d'ouvrages, lois déversoirs dénoyés, cloisons, cloisons aval |
|                                                             | Triangulaire tronqué   | Seuil mince (échancrure)                 | Noyé / dénoyé        | [Villemonte](./dever_triang_tronque.md) | 1,36                    | Lois d'ouvrages, lois déversoirs dénoyés, cloisons, cloisons aval |
| Orifice / vanne de fond supportant l'écoulement déversant à surface libre | Rectangulaire          | Seuil épais (échancrure) / orifice       | Noyé / dénoyé        | [Cemagref-D](./cem_88_d.md)    | 0,4                     | Lois ouvrages, cloisons, pré-barrages                             |
|                                                             | Rectangulaire          | Seuil épais (échancrure) / orifice       | Noyé / dénoyé        | [Cunge](./cunge_80.md)                        | 1                       | Lois ouvrages, pré-barrages                                       |
|                                                             | Rectangulaire          | Seuil épais (échancrure) / vanne de fond | Noyé / dénoyé        | [Cemagref-V](./cem_88_v.md)                   | 0,6                     | Lois ouvrages, cloison aval, pré-barrages                         |
| Vannes / orifices en charge                                 | Rectangulaire          | Vanne de fond                            | Dénoyé               | [Vanne de fond dénoyée (Bernoulli)](./vanne_denoyee.md) | 0,6                     | Lois d'ouvrages                                                   |
|                                                             | Rectangulaire          | Vanne de fond                            | Noyé                 | [Vanne de fond noyée](./vanne_noyee.md) | 0,8                     | Lois d'ouvrages                                                   |
|                                                             | Indéfini               | Orifice  | Dénoyé               | [Orifice dénoyé (Bernoulli)](./orifice_denoye.md)     | 0,7                     | Lois d'ouvrages                                                   |
|                                                             | Indéfini               | Orifice                                  | Noyé                 | [Orifice noyé (Bernoulli)](./orifice_noye.md)       | 0,7                     | Lois d'ouvrages, cloisons, cloison aval  

Table: Liste des équations d'ouvrages

## Seuil à crête mince ou crête épaisse ?

Extrait de CETMEF, 2005. Notice sur les déversoirs : synthèse des lois d’écoulement au droit des seuils et déversoirs. Centre d’Études Techniques Maritimes Et Fluviales, Compiègne.

> Le type de seuil est relatif à l’écoulement au droit de l’ouvrage. 
> 
> En effet, plus l’épaisseur de la crête du seuil est négligeable devant la hauteur d’eau amont au-dessus de celui-ci, plus le seuil paraît transparent vis-à-vis de l’écoulement et donc plus la crête du seuil paraît mince. 
>
> A l’inverse, plus la ligne d’eau amont se rapproche de la crête du seuil, plus la largeur du seuil paraît grande vis-à-vis de l’épaisseur de la lame d’eau qui y transite et donc plus la crête du seuil paraît épaisse. 
> Un déversoir en rivière appartient ainsi à l’une des trois catégories suivantes : 
> - seuil à crête mince
> - seuil à crête épaisse
> - seuil à crête non définie 
>
> Afin de déterminer le type de seuil étudié, les conditions suivantes doivent être vérifiées :
> - si $C < \frac{H_1}{2}$, alors le seuil est à crête mince ;
> - si $C > \frac{2H_1}{3}$, alors le seuil est à crête épaisse.