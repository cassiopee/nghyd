# Vanne noyée

![Schéma vanne noyée](vanne_noyee_schema.png)

*Extrait de Baume, J.-P., Belaud, G., Vion, P.-Y., 2013. Hydraulique pour le génie rural, Formations de Master, Mastère Spécialisé, Ingénieur agronome. UMR G-EAU, Irstea, SupAgro Montpellier.*

## Équation de la vanne noyée

\(Q = C'_d LW \sqrt{2g}\sqrt{h_{am} - h_{av}}\)

Le coefficient \(C'_d\) est de l'ordre de 0.8.
