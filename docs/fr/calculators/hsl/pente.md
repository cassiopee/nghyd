# Pente

## Définition

La définition de la pente utilisée dans tous les modules de Cassiopée est celle de la pente topographique:

> La pente topographique est la tangente de l'inclinaison entre deux points d'un terrain, donc de son angle vis-à-vis de l'horizontale. C'est donc le rapport entre la différence d'altitudes entre les deux points et la distance horizontale, cartographique, entre ces deux points. ([Source: Wikipédia](https://fr.wikipedia.org/wiki/Pente_(topographie)))

![Schéma en coupe longitudinale d'un tronçon rectiligne](pente.svg)

La pente (\(I\)) en m/m utilisée dans les modules de Cassiopée vaut alors:

$$ I = \Delta h / d = \tan(\alpha) $$

**Important&nbsp;:**

Tous les modules de calcul considère une pente descendante positive à l'exception du module "Impact de jet" où une pente positive sera considérée comme montante et inversement. Pour inverser la pente dans une suite de calcul de modules liés, utiliser le module "fonction affine" avec \(a= -1\) et \(b=0\).

## L'outil "Pente"

Cet outil permet de calculer la valeur manquante des quatre grandeurs&nbsp;:

- la cote amont (\(Z_1\)) en m;
- la cote aval (\(Z_2\)) en m;
- la longueur (\(d\)) en m;
- la pente (\(I\)) en m/m, avec \(I = \frac{(Z_1 - Z_2)}{d}\).
