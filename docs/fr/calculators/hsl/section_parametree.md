# Section paramétrée

Ce module calcule les grandeurs hydrauliques associées à&nbsp;:

- une section ayant une forme géométrique définie ([Voir les types de section gérées par Cassiopée](types_sections.md))
- un tirant d'eau \(y\) en m
- un débit \(Q\) en m<sup>3</sup>/s
- une pente du fond \(I_f\) en m/m
- une rugosité exprimée avec le coefficient de Strickler \(K\) en m<sup>1/3</sup>/s

Les grandeurs hydrauliques calculées sont&nbsp;:

- La largeur au miroir (m)
- Le périmètre mouillé (m)
- La surface hydraulique (m<sup>2</sup>)
- Le rayon hydraulique (m)
- La vitesse moyenne (m/s)
- La charge spécifique (m)
- La perte de charge (m)
- La variation linéaire de l'énergie spécifique (m/m)
- Le tirant d'eau normal (m)
- Le froude
- Le tirant d'eau critique (m)
- La charge critique (m)
- Le tirant d'eau correspondant (m)
- L'impulsion (N)
- Le tirant d'eau conjugué
- La force tractrice (Pa)

## Hauteur de berge, débordement et écoulement en charge

Les sections sont toutes pourvues d'une hauteur de berge qui intervient à trois niveaux dans les outils de calcul d'hydraulique à surface libre:

- Elle permet de définir si le tirant d'eau calculé déborde de la section
- Au delà de cette hauteur de berge les calculs hydrauliques simulent un écoulement entre deux murs verticaux. Par exemple, on pourra ainsi modéliser une conduite semi-circulaire en définissant une hauteur de berge égale au rayon de la conduite.
- Pour une conduite circulaire, une hauteur de berge supérieure ou égale au diamètre de la conduite permet de modéliser une conduite fermée. Si la hauteur d'eau dépasse le diamètre de la conduite, les calculs hydrauliques simulent un écoulement en charge à l'aide de la technique de la fente de Preissmann.

## Largeur au miroir, surface et périmètre mouillé

[Voir la page dédiée pour les paramètres propres à chaque type de section](types_sections.md)

### Section rectangulaire

- Largeur au miroir : \(B=L\)
- Surface : \(S=L.y\)
- Périmètre : \(P=L+2y\)

### Section trapézoïdale

- Largeur au miroir : \(B=L+2..m.y\)
- Surface : \(S=(L+m.y)y\)
- Périmètre : \(P=L+2y\sqrt{1+m^2}\)

### Section circulaire

- Largeur au miroir : \(B=D\sin\theta\)
- Surface : \(S=\frac{D^2}{4} \left(\theta - \sin\theta.\cos\theta \right)\)
- Périmètre : \(P=D.\theta\)

### Section parabolique

- Largeur au miroir : \(B=\frac{B_b}{y_b^k}y^k\)
- Surface : \(S=\frac{B_b}{y_b^k}\frac{y^{k+1}}{k+1}\)
- Périmètre : \(P=2\sum _{i=1}^{n}\sqrt{\frac{1}{n^2}+\frac{1}{4}\left( B\left(\frac{i.y}{n}\right)-B\left(\frac{(i-1).y}{n}\right) \right)^2}\) pour \(n\) suffisamment grand

## Le rayon hydraulique (m)

$$R = S / P$$

## La vitesse moyenne (m/s)

$$U = Q /S$$

## La charge spécifique (m)##

$$H(y) = y + \frac{U^2}{2g}$$

## La perte de charge (m/m)##

Cassiopée utilise la formule de Manning Strickler :

$$J=\frac{U^2}{K^{2}R^{4/3}}=\frac{Q^2}{S^2K^{2}R^{4/3}}$$

## La variation linéaire de l'énergie spécifique (m/m)

$$\Delta E_s = I_f - J$$

## Le tirant d'eau normal (m)

[Voir le calcul du régime uniforme.](regime_uniforme.md)

## Le Froude

Le nombre de Froude exprime le rapport entre la vitesse moyenne du fluide et la célérité des ondes de surface \(c\).

$$ c = \sqrt{\frac{gS}{B}}$$

$$ Fr = \frac{U}{c} = \sqrt{\frac{Q^2B}{gS^3}}$$

## Le tirant d'eau critique (m)

La hauteur critique est atteinte quand la vitesse moyenne de déplacement du fluide est égale à la célérité des ondes à la surface de l'eau.

La hauteur critique est donc atteinte quand le nombre de Froude \(Fr=1\).

Pour une section quelconque, on calcule la hauteur critique \(y_c\) en résolvant \(f(y_c)=Fr^2-1=0\)

On utilise la méthode de Newton en posant \(y_{k+1} = y_k - \frac{f(y_k)}{f'(y_k)}\) avec :
- \(f(y_k) = \frac{Q^2 B}{g S^3} - 1\)
- \(f'(y_k) = \frac{Q^2}{g} \frac{B'.S - 3 B S'}{S^4}\)

## La charge critique (m)

C'est la charge calculée pour un tirant d'eau égal au tirant d'eau critique \(H_c = H(y_c)\).

## Le tirant d'eau correspondant (m)

Pour un tirant d'eau fluvial (respectivement torrentiel) \(y\), le tirant correspondant est le tirant d'eau torrentiel (respectivement fluvial) pour lequel \(H(y) = H(y_{cor})\).

## L'impulsion hydraulique (N)

L'impulsion \(I\) est la somme de la quantité de mouvement et de la résultante de la force de pression dans une section :

$$I=\rho Q U + \rho g S y_g$$

Avec :

- \(\rho\) : la masse volumique de l'eau (kg/m<sup>3</sup>)
- \(y_g\) : la distance du centre de gravité de la section à la surface libre (m)

La distance du centre de gravité de la section à la surface libre \(y_g\) peut se retrouver à partir de la formule :

$$S.y_g = \int_{0}^{y} (y-z)B(z)dz$$

Avec \(y\) le tirant d'eau et \(B(z)\) la largeur au miroir pour un tirant d'eau \(z\)

Les formules de \(S.y_g\) pour les différentes formes de section sont :

- section rectangulaire : \(S.y_g = \frac{L.y^2}{2}\)
- section trapézoïdale : \(S.y_g = \left (\frac{L}{2} + \frac{m.y}{3} \right )y^2\)
- section circulaire : \(S.y_g = \frac{D^3}{8}\left (\sin\theta - \frac{\sin^3\theta}{3} - \theta \cos\theta \right )\)
- section parabolique : \(S.y_g=\frac{B_b.y^{k+2}}{y_b^k(k+1)(k+2)}\)

## Le tirant d'eau conjugué (m)

Pour un tirant d'eau fluvial (respectivement torrentiel) \(y\), le tirant conjugué est le tirant d'eau torrentiel (respectivement fluvial) pour lequel \(I(y) = I(y_{con})\).

## La force tractrice (Pa)

$$ \tau_0 = \rho g R J $$
