# Courbe de remous

Le calcul de la courbe de remous fait intervenir l'équation différentielle suivante&nbsp;:


$$\frac{dy}{dx}=\frac{I_f - J(h)}{1-F^2(h)}$$

où \(I_f\) est la pente d'un canal, \(J\) la formule nous donnant la perte de charge locale (dépendant la hauteur d'eau), \(y\) désigne ici la hauteur d'eau.

On donne ainsi, pour un canal rectangulaire de largeur \(b\) et [coefficient de Strickler](strickler.md) \(K\):

$$J=\frac{Q^2 (b+2y)^{4/3}}{K^2 b^{10/3}y^{10/3}}$$

et

$$F^2=\frac{Q^2}{gb^2y^3}$$

L'intégration de l'équation pourra se faire par l'une des méthodes suivantes&nbsp;: [Runge-Kutta 4](../../methodes_numeriques/rk4.md), [Euler explicite](../../methodes_numeriques/euler_explicite.md), [intégration de trapèzes](../../methodes_numeriques/integration_trapezes.md).

En fonction du régime d'écoulement, le calcul pourra s'effectuer&nbsp;:

 * de l'aval vers l'amont pour le régime fluvial avec définition d'une condition limite aval
 * de l'amont vers l'aval pour le régime torrentiel avec définition d'une condition limite amont

Si on prend l'exemple d'un canal rectangulaire, [l'exemple de code scilab proposé pour la résolution d'équation différentielle ordinaire](../../methodes_numeriques/euler_explicite.md) est modifié comme suit&nbsp;:

```scilab
  b=0.3;
  K=50;
  If=0.005;
  Q=0.01;
  function z=DQ(y);
    z=Q-K*(b*y)^(5/3)/(b+2*y)^(2/3)*sqrt(If);
  endfunction
  yn=fsolve(0.5,DQ);
  tmax=0;
  t0=10;
  dt=-0.5;
  function z=f(y,t);
    z=(If-Q^2*(b+2*y)^(4/3)/(K^2*(b*y)^(10/3)))/(1-Q^2/(9.81*b^2*y^3));
  endfunction
  y0=0.12;
```

ce qui nous donne la profondeur normale, et le tracé de la ligne d'eau. En fonction de la méthode numérique utilisée, on peut avoir de grosses erreurs dans le cas d'une courbe de remous de type F2 (condition aval sous la hauteur normale), car les pentes de ligne d'eau sont beaucoup plus fortes, et donc beaucoup plus sujettes aux erreurs liées à l'interpolation linéaire. On en déduit donc que d'une part le choix de la méthode de résolution est important, d'autre part qu'il est essentiel d'avoir un regard critique sur les solutions (avec une interprétation des processus qu'on cherche à modéliser).
