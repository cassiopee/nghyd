# Formule de Manning-Strickler

## Définition

La formule de Manning-Strickler s’écrit de la façon suivante&nbsp;:

$$V = K_s R_h^{2/3} i^{1/2}$$

avec&nbsp;:

- \(V\) la vitesse moyenne de la section transversale en m/s
- \(K_s\) le coefficient de Strickler
- \(R_h\) le rayon hydraulique en m
- \(i\) la pente en m/m

Le coefficient de Strickler \(K_s\) varie de 20 (pierre brute et surface rugueuse) à 80 (béton lisse et fonte).

Le coefficient de Manning \(n\) est obtenu par&nbsp;:

$$n = \frac{1}{K_s}$$

## Tableau de Chow (1959)

| Type de chenal et description            | \(K_S\) min. | \(K_S\)  normal  | \(K_S\) max.   |
|------------------------------------------|----------:|----------:|----------:|
| **1\. Lits mineurs**                                                                                                                                             |         |                          |                             |
| a\. propres, rectilignes, bien remplis d'eau, sans failles ni mouilles profondes                                                                             | 30      | 33                       | 40                          |
| b\. comme ci\-dessus, mais plus de pierres et d'herbiers                                                                                                     | 25      | 29                       | 33                          |
| c\. propres, sinueux, quelques mouilles et hauts\-fonds                                                                                                      | 22      | 25                       | 30                          |
| d\. comme ci\-dessus, mais quelques herbiers et pierres                                                                                                      | 20      | 22                       | 29                          |
| e\. comme ci\-dessus, hauteurs d'eau plus faibles, avec des pentes et tronçons plus inefficaces                                                              | 18      | 21                       | 25                          |
| f\. identiques à d avec plus de pierres                                                                                                                      | 17      | 20                       | 22                          |
| g\. tronçons mollassons, herbeux, mouilles profondes                                                                                                         | 13      | 14                       | 20                          |
| h\. tronçons très herbeux, mouilles profondes, ou lits moyens encombrés de bois et sous\-bois                                                                | 7       | 10                       | 13                          |
| **2\. Cours d'eau de montagne, pas de végétation dans le lit, berges généralement abruptes, arbres et broussailles le long des berges immergées en hautes eaux** |
| a\. fond : graviers, pavés, et quelques rochers                                                                                                              | 20      | 25                       | 33                          |
| b\. fond : pavés avec de gros rochers                                                                                                                        | 14      | 20                       | 25                          |
| **3\. Lits majeurs**                                                                                                                                             |
| *a\. Prairie, sans broussailles*                                                                                                                               |         |                          |                             |
| 1\. herbe rase                                                                                                                                               | 29      | 33                       | 40                          |
| 2\. herbe haute                                                                                                                                              | 20      | 29                       | 33                          |
| *b\. Surfaces cultivées*                                                                                                                                       |         |                          |                             |
| 1\. aucune culture                                                                                                                                           | 25      | 33                       | 50                          |
| 2\. cultures matures en rang                                                                                                                                 | 22      | 29                       | 40                          |
| 3\. cultures matures en champ                                                                                                                                | 20      | 25                       | 33                          |
| *c\. Broussailles*                                                                                                                                             |         |                          |                             |
| 1\. broussailles dispersées, herbes denses                                                                                                                   | 14      | 20                       | 29                          |
| 2\. broussailles aérées et arbres, en hiver                                                                                                                  | 17      | 20                       | 29                          |
| 3\. broussailles aérées et arbres, en été                                                                                                                    | 13      | 17                       | 25                          |
| 4\. broussailles moyennes à denses, en hiver                                                                                                                 | 9       | 14                       | 22                          |
| 5\. broussailles moyennes à denses, en été                                                                                                                   | 6       | 10                       | 14                          |
| *d\. Arbres*                                                                                                                                                   |         |                          |                             |
| 1\. saules denses, en été, alignés                                                                                                                           | 5       | 7                        | 9                           |
| 2\. terres défrichées avec des souches d'arbres, pas de pousses                                                                                              | 20      | 25                       | 33                          |
| 3\. comme ci\-dessus, mais avec fort développement de pousses                                                                                                | 13      | 17                       | 20                          |
| 4\. dense présence de bois, quelques arbres couchés,un peu de sous\-bois, hautes eaux sous les branches                                                      | 22      | 21                       | 20                          |
| 5\. comme le 4\., les hautes eaux atteignant les branches                                                                                                    | 6       | 8                        | 10                          |
| **4\. Chenaux creusés ou dragués**                                                                                                                               |
| *a\. En terre, droits, et uniformes*                                                                                                                           |         |                          |                             |
| 1\. propres, récemment achevés                                                                                                                               | 50      | 56                       | 63                          |
| 2\. propres, après exposition aux intempéries                                                                                                                | 40      | 45                       | 56                          |
| 3\. graviers, tronçon uniforme, propre                                                                                                                       | 33      | 40                       | 45                          |
| 4\. herbe rase, peu d'herbiers                                                                                                                               | 30      | 37                       | 45                          |
| *b\. En terre, sinueux, et ralentis*                                                                                                                           |         |                          |                             |
| 1\. pas de végétation                                                                                                                                        | 33      | 40                       | 43                          |
| 2\. herbe, quelques herbiers                                                                                                                                 | 30      | 33                       | 40                          |
| 3\. herbiers denses ou plantes aquatiques dans les chenaux profonds                                                                                          | 25      | 29                       | 33                          |
| 4\. fond en terre et bords en déblais                                                                                                                        | 29      | 33                       | 36                          |
| 5\. fond pierreux et berges herbeuses                                                                                                                        | 25      | 29                       | 40                          |
| 6\. fond en galets et bords propres                                                                                                                          | 20      | 25                       | 33                          |
| *c\. Excavé par dragline ou dragué*                                                                                                                            |         |                          |                             |
| 1\. pas de végétation                                                                                                                                        | 30      | 36                       | 40                          |
| 2\. broussailles aérées sur les berges                                                                                                                       | 17      | 20                       | 29                          |
| *d\. Tranchées dans la roche*                                                                                                                                  |         |                          |                             |
| 1\. lisses et uniformes                                                                                                                                      | 25      | 29                       | 40                          |
| 2\. déchiquetées et irrégulières                                                                                                                             | 20      | 25                       | 29                          |
| *e\. Chenaux pas entretenus, herbiers et broussailles non coupées*                                                                                             |         |                          |                             |
| 1\. herbiers denses, sur toute la hauteur d'écoulement                                                                                                       | 8       | 13                       | 20                          |
| 2\. fond propre, broussailles sur les bords                                                                                                                  | 13      | 20                       | 25                          |
| 3\. comme ci\-dessus, plus hautes eaux                                                                                                                       | 9       | 14                       | 22                          |
| 4\. broussailles denses, hautes eaux                                                                                                                         | 7       | 10                       | 13                          |
| **5\. Chenaux rectifiés ou construits**                                                                                                                          |
| *a\. Ciment*                                                                                                                                                   |         |                          |                             |
| 1\. surface nette                                                                                                                                            | 77      | 91                       | 100                         |
| 2\. mortier                                                                                                                                                  | 67      | 77                       | 91                          |
| *b\. Bois*                                                                                                                                                     |         |                          |                             |
| 1\. raboté, non traité                                                                                                                                       | 71      | 83                       | 100                         |
| 2\. raboté, créosoté                                                                                                                                         | 67      | 83                       | 91                          |
| 3\. non raboté                                                                                                                                               | 67      | 77                       | 91                          |
| 4\. planche avec tasseaux                                                                                                                                    | 56      | 67                       | 83                          |
| 5\. recouvert de textile imperméabilisant                                                                                                                    | 59      | 71                       | 100                         |
| *c\. Béton*                                                                                                                                                    |         |                          |                             |
| 1\. finition à la truelle                                                                                                                                    | 67      | 77                       | 91                          |
| 2\. finition à l'aplanissoir                                                                                                                                 | 63      | 67                       | 77                          |
| 3\. avec finition, avec du gravier sur le fond                                                                                                               | 50      | 59                       | 67                          |
| 4\. sans finition                                                                                                                                            | 50      | 59                       | 71                          |
| 5\. gunite, bonne section                                                                                                                                    | 43      | 53                       | 63                          |
| 6\. gunite, section ondulée                                                                                                                                  | 40      | 45                       | 56                          |
| 7\. sur de la bonne roche excavée                                                                                                                            |         | 50                       | 59                          |
| 8\. sur de la roche excavée irrégulière                                                                                                                      |         | 37                       | 45                          |
| *d\. Fond en béton, finition à l'aplanissoir, avec bords en :*                                                                                                 |         |                          |                             |
| 1\. pierre de taille liée au mortier                                                                                                                         | 50      | 59                       | 67                          |
| 2\. pierre brute liée au mortier                                                                                                                             | 42      | 50                       | 59                          |
| 3\. maçonnerie de ciment moellons, enduit                                                                                                                    | 42      | 50                       | 63                          |
| 4\. maçonnerie de ciment moellons                                                                                                                            | 33      | 40                       | 50                          |
| 5\. moellons non liés ou enrochements                                                                                                                        | 29      | 33                       | 50                          |
| *e\. Fond en graviers, avec bords en :*                                                                                                                        |         |                          |                             |
| 1\. béton coffré                                                                                                                                             | 40      | 50                       | 59                          |
| 2\. mortier de pierre brute                                                                                                                                  | 38      | 43                       | 50                          |
| 3\. déblais non liés ou enrochements                                                                                                                         | 28      | 30                       | 43                          |
| *f\. Brique*                                                                                                                                                   |         |                          |                             |
| 1\. brique vernissée                                                                                                                                         | 67      | 77                       | 91                          |
| 2\. liée au mortier de ciment                                                                                                                                | 56      | 67                       | 83                          |
| *g\. Maçonnerie*                                                                                                                                               |         |                          |                             |
| 1\. moellons cimentés                                                                                                                                        | 33      | 40                       | 59                          |
| 2\. moellons non liés                                                                                                                                        | 29      | 31                       | 43                          |
| *h\. Pierre de taille / dallage en pierre*                                                                                                                     | 59      | 67                       | 77                          |
| *i\. Asphalte*                                                                                                                                                 |         |                          |                             |
| 1\. lisse                                                                                                                                                    |         | 77                       | 77                          |
| 2\. rugueux                                                                                                                                                  |         | 63                       | 63                          |
| *j\. Revêtement végétal*                                                                                                                                       | 2       |                          | 33                          |

Table: Tableau de Chow (1959)
