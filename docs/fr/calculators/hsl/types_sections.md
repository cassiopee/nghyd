# Types de sections

## Section rectangulaire

![Section rectangulaire](section_rectangulaire.png)

La section rectangulaire se caractérise par les paramètres suivants&nbsp;:

 * la largeur au fond \(L\) (en m)

## Section circulaire

![Section circulaire](section_circulaire.png)

La section circulaire se caractérise par les paramètres suivants&nbsp;:

 * le diamètre de la conduite \(D\) (en m)
 * l'angle \(\theta\) entre le fond de la conduite et le point de contact entre la surface de l'eau la conduite (en Rad)

 \(\theta = \arccos\left(1-\frac{y}{D/2}\right)\)

 \(\theta' = \frac{2}{D\sqrt{1-\left(1-\frac{2y}{D}\right)^2}}\)

## Section trapézoïdale

![Section trapezoidale](section_trapezoidale.png)

La section trapézoïdale se caractérise par les paramètres suivants&nbsp;:

 * la largeur au fond \(L\) (en m)
 * le fruit des berges (inclinaison par rapport à la verticale&nbsp;: élargissement entre le haut et le bas du talus divisé par la profondeur) \(m\) (en m/m)

## Section parabolique

La section parabolique se caractérise par une largeur au miroir pouvant s'exprimer sous la forme&nbsp;:

\(B = \Lambda.y^k\).

Avec \(k\)&nbsp;: coefficient compris entre 0 et 1. \(k=0.5\) correspond à la forme parabolique vraie.

On peut calculer \(\Lambda\) en fournissant&nbsp;:

 * \(y_b\)&nbsp;: la hauteur de berge (en m)
 * \(B_b\)&nbsp;: la largeur au niveau des berges (en m)

On a alors&nbsp;: \(\Lambda = \frac{B_b}{y_b^k}\)
