# Cotes amont / aval d'un bief

Ce module basé sur les équations du [module de calcul des courbes de remous](courbe_remous.md) permet de calculer&nbsp;:

- La cote de l'eau à l'amont d'un bief d'une courbe de remous fluviale&nbsp;
- La cote de l'eau à l'aval d'un bief d'une courbe de remous torrentielle&nbsp;
- Le débit permettant de relié les cotes de l'eau amont et aval d'une courbe de remous fluviale ou torrentielle.

Le régime choisi sur le type de ligne d'eau détermine si le calcul s'effectue de l'aval vers l'amont (régime fluvial) et de l'amont vers l'aval (régime torrentiel).

Ce module de calcul est particulièrement utile pour calculer la ligne d'eau d'un enchaînement de ouvrages hydrauliques ou de biefs (Voir l'exemple type "Débit d'un chenal avec ouvrages").

