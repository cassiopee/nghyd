# Le régime uniforme


Le régime uniforme se caractérise par une hauteur d'eau appelée hauteur normale. La hauteur normale est atteinte quand la ligne d'eau est parallèle au fond, la charge est alors elle-même parallèle à la ligne d'eau et donc la perte de charge est égale à la pente du fond&nbsp;:
\(I_f = J\)

Avec&nbsp;:

- \(I_f\)&nbsp;: la pente du fond en m/m
- \(J\)&nbsp;: la perte de charge en m/m

La perte de charge {J} est ici calculée avec la [formule de Manning-Strickler](strickler.md)&nbsp;:

$$J=\frac{U^2}{K^{2}R^{4/3}}=\frac{Q^2}{S^2K^{2}R^{4/3}}$$

Avec&nbsp;:

- \(K\)&nbsp;: le coefficient de Strickler en m<sup>1/3</sup>/s

En régime uniforme, on obtient la formule&nbsp;:

$$Q=KR^{2/3}S\sqrt{I_f}$$

A partir de laquelle, on peut calculer analytiquement le débit \(Q\), la pente \(I_f\) et le Strickler \(K\) analytiquement.

Pour calculer la hauteur normale \(h_n\) , on peut résoudre \(f(h_n)=Q-KR^{2/3}S\sqrt{I_f}=0\)

en utilisant la méthode de Newton&nbsp;:

$$h_{k+1} = h_k - \frac{f(h_k)}{f'(h_k)}$$

 avec&nbsp;:

- \(f(h_k) = Q-KR^{2/3}S\sqrt{I_f}\)
- \(f'(h_k) = -K \sqrt{I_f}(\frac{2}{3}R'R^{-1/3}S+R^{2/3}S')\)

Pour calculer les paramètres géométriques de la section, le module de calcul utilise l'équation de calcul du débit et résout le problème par dichotomie.
