# Formule de Lechapt et Calmon

La formule de Lechapt et Calmon est basée sur des ajustements de la formule de  [Cyril Frank Colebrook](http://fr.wikipedia.org/wiki/Cyril_Frank_Colebrook)&nbsp;:

$$J_{lin}=\frac{l_T}{1000}L.Q^M.D^{-N}$$

Avec&nbsp;:

- \(J_{lin}\)&nbsp;: la perte de charge linéaire en m&nbsp;;
- \(l_T\)&nbsp;: la longueur du tuyau en m&nbsp;;
- \(Q\)&nbsp;: le débit en L/s&nbsp;;
- \(D\)&nbsp;: le diamètre de la conduite en m&nbsp;;
- \(L\), \(M\) et \(N\) des coefficients dépendants de la rugosité {&#x3F5;}.

L'erreur commise par rapport à la formule de Colebrook est inférieure à 3&nbsp;% pour des vitesses comprises entre 0,4 et 2 m/s.

Le tableau de correspondance des coefficients est le suivant&nbsp;:


| Matériau                                       | &#x3F5; (mm) | \(L\) | \(M\) | \(N\) |
|------------------------------------------------|-------------:|----:|----:|-----:|
| Fonte ou acier non revêtus - Béton grossier (eau corrosive) | 2 | 1.863 | 2 | 5.33 |
| Fonte ou acier non revêtus - Béton grossier (eau peu corrosive) | 1 | 1.601 | 1.975 | 5.25 |
| Fonte ou acier revêtement ciment | 0.5 | 1.40 | 1.96 | 5.19 |
| Fonte ou acier revêtement bitume - béton centrifugé | 0.25 | 1.16 | 1.93 | 5.11 |
| Acier laminé - béton lisse | 0.1 | 1.10 | 1.89 | 5.01 |
| Fonte ou acier revêtement centrifugé | 0.05 | 1.049 | 1.86 | 4.93 |
| PVC - polyéthylène | 0.025 | 1.01 | 1.84 | 4.88 |
| Tuyau hydrauliquement lisse - 0.05 &le; D &le; 0.2 | 0.00 | 0.916 | 1.78 | 4.78 |
| Tuyau hydrauliquement lisse - 0.25 &le; D &le; 1 | 0.00 | 0.971 | 1.81 | 4.81 |

Table&nbsp;: Matériaux et coefficients utilisés dans la formule de Lechapt et Calmon
