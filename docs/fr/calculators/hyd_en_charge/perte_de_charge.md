# Perte de charge

Ce module permet de calculer les pertes de charge dans une conduite circulaire à partir des lois suivantes donnant les pertes de charge linéaires :

- [Lechapt et Calmon](lechapt-calmon.md)
- [Strickler](strickler.md)

Il permet le calcul de la valeur d'une des grandeurs suivantes&nbsp;:

- Débit (m<sup>3</sup>/s)
- Diamètre du tuyau (m)
- Perte de charge totale (m)
- Longueur du tuyau (m)
- Coefficient de perte de charge locale (singulière) (m)

La perte de charge totale est la somme des pertes de charges linéaires \(J_{lin}\) (données par la loi utilisée) et locales \(J_{loc}\) dépendantes du coefficient ci-dessus.

## Perte de charge locale

$$ J_{loc} = K_{loc} \frac{V^2}{2g}$$

Avec&nbsp;:

- \(K_{loc}\)&nbsp;: le coefficient de perte de charge locale
- \(V\)&nbsp;: la vitesse de l'eau dans la conduite (\(V = 4 Q / \pi / D^2\))

## Coefficient de perte de charge linéaire

$$ K_{lin} = \frac{2g J_{lin}}{V^2} $$

## Coefficient de perte de charge de Darcy

$$ f_D = \frac{2g J D}{l_T V^2} $$
