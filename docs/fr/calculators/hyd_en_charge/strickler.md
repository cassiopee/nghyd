# Formule de Strickler

Cette loi de perte de charge linéaire est paramétrée par le coefficient de Stricker \(K_S\).

Les autres paramètres sont ceux communs à tous les calculs de perte de charge&nbsp;:

- \(Q\) : débit (m<sup>3</sup>/s)
- \(D\) : diamètre du tuyau (m)
- \(l_T\) : Longueur du tuyau (m)

$$J_L=\frac{l_T.Q^2}{(K_S.\pi.D^2/4)^2.(D/4)^{4/3}}$$
