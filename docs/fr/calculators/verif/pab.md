# Vérification des critères de franchissement&nbsp;: Passes à bassins

Lors de la vérification d'une passe à bassins, les bassins et les cloisons sont testés séquentiellement, ainsi que tous les ouvrages de chaque cloison. Si au moins un bassin est infranchissable, alors la passe est infranchissable.

Les critères diffèrent selon que l'ouvrage considéré donne lieu à un jet plongeant ou à un jet de surface. Si une cloison possède plusieurs ouvrages, elle sera considérée comme franchissable si au moins un ouvrage est franchissable.

Les orifices sont considérés comme infranchissables.

Pour certaines espèces, les valeurs de certains critères ne sont pas précisément connues ou sont susceptibles d'évoluer quelque peu en fonction des retours d'expérience futurs.

## Critères

### Jet plongeant

Certaines espèces ne supportent pas les jets plongeants. Pour ces espèces, chaque ouvrage donnant lieu à un jet plongeant est marqué comme infranchissable.

### Chute maximale, en m

La chute du bassin doit être inférieure à la chute maximale.

### Largeur minimale de fente ou échancrure latérale, en m (jet de surface seulement)

La largeur de la fente ou échancrure doit être supérieure à la largeur minimale.

### Surface minimale de l'orifice, en m²

La surface de l'orifice doit être supérieure à la largeur minimale de fente ou échancrure latérale (ci-dessus) au carré.

### Charge minimale sur l'échancrure, en m

La charge sur l'échancrure doit être supérieure à la charge minimale.

### Profondeur minimale de bassin, en m

La profondeur du bassin doit être supérieure à la profondeur minimale. Dans le cas d'un jet plongeant, la profondeur doit être supérieure au double de la chute du bassin.

### Longueur minimale de bassin, en m

La longueur du bassin doit être supérieure à la longueur minimale.

### Puissance dissipée maximale préconisée et maximale limite, en W/m³

La puissance dissipée maximale dans le bassin doit être inférieure à la puissance dissipée maximale limite. Si elle est supérieure à la puissance dissipée maximale préconisée, un avertissement est émis.

**Important&nbsp;:** la vérification de la puissance dissipée maximale est **désactivée** dans la version actuelle de Cassiopée

## Valeurs des critères pour les groupes d'espèces prédéfinis

D'après _"Informations sur la Continuité Écologique - ICE, Onema 2014"_, et et OFB com. pers.

### Valeurs communes pour jet de surface et jet plongeant

| Groupe ICE | Espèces | Charge minimale sur l'échancrure (m) | Puissance dissipée maximale préconisée (W/m³) | Puissance dissipée maximale limite (W/m³) |
|------------|---------|---------|---------|---------|
| 1 | Saumon atlantique (Salmo salar)<br>Truite de mer ou de rivière [50-100] (Salmo trutta) | 0.3 | 200 | 250 |
| 2 | Mulets (Chelon labrosus, Liza ramada) | 0.2 | 200 | 250 |
| 3a | Grande alose (Alosa alosa) | 0.4 | 150 | 200 |
| 3b | Alose feinte (Alosa fallax fallax) | 0.4 | 150 | 200 |
| 3c | Lamproie marine (Petromyzon marinus) | 0.15 | 200 | 250 |
| 4a | Truite de rivière ou truite de mer [25-55] (Salmo trutta) | 0.2 | 200 | 250 |
| 4b | Truite de rivière [15-30] (Salmo trutta) |0.2 | 200 | 250 |
| 5 | Aspe (Aspius aspius)<br>Brochet (Esox lucius) | 0.2 | 150 | 200 |
| 6 | Ombre commun (Thymallus thymallus) | 0.2 | 200 | 250 |
| 7a | Barbeau fluviatile (Barbus barbus)<br>Chevaine (Squalius cephalus)<br>Hotu (Chondrostoma nasus) | 0.2 | 150 | 200 |
| 7b | Lamproie fluviatile (Lampetra fluviatilis) | 0.15 | 130 | 150 |
| 8a | Carpe commune (Cyprinus carpio) | 0.2 | - | - |
| 8b | Brème commune (Abramis brama)<br>Sandre (Sander lucioperca) | 0.2 | 150 | 200 |
| 8c | Brème bordelière (Blicca bjoerkna)<br>Ide melanote (Leuciscus idus)<br>Lotte de rivière (Lota lota)<br>Perche (Perca fluviatilis)<br>Tanche (Tinca tinca) | 0.2 | 130 | 150 |
| 8d | Vandoises (Leuciscus sp hors Idus) | 0.2 | 150 | 200 |
| 9a | Ablette commune (Alburnus alburnus)<br>Ablette sprirlin (Alburnoides bipunctatus)<br>Barbeau méridional (Barbus meridionalis)<br>Blageon (Telestes souffia)<br>Carassin commun (Carassius carassius)<br>Carassin argenté (Carassius gibelio)<br>Gardon (Rutilus rutilus)<br>Rotengle (Scardinius erythrophthalmus)<br>Toxostome (Parachondrostoma toxostoma) | 0.2 | 130 | 150 |
| 9b | Apron (Zingel asper)<br>Chabots (Cottus sp)<br>Goujons (Gobio sp)<br>Grémille (Gymnocephalus cernuus)<br>Lamproie de Planer (Lampetra planeri)<br>Loche franche ( Barbatula barbatula)<br>Loche de rivière (Cobitis taenia) | 0.2 | 130 | 150 |
| 10 | Able de Heckel (Leucaspius delineatus)<br>Bouvière (Rhodeus amarus)<br>Epinoche (Gasterosteus gymnurus)<br>Epinochette (Pungitius laevis)<br>Vairons (Phoxinus sp) | 0.2 | 100 | 150 |

Table: Liste des valeurs prédéfinies pour les critères de franchissement d'une passe à bassins

### Jet de surface

| Groupe ICE | Espèces | Chute maximale (m) | Largeur minimale de fente ou échancrure latérale (m) | Profondeur minimale de bassin (m) | Longueur minimale de bassin (m) |
|------------|---------|---------|---------|--------------|----------|
| 1 | Saumon atlantique (Salmo salar)<br>Truite de mer ou de rivière [50-100] (Salmo trutta) | 0.35 | 0.3 | 1 | 2.5 |
| 2 | Mulets (Chelon labrosus, Liza ramada) | 0.35 | 0.2 | 1 | 1.75 |
| 3a | Grande alose (Alosa alosa) | 0.3 | 0.4 | 1 | 3.5 |
| 3b | Alose feinte (Alosa fallax fallax) | 0.3 |  0.4 | 1 | 3.5 |
| 3c | Lamproie marine (Petromyzon marinus) | 0.3 |  0.15 | 1 | 1.25 |
| 4a | Truite de rivière ou truite de mer [25-55] (Salmo trutta) | 0.35 | 0.2 | 1 | 1.75 |
| 4b | Truite de rivière [15-30] (Salmo trutta) | 0.3 | 0.15 | 0.75 | 1.75 |
| 5 | Aspe (Aspius aspius)<br>Brochet (Esox lucius) | 0.3  | 0.3 | 0.75 | 2.5 |
| 6 | Ombre commun (Thymallus thymallus) | 0.3 | 0.2 | 0.75 | 1.75 |
| 7a | Barbeau fluviatile (Barbus barbus)<br>Chevaine (Squalius cephalus)<br>Hotu (Chondrostoma nasus) | 0.3 | 0.25 | 0.75 | 2 |
| 7b | Lamproie fluviatile (Lampetra fluviatilis) | 0.3 | 0.15 | 0.75 | 1.25 |
| 8a | Carpe commune (Cyprinus carpio) | 0.25 | 0.3 | 0.75 | 2.5 |
| 8b | Brème commune (Abramis brama)<br>Sandre (Sander lucioperca) | 0.25 | 0.3 | 0.75 | 2.5 |
| 8c | Brème bordelière (Blicca bjoerkna)<br>Ide melanote (Leuciscus idus)<br>Lotte de rivière (Lota lota)<br>Perche (Perca fluviatilis)<br>Tanche (Tinca tinca) | 0.25 | 0.3 | 0.75 | 2.5 |
| 8d | Vandoises (Leuciscus sp hors Idus) | 0.25 | 0.3 | 0.75 | 2.5 |
| 9a | Ablette commune (Alburnus alburnus)<br>Ablette sprirlin (Alburnoides bipunctatus)<br>Barbeau méridional (Barbus meridionalis)<br>Blageon (Telestes souffia)<br>Carassin commun (Carassius carassius)<br>Carassin argenté (Carassius gibelio)<br>Gardon (Rutilus rutilus)<br>Rotengle (Scardinius erythrophthalmus)<br>Toxostome (Parachondrostoma toxostoma) | 0.25 | 0.25 | 0.75 | 2 |
| 9b | Apron (Zingel asper)<br>Chabots (Cottus sp)<br>Goujons (Gobio sp)<br>Grémille (Gymnocephalus cernuus)<br>Lamproie de Planer (Lampetra planeri)<br>Loche franche ( Barbatula barbatula)<br>Loche de rivière (Cobitis taenia) | 0.2 | 0.15 | 0.5 | 1.25 |
| 10 | Able de Heckel (Leucaspius delineatus)<br>Bouvière (Rhodeus amarus)<br>Epinoche (Gasterosteus gymnurus)<br>Epinochette (Pungitius laevis)<br>Vairons (Phoxinus sp) | 0.2 | 0.15 | 0.5 | 1.25 |

Table: Liste des valeurs prédéfinies pour les critères de franchissement d'une passe à bassins, en jet de surface

### Jet plongeant

| Groupe ICE | Espèces | Chute maximale (m) | Profondeur minimale de bassin (m) | Longueur minimale de bassin (m) |
|------------|---------|---------|-----------|--------------|
| 1 | Saumon atlantique (Salmo salar)<br>Truite de mer ou de rivière [50-100] (Salmo trutta) | 0.75 | 1 | 2 |
| 2 | Mulets (Chelon labrosus, Liza ramada) | 0.6 | 0.75 | 1.25 |
| 4a | Truite de rivière ou truite de mer [25-55] (Salmo trutta) | 0.4 | 0.75 | 1.25 |
| 4b | Truite de rivière [15-30] (Salmo trutta) | 0.3 | 0.75 | 1 |
| 6 | Ombre commun (Thymallus thymallus) | 0.3 | 0.75 | 1 |

Table: Liste des valeurs prédéfinies pour les critères de franchissement d'une passe à bassins, en jet plongeant
