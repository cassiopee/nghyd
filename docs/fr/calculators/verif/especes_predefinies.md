# Vérification des critères de franchissement&nbsp;: Espèces prédéfinies

D'après _"Informations sur la Continuité Écologique - ICE, Onema 2014"_.

| Groupe ICE | Espèces |
|-------------------------|----------------|
| 1 | Saumon atlantique (Salmo salar)<br>Truite de mer ou de rivière [50-100] (Salmo trutta) |
| 2 | Mulets (Chelon labrosus, Liza ramada) |
| 3a | Grande alose (Alosa alosa) |
| 3b | Alose feinte (Alosa fallax fallax) |
| 3c | Lamproie marine (Petromyzon marinus) |
| 4a | Truite de rivière ou truite de mer [25-55] (Salmo trutta) |
| 4b | Truite de rivière [15-30] (Salmo trutta) |
| 5 | Aspe (Aspius aspius)<br>Brochet (Esox lucius) |
| 6 | Ombre commun (Thymallus thymallus) |
| 7a | Barbeau fluviatile (Barbus barbus)<br>Chevaine (Squalius cephalus)<br>Hotu (Chondrostoma nasus) |
| 7b | Lamproie fluviatile (Lampetra fluviatilis) |
| 8a | Carpe commune (Cyprinus carpio) |
| 8b | Brème commune (Abramis brama)<br>Sandre (Sander lucioperca) |
| 8c | Brème bordelière (Blicca bjoerkna)<br>Ide melanote (Leuciscus idus)<br>Lotte de rivière (Lota lota)<br>Perche (Perca fluviatilis)<br>Tanche (Tinca tinca) |
| 8d | Vandoises (Leuciscus sp hors Idus) |
| 9a | Ablette commune (Alburnus alburnus)<br>Ablette sprirlin (Alburnoides bipunctatus)<br>Barbeau méridional (Barbus meridionalis)<br>Blageon (Telestes souffia)<br>Carassin commun (Carassius carassius)<br>Carassin argenté (Carassius gibelio)<br>Gardon (Rutilus rutilus)<br>Rotengle (Scardinius erythrophthalmus)<br>Toxostome (Parachondrostoma toxostoma) |
| 9b | Apron (Zingel asper)<br>Chabots (Cottus sp)<br>Goujons (Gobio sp)<br>Grémille (Gymnocephalus cernuus)<br>Lamproie de Planer (Lampetra planeri)<br>Loche franche ( Barbatula barbatula)<br>Loche de rivière (Cobitis taenia) |
| 10 | Able de Heckel (Leucaspius delineatus)<br>Bouvière (Rhodeus amarus)<br>Epinoche (Gasterosteus gymnurus)<br>Epinochette (Pungitius laevis)<br>Vairons (Phoxinus sp) |

Table: Liste des groupe d'espèces prédéfinis
