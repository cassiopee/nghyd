# Vérification des critères de franchissement&nbsp;: Passes à ralentisseurs (simulation)

Pour certaines espèces, les valeurs de certains critères ne sont pas précisément connues ou sont susceptibles d'évoluer quelque peu en fonction des retours d'expérience futurs.

## Critères

### Espèces déconseillées et incompatibles

Les groupes d'espèces 3a, 3b et 7b sont déconseillés pour le franchissement d'une passe à ralentisseurs. Cela donne lieu à un avertissement, sans pour autant entraîner l'infranchissabilité de la passe.

Les groupes d'espèces 8a, 8b, 8c, 8d, 9a, 9b et 10 sont incapables de franchir des passes à ralentisseurs.

### Tirant d'eau minimal, en m

Le tirant d'eau dans la passe doit être supérieur au tirant d'eau minimal.

## Valeurs des critères pour les groupes d'espèces prédéfinis

D'après _"Informations sur la Continuité Écologique - ICE, Onema 2014"_.

| Groupe ICE | Espèces | Tirant d'eau minimal sur les ralentisseurs plans / Fatou (m) | Tirant d'eau minimal sur les ralentisseurs suractifs / mixtes (chevrons) (m) |
|------------|---------|---------|-----------|
| 1 | Saumon atlantique (Salmo salar)<br>Truite de mer ou de rivière [50-100] (Salmo trutta) | 0.3 | 0.2 |
| 2 | Mulets (Chelon labrosus, Liza ramada) | 0.25 | 0.15 |
| 3a | Grande alose (Alosa alosa) | 0.3 | 0.2 |
| 3b | Alose feinte (Alosa fallax fallax) | 0.25 | 0.15 |
| 3c | Lamproie marine (Petromyzon marinus) | 0.1 | 0.1 |
| 4a | Truite de rivière ou truite de mer [25-55] (Salmo trutta) | 0.25 | 0.15 |
| 4b | Truite de rivière [15-30] (Salmo trutta) | 0.2 | 0.1 |
| 5 | Aspe (Aspius aspius)<br>Brochet (Esox lucius) | 0.3 | 0.2 |
| 6 | Ombre commun (Thymallus thymallus) | 0.25 | 0.15 |
| 7a | Barbeau fluviatile (Barbus barbus)<br>Chevaine (Squalius cephalus)<br>Hotu (Chondrostoma nasus) | 0.25 | 0.15 |
| 7b | Lamproie fluviatile (Lampetra fluviatilis) | 0.1 | 0.1 |

Table: Liste des valeurs prédéfinies pour les critères de franchissement d'une passe à ralentisseurs
