# Vérification des critères de franchissement

Cet outil permet de vérifier la capacité des différentes espèces de poissons à franchir les types de passes suivants&nbsp;:

- [passes à bassins](pab.md)
- [passes à ralentisseurs](par.md)
- [passes à macrorugosités](macrorugo.md)

## Principe

Pour chaque type de passe, on contrôle plusieurs critères de franchissabilité, exprimés sous forme de valeurs limites (ex: profondeur minimale de bassin, pour une passe à bassins).

Tout dépassement de la valeur limite d'un critère donne lieu à une erreur explicite mentionnant la grandeur concernée et la valeur limite, et entraîne la non franchissabilité de la passe.

Certains critères comme la puissance maximale dissipée possèdent à la fois une valeur d'alerte (la franchissabilité n'est pas garantie mais reste possible) et une valeur limite (le franchissement est impossible).

Pour une même passe, on peut verifier simultanément la capacité de franchissement de plusieurs espèces, sur plusieurs modalités (variation d'un ou plusieurs paramètres de la passe).

## Espèces prédéfinies

[Plusieurs groupes d'espèces communes](especes_predefinies.md) sont prédéfinis&nbsp;: des valeurs par défaut sont associées aux critères de franchissement.

Les valeurs par défaut de certains critères peuvent être absentes pour un groupe d'espèces, lorsque celui-ci est considéré comme incapable de franchir le type de passes auquel le critère est rattaché.

## Espèces personnalisées

Le module `Espèce` permet de définir des valeurs personnalisées pour tous les critères, avant de lancer la vérification d'une passe.

Si un critère est applicable à la passe à vérifier mais aucune valeur n'a été saisie, une erreur sera affichée lors de la vérification, entraînant la non franchissabilité de la passe.
