# Vérification des critères de franchissement&nbsp;: Passes à macrorugosités

## Passes à macrorugosités complexes

Dans le cas des pases à macrorugosités complexes, chaque radier (rappel&nbsp;: les radiers inclinés sont discrétisés) est vérifié comme une passe à macrorugosités indépendante.

Si au moins un radier est franchissable, la passe est considérée comme franchissable.

La largeur maximale franchissable, maximum des sommes des largeurs des radiers franchissables contigus, est indiquée à l'issue de la vérification.

Pour certaines espèces, les valeurs de certains critères ne sont pas précisément connues ou sont susceptibles d'évoluer quelque peu en fonction des retours d'expérience futurs.

## Critères

### Tirant d'eau minimal, en m

Le tirant d'eau dans la passe doit être supérieur au tirant d'eau minimal.

### Vitesse d'écoulement maximale limite, en m/s

La vitesse d'écoulement maximale dans la passe doit être inférieure à la vitesse d'écoulement maximale limite.

## Valeurs des critères pour les groupes d'espèces prédéfinis

D'après _"Informations sur la Continuité Écologique - ICE, Onema 2014"_.

| Groupe ICE | Espèces | Tirant d'eau minimal (m) | Vitesse d'écoulement maximale limite (m/s) |
|-------------------------|----------------|----------------|----------------|
| 1 | Saumon atlantique (Salmo salar)<br>Truite de mer ou de rivière [50-100] (Salmo trutta) | 0.4 | 2.5 |
| 2 | Mulets (Chelon labrosus, Liza ramada) | 0.3 | 2.5 |
| 3a | Grande alose (Alosa alosa) | 0.4 | 2 |
| 3b | Alose feinte (Alosa fallax fallax) | 0.4 | 2 |
| 3c | Lamproie marine (Petromyzon marinus) | 0.15 | 2 |
| 4a | Truite de rivière ou truite de mer [25-55] (Salmo trutta) | 0.3 | 2 |
| 4b | Truite de rivière [15-30] (Salmo trutta) | 0.2 | 2 |
| 5 | Aspe (Aspius aspius)<br>Brochet (Esox lucius) | 0.3 | 2 |
| 6 | Ombre commun (Thymallus thymallus) | 0.3 | 2 |
| 7a | Barbeau fluviatile (Barbus barbus)<br>Chevaine (Squalius cephalus)<br>Hotu (Chondrostoma nasus) | 0.3 | 2 |
| 7b | Lamproie fluviatile (Lampetra fluviatilis) | 0.15 | 2 |
| 8a | Carpe commune (Cyprinus carpio) | 0.3 | 1.5 |
| 8b | Brème commune (Abramis brama)<br>Sandre (Sander lucioperca) | 0.3 | 1.5 |
| 8c | Brème bordelière (Blicca bjoerkna)<br>Ide melanote (Leuciscus idus)<br>Lotte de rivière (Lota lota)<br>Perche (Perca fluviatilis)<br>Tanche (Tinca tinca) | 0.3 | 1.5 |
| 8d | Vandoises (Leuciscus sp hors Idus) | 0.3 | 1.5 |
| 9a | Ablette commune (Alburnus alburnus)<br>Ablette sprirlin (Alburnoides bipunctatus)<br>Barbeau méridional (Barbus meridionalis)<br>Blageon (Telestes souffia)<br>Carassin commun (Carassius carassius)<br>Carassin argenté (Carassius gibelio)<br>Gardon (Rutilus rutilus)<br>Rotengle (Scardinius erythrophthalmus)<br>Toxostome (Parachondrostoma toxostoma) | 0.2 | 1.5 |
| 9b | Apron (Zingel asper)<br>Chabots (Cottus sp)<br>Goujons (Gobio sp)<br>Grémille (Gymnocephalus cernuus)<br>Lamproie de Planer (Lampetra planeri)<br>Loche franche ( Barbatula barbatula)<br>Loche de rivière (Cobitis taenia) | 0.2 | 1.5 |
| 10 | Able de Heckel (Leucaspius delineatus)<br>Bouvière (Rhodeus amarus)<br>Epinoche (Gasterosteus gymnurus)<br>Epinochette (Pungitius laevis)<br>Vairons (Phoxinus sp) | 0.2 | 1.5 |

Table: Liste des valeurs prédéfinies pour les critères de franchissement d'une passe à macrorugosités
