# Passes à bassins&nbsp;: Nombre de chutes

Cet outil est une aide au prédimensionnement d'une passe à bassins&nbsp;: il permet de calculer la valeur manquante des trois grandeurs&nbsp;:

- la chute totale (\(\Delta H_T\)) en m;
- la chute entre les bassins (\(\Delta H\)) en m;
- le nombre de chutes (\(N\));

ainsi que la chute résiduelle (\(\Delta H_R\)) en m.

## Formule

$$\Delta H_T = \left ( \Delta H * N \right ) + \Delta H_R $$
