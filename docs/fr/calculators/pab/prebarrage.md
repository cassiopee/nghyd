# Pré-barrages

Les pré-barrages sont un type de passe à bassins utilisés pour le franchissement d'obstacles de faible hauteur. Ils sont constitués de plusieurs murs ou seuils fractionnant la chute en plusieurs grands bassins en parallèle et en série.

## Présentation générale

Ce module calcule la distribution des débits et la ligne d'eau des bassins d'un pré-barrage. Deux possibilités de calcul sont offertes&nbsp;: calcul du débit entrant dans la passe connaissant les cotes amont et aval de l'eau, calcul de la cote amont de l'eau connaissant le débit entrant dans la passe et le niveau d'eau à l'aval du pré-barrage.

La saisie des paramètres du pré-barrage comporte deux parties&nbsp;:

- La composition du pré-barrage en bassins et les cloisons faisant communiquer les bassins, sur la première partie de l'écran
- La saisie des paramètres du pré-barrage : conditions aux limites, paramètres des bassins et paramètres des cloisons, sur la deuxième partie de l'écran

Il est possible de faire varier n'importe quels paramètres de façon à obtenir une série de résultats.

Le schéma de la composition du pré-barrage est affichable en plein écran et exportable au format image PNG.

## Composition du pré-barrage

À sa création, le pré-barrage est constitué d'une condition limite amont, d'un bassin, d'une condition limite aval et de cloisons joignant ces trois éléments.

Les boutons "Ajouter un bassin" et "Ajouter une cloison" permettent d'ajouter respectivement un bassin et une cloison au pré-barrage. À l'ajout d'une cloison, il est demandé à quels conditions limites ou bassins la cloison est reliée. La saisie des liaisons avec les cloisons oblige l'utilisateur à suivre la numérotation des bassins qui respecte nécessairement un ordre amont-aval.

Une barre d'outil permet de dupliquer et effacer une cloison ou un bassin sélectionné et de changer l'ordre d'un bassin.

## Saisie des paramètres des éléments du pré-barrage

La saisie des paramètres de condition limite amont ou aval, d'un bassin ou d'une cloison s'effectue en sélectionnant l'élément voulu dans le schéma en première partie d'écran.

La saisie des éléments s'effectue ensuite comme pour tout module de Cassiopée. Pour la saisie des cloisons, se référer au module [lois d'ouvrages](../structures/lois_ouvrages.md).

## Lancement du calcul

Le bouton calculer s'active à partir du moment où tous les bassins sont connectés par des cloisons de l'amont jusqu'à l'aval du pré-barrage et où tous les paramètres ont été saisis.

## Visualisation des résultats

Après le calcul, l'interface fait apparaître deux onglets "Données" et "Résultats" qui permettent de naviguer entre la saisie du pré-barrage et les résultats du calcul.

Si le calcul produit une série de résultats, une liste déroulante permet de choisir quel résultat afficher pour chaque paramètre ayant varié.

La première partie de l'écran montre un synoptique du pré-barrage avec les débits et les chutes au niveau des cloisons, la puissance moyenne dissipée et la profondeur moyenne au niveau des bassins, et le débit et les cotes de l'eau aux conditions limites amont et aval. Pour afficher les résultats chiffrés au niveau des bassins, l'utilisateur doit cliquer sur un des ces éléments sur le schéma. Pour afficher les résultats chiffrés au niveau d'une cloison, cliquer sur la cloison correspondante sur le schéma.

Un bouton "Exporter tous les résultats" permet de récupérer un tableau au format Excel comprenant les résultats des conditions limites, des bassins et des cloisons pour toute la série de calcul.