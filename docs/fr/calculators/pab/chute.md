# Passes à bassins&nbsp;: Chute

Cet outil est une aide au prédimensionnement d'une passe à bassins&nbsp;: il permet de calculer la valeur manquante des trois grandeurs&nbsp;:

- la cote amont (\(Z_1\)) en m;
- la cote aval (\(Z_2\)) en m;
- la chute (\(\Delta H\)) en m;

## Formule

$$\Delta H = Z_1 - Z_2$$
