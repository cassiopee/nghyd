# Cloisons

Cet outil qui est similaire à celui des [Lois d'ouvrages](../structures/lois_ouvrages.md), est une aide au prédimensionnement hydraulique d'une passe à bassins&nbsp;: il
est utilisé le plus souvent pour le dimensionnement des échancrures, fentes, orifices
caractérisant les cloisons d'une passe ainsi que pour le calage en altitude des échancrures,
fentes et radier du bassin amont d'une passe.

Il permet de calculer la valeur manquante des 7 valeurs caractérisant la chute, la surface
de l'orifice noyé, la largeur de la fente, la charge sur la fente, la largeur de l'échancrure,
la charge sur l'échancrure et le débit.

Les données à fournir obligatoirement sont les dimensions des bassins (largeur et longueur) ainsi que le
tirant d'eau moyen en mètres. Ces données associées à la chute entre bassins permettent de calculer [la puissance volumique dissipée](volume.md).

Une fois le calcul de la cloison effectué, l'outil propose de créer une passe à bassin à partir de cette cloison en précisant, la cote amont de l'eau, le nombre de chute et la cote aval de l'eau de la passe.

## Ouvrages hydrauliques pouvant constituer la cloison

L'outil permet de placer un ou plusieurs ouvrages en parallèle parmi les types d'ouvrages suivants:

### Orifice noyé

![Schéma orifice noyé](../structures/orifice_noye_schema.png)

*Extrait de Larinier, M., Travade, F., Porcher, J.-P., Gosset, C., 1992. Passes à poissons : expertise et conception des ouvrages de franchissement. CSP. (page 94)*

L'équation de l'orifice noyé est décrite sur [la page de la formule de l'orifice noyé](../structures/orifice_noye.md).

### Fente noyée

![Schéma de la fente noyée](../structures/fente_noyee_schema.png)

*Extrait de Larinier, M., Travade, F., Porcher, J.-P., Gosset, C., 1992. Passes à poissons : expertise et conception des ouvrages de franchissement. CSP. (page 94)*

L'équation de la fente noyée est décrite sur [la page de la formule de la fente noyée](../structures/fente_noyee.md).

### Echancrure

![Schéma de l'échancrure](../structures/echancrure_schema.png)

*Extrait de Larinier, M., Travade, F., Porcher, J.-P., Gosset, C., 1992. Passes à poissons : expertise et conception des ouvrages de franchissement. CSP. (page 94)*

L'équation utilisée pour l'échancrure est [celle de Kindsvater-Carter et Villemonte](../structures/kivi.md).
