# Passes à bassins&nbsp;: Dimensions

Cet outil est une aide au dimensionnement des bassins d'une passe&nbsp;: il permet de
calculer la valeur manquante des quatre grandeurs&nbsp;:

- le volume d'eau (\(V\)) en m<sup>3</sup>;
- le tirant d'eau moyen (\(Y_{moy}\)) en m;
- la longueur du bassin (\(L\)) en m;
- la largeur du bassin (\(B\)) en m.

Le calcul est effectué en appliquant la formule&nbsp;:

$$V = Y_{moy} \times L \times B$$
