# Passe « naturelle » à enrochements en rangées périodiques

Il est indiqué en p.16 du guide de conception des passes naturelles
(Larinier et al., 2006)[^passe_rangees_periodiques1] que les passes en enrochements
en rangées périodiques s’apparentent aux passes à bassins et leurs critères de
conception sont identiques à ces dernières.

Il est donc possible d’utiliser les différents outils du module « [passe à bassins](pab.md) »
pour concevoir ce type de dispositif.
Notamment, l’outil « [cloisons](cloisons.md) » permet de renseigner les différentes
caractéristiques dimensionnelles des déversoirs (largeurs, cotes déversantes) et
des pseudo-bassins (longueurs, largeurs, cote mi-radier), qui permettent de
retrouver indirectement les paramètres hauteurs de seuils (\(p\)) et hauteurs utiles
des blocs (\(k\)) dans le tableau de géométrie généré.
La porosité (rapport entre le passage libre entre les blocs et la largeur totale
de la pseudo-cloison) n’est pas renseignée directement, mais ce paramètre peut
être déduit et examiné par ailleurs si besoin.

Selon ce principe, chaque portion déversante de la cloison doit être renseignée
en tant qu’échancrure ou fente, de même que la cote du sommet des blocs si ces
derniers sont ennoyé sur la plage de fonctionnement du dispositif.
Pour les échancrures, il convient de porter une attention particulière au type
seuil (épais ou mince) afin de bien retranscrire les conditions d’ennoiement par
l’aval du déversoir le cas échéant.

Il est également possible d’utiliser l’outil « [pré-barrages](prebarrage.md) » (les équations
implémentées sont les mêmes que dans l’outil « [cloisons](cloisons.md) »),
si le dispositif est concerné par des modes alimentations complexes,
avec par exemple des déversés du seuil vers différents bassins du dispositif
(cas moins fréquent pour une passes en enrochements en rangées périodiques).

Un exemple de passe est directement accessible via [ce lien](https://cassiopee.g-eau.fr/#/loadsession/app%2Fexamples%2Fpasse_rangees_periodiques.json).


[^passe_rangees_periodiques1]: Larinier, Michel, Dominique Courret, et Peggy Gomes. 2006. « Guide technique pour la conception des passes à poissons “naturelles” ». Rapport GHAPPE RA. Compagnie Nationale du Rhône / Agence de l’Eau Adour Garonne. [https://dx.doi.org/10.13140/RG.2.1.1834.8562](https://dx.doi.org/10.13140/RG.2.1.1834.8562).
