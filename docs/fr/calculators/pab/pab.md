# Passe à bassins

## Présentation générale

Ce module calcule la ligne d'eau d'une passe à bassins successifs. Deux possibilités de calcul sont offertes&nbsp;: calcul du débit entrant dans la passe connaissant les cotes amont et aval de l'eau, calcul de la cote amont de l'eau connaissant le débit entrant dans la passe.

La création d'une passe peut se faire ex-nihilo ou à partir d'un modèle de cloison créé avec l'outil [Cloisons](cloisons.md).

La saisie des paramètres de la passe comporte deux parties&nbsp;:

- Les paramètres hydrauliques qui comprennent les conditions aux limites (cote de l'eau à l'amont et à l'aval de la passe) et le débit entrant dans la passe
- Les paramètres des bassins qui comprennent la géométrie des bassins et les paramètres des ouvrages hydrauliques constituant les cloisons.

Il est possible de faire varier un ou deux paramètres hydraulique de façon à obtenir une série de résultats pour plusieurs conditions aux limites ou plusieurs débits.

## Saisie de la géométrie de la passe

Le tableau de la géométrie comporte une ligne par bassin et une ligne finale pour décrire la cloison aval. Pour chaque bassin, les paramètres présents sont&nbsp;:

- longueur du bassin (m)
- largeur du bassin (m)
- débit d'attrait (m<sup>3</sup>/s)
- Cote de radier mi-bassin (m)
- Cote du radier amont (m)

A ceux-ci s'ajoute les paramètres des ouvrages de la cloison amont du bassin.

### Modification de la structure de la passe

La structure de la passe, c'est-à-dire, le nombre de bassins ou le nombre d'ouvrages d'une cloison peuvent être modifiés à l'aide de la barre d'outil située en haut à droite du tableau&nbsp;:

![Barre d'outil d'édition de la géométrie de la passe à bassins](pab_barre_outils_edition.png)

Cette barre s'active lorsque on sélectionne&nbsp;:

- un bassin (qui comprend sa cloison amont) ou la cloison aval en en cliquant sur la première colonne d'une ligne
- un ouvrage en cliquant sur une cellule non éditable d'un ouvrage d'une cloison
- tous les ouvrages d'une colonne du tableau en cliquant sur l'entête de la colonne à sélectionner

Il est aussi possible d'étendre une sélection existante en pressant la touche [Ctrl] pour ajouter un nouvel élément à la sélection, ou en pressant la touche [MAJ] pour étendre la sélection entre deux lignes ou deux colonnes.

En fonction des éléments sélectionnés dans le tableau, la barre d'outil indique si les actions proposées s'appliqueront sur les bassins ou les ouvrages sélectionnés.

La barre d'outils est constituée des boutons suivants&nbsp;:

1. Nombre de bassin ou d'ouvrage à ajouter ou à dupliquer
1. Ajouter *n* bassins ou *n* ouvrages avec *n* le nombre indiqué sur le premier bouton
1. Dupliquer *n* bassins ou *n* ouvrages avec *n* le nombre indiqué sur le premier bouton
1. Supprimer les bassins ou ouvrages sélectionnés
1. Déplacer vers le haut (resp. la gauche), les bassins (resp. les ouvrages) sélectionnés
1. Déplacer vers le bas (resp. la droite), les bassins (resp. les ouvrages) sélectionnés

### Modification avancée de la géométrie de la passe

La sélection des bassins ou des ouvrages donne accès à un bouton "Modifier les valeurs" qui permet de modifier un paramètre parmi toutes les variables des cellules sélectionnées dans le tableau de géométrie.

Pour cette variable à modifier, on pourra&nbsp;:

- définir une valeur fixe&nbsp;;
- appliquer un delta&nbsp;;
- calculer une interpolation entre le bassin sélectionné à l'amont et celui à l'aval.

### La cloison aval

La cloison aval, en plus des lois d'ouvrages disponibles sur les cloisons, permet l'utilisation d'une "vanne levante" qui se présente sous la forme de deux lois&nbsp;:

- Échancrure régulée (Villemonte 1957);
- Fente noyée régulée (Larinier 1992).

La vanne levante est un ouvrage où la cote de crête du seuil est régulée pour conserver une chute d'eau de consigne entre le dernier bassin et le cours d'eau à l'aval. En plus des paramètres classiques des lois de débit, il comporte&nbsp;:

- une chute *DH* de consigne (m)&nbsp;
- une cote minimale de la crête du seuil *minZDV* (m)&nbsp;;
- une cote maximale de la crête du seuil *maxZDV* (m)&nbsp;;

Lors du calcul, si la cote calculée pour la crête du seuil est inférieur à *minZDV* (resp. supérieure à *maxZDV*) celle-ci est bloquée à *minZDV* (resp. à *maxZDV*) et un avertissement apparaît dans le journal de calcul.

## Résultats du calcul

Les résultats se présentent sous la forme d'un tableau récapitulatif des calculs hydrauliques pour tous les bassins et cloisons. On y retrouve toutes les données calculées par les modules [Cloisons](cloisons.md) et [Puissance dissipée](volume.md).

Deux graphiques sont présents&nbsp;:

- Un profil en long de la passe avec la cote de radier des bassins et la cote de l'eau dans chaque bassin.
- Un graphique général permettant de sélectionner n'importe quel paramètre du tableau de résultat en abscisse et en ordonnée.

Si plusieurs résultats sont disponibles du fait de la variation d'un ou deux paramètres hydrauliques de la passe, toutes les lignes d'eau calculées s'affichent dans le profil en long, et une liste déroulante permet de sélectionner le résultat à afficher dans le tableau et le graphique généraliste.
