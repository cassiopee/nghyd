# Passes à bassins&nbsp;: Puissance dissipée

Cet outil est une aide au prédimensionnement d'une passe à bassins&nbsp;: il permet de calculer la valeur manquante des quatre grandeurs&nbsp;:

- la chute entre les bassins (\(\Delta H\)) en m;
- le débit (\(Q\)) en m<sup>3</sup>/s;
- le volume des bassins (\(V\)) en m<sup>3</sup>;
- la puissance volumique dissipée (\(P_v\)) en W/m<sup>3</sup>.

La formule de calcul de la puissance dissipée est alors&nbsp;:

$$P_v = \frac{\rho \mathrm{g} Q \Delta H}{V}$$

avec&nbsp;:

- \(\rho\)&nbsp;: la masse volumique de l'eau;
- \(\mathrm{g}\)&nbsp;: l'accélération de la gravité terrestre = 9,81 m.s<sup>-2</sup>
