# Impact de jet

Ce module effectue un calcul balistique d'un jet en négligeant les frottements dus à l'air. Il permet de calculer la valeur manquante parmi les grandeurs suivantes&nbsp;:

- Vitesse initiale au départ du jet (m/s)
- Pente initiale au départ du jet (m/m). A noter que, contrairement à la pente utilisée dans le module de calcul à surface libre, cette pente est positive pour un jet dirigé vers le haut et négative pour un jet dirigé vers le bas.
- Abscisse de l'impact ou distance horizontale parcourue entre le départ du jet et le point d'impact (m)
- Cote de départ du jet (m)
- Cote de l'eau (m)

La cote du fond est utilisée pour calculer la profondeur et le rapport profondeur / chute.

## Dimensionnement d'un exutoire d'évacuation des poissons

L'exutoire d'évacuation des poissons vers l'aval se termine par un dispositif se jetant dans le canal de fuite de la centrale. Le présent module permet de calculer la position et la vitesse au point d'impact de la chute libre ou de la veine d'eau à la surface de l'eau du canal de fuite compte tenu de l'angle et de la vitesse initiaux du jet et de la hauteur de chute.

Extrait de Courret, Dominique, et Michel Larinier. Guide pour la conception de prise d’eau ichtyocompatibles pour les petites centrales hydroélectriques, 2008. https://doi.org/10.13140/RG.2.1.2359.1449, p.24&nbsp;:

> Les vitesses dans l’ouvrage et au point d’impact dans le bief aval doivent rester inférieures à une dizaine de m/s, certains organismes préconisant même de ne pas dépasser 7-8 m/s (ASCE 1995). (...) La hauteur de chute entre le débouché et le plan d’eau ne doit pas dépasser une douzaine de mètres pour éviter tout risque de blessures des poissons à l’impact, quels que soient leur taille et leur mode de chute (chute libre ou chute dans la veine d’eau) (Larinier et Travade 2002). Le rejet doit également se faire dans une zone d’une profondeur suffisante pour éviter tout risque de blessure par choc mécanique. Odeh et Orvis (1998) préconisent une profondeur minimale de l’ordre du quart de la chute, avec un minimum d’environ 1 m.

## Formules utilisées

### Hauteur de chute

$$H = 0.5 * g * \frac{D^{2}}{\cos \alpha^{2} * V_0^{2}} - \tan \alpha * D$$

Avec&nbsp;:

- \(H\)&nbsp;: hauteur de chute (m) qui correspond à la différence de cote entre le départ du jet et la surface de l'eau
- \(g\)&nbsp;: accélération de la gravité = 9.81 m.s-2
- \(D\)&nbsp;: distance horizontale parcourue entre le départ du jet et le point d'impact (m)
- \(\alpha\)&nbsp;: angle de tir par rapport à l'horizontale (°)
- \(\V_0\)&nbsp;: vitesse initiale (m/s)

### Abscisse de l'impact (distance horizontale parcourue)

$$D = \frac{V_0}{g * \cos \alpha} \left ( V_0 * \sin \alpha + \sqrt{ \left ( V_0 * \sin \alpha \right )^{2} + 2 * g * H } \right )$$

### Temps de vol

$$t = \frac{D}{V_0 \cos \alpha} $$

### Vitesse horizontale à l'impact

$$V_x = V_0 \cos \alpha$$

### Vitesse verticale à l'impact

$$V_z = V_0 \sin \alpha - t * g$$

### Vitesse à l'impact

$$V_t = \sqrt{ V_x^{2} + V_z^{2} }$$
