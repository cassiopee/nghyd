# Calcul de la perte de charge sur une grille de prise d'eau

La perte de charge \(\Delta H\) due au plan de grille se calcule comme suit&nbsp;:

$$ \Delta H = \xi \frac{V_1^2}{2g} $$

La vitesse débitante amont \(V_1\) est calculée à partir du débit entonné \(Q\), de la hauteur d’eau \(H_1\)
et de la largeur de la prise d’eau \(B\) à l’amont du plan de grille&nbsp;:

$$ V_1 = \frac{Q}{H_1 \times B} $$

Le calcul du coefficient de pertes de charge \(\xi\) se fait à partir des caractéristiques de la grille. Pour une description complète des hypothèses, des formules et des limitations de la méthode, se référer au rapport de Raynal et al. (2012)[^1] disponible ici&nbsp;: https://continuite-ecologique.fr/wp-content/uploads/2019/11/2012_014.pdf

## Grille conventionnelle

Plans de grille conventionnels&nbsp;: perpendiculaires à l'écoulement et peu inclinés par rapport à l'horizontale.

### Formule

Le calcul des pertes de charges s'effectue à l'aide de la formule F1 de Raynal et al. (2012)[^1]&nbsp;:

$$\xi = a * K_O * K_\beta$$

Avec&nbsp;:

- \(\xi\)&nbsp;: coefficient de perte de charge totale (-)
- \(a\)&nbsp;: coefficient de forme des barreaux (-), voir ci-dessous le paragraphe "Profil des barreaux"
- \(K_O\)&nbsp;: coefficient de perte de charge dû à l'obstruction totale due aux barreaux, éléments longitudinaux et colmatage (-)
- \(K_\beta\)&nbsp;: coefficient de perte de charge dû à l'inclinaison de la grille par rapport à la verticale (-)

$$K_O = \left ( \frac{O_C}{1-O_C} \right )^{1.6}$$

Avec \(O_C\) le coefficient d'obstruction totale due aux barreaux, éléments longitudinaux et colmatage rapporté à la section d'écoulement (-)&nbsp;:

$$ O_C = O + (1 - O) \times C $$

Avec&nbsp;:

- \(O\) l'obstruction globale de la grille, définie comme le ratio adimensionnel entre la surface immergée, effectivement apparente depuis l’amont, de tous les éléments de la grille (barreaux, entretoises, supports), et la surface immergée du plan de grille
- \(C\) le taux de colmatage

$$K_\beta = \left ( 1 - \cos{\beta} \right )^{0.39}$$

Avec \(\beta\) l'angle d’inclinaison de la grille par rapport à la verticale (°).


## Grille orientée

Plans de grille orientés par rapport à l'écoulement et quasi-verticaux.

![Grille orientée](grille-orientee.jpg)

*Courret, D. et Larinier, M. Guide pour la conception de prise d’eau ichtyocompatibles pour les petites centrales hydroélectriques, 2008. <https://doi.org/10.13140/RG.2.1.2359.1449>.*

### Formule

Le calcul des pertes de charges s'effectue à l'aide de la formule F2 de Raynal et al. (2012)[^1] ou encore de l'équation (7) de Raynal et al. (2013)[^2]

$$\xi = a * K_O * K_\alpha$$

Avec \(K_\alpha\) coefficient de perte de charge dû à l'inclinaison de la grille par rapport à l'horizontale (-)

$$K_\alpha = 1 + c * \left ( \frac{90 - \alpha}{90} \right )^{2.35} * \left ( \frac{1 - O}{O} \right )^{3} $$

Avec&nbsp;:

- \(\alpha\)&nbsp;: angle d’inclinaison de la grille par rapport à l'horizontale (°)
- \(c\)&nbsp;: coefficient de forme des barreaux, voir ci-dessous le paragraphe "Profil des barreaux"

## Grille inclinée

Plans de grille perpendiculaires à l'écoulement, et inclinés par rapport à l'horizontale

![Grille inclinée](grille-inclinee.jpg)

![Grille inclinée](grille-inclinee-b.jpg)

*Courret, D. et Larinier, M. Guide pour la conception de prise d’eau ichtyocompatibles pour les petites centrales hydroélectriques, 2008. <https://doi.org/10.13140/RG.2.1.2359.1449>.*

### Formule

Le calcul des pertes de charges s'effectue à l'aide de la formule F3 de Raynal et al. (2012)[^1] ou encore de l'équation (11) de Raynal et al. (2013)[^3]

$$\xi = a * K_b * K_\beta + K_{Fent} * K_{entH}$$

Avec&nbsp;:

- \(K_b\)&nbsp;: coefficient de perte de charge dû à l'obstruction totale due aux barreaux, éléments longitudinaux et colmatage (-)
- \(K_{Fent}\)&nbsp;: coefficient de forme moyen des entretoises et éléments transversaux (-), voir ci-dessous le paragraphe dédié
- \(K_{entH}\)&nbsp;: coefficient de perte de charge dû aux entretoises et éléments de support transversaux (-)

$$K_b = \left ( \frac{O_{b,C}}{1-O_{b,C}} \right )^{1.65}$$

Avec \(O_{b,C} = O_b + (1 - O_b) \times C\) et \(O_b\) l'obstruction due aux barreaux et autres éléments longitudinaux du plan de grille, définie comme le ratio adimensionnel entre la surface immergée, effectivement apparente depuis l’amont, de ces
éléments, et la surface immergée du plan de grille.

$$K_\beta = \left ( \sin \beta \right )^{2}$$

$$K_{entH} = \left ( \frac{O_{entH}}{1-O_{entH}} \right )^{0.77}$$

Avec \(O_{entH}\) l'obstruction effective due aux entretoises et éléments de support transversaux (rapportée à la section d'écoulement), voir ci-dessous le paragraphe dédié.

## Paramètres

### Cote du sommet immergé du plan de grille

Cette cote peut être différent du niveau d'eau si le sommet du plan de grille est noyé.

### Largeur de la section \(B\)

Dans les configurations grille conventionnelle ou inclinée, la largeur de la section doit correspondre à la largeur du plan de grille.

### Vitesse d'approche moyenne pour le débit maximum turbiné, en soustrayant la partie supérieure éventuellement obturée

C'est une valeur "maximalisée" de la vitesse d'approche prise en compte dans le calcul du perte de charge, dans une approche sécuritaire.

### Inclinaison par rapport à l'horizontale

#### grille conventionnelle

Domaine de validité de la formule&nbsp;: 45 ≤ β ≤ 90°

#### grille orientée

Plans de grille verticaux (β = 90°).

La légère inclinaison des plans de grille (β≈ 75/80°), souvent mise en place pour des questions de dégrillage, peut être négligée.

#### grille inclinée

Domaine de validité de la formule&nbsp;: 15° ≤ β ≤ 90°

Préconisation pour le guidage des poissons&nbsp;: β ≤ 26°

### Orientation par rapport à la direction de l'écoulement

#### grille conventionnelle

Plans de grille perpendiculaires à l'écoulement (α = 90°)

#### grille orientée

Domaine de validité de la formule&nbsp;: 30° ≤ α ≤ 90°

Préconisation pour le guidage des poissons&nbsp;: α ≤ 45°

#### grille inclinée

Plans de grille perpendiculaires à l'écoulement (α = 90°)

### Vitesse normale moyenne pour le débit maximum turbiné \(V_N\)

Préconisation pour éviter le placage des poissons sur le plan de grille (barrière physique) ou leur passage prématuré au travers (barrière comportementale)&nbsp;: \(V_N \leq 0.5\) m/s.

#### grille orientée ou inclinée

Au-delà de la valeur moyenne calculée ici, se reporter impérativement aux préconisations tirées de la caractérisation expérimentale des valeurs effectives de vitesses.

### Rapport de forme des barreaux \(b/p\)

Avec \(b\) l'épaisseur et \(p\) la profondeur des barreaux.

#### grille orientée

Domaine de validité de la formule&nbsp;: rapport \(b/p\) voisin de 0.125

### Rapport espacement/épaisseur des barreaux

#### grille orientée

Domaine de validité de la formule&nbsp;: \(1 \leq e / b \leq 3\) avec \(e\) l'espacement libre entre les barreaux.

### Obstruction globale du plan de grille pour les grilles conventionnelles et orientées

L'obstruction globale (barreaux + entretoises + éléments de supports longitudinaux et transversaux) retenue \(O\) est à déterminer à partir des plans de la grille. Elle doit être supérieure à l'obstruction due aux barreaux seuls qui est donnée par la formule \(b / (b + e)\).

Domaine de validité de la formule pour les grilles conventionnelles&nbsp;: \(0.2 \leq O \leq 0.60\)

Domaine de validité de la formule pour les grilles orientées&nbsp;: \(0.35 \leq O \leq 0.60\)

### Obstruction globale du plan de grille pour les grilles inclinées

Elle est décomposé en deux parties.

L'obstruction due aux barreaux et éléments de supports longitudinaux retenue \(O_b\). A déterminer à partir des plans de la grille. Elle doit être supérieure ou égale à l'obstruction due aux barreaux seuls qui est donnée par la formule \(b / (b + e)\). Domaine de validité de la formule&nbsp;: \(0.28 \leq O \leq 0.53\).

L'obstruction effective due aux entretoises et éléments de support transversaux rapportée à la section d'écoulement à déterminer à partir des plans de la grille. Domaine de validité de la formule&nbsp;: \(O_{entH} \leq 0.28\)

### Profil des barreaux

![Profil des barreaux](profil-barreaux.png)

*Raynal, Sylvain. « Étude expérimentale et numérique des grilles ichtyocompatibles ». Sciences et ingénierie en matériaux, mécanique, énergétique et aéronautique - SIMMEA, 2013.*

#### grille conventionnelle

Le coefficient de forme des barreaux \(a\) vaut 2.89 pour le profil rectangulaire (PR) et 1.70 pour le profil hydrodynamique (PH).

#### grille orientée

Le coefficient de forme des barreaux \(a\) vaut 2.89 pour le profil rectangulaire (PR) et 1.70 pour le profil hydrodynamique (PH).

Le coefficient de forme des barreaux \(c\) vaut 1.69 pour le profil rectangulaire (PR) et 2.78 pour le profil hydrodynamique (PH).

#### grille inclinée

| Forme des barreaux | Droplet | Plétina | Tadpole 8 | Tadpole 10 | Hydrodynamique | Rectangulaire |
| --- | --- | --- | --- | --- | --- | --- |
| Coefficient de forme $A_i$| 2.47 | 1.75 | 1.27 | 1.79 | 2.10 | 3.85 |

Extrait de Lemkecher et al. (2020)[^4]

### Coefficient de forme moyen des entretoises et éléments transversaux, pondérés selon leurs parts respectives

#### grille inclinée

A déterminer à partir des plans de la grille.

Vaut par exemple 1.79 pour les entretoises cylindriques, 2.42 pour les entretoises rectangulaires, et de l'ordre de 4 pour les poutres carrées et IPN.

[^1]: Raynal, S., Courret, D., Chatellier, L., Larinier, M., David, L., 2012. Définition de prises d’eau ichtyocompatibles -Pertes de charge au passage des plans de grille inclinés ou orientés dans des configurations ichtyocompatibles et champs de vitesse à leur approche (POLE RA11.02). https://continuite-ecologique.fr/wp-content/uploads/2019/11/2012_014.pdf

[^2]: Raynal, S., Chatellier, L., Courret, D., Larinier, M., David, L., 2013. An experimental study on fish-friendly trashracks–Part 2. Angled trashracks. Journal of Hydraulic Research 51, 67–75. https://doi.org/10.1080/00221686.2012.753646

[^3]: Raynal, S., Courret, D., Chatellier, L., Larinier, M., David, L., 2013. An experimental study on fish-friendly trashracks–Part 1. Inclined trashracks. Journal of Hydraulic Research 51, 56–66. https://doi.org/10.1080/00221686.2012.753647

[^4]: Lemkecher, F., Chatellier, L., Courret, D., David, L., 2020. Contribution of Different Elements of Inclined Trash Racks to Head Losses Modeling. Water 12, 966. https://doi.org/10.3390/w12040966
