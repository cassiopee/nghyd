# Présentation du logiciel Cassiopée

<div style="height: 86px; border: solid #aaa 2px; padding: 10px; padding-right: 20px; border-radius: 5px; display: inline-block;">
  <a href="https://cassiopee.g-eau.fr" style="text-decoration: none;">
    <img style="float:left; margin-right: 20px;" src="logo.png">
    <span style="font-size: 30px; line-height: 60px">https://cassiopee.g-eau.fr</span>
  </a>
</div>

## Caractéristiques générales

Cassiopée est un logiciel consacré à l'hydraulique des rivières avec notamment l'aide au dimensionnement des passes à poissons, l'hydraulique agricole et l'hydraulique à surface libre en général. Il se présente sous la forme de [modules de calcul](general/principe_fonctionnement.md) indépendants permettant chacun de résoudre un problème donné. Les modules de calcul peuvent être enchaînés (des paramètres ou des résultats de calcul peuvent être "liés" entre modules) afin de réaliser des enchaînements de calculs complexes. L'utilisateur peut enregistrer localement les modules utilisés afin de les réutiliser ultérieurement.

## Pré-requis - installation

Cassiopée ne nécessite aucune installation. Il est disponible en ligne à partir d'un navigateur récent (testé sous Firefox, Edge, Chrome et Chromium) en se rendant à l'adresse suivante&nbsp;: [https://cassiopee.g-eau.fr](https://cassiopee.g-eau.fr).

Des versions hors-ligne sont installables sur les plateformes Windows, Linux, macOS, Android. Voir le détail dans la [section installation](general/installation.md) de la documentation.

## Documentation

Télécharger [la documentation au format PDF](https://cassiopee.g-eau.fr/assets/docs/pdf/cassiopee_doc_fr.pdf)

Télécharger [le guide de prise en main illustrée au format PDF](https://cassiopee.g-eau.fr/assets/docs/pdf/cassiopee_notice_illustree_fr.pdf)

## Assistance et signalement de bug

Pour être tenu au courant de l'actualité de Cassiopée et bénéficier du réseau d'entraide des utilisateurs de Cassiopée, inscrivez-vous à la liste de diffusion des utilisateurs de Cassiopée&nbsp;: <https://groupes.renater.fr/sympa/subscribe/cassiopee-users>

Les initiés peuvent tester la future version de Cassiopée en cours de développement à l'adresse <https://cassiopee-dev.g-eau.fr> et faire des retours à bug@cassiopee.g-eau.fr.

Pour signaler un bug de l'application, prière d'utiliser le lien "Signaler un problème" présent dans le menu principal de l'application ou écrire directement à bug@cassiopee.g-eau.fr

Pour les questions concernant la conception des ouvrages de franchissements ichtyocompatibles pour la montaison (passes à bassins, à ralentisseurs, à macro-rugosités) et la dévalaison, adressez-vous à Sylvain Richard, pôle OFB-IMFT Écohydraulique, sylvain.richard@imft.fr.

## Citer Cassiopée

Si vous utilisez Cassiopée dans votre travail, n'oubliez pas de mentionner le logiciel en utilisant la référence suivante :

*Dorchies, David; Grand, François; Chouet, Mathias; Cassan, Ludovic; Courret, Dominique; Richard, Sylvain, 2022, "Cassiopée: tools for designing fish crossing devices for upstream and downstream migrations, and hydraulic calculation tools for environmental and agricultural engineering. Version 4.16.0", <https://doi.org/10.15454/TLO5LX>, Recherche Data Gouv, V1*
