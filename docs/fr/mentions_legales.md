# Mentions légales et conditions d'utilisation

## Éditeur

Le site cassiopee.g-eau.fr dénommé ci-après « Cassiopée » est édité par [l’UMR G-EAU (Unité Mixte de Recherche Gestion de l'Eau, Acteurs, Usages)](http://g-eau.fr):

> Irstea<br/>
> Centre de Montpellier<br/>
> 361, rue Jean-François Breton<br/>
> BP 5095<br/>
> 34196 Montpellier Cedex 5<br/>
> France

> Tél&nbsp;: +33 (0) 4 67 04 63 00

Directeur de publication&nbsp;: Marcel Kuper, Directeur de l’UMR G-EAU

Responsable du projet de développement&nbsp;: David Dorchies

Développeurs&nbsp;: David Dorchies, François Grand, Mathias Chouet

Cassiopée est un outil proposé par [l'OFB (Office Français de la Biodiversité)](https://ofb.gouv.fr/) dans le cadre des conventions OFB-Irstea (Action n°100 de la convention 2016-2018 et  Action n°21 de la convention 2019-2021).

## Hébergement

> Irstea<br/>
> Centre de Montpellier<br/>
> Direction des systèmes d'information<br/>
> 361, rue Jean-François Breton<br/>
> BP 5095<br/>
> 34196 Montpellier Cedex 5<br/>
> France

## Contenus du logiciel Cassiopée

L'OFB et l'UMR G-EAU proposent l’accès à des outils de calcul dans le domaine de l'hydraulique et plus spécifiquement des outils d'aide au dimensionnement de passes à poissons.

L’accès des internautes au site et aux outils de Cassiopée est gratuit et illimité pour tous les usages privés comme professionnels, dès lors que cet accès se fait par les seuls outils et interfaces du site cassiopee.g-eau.fr.

Sauf mention contraire, la propriété intellectuelle du contenu des pages est détenue par l'Institut national de Recherche en Sciences et Technologies pour l'Environnement et l'Agriculture (IRSTEA).

## Limitation de responsabilité

L'UMR G-EAU s’engage à fournir les moyens nécessaires et raisonnables pour assurer ou faire assurer un accès continu au site cassiopee.g-eau.fr et à ses contenus pour l’utilisateur.

L'UMR G-EAU ne pourra toutefois être tenu pour responsable à l’égard des utilisateurs en cas d’interruption, de défaillance, de défaut éventuel de qualité des services de Cassiopée pour quelque raison que ce soit, y compris pour des raisons de maintenance, d’entretien ou de mise à jour des serveurs.

Sauf mention contraire, les contenus de Cassiopée sont publiés à titre d’information, à l’exclusion de toute garantie sur leur exactitude ou leur adéquation aux besoins spécifiques des utilisateurs du site cassiopee.g-eau.fr. Les contenus de Cassiopée n’engagent en aucun cas la responsabilité des producteurs en cas de dommage direct ou indirect découlant de leur non-conformité à la réalité du terrain.

Si vous constatiez une erreur ou une omission dans les outils et contenus de Cassiopée, nous vous remercions de le signaler via la fonction « Signaler un problème » disponible dans le menu principal de Cassiopée.

## Informations personnelles des utilisateurs

### Données recueillies par Cassiopée

Le site cassiopee.g-eau.fr ne recueille aucune donnée personnelle sur l'utilisateur hormis l'adresse IP de la machine utilisée pour se connecter sur le site, les modules de calcul utilisés ainsi que le nombre de calcul lancés.

Les données et les calculs effectués par Cassiopée sont entièrement réalisés sur la machine de l'utilisateur et ne sont transmis ni à l'hébergeur, ni à aucun autre tiers.

La soumission d'un rapport de bug via la fonction « Signaler un problème » invite l'utilisateur à transmettre par mail le contenu de sa session de travail en cours à l'équipe de développement de Cassiopée. L'utilisateur peut choisir de ne pas transmettre sa session en supprimant le texte du mail situé sous la ligne `--- État de la session en cours - ne pas modifier le texte ci-dessous ---`. L'adresse électronique transmise par l'utilisateur à l'équipe de développement ne sera en aucun cas transmis à des tiers ou utilisé à d'autres fins que pour contacter l'utilisateur dans le cadre du problème signalé.

### Données stockées sur le terminal de l'utilisateur

Cassiopée ne stocke aucune information sur le terminal de l'utilisateur hormis les paramètres de l'application si ceux-ci sont explicitement sauvegardés par l'utilisateur.

Tout ou partie de la session de travail de l'utilisateur peut être enregistrée par l'utilisateur sur tout support à sa convenance (fichier au format json) et rechargé ensuite pour un usage ultérieur.

## Liens hypertextes

### Liens de cassiopee.g-eau.fr vers d’autres sites

Les liens insérés dans les pages du site cassiopee.g-eau.fr vers des sites tiers sont proposés à titre d’information&nbsp;; le contenu des sites vers lesquels ces liens conduisent n’engage pas la responsabilité de l’UMR G-EAU.

### Établir un lien vers cassiopee.g-eau.fr

L’établissement d’un hyperlien vers Cassiopée est libre et gratuit à la condition que cet hyperlien permette l’ouverture d’une nouvelle fenêtre de visualisation et que l’affichage de l’URL cassiopee.g-eau.fr dans la nouvelle fenêtre soit lisible par l’internaute dès son ouverture et tout au long de l’accès aux données de Cassiopée.

L’UMR G-EAU se réserve le droit de faire supprimer un hyperlien vers Cassiopée qui serait de nature à nuire à sa politique éditoriale ou à porter atteinte à son image.

## Marques et logotypes

Les marques et logotypes figurant sur le site permettent d’informer quant à l’origine des données et des logiciels utilisés&nbsp;; elles n’ont aucun caractère publicitaire et sont propriété de leurs détenteurs respectifs.

## Copies d’écran et impressions

Les contenus produits par Cassiopée (Résultats de calculs, tableaux, graphiques...) sont réutilisables et diffusable sur tout support sans aucune limitation.

La documentation de Cassiopée est publiée sous [Licence CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.fr) qui autorise à copier, distribuer, communiquer tout ou partie de la documentation par tous moyens et tous formats à condition de la créditer, d'intégrer un lien vers la licence, de ne pas en faire un usage commercial, et de ne pas l'avoir modifiée.

## Logiciels libres

### Licence du module de calcul de Cassiopée

Cassiopée est basé sur une bibliothèque nommée JaLHyd (pour JAvascript Library for HYDraulics) développée en Typescript par l'équipe de développement de Cassiopée. Cette bibliothèque est publiée sous [licence GNU AGPL 3](https://www.gnu.org/licenses/agpl.html) et le code source est disponible sur simple demande à l'éditeur de Cassiopée.

### Outils et bibliothèques tiers

Cassiopée a été développé à l'aide de nombreuses bibliothèques et outils du libre dont:

- [Typescript](https://github.com/Microsoft/TypeScript)
- [Visual Studio Code](https://github.com/Microsoft/vscode)
- [Angular](https://github.com/angular/angular)
- [Angular Material](https://github.com/angular/components)
- [Chart.js](https://github.com/chartjs/Chart.js)
- [MathJax](https://github.com/mathjax/MathJax)
