# Jet impact

This module performs a ballistic calculation of a jet while neglecting air friction. It allows to calculate the missing value among the following quantities:

- Initial velocity at the start of the jet (m/s)
- Initial slope at the start of the jet (m/m). Note that, contrary to the slope used in the free surface calculation module, this slope is positive for an upward directed jet and negative for a downward directed jet.
- Abscissa of the impact or horizontal distance travelled between the start of the jet and the point of impact (m)
- Start dimension of the jet (m)
- Water elevation (m)

The bottom elevation is used to calculate the depth and the depth to fall ratio.

## Designing an outlet for fish evacuation

The downstream fish evacuation outlet ends with a device that empties into the plant's tailrace. This module calculates the position and velocity at the point of impact of the free fall or water vein on the surface of the tailrace water taking into account the initial angle and velocity of the jet and the drop height.

Excerpt from Courret, Dominique, and Michel Larinier. Guide for the design of ichthyocompatible water intakes for small hydroelectric power plants, 2008. https://doi.org/10.13140/RG.2.1.2359.1449, p.24:

> Speeds in the structure and at the point of impact in the tailrace must remain below about 10 m/s, with some organizations even recommending that they not exceed 7-8 m/s (ASCE 1995). (...) The head between the outlet and the water body must not exceed a dozen metres to avoid any risk of injury to fish on impact, whatever their size and mode of fall (free fall or fall in the water vein) (Larinier and Travade 2002). The discharge must also be made in an area of sufficient depth to avoid any risk of injury from mechanical shock. Odeh and Orvis (1998) recommend a minimum depth of about a quarter of the fall, with a minimum of about 1 m.

## Formulas used

### Fall height

$$H = 0.5 * g * \frac{D^{2}}{\cos \alpha^{2} * V_0^{2}} - \tan \alpha * D$$

With:

- \(H\): height of fall (m) which corresponds to the difference in height between the jet's departure and the water surface.
- \(g\): acceleration of gravity = 9.81 m.s-2
- \(D\): horizontal distance travelled between the start of the jet and the point of impact (m)
- \(\alpha\)&nbsp;: angle of shooting in relation to the horizontal (°)
- \(\V_0\)&nbsp;: initial speed (m/s)

### Impact abscissa (horizontal distance covered)

$$D = \frac{V_0}{g * \cos \alpha} \left ( V_0 * \sin \alpha + \sqrt{ \left ( V_0 * \sin \alpha \right )^{2} + 2 * g * H } \right )$$

### Flight time

$$t = \frac{D}{V_0 \cos \alpha} $$

### Horizontal speed at impact

$$V_x = V_0 \cos \alpha$$

### Vertical speed at impact

$$V_z = V_0 \sin \alpha - t * g$$

### Speed at impact

$$V_t = \sqrt{ V_x^{2} + V_z^{2} }$$
