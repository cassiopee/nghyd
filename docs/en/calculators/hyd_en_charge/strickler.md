# Strickler formula

This linear head loss law is parameterised by the Stricker coefficient \(K_S\).

The other law parameters are shared with all pressure loss formulas:

- \(Q\) : flow (m<sup>3</sup>/s)
- \(D\) : pipe diameter (m)
- \(l_T\) : pipe length (m)

$$J_L=\frac{l_T.Q^2}{(K_S.\pi.D^2/4)^2.(D/4)^{4/3}}$$
