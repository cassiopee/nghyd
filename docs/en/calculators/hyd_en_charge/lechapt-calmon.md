# Lechapt and Calmon

This module allows to calculate the pressure losses in a circular pipe from the Lechapt and Calmon abacuses.

It allows the calculation of the value of one of the following quantities:

- Flow rate (m<sup>3</sup>/s)
- Pipe diameter (m)
- Total head loss (m)
- Pipe length (m)
- Singular pressure loss coefficient

The total head loss is the sum of the linear head losses \(J_L\) obtained from the Lechapt and Calmon abacuses and singular \(J_S\) depending on the above coefficient.

## Lechapt and Calmon abacuses

Lechapt and Calmon formula is based on adjustements of [Cyril Frank Colebrook formula](https://en.wikipedia.org/wiki/Darcy_friction_factor_formulae#Colebrook%E2%80%93White_equation):

$$J_L=\frac{l_T}{1000}L.Q^M.D^{-N}$$

With:

- \(J_L\): headloss in mm/m or m/km;
- \(l_T\): pipe length in m;
- \(Q\): flow in L/s;
- \(D\): pipe diameter in m;
- \(L\), \(M\) and \(N\) coefficients depending on roughness {&#x3F5;}.

The error made with respect to the Colebrook formula is less than 3% for speeds between 0.4 and 2 m/s.

The correlation table of the coefficients is as follows:


| Material                                       | &#x3F5; (mm) | \(L\) | \(M\) | \(N\) |
|------------------------------------------------|-------------:|----:|----:|-----:|
| Uncoated cast iron or steel - Coarse concrete (corrosive water) | 2 | 1.863 | 2 | 5.33 |
| Uncoated cast iron or steel - Coarse concrete (low corrosive water) | 1 | 1.601 | 1.975 | 5.25 |
| Cast iron or steel with cement coating | 0.5 | 1.40 | 1.96 | 5.19 |
| Cast iron or steel bitumen coating - centrifuged concrete | 0.25 | 1.16 | 1.93 | 5.11 |
| Rolled steel - smooth concrete | 0.1 | 1.10 | 1.89 | 5.01 |
| Cast iron or steel centrifugal coating | 0.05 | 1.049 | 1.86 | 4.93 |
| PVC - polyethylene | 0.025 | 1.01 | 1.84 | 4.88 |
| Hydraulically smooth pipe - 0.05 &le; D &le; 0.2 | 0.00 | 0.916 | 1.78 | 4.78 |
| Hydraulically smooth pipe - 0.25 &le; D &le; 1 | 0.00 | 0.971 | 1.81 | 4.81 |

Table: Materials and coefficients used in the Lechapt and Calmon formula

## Singular head loss

$$ J_S = K_S \frac{V^2}{2g}$$

With:

- \(K_S\)&nbsp;: singular head loss coefficient
- \(V\)&nbsp;: water speed in the pipe (\(V = 4 Q / \pi / D^2\))

## Darcy's head loss coefficient

$$ f_D = \frac{2g J D}{l_T V^2}
