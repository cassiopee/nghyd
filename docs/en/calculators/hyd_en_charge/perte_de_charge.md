# Pressure loss

This module computes linear head losses in a circular pipe with the following laws:

- [Lechapt et Calmon](lechapt-calmon.md)
- [Strickler](strickler.md)


The following values can be computed:

- Flow (m<sup>3</sup>/s)
- Pipe diameter (m)
- Total pressure loss (m)
- Pipe length (m)
- Singular head loss coefficient

The total pressure loss is the sum of linear losses \(J_{lin}\), according to the used law, and singular losses \(J_{loc}\) depending on the above coefficient.

## Singular pressure loss

$$ J_{loc} = K_{loc} \frac{V^2}{2g}$$

Given&nbsp;:

- \(K_{loc}\)&nbsp;: singular head loss coefficient
- \(V\)&nbsp;: water speed in the pipe (\(V = 4 Q / \pi / D^2\))

## Linear head loss coefficient

$$ K_{lin} = \frac{2g J_{lin}}{V^2} $$

## Darcy head loss coefficient 

$$ f_D = \frac{2g J D}{l_T V^2} $$
