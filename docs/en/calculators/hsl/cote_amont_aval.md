# Upstream / downstream elevations of a reach

This module is based on the equations of the [backwater curves module](courbe_remous.md) and is used to calculate the following:

- The water elevation upstream of a reach of a fluvial backwater curve;
- The water elevation downstream of a reach of a torrential backwater curve;
- The flow that connects the upstream and downstream water elevations of a fluvial or torrential backwater curve.

The regime chosen on the type of water line determines whether the calculation is made from downstream to upstream (fluvial regime) and from upstream to downstream (torrential regime).

This calculation module is particularly useful for calculating the water line of a series of hydraulic structures or reaches (see the typical example "Flow of a channel with structures").
