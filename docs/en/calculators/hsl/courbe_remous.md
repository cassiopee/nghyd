# Backwater curve

The calculation of the backwater curve involves the following differential equation:



$$\frac{dy}{dx}=\frac{I_f - J(h)}{1-F^2(h)}$$

where \(I_f\) is the slope of a canal, \(J\) the formula giving us the local pressure drop (depending on the water level), \(y\) here refers to the height of water.

Thus, for a rectangular channel of width \(b\) and a Strickler coefficient \(K\):

$$J=\frac{Q^2 (b+2y)^{4/3}}{K^2 b^{10/3}y^{10/3}}$$

and

$$F^2=\frac{Q^2}{gb^2y^3}$$

The integration of the equation can be done by one of the following methods: [Runge-Kutta 4](../../methodes_numeriques/rk4.md), [Explicit Euler](../../methodes_numeriques/euler_explicite.md), [trapezes integration](../../methodes_numeriques/integration_trapezes.md).

Depending on the flow regime, the calculation can be carried out:

 * from downstream to upstream for subcritical flow with definition of a downstream boundary condition.
 * from upstream to downstream for supercritical flow with definition of an upstream boundary condition

If we take the example of a rectangular channel, [the proposed scilab code example for solving an ordinary differential equation](../../methodes_numeriques/euler_explicite.md) is amended as follows:

```scilab
  b=0.3;
  K=50;
  If=0.005;
  Q=0.01;
  function z=DQ(y);
    z=Q-K*(b*y)^(5/3)/(b+2*y)^(2/3)*sqrt(If);
  endfunction
  yn=fsolve(0.5,DQ);
  tmax=0;
  t0=10;
  dt=-0.5;
  function z=f(y,t);
    z=(If-Q^2*(b+2*y)^(4/3)/(K^2*(b*y)^(10/3)))/(1-Q^2/(9.81*b^2*y^3));
  endfunction
  y0=0.12;
```

which gives us the normal depth, and the water line. Depending on the numerical method used, we can have large errors in the case of an F2 backwater curve (downstream condition below normal height), because the waterline slopes are much steeper, and therefore much more prone to errors related to linear interpolation. We can therefore deduce that on the one hand the choice of the resolution method is important, and on the other hand it is essential to take a critical look at the solutions (with an interpretation of the processes we are trying to model).
