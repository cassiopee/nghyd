# Manning-Strickler's formula

## Definition

Manning-Strickler formula is written as follows:

$$V = K_s R_h^{2/3} i^{1/2}$$

with:

- \(V\) la vitesse moyenne de la section transversale en m/s
- \(K_s\) Strickler's coefficient
- \(R_h\) Hydraulic radius in m
- \(i\) slope en m/m

The Strickler coefficient \(K_s\) varies from 20 (rough stone and rough surface) to 80 (smooth concrete and cast iron).

Manning's coefficient \(n\) is obtained by&nbsp;:

$$n = \frac{1}{K_s}$$

## Chow's table (1959)

| Type of channel and description          | \(K_S\) min.   | \(K_S\)  normal  | \(K_S\) max. |
|------------------------------------------|----------:|----------:|----------:|
| **1\. Main channels**                                                                                                               |         |                       |                          |
|   a\. clean, straight, full stage, no rifts or deep pools                                                                      | 30      | 33                    | 40                       |
|   b\. same as above, but more stones and weeds                                                                                 | 25      | 29                    | 33                       |
|   c\. clean, winding, some pools and shoals                                                                                    | 22      | 25                    | 30                       |
|   d\. same as above, but some weeds and stones                                                                                 | 20      | 22                    | 29                       |
|   e\. same as above, lower stages, more ineffective slopes and sections                                                        | 18      | 21                    | 25                       |
|   f\. same as "d" with more stones                                                                                             | 17      | 20                    | 22                       |
|   g\. sluggish reaches, weedy, deep pools                                                                                      | 13      | 14                    | 20                       |
|   h\. very weedy reaches, deep pools, or floodways  with heavy stand of timber and underbrush                                   | 7       | 10                    | 13                       |
| **2\. Mountain streams, no vegetation in channel, banks usually steep, trees and brush along banks submerged at high stages**						 |
|   a\. bottom: gravels, cobbles, and few boulders                                                                               | 20      | 25                    | 33                       |
|   b\. bottom: cobbles with large boulders                                                                                      | 14      | 20                    | 25                       |
| **3\. Floodplains** 						                                                                                                         |
|   *a\. Pasture, no brush* |         |                       |                          |
| 1\. short grass                                                                                                                 | 29      | 33                    | 40                       |
| 2\. high grass                                                                                                                  | 20      | 29                    | 33                       |
| *b\. Cultivated areas*                                                                                                            |         |                       |                          |
|  1\. no crop                                                                                                                   | 25      | 33                    | 50                       |
|   2\. mature row crops                                                                                                         | 22      | 29                    | 40                       |
|   3\. mature field crops                                                                                                       | 20      | 25                    | 33                       |
| *c\. Brush*                                                                                                                       |         |                       |                          |
|   1\. scattered brush, heavy weeds                                                                                             | 14      | 20                    | 29                       |
|   2\. light brush and trees, in winter                                                                                         | 17      | 20                    | 29                       |
|   3\. light brush and trees, in summer                                                                                         | 13      | 17                    | 25                       |
|   4\. medium to dense brush, in winter                                                                                         | 9       | 14                    | 22                       |
|   5\. medium to dense brush, in summer                                                                                         | 6       | 10                    | 14                       |
|     *d\. Trees*                                                                                                                  |         |                       |                          |
|   1\. dense willows, summer, straight                                                                                          | 5       | 7                     | 9                        |
|   2\. cleared land with tree stumps, no sprouts                                                                                | 20      | 25                    | 33                       |
|   3\. same as above, but with heavy growth of sprouts                                                                          | 13      | 17                    | 20                       |
|   4\. heavy stand of timber, a few down trees, little undergrowth, flood stage below branches                                   | 22      | 21                    | 20                       |
|   5\. same as 4\. with flood stage reaching  branches                                                                          | 6       | 8                     | 10                       |
| **4\. Excavated or Dredged Channels**						                                                                                        |
| *a\. Earth, straight, and uniform* |         |                       |                          |
| 1\. clean, recently completed                                                                                                   | 50      | 56                    | 63                       |
|  2\. clean, after weathering                                                                                                   | 40      | 45                    | 56                       |
| 3\. gravel, uniform section, clean                                                                                              | 33      | 40                    | 45                       |
|  4\. with short grass, few weeds                                                                                               | 30      | 37                    | 45                       |
| *b\. Earth winding and sluggish*                                                                                                 |         |                       |                          |
| 1\. no vegetation                                                                                                               | 33      | 40                    | 43                       |
|  2\. grass, some weeds                                                                                                         | 30      | 33                    | 40                       |
|  3\. dense weeds or aquatic plants in deep channels                                                                            | 25      | 29                    | 33                       |
|  4\. earth bottom and rubble sides                                                                                             | 29      | 33                    | 36                       |
|  5\. stony bottom and weedy banks                                                                                              | 25      | 29                    | 40                       |
|  6\. cobble bottom and clean sides                                                                                             | 20      | 25                    | 33                       |
| *c\. Dragline\-excavated or dredged*                                                                                             |         |                       |                          |
| 1\. no vegetation                                                                                                               | 30      | 36                    | 40                       |
|  2\. light brush on banks                                                                                                      | 17      | 20                    | 29                       |
| *d\. Rock cuts*                                                                                                                  |         |                       |                          |
| 1\. smooth and uniform                                                                                                          | 25      | 29                    | 40                       |
|  2\. jagged and irregular                                                                                                      | 20      | 25                    | 29                       |
| *e\. Channels not maintained, weeds and brush uncut*                                                                             |         |                       |                          |
|   1\. dense weeds, high as flow depth                                                                                          | 8       | 13                    | 20                       |
|   2\. clean bottom, brush on sides                                                                                             | 13      | 20                    | 25                       |
|   3\. same as above, highest stage of flow                                                                                     | 9       | 14                    | 22                       |
|   4\. dense brush, high stage                                                                                                  | 7       | 10                    | 13                       |
| **5\. Lined or Constructed Channels**						                                                                                        |
| *a\. Cement*   |         |                       |                          |
|  1\.  neat surface                                                                                                             | 77      | 91                    | 100                      |
|  2\. mortar                                                                                                                    | 67      | 77                    | 91                       |
| *b\. Wood*                                                                                                                       |         |                       |                          |
|  1\. planed, untreated                                                                                                         | 71      | 83                    | 100                      |
|  2\.  planed, creosoted                                                                                                        | 67      | 83                    | 91                       |
|  3\. unplaned                                                                                                                  | 67      | 77                    | 91                       |
|  4\. plank with battens                                                                                                        | 56      | 67                    | 83                       |
|  5\. lined with roofing paper                                                                                                  | 59      | 71                    | 100                      |
| *c\. Concrete*                                                                                                                   |         |                       |                          |
|   1\. trowel finish                                                                                                            | 67      | 77                    | 91                       |
|   2\. float finish                                                                                                             | 63      | 67                    | 77                       |
|   3\. finished, with gravel on bottom                                                                                          | 50      | 59                    | 67                       |
|   4\. unfinished                                                                                                               | 50      | 59                    | 71                       |
|   5\. gunite, good section                                                                                                     | 43      | 53                    | 63                       |
|   6\. gunite, wavy section                                                                                                     | 40      | 45                    | 56                       |
|   7\. on good excavated rock                                                                                                   |         | 50                    | 59                       |
|   8\. on irregular excavated rock                                                                                              |         | 37                    | 45                       |
| *d\. Concrete bottom float finish with sides of:*                                                                                |         |                       |                          |
|   1\. dressed stone in mortar                                                                                                  | 50      | 59                    | 67                       |
|   2\. random stone in mortar                                                                                                   | 42      | 50                    | 59                       |
|   3\. cement rubble masonry, plastered                                                                                         | 42      | 50                    | 63                       |
|   4\. cement rubble masonry                                                                                                    | 33      | 40                    | 50                       |
|   5\. dry rubble or riprap                                                                                                     | 29      | 33                    | 50                       |
| *e\. Gravel bottom with sides of:*                                                                                               |         |                       |                          |
|   1\. formed concrete                                                                                                          | 40      | 50                    | 59                       |
|   2\. random stone mortar                                                                                                      | 38      | 43                    | 50                       |
|   3\. dry rubble or riprap                                                                                                     | 28      | 30                    | 43                       |
| *f\. Brick*                                                                                                                      |         |                       |                          |
|   1\. glazed                                                                                                                   | 67      | 77                    | 91                       |
|   2\. in cement mortar                                                                                                         | 56      | 67                    | 83                       |
| *g\. Masonry*                                                                                                                    |         |                       |                          |
|   1\. cemented rubble                                                                                                          | 33      | 40                    | 59                       |
|   2\. dry rubble                                                                                                               | 29      | 31                    | 43                       |
| *h\. Dressed ashlar/stone paving*                                                                                                | 59      | 67                    | 77                       |
| *i\. Asphalt*                                                                                                                    |         |                       |                          |
|   1\. smooth                                                                                                                   |         | 77                    | 77                       |
|   2\. rough                                                                                                                    |         | 63                    | 63                       |
| *j\. Vegetal lining*                                                                                                             | 2       |                       | 33                       |

Table: Chow's table (1959)
