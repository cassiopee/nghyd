# Uniform flow


The uniform flow is characterized by a water height called the normal height. The normal height is reached when the water line is parallel to the bottom, the load is then itself parallel to the water line and thus the head loss is equal to the slope of the bottom:
\(I_f = J\)

With:

- \(I_f\): bottom slope in m/m
- \(J\): head loss in m/m

The head loss {J} is calculated here using Manning-Strickler's formula:

$$J=\frac{U^2}{K^{2}R^{4/3}}=\frac{Q^2}{S^2K^{2}R^{4/3}}$$

With:

- \(K\): Strickler coefficient in m<sup>1/3</sup>/s

In uniform flow, we obtain the formula:

$$Q=KR^{2/3}S\sqrt{I_f}$$

Based on the which, flow \(Q\), slope \(I_f\) and Strickler calculation \(K\) can be calculated analytically.

To calculate normal height \(h_n\) , one can solve \(f(h_n)=Q-KR^{2/3}S\sqrt{I_f}=0\)

using Newton's method:

$$h_{k+1} = h_k - \frac{f(h_k)}{f'(h_k)}$$

 with:

- \(f(h_k) = Q-KR^{2/3}S\sqrt{I_f}\)
- \(f'(h_k) = -K \sqrt{I_f}(\frac{2}{3}R'R^{-1/3}S+R^{2/3}S')\)

To calculate the geometrical parameters of the section, the calculation module uses the flow calculation equation and solves the problem by dichotomy.
