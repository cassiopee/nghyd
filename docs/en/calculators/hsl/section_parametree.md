# Parametric section

This module calculates the hydraulic quantities associated to:

- a section with a defined geometrical shape ([See section types managed by Cassiopée](types_sections.md))
- a water depth \(y\) in m
- a flow \(Q\) in m<sup>3</sup>/s
- a bottom slope \(I_f\) in m/m
- a roughness expressed with the Strickler's coefficient \(K\) in m<sup>1/3</sup>/s

The calculated hydraulic quantities are:

- Width at mirror (m)
- Wet perimeter (m)
- Hydraulic surface (m<sup>2</sup>)
- Hydraulic radius (m)
- Average speed (m/s)
- Specific head (m)
- Head loss (m)
- Linear variation of specific energy (m/m)
- Normal depth (m)
- Froude number
- Critical depth (m)
- Critical head (m)
- Corresponding depth (m)
- Impulsion (N)
- Conjugate depth
- Tractive force (Pa)

<div style="position: relative"><a id="hauteur-de-berge-debordement-et-ecoulement-en-charge" style="position: absolute; top: -60px;"></a></div>
## Bank height, overflow and closed-conduit flow

The sections are all provided with a bank height which is used at three levels in the free surface hydraulics calculation tools:

- It allows to define if the calculated water level overflows the section.
- Beyond this bank height the hydraulic calculations simulate a flow between two vertical walls. For example, a semi-circular pipe can be modelled by defining a bank height equal to the radius of the pipe.
- For a circular pipe, a bank height greater than or equal to the diameter of the pipe makes it possible to model a closed pipe. If the water level exceeds the diameter of the pipe, hydraulic calculations simulate a closed-conduit flow using the Preissmann slot technique.

<div style="position: relative"><a id="largeur-au-miroir-surface-et-perimetre-mouille" style="position: absolute; top: -60px;"></a></div>
## Width at mirror, wet perimeter and surface

[See the dedicated page for the parameters specific to each type of section](types_sections.md)

### Rectangular section

- Width at mirror : \(B=L\)
- Surface : \(S=L.y\)
- Perimeter : \(P=L+2y\)

### Trapezoidal section

- Width at mirror : \(B=L+2..m.y\)
- Surface : \(S=(L+m.y)y\)
- Perimeter : \(P=L+2y\sqrt{1+m^2}\)

### Circular section

- Width at mirror : \(B=D\sin\theta\)
- Surface : \(S=\frac{D^2}{4} \left(\theta - \sin\theta.\cos\theta \right)\)
- Perimeter : \(P=D.\theta\)

### Parabolic section

- Width at mirror : \(B=\frac{B_b}{y_b^k}y^k\)
- Surface : \(S=\frac{B_b}{y_b^k}\frac{y^{k+1}}{k+1}\)
- Perimeter : \(P=2\sum _{i=1}^{n}\sqrt{\frac{1}{n^2}+\frac{1}{4}\left( B\left(\frac{i.y}{n}\right)-B\left(\frac{(i-1).y}{n}\right) \right)^2}\) for \(n\) large enough

<div style="position: relative"><a id="le-rayon-hydraulique-m" style="position: absolute; top: -60px;"></a></div>
## Hydraulic radius (m)

$$R = S / P$$

<div style="position: relative"><a id="la-vitesse-moyenne-ms" style="position: absolute; top: -60px;"></a></div>
## Average speed (m/s)

$$U = Q /S$$

<div style="position: relative"><a id="la-charge-specifique-m" style="position: absolute; top: -60px;"></a></div>
## Specific head (m)

$$H(y) = y + \frac{U^2}{2g}$$

<div style="position: relative"><a id="la-perte-de-charge-mm" style="position: absolute; top: -60px;"></a></div>
## Head loss (m/m)

Cassiopée uses Manning Strickler formula:

$$J=\frac{U^2}{K^{2}R^{4/3}}=\frac{Q^2}{S^2K^{2}R^{4/3}}$$

<div style="position: relative"><a id="la-variation-lineaire-de-lenergie-specifique-mm" style="position: absolute; top: -60px;"></a></div>
## Linear variation of specific energy (m/m)

$$\Delta E_s = I_f - J$$

<div style="position: relative"><a id="le-tirant-deau-normal-m" style="position: absolute; top: -60px;"></a></div>
## Normal depth (m)

[See the uniform flow calculation.](regime_uniforme.md)

<div style="position: relative"><a id="le-froude" style="position: absolute; top: -60px;"></a></div>
## Froude number

The Froude number expresses the ratio between the mean fluid velocity and the surface wave velocity. \(c\).

$$ c = \sqrt{\frac{gS}{B}}$$

$$ Fr = \frac{U}{c} = \sqrt{\frac{Q^2B}{gS^3}}$$

<div style="position: relative"><a id="le-tirant-deau-critique-m" style="position: absolute; top: -60px;"></a></div>
## Critical depth (m)

The critical height is reached when the average velocity of the fluid is equal to the velocity of the waves on the water surface.

The critical height is therefore reached when the Froude number \(Fr=1\).

For any section, the critical height is calculated as follows \(y_c\) by solving \(f(y_c)=Fr^2-1=0\)

We use Newton's method by posing \(y_{k+1} = y_k - \frac{f(y_k)}{f'(y_k)}\) with :
- \(f(y_k) = \frac{Q^2 B}{g S^3} - 1\)
- \(f'(y_k) = \frac{Q^2}{g} \frac{B'.S - 3 B S'}{S^4}\)

<div style="position: relative"><div style="position: relative"><a id="la-charge-critique-m" style="position: absolute; top: -100px;" style="position: absolute; top: -60px;"></a></div></div>
## Critical head (m)

This is the head calculated for a water depth equal to the critical depth. \(H_c = H(y_c)\).

<div style="position: relative"><a id="le-tirant-deau-correspondant-m" style="position: absolute; top: -60px;"></a></div>
## Corresponding depth (m)

For a fluvial (respectively torrential water depth) \(y\), corresponding depth is the torrential (respectively fluvial) water depth for which \(H(y) = H(y_{cor})\).

<div style="position: relative"><a id="limpulsion-hydraulique-N" style="position: absolute; top: -60px;"></a></div>
## Hydraulic impulsion (N)

The impulsion \(I\) is the sum of the amount of movement and the resultant of the pressure force in a section:

$$I=\rho Q U + \rho g S y_g$$

With :

- \(\rho\) : the density of water (kg/m<sup>3</sup>)
- \(y_g\) : the distance from the centre of gravity of the section to the free surface (m)

The distance from the centre of gravity of the section to the free surface \(y_g\) can be found from the formula :

$$S.y_g = \int_{0}^{y} (y-z)B(z)dz$$

With \(y\) the depth and \(B(z)\) the width at mirror for a water depth \(z\)

Formulas of \(S.y_g\) for the different section shapes are :

- rectangular section: \(S.y_g = \frac{L.y^2}{2}\)
- trapezoidal section: \(S.y_g = \left (\frac{L}{2} + \frac{m.y}{3} \right )y^2\)
- circular section: \(S.y_g = \frac{D^3}{8}\left (\sin\theta - \frac{\sin^3\theta}{3} - \theta \cos\theta \right )\)
- parabolic section: \(S.y_g=\frac{B_b.y^{k+2}}{y_b^k(k+1)(k+2)}\)

<div style="position: relative"><a id="le-tirant-deau-conjugue-m" style="position: absolute; top: -60px;"></a></div>
## Conjugate depth (m)

For a fluvial (respectively torrential water depth) \(y\), conjugate depth is the torrential (respectively fluvial) water depth for which \(I(y) = I(y_{con})\).

<div style="position: relative"><a id="la-force-tractrice-pa" style="position: absolute; top: -60px;"></a></div>
## Tractive force (Pa)

$$ \tau_0 = \rho g R J $$
