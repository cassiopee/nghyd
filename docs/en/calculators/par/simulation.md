# Baffle fishway (or baffle fishway) simulation

This module allows to calculate different hydraulic conditions on a baffle fishway with a known geometry. This geometry may come from topographical measurements or from the [result of a baffle fishway setup](calage.md).

Supported baffle fishway types are:

- [plane baffles (Denil) fishway](theorie_plans.md);
- ["Fatou" baffle fishway](theorie_fatou.md);
- [superactive baffles fishway](theorie_suractif.md);
- [mixed / chevron baffles fishway](theorie_mixte.md).

See [all the formulas used for baffle fishways](formules.md).

This tool allows to calculate one of the following values:

- flow through the pass (m<sup>3</sup>/s);
- upstream water elevation (m);
- upstream spilling elevation (m).

Given the following mandatory parameters:

- pass type (Plane, Fatou, superactive, mixed);
- [slope (m/m)](../hsl/pente.md).
- pass width (m);
- upstream spilling or apron elevation (m);
- downstream spilling or apron elevation (m).
