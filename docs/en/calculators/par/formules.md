# Baffle fishways (or baffle fishways) calculation formulas

For calculation of:

- upstream head \(ha\);
- water level in the pass \(h\);
- flow \(Q\);
- flow velocity \(V\);
- upstream apron elevation \(Z_{r1}\);
- minimal rake height of upstream side walls \(Z_m\)

Refer to the formulas specific to each baffle fishway type:

- [plane baffles (Denil) fishway](theorie_plans.md);
- ["Fatou" baffle fishway](theorie_fatou.md);
- [superactive baffles fishway](theorie_suractif.md);
- [mixed / chevron baffles fishway](theorie_mixte.md).

## Upstream water elevation \(Z_1\)

$$Z_{1} = Z_{d1} + h_a$$

With \(Z_{d1}\) the spilling elevation of the first upstream baffle, \(h_a\) the upstream head.

## Pass length

Pass length along a water line parallel to the pass slope \(L_w\) equals

$$L_w = (Z_1 - Z_2)\dfrac{\sqrt{1 + S^2}}{S}$$

with \(Z_1\) and \(Z_2\) the upstream and downstream water elevations, \(S\) the slope.

Pass length along the slope \(L_S\) must be a multiple of the length between two baffles \(P\) rounded to the greater integer:

$$L_S = \lceil (L_w - \epsilon) / P \rceil \times P $$

With \(\epsilon\) = 1 mm to leave a margin before adding an extra baffle.

Horizontal projection of the pass length \(L_h\) thus equals:

$$L_h = \dfrac{L_S}{\sqrt{1 + S^2}} $$

## Number of baffles \(N_b\)

For plane and Fatou types:

$$N_b = L_S / P + 1$$

For superactive and mixed types:

$$N_b = L_S / P$$

## Downstream apron \(Z_{r2}\) and spilling \(Z_{d2}\) elevations:

$$Z_{r2} = Z_{r1} - \dfrac{L_S \times S}{\sqrt{1 + S^2}}$$

$$Z_{d2} = Z_{r2} + Z_{d1} - Z_{r1}$$
