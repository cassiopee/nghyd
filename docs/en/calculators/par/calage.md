# Baffle fishway (or baffle fishway) setup

This module allows to dimension a baffle fishway. Supported baffle fishway types are:

- [plane baffles (Denil) fishway](theorie_plans.md);
- ["Fatou" baffle fishway](theorie_fatou.md);
- [superactive baffles fishway](theorie_suractif.md);
- [mixed / chevron baffles fishway](theorie_mixte.md).

See [all the formulas used for baffle fishways](formules.md).

## Hydraulic setup

This tool allows to calculate one of the following values:

- flow through the pass (m<sup>3</sup>/s);
- upstream head (m);
- pass width (m) for plane and Fatou types.

Given the following mandatory parameters:

- pass type (Plane, Fatou, superactive, mixed);
- [slope (m/m)](../hsl/pente.md).

Parameter "Space between baffles (m)" is optional. When not given, its standard value is calculated. When given, if its value deviates by more than 10% from standard value, an error is generated.

## Altimetric setup

Altimetric setup parameters (upstream water level and downstream water level) are optional and allow to calculate:

- pass length in horizontal projection and following the slope
- number of baffles
- apron and spilling elevations, upstream and downstream
- rake height of upstream side walls

## Generating a baffle fishway simulation module

Results of an altimetrically setup pass may be used to generate a [baffle fishway simulation](simulation.md) module, using the ad hoc button.