# Crossability verification: Rock-ramp fishpasses

## Compound rock-ramp fishpasses

In the case of compound rock-ramp fishpasses, each apron (reminder: inclined apron are discretised) is verified like an independent rock-ramp fishpass.

If at least one apron is crossable, the pass is considered crossable.

Maximum crossable width, wich is the maximum of the sums of the widths of contiguous crossable aprons, is given at the end of the verification.

For certain species, values of certain criteria are not precisely known or are subject to slight changes depending on future feedback.

## Criteria

### Minimum water level, in m

Water level in the pass should be greater than minimum water level.

### Limit maximum flow velocity, in m/s

Maximum flow velocity in the pass should be lower than limit maximum flow velocity.

## Criteria values for predefined species groups

From _"Informations sur la Continuité Écologique - ICE, Onema 2014"_.

| ICE group | Species | Minimum water level (m) | Limit maximum flow velocity (m/s) |
|-------------------------|----------------|----------------|----------------|
| 1 | Atlantic salmon (Salmo salar)<br>Sea or river trout [50-100] (Salmo trutta) | 0.4 | 2.5 |
| 2 | Mules (Chelon labrosus, Liza ramada) | 0.3 | 2.5 |
| 3a | Gread shad (Alosa alosa) | 0.4 | 2 |
| 3b | Shad (Alosa fallax fallax) | 0.4 | 2 |
| 3c | Sea lamprey (Petromyzon marinus) | 0.15 | 2 |
| 4a | River trout or sea trout [25-55] (Salmo trutta) | 0.3 | 2 |
| 4b | River trout [15-30] (Salmo trutta) | 0.2 | 2 |
| 5 | Aspe (Aspius aspius)<br>Pike (Esox lucius) | 0.3 | 2 |
| 6 | Common grayling (Thymallus thymallus) | 0.3 | 2 |
| 7a | Common barbel (Barbus barbus)<br>Chub (Squalius cephalus)<br>Nase (Chondrostoma nasus) | 0.3 | 2 |
| 7b | River lamprey (Lampetra fluviatilis) | 0.15 | 2 |
| 8a | Common carp (Cyprinus carpio) | 0.3 | 1.5 |
| 8b | Common bream (Abramis brama)<br>Pike-perch (Sander lucioperca) | 0.3 | 1.5 |
| 8c | White bream (Blicca bjoerkna)<br>Ide (Leuciscus idus)<br>Burbot (Lota lota)<br>Perch (Perca fluviatilis)<br>Tench (Tinca tinca) | 0.3 | 1.5 |
| 8d | Common dace (Leuciscus sp but Idus) | 0.3 | 1.5 |
| 9a | Bleak (Alburnus alburnus)<br>Sprirlin (Alburnoides bipunctatus)<br>Mediterranean barbel (Barbus meridionalis)<br>Souffia (Telestes souffia)<br>Crucian carp (Carassius carassius)<br>Prussian carp (Carassius gibelio)<br>Roach (Rutilus rutilus)<br>Common rudd (Scardinius erythrophthalmus)<br>South-west european nase (Parachondrostoma toxostoma) | 0.2 | 1.5 |
| 9b | Apron (Zingel asper)<br>Cottus sp<br>Gobio sp<br>Ruffe (Gymnocephalus cernuus)<br>Brook lamprey (Lampetra planeri)<br>Stone loach ( Barbatula barbatula)<br>Spined loach (Cobitis taenia) | 0.2 | 1.5 |
| 10 | Sunbleak (Leucaspius delineatus)<br>European bitterling (Rhodeus amarus)<br>Three-spined stickleback (Gasterosteus gymnurus)<br>Ninespine stickleback (Pungitius laevis)<br>Minnows (Phoxinus sp) | 0.2 | 1.5 |

Table: List of predefined values for crossing criteria of a rock-ramp fishpass
