# Crossability verification: Baffle fishways (simulation)

For certain species, values of certain criteria are not precisely known or are subject to slight changes depending on future feedback.

## Criteria

### Incompatible and discouraged species

Species groups 3a, 3b and 7b are discouraged for crossing baffle fishways. This leads to a warning, but does not make the pass not crossable.

Species groups 8a, 8b, 8c, 8d, 9a, 9b and 10 are unable to cross baffle fishways.

### Minimum water level, in m

Water level in the pass must be greater than the minimum water level.

## Criteria values for predefined species groups

From _"Informations sur la Continuité Écologique - ICE, Onema 2014"_.

| ICE group | Species | Minimum water level on plane and Fatou baffles (m) | Minimum water level on superactive and chevron baffles (m) |
|------------|---------|---------|-----------|
| 1 | Atlantic salmon (Salmo salar)<br>Sea or river trout [50-100] (Salmo trutta) | 0.3 | 0.2 |
| 2 | Mules (Chelon labrosus, Liza ramada) | 0.25 | 0.15 |
| 3a | Gread shad (Alosa alosa) | 0.3 | 0.2 |
| 3b | Shad (Alosa fallax fallax) | 0.25 | 0.15 |
| 3c | Sea lamprey (Petromyzon marinus) | 0.1 | 0.1 |
| 4a | River trout or sea trout [25-55] (Salmo trutta) | 0.25 | 0.15 |
| 4b | River trout [15-30] (Salmo trutta) | 0.2 | 0.1 |
| 5 | Aspe (Aspius aspius)<br>Pike (Esox lucius) | 0.3 | 0.2 |
| 6 | Common grayling (Thymallus thymallus) | 0.25 | 0.15 |
| 7a | Common barbel (Barbus barbus)<br>Chub (Squalius cephalus)<br>Nase (Chondrostoma nasus) | 0.25 | 0.15 |
| 7b | River lamprey (Lampetra fluviatilis) | 0.1 | 0.1 |

Table: List of predefined values for crossing criteria of a baffle fishway
