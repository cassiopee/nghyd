# Crossability verification

## Warning

The purpose of this tool is to carry out a simple check of the compatibility
between certain fish pass sizing criteria and the passage capacities of the
target fish species. 
It may prove useful during the design phase of a facility to ensure that the
proposed design is consistent with its hydrological operating range.

However, this tool is not sufficient to fully verify the functionality of a
planned or existing system, insofar as other important dimensional and
hydraulic criteria are not taken into account by this module 
(feed flow rate and characteristics of the fish intake in terms of attractiveness,
and depending on the type of system: basin aspect ratio,
concentration of macro-roughness, bottom roughness, size of baffles, etc.).

This module allows to verify the capacity of different fish species to cross the following types of fish passes:

- [fish ladders](pab.md)
- [baffle fishways](par.md)
- [rock-ramp fishpasses](macrorugo.md)

## Principle

For each pass type, several crossability criteria are checked, expressed as threshold values (ex: minimal basin depth, for a fish ladder).

Exceeding the threshold value of a criterion leads to an explicit error mentioning the quantity concerned and the threshold value, and results in the pass not being crossable.

Some criteria such as maximal dissipated power have both an alert value (crossability is possible but not guaranteed) and a limit value (crossing the pass is impossible).

For a given pass, one can check at once the crossing capabilities of several species, over multiple modalities (variation of one or more parameters in the pass).

## Predefined species

[Several common species groups](especes_predefinies.md) are predefined: default values are associated to crossability criteria.

Default values for some of those criteria might be missing for a species group, when the latter is considered unabled to cross the pass type the criterion is related to.

## Custom species

The `Fish species characteristics` module allows to define custom values for all criteria, before running a pass verification.

If a criterion is applicable to the pass that is being checked, but no value was given, an error will be triggered when running the verification, leading to the pass being not crossable.
