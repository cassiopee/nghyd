# Crossability verification: Predefined species

From _"Informations sur la Continuité Écologique - ICE, Onema 2014"_.

| ICE group | Species |
|-------------------------|----------------|
| 1 | Atlantic salmon (Salmo salar)<br>Sea or river trout [50-100] (Salmo trutta) |
| 2 | Mules (Chelon labrosus, Liza ramada) |
| 3a | Gread shad (Alosa alosa) |
| 3b | Shad (Alosa fallax fallax) |
| 3c | Sea lamprey (Petromyzon marinus) |
| 4a | River trout or sea trout [25-55] (Salmo trutta) |
| 4b | River trout [15-30] (Salmo trutta) |
| 5 | Aspe (Aspius aspius)<br>Pike (Esox lucius) |
| 6 | Common grayling (Thymallus thymallus) |
| 7a | Common barbel (Barbus barbus)<br>Chub (Squalius cephalus)<br>Nase (Chondrostoma nasus) |
| 7b | River lamprey (Lampetra fluviatilis) |
| 8a | Common carp (Cyprinus carpio) |
| 8b | Common bream (Abramis brama)<br>Pike-perch (Sander lucioperca) |
| 8c | White bream (Blicca bjoerkna)<br>Ide (Leuciscus idus)<br>Burbot (Lota lota)<br>Perch (Perca fluviatilis)<br>Tench (Tinca tinca) |
| 8d | Common dace (Leuciscus sp but Idus) |
| 9a | Bleak (Alburnus alburnus)<br>Sprirlin (Alburnoides bipunctatus)<br>Mediterranean barbel (Barbus meridionalis)<br>Souffia (Telestes souffia)<br>Crucian carp (Carassius carassius)<br>Prussian carp (Carassius gibelio)<br>Roach (Rutilus rutilus)<br>Common rudd (Scardinius erythrophthalmus)<br>South-west european nase (Parachondrostoma toxostoma) |
| 9b | Apron (Zingel asper)<br>Cottus sp<br>Gobio sp<br>Ruffe (Gymnocephalus cernuus)<br>Brook lamprey (Lampetra planeri)<br>Stone loach ( Barbatula barbatula)<br>Spined loach (Cobitis taenia) |
| 10 | Sunbleak (Leucaspius delineatus)<br>European bitterling (Rhodeus amarus)<br>Three-spined stickleback (Gasterosteus gymnurus)<br>Ninespine stickleback (Pungitius laevis)<br>Minnows (Phoxinus sp) |

Table: List of predefined group species
