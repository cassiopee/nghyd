# Parallel structures

## Description of the calculation module

This calculation module allows to simulate the hydraulic operation of valves and thresholds placed in parallel.  All the flow laws present in Cassiopée are grouped in this module, which makes it possible in particular to easily compare the flow laws between them.

This module allows to calculate any missing parameter among them:

- Boundary conditions (water level upstream and downstream of the structures);
- The flow through the structures;
- Parameters of the structures (crest elevation, width, flow coefficient...).

The module calculates the requested parameter and displays for each structure present:

- The flow passing through the structure;
- The type of flow: under load (flow pinched under a gate), or free surface;
- The speed: flooded, partially flooded or dewatered;
- The type of jet for free surface flows: surface or submerged.

<div style="position: relative"><a id="type-de-jet" style="position: absolute; top: -60px;"></a></div>
## Jet type

For the definition of the type of jet (plunging or surface), see:  Larinier, M., 1992.  Successive basin transitions, pre-dams and artificial rivers.  Bulletin Français de la Pêche et de la Pisciculture 45-72.  <https://doi.org/10.1051/kmae:1992005>.

![Diagram of jet type](type_de_jet.png)

*Excerpt from Larinier, M., 1992.  Passages to successive basins, pre-dams and artificial rivers.  Bulletin Français de la Pêche et de la Pisciculture 45-72.  <https://doi.org/10.1051/kmae:1992005>*

The definition used in Cassiopée is as follows:

- if \(DH \geq 0.5 H1\) then the jet is plunging;
- if \(DH < 0.5 H1\) then the jet is surface.

With \(H1\), the upstream head over the weir and \(DH\) the head drop across the weir.
