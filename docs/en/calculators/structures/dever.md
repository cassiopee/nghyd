# Free flow weir stage-discharge laws

This calculation module is similar to that of the [Parallel structures](lois_ouvrages.md), except that it simulates only free flows and refine by using the upstream approach speed.

It can be used to calculate the relationship between water level upstream of a weir and flow. It is
most often used to assess the upstream-flow rating relationship at a weir or structure
evacuator of a development. The facility may have several distinct discharge levels and
Rectangular (with horizontal discharge side), triangular, semi-triangular weirs or
truncated triangles.

The classical use consists in entering the extreme levels (min/max) of the upstream water level and the calculation step
for which flow estimates are desired.

The upstream characteristics (upstream width and bed elevation) make it possible to estimate the approach speed and upstream kinetic energy, expressed in metres, and to calculate total flow from the total head at a non-negligible approach speed.

$$ V = \frac{Q}{L \times (Z_1 - Z_{lit})} $$

with \(V\) the approach speed, \(Q\) the flow, \(Z_1\) the upstream water elevation, \(Z{lit}\) the upstream river bed elevation.

$$ E_c = \frac{V^2}{2g} $$

with \(E_c\) the upstream cinetic energy in metres, and \(g\) the acceleration of gravity (9.81 m.s<sup>-2</sup>).

Head \(H\) used for flow calculation then is:

$$ H = Z_1 + E_c $$

The difficulty of the calculation lies in the fact that the flow rate to be calculated is involved in the calculation of the head. This problem is solved with the fixed point algorithm where the flow rate calculation is repeated several times by updating the head at each iteration until the calculation converges to the final value of the flow rate.

The approach speed correction coefficient \(Cv\) is then calculated by relating the flow rate obtained with the head \(H\) to the flow rate calculation with the upstream dimension \(Z_1\).
