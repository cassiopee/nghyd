# Cunge 1980 formula

This stage discharge equation is based on the equations described by Cunge in his book [^1], or in more detail in an article by Mahmood and Yevjevich [^2]. This law is a compilation of the classical laws taking into account the different flow conditions: submerged, free flow, free surface and in charge as well as the equations [CEM88(D) : Weir / Orifice (important sill)](cem_88_d.md) and [CEM88(D) : Weir / Orifice (low sill)](cem_88_v.md). However, contrary to these equations, it does not provide any continuity between free surface and in charge flow conditions. This can lead to design problems in the vicinity of this transition.

This law is suitable for a broad-crested rectangular weir, possibly in combination with a valve. The default discharge coefficient \(C_d = 1\) corresponds to the following discharge coefficients for the classical equations:

- \(C_d = 0.385\) for [the free flow weir](seuil_denoye.md).
- \(C_d = 1\) for [the submerged weir](seuil_noye.md).
- \(C_d = 1\) for [the submerged gate](vanne_noyee.md).
- \(C_c = 0.611\) for [the free flow gate](vanne_denoyee.md) with \(C_d\) calculated from \(C_c\) (See below).

## Free flow / submerged regime detection

The flow regime is in free flow as long as the downstream water level is below critical height:

$$ (Z_2 - Z_{dv}) < \dfrac{2}{3} (Z_1 - Z_{dv})$$

with \(Z_1\) upstream water elevation, \(Z_2\) downstream water elevation, et \(Z_{dv}\) apron or sill elevation of the hydraulic structure.

## Free flow / in charge flow detection

The water level at the gate when the gate is open is considered here to be equal to:

- the critical height in the case of a free flow regime;
- the downstream water height in the case of a submerged regime.

The flow becomes in charge when the gate touches the surface of the water at this point.

In free flow, the flow is in charge when:

$$ W \leq \dfrac{2}{3} (Z_1 - Z_{dv})$$

In submerged flow, the condition becomes:

$$ W \leq Z_2$$

## Discharge equations

The free flow gate equation uses a fixed contraction coefficient \(C_c\) with:

\(C_d = \frac{C_c}{\sqrt{1 + C_c W / h_{am}}}\)

For all other flow regimes, used equations here are the following as they can be used independently:

|        | Free surface | In charge |
|--------|---------------|-----------|
| Free flow | [Free flow weir](seuil_denoye.md) | [free flow gate](vanne_denoyee.md) |
| Submerged | [Submerged weir](seuil_noye.md) | [Submerged gate](vanne_noyee.md) |

[^1]: Cunge, Holly, Verwey, 1980, "Practical aspects of computational river hydraulics", Pitman, p. 169 for weirs and p. 266 for gates.

[^2]: Mahmood K., Yevjevich V., 1975, "Unsteady flow in open channels, Volume 1 and 2", Water resources publications, Fort Collins, USA, 923 p.
