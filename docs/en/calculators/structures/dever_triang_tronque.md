# Truncated triangular weir formula

TT1 caracterized by:

* \(C_d\): discharge coefficient
* \(Z_d\): triangle's lower overflow elevation
* \(Z_t\): triangle's higher overflow elevation
* \(B/2\): half-opening of the triangle

## Formula

### for \(Z_{am} \leq Z_t\)

$$Q = C_d \frac{B}{2 (Z_t - Z_d)} \left ( Z_{am} - Z_d \right )^{2.5}$$

### for \(Z_{am} > Z_t\)

$$Q = C_d \frac{B}{2 (Z_t - Z_d)} \left ( \left ( Z_{am} - Z_d \right )^{2.5} - \left ( Z_{am} - Z_t \right )^{2.5} \right )$$

Thin wall weir: \(C_d\) = 1.37

Thick weir without contraction (rounded \(r > 0.1 * h1\)): \(C_d\) = 1.27

Triangular profile weir: (1/2 upstream, 1/2 or 1/5 downstream): \(C_d\) = 1.68 and 1.56
