# Free weir formula

The formula is derived from the original formula of Poleni (1717).

In a free flow, the flow rate depends only on the upstream water level \(h_{amont}\):

$$Q = Cd \sqrt{2g} L h_{amont}^{3/2}$$

With:

* *Q* the flow in m<sup>3</sup>/s
* *C<sub>d</sub>* the discharge coefficient
* \(g\) the acceleration of gravity 9.81 m/s<sup>2</sup>
* *L* the width of the weir in m
* *h<sub>amont</sub>* the upstream water level above the crest of the weir in m

A flow coefficient value \(C_d = 0.4\) is generally a good approximation for a rectangular weir. For more complex weir shapes (trapezoidal, circular...) or to take into account the characteristics of the longitudinal profile (thin-crested weir, thick-crested weir), one can refer to the CETMEF weir leaflet (CETMEF, 2005).

*[CETMEF (2005). Notice sur les déversoirs : synthèse des lois d’écoulement au droit des
seuils et déversoirs. Compiègne : Centre d’Études Techniques Maritimes Et Fluviales.
89 p.](http://www.side.developpement-durable.gouv.fr/EXPLOITATION/DEFAULT/doc/IFD/IFD_REFDOC_0513410/notice-sur-les-deversoirs-synthese-des-lois-d-ecoulement-au-droit-des-seuils-et-deversoirs)*
