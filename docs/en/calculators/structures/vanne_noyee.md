# Submerged sluice gate

![Submerged gate diagram](vanne_noyee_schema.png)

*Excerpt from Baume, J.-P., Belaud, G., Vion, P.-Y., 2013. Hydraulique pour le génie rural, Formations de Master, Mastère Spécialisé, Ingénieur agronome. UMR G-EAU, Irstea, SupAgro Montpellier.*

## Submerged sluice gate equation

\(Q = C'_d LW \sqrt{2g}\sqrt{h_{am} - h_{av}}\)

Coefficient \(C'_d\) is around 0.8.
