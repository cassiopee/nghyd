# Stage-discharge equations list

| Structure typology                                    | Structure geometry   | Structure type                           | Flow regimes     | Name of equation                            | Default flow coefficient  | Modules                                                                         |
|-------------------------------------------------------|----------------------|------------------------------------------|------------------|---------------------------------------------|---------------------------|---------------------------------------------------------------------------------|
| Weir                                                  | Rectangular          | Sharp-crested weir (notch)               | Free             | [Poleni](./seuil_denoye.md)                 | 0.4                       | Parallel structures, free flow weir stage-discharge laws                        |
|                                                       |                      | Sharp-crested weir (slot)                | Submerged        | [Rajaratnam](./seuil_noye.md)               | 0.9                       | Parallel structures                                                             |
|                                                       |                      | Sharp-crested weir (slot)                | Submerged        | [Larinier slot](./fente_noyee.md)           | to be defined by designer | Parallel structures, cross walls, downwall                                      |
|                                                       |                      | Sharp-crested weir (notch)               | Submerged / free | [Kindsvater-Carter & Villemonte](./kivi.md) | ⍺ = 0.4; ß = 0.001        | Parallel structures                                                             |
|                                                       |                      | Sharp-crested weir (notch)               | Submerged / free | [Villemonte](./villemonte_1947.md)          | 0.4                       | Parallel structures, cross walls, downwall                                      |
|                                                       |                      | Sharp-crested regulated weir (notch)     | Submerged / free | [Villemonte](./villemonte_1947.md)          | 0.4                       | Downwall                                                                        |
|                                                       |                      | Sharp-crested regulated weir (slot)      | Submerged        | [Larinier slot](./fente_noyee.md)           | To be defined by designer | Downwall                                                                        |
|                                                       | Triangular           | Sharp-crested weir (notch)               | Submerged / free | [Villemonte](./dever_triang.md)             | 1.36                      | Parallel structures, free flow weir stage-discharge laws, cross walls, downwall |
|                                                       |                      | Broad-crested weir (notch)               | Submerged / free | [Bos](./dever_triang.md)                    | 1.36                      | Parallel structures, free flow weir stage-discharge laws, cross walls, downwall |
|                                                       | Triangular truncated | Sharp-crested weir (notch)               | Submerged / free | [Villemonte](./dever_triang_tronque.md)     | 1.36                      | Parallel structures, free flow weir stage-discharge laws, cross walls, downwall |
| Orifice / Sluice gate handling free surface weir flow | Rectangular          | Broad-crested weir (notch) / orifice     | Submerged / free | [Cemagref-D](./cem_88_d.md)                 | 0.4                       | Parallel structures, cross walls, pre-dams                                      |
|                                                       | Rectangular          | Broad-crested weir (notch) / orifice     | Submerged / free | [Cunge](./cunge_80.md)                      | 1                         | Parallel structures, pre-dams                                                   |
|                                                       | Rectangular          | Broad-crested weir (notch) / bottom gate | Submerged / free | [Cemagref-V](./cem_88_v.md)                 | 0.6                       | Parallel structures, downwall, pre-dams                                         |
| Orifice / Sluice gate                                 | Rectangular          | Sluice gate                              | Free             | [Free flow sluice gate](./vanne_denoyee.md) | 0.6                       | Parallel structures                                                             |
|                                                       | Rectangular          | Sluice gate                              | Submerged        | [Submerged sluice gate](./vanne_noyee.md)   | 0.8                       | Parallel structures                                                             |
|                                                       | Undefined            | Orifice                                  | Submerged        | [Free flow orifice](./orifice_denoye.md)    | 0.7                       | Parallel structures, cross walls, downwall                                      |
|                                                       | Undefined            | Orifice                                  | Free             | [Submerged orifice](./orifice_noye.md)      | 0.7                       | Parallel structures                                                             |

Table: Stage-discharge equations list

## Sharp-crested or broad-crested weir ?

Extract from CETMEF, 2005. Note on weirs : synthesis of flow laws at the right of weirs and spillways. Centre d'Études Techniques Maritimes Et Fluviales, Compiègne.

> The type of weir is related to the flow at the right of the structure. 
> 
> Indeed, the more the breadth of the crest of the weir is negligible compared to the upstream water height above it, the more the weir appears transparent to the flow and thus the sharper the crest of the weir appears. 
>
> On the other hand, the closer the upstream water line is to the weir crest, the greater the width of the weir appears in relation to the breadth of the water flowing over it and therefore the broader the weir crest appears. 
> A weir in a river thus belongs to one of the three following categories: 
> - sharp-crested weir
> - broad-crested weir
> - weir with undefined crest 
>
> In order to determine the type of weir studied, the following conditions must be verified:
> - if $C < \frac{H_1}{2}$, then the threshold is sharp-crested;
> - if $C > \frac{2H_1}{3}$, then the threshold is broad-crested.
