# Multi-module solver

The multi-module solver allows to calculate the value of an input parameter of a module depending on a calculation chain made up of several modules for which a target value for an output calculated parameter is sought.

The diagram below shows the example of a calculation chain comprising two modules. The module `x` takes the parameter `Input x` and calculates the parameter `Output x`. The `y` module takes the `Input y` parameter and calculates the `Output y` parameter. The `Input y` parameter is linked to the `Output x` parameter. The problem to be solved by the solver is to get the value of the `Input x` to obtain a target value in `Output y`.

<div class="mermaid">
    graph LR
    Ix([Input x])
    x[Module x]
    Ox([Output x / Input y])
    y[Module y]
    Oy([Output y])
    Ix --> x
    x --> Ox
    Ox --> y
    y --> Oy
    Oy -..->|Input x for a target value Output y ?| Ix
</div>

To solve this problem, the characteristics of the target parameter must be defined which include:

- The module and its calculated parameter (here the module `y` calculating `Output y`);
- The target result which can be the parameter calculated by the module or one of its complementary results (Here `Output y`);
- The desired target value.

It is also necessary to define the characteristics of the searched parameter:

- The searched parameter is to be chosen from the list of input parameters of the modules of the session (Here the `Input x` parameter of the `x` module);
- An initial value of this parameter to start the calculation.

The initial value chosen must be close enough to the solution because there is no guarantee that the function resulting from the sequence of calculations is continuous and monotonous.

Examples of the use of the multi-module solver are given in the example sessions "Channel flow with structures" and "Critical slope of a channel". The notes of these sessions describe the sequence of the modules and the use of the solver.
