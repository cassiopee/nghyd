# Operators and trigonometric functions

The basic mathematical operators and functions provided by Cassiopeia make it possible above all to facilitate the link between the results of one calculation module and the input into another in the case of a sequence of calculations between several modules. The provided example "Weir jet length" shows the use of the "Linear function" module.

<div style="position: relative"><a id="fonction-affine" style="position: absolute; top: -60px;"></a></div>
## Linear function

The linear function module solves the equation of a line:

$$ y = a x + b $$

Three parameters must be entered and the module calculates the missing parameter.

<div style="position: relative"><a id="somme-et-produit-de-puissances" style="position: absolute; top: -60px;"></a></div>
## Sum and product of powers

This module allows to write an equation summing powers in the form \(a x ^ n\) with \(a\), \(x\), and \(n\) of real numbers.

In the case of a sum the equation solved by the module is written:

$$ y = \sum_{i=1}^{k} a_i x_i ^ {n_i} $$

In the case of a product the equation solved by the module is written:

$$ y = \prod_{i=1}^{k} a_i x_i ^ {n_i} $$

All parameters must be entered except the last one which is the value calculated in the equation.

<div style="position: relative"><a id="fonction-trigonometrique" style="position: absolute; top: -60px;"></a></div>
## Trigonometric function

This module allows to calculate the value of a trigonometric function or its inverse.

The equation solved by this module is written:

$$ y = f(x) $$

With \(f\), a trigonometric function and \(x\) an angle in degrees or radian.

The "Operation" parameter allows to choose the operator among the available functions: cos, sin, tan, cosh, sinh and tanh. The "Unit" parameter is used to choose between degree and radian.
