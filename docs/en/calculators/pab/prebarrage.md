# Pre-barrages

Pre-barrages are a type of basin pass used for crossing low obstacles. They are made up of several walls or thresholds dividing the fall into several large basins in parallel and in series.

## General presentation

This module calculates the flow distribution and the water line of the basins of a pre-barrage. Two calculation possibilities are offered: calculation of the channel flow into knowing the upstream and downstream water level, calculation of the upstream water level knowing the channel flow and the downstream water level.

The input of the pre-barrage parameters is divided into two parts:

- The composition of the pre-barrage in basins and the walls connecting the basins, on the first part of the screen.
- The input of the pre-barrage parameters: boundary conditions, basin parameters and walls parameters, on the second part of the screen.

It is possible to vary any parameter in order to obtain a series of results.

The pre-barrage composition scheme can be displayed in full screen and exported to PNG image format.

## Pre-barrage composition

At its creation, the pre-barrage consists of an upstream boundary condition, a basin, a downstream boundary condition and walls joining these three elements.

The buttons "Add new basin" and "Add new wall" are used to add respectively a basin and a wall to the pre-barrage. When adding a wall, you are asked to which boundary conditions or basins the wall is connected. When entering the connections to the walls, the user must follow the numbering of the basins, which necessarily respects an upstream-downstream order.

A toolbar allows to duplicate and delete a selected wall or basin and to change the order of a basin.

## Entering parameters of the pre-barrage elements

The entry of the parameters of the upstream or downstream boundary condition, of a basin or a wall is made by selecting the desired element in the diagram in the first part of the screen.

The entry of the elements is then carried out as for any Cassiopée module. For the entry of walls, refer to the module [Parallel structures](../structures/lois_ouvrages.md).

## Launching the calculation

The calculate button is activated from the moment all the basins are connected by walls from upstream to downstream of the pre-barrage and all the parameters have been entered.

## Visualization of the results

After the calculation, the interface displays two tabs "Input" and "Results" that allow to navigate between the pre-barrage input and the calculation results.

If the calculation produces a series of results, a drop-down list allows you to choose which result to display for each parameter that has changed.

The first part of the screen shows a synoptic of the pre-barrage with the flows and falls at the walls, the average power dissipated and the average depth at the basins, and the flow and water levels at the upstream and downstream boundary conditions. To view the numerical results at basin level, the user must click on one of these items on the diagram. To view the numerical results at a wall, click on the corresponding wall on the diagram.

An "Export all results" button allows you to retrieve a table in Excel format containing the results of boundary conditions, basins and walls for the whole calculation series.
