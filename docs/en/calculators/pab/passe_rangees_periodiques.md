# Nature-like fish passage with riprap in periodic rows

It is stated on p.16 of the design guide for nature-like fishways
(Larinier et al., 2006) that rock-rigged fishways in periodic rows are similar
to fish ladders, and their design criteria are identical to those of.

It is therefore possible to use the various tools in the "[fish ladder](pab.md)" module
module to design this type of device.
In particular, the "[cloisons](cloisons.md)" tool can be used to enter the various
dimensional characteristics of weirs (widths, overflow dimensions) and pseudo-basins
pseudo-basins (length, width, mid-radius), which can be used to indirectly
indirectly retrieve the sill height (\(p\)) and useful block height (\(k\)) parameters
in the generated geometry table.
Porosity (ratio between the free passage between the blocks and the total width
width of the pseudo-wall) is not entered directly, but this parameter can be
be deduced and examined elsewhere if necessary.

According to this principle, each overhanging portion of the partition must be entered
as an indentation or slot, as well as the dimension of the top of the blocks if the latter
are flooded within the operating range of the device.
In the case of notches, particular attention should be paid to the type of
(thick or thin) in order to accurately reflect the conditions of flooding by the
downstream of the weir.

It is also possible to use the "[pre-dams](prebarrage.md)" tool (the equations
implemented are the same as in the "[cloisons](cloisons.md)" tool),
if the device is affected by complex feed modes,
with, for example, overflows from the weir to different basins in the system
(a less frequent case for a riprap pass in periodic rows).

An example of a pass can be accessed directly via [this link](https://cassiopee.g-eau.fr/#/loadsession/app%2Fexamples%2Fpasse_rangees_periodiques.json).

## References

Larinier, Michel, Dominique Courret, et Peggy Gomes. 2006. « Guide technique pour la conception des passes à poissons “naturelles” ». Rapport GHAPPE RA. Compagnie Nationale du Rhône / Agence de l’Eau Adour Garonne. [http://dx.doi.org/10.13140/RG.2.1.1834.8562](http://dx.doi.org/10.13140/RG.2.1.1834.8562)
