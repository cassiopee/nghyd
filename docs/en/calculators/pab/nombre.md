# Fish ladders: Number of falls

This tool is an aid for the pre-dimensioning of a fish ladder: it allows to calculate the missing value of the three quantities:

- the total fall (\(\Delta H_T\)) in m;
- the fall between basins (\(\Delta H\)) in m;
- the number of falls (\(N\));

as well as the residual fall (\(\Delta H_R\)) in m.

## Formula

$$\Delta H_T = \left ( \Delta H * N \right ) + \Delta H_R $$
