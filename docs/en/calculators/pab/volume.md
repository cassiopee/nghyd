# Fish ladders: Power dissipation

This tool is an aid for the pre-dimensioning of a fish ladder: it allows to calculate the missing value of the four quantities:

- the fall between the basins (\(\Delta H\)) in m;
- the flow rate (\(Q\)) in m<sup>3</sup>/s;
- the volume of the basins (\(V\)) in m<sup>3</sup>;
- the power density (\(P_v\)) in W/m<sup>3</sup>.

The formula for calculating the power dissipation is then:

$$P_v = \frac{\rho \mathrm{g} Q \Delta H}{V}$$

with:

-  \(\rho\): the density of water;
- \(\mathrm{g}\): the acceleration of the Earth's gravity = 9.81 m.s<sup>-2</sup>
