# Cross walls

This tool, which is similar to the [Parallel Structures](../structures/lois_ouvrages.md) tool, is an aid to the hydraulic pre-dimensioning of a fish pass: it
is most often used for the dimensioning of notches, slots, orifices, etc.
characterizing the walls of a pass as well as for the setting in altitude of the notches,
slots and apron of the upstream basin of a pass.

It allows to calculate the missing value of the 7 values characterizing the fall, the surface
of the submerged orifice, the width of the slot, the load on the slot, the width of the notch,
the load on the notch and the flow rate.

Mandatory data to be provided are the dimensions of the basins (width and length) and the
average draught in metres. These data associated with the fall between basins allow us to calculate [the power dissipation](volume.md).

Once the module is calculated, the tool proposes to create a basin pass from this cross wall by specifying the upstream water elevation, the number of falls in the pass and the downstream water elevation.

## Hydraulic structures that can be part of the cross wall

The tool allows you to place one or more structures in parallel among the following types of structures:

### Submerged orifice

![Submerged orifice diagram](../structures/orifice_noye_schema.png)

*Excerpt from Larinier, M., Travade, F., Porcher, J.-P., Gosset, C., 1992. Fish passage: expertise and design of crossing structures. CSP. (page 94)*.

The submerged orifice equation is described on [the submerged orifice formula page](../structures/orifice_noye.md).

### Submerged Slot

![Schematic of the submerged slot](../structures/fente_noyee_schema.png)

*Excerpt from Larinier, M., Travade, F., Porcher, J.-P., Gosset, C., 1992. Fish passage: expertise and design of crossing structures. CSP. (page 94)*.

The submerged slot equation is described on [the sumberged slot formula page](../structures/fente_noyee.md).

### Notch

![Notch diagram](../structures/echancrure_schema.png)

*Excerpt from Larinier, M., Travade, F., Porcher, J.-P., Gosset, C., 1992. Fish passage: expertise and design of crossing structures. CSP. (page 94)*.

The equation used for the notch is [that of Kindsvater-Carter and Villemonte](../structures/kivi.md).
