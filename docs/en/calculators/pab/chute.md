# Fish ladders: Fall

This tool is an aid for the pre-dimensioning of a fish ladder: it allows to calculate the missing value of the three quantities:

- the upstream dimension (\(Z_1\)) in m;
- the downstream dimension (\(Z_2\)) in m;
- the fall (\(\Delta H\)) in m;

## Formula

$$\Delta H = Z_1 - Z_2$$
