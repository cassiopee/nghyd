# Fish ladders: Dimensions

This tool is an aid for dimensioning the basins of a pass: it allows to
calculate the missing value of the four quantities:

- the volume of water (\(V\)) in m<sup>3</sup>;
- the mean draught \((Y_{mean}\)) in m;
- the length of the basin (\(L\)) in m;
- the width of the basin (\(B\)) in m.

The calculation is carried out by applying the formula:

$$V = Y_{mean} \times L \times B$$
