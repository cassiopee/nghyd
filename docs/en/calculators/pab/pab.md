# Fish ladder

## General presentation

This module calculates the water line of a fish ladder with successive basins. Two calculation possibilities are offered: calculation of the inflow into the channel from the upstream and downstream water levels, calculation of the upstream water level from the inflow into the channel and the downstream water level.

The creation of a channel can be done from scratch or from a wall model created with the [Cross walls tool](cloisons.md).

Input parameters are divided into two steps:

- The hydraulic parameters which include the boundary conditions (water level upstream and downstream of the fish ladder) and the inflow into the fish ladder.
- The parameters of the basins, which include the geometry of the basins and the parameters of the hydraulic structures constituting the walls.

It is possible to vary one or two hydraulic parameters so as to obtain a series of results for several boundary conditions or flows.

## Input of the pass geometry

The geometry table has a line for each basin and a final line to describe the downstream wall. For each basin, the parameters present are

- length of the basin (m)
- basin width (m)
- attraction flow (m<sup>3</sup>/s)
- Mid-basin invert rating (m)
- Upstream invert elevation (m)

To these are added the parameters of the structures of the upstream wall of each basin.

### Modification of the pass structure

The structure of the pass, i.e. the number of basins or the number of structures in a wall can be changed using the toolbar at the top right of the table:

![Edit toolbar for the geometry of the pass](pab_barre_outils_edition.png)

This bar is activated when you select:

- a basin (which includes its upstream wall) or the downstream wall by clicking on the first column of a row
- a structure by clicking on an uneditable cell of a structure in a wall
- all structures in a column of the table by clicking on the header of the column to be selected

It is also possible to expand an existing selection by pressing the [Ctrl] key to add a new item to the selection, or by pressing the [Shift] key to expand the selection between two rows or two columns.

Depending on the elements selected in the table, the toolbar indicates whether the proposed actions will apply to the selected basins or structures.

The toolbar consists of the following buttons:

1. Number of basins or structures to be added or duplicated
1. Add *n* basins or *n* structures with *n* the number indicated on the first button.
1. Duplicate *n* basins or *n* structures with *n* the number indicated on the first button.
1. Delete selected basins or structures
1. 1. Move the selected pools (or structures) upwards (or to the left).
1. 1. Move the selected basins (or structures) downwards (or to the right).

### Advanced modification of the pass geometry

The selection of basins or structures gives access to a button "Modify values" which allows to modify a parameter among all the variables of the selected cells in the geometry table.

For this variable to be modified, one can:

- define a fixed value;
- apply a delta;
- calculate an interpolation between the basin selected upstream and downstream.

### The downstream wall

The downstream wall, in addition to the laws of structures available on the walls, allows the use of a "lift gate" in the form of two laws:

- Regulated notch (Villemonte 1957);
- Regulated submerged slot (Larinier 1992).

The lift gate is a structure where the crest of the weir is regulated to maintain a setpoint waterfall between the last basin and the downstream water level. In addition to the conventional parameters of the flow laws, it includes:

- a *DH* setpoint waterfall (m)
- a minimum crest elevation *minZDV* (m);
- a maximum crest elevation *maxZDV* (m);

During the calculation, if the calculated crest elevation is less than *minZDV* (resp. greater than *maxZDV*), the value is locked at *minZDV* (resp. *maxZDV*) and a warning appears in the calculation log.

## Calculation results

The results are presented in the form of a summary table of hydraulic calculations for all basins and walls. It contains all the data calculated by the modules [Cross walls](cloisons.md) and [Power dissipation](volume.md).

Two graphs are present:

- A profile along the channel with the apron elevation of the basins and the water elevation in each basin.
- A general graph allowing the selection of any parameter from the result table in abscissa and ordinate.

If several results are available due to the variation of one or two hydraulic parameters of the pass, all calculated water lines are displayed in the long profile, and a drop-down list allows to select the result to be displayed in the generalist table and graph.
