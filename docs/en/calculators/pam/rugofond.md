# Rough bottom ramp

Rough-bottom ramps are generally made up of blocks of relatively uniform dimensions
arranged one against the other and forming a channel of varying slope.

![Schematic longitudinal section of a rough bottom ramp](rugofond_schema.png)

## Warning about the passability of the structure

In the absence of pools and protruding elements in the flow, these ramps do not
provide rest areas for upstream migration fishes. Fish therefore have to swim
over them in one go, generally using their maximum swimming speed. The
crossability of a rough bottom ramp depends on the flow velocities, the
water heights above the crest of the embankments, the ‘quality’ of the water
surface (absence of jumps and slumps making it difficult, if not impossible, for
fish to swim), and also the length of the ramp that the fish have to cover
before becoming tired. This is why rough bottom ramps are not
considered to be crossing devices as such (Larinier et al. 2006). This type of
device can be useful (1) to make a sill directly passable by species with the
best swimming ability (salmonids, even shad and lampreys) or (2) to form
pre-dams combining short ramps with reduced falls and intermediate basins.

## Calculation method


### <a name = "apron_type"></a>Inclined apron

The inclined apron is calculated by discretizing the pass width into several
horizontal aprons whose number is determined by the parameter “Number of
flow slices".

### Relationship between upstream head on the crest and flow rate

The relationship between the upstream load and the flow is represented by a [formula of
free weir](../structures/seuil_denoye.md).

### Relationship between water depth, flow velocity and uniform flow

From the flow supplied or calculated at the inlet, the head of water and the flow velocity in the channel are calculated using [the Manning-Strickler formula
for a uniform flow](../hsl/regime_uniforme.md) for a rectangular cross section
equivalent to that of the ramp.

<a name="coef_a"></a>Strickler coefficient is estimated from the
\(D_{65}\) size of riprap:

$$ K_s = \dfrac{a}{{D_{65}}^{1/6}} $$

With \(a\) the **Strickler correction coefficient** function of how the riprap is placed and the level at which they are joined (Larinier et al., 2006):

 - spilt rock, not grouted&nbsp;: 21
 - riprap in a compact arrangement without jointing&nbsp;: 15.5
 - 30% grouting&nbsp;: 16.7
 - 50% grouting&nbsp;: 18

## References

Larinier, M., J. Chorda, et O. Ferlin. 1995. « Le franchissement des seuils en
enrochements par les poissons migrateurs : étude expérimentale ». GHAAPPE
95/05-HYDRE 161. irstea. [https://hal.inrae.fr/hal-02575575](https://hal.inrae.fr/hal-02575575)

Larinier, Michel, Dominique Courret, et Peggy Gomes. 2006. « Guide technique
pour la conception des passes à poissons “naturelles” ». Rapport GHAPPE RA.
Compagnie Nationale du Rhône / Agence de l’Eau Adour Garonne.
[http://dx.doi.org/10.13140/RG.2.1.1834.8562](http://dx.doi.org/10.13140/RG.2.1.1834.8562)
