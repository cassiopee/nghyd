# Compound rock-ramp fishpasses

This calculation module allows to calculate the flow passing through a rock-ramp pass called "complex" because it has an inclined apron or multiple aprons.

## General characteristics

The parameters to be entered are the same as for [the so-called "simple" macro-roughness pass](macrorugo.md). Concerning the apron of the pass two choices are offered:

- Multiple aprons: it is possible to create, duplicate, delete, change the order of as many aprons as necessary. For each apron, the parameters to be entered are: the width of the apron and the dimension of the apron upstream of the pass.
- The inclined apron: in addition to the width of the apron, the right and left sides of the apron must be entered upstream of the pass.

The calculated data are the same as for [the so-called "simple" macro-roughness pass](macrorugo.md). The results display the different data for each apron and the graph allows you to view this data for each apron (transverse profile). In the event that at least one of the calculation parameters varies, the results are available individually via a drop-down list.

## Inclined apron case

The calculation of an inclined apron pass consists in discretizing the pass into several horizontal aprons. The width of the created aprons is fixed at the distance between two blocks with an adjustment of the uppest apron to obtain the total width of the pass. It is possible to edit the created aprons by selecting "Multiple aprons" after performing a calculation with an inclined apron.
