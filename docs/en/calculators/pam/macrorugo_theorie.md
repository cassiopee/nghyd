# Calculation of the flow rate of a rock-ramp pass

The calculation of the flow rate of a rock-ramp pass corresponds to the implementation of the algorithm and the equations present in Cassan et al. (2016)[^1].

## General calculation principle

![Organigramme de la méthode de calcul](cassan2016_flow_chart_design_method.png)

*After Cassan et al., 2016[^1]*

There are three possibilities:

- the submerged case when \(h \ge 1.1 \times k\)
- the emergent case when \(h \le k\)
- the quasi-emergent case when \(k < h < 1.1 \times k\)

In the quasi-emergent case, the calculation of the flow corresponds to a transition between emergent and submerged case formulas:

$$Q = a \times Q_{submerge} + (1 - a) \times Q_{emergent}$$

with \(a = \dfrac{h / k - 1}{1.1 - 1}\)

## Submerged case

The calculation is done by integrating the velocity profile in and above the macro-roughnesses.
The calculated velocities are the temporal and spatial averages per plane parallel to the bottom.

In macro-roughnesses, velocities are obtained by double averaging the Navier-Stokes equations in uniform regime with a mixing length model for turbulence.

Above the macro-roughnesses, the classical turbulent boundary layer analysis is maintained.
The velocity profile is continuous at the top of the macro-roughnesses and is dependent on the boundary conditions set by the hydraulics:

- velocity at the bottom (without turbulence) in m/s:

$$u_0 = \sqrt{2 g S D (1 - \sigma C)/(C_d C)}$$

- total shear stress at the top of the roughnesses in m/s:

$$u_* = \sqrt{gS(h-k)}$$

The average bed velocity is given by integrating the flows between and above the blocks:

$$\bar{u} = \frac{Q_{inf} + Q_{sup}}{h}$$

with respectively \(Q_{inf}\) and \(Q_{sup}\) the unit flows for the part in the canopy and the part above the canopy.

### Calculation of the unit flow rate *Q<sub>inf</sub>* in the canopy

The flow in the canopy is obtained by integrating the velocity profile (Eq. 9, Cassan et al., 2016):

$$Q_{inf} = \int_{0}^1 u(\tilde{z}) d \tilde{z}$$

with

$$u(\tilde{z}) = u_0 \sqrt{\beta \left( \frac{h}{k} -1 \right) \frac{\sinh(\beta \tilde{z})}{\cosh(\beta)} + 1}$$

with

$$\beta = \sqrt{(k / \alpha_t)(C_d C k / D)/(1 - \sigma C)}$$

with

$$C_d = C_{x} f_{h_*}(h_*)$$

and \(\alpha_t\) obtained by solving the following equation:

$$\alpha_t u(1) - l_0 u_* = 0$$

with

$$l_0 = \min \left( s, 0.15 k \right)$$

with

$$s = D \left( \frac{1}{\sqrt{C}} - 1 \right)$$

### Calculation of the unit flow *Q<sub>sup</sub>* above the canopy

$$Q_{sup} = \int_k^h u(z) dz$$

with (Eq. 12, Cassan et al., 2016)

$$u(z) = \frac{u_*}{\kappa} \ln \left( \frac{z - d}{z_0} \right)$$

with (Eq. 14, Cassan et al., 2016)

$$z_0 = (k - d) \exp \left( {\frac{-\kappa u_k}{u_*}} \right)$$

and (Eq. 13, Cassan et al., 2016)

$$ d =  k - \frac{\alpha_t u_k}{\kappa u_*}$$

which gives

$$Q_{sup} = \frac{u_*}{\kappa} \left( (h - d) \left( \ln \left( \frac{h-d}{z_0} \right) - 1\right)  - \left( (k - d) \left( \ln \left( \frac{k-d}{z_0} \right) - 1 \right) \right) \right)$$

## Emerging case

The calculation of the flow rate is done by successive iterations which consist in finding the flow rate value allowing to obtain the equality between the flow velocity \(V\) and the average velocity of the bed given by the equilibrium of the friction forces (bottom + drag) with gravity:

$$u_0 = \sqrt{\frac{2 g S D (1 - \sigma C)}{C_d f_F(F) C (1 + N)}}$$

with

$$N = \frac{\alpha C_f}{C_d f_F(F) C h_*}$$

with

$$\alpha = 1 - (a_y / a_x \times C)$$

## Formulas used

### Bulk velocity *V*

$$V = \frac{Q}{B \times h}$$

### Average speed between blocks *V<sub>g</sub>*

From Eq. 1 Cassan et al (2016)[^1] and Eq. 1 Cassan et al (2014)[^2]:

$$V_g = \frac{V}{1 - \sqrt{(a_x/a_y)C}}$$

<div style="position: relative"><a id="coefficient-de-trainee-dun-bloc-cd0" style="position: absolute; top: -60px;"></a></div>
### Drag coefficient of a single block *C<sub>d0</sub>*

\(C_{d0}\) is the drag coefficient of a block considering a single block
infinitely high with \(F << 1\) (Cassan et al, 2014[^2]).


| Block shape | Cylinder | "Rounded face" shape | Square-based parallelepiped | "Flat face" shape |
|:------------|:--------:|:--------------------:|:-----------------------:|:--------------------:|
| | ![Cylinder](bloc_cylindre.png) | !["Rounded face" shape](bloc_face_arrondie.png) | ![Square-based parallelepiped](bloc_base_carree.png) | !["Flat face" shape](bloc_face_plate.png) |
| Value of \(C_{d0}\) | 1.0 | 1.2-1.3 | 2.0 | 2.2 |

When establishing the statistical formulae for the 2006 technical guide (Larinier et al. 2006[^4]), the definition of the block shapes to be tested was based on the use of quarry blocks with neither completely round nor completely square faces.
The so-called "rounded face" shape was thus not completely cylindrical, but had a trapezoidal bottom face (seen in plan).
Similarly, the "flat face" shape was not square in cross-section, but also had a trapezoidal bottom face.
These differences in shape between the "rounded face" and a true cylinder on the one hand, and the "flat face" and a true parallelepiped with a square base on the other hand, result in slight differences between them in the shape coefficients \(C_{d0}\).


### Block shape coefficient *σ*

Cassan et al. (2014)[^2], et Cassan et al. (2016)[^1] define \(\sigma\) as the ratio between the
block area in the \(x,y\) plane and \(D^2\).
For the cylindrical form of the blocks, \(\sigma\) is equal to \(\pi / 4\) and for a square block, \(\sigma = 1\).

### Ratio between the average speed downstream of a block and the maximum speed *r*

The values of (\r\) depends on the block shapes (Cassan et al., 2014[^2] et Tran et al. 2016 [^3]):

- round : \(r_Q=1.1\)
- "rounded face" shape : \(r=1.2\)
- square-based parallelepiped : \(r=1.5\)
- "flat face" shape : \(r=1.6\)

Cassiopée implements a formula depending on \(C{d0}\):

$$ r = 0.4 C_{d0} + 0.7 $$

### Froude *F*

$$F = \frac{V_g}{\sqrt{gh}}$$

### Froude-related drag coefficient correction function *f<sub>F</sub>(F)*

If \(F < 1\) (Eq. 19, Cassan et al., 2014[^2]):

$$f_F(F) = \min \left( \frac{r}{1- \frac{F_{g}^{2}}{4}}, \frac{1}{F^{\frac{2}{3}}} \right)^2$$

otherwise \(f_F(F) = 1\) because a torrential flow upstream of the blocks is theoretically impossible because of the hydraulic jump caused by the downstream block.

### Maximum speed *u<sub>max</sub>*

According to equation 19 of Cassan et al, 2014[^2] :

$$ u_{max} = V_g \sqrt{f_F(F)} $$

### Drag coefficient correction function linked to relative depth *f<sub>h\*</sub>(h<sub>\*</sub>)*

The equation used in Cassiopeia differs slightly from equation 20 of Cassan et al. 2014[^2] and equation 6 of Cassan et al. 2016[^1].
This formula is a fit to the experimental measurements on circular blocks used in Cassan et al. 2016[^1]:

$$ f_{h_*}(h_*) = (1 + 1 / h_*^{2}) $$

### Coefficient of friction of the bed *C<inf>f</inf>*

If \(k_s < 10^{-6} \mathrm{m}\) then we use Blasius' formula

$$C_f = 0.3164 / 4 * Re^{-0.25}$$

with

$$Re = u_0 \times h / \nu$$

Else (Eq. 3, Cassan et al., 2016 d'après Rice et al., 1998[^5])

$$C_f = \frac{2}{(5.1 \mathrm{log} (h/k_s)+6)^2}$$


## Notations

- \(\alpha\): ratio of the area affected by the bed friction to \(a_x \times a_y\)
- \(\alpha_t\): length scale of turbulence in the block layer (m)
- \(\beta\): ratio between drag stress and turbulence stress
- \(\kappa\): Von Karman constant = 0.41
- \(\sigma\): ratio between the block area in the plane X,y et \(D^2\)
- \(a_x\):  cell width (perpendicular to the flow) (m)
- \(a_y\):  length of a cell (parallel to the flow) (m)
- \(B\): pass width (m)
- \(C\): blocks concentration
- \(C_d\): drag coefficient of a block under current flow conditions
- \(C_{d0}\): drag coefficient of a block considering an infinitely high block with \(F \ll 1\)
- \(C_f)\): bed friction coefficient
- \(d\): displacement in the zero plane of the logarithmic profile (m)
- \(D\): width of the block facing the flow (m)
- \(F\): Froude number based on \(h\) and \(V_g\)
- \(g\): acceleration of gravity = 9.81 m.s<sup>-2</sup>
- \(h\): average depth (m)
- \(h_*\): dimensionless depth (\(h / D\))
- \(k\): useful block height (m)
- \(k_s\): roughness height (m)
- \(l_0\): length scale of turbulence at the top of the blocks (m)
- \(N\): ratio between bed friction and drag force
- \(Q\): flow (m<sup>3</sup>/s)
- \(S\): pass slope (m/m)
- \(u_0\): average bed speed (m/s)
- \(u_*\): shear velocity (m/s)
- \(V\): flow velocity (m/s)
- \(V_g\): velocity between blocks (m/s)
- \(s\): minimum distance between blocks (m)
- \(z\): vertical position (m)
- \(z_0\): hydraulic roughness (m)
- \(\tilde{z}\): dimensionless stand \(\tilde{z} = z / k\)

[^1]: Cassan L, Laurens P. 2016. Design of emergent and submerged rock-ramp fish passes. Knowl. Manag. Aquat. Ecosyst., 417, 45. https://doi.org/10.1051/kmae/2016032

[^2]: Cassan, L., Tien, T.D., Courret, D., Laurens, P., Dartus, D., 2014. Hydraulic Resistance of Emergent Macroroughness at Large Froude Numbers: Design of Nature-Like Fishpasses. Journal of Hydraulic Engineering 140, 04014043. https://doi.org/10.1061/(ASCE)HY.1943-7900.0000910

[^3]: Tran, T.D., Chorda, J., Laurens, P., Cassan, L., 2016. Modelling nature-like fishway flow around unsubmerged obstacles using a 2D shallow water model. Environmental Fluid Mechanics 16, 413–428. https://doi.org/10.1007/s10652-015-9430-3

[^4]: Larinier, Michel, Courret, D., Gomes, P., 2006. Guide technique pour la conception des passes à poissons “naturelles,” Rapport GHAPPE RA. Compagnie Nationale du Rhône / Agence de l’Eau Adour Garonne. http://dx.doi.org/10.13140/RG.2.1.1834.8562

[^5]: Rice C. E., Kadavy K. C., et Robinson K. M., 1998. Roughness of Loose Rock Riprap on Steep Slopes. Journal of Hydraulic Engineering 124, 179‑85. https://doi.org/10.1061/(ASCE)0733-9429(1998)124:2(179)
