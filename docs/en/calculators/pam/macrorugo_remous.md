# Backwater curve for a rock-ramp fishpass

This module can be used to simulate the backwater curve of a
[simple macro-roughness fishway](macrorugo.md) in order to determine the
downstream flooding level of the fishway.

## Operating principle

The parameters of this module are :

- The choice of the ["simple" rock-ramp fishpass](macrorugo.md) module among
those present in the work session which will be used to perform the
calculation&nbsp;
- The water level downstream of the pass&nbsp;
- The space step used to discretise the curve calculation.

N.B.&nbsp;: As the backwater curve can only be calculated for one set of
parameters, the [simple rock-ramp fishpass](macrorugo.md) module
cannot contain [varied parameters](../../general/principe_fonctionnement.md).

The theoretical calculation carried out in the macro-roughness channel
corresponds to the calculation of the water line in a uniform regime where the
slope of the water is equal to the slope of the bottom of the channel.
The rock-ramp fishpass module is used here to calculate the slope of the water
in the non-uniform case.
The fluvial backwater curve is then calculated from the water level
downstream using the [trapezoid integration method](../../theorie/methodes_numeriques/integration_trapezes.md).

This module is based on [the module for calculating the backwater curve of a
section](../hsl/courbe_remous.md) to calculate and display the results.
