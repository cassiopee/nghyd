# Trapezes integration method

The integral form of the ordinary first-order differential equation is written:

$$\int_{t_{i}}^{t_{i+1}}\frac{dy}{dt} = \int_{t_{i}}^{t_{i+1}}f(y,t)$$

Trapezes methods gives:

$$y_{i+1} \simeq y_i + \frac{y'_i + y'_{i+1}}{2} \Delta t$$
