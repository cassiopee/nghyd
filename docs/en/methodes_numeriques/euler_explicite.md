# Explicite Euler method

To describe an evolutionary process, or the profile of a water line, for example, we often have to solve a first-order ordinary differential equation (EDO). This equation writes how a function varies, at a given point (an instant or a point in space), knowing the value of this mathematical function, the problem to be solved is written:

$$
\left\{
  \begin{array}{rcr}
    \frac{dy}{dt} & = & f(y,t) \\
    y(t=t_0) & = & y_0 \\
  \end{array}
\right.
$$

where \(\frac{dy}{dt}\) refers to the derivative with respect to t of the function \(y\) (which depends on the variable \(t\)); the variable \(y_0\) is called the boundary condition; it conditions the final solution of the equation.

As we often do not know an analytical solution to this problem, we will use approximate methods to estimate the solution. So we make a discretization of the variable \(t\). We note the discretization step, and solve the problem at the points \(t_0\), \(t_1=t_0+\Delta t\), \(t_2=t_0+2\Delta t\), ..., \(t_n=t_0+n\Delta t\) where \(n\) is an integer.


The explicit Euler method is the most intuitive; it consists in considering that, from a point \(t_i\) to a point \(t_{i+1}\), the function evolves linearly, with a trajectory that is the one that can be calculated at point \(t_i\).

The problem is solved as follows:

 * we know the function \(f\), a point \(t_i\) where we know \(y_i\)
 * so we can calculate \(y'_i=f(y,t)\)
 * we then estimate the value of \(y\) at point \(t_{i+1}=t_i+\Delta t\): \(y_{i+1}\simeq y_i + y'_i \Delta t\)
 * you can then iterate (solve step by step) to move to the next point. The problem is initialized from \(t_0\) where we know \(y_0\) (boundary condition).

We can feel that this scheme will only work well if Delta is not too big. Values of too high \(\Delta t\) can give completely false results, leading to erroneous physical interpretations. However, its interest is its simplicity, and it can be easily implemented on a table.

## Example application: exponential process

Consider the following (simple) problem:
$$
\left\{
  \begin{array}{rcr}\label{eq:exp}
    \frac{dy}{dt} & = & -a y \\
    y(t=t_0) & = & y_0 \\
  \end{array}
\right.
$$

So we have here \(f(y,t)=-ay\). The analytical solution is easily solved, giving \(y(t)=y_0 \exp\left(-a(t-t_0)\right)\).
The problem can be solved by the Euler method: 

 * we choose \(\Delta t\) (for example, \(\Delta t=1\))
 * calculate \( y_1=y_0 - a y_0 \Delta t\)
 * calculate \( y_2=y_1 - a y_1 \Delta t\) etc.

It can be seen that the resolution is not very precise; this is linked to the calculation step being too large given the method chosen and the equation to be solved.
