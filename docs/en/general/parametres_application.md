# Application parameters

Accessible from the left side menu, the application parameters that can be modified by the user are as follows:

- Number of displayed decimals: Number of displayed decimals for the calculation results. For numbers close to zero displayed in scientific notation, this option sets the number of significant digits displayed;
- Computation accuracy: Precision used for the convergence of numerical calculations ([Brent's method](https://en.wikipedia.org/wiki/Brent%27s_method) or [Newton's method](https://en.wikipedia.org/wiki/Newton's_method));
- Solver iteration limit: Maximum iteration number of the numerical calculation;
- Enable on-screen notifications: allows notifications to be displayed during certain operations (warning when loading a session, calculation invalidation...);
- Enable keyboard shortcuts: allows the use of keyboard shortcuts (See [list of available shortcuts](raccourcis_clavier.md));
- Create new calculators with empty fields (no default values): if unchecked, module parameters are pre-filled with default values;
- Language: defines the language of the software interfaces in French or English.

The save button at the top of the window allows you to save the user's preferences in your browser for future use. The "Reset" button restores the application's default settings.
