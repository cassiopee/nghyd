# Keyboard shortcuts list

To use keyboard shortcuts, the feature must be enabled in [application parameters](parametres_application.md).

- Alt + S: Saves the current session
- Alt + O: Opens a session file
- Alt + Q: Empties the current session
- Alt + N: Adds a new calculation module to the current session
- Alt + ↵: Triggers calculation of the current module
- Alt + D: Duplicates the current module
- Alt + W: Closes the current module
- Alt + G: Shows modules diagram
- Alt + 1: Positions the page on the "Input" section of the current module
- Alt + 2: Positions the page on the "Results" section of the current module
- Alt + 3: Positions the page on the "Charts" section of the current module
