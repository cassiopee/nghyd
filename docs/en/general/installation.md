# Installation

Cassiopée can be installed for use without an internet connection as an executable program or as a progressive web app (PWA).

## Installation of the Cassiopée progressive web application

A progressive web app (PWA) is a web application that consists of pages or websites, and can appear to the user in the same way as native applications or mobile applications.

PWAs are available for all platforms (Windows, Linux, MacOS, Android, and iOS) provided you have a compatible browser:

| Plateforme | Chrome/Chromium | Edge | Firefox       | Safari |
|------------|-----------------|------|---------------|--------|
| Windows    | Yes             | Yes  | via extension |        |
| Linux      | Yes             |      | via extension |        |
| MacOS      | Yes             |      | via extension |        |
| Android    | Yes             |      | Yes           |        |
| iOS        |                 |      |               | Yes    |

The installation is done directly from the web browser at <https://cassiopee.g-eau.net> by clicking on a button located on the right of the address bar. The appearance of the button may vary depending on the browser used:

* Install a PWA with Chrome/Chromium: <https://support.google.com/chrome/answer/9658361?hl=fr>
* Installing a PWA with Edge: <https://learn.microsoft.com/fr-fr/microsoft-edge/progressive-web-apps-chromium/ux>
* Install a PWA (any browser and platform): <https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Installing>

After the installation, Cassiopée can be launched from the application icon on the desktop.

Updates are automatically detected and installed (the user is invited to restart Cassiopée after downloading the update).

N.B.: even without going through the PWA installation procedure, Cassiopée is available in the browser without an internet connection provided it has been previously loaded.

## Installation of the Desktop application (obsolete)

Cassiopée is available as an executable program for Windows, Linux and MacOS platforms. The installation programs can be downloaded at the following address: <https://cassiopee.g-eau.fr/cassiopee-releases/>

The installation in the form of a progressive web application (see above) will definitely replace this installation mode in a future version of Cassiopée.
