# Legal notice and terms of use

## Editor

The site cassiopee.g-eau.fr hereinafter referred to as "Cassiopée" is published by [the UMR G-EAU (Mixed Research Unit "Water Management, Actors, Uses")](http://g-eau.fr):

> UMR G-EAU<br/>
> 361 rue Jean-François Breton<br/>
> BP 5095<br/>
> 34196 Montpellier Cedex 5<br/>
> France
> Tel: +33 (0) 4 67 04 63 00

Director of publication: Marcel Kuper, Director of the UMR G-EAU

Development Project Manager: David Dorchies

Developers: David Dorchies, François Grand, Mathias Chouet

Cassiopée is a tool proposed by [OFB (French Office for Biodiversity)](https://ofb.gouv.fr/) within the framework of the OFB-Irstea conventions (Action n°100 of the 2016-2018 convention and Action n°21 of the 2019-2021 convention).

## Hosting

> UMR G-EAU<br/>
> 361 rue Jean-François Breton<br/>
> BP 5095<br/>
> 34196 Montpellier Cedex 5<br/>
> France

## Contents of the Cassiopée software

The OFB and the UMR G-EAU offer access to calculation tools in the field of hydraulics and more specifically tools to assist in the sizing of fish passes.

Internet users' access to the Cassiopee site and tools is free and unlimited for all private and professional uses, as long as this access is made only through the tools and interfaces of the site cassiopee.g-eau.fr.

Unless otherwise stated, the intellectual property of the content of the pages is held by the Institut national de Recherche en Sciences et Technologies pour l'Environnement et l'Agriculture (IRSTEA).

## Limitation of liability

The UMR G-EAU undertakes to provide the necessary and reasonable means to ensure or make ensure continuous access to the cassiopee.g-eau.fr website and its contents for the user.

However, the UMR G-EAU cannot be held liable to users in the event of interruption, failure or possible lack of quality of Cassiopée's services for any reason whatsoever, including for reasons of maintenance, upkeep or updating of the servers.

Unless otherwise stated, the contents of Cassiopee are published for information purposes only, excluding any guarantee as to their accuracy or suitability for the specific needs of users of the site cassiopee.g-eau.fr. The contents of Cassiopée do not in any way engage the responsibility of the producers in the event of direct or indirect damage resulting from their non-conformity with the reality on the ground.

If you notice an error or omission in the tools and content of Cassiopée, please report it via the "Report a problem" feature available in the main menu of Cassiopée.

## Users' personal information

#### Data collected by Cassiopée

The cassiopee.g-eau.fr site does not collect any personal data about the user except the IP address of the machine used to connect to the site and the calculation modules used by the user and the number of calculation run.

The data and calculations carried out by Cassiopée are entirely carried out on the user's machine and are not transmitted to the server or any other third party.

Submitting a bug report via the "Report an issue" feature invites the user to send by email the content of his current work session to the Cassiopée development team. The user can choose not to transmit his session by deleting the text of the email below the line `--- Status of the current session - do not modify the text below ---`. The email address provided by the user to the development team will not be shared with third parties or used for any purpose other than to contact the user in connection with the reported problem.

#### Data stored on the user's terminal

Cassiopée does not store any information on the user's terminal except the application settings if they are explicitly saved by the user.

All or part of the user's working session can be saved by the user on any medium at his convenience (json file) and then loaded for later use.

## Hypertext links

#### Links from cassiopee.g-eau.fr to other sites

The links inserted in the pages of the site cassiopee.g-eau.fr to third party sites are provided for information purposes only. The content of the sites at which these links point does not engage the responsibility of the UMR G-EAU.

#### Link to cassiopee.g-eau.fr

The establishment of a hyperlink to Cassiopée is free and open on condition that this hyperlink allows the opening of a new viewing window and that the display of the URL cassiopee.g-eau.fr in the new window is readable by the Internet user as soon as it is opened and throughout the access to Cassiopée's data.

UMR G-EAU reserves the right to delete any hyperlink to Cassiopée that may harm its editorial policy or its image.

## Brands and logos

The brands and logos appearing on the site make it possible to inform as to the origin of the data and software used; they have no advertising character and are the property of their respective owners.

## Screenshots and prints

The content produced by Cassiopée (calculation results, tables, graphs, etc.) can be reused and distributed on any medium without any limitation.

Cassiopée's documentation is published under[CC BY-NC-ND 4.0 License](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode) which authorizes to copy, distribute, communicate all or part of the documentation by any means and in any format under the conditions that it is credited with a link to the license, it is not used commercially, and that it is not modified.

## Free software

#### License of the Cassiopée calculation module

Cassiopée is based on a library called JaLHyd (for JAvascript Library for HYDraulics) developed in Typescript by the Cassiopée development team. This library is published under [GNU AGPL 3 license](https://www.gnu.org/licenses/agpl.html) and the source code is available on request from the Cassiopée editor.

#### Third-party tools and libraries

Cassiopée has been developed using many libraries and free software tools including:

- [Typescript](https://github.com/Microsoft/TypeScript)
- [Visual Studio Code](https://github.com/Microsoft/vscode)
- [Angular](https://github.com/angular/angular)
- [Angular Material](https://github.com/angular/components)
- [Chart.js](https://github.com/chartjs/Chart.js)
- [MathJax](https://github.com/mathjax/MathJax)
