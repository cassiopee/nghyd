# Presentation of Cassiopée software

<div style="height: 86px; border: solid #aaa 2px; padding: 10px; padding-right: 20px; border-radius: 5px; display: inline-block;">
  <a href="https://cassiopee.g-eau.fr" style="text-decoration: none;">
    <img style="float:left; margin-right: 20px;" src="logo.png">
    <span style="font-size: 30px; line-height: 60px">https://cassiopee.g-eau.fr</span>
  </a>
</div>

## General characteristics

Cassiopée is a software dedicated to rivers hydraulics with especially some help for sizing fish passes, agricultural hydraulics and open-channel hydraulics in general. It comes in the form of independent [calculation modules](general/principe_fonctionnement.md) allowing one to solve a given problem. Calculation modules may be chained (parameters or calculation results may be "linked" from one module to another) in order to build complex calculation chains. Users may locally save the modules they use, in order to reuse them later.

## Pre-requisites - installation

Cassiopée does not require any installation. It is available online using an up-to-date browser (tested with Firefox, Edge, Chrome and Chromium) by navigating to the following address: [https://cassiopee.g-eau.fr](https://cassiopee.g-eau.fr).

Offline versions are available for the Windows, Linux, macOS, Android platforms. For details, see the ["Installation" section](general/installation.md) in the documentation.

## Documentation

Download [documentation in PDF format](https://cassiopee.g-eau.fr/assets/docs/pdf/cassiopee_doc_en.pdf)

Download [illustrated quick start guide (in french) in PDF format](https://cassiopee.g-eau.fr/assets/docs/pdf/cassiopee_notice_illustree_fr.pdf)

## Support and bug reports

To be kept informed of Cassiopée news or to benefit from the Cassiopée users' support network, subscribe to the Cassiopée users' mailing list: <https://groupes.renater.fr/sympa/subscribe/cassiopee-users>

Insiders can test the future version of Cassiopée under development at <https://cassiopee-dev.g-eau.fr> and provide feedback at bug@cassiopee.g-eau.fr.

To report a bug in the application, please use the "Report an issue" link in the main menu of the application or write directly to bug@cassiopee.g-eau.fr.

For questions concerning the design of fish crossing structures for upstream (basin, retarder, macro-roughness) and downstream migrations, please contact Sylvain Richard, pole OFB-IMFT Écohydraulique, sylvain.richard@imft.fr.

## Citing Cassiopée

If you use Cassiopée in your work please remember to give appropriate credit by using the following reference:

*Dorchies, David; Grand, François; Chouet, Mathias; Cassan, Ludovic; Courret, Dominique; Richard, Sylvain, 2022, "Cassiopée: tools for designing fish crossing devices for upstream and downstream migrations, and hydraulic calculation tools for environmental and agricultural engineering. Version 4.16.0", <https://doi.org/10.15454/TLO5LX>, Recherche Data Gouv, V1*
