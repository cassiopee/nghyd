#!/bin/bash

set -o errexit
# Output command lines for debugging
set -x

# Fabrique les exécutables Electron pour une version de Cassiopée $1, les
# distribue sur le serveur $2@$3 dans le dossier $4, et met à jour le fichier releases.json
# et les liens symboliques si la version n'est pas nightly

echo "args $@"

if (( $# < 4 )); then
    echo "usage: $0 <X.Y.Z|nightly> <login> <server> <releases dir>"
    exit 1
fi

VERSION="$1"
LOGIN="$2"
HOST="$3"
RELEASES_DIR="$4"
# RELEASES_DIR="/tmp/cassiopee-releases" # debug
RELEASES_FILE="$RELEASES_DIR/releases.json"
TMP_RELEASES_FILE="tmp-releases.json"
HOST_LOGIN=$LOGIN@$HOST

# update flag for releases file/symlinks
update_latest=1
if [[ $VERSION == "nightly" ]]; then
  update_latest=0
fi

echo "release-version.sh: building release for version $VERSION, deploying in $HOST_LOGIN:$RELEASES_DIR"

# build releases
npm run release-all

# update existing releases file
if (( $update_latest )); then
  echo "updating releases.json"

  # fetch current releases file
  scp "$HOST_LOGIN:$RELEASES_FILE" "./$TMP_RELEASES_FILE"

  if [[ -n $(grep -P "\"latest\": \"$VERSION\"" "$TMP_RELEASES_FILE") ]]; then
    echo "$VERSION est déjà la version la plus récente, pas de mise à jour du fichier releases.json"
  else
    echo "mise à jour du fichier releases.json à la version $VERSION"

    sed -i -E "s/\"latest\": .+,/\"latest\": \"$VERSION\",/" "$TMP_RELEASES_FILE"
    echo -e "\t\"$VERSION\": {
\t\t\"darwin\": \"Cassiopée-${VERSION}-mac.zip\",
\t\t\"linux\": \"fr.irstea.cassiopee_${VERSION}_amd64.deb\",
\t\t\"win32\": \"Cassiopée Setup $VERSION.exe\"
\t}," > releases_patch.tmp

    sed -i "/\"latest\": \"$VERSION\",/r releases_patch.tmp" "$TMP_RELEASES_FILE"
    rm releases_patch.tmp
  fi

  # send updated file, remove local copy
  scp "./$TMP_RELEASES_FILE" "$HOST_LOGIN:$RELEASES_FILE"
  rm "./$TMP_RELEASES_FILE"
fi

# copy releases to public web directory
ssh $HOST_LOGIN mkdir -p $RELEASES_DIR
if [[ $VERSION == nightly ]]; then
  find release -name "fr.irstea.cassiopee_*.deb" -exec scp "{}" $HOST_LOGIN:$RELEASES_DIR/linux-nightly.deb \;
  find release -name "Cassio*Setup*.exe" -exec scp "{}" $HOST_LOGIN:$RELEASES_DIR/windows-nightly.exe \;
  find release -name "Cassio*-mac.zip" -exec scp "{}" $HOST_LOGIN:$RELEASES_DIR/macos-nightly.zip \;
else
  scp "release/Cassiopée Setup $VERSION.exe" "release/fr.irstea.cassiopee_${VERSION}_amd64.deb" "release/Cassiopée-${VERSION}-mac.zip" "$HOST_LOGIN:$RELEASES_DIR/"
fi

# symlink "latest" version for each platform

if (( $update_latest )); then
  echo "updating 'latest' symlinks"

  ssh $HOST_LOGIN /bin/bash << EOF
    cd "$RELEASES_DIR"
    ln -sf "Cassiopée Setup $VERSION.exe" "windows-latest.exe"
    ln -sf "fr.irstea.cassiopee_${VERSION}_amd64.deb" "linux-latest.deb"
    ln -sf "Cassiopée-${VERSION}-mac.zip" "macos-latest.zip"
EOF
fi
