/*
 * Définition de la date du dernier commit de ngHyd
 *
 */

var fs = require('fs');
date_last_commit = require('child_process')
    .execSync('git log -1 --format=%cd --date=short')
    .toString().trim()
version = require('child_process')
    .execSync('git describe')
    .toString().trim()

var sFileName = "src/date_revision.ts";

fs.writeFile(sFileName, `export const nghydDateRev = "${date_last_commit}";\nexport const nghydVersion = "${version}";\n`, function(err) {
    if(err) {
        return console.log(err);
    }

    console.log(`File ${sFileName} saved with date ${date_last_commit}, version ${version}`);
});
