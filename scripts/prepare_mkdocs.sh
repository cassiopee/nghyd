#!/bin/bash

set -o errexit

BUILD_DIR=build
VER_FILE=$BUILD_DIR/cassiopee_version

function prepareMkdocs
{
  local lang=$1

  # copy and modify Mkdocs configuration file :
  # - add Cassiopée version
  # - modify path to input files
  # - modify path to output files
  cat mkdocs/mkdocs-$lang.yml \
  | sed "/^site_name:/ s/$/ v$VER/" \
  | sed "/^docs_dir:/ s/docs_dir: /docs_dir: ..\//" \
  | sed "/^site_dir:/ s/site_dir: /site_dir: ..\//" > $BUILD_DIR/mkdocs-$1.yml
}

node scripts/extract-nghyd-version.js $VER_FILE
VER=$(cat $VER_FILE)

prepareMkdocs fr
prepareMkdocs en
