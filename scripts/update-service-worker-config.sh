#!/bin/bash
# update ngsw-config.json from template and "git describe" output

# error on unset variables
set -o nounset

# exit on error
set -o errexit

# Angular service worker configuration file
NGSW_CONF=ngsw-config.json

# Angular service worker configuration template
NGSW_CONF_TMPL=ngsw-config-template.json

echo "updating version in Angular service worker configuration" >&2

VERSION=$(git describe)

cp $NGSW_CONF_TMPL $NGSW_CONF

sed -i "/\"version\": \"/s/\": \".*/\": \"$VERSION\"/" $NGSW_CONF
# check
if [[ -z $(grep version $NGSW_CONF | grep $VERSION) ]]; then
  echo "error updating version in service worker configuration file $NGSW_CONF" >&2
  exit 1
fi
