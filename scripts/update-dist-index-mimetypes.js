'use strict';

const fs = require('fs');

const path = "./dist/index.html";
const contents = fs.readFileSync(path, "utf-8");
fs.writeFileSync(path, contents.replace(/type="module"/g, 'type="text/javascript"'));
