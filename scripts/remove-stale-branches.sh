#!/bin/bash

# Supprime du serveur Web les "builds" de Cassiopée correspondant à des branches ou des
# tags qui n'existent plus dans le dépôt.
# Conçu pour être utilisé par Gitlab CI/CD

if [ "$#" -lt 2 ]; then
    echo "usage: $0 login@server /var/www/deploy_dir"
    exit 1
fi

HOST_LOGIN="$1"
DEPLOY_DIR="$2"
# DEPLOY_DIR="/tmp/cassiopee" # debug

echo "remove-stale-branches.sh: cleaning directory $DEPLOY_DIR on host $HOST_LOGIN"

nghyd_branches=$(git ls-remote --heads origin|awk '{print $2}'|awk 'BEGIN{FS="/"} {print $3}')
nghyd_tags=$(git tag)
TNB="$nghyd_branches $nghyd_tags"

ssh $HOST_LOGIN /bin/bash << EOF
    cd $DEPLOY_DIR
    LOCALTNB="$TNB"
    for d in \$(ls -d *); do
        found=0
        for tag_or_branch in \$LOCALTNB; do
            if [[ \$d == \$tag_or_branch ]]; then
                found=1
                break
            fi
        done
        if (( \$found == 0 )); then
            echo "suppression de branche/tag obsolète \$d"
            rm -rf \$d
        fi
    done
EOF
