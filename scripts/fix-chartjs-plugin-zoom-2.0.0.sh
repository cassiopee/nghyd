# fix compilation error due to chartjs-plugin-zoom 2.0.0
# cf. https://gitlab.irstea.fr/cassiopee/nghyd/-/issues/238#note_76247

sed -i "s/const enum UpdateModeEnum {$/enum UpdateModeEnum {/g" node_modules/chartjs-plugin-zoom/types/index.d.ts
