'use strict';

/*
 * mkdocs2pdf post processing.
 * - copy illustrated guide (not related to MkDocs, but still a part of the doc) to dist/assets/docs/pdf
 */

const fs = require('fs-extra');

const destPath_PDF = "dist/assets/docs/pdf"; // pour la doc illustrée

fs.ensureDirSync(destPath_PDF);
fs.copySync("docs/cassiopee_notice_illustree_fr.pdf", destPath_PDF + "/cassiopee_notice_illustree_fr.pdf");
