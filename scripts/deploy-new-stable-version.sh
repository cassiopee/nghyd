#!/bin/bash

# Use this script to tag new stable versions of JaLHyd and NgHyd, to be then built and deployed by Gitlab CI/CD
#
# This script requires:
# * bash
# * git
# * npm

# exit on error
set -e

VERSION=$1

if [[ $VERSION == "" ]]
then
    echo "Utilisation: $0 x.y.z
    ex: $0 4.7.0"
    exit 1
fi

# Angular service worker configuration file
NGSW_CONF=ngsw-config.json

# 0. changelog
read -p "Avez-vous rempli jalhyd_branch, et les CHANGELOG de JaLHyd et NgHyd pour la version $VERSION ? (o/N) " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Oo]$ ]]
then
    echo "Remplissez d'abord nghyd/jalhyd_branch, jalhyd/CHANGELOG.md et nghyd/CHANGELOG.md, puis faites 'commit' et 'push' dans chaque dépôt"
    exit 2
fi

if [[ ! -f $NGSW_CONF ]]; then
  echo "Fichier de configuration du service worker $NGSW_CONF non trouvé" >&2
  exit 1
fi

# 1. JaLHyd ###################################################################

echo "BUILDING JALHYD"
cd jalhyd

# 1.2 update Git repository and test
git checkout master
git pull --rebase
npm install
npm run jasmine
if [ ! -z "$(git status --untracked-files=no --porcelain)" ]
then
    echo "commiting changes induced by 'npm install'"
    git commit -a -m "verify dependencies (npm install) before deploying version $VERSION"
fi

# 1.3 version in package.*
npm version "$VERSION" --allow-same-version --git-tag-version=false
if [ ! -z "$(git status --untracked-files=no --porcelain)" ]
then
    echo "commiting changes induced by 'npm version'"
    git commit -a -m "update package.* to version $VERSION"
fi

# 1.4 tags
echo "setting tags to $VERSION version"
git tag -fa "nghyd_$VERSION" -m "release version $VERSION"
sleep 1
git tag -fa stable -m "stable version"

# 1.5 push code, push tags
git push
git push --tags --force


# 2. NgHyd ####################################################################

echo "BUILDING NGHYD"
cd ..

# 2.1 update Git repository
git checkout master
git pull --rebase
npm install
if [ ! -z "$(git status --untracked-files=no --porcelain)" ]
then
    echo "commiting changes induced by 'npm install'"
    git commit -a -m "verify dependencies (npm install) before deploying version $VERSION"
fi

# 2.2 version in package.*
npm version "$VERSION" --allow-same-version --git-tag-version=false
if [ ! -z "$(git status --untracked-files=no --porcelain)" ]
then
    echo "commiting changes induced by 'npm version'"
    git commit -a -m "update package.* to version $VERSION"
fi

# 2.3 tags
echo "setting tags to $VERSION version"
git tag -fa stable -m "stable version"
sleep 1
git tag -fa "$VERSION" -m "release version $VERSION"

# 2.4 push code, push tags
git push
git push --tags --force

echo
echo "N'oubliez pas de modifier le numéro de version dans le document https://doi.org/10.15454/TLO5LX"
echo 

echo "Tagging done, Gitlab CI/CD should take care of the rest."
