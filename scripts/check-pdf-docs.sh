#!/bin/bash

# check that PDF doc files are generated

# error on unset variables
set -o nounset

# exit on error
set -o errexit

function check_file()
{
  if [[ ! -f $1 ]]; then
    echo "$1 : missing documentation file" >&2
    exit 1
  fi
}

check_file dist/assets/docs/pdf/cassiopee_notice_illustree_fr.pdf
check_file dist/assets/docs/pdf/cassiopee_doc_fr.pdf
check_file dist/assets/docs/pdf/cassiopee_doc_en.pdf
