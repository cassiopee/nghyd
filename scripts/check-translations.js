'use strict';

/**
 * 1) Reads Message enum in jalhyd, and for every message code in it, checks
 * that there is a translation in each nghyd's locale/*.json file (ie. for
 * every language)
 *
 * 2) For every nghyd calculator, checks that the translated keys are the same
 * in all language files
 */

const fs = require('fs');

/* IMPORTANT: during step 2, will look for those languages codes only */
const expectedLanguages = [ "fr", "en" ];

// total errors
let nbErr = 0;

// ---- 1. JaLHyd messages ----

// read and transform JaLHyd message file
const jalhydMessagesPath = "./jalhyd/src/util/message.ts";
let jm = fs.readFileSync(jalhydMessagesPath, "utf-8");
// extract enum block
jm = jm.replace(/export enum MessageCode \{([^{]+)\}[\s\S]+/m, (match, p1) => {
    return p1;
});
// remove comments
jm = jm.replace(/\/\*[\s\S]*?\*\//gm, "");
jm = jm.replace(/\/\/.+/g, "");

// remove spaces
jm = jm.replace(/[ \t]+/g, "");
// remove line breaks
jm = jm.replace(/\n/g, "");

// split on ","
const messages = jm.split(",");

// remove imports
const firstLine = messages[0].split(";");
while (firstLine.length > 1) {
    if (firstLine[0].indexOf("import") !== -1) {
        firstLine.splice(0, 1);
    }
}
messages[0] = firstLine[firstLine.length - 1];

// read every language file
const localePath = "src/locale";
const localeDir = fs.readdirSync(localePath);
for (let i = 0; i < localeDir.length; i++) {
    const localeFile = localeDir[i];
    const res = localeFile.match(/^messages\.([a-z]{2})\.json$/);
    if (res) {
        const lang = res[1];
        console.log("Loading global translations for language [" + lang + "]");
        const langFilePath = localePath + '/' + localeFile;
        const translations = Object.keys(JSON.parse(fs.readFileSync(langFilePath, "utf-8")));
        // console.log(translations);
        // check against JaLHyd messages list
        for (const mess of messages) {
            if (! translations.includes(mess)) {
                console.log("  missing message in [" + lang + "] translation: " + mess);
                nbErr++;
            }
        }
    }
}

// ---- 2. calculators localisation ----

// find every calculator folder
const calculatorsPath = "src/app/calculators";
const calculatorsDir = fs.readdirSync(calculatorsPath);
console.log("Checking all calculators translations for languages [" + expectedLanguages.join(", ") + "]");
for (let i = 0; i < calculatorsDir.length; i++) {
    const calcPath = calculatorsPath + "/" + calculatorsDir[i];
    const stats = {};
    // console.log(" checking calculator [" + calculatorsDir[i] + "]");
    // find all language files for this calculator, and store translation keys
    for (let j = 0; j < expectedLanguages.length; j++) {
        const exLang = expectedLanguages[j];
        const langFilePath = calcPath + "/" + exLang + ".json";
        if (fs.existsSync(langFilePath)) {
            const translations = Object.keys(JSON.parse(fs.readFileSync(langFilePath, "utf-8")));
            // console.log(translations);
            stats[exLang] = translations;
        } else {
            console.log(" missing language file [" + exLang + ".json] for calculator [" + calculatorsDir[i] + "]");
            nbErr++;
        }
    }
    // console.log(stats);
    // compare number of translations per file
    let sameNumber = true;
    // compare keys names (order-dependent) per file
    let sameKeys = true;
    let prevKeys = null;
    const sKeys = Object.keys(stats);
    for (let k = 0; k < sKeys.length; k++) {
        const key = sKeys[k];
        if (k > 0) {
            sameNumber = sameNumber && (stats[key].length === prevKeys.length);
            sameKeys = sameKeys && (JSON.stringify(stats[key]) === JSON.stringify(prevKeys)); // compare arrays
        }
        prevKeys = stats[key];
    }
    // difference found ?
    if (! sameNumber) {
        console.log(" [" + calculatorsDir[i] + "]: different number of keys found", sKeys.map((s) => {
            return s + ": " + stats[s].length;
        }));
        nbErr++;
    } else if (! sameKeys) {
        console.log(" [" + calculatorsDir[i] + "]: different keys found", stats);
        nbErr++;
    }
}


// ---- 3. JaLHyd messages ----

if (nbErr === 0) {
    console.log("Everything OK !");
    process.exit(0);
} else {
    process.exit(1);
}
