#!/usr/bin/env bash

# Inspired from https://stackoverflow.com/a/25042323/5300212
# How to run e2e test after serving the app

# Start the web app
npm start &
NG_SERVE_PID=$!

# Wait for http response 200 on port 4200
response=0
while [ "$response" != "200" ]; do
    response=$(curl --connect-timeout 5 --write-out '%{http_code}' --silent --output /dev/null 127.0.0.1:4200 || echo 0)
    sleep 5;
done

echo "**************************************************************************"
echo "If something goes wrong, use 'pkill ng', to kill the server"
echo "**************************************************************************"

# Run wdio
npm run e2equick

# Récupération du code d'erreur dans une variable
error_code=$?
if [ $error_code -eq 0 ]; then
    echo "Cool man...Cadence man... Trace la glace c'est bob man !!!"
else
    echo "Caramba! Encore raté! Hay un error! Error Code : $error_code"
fi

# Cleanup daemon processes
pkill ng

exit $error_code
