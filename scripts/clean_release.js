'use strict';

const fs = require('fs');
const rimraf = require('rimraf');

const path = './release';

if (fs.existsSync(path)) {
    rimraf.sync(path);
    fs.mkdirSync(path);
}
