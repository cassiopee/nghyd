#!/bin/bash

set -o errexit

# deploie l'appli sur le serveur $3 (user $2) dans le dossier $4 et met à jour le fichier index.html
# si la version est "prod"

if (( $# < 4 )); then
    echo "usage: $0 <version> <login> <server> <path>"
    exit 1
fi

VERSION="$1"
LOGIN="$2"
HOST="$3"
DIR="$4"

LOCAL_DIR=dist

function display_local_href()
{
  echo "base href in local index.html :"
  grep "\<base" $LOCAL_DIR/index.html
}

function display_remote_href()
{
  local dir=$1
  echo "base href in deployed index.html (host=$HOST) :"
  ssh $LOGIN@$HOST "grep \<base $dir/index.html"
}

echo "$(basename $0): deploying version $VERSION in $LOGIN@$HOST:$DIR"

if [[ $VERSION == "prod" || $VERSION == "prod-devel" ]]; then
  display_local_href

  # Copie de la branche production
  rsync -a --delete --exclude=cassiopee-releases -e "ssh -o StrictHostKeyChecking=no" $LOCAL_DIR/ ${LOGIN}@${HOST}:${DIR}/

  display_remote_href $DIR
else
  display_local_href

  # Copie de la branche / du tag
  rsync -a --delete --exclude=cassiopee-releases -e "ssh -o StrictHostKeyChecking=no" $LOCAL_DIR/ "$LOGIN@$HOST:$DIR/$VERSION"

  display_remote_href $DIR/$VERSION
fi
