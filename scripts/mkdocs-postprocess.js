'use strict';

/**
 * copies extra javascripts (MathJax, matomo-tracking) to src/assets/docs/javascripts after doc was built by MkDocs
 */

const fs = require('fs-extra');

const destPath_JS = "src/assets/docs/javascripts";
const destPath_CSS = "src/assets/docs/stylesheets";
const destPath_JS_MJ = destPath_JS + "/mathjax";

// empty destination folder
fs.emptyDirSync(destPath_JS);

// create destination subfolders
fs.ensureDirSync(destPath_JS_MJ + "/fonts/HTML-CSS/TeX/", { recursive: true });

// copy required files only
fs.copySync("node_modules/mathjax/es5/tex-mml-chtml.js", destPath_JS_MJ + "/tex-mml-chtml.js");
fs.copySync("node_modules/mathjax/es5/output/chtml/fonts", destPath_JS_MJ + "/output/chtml/fonts");
fs.copySync("docs/mathjax.config.js", destPath_JS + "/mathjax.config.js");
fs.copySync("docs/mathjax-scrollbar.css", destPath_CSS + "/mathjax-scrollbar.css");
fs.copySync("docs/matomo-tracking.js", destPath_JS + "/matomo-tracking.js");
fs.copySync("node_modules/mermaid/dist/mermaid.min.js", destPath_JS + "/mermaid.min.js");
fs.copySync("node_modules/mermaid/dist/mermaid.min.js.map", destPath_JS + "/mermaid.min.js.map");

/*
Ici, on veut que les ressources copiées ci dessus soit accessibles.

mkdocs-xx.yml :
extra_css:
    - ../stylesheets/mathjax-scrollbar.css

Fichiers HTML générés :
Par ex src/assets/docs/fr/calculators/maths/solver.html contient <link rel="stylesheet" href="../../stylesheets/mathjax-scrollbar.css">
ce qui fait référence à stylesheets dans src/assets/docs/fr alors qu'on veut référencer stylesheets dans src/assets/docs/
(le chemin généré dépend de l'emplacement du fichier le contenant).

Le problème, c'est que si on modifie extra_css avec ../../stylesheets/mathjax-scrollbar.css, cela génère la même chose
(href="../../stylesheets/mathjax-scrollbar.css") vraisemblablement à cause de site_dir (=docs/fr) qui limite les "remontées"
de niveau de répertoire.

La solution est donc de modifier les fichiers HTML générés pour ajouter ../ dans href="...".
*/

const cp = require('child_process');

function execBashCmd(cmd) {
    // console.log(cmd);
    // console.log(cp.execSync(cmd, { encoding: 'utf-8' }));
    cp.execSync(cmd);
}

function escapeSlash(s) {
    return s.replaceAll('/', '\\\/');
}

function escapeDot(s) {
    return s.replaceAll('\.', '\\.');
}

function escapeDoubleQuote(s) {
    return s.replaceAll('"', '\\"');
}

function applyEscapes(s) {
    return escapeDot(escapeSlash(escapeDoubleQuote(s)));
}

function replaceHtml(base_dir, s1, s2) {
    s1 = applyEscapes(s1);
    s2 = applyEscapes(s2);
    const cmd = "find " + base_dir + " -name '*.html' -exec sed -i \"s/" + s1 + "/" + s2 + "/g\" {} \\\;";
    execBashCmd(cmd);
}

replaceHtml("src/assets/docs/fr/", "../../stylesheets/mathjax-scrollbar.css", "../../../stylesheets/mathjax-scrollbar.css");
replaceHtml("src/assets/docs/en/", "../../stylesheets/mathjax-scrollbar.css", "../../../stylesheets/mathjax-scrollbar.css");

replaceHtml("src/assets/docs/fr/", "../../javascripts/mathjax.config.js", "../../../javascripts/mathjax.config.js");
replaceHtml("src/assets/docs/en/", "../../javascripts/mathjax.config.js", "../../../javascripts/mathjax.config.js");

replaceHtml("src/assets/docs/fr/", "../../javascripts/matomo-tracking.js", "../../../javascripts/matomo-tracking.js");
replaceHtml("src/assets/docs/en/", "../../javascripts/matomo-tracking.js", "../../../javascripts/matomo-tracking.js");

replaceHtml("src/assets/docs/fr/", "../../javascripts/mermaid.min.js", "../../../javascripts/mermaid.min.js");
replaceHtml("src/assets/docs/en/", "../../javascripts/mermaid.min.js", "../../../javascripts/mermaid.min.js");

replaceHtml("src/assets/docs/fr/", "../../javascripts/mathjax/tex-mml-chtml.js", "../../../javascripts/mathjax/tex-mml-chtml.js");
replaceHtml("src/assets/docs/en/", "../../javascripts/mathjax/tex-mml-chtml.js", "../../../javascripts/mathjax/tex-mml-chtml.js");
