/*
 * extract Cassiopée version from package.json and write files according to provided paths extension :
 * - a LateX file defining a variable in the form \newcommand\{\cassiopeeversion}{x.y.z} for path(s) with .tex extension
 * - a plain text file with version value (x.y.z) for path(s) without extension
 */

'use strict';

const fs = require('fs');
const path = require('path');

if (process.argv.length < 3) {
    console.error("ERROR : missing output file path(s)\n");
    console.error("syntax : extract-nghyd-version <path> [<path> ...]");
    console.error("  extract ngHyd current version to given files according to extension");
    console.error("  available extensions : .tex (LateX file with variable), <none (plain version in text file)>");
    process.exit(1);
}

function createMissingDirs(p) {
    const targetDir = path.dirname(p);
    if (!fs.existsSync(targetDir)) {
        console.log("creating output directory", targetDir)
        fs.mkdirSync(targetDir, { recursive: true });
    }
}

function writeTex(outPath, ver) {
    // make LateX variable
    const s = `\\newcommand\{\\cassiopeeversion\}\{${ver}\}`;

    // create intermediate folders if necessary
    createMissingDirs(outPath)

    // write file
    console.log("writing LateX file", outPath);
    fs.writeFileSync(outPath, s);
}

function writePlain(outPath, ver) {
    // create intermediate folders if necessary
    createMissingDirs(outPath)

    // write file
    console.log("writing plain file", outPath);
    fs.writeFileSync(outPath, ver);
}

// read package.json
console.log("reading package.json");
var fdata = fs.readFileSync('package.json', 'utf8');
var data = JSON.parse(fdata)

// get Cassiopée version
const ver = data.version
console.log("got ngHyd version", ver);

for (let i = 2; i < process.argv.length; i++) {
    const p = process.argv[i];
    if (p.endsWith(".tex")) {
        writeTex(p, ver);
    } else {
        writePlain(p, ver);
    }
}
