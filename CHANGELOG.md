# Historique des versions

### 4.19.0 - 03/03/2025

#### Nouvelles fonctionnalités

* Ajout de la passe à rugosité de fond ([jalhyd#340](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/340))
* RugoFond: Ajouter un avertissement sur les lignes d'eau trop élevées ([jalhyd#379](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/379), [nghyd#675](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/675))
* Espèce personnalisée : grouper les critères de PAB ([nghyd#457](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/457))
* Documentation > Lois d'ouvrages: homogénéiser schémas, formules et noms des lois ([nghyd#512](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/512))
* Solveur: Résultat ciblé: ne pas proposer le paramètre calculé ([nghyd#669](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/669))

#### Correction de bogues

* Courbe de remous - génération section paramétrée : la pente de fond n'est pas copiée ([jalhyd#366](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/366))
* Macrorugo Complexe: Non convergence du calcul ([jalhyd#368](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/368))
* Solveur: absence de message d'erreur à la non convergence ([jalhyd#369](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/369))
* Solveur: Les select d'une session chargée ne s'initialise pas ([jalhyd#374](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/374), [nghyd#668](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/668))
* Non fonctionnement des tests E2E suite à la mise à jour de NodeJS ([nghyd#646](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/646))
* Documentation > HTML: Les formules ne s'affichent pas dans le chapitre méthodes numériques ([nghyd#653](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/653))
* Documentation > PDF: erreur de notes de bas de page ([nghyd#655](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/655))
* Cassiopée doesn't work for languages other than French and English ([nghyd#667](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/667))
* Rugofond Multiple/Macrorugo complexe: affichage des unités sur les résultats complémentaires ([nghyd#676](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/676))

#### Changements internes

* Non fonctionnement des tests E2E suite à la mise à jour de NodeJS ([nghyd#646](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/646))
* Optimize waiting time for starting e2e tests ([nghyd#647](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/647))
* Mise à jour vers Angular 15 ([nghyd#659](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/659))
* Tests E2E: Error: Couldn't find a matching Chrome browser for tag "121.0.6167.184" on platform "Linux" ([nghyd#661](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/661))
* E2E tests are failing for one month ([nghyd#678](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/678))

### 4.18.0 - 2024-01-30

#### Nouvelles fonctionnalités

* Ajout du module courbe de remous d'une passe à macro-rugosité ([jalhyd#325](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/325), [nghyd#609](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/609), [nghyd#650](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/650), [nghyd#654](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/654))
* Add bundle maker for CassiopeeR ([jalhyd#360](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/360))
* Chargement d'une session: Afficher la page de note si elle existe au lieu du diagramme des modules ([nghyd#649](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/649))
* PreBarrage: autoriser les cotes de seuil inférieures à la cote de fond du bassin amont ([jalhyd#353](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/353), [nghyd#634](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/634))
* Electron: ajouter un message d'avertissement de transition vers PWA ([nghyd#640](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/640))

#### Correction de bogues

* L'URL https://cassiopee.g-eau.fr/cassiopee-releases/ est accessible aléatoirement ([nghyd#624](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/624))
* PWA: Les bloqueurs de tracker empêchent la mise à jour de l'application ([nghyd#633](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/633))
* Vérificateur: la vérification ne devrait pas être possible quand aucune espèce n'est sélectionnée ([jalhyd#349](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/349), [nghyd#637](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/637))
* Prebarrage: erreur de calcul de la somme des débits sur plusieurs branches ([jalhyd#351](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/351))
* MacroRugo: erreur de calcul du Strickler équivalent ([jalhyd#361](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/361))
* Documentation PDF: erreurs à la compilation ([nghyd#656](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/656))

#### Documentation

* Grilles: ajouter les schémas de profil issus de Lemkecher et al. (2020) ([nghyd#594](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/594))
* Ajouter un exemple de passe à rangées périodiques ([nghyd#635](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/635))
* Vérificateur: ajouter un avertissement à l'utilisation ([nghyd#643](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/643))
* Mise à jour du logo de l'UMR G-EAU ([nghyd#651](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/651))

#### Changements internes

* Update chartjs zoom plugin to v2.0.1 ([nghyd#638](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/638))
* Importer le DockerFile utilisé pour le CI/CD de Cassiopée dans ngHyd ([nghyd#639](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/639), [nghyd#648](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/648))
* Clean npm dependencies ([jalhyd#356](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/356))
* CI: automatically publish jalhyd package on NPM ([jalhyd#359](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/359))

### 4.17.1 - 2023-11-20

#### Correction de bogues

* Cloisons: le champ "Cote de l'eau amont" n'est pas vide par défaut ([jalhyd#355](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/355), [nghyd#623](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/623))
* Le séparateur décimal est passé à la virgule sur certaines configurations ([nghyd#628](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/628))

#### Changements

* Modifier l'avertissement de limite d'ennoiement de Villemonte ([jalhyd#350](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/350), [nghyd#629](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/629))

#### Changements internes

* CI: change cache strategy for node modules ([jalhyd#358](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/358), [nghyd#630](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/630))

### 4.17.0 - 2023-05-30

#### Nouvelles fonctionnalités

* Structure : Ajout d'une erreur sur l'ennoiement ([jalhyd#302](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/302), [nghyd#614](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/614))
* Conduites en charge : ajout de la loi de Strickler ([jalhyd#215](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/215), [nghyd#596](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/596))
* Ajout d'une redirection vers https pour les adresses http ([nghyd#587](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/587))
* Courbe de remous : rendre facultatif l'une des deux conditions limites en cote ([jalhyd#343](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/343), [nghyd#610](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/610))
* Courbe de remous: visualiser les profils de sections ([nghyd#496](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/496))
* Courbe de remous: renommer la ligne d'eau en ZW et fournir le tirant d'eau d'après celle ci ([jalhyd#333](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/333))

#### Changements

* Prébarrages : interdire de supprimer le dernier bassin ([nghyd#582](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/582))
* PAB: Alignement à droite des cellules numériques dans le tableau des bassins et cloisons ([nghyd#583](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/583))
* PAB: Optimisation du tableau : déplacement de la colonne "cote de radier amont" ([nghyd#615](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/615))

#### Correction de bogues

* Documentation : Les formules de math ne s'affichent pas dans la version anglaise ([nghyd#608](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/608))
* Perte de charge : les paramètres de loi ne sont pas modifiés quand on change le type de perte ([nghyd#611](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/611))
* Solveur multimodule : le module existe toujours après suppression ([jalhyd#342](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/342), [nghyd#601](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/601))
* Notes de session : la note ne s'affiche pas directement ([nghyd#602](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/602))
* PWA : l'application ne se met pas à jour ([nghyd#604](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/604))
* Traduction des résultats : tous les libellés ne sont pas modifiés quand on change de langue ([nghyd#586](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/586))
* Un paramètre cible d'un lien ne doit pas se lier à un autre paramètre ([jalhyd#341](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/341), [nghyd#605](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/605))
* Application PWA inaccessible hors ligne ([nghyd#588](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/588))
* Régression : le fichier de session n'enregistre plus le type de section ([nghyd#592](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/592))
* PAB : la précision d'affichage a une influence sur la valeur des paramètres ([nghyd#543](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/543))
* Le bouton calculer est activé malgré un champ en erreur ([nghyd#616](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/616))
* Solveur multimodule : impossibilité d'utilisation sur un seul module  ([nghyd#606](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/606))
* PreBarrage: Changement intempestif des paramètres d'ouvrage au changement d'équation ([nghyd#620](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/620))
* PreBarrage: il n'y a plus aucun résultat au niveau des cloisons ([nghyd#619](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/619))
* Passe à macrorugosités: des champs ne sont pas liables avec le module "Concentration de blocs" ([jalhyd#345](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/345))
* Passe à macrorugosité: la largeur doit avoir un centimètre de tolérance ([jalhyd#344](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/344))
* Structure: le chargement d'une session loi d'ouvrages avec Q varié remet Q en mode fixé ([nghyd#603](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/603))
* Structure: résultat du calcul de la cote amont dépendant de la cote initiale pour un débit nul ([jalhyd#219](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/219))
* Passe à bassins : message non défini dans la légende des graphiques ([nghyd#584](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/584))
* Les résultats ne sont pas réinitialisés quand on modifie des paramètres globaux ([jalhyd#331](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/331), [nghyd#574](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/574))
* PreBarrage: Doublement des avertissements ([jalhyd#348](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/348))

#### Documentation

* Passe à ralentisseurs : ajouter de liens vers les pages de documentation des types de passes ([nghyd#598](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/598))
* Perte de charge : documentation du module ([nghyd#597](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/597))
* Modification de la documentation sur le coefficient de débit d'une fente ([nghyd#595](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/595))
* Ajout d'une documentation pour l'installation de Cassiopée PWA ([nghyd#617](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/617))
* Perte de charge: Il manque l'aide dans le module perte de charge ([nghyd#593](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/593))
* Ajouter le numéro de version de Cassiopée sur la documentation ([nghyd#578](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/578))

#### Changements internes

* Restructurer Lechapt et Calmon pour de nouvelles lois de pertes de charge ([jalhyd#334](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/334), [jalhyd#590](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/590), [nghyd#585](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/585))
* Migration des tests e2e vers WebDriverIO ([nghyd#618](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/618))
* Documentation: localisation des dépendances javascript dans un seul dossier ([nghyd#612](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/612))
* Optimiser l'affichage des unités ([jalhyd#338](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/338))
* Fusionner les "select" avec "source" et les "select_custom" ([jalhyd#328](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/328), [nghyd#483](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/483))
* Docker: Supprimer les dépendances à l'application Android ([cassiopee2-integration#12](https://forgemia.inra.fr/cassiopee/cassiopee2-integration/-/issues/12))
* Supprimer les dépendances et la chaîne de compilation pour l'application Android ([nghyd#580](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/580))
* Suppression des warnings à la compilation ([nghyd#579](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/579))
* Tests E2E: Vérifier la cohérence entre le json de description des calculettes et le flag visible des paramètres ([nghyd#550](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/550))
* Thème Angular Material personnalisé : avertissements dart-sass à la compilation ([nghyd#414](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/414))

### 4.16.3 - 2023-01-11

#### Correction de bogues

* Lechapt et Calmon : erreur de sélection de matériau ([jalhyd#337](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/337), [nghyd#589](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/589))

### 4.16.1 - 2022-11-16

#### Correction de bogues

* Un paramètre lié ne change pas d'état après la suppression du module cible ([jalhyd#329](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/329), [nghyd#571](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/571))
* Le mode lié d'un paramètre de section est perdu quand on change le type de section ([jalhyd#329](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/329), [nghyd#572](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/572))

#### Documentation

* Rajouter François Grand comme auteur de la documentation PDF ([nghyd#573](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/573))
* Ajouter les références pour les schémas des lois d'ouvrages ([nghyd#575](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/575))

### 4.16.0 - 2022-10-12 (Anguilla anguilla)

#### Nouvelles fonctionnalités

* PAB : ajout de la charge et l'ennoiement dans le tableau de résultat et l'export ([jalhyd#324](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/324), [nghyd#518](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/518))
* Courbe de remous (et bief) : remontée d'une erreur quand le pas de discrétisation est supérieur à la longueur du bief ([jalhyd#316](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/316), [nghyd#565](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/565))
* Section paramétrée : profil de section : option axes orthonormés ([nghyd#497](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/497),[nghyd#568](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/568))
* URL de routeur "/loadsession" pour charger un exemple ([nghyd#476](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/476))
* Deploy devel branch on cassiopee-dev.g-eau.fr ([nghyd#564](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/564))

#### Correction de bogues

* Les caractères UTF8 ne sont pas imprimés dans la doc PDF ([nghyd#556](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/556))
* PréBarrages: La sélection de l'amont ou l'aval n'est pas visible au premier clic ([nghyd#560](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/560))
* Solveur: le paramètre recherché n'est pas conservé ([nghyd#555](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/555))
* PAB: Bugs de format du tableau NgPrime ([nghyd#562](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/562))
* Section paramétrée: crash de l'appli sur variation de paramètre ([jalhyd#319](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/319), [nghyd#561](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/561))
* Module avec une section : le mode champs vide ne fonctionne pas ([jalhyd#327](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/327), [nghyd#569](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/569))
* Déplacement du paramètre calculé lors de la duplication d'un Nub ([jalhyd#322](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/322), [nghyd#567](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/567))
* Lois d'ouvrages : mauvaise gestion du paramètre calculé sur suppression d'ouvrage ([jalhyd#321](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/321), [nghyd#566](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/566))

#### Changements

* Ouvrages: modification des types d'ouvrages (ajout de seuil/orifice rectangulaire, vanne rectangulaire renommée en vanne de fond rectangulaire) ([jalhyd#326](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/326), [nghyd#511](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/511))
* Prébarrages : regroupement de la saisie des bassins ([nghyd#522](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/522))

#### Documentation

* corrections diverses ([nghyd#559](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/559))
* MacroRugo : ajout d'un schéma rugosité de fond ([nghyd#524](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/524))
* Lois d'ouvrages : définition seuil mince/épais ([nghyd#514](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/514))
* Ajout d'un tableau synthétiques des lois d'ouvrages ([nghyd#513](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/513))
* MAJ de la documentation des grilles avec les données de Lemkecher et al. (2020) ([nghyd#438](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/438))

#### Changements internes

* Mise à jour vers Angular 14 ([nghyd#500](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/500))
* CI : MAJ de l'image Docker vers Debian Bullseye compatibilité TLS) ([cassiopee2-integration#10](https://forgemia.inra.fr/cassiopee/cassiopee2-integration/-/issues/10))
* Angular : compilation avec Ivy ([nghyd#369](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/369))
* Déplacer le répertoire Jalhyd dans celui de Nghyd ([nghyd#558](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/558))
* Déménagement de l'intégration continue sur les serveurs gitlab à Lyon ([nghyd#557](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/557))
* Mise à jour de Chartjs ([nghyd#554](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/554))
* MAJ vers PrimeNG 10 ([nghyd#481](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/481))

### 4.15.1 - 2022-07-04

#### Nouvelles fonctionnalités

* Structure: Modification de l'avertissement ennoiement ([jalhyd#314](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/314), [nghyd#520](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/520))
* Dialogue de paramètre variable : pouvoir valider avec la touche entrée ([nghyd#541](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/541))
* Journal de calcul repliable ([nghyd#519](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/519))

#### Changements

* MacroRugo: changer cote de radier par cote de fond ([nghyd#523](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/523))
* PAB, MacroRugo complexe et Prébarrages: modifier le message d'erreur synthétique ([nghyd#517](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/517))
* Error 404 on language files load ([nghyd#499](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/499))

#### Correction de bogues

* Plantage PAB si un paramètre est passé en mode variable puis annulé ([nghyd#549](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/549))
* Liens inaccessibles pour certains modules ([jalhyd#289](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/289), [nghyd#547](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/547))
* Plantage du calcul sur modules liés ([jalhyd#286](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/286))
* PréBarrages: les champs ne sont pas vides à la création du module ([jalhyd#310](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/310), [nghyd#546](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/546))
* L'annulation de la saisie du mode "Varier" mémorise les valeurs non valides ([nghyd#545](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/545))
* PréBarrages: les changements de couleur du schéma ne sont pas instantanées ([nghyd#544](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/544))
* Crash sur annulation du dialogue d'édition du paramètre variable pour un paramètre initialement en calcul ([nghyd#542](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/542))
* Le passage en mode varier devrait systématiquement ouvrir la boite de dialogue ([nghyd#537](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/537))
* Lois d'ouvrages: les champs ne sont pas vide à l'ajout d'un ouvrage ([nghyd#536](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/536))
* PAR Calage et Simulation: répétition des paramètres dans le résultat ([nghyd#535](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/535))
* PAB nombre: mauvaise colonne de résultat ([jalhyd#304](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/304), [nghyd#534](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/534))
* MacroRugo complexe: le graphique des vitesses moyennes entre les blocs ne s'affiche pas ([nghyd#533](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/533))
* Cote amont/aval de bief: le bouton "détail d'une section hydraulique" ne fonctionne pas toujours ([nghyd#504](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/504), [jalhyd#311](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/311))
* PréBarrages: les valeurs erronées ne sont pas conservées ([nghyd#501](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/501))
* Solveur multimodule: le choix du paramètre recherché n'est pas maintenu à l'écran ([nghyd#486](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/486))
* Log : améliorer la synthèse de journal ([jalhyd#308](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/308))
* Les liens erronés sont remplacés par d'autres liens ([nghyd#551](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/551))
* PAB: Lancement du calcul possible avec des champs invalides ([nghyd#552](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/552), [jalhyd#317](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/317))

#### Changements internes

* Path error in stable deployment version on the dev server [nghyd#540](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/540)

### 4.15.0 - 2022-05-04 (Salmo trutta)

#### Nouvelles fonctionnalités

* PAB : Variation du débit d'attrait ([nghyd#431](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/431))
* Ajouter un bouton "Annuler" sur la saisie des paramètres variables ([jalhyd#300](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/300), [nghyd#507](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/507))
* Prébarrages : mettre les enfants invalides en rouge dans le schéma ([jalhyd#298](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/298), [nghyd#484](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/484))

#### Changements

* Fente Larinier : laisser le coefficient de débit vide ([nghyd#515](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/515))
* Cloisons : Générer une PAB : vider les champs ([jalhyd#306](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/306), [nghyd#516](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/516))

#### Correction de bogues

* Courbe de remous: crash de l'application sur données erronées ([jalhyd#307](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/307), [nghyd#532](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/532)) Deux bugs en un, l'appli crashe quand :
  - la hauteur de berge dépasse une certaine valeur avec des paramètres corrects pour effectuer un calcul (par exemple les valeurs par défaut)
  - les deux cotes de l'eau se situent sous les cotes de fond amont et aval
* Sections : non convergence du calcul du tirant d'eau critique ([jalhyd#301](https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/301), [nghyd#528](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/528))
* Remettre le paramètre dans son état initial quand le dialogue "Varier" est annulé ([nghyd#508](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/508))
* Prébarrages: les champs ne sont pas vides lors des ajouts de bassins et cloisons ([nghyd#503](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/503))
* Mode "champs vides par défaut" : changer le type d'un ouvrage (ex: dans Cloisons) remplit les champs ([nghyd#480](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/480))
* PréBarrages : perte du focus lorsqu'on édite un paramètre d'un enfant (cloison ou bassin) ([nghyd#469](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/469))

#### Documentation

* Cloisons : il manque l'aide contextuelle pour les lois de débit ([nghyd#529](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/529))
* Documentation : corrections ([nghyd#498](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/498))

#### Changements internes

* Nightly build: clean folder before installation ([nghyd#495](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/495))
* Transfert du site de production sur OVH ([nghyd#505](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/505))
* Plantage des tests e2e sur le chargement des exemples ([nghyd#530](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/530), [nghyd#531](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/531))
  - Les champs des exemples chargés sont vides lorsque le mode "champ vides" est activé.
  - Les tests e2e plantent par manque de temporisation
* CI : les jobs build en schedule de master et devel plantent ([nghyd#527](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/527))
* CI : affiner la gestion du cache ([nghyd#526](https://forgemia.inra.fr/cassiopee/nghyd/-/issues/526))

### 4.14.2 - 2021-03-25

#### Nouvelles fonctionnalités

* Passe à macro-rugosité: Retour aux formules utilisées dans la v4.13.1 ([jalhyd#297](https://forgemia.inra.fr/cassiopee/jalhyd/issues/297), [nghyd#493](https://forgemia.inra.fr/cassiopee/nghyd/issues/493))

#### Correction de bogues

* Passe à macro-rugosité: error de calcul de la vitesse max ([jalhyd#294](https://forgemia.inra.fr/cassiopee/jalhyd/issues/294))
* Graphiques: lorsqu'on relance un calcul les axes ne se mettent pas à jour ([nghyd#489](https://forgemia.inra.fr/cassiopee/nghyd/issues/489))

#### Documentation

* Macrorugo : Documentation de Cd0 avec schéma ([nghyd#492](https://forgemia.inra.fr/cassiopee/nghyd/issues/492))

### 4.14.1 - 2021-02-17

#### Nouvelles fonctionnalités

* Passe à macro-rugosité: Changer Cd0 pour Cx et ajuster Cd0 aux données expérimentales ([jalhyd#291](https://forgemia.inra.fr/cassiopee/jalhyd/issues/291))

#### Correction de bogues

* Vérificateur de passe: message erroné pour les passes à macro-rugosités submergées ([jalhyd#292](https://forgemia.inra.fr/cassiopee/jalhyd/issues/292))

### 4.14.0 - 2021-02-16 (Scomber scombrus)

#### Nouvelles fonctionnalités

* Passe à macro-rugosité: Mise à jour des formules de calcul ([jalhyd#283](https://forgemia.inra.fr/cassiopee/jalhyd/issues/283))
* Passe à macro-rugosité: Ajout de la vitesse moyenne entre les blocs ([jalhyd#285](https://forgemia.inra.fr/cassiopee/jalhyd/issues/285))
* Passe à macro-rugosité: Ajout du Strickler équivalent dans les résultats liables à des Strickler ([jalhyd#287](https://forgemia.inra.fr/cassiopee/jalhyd/issues/287))
* Passe à macro-rugosité: ajout d'un avertissement pour les concentrations en dehors de l'intervalle validé par les expérimentations ([jalhyd#284](https://forgemia.inra.fr/cassiopee/jalhyd/issues/284))
* Vérificateur: Les passes à macro-rugosité submergées sont non franchissables ([jalhyd#290](https://forgemia.inra.fr/cassiopee/jalhyd/issues/290))

#### Correction de bogues

* Passe à macro-rugosité: Calcul de la puissance dissipée erroné ([jalhyd#282](https://forgemia.inra.fr/cassiopee/jalhyd/issues/282))
* Crash au chargement d'un module contenant une parenthèse dans son nom ([nghyd#487](https://forgemia.inra.fr/cassiopee/nghyd/issues/487))
* Electron: l'icône de l'application n'est plus reconnue ([nghyd#485](https://forgemia.inra.fr/cassiopee/nghyd/issues/485))

#### Documentation

* Documentation du solveur multi-module et des modules de calcul mathématiques ([nghyd#433](https://forgemia.inra.fr/cassiopee/nghyd/issues/433))
* Macrorugo : documenter le coefficient de forme Cd0 ([nghyd#477](https://forgemia.inra.fr/cassiopee/nghyd/issues/477))
* MacroRugo: erreur de formule de correction de Cd dans la documentation ([nghyd#488](https://forgemia.inra.fr/cassiopee/nghyd/issues/488))
* Documentation du module de calcul de la cote amont / aval d'un bief ([nghyd#490](https://forgemia.inra.fr/cassiopee/nghyd/issues/490))

### 4.13.1 - 2020-10-02

#### Correction de bogues

* PreBarrage: Distribution des débits erronée sur exemple simple ([jalhyd#279](https://forgemia.inra.fr/cassiopee/jalhyd/issues/279))
* PreBarrage: erreur de calcul sur Z2 > Z1 initiale ([jalhyd#280](https://forgemia.inra.fr/cassiopee/jalhyd/issues/280))
* Prebarrage : avec Chrome (et electron) le schéma est mal rendu ([nghyd#482](https://forgemia.inra.fr/cassiopee/nghyd/issues/482))

### 4.13.0 - 2020-09-24 (Michel Larinier)

#### Nouvelles fonctionnalités

* Module Pré-barrage ([jalhyd#32](https://forgemia.inra.fr/cassiopee/jalhyd/issues/32), [jalhyd#269](https://forgemia.inra.fr/cassiopee/jalhyd/issues/269), [jalhyd#268](https://forgemia.inra.fr/cassiopee/jalhyd/issues/268), [jalhyd#243](https://forgemia.inra.fr/cassiopee/jalhyd/issues/243), [jalhyd#246](https://forgemia.inra.fr/cassiopee/jalhyd/issues/246), [nghyd#395](https://forgemia.inra.fr/cassiopee/nghyd/issues/395), [nghyd#430](https://forgemia.inra.fr/cassiopee/nghyd/issues/430), [nghyd#456](https://forgemia.inra.fr/cassiopee/nghyd/issues/456), [nghyd#455](https://forgemia.inra.fr/cassiopee/nghyd/issues/455), [jalhyd#275](https://forgemia.inra.fr/cassiopee/jalhyd/issues/275), [jalhyd#276](https://forgemia.inra.fr/cassiopee/jalhyd/issues/276), [jalhyd#277](https://forgemia.inra.fr/cassiopee/jalhyd/issues/277), [jalhyd#278](https://forgemia.inra.fr/cassiopee/jalhyd/issues/278), [nghyd#452](https://forgemia.inra.fr/cassiopee/nghyd/issues/452), [nghyd#470](https://forgemia.inra.fr/cassiopee/nghyd/issues/470), [nghyd#451](https://forgemia.inra.fr/cassiopee/nghyd/issues/451))
* Simplification de l'architecture des composants de résultats Angular ([nghyd#418](https://forgemia.inra.fr/cassiopee/nghyd/issues/418), [nghyd#466](https://forgemia.inra.fr/cassiopee/nghyd/issues/466), [nghyd#465](https://forgemia.inra.fr/cassiopee/nghyd/issues/465))
* Modifier les titres et descriptions de "Passes à bassins" et "Passes à macro-rugosités" ([nghyd#478](https://forgemia.inra.fr/cassiopee/nghyd/issues/478))
* Ajouter des mots-clés "maths" pour les outils mathématiques (moteur de recherche) ([nghyd#474](https://forgemia.inra.fr/cassiopee/nghyd/issues/474))

#### Correction de bogues

* Electron : la détection de mise à jour disponible ne fonctionne plus ([nghyd#462](https://forgemia.inra.fr/cassiopee/nghyd/issues/462))
* Débit lié au débit en calcul d'un PréBarrage : erreur dans CalcSerie() ([jalhyd#274](https://forgemia.inra.fr/cassiopee/jalhyd/issues/274))
* Cloisons : changer le type d'ouvrage pour Seuil Triangulaire casse le fieldset ([nghyd#479](https://forgemia.inra.fr/cassiopee/nghyd/issues/479))

#### Documentation

* Prébarrages ([nghyd#467](https://forgemia.inra.fr/cassiopee/nghyd/issues/467))
* Aide du Jet / de la pente : mentionner l'inversion de la pente pour le module Jet ([nghyd#475](https://forgemia.inra.fr/cassiopee/nghyd/issues/475))
* Ajouter à l'accueil de la documentation un chapitre "contact, bugs, remarques…" ([nghyd#472](https://forgemia.inra.fr/cassiopee/nghyd/issues/472))

### 4.12.1 - 2020-09-15

#### Correction de bogues

* Lien vers la documentation cassé sur Chrome et Edge ([nghyd#458](https://forgemia.inra.fr/cassiopee/nghyd/issues/458))
* Macrorugo : en mode "champs vides par défault", L est en calcul avec une valeur initiale vide ([nghyd#459](https://forgemia.inra.fr/cassiopee/nghyd/issues/459))
* Grille : différencier Ob de O pour les grilles inclinées ([jalhyd#273](https://forgemia.inra.fr/cassiopee/jalhyd/issues/273))
* Fermeture d'un module Jet calculé : plante l'application ([nghyd#460](https://forgemia.inra.fr/cassiopee/nghyd/issues/460))
* Déversoir dénoyé : Infinity pour des largeurs de lit faible et sur variation ([jalhyd#272](https://forgemia.inra.fr/cassiopee/jalhyd/issues/272))

#### Documentation

* Lechapt-Calmon : documentation du coefficient de pertes de charges singulières ([nghyd#338](https://forgemia.inra.fr/cassiopee/nghyd/issues/338))

### 4.12.0 - 2020-09-09 (Les critères de Francis Blanche - ils peuvent le faire !)

#### Nouvelles fonctionnalités

* Vérification des critères de franchissement des passes à poissons ([jalhyd#204](https://forgemia.inra.fr/cassiopee/jalhyd/issues/204), [nghyd#60](https://forgemia.inra.fr/cassiopee/nghyd/issues/60), [jalhyd#236](https://forgemia.inra.fr/cassiopee/jalhyd/issues/236), [jalhyd#251](https://forgemia.inra.fr/cassiopee/jalhyd/issues/251), [jalhyd#238](https://forgemia.inra.fr/cassiopee/jalhyd/issues/238), [jalhyd#252](https://forgemia.inra.fr/cassiopee/jalhyd/issues/252), [jalhyd#250](https://forgemia.inra.fr/cassiopee/jalhyd/issues/250), [jalhyd#258](https://forgemia.inra.fr/cassiopee/jalhyd/issues/258), [jalhyd#247](https://forgemia.inra.fr/cassiopee/jalhyd/issues/247), [jalhyd#239](https://forgemia.inra.fr/cassiopee/jalhyd/issues/239), [jalhyd#249](https://forgemia.inra.fr/cassiopee/jalhyd/issues/249), [jalhyd#248](https://forgemia.inra.fr/cassiopee/jalhyd/issues/248), [jalhyd#254](https://forgemia.inra.fr/cassiopee/jalhyd/issues/254), [jalhyd#235](https://forgemia.inra.fr/cassiopee/jalhyd/issues/235), [jalhyd#237](https://forgemia.inra.fr/cassiopee/jalhyd/issues/237), [nghyd#402](https://forgemia.inra.fr/cassiopee/nghyd/issues/402), [jalhyd#216](https://forgemia.inra.fr/cassiopee/jalhyd/issues/216), [nghyd#426](https://forgemia.inra.fr/cassiopee/nghyd/issues/426))
* Moteur de recherche sur la page d'accueil ([nghyd#428](https://forgemia.inra.fr/cassiopee/nghyd/issues/428))
* Améliorer le système de traduction ([nghyd#223](https://forgemia.inra.fr/cassiopee/nghyd/issues/223))
* Labels des paramètres : lire l'unité dans le modèle et non dans les fichiers de traduction ([nghyd#417](https://forgemia.inra.fr/cassiopee/nghyd/issues/417))
* Cloisons : remplacement de la loi Cunge80 par la loi CEM88D ([jalhyd#264](https://forgemia.inra.fr/cassiopee/jalhyd/issues/264))

#### Correction de bogues

* Cordova : la notification de mise à jour ne fonctionne plus ([nghyd#436](https://forgemia.inra.fr/cassiopee/nghyd/issues/436))
* Conditionner le passage en mode CALC aux liens déjà définis, pour éviter les boucles ([nghyd#181](https://forgemia.inra.fr/cassiopee/nghyd/issues/181))
* Solveur : le paramètre recherché, si c'est un extraResult, ne s'initialise pas correctement lors du chargement d'une session ([jalhyd#263](https://forgemia.inra.fr/cassiopee/jalhyd/issues/263))
* Solveur : this.prms.X is undefined ([jalhyd#262](https://forgemia.inra.fr/cassiopee/jalhyd/issues/262))
* Exemple "débit d'un chenal avec ouvrages" : plusieurs bugs ([nghyd#446](https://forgemia.inra.fr/cassiopee/nghyd/issues/446))
* PAR, générer une simulation à partir d'un calage : NaN ([nghyd#447](https://forgemia.inra.fr/cassiopee/nghyd/issues/447))
* Paramètres liés d'un enfant à l'autre d'un même module : boucle infinie si la source varie ([nghyd#444](https://forgemia.inra.fr/cassiopee/nghyd/issues/444))
* JaLHyd : dans createStructure(), définir automatiquement le structureType en fonction de la loiDebit
* Lors de la vérification d'une passe à bassins variée, bug sur la vérification de charge minimale ([jalhyd#265](https://forgemia.inra.fr/cassiopee/jalhyd/issues/265))
* PAB, calcul de cloison qui échoue : l'erreur ne dit pas quelle cloison est en cause ([jalhyd#267](https://forgemia.inra.fr/cassiopee/jalhyd/issues/267))
* Vérification d'une PAB variée : erreur dans la vérification des critères obligatoires ([jalhyd#266](https://forgemia.inra.fr/cassiopee/jalhyd/issues/266))
* Vérification de PAB : rendre la largeur minimale d'échancrure obligatoire ([jalhyd#270](https://forgemia.inra.fr/cassiopee/jalhyd/issues/270))

#### Documentation

* Documentation de la vérification des passes ([nghyd#434](https://forgemia.inra.fr/cassiopee/nghyd/issues/434))
* Traduire la documentation des PAR ([nghyd#443](https://forgemia.inra.fr/cassiopee/nghyd/issues/443))
* Harmonisation de l'indentation des fichiers ([nghyd#409](https://forgemia.inra.fr/cassiopee/nghyd/issues/409))

#### Mises à jour de dépendances

* Angular 10
* Cordova 10
* Electron 10
* Mathjax 3 ([nghyd#416](https://forgemia.inra.fr/cassiopee/nghyd/issues/416))

### 4.11.1 - 2020-08-11

#### Nouvelles fonctionnalités

* Lois d'ouvrages: ajouter le n° d'ouvrage dans les logs ([jalhyd#260](https://forgemia.inra.fr/cassiopee/jalhyd/issues/260), [nghyd#442](https://forgemia.inra.fr/cassiopee/nghyd/issues/442))

#### Correction de bogues

* Erreur de formulation de la loi de Cunge en orifice dénoyé ([jalhyd#259](https://forgemia.inra.fr/cassiopee/jalhyd/issues/259))
* Définition de la valeur initiale d'un calcul ([nghyd#440](https://forgemia.inra.fr/cassiopee/nghyd/issues/440))
* Structure et Dever : exposer les résultats pour liage ([jalhyd#255](https://forgemia.inra.fr/cassiopee/jalhyd/issues/255))
* Déversoirs dénoyés: Ajouter les liens vers les lois de débit ([nghyd#437](https://forgemia.inra.fr/cassiopee/nghyd/issues/437))

#### Documentation

* Mise à jour de la documentation de la loi de Cunge ([nghyd#441](https://forgemia.inra.fr/cassiopee/nghyd/issues/441))

### 4.11.0 - 2020-07-28 (Puisque tu PAR)

#### Nouvelles fonctionnalités

* Calage d'une passe à ralentisseurs ([jalhyd#34](https://forgemia.inra.fr/cassiopee/jalhyd/issues/34), [jalhyd#223](https://forgemia.inra.fr/cassiopee/jalhyd/issues/223), [jalhyd#225](https://forgemia.inra.fr/cassiopee/jalhyd/issues/225), [jalhyd#226](https://forgemia.inra.fr/cassiopee/jalhyd/issues/226), [jalhyd#232](https://forgemia.inra.fr/cassiopee/jalhyd/issues/232), [jalhyd#233](https://forgemia.inra.fr/cassiopee/jalhyd/issues/233), [jalhyd#234](https://forgemia.inra.fr/cassiopee/jalhyd/issues/234), [jalhyd#240](https://forgemia.inra.fr/cassiopee/jalhyd/issues/240), [nghyd#365](https://forgemia.inra.fr/cassiopee/nghyd/issues/365), [nghyd#394](https://forgemia.inra.fr/cassiopee/nghyd/issues/394), [nghyd#408](https://forgemia.inra.fr/cassiopee/nghyd/issues/408), [nghyd#422](https://forgemia.inra.fr/cassiopee/nghyd/issues/422), [nghyd#423](https://forgemia.inra.fr/cassiopee/nghyd/issues/423), [nghyd#424](https://forgemia.inra.fr/cassiopee/nghyd/issues/424), [nghyd#425](https://forgemia.inra.fr/cassiopee/nghyd/issues/425))
* Simulation d'une passe à ralentisseurs ([jalhyd#201](https://forgemia.inra.fr/cassiopee/jalhyd/issues/201), [jalhyd#229](https://forgemia.inra.fr/cassiopee/jalhyd/issues/229), [nghyd#366](https://forgemia.inra.fr/cassiopee/nghyd/issues/366), [nghyd#382](https://forgemia.inra.fr/cassiopee/nghyd/issues/382), [nghyd#394](https://forgemia.inra.fr/cassiopee/nghyd/issues/394), [nghyd#425](https://forgemia.inra.fr/cassiopee/nghyd/issues/425))
* Bief : lier les paramètres de section pour les Sections Paramétrées générées ([nghyd#380](https://forgemia.inra.fr/cassiopee/nghyd/issues/380))
* Permettre de lier des paramètres de sections de types identiques, sans utiliser les familles ([jalhyd#203](https://forgemia.inra.fr/cassiopee/jalhyd/issues/203), [nghyd#379](https://forgemia.inra.fr/cassiopee/nghyd/issues/379))

#### Correction de bogues

* Lien entre deux paramètres de section : la valeur n'apparaît pas dans le tableau de résultats ([nghyd#381](https://forgemia.inra.fr/cassiopee/nghyd/issues/381))
* Désactiver le suivi Matomo lorsqu'Angular n'est pas en mode "prod" ([nghyd#412](https://forgemia.inra.fr/cassiopee/nghyd/issues/412))
* Corriger le coefficient de débit de la vanne submergée ([jalhyd#231](https://forgemia.inra.fr/cassiopee/jalhyd/issues/231), [nghyd#421](https://forgemia.inra.fr/cassiopee/nghyd/issues/421))
* Cloisons : une pelle (négative) est calculée pour les orifices, ce qui donne lieu à des avertissements ([jalhyd#242](https://forgemia.inra.fr/cassiopee/jalhyd/issues/242))
* Empêcher de créer des liens vers des paramètre invisibles ([jalhyd#244](https://forgemia.inra.fr/cassiopee/jalhyd/issues/244))
* MRC : après un calcul varié, il n'y a plus d'eau sur certains radiers ([jalhyd#253](https://forgemia.inra.fr/cassiopee/jalhyd/issues/253), [nghyd#432](https://forgemia.inra.fr/cassiopee/nghyd/issues/432))

#### Documentation

* Passes à ralentisseurs ([nghyd#398](https://forgemia.inra.fr/cassiopee/nghyd/issues/398))

### 4.10.6 - 2020-07-21

#### Nouvelles fonctionnalités

* Transférer les fonctionnalités de cassiopee-2-integration dans Gitlab-CI ([nghyd#374](https://forgemia.inra.fr/cassiopee/nghyd/issues/374))

#### Correction de bogues

* Lechapt-Calmon : ne plus proposer de matériau "NONE"  ([jalhyd#230](https://forgemia.inra.fr/cassiopee/jalhyd/issues/230))

#### Documentation

* Renommer les lois d'ouvrage ([nghyd#419](https://forgemia.inra.fr/cassiopee/nghyd/issues/419))
* Erreur dans l'équation de Cunge
* Corrections mineures sur Vanne Dénoyée

### 4.10.5 - 2020-06-30

#### Nouvelles fonctionnalités

* Renommer les lois triangulaires "dénoyées" en "(Villemonte)" ([jalhyd#210](https://forgemia.inra.fr/cassiopee/jalhyd/issues/210), [nghyd#393](https://forgemia.inra.fr/cassiopee/nghyd/issues/393))
* Lois d'ouvrages: ajout du seuil triangulaire épais ([jalhyd#211](https://forgemia.inra.fr/cassiopee/jalhyd/issues/211), [nghyd#399](https://forgemia.inra.fr/cassiopee/nghyd/issues/399))
* Régime Uniforme, conduite circulaire: provoquer une erreur fatale si la conduite est en charge ([jalhyd#214](https://forgemia.inra.fr/cassiopee/jalhyd/issues/214), [nghyd#406](https://forgemia.inra.fr/cassiopee/nghyd/issues/406))
* Cunge 1980 : ajout dans les cloisons et modification du coefficient de débit à 1 ([jalhyd#220](https://forgemia.inra.fr/cassiopee/jalhyd/issues/220), [jalhyd#221](https://forgemia.inra.fr/cassiopee/jalhyd/issues/221),  [nghyd#404](https://forgemia.inra.fr/cassiopee/nghyd/issues/404))

#### Correction de bogues

* PAB : résultats cassés ([nghyd#392](https://forgemia.inra.fr/cassiopee/nghyd/issues/392))
* PAM : Supprimer le lien rugosité de fond ([nghyd#391](https://forgemia.inra.fr/cassiopee/nghyd/issues/391))
* Cloisons : avertissement si les cotes de radier des seuils se situent sous la cote de radier du bassin amont ([jalhyd#217](https://forgemia.inra.fr/cassiopee/jalhyd/issues/217))
* Absence d'erreur en cas de code de langue manquant dans les listes déroulantes ([nghyd#400](https://forgemia.inra.fr/cassiopee/nghyd/issues/400))
* Contrôler le domaine de définition lors de l'affectation de .singleValue ([jalhyd#218](https://forgemia.inra.fr/cassiopee/jalhyd/issues/218))
* Ouverture de vanne et liens : bug sur longueur du paramètre varié ([jalhyd#222](https://forgemia.inra.fr/cassiopee/jalhyd/issues/222))
* Exemple "Longueur de jet d'un déversoir" cassé ([jalhyd#224](https://forgemia.inra.fr/cassiopee/jalhyd/issues/224))
* Exemple 3 : l'affichage de graphique clignote ([nghyd#407](https://forgemia.inra.fr/cassiopee/nghyd/issues/407))
* Définition de la pente ([jalhyd#212](https://forgemia.inra.fr/cassiopee/jalhyd/issues/212))

#### Documentation

* Documentation de la pente ([nghyd#397](https://forgemia.inra.fr/cassiopee/nghyd/issues/397))
* Documentation de la loi Cunge80 ([nghyd#403](https://forgemia.inra.fr/cassiopee/nghyd/issues/403))
* Documentation des grilles: il manque la définition des variables ([nghyd#401](https://forgemia.inra.fr/cassiopee/nghyd/issues/401))

### 4.10.4 - 2020-04-17

#### Nouvelles fonctionnalités

* Régime uniforme: ajouter un bouton pour créer une section paramétrée ([nghyd#386](https://forgemia.inra.fr/cassiopee/nghyd/issues/386))
* Cordova : notifications de mise à jour ([nghyd#384](https://forgemia.inra.fr/cassiopee/nghyd/issues/384))

#### Correction de bogues

* Section paramétrée: le tirant d'eau critique ne converge pas sur une section circulaire fermée ([jalhyd#209](https://forgemia.inra.fr/cassiopee/jalhyd/issues/209))
* Régime uniforme: erreur de calcul de la vitesse ([jalhyd#206](https://forgemia.inra.fr/cassiopee/jalhyd/issues/206), [jalhyd#207](https://forgemia.inra.fr/cassiopee/jalhyd/issues/207))
* Impact de jet: problème de gestion des erreurs fatales ([jalhyd#205](https://forgemia.inra.fr/cassiopee/jalhyd/issues/205))
* Unité du coefficient de Strickler ([jalhyd#208](https://forgemia.inra.fr/cassiopee/jalhyd/issues/208))
* Robustifier le solveur sur la recherche de l'intervalle de départ ([jalhyd#164](https://forgemia.inra.fr/cassiopee/jalhyd/issues/164))
* Champ vide à la création d'un module: les champs de section ne sont pas vides quand on choisit un type de section ([nghyd#388](https://forgemia.inra.fr/cassiopee/nghyd/issues/388))
* Section paramétrée: Ajouter le tirant d'eau dans le schéma en coupe de la section ([nghyd#389](https://forgemia.inra.fr/cassiopee/nghyd/issues/389))
* Saisie paramètre qui varie: message d'erreur persistant sur le champ min ([nghyd#385](https://forgemia.inra.fr/cassiopee/nghyd/issues/385))
* Cordova : version erronée ([nghyd#383](https://forgemia.inra.fr/cassiopee/nghyd/issues/383))

#### Documentation

* Fusionner "Section paramétrée" et "Variables hydrauliques" ([nghyd#390](https://forgemia.inra.fr/cassiopee/nghyd/issues/390))
* Sections : documentation du champ Hauteur de berge

### 4.10.3 - 2020-03-12

#### Nouvelles fonctionnalités

* Nouveau raccourci clavier Alt+G pour afficher le diagramme des modules
* Diagramme des modules : lien vers les notes
* Test e2e des exemples officiels ([nghyd#373](https://forgemia.inra.fr/cassiopee/nghyd/issues/373))
* Exemples types mis à jour, avec notes

#### Correction de bogues

* Calcul d'un module aval qui casse les résultats du module amont ([nghyd#371](https://forgemia.inra.fr/cassiopee/nghyd/issues/371))
* Astérisques sur les champs non-obligatoires ([nghyd#368](https://forgemia.inra.fr/cassiopee/nghyd/issues/368))
* Exemple "Débit d'un chenal avec ouvrages" : impossible de calculer la cote amont dans le module "Cotes d'un bief" ([jalhyd#202](https://forgemia.inra.fr/cassiopee/jalhyd/issues/202))
* Déversoirs et Lois d'ouvrages : liens erronés vers la documentation du seuil dénoyé
* Jet : ne pas remplir "sous" la ligne de fond, lorsque sa cote est négative ([nghyd#372](https://forgemia.inra.fr/cassiopee/nghyd/issues/372))
* Passage en mode calcul d'un paramètre dont la singleValue est undefined ([nghyd#367](https://forgemia.inra.fr/cassiopee/nghyd/issues/367))
* Marges sur les titres de Fieldset, Fieldset container, PAB Table
* Désactivation des notifications lorsqu'on vide la session ([nghyd#375](https://forgemia.inra.fr/cassiopee/nghyd/issues/375))
* Chargement d'un Solveur avant ses Nubs cibles
* Sélection de la cible du Solveur lorsque le Nub calculé n'a pas de paramètre calculé (ex: Section Paramétrée) ([nghyd#378](https://forgemia.inra.fr/cassiopee/nghyd/issues/378))
* Section Paramétrée : tableau de résultats fixes en plusieurs exemplaires lors de l'utilisation avec le Solveur ([nghyd#377](https://forgemia.inra.fr/cassiopee/nghyd/issues/377))
* Bief : calcul des sections amont et aval ([nghyd#376](https://forgemia.inra.fr/cassiopee/nghyd/issues/376))

### 4.10.2 - 2020-02-25

#### Correction de bogues

* Correction de liens erronés vers la documentation

#### Documentation

* Documentation : ajout de liens vers la page de téléchargement

### 4.10.1 - 2020-02-25

#### Correction de bogues

* Electron : erreur de détection de mise à jour (comparaison chaînes semver)

### 4.10.0 - 2020-02-24 (Langue Hilare Neuve)

#### Nouvelles fonctionnalités

* Solveur : cibler un résultat complémentaire ([nghyd#363](https://forgemia.inra.fr/cassiopee/nghyd/issues/363), [jalhyd#188](https://forgemia.inra.fr/cassiopee/jalhyd/issues/188))
* *Monkey test* sur les interfaces ([nghyd#235](https://forgemia.inra.fr/cassiopee/nghyd/issues/235))
* Lechapt-Calmon : ajouter un avertissement lorsque la vitesse est en dehors de l'intervalle [0\.4, 2] ([jalhyd#192](https://forgemia.inra.fr/cassiopee/jalhyd/issues/192))
* Rendre plus générique la gestion des listes déroulantes ([nghyd#359](https://forgemia.inra.fr/cassiopee/nghyd/issues/359))
* Automatiser les chemins de configuration depuis le CalculatorType ([nghyd#358](https://forgemia.inra.fr/cassiopee/nghyd/issues/358))

#### Correction de bogues

* Certains liens doivent être cliqués deux fois ([nghyd#364](https://forgemia.inra.fr/cassiopee/nghyd/issues/364))
* Solveur : interdire de travailler sur un Nub dont le résultat est varié ([jalhyd#198](https://forgemia.inra.fr/cassiopee/jalhyd/issues/198))
* Paramètre varié et lien à un résultat varié simultanément ([jalhyd#199](https://forgemia.inra.fr/cassiopee/jalhyd/issues/199))
* Parfois lorsqu'on charge une session, le bouton Calculer reste grisé ([nghyd#349](https://forgemia.inra.fr/cassiopee/nghyd/issues/349))
* PAB : parfois le type de jet est undefined ([jalhyd#196](https://forgemia.inra.fr/cassiopee/jalhyd/issues/196))
* Export XLSX : retirer "help" des entêtes de colonnes ([nghyd#360](https://forgemia.inra.fr/cassiopee/nghyd/issues/360))
* Chargement de session : selon l'ordre des paramètres, le paramètre calculé n'est pas correctement défini

#### Documentation

* Générer la documentation en PDF ([nghyd#348](https://forgemia.inra.fr/cassiopee/nghyd/issues/348))

#### Mises à jour de dépendances

* Angular 9 ([nghyd#354](https://forgemia.inra.fr/cassiopee/nghyd/issues/354))
* Typescript 3.7 ([jalhyd#197](https://forgemia.inra.fr/cassiopee/jalhyd/issues/197))
* mise à jour de toutes les dépendances jalhyd/nghyd sauf Mermaid (provoque un bug) et Mathjax (adaptations importantes nécessaires)

### 4.9.0 - 2020-01-15 (On Fusionne Bien)

#### Nouvelles fonctionnalités

* Nouveau module "Concentration de blocs" ([jalhyd#185](https://forgemia.inra.fr/cassiopee/jalhyd/issues/185))
* Lechapt-Calmon : ajout des pertes de charge singulières ([nghyd#352](https://forgemia.inra.fr/cassiopee/nghyd/issues/352), [jalhyd#172](https://forgemia.inra.fr/cassiopee/jalhyd/issues/172))
* Simplification du code des Formulaires ([nghyd#353](https://forgemia.inra.fr/cassiopee/nghyd/issues/353))
* Passage de l'AFB à l'OFB : changement de logo, de nom, d'URL

#### Documentation

* Documentation utilisateurs en anglais ([nghyd#321](https://forgemia.inra.fr/cassiopee/nghyd/issues/321))
* Réorganisation de la documentation utilisateurs ([nghyd#355](https://forgemia.inra.fr/cassiopee/nghyd/issues/355))
* Documentation développeurs ([nghyd#317](https://forgemia.inra.fr/cassiopee/nghyd/issues/317))
* Exemples de code Typescript et Javascript pour le développement d'applications en ligne de commande basées sur JaLHyd
* Diagramme de classes simplifié de JaLHyd

#### Mises à jour de dépendances

* Jasmine 3.5
* Karma 4.4

### 4.8.1 - 2019-12-20

#### Nouvelles fonctionnalités

* Dever: Ajout d'un avertissement si la cote de radier d'un ouvrage est sous la cote de fond du lit ([jalhyd#179](https://forgemia.inra.fr/cassiopee/jalhyd/issues/179))
* Strickler: Ajout d'une aide contextuelle ([nghyd#332](https://forgemia.inra.fr/cassiopee/nghyd/issues/332))
* Grille: permettre les calculs partiels ([nghyd#336](https://forgemia.inra.fr/cassiopee/nghyd/issues/336))
* MacroRugo: Ajout d'avertissement sur l'adéquation taille des cellules - largeur de la rampe ([jalhyd#174](https://forgemia.inra.fr/cassiopee/jalhyd/issues/174))
* Documentation lois d'ouvrages manquantes ([nghyd#342](https://forgemia.inra.fr/cassiopee/nghyd/issues/342))
* Champs vides à la création d'un module ([nghyd#331](https://forgemia.inra.fr/cassiopee/nghyd/issues/331))
* Dever: Calcul du débit corrigé en utilisant la charge dans les formules ([jalhyd#52](https://forgemia.inra.fr/cassiopee/jalhyd/issues/52), [nghyd#345](https://forgemia.inra.fr/cassiopee/nghyd/issues/345))
* Grille: Ajouter le coefficient de forme des barreaux dans les résultats complémentaires ([jalhyd#178](https://forgemia.inra.fr/cassiopee/jalhyd/issues/178))
* Grille: Ajouter un profil de barreaux personnalisé ([nghyd#334](https://forgemia.inra.fr/cassiopee/nghyd/issues/334))
* PAB: Ajouter la position du radier des seuils sur le graphique du profil en long ([jalhyd#171](https://forgemia.inra.fr/cassiopee/jalhyd/issues/171))
* Impact de jet: Modification des champs de hauteurs ([jalhyd#181](https://forgemia.inra.fr/cassiopee/jalhyd/issues/181))
* Hydraulique à surface libre: ajouter un avertissement quand ça déborde ([jalhyd#180](https://forgemia.inra.fr/cassiopee/jalhyd/issues/180))
* Cloisons: Ajouter le calcul de la pelle ([jalhyd#169](https://forgemia.inra.fr/cassiopee/jalhyd/issues/169))
* MacroRugoCompound: radier incliné - Ajouter le calcul du dévers latéral ([jalhyd#177](https://forgemia.inra.fr/cassiopee/jalhyd/issues/177))
* MacroRugo: Supprimer les débits et vitesses du guide technique ([jalhyd#177](https://forgemia.inra.fr/cassiopee/jalhyd/issues/177))
* MacroRugo: Domaine de définition de Cd0 ([jalhyd#175](https://forgemia.inra.fr/cassiopee/jalhyd/issues/175))
* PAB nombre: Ajout du nombre de chutes harmonisé ([jalhyd#167](https://forgemia.inra.fr/cassiopee/jalhyd/issues/167))
* MacroRugoComplexe: modification du libellé des champs ([nghyd#333](https://forgemia.inra.fr/cassiopee/nghyd/issues/333))
* PAB: Export du tableau de géométrie au format XLSX ([jalhyd#170](https://forgemia.inra.fr/cassiopee/jalhyd/issues/170))
* MacroRugo: transition douce entre régime émergent et régime submergé ([jalhyd#191](https://forgemia.inra.fr/cassiopee/jalhyd/issues/191))

#### Correction de bogues

* Parfois, lorsque le débit varie, la PAB n'a pas d'eau à l'aval pour certaines valeurs de débit ([jalhyd#187](https://forgemia.inra.fr/cassiopee/jalhyd/issues/187))
* Lorsqu'on vide un champ, si on change de page, au retour le champ est à nouveau rempli ([nghyd#343](https://forgemia.inra.fr/cassiopee/nghyd/issues/343))
* Impact de chute: Erreur de calcul de la chute nécessaire pour atteindre l'abscisse d'impact ([jalhyd#183](https://forgemia.inra.fr/cassiopee/jalhyd/issues/183))
* Impact de jet: non prise en compte de l'angle ([jalhyd#182](https://forgemia.inra.fr/cassiopee/jalhyd/issues/182))
* Grille: Pas d'invalidation des résultats sur le choix du profil des barreaux ([nghyd#335](https://forgemia.inra.fr/cassiopee/nghyd/issues/335))
* MacroRugoCompound: radier incliné - changer la répartition des cellules ([jalhyd#173](https://forgemia.inra.fr/cassiopee/jalhyd/issues/173))

### 4.8.0 - 2019-11-26 (Affine et forte à la fois, par amour du remous)

#### Nouvelles fonctionnalités

* Module Fonction affine ([jalhyd#160](https://forgemia.inra.fr/cassiopee/jalhyd/issues/160), [nghyd#319](https://forgemia.inra.fr/cassiopee/nghyd/issues/319))
* Module Trigonométrie ([jalhyd#161](https://forgemia.inra.fr/cassiopee/jalhyd/issues/161))
* Module Somme et produit de puissances ([jalhyd#162](https://forgemia.inra.fr/cassiopee/jalhyd/issues/162))
* Loi Déversoir noyé ([jalhyd#165](https://forgemia.inra.fr/cassiopee/jalhyd/issues/165), [nghyd#318](https://forgemia.inra.fr/cassiopee/nghyd/issues/318))
* Logo animé lors du chargement de l'application ([nghyd#322](https://forgemia.inra.fr/cassiopee/nghyd/issues/322))

#### Correction de bogues

* MacroRugo: définition de la valeur par défaut de Cd0 à 1.2 ([jalhyd#166](https://forgemia.inra.fr/cassiopee/jalhyd/issues/166))
* Échec du calcul en chaîne dans certains cas ([nghyd#325](https://forgemia.inra.fr/cassiopee/nghyd/issues/325))
* Remous : suppression de LargeurBerge dans le log ([nghyd#326](https://forgemia.inra.fr/cassiopee/nghyd/issues/326))
* Remous : les tirants d'eau critique et normal sont erronés sur le graphique ([nghyd#327](https://forgemia.inra.fr/cassiopee/nghyd/issues/327))
* Remous : une fois calculé, chaque rechargement du module ajoute une copie des logs ([nghyd#324](https://forgemia.inra.fr/cassiopee/nghyd/issues/324))
* Remous : en fluvial uniquement avec forte pente, les abscisses sont fausses ([nghyd#328](https://forgemia.inra.fr/cassiopee/nghyd/issues/328))
* Calcul en chaîne : ERR inopiné dans le tableau de résultats fixes ([nghyd#329](https://forgemia.inra.fr/cassiopee/nghyd/issues/329))

### 4.7.0 - 2019-10-29 (AGB - Agence Grolandaise pour la Biodiversité)

#### Nouvelles fonctionnalités

* Solveur multi-modules ([jalhyd#152](https://forgemia.inra.fr/cassiopee/jalhyd/issues/152), [nghyd#301](https://forgemia.inra.fr/cassiopee/nghyd/issues/301))
* Nouvelle loi d'ouvrage: Orifice Dénoyé ([jalhyd#156](https://forgemia.inra.fr/cassiopee/jalhyd/issues/156), [nghyd#311](https://forgemia.inra.fr/cassiopee/nghyd/issues/311))
* Script de déploiement d'une nouvelle version (sur Aubes) ([cassiopee2-integration#9](https://forgemia.inra.fr/cassiopee/cassiopee2-integration/issues/9))
* Documentation lois d'ouvrages CEM88 V et D ([nghyd#315](https://forgemia.inra.fr/cassiopee/nghyd/issues/315))
* Remous: connecter le ressaut sur un seul point lorsque le ressaut est court (une seule abscisse) ([nghyd#312](https://forgemia.inra.fr/cassiopee/nghyd/issues/312))

#### Correction de bogues

* Lois d'ouvrages: bug à l'affichage des résultats variés lorsque le calcul échoue ([jalhyd#163](https://forgemia.inra.fr/cassiopee/jalhyd/issues/163))
* PAB : problème de cotes sur les cloisons ([jalhyd#158](https://forgemia.inra.fr/cassiopee/jalhyd/issues/158))
* Calcul en chaîne: stopper la chaîne si une erreur survient ([jalhyd#155](https://forgemia.inra.fr/cassiopee/jalhyd/issues/155))
* Diagramme de Jet / de Section : problème de rafraîchissement ([nghyd#308](https://forgemia.inra.fr/cassiopee/nghyd/issues/308))
* Remous : il manque parfois une abscisse ([jalhyd#147](https://forgemia.inra.fr/cassiopee/jalhyd/issues/147))

#### Mises à jour de dépendances

* chartjs-plugin-zoom 0.7.4

### 4.6.1 - 2019-10-15

#### Nouvelles fonctionnalités

* Suivi des comportements des utilisateurs à l'aide de Matomo (sur Aubes) ([nghyd#306](https://forgemia.inra.fr/cassiopee/nghyd/issues/306))
* Documentation: faciliter l'accès à l'application plutôt qu'à GitLab ([nghyd#307](https://forgemia.inra.fr/cassiopee/nghyd/issues/307))

#### Mises à jour de dépendances

* chartjs-plugin-zoom 0.7.4

### 4.6.0 - 2019-10-14 (Bluefish délavé)

#### Nouvelles fonctionnalités

* Ajout du module Bief ([jalhyd#55](https://forgemia.inra.fr/cassiopee/jalhyd/issues/55), [nghyd#299](https://forgemia.inra.fr/cassiopee/nghyd/issues/299))
* Ajout du module Grille ([jalhyd#114](https://forgemia.inra.fr/cassiopee/jalhyd/issues/114), [nghyd#289](https://forgemia.inra.fr/cassiopee/nghyd/issues/289))
* Ajout du module Impact de Jet ([jalhyd#112](https://forgemia.inra.fr/cassiopee/jalhyd/issues/112), [nghyd#287](https://forgemia.inra.fr/cassiopee/nghyd/issues/287))
* Ajout du module : Pente ([jalhyd#143](https://forgemia.inra.fr/cassiopee/jalhyd/issues/143), [nghyd#295](https://forgemia.inra.fr/cassiopee/nghyd/issues/295))
* Passage des courbes de remous en cotes ([jalhyd#146](https://forgemia.inra.fr/cassiopee/jalhyd/issues/146), [nghyd#298](https://forgemia.inra.fr/cassiopee/nghyd/issues/298))
* SectionParametree: remplacement de Yf et Yt par Ycor ([jalhyd#145](https://forgemia.inra.fr/cassiopee/jalhyd/issues/145), [nghyd#297](https://forgemia.inra.fr/cassiopee/nghyd/issues/297))
* Régime uniforme: ajout de la vitesse moyenne ([jalhyd#139](https://forgemia.inra.fr/cassiopee/jalhyd/issues/139))
* Page d'accueil: ajout du logo du pôle (IMFT), remplacement du texte [nghyd#208](https://forgemia.inra.fr/cassiopee/nghyd/issues/208))
* Tests e2e sur les messages de langues absents ([nghyd#294](https://forgemia.inra.fr/cassiopee/nghyd/issues/294))
* Ajout de la possibilité d'un bouton d'aide dans les résultats ([nghyd#293](https://forgemia.inra.fr/cassiopee/nghyd/issues/293))
* Electron : mise à jour automatique ([nghyd#250](https://forgemia.inra.fr/cassiopee/nghyd/issues/250))
* Intégration continue : exécution des tests e2e ([nghyd#278](https://forgemia.inra.fr/cassiopee/nghyd/issues/278))
* Préférences: applicaton de "precision" et "newtonMaxIter" à la Session en cours ([jalhyd#40](https://forgemia.inra.fr/cassiopee/jalhyd/issues/40), [nghyd#286](https://forgemia.inra.fr/cassiopee/nghyd/issues/286))
* Lorsqu'un paramètre varie, ajout dans le log global d'un résumé des erreurs/avertissements ([jalhyd#140](https://forgemia.inra.fr/cassiopee/jalhyd/issues/140))

#### Correction de bogues

* MacroRugo: écart des débits en submergé ([jalhyd#154](https://forgemia.inra.fr/cassiopee/jalhyd/issues/154))
* MacroRugo: non convergence du calcul pour les faibles profondeurs ([jalhyd#144](https://forgemia.inra.fr/cassiopee/jalhyd/issues/144))
* Remous : crash avec paramètre lié à un résultat non calculé ([jalhyd#151](https://forgemia.inra.fr/cassiopee/jalhyd/issues/151))
* Remous : lorsque la longueur du bief n'est pas un multiple du pas de discrétisation, exécuter le calcul sur la dernière abscisse tout de même ([jalhyd#153](https://forgemia.inra.fr/cassiopee/jalhyd/issues/153))
* Remous: parfois le premier point de la courbe torrentielle est absent ([jalhyd#148](https://forgemia.inra.fr/cassiopee/jalhyd/issues/148))
* Cordova: le zoom sur les graphiques n'est pas réinitialisable ([nghyd#270](https://forgemia.inra.fr/cassiopee/nghyd/issues/270))
* Chargement de session : déduire le type de structure (nodeType) de la loi de débit ([nghyd#265](https://forgemia.inra.fr/cassiopee/nghyd/issues/265))
* Lechapt-Calmon : effacer les résultats lorsqu'on change de matériau ([nghyd#291](https://forgemia.inra.fr/cassiopee/nghyd/issues/291))
* Lois d'Ouvrages: les logs ne s'affichent pas ([jalhyd#120](https://forgemia.inra.fr/cassiopee/jalhyd/issues/120), [nghyd#284](https://forgemia.inra.fr/cassiopee/nghyd/issues/284))
* Lois d'ouvrages: calculer un paramètre enfant en variant la cote aval fait planter les résultats ([nghyd#285](https://forgemia.inra.fr/cassiopee/nghyd/issues/285))

#### Mises à jour de dépendances

* chartjs-plugin-zoom 0.7.4
* ngx-markdown 8.2.1
* electron 6.0.10

### 4.5.0 - 2019-09-09 (Fish ramps that rock!)

#### Nouvelles fonctionnalités

* Passe à macro-rugosité complexe ([jalhyd#35](https://forgemia.inra.fr/cassiopee/jalhyd/issues/35), [nghyd#271](https://forgemia.inra.fr/cassiopee/nghyd/issues/271))
* Réorganisation des onglets par glisser-déposer ([nghyd#206](https://forgemia.inra.fr/cassiopee/nghyd/issues/206))
* Raccourcis clavier ([nghyd#192](https://forgemia.inra.fr/cassiopee/nghyd/issues/192))
* Ajout des graphiques de type "Points" ([nghyd#118](https://forgemia.inra.fr/cassiopee/nghyd/issues/118))
* Passe à bassins: Supprimer simultanément plusieurs bassins ([nghyd#269](https://forgemia.inra.fr/cassiopee/nghyd/issues/269))
* Sessions exemples ([nghyd#165](https://forgemia.inra.fr/cassiopee/nghyd/issues/165))
* Diagrammes des modules de calcul et de leurs liens ([nghyd#140](https://forgemia.inra.fr/cassiopee/nghyd/issues/140))
* Amélioration de la précision d'affichage ([nghyd#281](https://forgemia.inra.fr/cassiopee/nghyd/issues/281), [nghyd#29](https://forgemia.inra.fr/cassiopee/nghyd/issues/29))
* Ouverture d'un fichier de session pour chargement : prévenir si le fichier est vide ou mal formé ([nghyd#264](https://forgemia.inra.fr/cassiopee/nghyd/issues/264))

#### Correction de bogues

* Lechapt-Calmon : le sélecteur de matériaux n'a plus de sélection par défaut ([nghyd#276](https://forgemia.inra.fr/cassiopee/nghyd/issues/276))
* Lechapt-Calmon : enregistrement de la propriété "matériaux" ([jalhyd#138](https://forgemia.inra.fr/cassiopee/jalhyd/issues/138))
* Lois d'ouvrages: plantage sur deux paramètres qui varient ([nghyd#273](https://forgemia.inra.fr/cassiopee/nghyd/issues/273))
* Passe à bassins: prise en compte de la longueur des bassins dans l'interpolation ([nghyd#268](https://forgemia.inra.fr/cassiopee/nghyd/issues/268))
* Passe à bassins: il manque l'édition de la cote de radier de la cloison aval ([nghyd#277](https://forgemia.inra.fr/cassiopee/nghyd/issues/277))
* Résultats fixés: l'unité des paramètres calculés n'est pas affichée ([nghyd#274](https://forgemia.inra.fr/cassiopee/nghyd/issues/274))

### 4.4.2 - 2019-08-06

#### Nouvelles fonctionnalités

* Boutons d'aide sur tous les éléments graphiques ([nghyd#157](https://forgemia.inra.fr/cassiopee/nghyd/issues/157))
* Chargement de session: ouverture automatique du premier module nouvellement chargé
* Touche Tab dans un champ de saisie: le texte est surligné ([nghyd#259](https://forgemia.inra.fr/cassiopee/nghyd/issues/259))
* Simplification des fichiers de session
* Graphique : afficher tous les paramètres d'une même famille simultanément ([nghyd#246](https://forgemia.inra.fr/cassiopee/nghyd/issues/246))
* Réorganisation du format des résultats ([jalhyd#128](https://forgemia.inra.fr/cassiopee/jalhyd/issues/128))
* Erreur au chargement de session avec lien sur des paramètres calculés ([nghyd#263](https://forgemia.inra.fr/cassiopee/nghyd/issues/263))
* Enregistrement de session partielle avec liens tronqués: enregistrer les valeurs courantes des paramètres ([jalhyd#133](https://forgemia.inra.fr/cassiopee/jalhyd/issues/133))

#### Correction de bogues

* Documentation de la passe à Macrorugosités
* Enregistrement de session: dédoublonner les messages concernant les dépendances
* nodeType est lu depuis la Section et plus depuis le Nub parent ([jalhyd#124](https://forgemia.inra.fr/cassiopee/jalhyd/issues/124))
* Electron / Cordova : script npm pour mettre à jour les mimetypes dans dist/index.html ([nghyd#258](https://forgemia.inra.fr/cassiopee/nghyd/issues/258))
* Passe à bassins : traduction
* Graphiques : ne pas représenter les données de type ENUM ([nghyd#260](https://forgemia.inra.fr/cassiopee/nghyd/issues/260))
* Remous : l'itérateur d'abscisses est en retard sur le dessin du graphe ([nghyd#267](https://forgemia.inra.fr/cassiopee/nghyd/issues/267))

### 4.4.1 - 2019-07-30

#### Nouvelles fonctionnalités

* Passe à bassins : ajouter un accès rapide aux différents panneaux (table, résultats, graphiques...) pour éviter de faire défiler péniblement ([nghyd#237](https://forgemia.inra.fr/cassiopee/nghyd/issues/237))
* Passe à bassins : permettre d'ajouter / dupliquer un ouvrage pour plusieurs cloisons à la fois ([nghyd#243](https://forgemia.inra.fr/cassiopee/nghyd/issues/243))
* Passe à bassins : ajouter la nature du jet ([nghyd#245](https://forgemia.inra.fr/cassiopee/nghyd/issues/245))
* Lois d'ouvrages: Ne pas écraser les valeurs par défaut du coefficient de débit au changement de loi ([nghyd#225](https://forgemia.inra.fr/cassiopee/nghyd/issues/225))
* Lois d'ouvrages: Ajouter le type de jet dans les résultats complémentaires des ouvrages ([jalhyd#92](https://forgemia.inra.fr/cassiopee/jalhyd/issues/92))
* Amélioration du titre et de l'icône de l'application ([nghyd#257](https://forgemia.inra.fr/cassiopee/nghyd/issues/257))

#### Correction de bogues

* Ne pas exposer les paramètres de cloisons pour les rendre liables ([nghyd#247](https://forgemia.inra.fr/cassiopee/nghyd/issues/247), [jalhyd#111](https://forgemia.inra.fr/cassiopee/jalhyd/issues/111))
* Passe à macro-rugosités : erreur de calcul ([nghyd#247](https://forgemia.inra.fr/cassiopee/nghyd/issues/247))
* Passe à bassins : Le journal de calcul ne s'efface pas quand les données d'entrée changent ([nghyd#241](https://forgemia.inra.fr/cassiopee/nghyd/issues/241))
* Courbes de remous : les inputs ne sont plus pris en compte ([nghyd#256](https://forgemia.inra.fr/cassiopee/nghyd/issues/256))
* Invalidation de calcul bien qu'absence de lien de résultat ([jalhyd#98](https://forgemia.inra.fr/cassiopee/jalhyd/issues/98))
* Lois d'ouvrages: Erreur de calcul des lois de seuil / vanne ([jalhyd#118](https://forgemia.inra.fr/cassiopee/jalhyd/issues/118))
* Lechapt-Calmon : le sélecteur de matériaux ne charge plus les coefficients, depuis la 4.4.0a ([nghyd#231](https://forgemia.inra.fr/cassiopee/nghyd/issues/231))
* Cloisons: Erreur de calcul de la charge ([jalhyd#127](https://forgemia.inra.fr/cassiopee/jalhyd/issues/127))
* Cloisons: Erreur de calcul de la cote de radier si la charge est en calcul ([jalhyd#126](https://forgemia.inra.fr/cassiopee/jalhyd/issues/126))
* Passe à macro-rugosité: Écart entre le débit calculé et celui du guide technique ([jalhyd#113](https://forgemia.inra.fr/cassiopee/jalhyd/issues/113))
* Passe à macro-rugosité: Erreurs de calcul ([jalhyd#85](https://forgemia.inra.fr/cassiopee/jalhyd/issues/85))
* Paramètre varié : le champ d'édition de la liste de valeurs s'affiche mal ([nghyd#244](https://forgemia.inra.fr/cassiopee/nghyd/issues/244))
* Paramètres liables : parfois le mat-select est trop étroit et on ne sait pas ce qu'on est en train de choisir ([nghyd#248](https://forgemia.inra.fr/cassiopee/nghyd/issues/248))

#### Mises à jour de dépendances

* Angular 8.1.2

### 4.4.0 - 2019-07-16 (Basse à Sapins)

#### Nouvelles fonctionnalités

* Module "Passe à bassins"
* Plusieurs paramètres peuvent varier simultanément
* Implémentation de la vanne levante
* Implémentation de la loi de Villemonte sur les seuils triangulaires et triangulaires tronqués
* Remplacement de la cote de radier par la charge sur les seuils de cloisons
* Remplacement de la dichotomie par la méthode de Brent
* Simplification de la loi Kivi pour les cloisons et les PAB
* Déploiement avec Electron : paquets installables pour Windows 32 bits et Linux (.deb)
* Déploiement avec Cordova : paquet .apk (non signé) pour Android
* Zoom sur les graphiques
* Bouton d'aide dans la barre de navigation, lorsque la session est vide
* Carte de bienvenue lorsque la session est vide: logos et mentions légales
* Système de rapport de bugs par email
* Paramètres liés: affichage de la valeur, icônes d'information
* Compilation: allègement de la bibliothèque Mathjax embarquée
* Magnifique icône en SVG
* Option pour désactiver les notifications à l'écran
* Nouveaux tests

#### Correction de bogues

* Correction erreur de calcul de ZDV
* Corrections d'erreurs liées au cycle de vie d'Angular
* Structures en parallèle: interdiction de supprimer le dernier ouvrage
* Mode plein écran compatible avec les navigateurs plus anciens
* Amélioration de la robustesse de l'enregistrement / chargement de session
* Suppression du lissage sur les graphiques de type "scatter"
* Nettoyage de code
* Limitation de la précision numérique à différents endroits

### 4.3.0 - 2019-04-12 (Éditions LLL)

#### Nouvelles fonctionnalités

* Module "Passe à bassin : chute"
* Module "Passe à bassin : nombre de bassins"
* Amélioration du filtre de choix des paramètres liables
* Vérification de la cohérence des paramètres liés au chargement de session
* Validation et invalidation en cascade des modules de calcul liés
* Calcul en cascade automatique des modules de calculs liés
* Transfert de ngHyd vers JaLHyd des mécanismes gérant les états des paramètres des modules de calcul
* Ajout des tags de version dans le panneau latéral
* La touche TAB permet de passer directement d'un champ de saisie à un autre
* Le bouton "+" disparaît sur la page d'accueil

#### Correction de bogues

* Divers bogues autour des paramètres liés
* Mise à jour intempestive des paramètres calculés dans le formulaire de saisie
* Précision d'affichage des données dans les infobulles

### 4.2.0 - 2019-03-11

#### Nouvelles fonctionnalités

* titres courts pour les modules, suffixe numérique automatique
* Lechapt-Calmon : amélioration du sélecteur de matériau
* affichage des valeurs liées
* détection de la langue du navigateur
* mémorisation des paramètres par le navigateur
* mécanisme de langue de secours pour les modules non traduits
* paramètres variables : courbe d'évolution
* graphiques de résultats : choix libre de l'abscisse et de l'ordonnée

#### Correction de bogues

* déplacement de la sérialisation au niveau du modèle (JaLHyd)
* nouvelle gestion des langues: plus robuste, charge moins de fichiers inutiles, ajout d'un cache
* meilleure gestion de la session et de la hiérarchie (ouvrages en parallèle / parent)
* gestion homogène de la touche entrée dans les formulaires : déclenche le calcul
* la précision Pr est traitée comme un paramètre normal
* simplification de la gestion des types d'ouvrages
* désérialisation des ouvrages en parallèle
* validation des ouvrages en parallèle
* validation des paramètres variables
* simplification et suppression de code inutilisé
* affichage des icônes et polices hors-ligne
* conservation du type de graphe lorsqu'on change de module
* ajout de tests exhaustifs sur le calcul des paramètres et le clonage des modules

### 4.1.0 - 2019-02-20

#### Nouvelles fonctionnalités

* interface : angular-material, angular-flex, charte graphique Irstea
* nouvelle page de liste, modules groupés par thèmes
* ajout du module Passe à Enrochement
* bouton pour vider la session
* bouton pour cloner un module de calcul
* fichier de configuration JSON, gestion d'une langue par défaut
* paramètre variable : amélioration des listes de valeurs
* affichage des graphiques et des tableaux de résultats en plein écran
* export des graphiques en PNG
* export des tableaux de résultats vers Excel
* utilisation de chaînes pour les UID
* utilisation de routerLink et des fragments d'URL (#)
* ajout de tests e2e avec Protractor
* limitation de la précision dans les graphiques

#### Correction de bogues

* redirection des URL invalides vers /list
* chargement de paramètres liés
* sauvegarde et chargement des valeurs des paramètres
* IDs uniques dans les champs de formulaires
* nettoyage du code (tslint)
* suppression de code inutilisé
* renommage et simplification de classes
* amélioration de la traduction

#### Mises à jour de dépendances

* Angular 7.2
* Compodoc
* angular-material
* angular-flex
* suppression de MDBootstrap
* suppression de FontAwesome

### 4.0.0 - 2018-07-23