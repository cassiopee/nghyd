site_name: Documentation de Cassiopée
site_author: UMR G-EAU
docs_dir: docs/fr
site_dir: src/assets/docs/fr/
copyright: CC BY-NC-ND 4.0, UMR G-EAU 2019 - <a href="https://cassiopee.g-eau.fr">https://cassiopee.g-eau.fr</a>
repo_name: ''
repo_url: 'https://gitlab.irstea.fr/cassiopee'
theme:
    name: 'material'
    language: 'fr'
use_directory_urls: false
extra_css:
    - ../stylesheets/mathjax-scrollbar.css
extra_javascript:
    - ../javascripts/mathjax.config.js
    - ../javascripts/mathjax/tex-mml-chtml.js
    - ../javascripts/matomo-tracking.js
    - ../javascripts/mermaid.min.js
markdown_extensions:
    - mdx_math
    - footnotes
    - codehilite
    - pymdownx.arithmatex:
        generic: true
nav:
    - Présentation de Cassiopée:
        - index.md
        - general/installation.md
        - general/principe_fonctionnement.md
        - general/parametres_application.md
        - general/raccourcis_clavier.md
    - Hydraulique en charge:
        - calculators/hyd_en_charge/perte_de_charge.md
        - calculators/hyd_en_charge/lechapt-calmon.md
        - calculators/hyd_en_charge/strickler.md
        - calculators/hyd_en_charge/cond_distri.md
    - Hydraulique à surface libre:
        - Régime uniforme: calculators/hsl/regime_uniforme.md
        - calculators/hsl/courbe_remous.md
        - calculators/hsl/cote_amont_aval.md
        - calculators/hsl/section_parametree.md
        - calculators/hsl/pente.md
        - calculators/hsl/types_sections.md
        - calculators/hsl/strickler.md
    - Lois d'ouvrages:
        - calculators/structures/lois_ouvrages.md
        - calculators/structures/dever.md
        - Cloisons: calculators/pab/cloisons.md
        - Equations d'ouvrages:
            - calculators/structures/liste.md
            - calculators/structures/kivi.md
            - calculators/structures/orifice_noye.md
            - calculators/structures/orifice_denoye.md
            - calculators/structures/fente_noyee.md
            - calculators/structures/seuil_noye.md
            - calculators/structures/seuil_denoye.md
            - calculators/structures/dever_triang.md
            - calculators/structures/dever_triang_tronque.md
            - calculators/structures/cem_88_v.md
            - calculators/structures/cem_88_d.md
            - calculators/structures/cunge_80.md
            - calculators/structures/vanne_denoyee.md
            - calculators/structures/vanne_noyee.md
            - calculators/structures/villemonte_1947.md
    - Passes à bassins:
        - Chute: calculators/pab/chute.md
        - Nombre de chutes: calculators/pab/nombre.md
        - Puissance dissipée: calculators/pab/volume.md
        - Dimensions: calculators/pab/dimensions.md
        - calculators/pab/cloisons.md
        - calculators/pab/pab.md
        - calculators/pab/prebarrage.md
    - Passes à macro-rugosité:
        - calculators/pam/macrorugo.md
        - calculators/pam/macrorugo_theorie.md
        - calculators/pam/macrorugo_complexe.md
        - calculators/pam/concentration.md
    - Passes à ralentisseurs:
        - calculators/par/calage.md
        - calculators/par/simulation.md
        - calculators/par/formules.md
        - calculators/par/theorie_plans.md
        - calculators/par/theorie_fatou.md
        - calculators/par/theorie_suractif.md
        - calculators/par/theorie_mixte.md
    - Vérification des critères de franchissement:
        - Principe: calculators/verif/principe.md
        - Passes à bassins: calculators/verif/pab.md
        - Passes à ralentisseurs: calculators/verif/par.md
        - Passes à macrorugosités: calculators/verif/macrorugo.md
        - Espèces prédéfinies: calculators/verif/especes_predefinies.md
    - Dévalaison:
        - Perte de charge sur grille de prise d'eau: calculators/devalaison/grille.md
        - Impact de jet: calculators/devalaison/jet.md
    - Outils mathématiques:
        - calculators/maths/operators.md
        - calculators/maths/solver.md
    - Méthodes numériques de résolution:
        - Runge-Kutta 4: methodes_numeriques/rk4.md
        - Euler explicite: methodes_numeriques/euler_explicite.md
        - Intégration de trapèzes: methodes_numeriques/integration_trapezes.md
        - Méthode de Brent: methodes_numeriques/brent.md
        - Méthode de Newton: methodes_numeriques/newton.md
    - CHANGELOG.md
    - mentions_legales.md
